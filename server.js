import basicAuth from "express-basic-auth";
import axios from 'axios';
import express from "express";
import compression from "compression";
import path from "path";
import React from "react";
import { renderToString } from "react-dom/server";
import { Provider as ReduxProvider } from "react-redux";
import createStore from "./src/store";
import { StaticRouter } from "react-router-dom";
import {
  fetchReviewData,
  fetchTeamData,
  fetchFaqsData,
  fetchBecomePartnerData,
  fetchAboutData,
  fetchHomepageData,
  fetchLandingData,
  fetchLandingConfirmationData,
  whySMData,
  howIWData,
  fetchAllCities,
  setBodyClass,
  fetchServices,
  setCurrentCity,
  fetchAllServices,
  fetchDateTimeAvailability,
  setSignIn,
  fetchUserProfile,
  submitVerifyCreditCard,
  fetchDataDictionaryValues,
  fetchPopupData,
  activateBookingRequest,
  postLWRequest,
  fetchDataConstantValues,
  fetchFooterItemsData,
  isLiteWeightBooking,
  isCoreBooking,
  fetchDateTime,
  isBotData,
  fetchAllApiServices,
  DEFAULT_CITY_NAME,
  URLCONSTANT,
  JEDDAH, RIYADH, DAMMAM,
  LANG_EN,
  cityCodes, fetchProviderByURLDispatch, fetchAllProviders, fetchwpAllServicesWithCity, setCurrentLang, setPageParams, DEFAULT_LANG, LANG_AR, arabicAllowedPages,
  arabicAllowedCities,
  fetchAllCitiesForRedirection,
  showLangNav,
  DUBAI,
  fetchAllReviews
} from "./src/actions/index";
import Layout from "./src/pages/Layout";
import ZohoLayout from "./src/pages/ZohoLayout";
import qs from "qs";
import fileSystem from 'fs';
import { CookiesProvider } from "react-cookie";
import cookiesMiddleware from 'universal-cookie-express';
var minify = require('html-minifier').minify;
import commonHelper from "./src/helpers/commonHelper";
import {
  fetchCompaniesPageData,
  fetchDisclaimerData,
  fetchPrivacyData,
  fetchProviderByURL,
  fetchPRPData,
  fetchTermsData,
  fetchUserDetailsByResetTokenCustomer
} from "./src/actions";
import staticVariables from "./src/staticVariables";
import { getBundles } from 'react-loadable/webpack';
import Loadable from 'react-loadable';
import stats from './dist/js/react-loadable.json';
import ieStaticPopupContent from './dist/ie_static_popup/index';
//var slash   = require('express-slash');
var bodyParser = require('body-parser')

const dotenv = require('dotenv');
dotenv.config();

if (process.env.NODE_ENV == "PROD") {
  var bugsnag = require('@bugsnag/js');
  var bugsnagExpress = require('@bugsnag/plugin-express');
  var bugsnagClient = bugsnag(staticVariables.BUGSNAG_SERVER_APP_ID);
  bugsnagClient.use(bugsnagExpress);
  var bugsnagMiddleware = bugsnagClient.getPlugin('express');
}

const app = express();

var sm = require('sitemap');

app.enable('strict routing');

/*if(typeof bugsnagMiddleware != "undefined"){
    app.use(bugsnagMiddleware.requestHandler);
}*/

app.use(compression({ threshold: 0 }));
const bundleJSFileName = 'appv51';

const allowedCities = ['dubai', 'abu-dhabi', 'sharjah', 'doha', 'jeddah', 'riyadh', 'kuwait', 'bahrain', 'dammam', 'muscat', 'singapore', 'kuala-lumpur', 'hong-kong'];
const allowedPages = ['about-us', 'become-a-partner', 'write-a-review', 'careers', 'faqs', 'contact-us', 'privacy-policy', 'disclaimer', 'terms-and-conditions', 'payment-and-refund-policy', 'profile'];

//app.use(slash());
app.use('/dist', express.static(path.join(__dirname, 'dist'), { maxAge: 0 }))
  .use(cookiesMiddleware());


app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));

app.use(function (req, res, next) {
  const browserName = getBrowserNameAndVersion(req.headers['user-agent']);

  if (browserName === "IE") {
    res.send(renderLayoutForIE());
  }
  else {
    next()
  }
});

function renderLayoutForIE() {
  return ieStaticPopupContent;
}

const getBrowserNameAndVersion = (userAgent) => {
  var ua = userAgent,
    tem,
    M =
      ua.match(
        /(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i
      ) || [];
  if (/trident/i.test(M[1])) {
    tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
    return 'IE';
  }
  if (M[1] === 'Chrome') {
    tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
    if (tem != null)
      return tem
        .slice(1)
        .join(' ')
        .replace('OPR', 'Opera');
  }
  M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
  if ((tem = ua.match(/version\/(\d+)/i)) != null) M.splice(1, 1, tem[1]);
  return M.join(' ');
}

//app.use(express.multipart());
function renderLayout(store, context, req) {
  let modules = [];
  const jsx = renderToString(
    <Loadable.Capture report={moduleName => modules.push(moduleName)}>
      <CookiesProvider cookies={req.universalCookies}>
        <ReduxProvider store={store}>
          <StaticRouter context={context} location={req.url}>
            <Layout />
          </StaticRouter>
        </ReduxProvider>
      </CookiesProvider>
    </Loadable.Capture>
  );
  //console.log("moduleName", modules);
  let bundles = getBundles(stats, modules);
  const reactDom = jsx;
  return { reactDom: reactDom, lang: req.params.lang, originalUrl: req.originalUrl, bundles: bundles };

}
function renderZohoLayout(store, context, req) {
  const jsx = (
    <CookiesProvider cookies={req.universalCookies}>
      <ReduxProvider store={store}>
        <StaticRouter context={context} location={req.url}>
          <ZohoLayout />
        </StaticRouter>
      </ReduxProvider>
    </CookiesProvider>
  );
  const reactDom = renderToString(jsx);
  return reactDom;
}
function locationDetection(ip) {
  return axios({
    method: 'get',
    url: 'https://api.ipstack.com/' + ip + '?access_key=a136e9deffd73e54cb0badd3401735ff'
  }).then(function (res) {
    var response = res.data;

    var city = DEFAULT_CITY_NAME;

    if (response.country_code) {
      let cityName = (typeof response.city != "undefined" && response.city != null) ? response.city.toLowerCase() : "";
      switch (response.country_code) {
        case "AE":
          if (cityName == 'abu dhabi') {
            city = 'abu-dhabi';
          } else if (cityName == 'sharjah') {
            city = 'sharjah';
          }
          break;

        case "QA":
          city = 'Doha';
          break;

        case "SA":
          if (cityName == 'jeddah') {
            city = 'jeddah';
          } else if (cityName == 'dammam') {
            city = 'dammam';
          } else {
            city = 'riyadh';
          }
          break;

        case "KW":
          city = 'kuwait';
          break;

        case "BH":
          city = 'bahrain';
          break;

        case "OM":
          city = 'muscat';
          break;

        default:
          city = DEFAULT_CITY_NAME;
      }
    }
    return city;
  })
    .catch(function (error) {
      // handle error
      // console.log("locationDetection Error", error);
    });
}
const getIpAddressFromRequest = (request) => {
  var ip = request.headers['x-forwarded-for'] || request.connection.remoteAddress;
  return ip;
};

app.get("/google422bc2a17274c281.html", (req, res) => {
  res.sendFile(path.join(__dirname + '/google422bc2a17274c281.html'));
});
app.get("/google35128b977544e599.html", (req, res) => {
  res.sendFile(path.join(__dirname + '/google35128b977544e599.html'));
});
app.get("/robots.txt", (req, res) => {
  res.sendFile(path.join(__dirname + '/robots.txt'));
});
app.get("/en/apple-app-site-association", (req, res) => {
  res.sendFile(path.join(__dirname + '/apple-app-site-association.p7c'));
});
// app.get("/sitemap.xml", (req, res) => {
//     res.contentType('application/xml');
//     res.sendFile(path.join(__dirname + '/sitemap.xml'));
// });
app.use("/save-local-storage", (req, res) => {
  let body = '';
  if (typeof req.body != "undefined" && Object.keys(req.body).length) {
    let cookieName = Object.keys(req.body)[0];
    let data = req.body;
    if (isLocalStorageAvailable()) {
      if (typeof localStorage === "undefined" || localStorage === null) {
        var LocalStorage = require('node-localstorage').LocalStorage;
        let localStorage = new LocalStorage('./scratch');
        localStorage.setItem(cookieName, data[cookieName]);
        //console.log(localStorage.getItem(cookieName));
      }
    }
  }
  res.end("Saved");
})
app.get('/sitemap.xml', function (req, res) {
  const promiseProviders = fetchAllProviders(false);
  const promiseAllServices = fetchwpAllServicesWithCity();
  //const promiseAllArabicServices = fetchwpAllServicesWithCity(LANG_AR);
  const store = createStore();
  const services = store.dispatch(fetchAllServices());
  //const servicesArabic = store.dispatch(fetchAllServices(LANG_AR));
  const newDataConstants = store.dispatch(fetchDataConstantValues());

  Promise.all([promiseProviders, promiseAllServices, services, newDataConstants]).then(rrr => {
    const reduxState = store.getState();
    var providers = [];
    //console.log("rrr",rrr[0])
    rrr[0].map(item => {
      if (item.url != "") {
        providers.push({
          url: '/service-provider/' + item.url,
          changefreq: 'weekly',
          priority: 0.7
        })
      }
    });

    var services = [];
    var addedJourneys = new Set();
    var addedBooking = new Set();
    var cities = new Set();
    let city = "";
    if (typeof rrr[1] != "undefined" && rrr[1] != null) {
      rrr[1].map(item => {
        city = commonHelper.slugify(item.city);
        services.push({
          url: '/en/' + commonHelper.slugify(item.city) + '/' + item.url_slug,
          changefreq: 'weekly',
          priority: 0.7
        });

        if (arabicAllowedCities.includes(city)) {
          //console.log(' asdsa saf saf', city);
          services.push({
            url: '/' + LANG_AR + '/' + commonHelper.slugify(item.city) + '/' + item.url_slug,
            changefreq: 'weekly',
            priority: 0.7
          });
        }

        if (!addedBooking.has(commonHelper.slugify(item.city) + '/' + item.book_now_journey) && item.book_now_journey != '-- Select if any --' && item.serviceID == 'SERVICE_HOME_INSURANCE' && item.serviceID == 'SERVICE_CAR_INSURANCE') {
          addedBooking.add(commonHelper.slugify(item.city) + '/' + item.book_now_journey);
          services.push({
            url: '/en/' + commonHelper.slugify(item.city) + '/' + item.book_now_journey + '/book-online',
            priority: 0.8
          });
        }
        if (!addedJourneys.has(commonHelper.slugify(item.city) + '/' + item.get_quote_journey) && item.get_quote_journey != '-- Select if any --' && item.serviceID == 'SERVICE_HOME_INSURANCE' && item.serviceID == 'SERVICE_CAR_INSURANCE') {
          addedJourneys.add(commonHelper.slugify(item.city) + '/' + item.get_quote_journey);
          services.push({
            url: '/en/' + commonHelper.slugify(item.city) + '/' + item.get_quote_journey + '/journey1',
            priority: 0.8
          });
        }

        if (!cities.has(commonHelper.slugify(item.city))) {
          cities.add(commonHelper.slugify(item.city));
          if (arabicAllowedCities.includes(city)) {
            services.push({
              url: '/' + LANG_AR + '/' + city + '/partners-list',
              changefreq: 'weekly',
              priority: 0.7
            });
          }

          services.push({
            url: '/en/' + commonHelper.slugify(item.city),
            changefreq: 'weekly',
            priority: 0.7
          });
        }
        reduxState.allServices.parentServices.map(item2 => {
          var slug = commonHelper.getServiceSlugByParentID(URLCONSTANT, reduxState.serviceConstants, item2.id);
          if (slug == item.url_slug) {
            services.push({
              url: '/en/' + commonHelper.slugify(item.city) + '/' + item.url_slug + '/reviews',
              priority: 0.8
            });
          }
        })

      });
    }

    var urls = [
      {
        url: '/',
        changefreq: 'daily',
        priority: 1.0
      },
      {
        url: '/en/about-us',
        priority: 0.7
      },
      {
        url: '/en/contact-us',
        priority: 0.7
      },
      {
        url: '/en/faqs',
        priority: 0.9
      },
      {
        url: '/en/blog',
        priority: 0.5
      },
      {
        url: '/en/write-a-review',
        priority: 0.5
      },
      {
        url: '/en/careers',
        priority: 0.7
      },
      {
        url: '/en/privacy-policy',
        priority: 0.7
      },
      {
        url: '/en/disclaimer',
        priority: 0.7
      },
      {
        url: '/en/become-a-partner',
        priority: 0.9
      },
      {
        url: '/ar/about-us',
        priority: 0.7
      },
      {
        url: '/ar/contact-us',
        priority: 0.7
      },
      {
        url: '/ar/faqs',
        priority: 0.9
      },
      {
        url: '/ar/write-a-review',
        priority: 0.5
      },
      {
        url: '/ar/careers',
        priority: 0.7
      },
      {
        url: '/ar/privacy-policy',
        priority: 0.7
      },
      {
        url: '/ar/disclaimer',
        priority: 0.7
      },
      {
        url: '/ar/become-a-partner',
        priority: 0.9
      }
    ];

    urls = urls.concat(services);
    urls = urls.concat(providers);

    var sitemap = sm.createSitemap({
      hostname: 'https://servicemarket.com',
      urls: urls,
      xslUrl: 'sitemap.xsl'
    });

    sitemap.toXML(function (err, xml) {
      if (err) {
        return res.status(500).end();
      }
      res.header('Content-Type', 'application/xml');
      res.send(xml);
    });
  })


});
app.get("/sitemap.xsl", (req, res) => {
  res.sendFile(path.join(__dirname + '/sitemap.xsl'));
});
app.get("/en/sitemap.xsl", (req, res) => {
  res.sendFile(path.join(__dirname + '/sitemap.xsl'));
});

// Partner Details SSR
app.get("/service-provider/:provider", (req, res) => {
  req.params.lang = 'en';
  req.params.city = getDefaultCity(req);

  const context = {};
  var { city, lang } = req.params;
  const store = createStore();

  let dipatchCall = commonDispatch(store, req);
  const bodyClass = store.dispatch(setBodyClass('home'));
  dipatchCall.push(bodyClass);
  const extraData = ['whySMData', 'howIWData', 'logosData', 'footerItemsData'];
  const dataPromise = store.dispatch(fetchHomepageData(city, extraData));
  dipatchCall.push(dataPromise);
  const currentProvider = store.dispatch(fetchProviderByURLDispatch(req.params.provider));
  dipatchCall.push(currentProvider);
  Promise.all(dipatchCall).then(() => {
    const reduxState = store.getState();
    if (!reduxState.currentProvider) {
      res.redirect('/en/dubai');
    }
    res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });

    var offeredServices = '';
    if (reduxState.currentProvider && reduxState.currentProvider.offeredServices.length) {
      reduxState.currentProvider.offeredServices.map((item) => {
        offeredServices = offeredServices + (item.serviceName) + ',';
      });
    }
    var parentsServices = [];
    var nestedServices = reduxState.currentProvider.offeredServices.reduce((accumulator, pilot) => {
      if (pilot.parentServiceId != null) {
        if (!accumulator[pilot.parentServiceId]) {
          accumulator[pilot.parentServiceId] = [];
        }
        if (pilot.serviceStatus == 1) {
          accumulator[pilot.parentServiceId].push({ id: pilot.serviceId, value: pilot, label: pilot.serviceLabel });
        }
      } else {
        parentsServices.push({ id: pilot.serviceId, value: pilot, label: pilot.serviceLabel });
      }
      return accumulator;
    }, []);

    var serviceProviderTemplate = commonHelper.replaceAll(JSON.stringify(commonHelper.getServiceProviderDetails(reduxState.currentProvider, nestedServices, parentsServices)), "'", "\\'");

    // console.log(serviceProviderTemplate);

    res.end(htmlTemplate(renderLayout(store, context, req), bundleJSFileName, 1050, reduxState, { title: reduxState.currentProvider.name + ' - ' + reduxState.currentProvider.headOffice.addressResponse.city + " | ServiceMarket", desc: reduxState.currentProvider.numberOfReviews + ' reviews for ' + reduxState.currentProvider.name + ' - Read reviews and get quotes for ' + offeredServices + ' from ' + reduxState.currentProvider.name + ' on ServiceMarket' }, false, '', false, serviceProviderTemplate));
  }).catch(function (error) {

    // handle error
    // console.log("Error Fetch", error);
  });
});
app.get("/reset/:resetKey", (req, res) => {
  /* 
  req.params.lang = 'en';
  req.params.city = DEFAULT_CITY_NAME;

  const context = {};
  var city = req.params.city;
  const store = createStore();
  // Data Constants
  const services = store.dispatch(fetchAllServices());
  const dataConstants = store.dispatch(fetchDataDictionaryValues());
  const newDataConstants = store.dispatch(fetchDataConstantValues());
  const DateTimeAvailability = store.dispatch(fetchDateTimeAvailability());

  store.dispatch(isBotData(req.headers['user-agent'].indexOf('bot') != -1));
  const currentCity = store.dispatch(setCurrentCity(req.params.city));
  const extraData = ['whySMData', 'howIWData', 'logosData', 'footerItemsData'];
  const dataPromise = store.dispatch(fetchHomepageData(req.params.city, extraData));
  const citiesPromise = store.dispatch(fetchAllCities(req.params.city));
  const servicesPromise = store.dispatch(fetchServices(req.params.city));
  const bodyClass = store.dispatch(setBodyClass('home'));
  var userDetialsCookie = req.universalCookies.get('_user_details') || {};
  const userDetails = store.dispatch(setSignIn(userDetialsCookie));
  const resetUserDetails = store.dispatch(fetchUserDetailsByResetTokenCustomer(req.params.resetKey));
  const userProfile = store.dispatch(fetchUserProfile(userDetialsCookie.access_token));
  //const popupData = store.dispatch(fetchPopupData());
  const currentDateTime = store.dispatch(fetchDateTime());
  const fetchAllApiServicesPromise = fetchAllApiServicesPromiseCall(store, city);
  */
  req.params.lang = 'en';
  req.params.city = getDefaultCity(req);
  const context = {};
  const store = createStore();
  const resetUserDetails = store.dispatch(fetchUserDetailsByResetTokenCustomer(req.params.resetKey));
  let userLang = DEFAULT_LANG;
  let userCity = DEFAULT_CITY_NAME;
  Promise.all([resetUserDetails]).then((resetData) => {
    if (resetData.length && resetData[0].success) {
      resetData = resetData[0].data;
      if (typeof resetData.preferredLanguage != "undefined" && resetData.preferredLanguage.code) {
        userLang = resetData.preferredLanguage.code;
        userCity = ((typeof resetData.address != "undefined" && typeof resetData.address.city != "undefined") && resetData.address.city != null) ? commonHelper.slugify(resetData.address.city) : userCity
      }
    }
    req.params.lang = userLang;
    req.params.city = userCity;
    var { city, lang } = req.params;
    if (typeof req.headers['user-agent'] != "undefined") {
      store.dispatch(isBotData(req.headers['user-agent'].indexOf('bot') != -1));
    }
    let dipatchCall = commonDispatch(store, req);
    const bodyClass = store.dispatch(setBodyClass('home'));
    dipatchCall.push(bodyClass);
    const extraData = ['whySMData', 'howIWData', 'logosData', 'footerItemsData'];
    const dataPromise = store.dispatch(fetchHomepageData(city, extraData, lang));
    dipatchCall.push(dataPromise);
    dipatchCall.push(resetUserDetails);
    Promise.all(dipatchCall).then(() => {
      const reduxState = store.getState();
      res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
      res.end(htmlTemplate(renderLayout(store, context, req), bundleJSFileName, 1050, reduxState));
    }).catch(function (error) {
      // handle error
      // console.log("Error Fetch", error);
    });
  }).catch(function (error) {

  })
});
app.get("/", (req, res) => {
  // HANDLE DEFAULT BROWSER REQUEST FOR FAVICON
  if (req.url === '/favicon.ico') {
    res.writeHead(200, { 'Content-Type': 'image/x-icon' });
    res.end();
    return;
  }
  //let user_city = getDefaultCity(req);

  let user_city = req.universalCookies.get('user_city') || "";

  if (user_city != "") {
    res.redirect('/en/' + user_city);
  } else {
    // Locaion Redirect
    let ipAddress = getIpAddressFromRequest(req);
    var loc = locationDetection(ipAddress);
    var citiesLoc = fetchAllCitiesForRedirection();
    Promise.all([loc, citiesLoc]).then(([city, allCities]) => {
      let redirectedURL = "";
      allCities = [];
      if (typeof allCities != "undefined" && allCities.length) {
        var cityData = allCities.filter((item) => city == commonHelper.slugify(item.name))
        req.universalCookies.set("user_city", city, { path: '/' });
        if (cityData.length) {
          let defaultLang = cityData[0].supportedLanguages.filter((item) => item.default == 1);
          redirectedURL = '/' + defaultLang[0].language + '/' + city;
        } else {
          redirectedURL = "/en/" + city;
        }
        res.redirect(redirectedURL);
      } else {
        //console.log("else 312412")
        redirectedURL = "/en/" + DUBAI;
        req.universalCookies.set("user_city", DUBAI, { path: '/' });
        res.redirect(redirectedURL);
      }
    })
  }
});
app.get("/ip-test", (req, res) => {

  if (typeof req.query.ip_address != "undefined") {
    let ipAddress = req.query.ip_address;
    var loc = locationDetection(ipAddress);
    var citiesLoc = fetchAllCitiesForRedirection();
    Promise.all([loc, citiesLoc]).then(([city, allCities]) => {
      var cityData = allCities.filter((item) => city == commonHelper.slugify(item.name))
      if (cityData.length) {
        let defaultLang = cityData[0].supportedLanguages.filter((item) => item.default == 1);
        req.universalCookies.set("user_city", city, { path: '/' });
        let redirectedURL = '/' + defaultLang[0].language + '/' + city;
        res.redirect(redirectedURL);
      }
    })
  } else {
    res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
    res.end("IP Address missing");
  }
})
// Maid Review Page SSR
app.get("/:lang/rate-your-service/:leadId", (req, res) => {

    const context = {};
    const store = createStore();
    req.params.city = getDefaultCity(req);
    let {city,lang} = req.params;
    let dipatchCall = commonDispatch(store, req);
    const dataConstants = store.dispatch(fetchDataDictionaryValues(lang));
    const newDataConstants = store.dispatch(fetchDataConstantValues(lang));
    dipatchCall.push(dataConstants);
    dipatchCall.push(newDataConstants);
    const  bodyClass = store.dispatch(setBodyClass('bg-white'));
    dipatchCall.push(bodyClass);
    
    Promise.all(dipatchCall).then(() => {     
        const reduxState = store.getState();
        res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
        res.end(htmlTemplate(renderLayout(store, context, req), bundleJSFileName, 1050, reduxState));
    }).catch(function (error) {
        // handle error
        // console.log("Error Fetch", error);
    });
});
// Maid Review Page SSR
app.get("/:lang/users/:typeOfReview/:eventId", (req, res) => {

    const context = {};
    req.params.city = getDefaultCity(req);
    let {city,lang} = req.params;

    const store = createStore();
    let dipatchCall = commonDispatch(store, req);
    const dataConstants = store.dispatch(fetchDataDictionaryValues(lang));
    const newDataConstants = store.dispatch(fetchDataConstantValues(lang));
    dipatchCall.push(dataConstants);
    dipatchCall.push(newDataConstants);
    const  bodyClass = store.dispatch(setBodyClass('bg-white'));
    dipatchCall.push(bodyClass);

    Promise.all(dipatchCall).then(() => {    
        const reduxState = store.getState();
        res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
        res.end(htmlTemplate(renderLayout(store, context, req), bundleJSFileName, 1050, reduxState));
    }).catch(function (error) {
        // handle error
        // console.log("Error Fetch", error);
    });

});

// app.get("/test", (req, res) => {
//     res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
//     var loc = locationDetection(getIpAddressFromRequest(req));
//     Promise.all([loc]).then(city => {
//         res.end('Redirect to ' + city[0] + ' User IP: ' + getIpAddressFromRequest(req));
//     })
// });
app.get("/:lang", (req, res) => {
  // HANDLE DEFAULT BROWSER REQUEST FOR FAVICON
  if (req.url === '/favicon.ico') {
    res.writeHead(200, { 'Content-Type': 'image/x-icon' });
    res.end();
    return;
  }
  let user_city = req.universalCookies.get('user_city') || "";
  if (user_city == "") {
    let ipAddress = getIpAddressFromRequest(req);
    var loc = locationDetection(ipAddress);
    var citiesLoc = fetchAllCitiesForRedirection();
    Promise.all([loc, citiesLoc]).then(([city, allCities]) => {
      let redirectedURL = "";
      if (typeof allCities != "undefined" && allCities.length) {
        var cityData = allCities.filter((item) => city == commonHelper.slugify(item.name))
        req.universalCookies.set("user_city", city, { path: '/' });
        if (cityData.length) {
          let defaultLang = cityData[0].supportedLanguages.filter((item) => item.default == 1);
          redirectedURL = '/' + defaultLang[0].language + '/' + city;
        } else {
          redirectedURL = "/en/" + city;
        }
        res.redirect(redirectedURL);
      } else {
        redirectedURL = "/en/" + DUBAI;
        req.universalCookies.set("user_city", city, { path: '/' });
        res.redirect(redirectedURL);
      }

    })
  } else {
    let city = getDefaultCity(req);
    let allowedLang = getAllowedLang(city);
    if (allowedLang.indexOf(req.params.lang) != -1) {
      res.redirect('/' + req.params.lang + '/' + city);
    } else {
      res.redirect('/en/' + city);
    }
  }
});

function arActivatedPages(req, city, slug = "") {

  let allowedCity = [JEDDAH, RIYADH, DAMMAM];

  let activatedPages = [];
  let returnData = "";
  let SwithchURL = req.url;

  if (!(req.params.lang == LANG_AR && (allowedCity.includes(city) && activatedPages.includes(slug)))) {
    returnData = SwithchURL.replace("/" + LANG_AR + "/", "/" + LANG_EN + "/");
    return returnData;
  }
  return true;

}
// Contact us Page SSR
app.get("/:lang/profile", (req, res) => {
  let { lang } = req.params;
  let city = getDefaultCity(req);
  var userDetialsCookie = req.universalCookies.get('_user_details') || {};
  let isArabicAllowed = showLangNav(city);
  let allowedLang = getAllowedLang(city);
  if (!userDetialsCookie.access_token) {
    if (isArabicAllowed == true) {
      res.redirect('/' + lang + '/' + city);
    } else {
      res.redirect(isArabicAllowed);
    }
  } else if (!(allowedLang.indexOf(req.params.lang) != -1)) {
    redirectDefaultLangPage(req, res);
  } else {
    const context = {};
    req.params.city = getDefaultCity(req);
    let { city, lang } = req.params;
    const store = createStore();// Data Constants

    let dipatchCall = commonDispatch(store, req);
    const bodyClass = store.dispatch(setBodyClass('users'));
    dipatchCall.push(bodyClass);
    const extraData = ['whySMData', 'howIWData', 'logosData', 'footerItemsData'];
    const dataPromise = store.dispatch(fetchLandingData(req.params.city, req.params.slug, extraData, lang));
    dipatchCall.push(dataPromise);

    Promise.all(dipatchCall).then(() => {
      const reduxState = store.getState();
      res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
      res.end(htmlTemplate(renderLayout(store, context, req), bundleJSFileName, 1050, reduxState));
    }).catch(function (error) {
      // handle error
      // console.log("Error Fetch", error);
    });
  }
});
// About Page SSR
app.get("/:lang/about-us", (req, res) => {
  var city = getDefaultCity(req);
  let allowedLang = getAllowedLang(city);
  if (!(allowedLang.indexOf(req.params.lang) != -1)) {
    res.redirect('/en/about-us');
  } else {
    const context = {};
    req.params.city = city;
    let lang = req.params.lang;
    const store = createStore();
    const bodyClass = store.dispatch(setBodyClass('default'));
    let dipatchCall = commonDispatch(store, req);
    dipatchCall.push(bodyClass);
    const pageData = store.dispatch(fetchAboutData(lang));
    dipatchCall.push(pageData);
    Promise.all(dipatchCall).then(() => {
      const reduxState = store.getState();
      res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
      res.end(htmlTemplate(renderLayout(store, context, req), bundleJSFileName, 1050, reduxState, { title: reduxState.aboutData.acf.seo_meta_title, desc: reduxState.aboutData.acf.seo_meta_description }));
    }).catch(function (error) {
      // handle error
      // console.log("Error Fetch", error);
    });
  }
});
// terms Page SSR
app.get("/:lang/terms-and-conditions", (req, res) => {
  var city = getDefaultCity(req);
  let allowedLang = getAllowedLang(city);

  if (!(allowedLang.indexOf(req.params.lang) != -1)) {
    redirectDefaultLangPage(req, res);
  } else {
    const context = {};
    req.params.city = city;
    let lang = req.params.lang;
    const store = createStore();
    const bodyClass = store.dispatch(setBodyClass('default'));
    let dipatchCall = commonDispatch(store, req);
    dipatchCall.push(bodyClass);
    const pageData = store.dispatch(fetchTermsData(lang));
    dipatchCall.push(pageData);
    Promise.all(dipatchCall).then(() => {
      const reduxState = store.getState();
      res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
      res.end(htmlTemplate(renderLayout(store, context, req), bundleJSFileName, 1050, reduxState, { title: reduxState.termsData.acf.seo_meta_title, desc: reduxState.termsData.acf.seo_meta_description }));
    }).catch(function (error) {
      // handle error
      // console.log("Error Fetch", error);
    });
  }
});
// careers Page SSR
app.get("/:lang/careers", (req, res) => {
  var city = getDefaultCity(req);
  let allowedLang = getAllowedLang(city);

  if (!(allowedLang.indexOf(req.params.lang) != -1)) {
    redirectDefaultLangPage(req, res);
  } else {
    const context = {};
    var city = getDefaultCity(req);
    req.params.city = city;
    let lang = req.params.lang;
    var seoText = commonHelper.getSeoCareersTextArray();
    const store = createStore();
    const bodyClass = store.dispatch(setBodyClass('default'));
    let dipatchCall = commonDispatch(store, req);
    dipatchCall.push(bodyClass);
    const pageData = store.dispatch(fetchTeamData(lang));
    dipatchCall.push(pageData);
    Promise.all(dipatchCall).then(() => {
      const reduxState = store.getState();
      res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
      res.end(htmlTemplate(renderLayout(store, context, req), bundleJSFileName, 1050, reduxState, { title: seoText.seoTitle, desc: seoText.seoDesc }));
    }).catch(function (error) {
      // handle error
      // console.log("Error Fetch", error);
    });
  }
});
// privacy Page SSR
app.get("/:lang/privacy-policy", (req, res) => {
  var city = getDefaultCity(req);
  let allowedLang = getAllowedLang(city);
  if (!(allowedLang.indexOf(req.params.lang) != -1)) {
    redirectDefaultLangPage(req, res);
  } else {
    const context = {};
    req.params.city = city;
    let lang = req.params.lang;
    const store = createStore();
    const bodyClass = store.dispatch(setBodyClass('default'));
    let dipatchCall = commonDispatch(store, req);
    dipatchCall.push(bodyClass);
    const pageData = store.dispatch(fetchPrivacyData(lang));
    dipatchCall.push(pageData);
    Promise.all(dipatchCall).then(() => {
      const reduxState = store.getState();
      res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
      res.end(htmlTemplate(renderLayout(store, context, req), bundleJSFileName, 1050, reduxState, { title: reduxState.privacyData.acf.seo_meta_title, desc: reduxState.privacyData.acf.seo_meta_description }));
    }).catch(function (error) {
      // handle error
      // console.log("Error Fetch", error);
    });
  }
});
// disclaimer Page SSR
app.get("/:lang/disclaimer", (req, res) => {
  var city = getDefaultCity(req);
  let allowedLang = getAllowedLang(city);
  if (!(allowedLang.indexOf(req.params.lang) != -1)) {
    redirectDefaultLangPage(req, res);
  } else {
    const context = {};
    req.params.city = city;
    let lang = req.params.lang;
    const store = createStore();
    const bodyClass = store.dispatch(setBodyClass('default'));
    let dipatchCall = commonDispatch(store, req);
    dipatchCall.push(bodyClass);
    const pageData = store.dispatch(fetchDisclaimerData(lang));
    dipatchCall.push(pageData);
    Promise.all(dipatchCall).then(() => {
      const reduxState = store.getState();
      res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
      res.end(htmlTemplate(renderLayout(store, context, req), bundleJSFileName, 1050, reduxState, { title: reduxState.disclaimerData.acf.seo_meta_title, desc: reduxState.disclaimerData.acf.seo_meta_description }));
    }).catch(function (error) {
      // handle error
      // console.log("Error Fetch", error);
    });
  }
});
// contact Page SSR
app.get("/:lang/contact-us", (req, res) => {
  var city = getDefaultCity(req);
  let allowedLang = getAllowedLang(city);
  if (!(allowedLang.indexOf(req.params.lang) != -1)) {
    redirectDefaultLangPage(req, res);
  } else {
    const context = {};
    var seoText = commonHelper.getSeoContactUsTextArray();
    req.params.city = city;
    let lang = req.params.lang;
    const store = createStore();
    const bodyClass = store.dispatch(setBodyClass('default'));
    let dipatchCall = commonDispatch(store, req);
    dipatchCall.push(bodyClass);
    const pageData = store.dispatch(fetchAboutData(lang));
    dipatchCall.push(pageData);
    Promise.all(dipatchCall).then(() => {
      const reduxState = store.getState();
      res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
      res.end(htmlTemplate(renderLayout(store, context, req), bundleJSFileName, 1050, reduxState, { title: seoText.seoTitle, desc: seoText.seoDesc }));
    }).catch(function (error) {
      // handle error
      // console.log("Error Fetch", error);
    });
  }
});
// payment Page SSR
app.get("/:lang/payment-and-refund-policy", (req, res) => {
  var city = getDefaultCity(req);
  let allowedLang = getAllowedLang(city);
  if (!(allowedLang.indexOf(req.params.lang) != -1)) {
    redirectDefaultLangPage(req, res);
  } else {
    var seoText = commonHelper.getSeoContactUsTextArray();
    const context = {};
    req.params.city = city;
    let lang = req.params.lang;
    const store = createStore();
    const bodyClass = store.dispatch(setBodyClass('default'));
    let dipatchCall = commonDispatch(store, req);
    dipatchCall.push(bodyClass);
    const pageData = store.dispatch(fetchPRPData(lang));
    dipatchCall.push(pageData);
    Promise.all(dipatchCall).then(() => {
      const reduxState = store.getState();
      res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
      res.end(htmlTemplate(renderLayout(store, context, req), bundleJSFileName, 1050, reduxState, { title: reduxState.prpData.acf.seo_meta_title, desc: reduxState.prpData.acf.seo_meta_description }));
    }).catch(function (error) {
      // handle error
      // console.log("Error Fetch", error);
    });
  }
});

// Become a Partner Page SSR
app.get("/:lang/become-a-partner", (req, res) => {
  var city = getDefaultCity(req);
  let allowedLang = getAllowedLang(city);
  if (!(allowedLang.indexOf(req.params.lang) != -1)) {
    redirectDefaultLangPage(req, res);
  } else {
    const context = {};
    req.params.city = city;
    let lang = req.params.lang;
    const store = createStore();
    const bodyClass = store.dispatch(setBodyClass('default'));
    let dipatchCall = commonDispatch(store, req);
    dipatchCall.push(bodyClass);
    const pageData = store.dispatch(fetchBecomePartnerData(lang));
    dipatchCall.push(pageData);
    Promise.all(dipatchCall).then(() => {
      const reduxState = store.getState();
      res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
      res.end(htmlTemplate(renderLayout(store, context, req), bundleJSFileName, 1050, reduxState, { title: reduxState.bePartnerData.acf.seo_meta_title, desc: reduxState.bePartnerData.acf.seo_meta_description }));
    }).catch(function (error) {
      // handle error
      // console.log("Error Fetch", error);
    });
  }
});

// Faqs Page SSR
app.get("/:lang/faqs", (req, res) => {
  var city = getDefaultCity(req);
  let allowedLang = getAllowedLang(city);
  if (!(allowedLang.indexOf(req.params.lang) != -1)) {
    redirectDefaultLangPage(req, res);
  } else {
    const context = {};
    req.params.city = city;
    let lang = req.params.lang;
    const store = createStore();
    const bodyClass = store.dispatch(setBodyClass('default'));
    let dipatchCall = commonDispatch(store, req);
    dipatchCall.push(bodyClass);
    const pageData = store.dispatch(fetchFaqsData(lang));
    dipatchCall.push(pageData);
    Promise.all(dipatchCall).then(() => {
      const reduxState = store.getState();
      res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
      res.end(htmlTemplate(renderLayout(store, context, req), bundleJSFileName, 1050, reduxState, { title: reduxState.faqsData.acf.seo_meta_title, desc: reduxState.faqsData.acf.seo_meta_description }));
    }).catch(function (error) {
      // handle error
      // console.log("Error Fetch", error);
    });
  }
});
// Write a Review Page SSR
app.get("/:lang/write-a-review/:url", (req, res) => {
  var city = getDefaultCity(req);
  let allowedLang = getAllowedLang(city);
  if (!(allowedLang.indexOf(req.params.lang) != -1)) {
    redirectDefaultLangPage(req, res);
  } else {
    const context = {};
    req.params.city = city;
    let lang = req.params.lang;
    const store = createStore();
    const bodyClass = store.dispatch(setBodyClass('default'));
    let dipatchCall = commonDispatch(store, req);
    dipatchCall.push(bodyClass);
    const pageData = store.dispatch(fetchReviewData(lang));
    dipatchCall.push(pageData);
    Promise.all(dipatchCall).then(() => {
      const reduxState = store.getState();
      res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
      res.end(htmlTemplate(renderLayout(store, context, req), bundleJSFileName, 1050, reduxState, { title: reduxState.reviewData.acf.seo_meta_title, desc: reduxState.reviewData.acf.seo_meta_description }));
    }).catch(function (error) {
      // handle error
      // console.log("Error Fetch", error);
    });
  }
});
// Write a Review Page SSR
app.get("/:lang/write-a-review", (req, res) => {
  var city = getDefaultCity(req);
  let allowedLang = getAllowedLang(city);
  if (!(allowedLang.indexOf(req.params.lang) != -1)) {
    redirectDefaultLangPage(req, res);
  } else {
    const context = {};
    req.params.city = city;
    let lang = req.params.lang;
    const store = createStore();
    const bodyClass = store.dispatch(setBodyClass('default'));
    let dipatchCall = commonDispatch(store, req);
    dipatchCall.push(bodyClass);
    const pageData = store.dispatch(fetchReviewData(lang));
    dipatchCall.push(pageData);
    Promise.all(dipatchCall).then(() => {
      const reduxState = store.getState();
      res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
      res.end(htmlTemplate(renderLayout(store, context, req), bundleJSFileName, 1050, reduxState, { title: reduxState.reviewData.acf.seo_meta_title, desc: reduxState.reviewData.acf.seo_meta_description }));
    }).catch(function (error) {
      // handle error
      // console.log("Error Fetch", error);
    });
  }
});

// for Zoho Accept leads
app.get("/book-direct/zohosm", (req, res) => {
  const context = {};
  const store = createStore();
  // console.log("calling here")
  res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
  res.end(zohoTemplate(renderZohoLayout(store, context, req), bundleJSFileName));
});

// Homepage SSR
app.get("/:lang/:city", (req, res) => {
  var city = req.params.city;
  let allowedLang = getAllowedLang(city);
  if (!(allowedLang.indexOf(req.params.lang) != -1)) {
    redirectDefaultLangPage(req, res);
  } else if (!(allowedCities.indexOf(req.params.city) != -1) && !(allowedPages.indexOf(req.params.city) != -1)) {
    res.redirect('/' + req.params.lang + '/dubai');
  } else {
    if (allowedPages.indexOf(req.params.city) != -1) {
      req.params.city = getDefaultCity(req);
    }
    const context = {};
    var lang = req.params.lang;
    const store = createStore();
    if (typeof req.headers['user-agent'] != "undefined") {
      store.dispatch(isBotData(req.headers['user-agent'].indexOf('bot') != -1));
    }
    let dipatchCall = commonDispatch(store, req);
    const bodyClass = store.dispatch(setBodyClass('home'));
    dipatchCall.push(bodyClass);
    const extraData = ['whySMData', 'howIWData', 'logosData', 'footerItemsData'];
    const dataPromise = store.dispatch(fetchHomepageData(city, extraData, lang));
    dipatchCall.push(dataPromise);
    let cityCode = typeof cityCodes[city] != "undefined" ? cityCodes[city] : 'DXB';
    const fetchReviews = store.dispatch(fetchAllReviews(cityCode, '', lang));
    dipatchCall.push(fetchReviews);
    Promise.all(dipatchCall).then(() => {
      const reduxState = store.getState();
      res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
      var seoText = commonHelper.getSeoHomePageTextArray(lang);
      res.end(htmlTemplate(renderLayout(store, context, req), bundleJSFileName, 1050, reduxState, { title: commonHelper.replaceCurrentCityName(seoText.seoTitle, reduxState.currentCityData), desc: commonHelper.replaceCurrentCityName(seoText.seoDesc, reduxState.currentCityData) }, false, req.protocol + '://' + req.get('host') + req.originalUrl));
    }).catch(function (error) {
      console.log("Error /:lang/:city", error);
    });
  }
});
function commonDispatch(store, req) {
  var city = req.params.city;
  var lang = req.params.lang;
  var access_token = getUserToken(req);
  //const store = createStore();
  const services = store.dispatch(fetchAllServices(lang));
  /*const dataConstants = store.dispatch(fetchDataDictionaryValues(lang));
  const newDataConstants = store.dispatch(fetchDataConstantValues(lang));
  const DateTimeAvailability = store.dispatch(fetchDateTimeAvailability(access_token,lang));*/
  const currentCity = store.dispatch(setCurrentCity(city));
  const currentLang = store.dispatch(setCurrentLang(lang));
  const citiesPromise = store.dispatch(fetchAllCities(city, lang));
  const servicesPromise = store.dispatch(fetchServices(city, lang));
  var userDetialsCookie = req.universalCookies.get('_user_details') || {};
  const userDetails = store.dispatch(setSignIn(userDetialsCookie));
  const userProfile = store.dispatch(fetchUserProfile(access_token, lang));
  //const popupData = store.dispatch(fetchPopupData(lang));
  const currentDateTime = store.dispatch(fetchDateTime());
  const footerItemsData = store.dispatch(fetchFooterItemsData(lang));
  const fetchAllApiServicesPromise = fetchAllApiServicesPromiseCall(store, city, lang);

  return [services,
    /*dataConstants, 
    newDataConstants, 
    DateTimeAvailability,*/
    servicesPromise,
    currentLang,
    currentCity,
    citiesPromise,
    userDetails,
    userProfile,
    //popupData,
    currentDateTime,
    footerItemsData,
    fetchAllApiServicesPromise];
}
// Partners list SSR
app.get("/:lang/:city/partners-list", (req, res) => {
  var city = req.params.city;
  let allowedLang = getAllowedLang(city);
  if (!(allowedLang.indexOf(req.params.lang) != -1)) {
    redirectDefaultLangPage(req, res);
  } else if (!(allowedCities.indexOf(req.params.city) != -1)) {
    res.redirect('/' + req.params.lang + '/dubai');
  } else {
    var seoText = commonHelper.getSeoPartnersTextArray();
    const context = {};
    var lang = req.params.lang;
    const store = createStore();
    let dipatchCall = commonDispatch(store, req);
    const bodyClass = store.dispatch(setBodyClass('home'));
    dipatchCall.push(bodyClass);
    const extraData = ['whySMData', 'howIWData', 'logosData', 'footerItemsData'];
    const dataPromise = store.dispatch(fetchLandingData(req.params.city, req.params.slug, extraData, lang));
    dipatchCall.push(dataPromise);
    const companiesPageData = store.dispatch(fetchCompaniesPageData(lang));
    dipatchCall.push(companiesPageData);

    Promise.all(dipatchCall).then(() => {

      //Promise.all([companiesPageData, dataPromise, citiesPromise, servicesPromise, bodyClass, currentCity, userDetails, userProfile,  services, dataConstants, newDataConstants, DateTimeAvailability, currentDateTime, fetchAllApiServicesPromise]).then(() => {
      const reduxState = store.getState();
      res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
      res.end(htmlTemplate(renderLayout(store, context, req), bundleJSFileName, 1050, reduxState, { title: commonHelper.replaceCurrentCityName(seoText.seoTitle, reduxState.currentCityData), desc: commonHelper.replaceCurrentCityName(seoText.seoDesc, reduxState.currentCityData) }));
    }).catch(function (error) {

      // handle error
      // console.log("Error Fetch", error);
    });
  }
});


// Service Landing SSR
app.get("/:lang/:city/:slug", (req, res) => {
  var city = req.params.city;
  var slug = req.params.slug;
  let allowedLang = getAllowedLang(city);
  if (!(allowedLang.indexOf(req.params.lang) != -1)) {
    redirectDefaultLangPage(req, res);
  } else if (!(allowedCities.indexOf(req.params.city) != -1)) {
    res.redirect('/' + req.params.lang + '/dubai');
  } else if (slug === 'carpentry') {
    res.redirect('/' + req.params.lang + '/' + city + '/carpenter');
  } else {
    let startRequestTime = new Date().getTime()
    //console.log("Request Start:"+startRequestTime);
    let QueryParams = {};
    if (typeof req.query.h1 != "undefined" || typeof req.query.h2 != "undefined" || typeof req.query.promo != "undefined") {
      //QueryParams.push()
      QueryParams = {
        h1: typeof req.query.h1 != "undefined" ? req.query.h1 : "",
        h2: typeof req.query.h2 != "undefined" ? req.query.h2 : "",
        promo: typeof req.query.promo != "undefined" ? req.query.promo : ""
      };
    }

    const context = {};
    var lang = req.params.lang;
    const store = createStore();
    let dipatchCall = commonDispatch(store, req);
    const bodyClass = store.dispatch(setBodyClass('home'));
    dipatchCall.push(bodyClass);
    const extraData = ['whySMData', 'howIWData', 'logosData', 'footerItemsData'];
    const dataPromise = store.dispatch(fetchLandingData(req.params.city, req.params.slug, extraData, lang));
    dipatchCall.push(dataPromise);
    const QueryParamsCall = store.dispatch(setPageParams(QueryParams));
    dipatchCall.push(QueryParamsCall);

    let cityCode = typeof cityCodes[city] != "undefined" ? cityCodes[city] : 'DXB';

    //this.props.serviceID != '' ? this.props.service_constants[this.props.serviceID] : this.props.serviceID,

    Promise.all(dipatchCall).then(() => {
      /*let endRequestTime = new Date().getTime();
      console.log("Request END: "+ endRequestTime);
      console.log("( Start Time - End Time ): "+ ( endRequestTime - startRequestTime ));*/
      /*let  endRenderRequestTime = new Date().getTime();
      console.log("After Page render : "+endRenderRequestTime);
      console.log(" (Render Start - Render End) : "+(endRenderRequestTime - endRequestTime))*/
      const reduxState = store.getState();
      let serviceConstants = reduxState.serviceConstants;
      let landingPageData = (typeof reduxState.landingData != "undefined" && reduxState.landingData.length) ? reduxState.landingData : [];
      if (landingPageData.length) {
        landingPageData = landingPageData[0];
        let serviceID = typeof serviceConstants[landingPageData.acf.id] != "undefined" ? serviceConstants[landingPageData.acf.id] : '';
        const fetchReviews = store.dispatch(fetchAllReviews(cityCode, serviceID, lang));
        Promise.all([fetchReviews]).then((reviews) => {
          //console.log("reviews", reviews[0].length);
          const newReduxState = store.getState();
          res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
          res.end(htmlTemplate(renderLayout(store, context, req), bundleJSFileName, 1050, newReduxState, { title: reduxState.landingData[0].acf.meta_title, desc: reduxState.landingData[0].acf.meta_description }, true, req.protocol + '://' + req.get('host') + req.originalUrl));
        })
      } else {
        res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
        res.end(htmlTemplate(renderLayout(store, context, req), bundleJSFileName, 1050, reduxState, { title: reduxState.landingData[0].acf.meta_title, desc: reduxState.landingData[0].acf.meta_description }, true, req.protocol + '://' + req.get('host') + req.originalUrl));
      }
    }).catch(function (error) {
      // handle error
      // console.log("Error Fetch", error);
    });
  }
});

// Service reviews
app.get("/:lang/:city/:slug/reviews", (req, res) => {
  var city = req.params.city;
  var lang = req.params.lang;
  if (req.params.lang == LANG_AR) {
    res.redirect('/en/' + req.params.city + '/' + req.params.slug + '/' + 'reviews');
  }
  let allowedLang = getAllowedLang(city);
  if (!(allowedLang.indexOf(req.params.lang) != -1)) {
    redirectDefaultLangPage(req, res);
  } else if (!(allowedCities.indexOf(req.params.city) != -1)) {
    res.redirect('/' + req.params.lang + '/dubai');
  } else {
    const context = {};
    const store = createStore();
    let dipatchCall = commonDispatch(store, req);
    const bodyClass = store.dispatch(setBodyClass('landing'));
    dipatchCall.push(bodyClass);
    const extraData = ['whySMData', 'howIWData', 'logosData', 'footerItemsData'];
    const dataPromise = store.dispatch(fetchLandingData(req.params.city, req.params.slug, extraData, lang));
    dipatchCall.push(dataPromise);
    Promise.all(dipatchCall).then(() => {
      const reduxState = store.getState();
      res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
      var serviceID = commonHelper.getServiceIDBySlug(URLCONSTANT, reduxState.serviceConstants, req.params.slug);
      var seoText = commonHelper.getSeoJourney2TextArray(serviceID, '', reduxState.serviceConstants);
      var seoTitle = Object.keys(seoText).length ? commonHelper.replaceCurrentCityName(seoText.seoTitle, reduxState.currentCityData) : "ServiceMarket";
      var seoDesc = Object.keys(seoText).length ? commonHelper.replaceCurrentCityName(seoText.seoDescription, reduxState.currentCityData) : "ServiceMarket";
      res.end(htmlTemplate(renderLayout(store, context, req), bundleJSFileName, 1050, reduxState, { title: seoTitle, desc: seoDesc }));
    }).catch(function (error) {
      // handle error
      // console.log("Error Fetch", error);
    });
  }
});

app.get("/:lang/:city/:slug/book-online/", (req, res) => {
  res.redirect(`/${req.params.lang}/${req.params.city}/${req.params.slug}/book-online`);
});
// Booking Page SSR
app.get("/:lang/:city/:slug/book-online", (req, res) => {
  let allowedLang = getAllowedLang(city);
  const context = {};
  var city = req.params.city;
  var lang = req.params.lang;
  if (!(allowedLang.indexOf(lang) != -1)) {
    redirectDefaultLangPage(req, res);
  }
  const store = createStore();
  let dipatchCall = commonDispatch(store, req);
  const bodyClass = store.dispatch(setBodyClass('book-online'));
  dipatchCall.push(bodyClass);
  const extraData = ['whySMData', 'howIWData', 'logosData', 'footerItemsData'];
  const dataPromise = store.dispatch(fetchLandingData(req.params.city, req.params.slug, extraData, lang));
  dipatchCall.push(dataPromise);

  Promise.all(dipatchCall).then(() => {
    const reduxState = store.getState();
    res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
    res.end(htmlTemplate(renderLayout(store, context, req), bundleJSFileName, 1050, reduxState, { title: 'ServiceMarket', desc: 'ServiceMarket' }, false, '', true));
  }).catch(function (error) {
    // handle error
    // console.log("Error Fetch", error);
  });
});

app.get("/:lang/:city/:slug/:subservice/book-online", (req, res) => {
  let allowedLang = getAllowedLang(city);
  const context = {};
  var city = req.params.city;
  var lang = req.params.lang;
  const store = createStore();
  let dipatchCall = commonDispatch(store, req);
  const bodyClass = store.dispatch(setBodyClass('book-online'));
  dipatchCall.push(bodyClass);
  const extraData = ['whySMData', 'howIWData', 'logosData', 'footerItemsData'];
  const dataPromise = store.dispatch(fetchLandingData(req.params.city, req.params.slug, extraData, lang));
  dipatchCall.push(dataPromise);

  Promise.all(dipatchCall).then(() => {
    const reduxState = store.getState();
    res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
    res.end(htmlTemplate(renderLayout(store, context, req), bundleJSFileName, 1050, reduxState, { title: 'ServiceMarket', desc: 'ServiceMarket' }, false, '', true));
  }).catch(function (error) {
    // handle error
    // console.log("Error Fetch", error);
  });
});

// Booking Subscription
app.get("/:lang/:city/:slug/book-online/:subscription", (req, res) => {
  var { city, lang } = req.params;
  let allowedLang = getAllowedLang(city);
  const context = {};
  const store = createStore();
  let dipatchCall = commonDispatch(store, req);
  const bodyClass = store.dispatch(setBodyClass('book-online'));
  dipatchCall.push(bodyClass);
  const extraData = ['whySMData', 'howIWData', 'logosData', 'footerItemsData'];
  const dataPromise = store.dispatch(fetchLandingData(req.params.city, req.params.slug, extraData, lang));
  dipatchCall.push(dataPromise);

  Promise.all(dipatchCall).then(() => {
    const reduxState = store.getState();
    res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
    res.end(htmlTemplate(renderLayout(store, context, req), bundleJSFileName, 1050, reduxState, { title: 'ServiceMarket', desc: 'ServiceMarket' }, false, '', true));
  }).catch(function (error) {
    // handle error
    // console.log("Error Fetch", error);
  });
});

// Quotes Page SSR
app.get("/:lang/:city/:slug/journey1", (req, res) => {
  const context = {};
  var { city, lang } = req.params;
  let allowedLang = getAllowedLang(city);
  if (!(allowedLang.indexOf(req.params.lang) != -1)) {
    redirectDefaultLangPage(req, res);
  }
  const store = createStore();
  let dipatchCall = commonDispatch(store, req);
  const bodyClass = store.dispatch(setBodyClass('journey'));
  dipatchCall.push(bodyClass);
  const extraData = ['whySMData', 'howIWData', 'logosData', 'footerItemsData'];
  const dataPromise = store.dispatch(fetchLandingData(req.params.city, req.params.slug, extraData, lang));
  dipatchCall.push(dataPromise);

  Promise.all(dipatchCall).then(() => {
    const reduxState = store.getState();
    res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
    res.end(htmlTemplate(renderLayout(store, context, req), bundleJSFileName, 1050, reduxState, { title: 'ServiceMarket', desc: 'ServiceMarket' }, true, '', true));
  }).catch(function (error) {
    // handle error
    // console.log("Error Fetch", error);
  });
});
app.get("/:lang/:city/:slug/:journey/step2", (req, res) => {
  var { city, lang } = req.params;
  let allowedLang = getAllowedLang(city);
  const context = {};
  const store = createStore();
  let dipatchCall = commonDispatch(store, req);
  const bodyClass = store.dispatch(setBodyClass('journey'));
  dipatchCall.push(bodyClass);
  const extraData = ['whySMData', 'howIWData', 'logosData', 'footerItemsData'];
  const dataPromise = store.dispatch(fetchLandingData(req.params.city, req.params.slug, extraData, lang));
  dipatchCall.push(dataPromise);

  Promise.all(dipatchCall).then(() => {
    const reduxState = store.getState();
    res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
    res.end(htmlTemplate(renderLayout(store, context, req), bundleJSFileName, 1050, reduxState, { title: 'ServiceMarket', desc: 'ServiceMarket' }, true, '', true));
  }).catch(function (error) {
    // handle error
    // console.log("Error Fetch", error);
  });
});
///en/dubai/pest-control-companies/book-online/confirmation/20190213000010BBDXB

app.get("/:lang/:city/:slug/book-online/confirmation/:bookingID", (req, res) => {
  var { city, lang } = req.params;
  let allowedLang = getAllowedLang(city);
  if (!(allowedLang.indexOf(req.params.lang) != -1)) {
    redirectDefaultLangPage(req, res);
  } else if (!(allowedCities.indexOf(req.params.city) != -1)) {
    res.redirect('/' + req.params.lang + '/dubai');
  } else {

    //var cookieName = getCookieName(req.params, "confirmation_summary");

    var confirmationSummary = getCookieValue(req, "confirmation_summary");

    /*console.log(confirmationSummary, "confirmationSummary");
    res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
    res.end();
    return false;*/

    /* var confirmationSummary = req.universalCookies.get(cookieName) || {};*/

    if (Object.keys(confirmationSummary).length == 0) {
      res.redirect('/' + req.params.lang + '/' + req.params.city);
    }
    const bookingID = req.params.bookingID;
    const context = {};
    const store = createStore();
    let dipatchCall = commonDispatch(store, req);
    const bodyClass = store.dispatch(setBodyClass('confirmation'));
    dipatchCall.push(bodyClass);
    const extraData = ['whySMData', 'howIWData', 'logosData', 'footerItemsData'];
    const dataPromise = store.dispatch(fetchLandingConfirmationData(req.params.city, req.params.slug, extraData, lang));
    dipatchCall.push(dataPromise);

    Promise.all(dipatchCall).then(() => {
      removeLocalStorage(req); // delete local storage value
      const reduxState = store.getState();
      res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
      res.end(htmlTemplate(renderLayout(store, context, req), bundleJSFileName, 1050, reduxState, { title: 'Thank you for using ServiceMarket!', desc: 'Thank you for using ServiceMarket!' }, false, '', true));
    }).catch(function (error) {
      // handle error
      // console.log("Error Fetch", error);
    });
  }
});
// Quotes Page SSR
app.get("/:lang/:city/:slug/:journey/confirmation/:rid", (req, res) => {
  var { city, lang } = req.params;
  let allowedLang = getAllowedLang(city);
  const context = {};

  const store = createStore();
  // Data Constants
  var confirmationRid = req.universalCookies.get("rid") || false;
  //console.log("confirmationRid", confirmationRid);
  if (!confirmationRid) {
    res.redirect('/' + req.params.lang + '/' + req.params.city);
  }
  let dipatchCall = commonDispatch(store, req);
  const bodyClass = store.dispatch(setBodyClass('confirmation'));
  dipatchCall.push(bodyClass);
  const extraData = ['whySMData', 'howIWData', 'logosData', 'footerItemsData'];
  const dataPromise = store.dispatch(fetchLandingConfirmationData(req.params.city, req.params.slug, extraData, lang));
  dipatchCall.push(dataPromise);

  Promise.all(dipatchCall).then(() => {
    const reduxState = store.getState();
    res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
    res.end(htmlTemplate(renderLayout(store, context, req), bundleJSFileName, 1050, reduxState, { title: 'Thank you for using ServiceMarket!', desc: 'Thank you for using ServiceMarket!' }, false, '', true));
  }).catch(function (error) {
    // handle error
    // console.log("Error Fetch", error);
  });
});



// register card Landing success
app.get("/:lang/:city/profile/success/:verifyToken", (req, res) => {
  var { city, lang } = req.params;
  let allowedLang = getAllowedLang(city);
  if (!(allowedLang.indexOf(req.params.lang) != -1)) {
    redirectDefaultLangPage(req, res);
  } else if (!(allowedCities.indexOf(req.params.city) != -1)) {
    res.redirect('/' + req.params.lang + '/dubai');
  } else {
    const verifyToken = req.params.verifyToken;
    // console.log(verifyToken);
    const context = {};
    var { lang, city } = req.params;
    const store = createStore();
    var access_token = getUserToken(req);
    let dipatchCall = commonDispatch(store, req);
    const bodyClass = store.dispatch(setBodyClass('landing'));
    dipatchCall.push(bodyClass);
    const extraData = ['whySMData', 'howIWData', 'logosData', 'footerItemsData'];
    const dataPromise = store.dispatch(fetchLandingData(req.params.city, req.params.slug, extraData, lang));
    dipatchCall.push(dataPromise);
    const submitCreditCard = submitVerifyCreditCard(verifyToken, access_token);

    dipatchCall.push(submitCreditCard);

    Promise.all(dipatchCall).then(() => {
      res.redirect('/en/profile#creditcard');
    }).catch(function (error) {
      // handle error
      // console.log("Error Fetch", error);
    });
  }
});

// register card Landing decline
app.get("/:lang/:city/profile/decline/:verifyToken", (req, res) => {
  var { city, lang } = req.params;
  let allowedLang = getAllowedLang(city);
  if (!(allowedLang.indexOf(req.params.lang) != -1)) {
    redirectDefaultLangPage(req, res);
  } else if (!(allowedCities.indexOf(req.params.city) != -1)) {
    res.redirect('/' + req.params.lang + '/dubai');
  } else {
    res.redirect('/' + req.params.lang + '/' + req.params.city);
  }
});

// register card Landing cancel
app.get("/:lang/:city/profile/cancel/:verifyToken", (req, res) => {
  var { city, lang } = req.params;
  let allowedLang = getAllowedLang(city);
  if (!(allowedLang.indexOf(req.params.lang) != -1)) {
    redirectDefaultLangPage(req, res);
  } else if (!(allowedCities.indexOf(req.params.city) != -1)) {
    res.redirect('/' + req.params.lang + '/dubai');
  } else {
    res.redirect('/' + req.params.lang + '/' + req.params.city);
  }
});
app.get("/:lang/:city/:slug/:subservice/book-online/confirmation/:bookingID", (req, res) => {
  var { city, lang } = req.params;
  let allowedLang = getAllowedLang(city);
  if (!(allowedLang.indexOf(req.params.lang) != -1)) {
    redirectDefaultLangPage(req, res);
  } else if (!(allowedCities.indexOf(req.params.city) != -1)) {
    res.redirect('/' + req.params.lang + '/dubai');
  } else {

    var cookieName = getCookieName(req.params, "confirmation_summary");

    var confirmationSummary = req.universalCookies.get(cookieName) || {};

    // console.log("confirmationSummary", confirmationSummary);

    if (Object.keys(confirmationSummary).length == 0) {
      res.redirect('/' + req.params.lang + '/' + req.params.city);
    }

    const bookingID = req.params.bookingID;
    const context = {};
    const store = createStore();
    let dipatchCall = commonDispatch(store, req);
    const bodyClass = store.dispatch(setBodyClass('confirmation'));
    dipatchCall.push(bodyClass);
    const extraData = ['whySMData', 'howIWData', 'logosData', 'footerItemsData'];
    const dataPromise = store.dispatch(fetchLandingConfirmationData(req.params.city, req.params.slug, extraData, lang));
    dipatchCall.push(dataPromise);

    Promise.all(dipatchCall).then(() => {
      const reduxState = store.getState();
      res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
      res.end(htmlTemplate(renderLayout(store, context, req), bundleJSFileName, 1050, reduxState, { title: 'Thank you for using ServiceMarket!', desc: 'Thank you for using ServiceMarket!' }, false, '', true));
    }).catch(function (error) {
      // handle error
      // console.log("Error Fetch", error);
    });
  }
});
app.get("/:lang/:city/:slug/book-online/:subscription/confirmation/:bookingID", (req, res) => {
  var { city, lang } = req.params;
  let allowedLang = getAllowedLang(city);
  if (!(allowedLang.indexOf(req.params.lang) != -1)) {
    redirectDefaultLangPage(req, res);
  } else if (!(allowedCities.indexOf(req.params.city) != -1)) {
    res.redirect('/' + req.params.lang + '/dubai');
  } else {

    var cookieName = getCookieName(req.params, "confirmation_summary");

    var confirmationSummary = req.universalCookies.get(cookieName) || {};

    // console.log("confirmationSummary", confirmationSummary);

    if (Object.keys(confirmationSummary).length == 0) {
      res.redirect('/' + req.params.lang + '/' + req.params.city);
    }

    const bookingID = req.params.bookingID;
    const context = {};
    const store = createStore();
    let dipatchCall = commonDispatch(store, req);
    const bodyClass = store.dispatch(setBodyClass('confirmation'));
    dipatchCall.push(bodyClass);
    const extraData = ['whySMData', 'howIWData', 'logosData', 'footerItemsData'];
    const dataPromise = store.dispatch(fetchLandingConfirmationData(req.params.city, req.params.slug, extraData, lang));
    dipatchCall.push(dataPromise);

    Promise.all(dipatchCall).then(() => {
      const reduxState = store.getState();
      res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
      res.end(htmlTemplate(renderLayout(store, context, req), bundleJSFileName, 1050, reduxState, { title: 'Thank you for using ServiceMarket!', desc: 'Thank you for using ServiceMarket!' }, false, '', true));
    }).catch(function (error) {
      // handle error
      // console.log("Error Fetch", error);
    });
  }
});
//handle Teller response
app.get("/:lang/handle-teller-response/:status/:bookingId/:requestUuid/:city/:service/:verifyToken", (req, res) => {
  const context = {};
  const store = createStore();
  const status = req.params.status;
  const bookingID = req.params.bookingId;
  const verifyToken = req.params.verifyToken;
  const requestUuid = req.params.requestUuid;
  const serviceURL = req.params.service;
  const city = req.params.city;
  const lang = req.params.lang;
  var redirectLocation = "";
  var home_url = '/' + req.params.lang + '/' + city;

  var requestParams = {
    lang: lang,
    city: city,
    slug: serviceURL
  };

  //var cookieName = params.lang + "." + params.city + "." + params.slug + "." + name;

  var confirmationCookieName = getCookieName(requestParams, "confirmation_summary");

  var couponDataCookieName = getCookieName(requestParams, "coupon_data");

  var submittedDataCookieName = getCookieName(requestParams, "submitted_data");

  var bookingDataCookieName = getCookieName(requestParams, "booking_data");

  var bookingData = getCookieValue(req, 'booking_data', bookingDataCookieName); //req.universalCookies.get(bookingDataCookieName) || {};

  var userDetialsCookie = req.universalCookies.get('_user_details') || {};

  var accessToken = userDetialsCookie.access_token;

  if (status == "success") {
    if (isCoreBooking(serviceURL) || bookingData.isNewBookingEngine) {



      var confirmationSummary = getCookieValue(req, 'confirmation_summary', confirmationCookieName);//req.universalCookies.get(confirmationCookieName) || {};

      var signedInCustomerId = "";

      var softSafeBookingId = bookingID;

      var paymentMethod = "CREDIT_CARD";



      submitVerifyCreditCard(verifyToken, accessToken).then((response) => {
        if (response.success) {
          var responseData = response.data;
          var creditCardId = responseData.id;
          //var bookingDetailCookie = req.universalCookies.get(confirmationCookieName) || {};
          var couponValidationModel = getCookieValue(req, 'coupon_data', couponDataCookieName);//req.universalCookies.get(couponDataCookieName) || {};

          var isNewBookingEngine = false;
          if (bookingData.isNewBookingEngine) {
            isNewBookingEngine = true;
          }

          activateBookingRequest(accessToken, signedInCustomerId, softSafeBookingId, creditCardId, paymentMethod, couponValidationModel, isNewBookingEngine).then((activateRes) => {
            redirectLocation = home_url + "/" + serviceURL + "/book-online/confirmation/" + requestUuid;
            if (activateRes.data.success) {

              if (typeof bookingData.isSubscription != "undefined" && bookingData.isSubscription) {

                var subscriptionUrl = bookingData.typeOfSubscription == "daily" ? "daily-subscription" : "subscription";

                redirectLocation = home_url + "/" + serviceURL + "/book-online/" + subscriptionUrl + "/confirmation/" + requestUuid;
              }

              res.redirect(redirectLocation);
            }
          });
        }
      })
    } else {
      if (isLiteWeightBooking(serviceURL)) {
        submitVerifyCreditCard(verifyToken, accessToken).then((response) => {
          if (response.success) {
            var responseData = response.data;
            var creditCardId = responseData.id;
            // var bookingDetailCookie = req.universalCookies.get(confirmationCookieName) || {};
            var submittedData = getCookieValue(req, 'submitted_data', submittedDataCookieName); //req.universalCookies.get(submittedDataCookieName);
            //submittedData;

            submittedData[0]["Auth_token"] = String(creditCardId);

            // console.log("submittedData", submittedData);

            postLWRequest(JSON.stringify(submittedData)).then((response) => {
              // console.log(response);
              var resData = response.data;
              if (resData && resData.data[0].code == "SUCCESS") {
                var rid = resData.data[0].details.id;
                redirectLocation = home_url + "/" + serviceURL + "/book-online/confirmation/" + rid;
                res.redirect(redirectLocation);
              }
            });
          }
        })
      }
    }
  } else {
    var query = { status: status, bid: bookingID, tt: verifyToken };

    var queryParams = qs.stringify(query);

    redirectLocation = home_url + "/" + serviceURL + "/book-online?" + queryParams;

    if (typeof bookingData.isSubscription != "undefined" && bookingData.isSubscription) {

      var subscriptionUrl = bookingData.typeOfSubscription == "daily" ? "/daily-subscription" : "/subscription";

      redirectLocation = home_url + "/" + serviceURL + "/book-online" + subscriptionUrl + "?" + queryParams;
    }
    submitVerifyCreditCard(verifyToken, accessToken).then((response) => {
      res.redirect(redirectLocation);
    })
    /*if (status == "cancel") {
        
    }
    if (status == "decline") {
        //redirectLocation = home_url+"/cleaning-maid-services/book-online?"+queryParams;
        res.redirect(redirectLocation);
    }*/
  }

});

// Book online Landing success
app.get("/:lang/:city/:slug/book-online/:bookingID/success/:verifyToken", (req, res) => {
  var { city, lang } = req.params;
  let allowedLang = getAllowedLang(city);
  if (!(allowedLang.indexOf(req.params.lang) != -1)) {
    redirectDefaultLangPage(req, res);
  } else if (!(allowedCities.indexOf(req.params.city) != -1)) {
    res.redirect('/' + req.params.lang + '/dubai');
  } else {
    const verifyToken = req.params.verifyToken;
    var access_token = getUserToken(req);
    const bookingID = req.params.bookingID;
    // console.§log(verifyToken);
    const context = {};
    const store = createStore();
    let dipatchCall = commonDispatch(store, req);
    const bodyClass = store.dispatch(setBodyClass('confirmation'));
    dipatchCall.push(bodyClass);
    const extraData = ['whySMData', 'howIWData', 'logosData', 'footerItemsData'];
    const dataPromise = store.dispatch(fetchLandingData(req.params.city, req.params.slug, extraData, lang));
    dipatchCall.push(dataPromise);

    const submitCreditCard = submitVerifyCreditCard(verifyToken, access_token);

    dipatchCall.push(submitCreditCard);

    Promise.all(dipatchCall).then(() => {
      const reduxState = store.getState();
      res.writeHead(200, { "Content-Type": "text/html; charset=utf-8" });
      res.end(htmlTemplate(renderLayout(store, context, req), bundleJSFileName, 1050, reduxState));
    }).catch(function (error) {
      // handle error
      // console.log("Error Fetch", error);
    });
  }
});
// Book online Landing cancel
app.get("/:lang/:city/:slug/book-online/cancel/:verifyToken", (req, res) => {
  var { city, lang } = req.params;
  let allowedLang = getAllowedLang(city);
  if (!(allowedLang.indexOf(req.params.lang) != -1)) {
    redirectDefaultLangPage(req, res);
  } else if (!(allowedCities.indexOf(req.params.city) != -1)) {
    res.redirect('/' + req.params.lang + '/dubai');
  }
  res.redirect('/' + req.params.lang + '/' + req.params.city);
});
// Book online Landing decline
app.get("/:lang/:city/:slug/book-online/decline/:verifyToken", (req, res) => {
  var { city, lang } = req.params;
  let allowedLang = getAllowedLang(city);
  if (!(allowedLang.indexOf(req.params.lang) != -1)) {
    redirectDefaultLangPage(req, res);
  } else if (!(allowedCities.indexOf(req.params.city) != -1)) {
    res.redirect('/' + req.params.lang + '/dubai');
  }
  res.redirect('/' + req.params.lang + '/' + req.params.city);
});

app.use(function (req, res, next) {
  res.status(404).sendFile(__dirname + '/src/pages/404.html');
});
if (typeof bugsnagMiddleware != "undefined") {
  app.use(bugsnagMiddleware.errorHandler);
}
Loadable.preloadAll().then(() => {
  app.listen(8080, () => {
    console.log('Running on http://localhost:8080/');
  });
});


function zohoTemplate(reactDom, jsBundle) {
  let env = typeof process.env.NODE_ENV != "undefined" ? process.env.NODE_ENV : "";
  let MS_GA = env == "PROD" ? "UA-40300026-1" : "UA-47257077-1";


  return `<html>
        <head>
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <meta name="format-detection" content="telephone=yes">
            <title>ServiceMarket - Book Direct | ServiceMarket</title>
            <link rel="icon" type="image/png" href="/dist/images/favicon.png">
            <link rel="apple-touch-icon" href="/dist/images/sm-app-icon.png">
            <link rel="android-touch-icon" href="/dist/images/sm-app-icon.png">
            <link rel="preload" href="/dist/webfonts/6xK3dSBYKcSV-LCoeQqfX1RYOo3qNa7lqDY.woff2" as="font" type="font/woff2" crossorigin="anonymous">
            <link rel="preload" href="/dist/webfonts/6xKydSBYKcSV-LCoeQqfX1RYOo3i54rwmhduz8A.woff2" as="font" type="font/woff2" crossorigin="anonymous">
            <link rel="preload" href="/dist/webfonts/fontawesome-webfont.woff2" as="font" type="font/woff2" crossorigin="anonymous">
            <!-- Google Tag Manager -->
            <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-5RD9PL');</script>
            <!-- End Google Tag Manager -->*/ 
            <style type="text/css">
                .starter-template {
                    padding-top:50px;
                    text-align: center;
                }
    
                #image {
                    width:150px;
                    padding:15px;
                }
    
                #message {
                    color:gray;
                    font-size:1.2em;
                }
    
    
                .success, .failure, .error{
                    display:none;
                }
                    .zui-table {
                    border-collapse: collapse;
                    border-spacing: 0;
                    width: 400px;
                    margin:0 auto;
                    font: normal 13px Arial, sans-serif;
                }
                .zui-table thead th {
                    border: solid 1px #DDEEEE;
                    color: #336B6B;
                    padding: 10px;
                    text-align: left;
                    text-shadow: 1px 1px 1px #fff;
                }
                .zui-table tbody td {
                    border: solid 1px #DDEEEE;
                    color: #333;
                    padding: 10px;
                    text-shadow: 1px 1px 1px #fff;
                    text-align:left;
                }
    
                .zui-table tbody td:nth-child(2) {
                    text-align:right;
                }
    
                .zui-table tbody td:nth-child(1) {
                    color:gray;
                }
    
    
                .zui-table-horizontal tbody td {
                    border-left: none;
                    border-right: none;
                }
    
                .zui-table {
                    padding-left:15px;
                    padding-right:15px;
                }
    
                @media only screen and (max-width: 430px) {
                    .zui-table {
                        width: 100% !important;
                    }
                }
        </style>
        <script>


            window.addEventListener("load", function(){
                if(window.ga && ga.create) {
                    // Hooray! Analytics is present!
                    ga('SM.send', 'pageview');
                } else {
                    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
                    ga('create', '${MS_GA}', 'auto', 'SM');
                    ga('SM.send', 'pageview');
                }
            }, false);



            (function(history){
                var pushState = history.pushState;
                history.pushState = function(state) {
                    if (typeof history.onpushstate == "function") {
                        history.onpushstate({state: state});
                    }
                    
                    setTimeout(function () { 
                    if(window.ga && ga.create) {
                            ga('SM.set', 'page', location.pathname+location.search+location.hash);
                            ga('SM.set', 'title', document.title);
                            ga('SM.send', 'pageview');
                    }
                    } , 1500);

                    return pushState.apply(history, arguments);
                };
            })(window.history);

            </script>
        </head>
        
        <body>
            <!-- Google Tag Manager (noscript) -->
            <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5RD9PL"
            height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
            <!-- End Google Tag Manager (noscript) -->
           <div id="app">${ reactDom}</div>
            <script src="/dist/js/${ jsBundle}.js" charset="UTF-8"></script>
        </body>
        </html>`;
}
function htmlTemplate(reactDom, jsBundle, containerWidth, data, metaData = { title: 'ServiceMarket', desc: 'ServiceMarket' }, loadGoogleScript = false, fullURL = '', noindex = false, serviceProviderTemplate = '') {
  // console.log('script google load: '+ loadGoogleScript);
  var css = '';

  //const googleApiKey = commonHelper.getGoogleAPIKey();
  var bodyClass = (typeof data.bodyClass != "undefined" && data.bodyClass == "bg-white") ? data.bodyClass : "";

  let env = typeof process.env.NODE_ENV != "undefined" ? process.env.NODE_ENV : "";
  let direction = "";
  //bootstrap.css
  let langCode = "en-AE";
  let countryCode = "";
  let currentLang = typeof reactDom.lang != "undefined" ? reactDom.lang : LANG_EN;

  if ((typeof reactDom.lang != "undefined" && typeof data.currentCityData != "undefined") && (data.currentCityData.length && typeof data.currentCityData[0].cityDto.countryDto.countryCode != "undefined")) {
    langCode = reactDom.lang + '-' + data.currentCityData[0].cityDto.countryDto.countryCode;
    countryCode = data.currentCityData[0].cityDto.countryDto.countryCode;
  }
  if (typeof reactDom.lang != "undefined" && reactDom.lang == "ar") {
    direction = 'dir="rtl"';
    bodyClass += " " + reactDom.lang;
    css = fileSystem.readFileSync('./dist/css/bootstrap-rtl.css', 'utf8');
  } else {
    css = fileSystem.readFileSync('./dist/css/bootstrap.css', 'utf8');
  }

  css += fileSystem.readFileSync('./dist/css/bundle.css', 'utf8');

  noindex = (env != "PROD") ? true : noindex;
  let MS_GA = env == "PROD" ? "UA-40300026-1" : "UA-47257077-1";
  var result = '';
  let currentCity = typeof data.currentCity != "undefined" ? data.currentCity : DUBAI;
  let linkAlternateText = "";

  if ((currentCity != "" && showLangNav(currentCity)) && typeof reactDom.originalUrl != "undefined") {
    //
    let originalUrl = reactDom.originalUrl;
    let arabicUrl = '';
    let englishUrl = '';
    if (typeof data.bodyClass != "undefined" && data.bodyClass == "home") {
      arabicUrl = originalUrl.replace("/" + LANG_EN, "/" + LANG_AR);
      englishUrl = originalUrl.replace("/" + LANG_AR, "/" + LANG_EN);
    } else {
      arabicUrl = originalUrl.replace("/" + LANG_EN + "/", "/" + LANG_AR + "/");
      englishUrl = originalUrl.replace("/" + LANG_AR + "/", "/" + LANG_EN + "/");
    }
    linkAlternateText = '<link rel="alternate" hreflang="en-' + countryCode + '" href="https://servicemarket.com' + englishUrl + '" />\n';
    linkAlternateText += '<link rel="alternate" hreflang="ar-' + countryCode + '" href="https://servicemarket.com' + arabicUrl + '" />';
  }
  let bundles = reactDom.bundles;
  //console.log("bundles", bundles);
  if (loadGoogleScript) {
    result = `
        <html ${direction} lang="${langCode}">
        <head>
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <meta name="apple-itunes-app" content="app-id=1146756698">
            <meta name="google-play-app" content="app-id=com.servicemarket.app">
            <meta name="Language" Content="${langCode}">  
            <meta name="description" content="${metaData.desc}">
            <meta name="robots" content="${ noindex ? 'noindex' : 'follow, index'}">
            <title>${metaData.title}</title>
            <link rel="icon" type="image/png" href="/dist/images/favicon.png">
            <link rel="apple-touch-icon" href="/dist/images/sm-app-icon.png">
            <link rel="android-touch-icon" href="/dist/images/sm-app-icon.png">
            <link rel="preload" href="/dist/webfonts/6xKydSBYKcSV-LCoeQqfX1RYOo3i54rwlxdu.woff2" as="font" type="font/woff2" crossorigin="anonymous">
            <link rel="preload" href="/dist/webfonts/6xK3dSBYKcSV-LCoeQqfX1RYOo3qOK7l.woff2" as="font" type="font/woff2" crossorigin="anonymous">
            <link rel="preload" href="/dist/webfonts/fontawesome-webfont.woff2" as="font" type="font/woff2" crossorigin="anonymous">
            <!-- Google Tag Manager -->
            <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-5RD9PL');</script>
            <!-- End Google Tag Manager --> 
            <script>                
            var scriptLoaded=!0;window.myCallbackFunc=function(){window.initOne&&window.initOne(),window.initTwo&&window.initTwo(),window.initPickup&&window.initPickup(),window.initDropoff&&window.initDropoff()};
            </script>
            <style>${ css}</style>
            <script charset="UTF-8">
            var env="${env}";window.REDUX_DATA=${JSON.stringify(data)};
            </script>
            <script>
            window.addEventListener("load",function(){if(window.ga&&ga.create)ga("SM.send","pageview");else{(function(b,c,d,e,f,g,h){b.GoogleAnalyticsObject=f,b[f]=b[f]||function(){(b[f].q=b[f].q||[]).push(arguments)},b[f].l=1*new Date,g=c.createElement(d),h=c.getElementsByTagName(d)[0],g.async=1,g.src=e,h.parentNode.insertBefore(g,h)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga"), ga("create","${MS_GA}","auto","SM"),ga("SM.send","pageview")}},!1),function(a){var b=a.pushState;a.pushState=function(c){return"function"==typeof a.onpushstate&&a.onpushstate({state:c}),setTimeout(function(){window.ga&&ga.create&&(ga("SM.set","page",location.pathname+location.search+location.hash),ga("SM.set","title",document.title),ga("SM.send","pageview"))},1500),b.apply(a,arguments)}}(window.history);
            </script>
            ${linkAlternateText}
        </head>
        
        <body class="${ bodyClass}">
            <!-- Google Tag Manager (noscript) -->
            <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5RD9PL"
            height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
            <!-- End Google Tag Manager (noscript) -->
            <div id="app">${ reactDom.reactDom}</div>
            <script src="/dist/js/${ jsBundle}.js" charset="UTF-8"></script>
            ${bundles.map(bundle => {
      return ((bundle.file).indexOf(".js") && (bundle.file).indexOf(jsBundle + '.js') > 1) ? `<script src="/dist/js/${bundle.file}"></script>` : ''
    }).join(' ')}
            <script>
            var el=document.createElement("script");el.type="application/ld+json",el.text=JSON.stringify({"@context":"http://schema.org","@type":"Organization",name:"ServiceMarket",url:"https://servicemarket.com/${currentLang}/${currentCity}",description:"Book online, get exclusive offers, get free quotes, and read customer reviews of over 150 licensed moving, cleaning, insurance, and other service companies in "+'${commonHelper.toTitleCase(data.currentCity.replace("-", " "))}',logo:"https://servicemarket.com/dist/images/servicemarket-main-logo.svg",address:{"@type":"PostalAddress",addressCountry:"United Arab Emirates",addressLocality:"Dubai",postalCode:"28258",streetAddress:"2203, Reef Tower, JLT"},founder:"Bana Shomali, Wim Torfs",sameAs:["https://www.facebook.com/myServiceMarket","https://www.twitter.com/myServiceMarket","http://www.instagram.com/myServiceMarket","https://plus.google.com/+MovesouqGCC","https://www.linkedin.com/company/servicemarket","https://www.youtube.com/channel/UCAQQbqkK1MuEQxHo1mx1Iog"],contactPoint:{"@type":"ContactPoint",telephone:"+971 4 4229639",email:"mailto:hello@servicemarket.com",contactType:"customer service"}}),document.querySelector("body").appendChild(el);
                
            </script>
            <script>
                if('${serviceProviderTemplate.length > 0}' == 'true') {
                    var el = document.createElement('script');
                    el.type = 'application/ld+json';
                    el.text = '${serviceProviderTemplate}';
                    document.querySelector('body').appendChild(el);
                }
                
            </script>
        </body>
        </html>
    `;
  }
  else
    result = `
        <html  ${direction} lang="${langCode}">
        <head>
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <meta name="apple-itunes-app" content="app-id=1146756698">
            <meta name="google-play-app" content="app-id=com.servicemarket.app">
            <meta name="Language" Content="${langCode}">  
            <meta name="description" content="${metaData.desc}">
            <title>${metaData.title}</title>
            <meta name="robots" content="${ noindex ? 'noindex' : 'follow, index'}">
            <link rel="icon" type="image/png" href="/dist/images/favicon.png">
            <link rel="apple-touch-icon" href="/dist/images/sm-app-icon.png">
            <link rel="android-touch-icon" href="/dist/images/sm-app-icon.png">
            <link rel="preload" href="/dist/webfonts/6xKydSBYKcSV-LCoeQqfX1RYOo3i54rwlxdu.woff2" as="font" type="font/woff2" crossorigin="anonymous">
            <link rel="preload" href="/dist/webfonts/6xK3dSBYKcSV-LCoeQqfX1RYOo3qOK7l.woff2" as="font" type="font/woff2" crossorigin="anonymous">
            <link rel="preload" href="/dist/webfonts/fontawesome-webfont.woff2" as="font" type="font/woff2" crossorigin="anonymous">
            <!-- Google Tag Manager -->
            <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-5RD9PL');</script>
            <!-- End Google Tag Manager -->
            <style>${ css}</style>
            <script>
                
            var scriptLoaded=!0;window.myCallbackFunc=function(){window.initOne&&window.initOne(),window.initTwo&&window.initTwo(),window.initPickup&&window.initPickup(),window.initDropoff&&window.initDropoff()};
            </script>
            <script charset="UTF-8">
                 var env = "${env}";  window.REDUX_DATA = ${JSON.stringify(data)};
            </script>
            <script>


            window.addEventListener("load",function(){if(window.ga&&ga.create)ga("SM.send","pageview");else{(function(b,c,d,e,f,g,h){b.GoogleAnalyticsObject=f,b[f]=b[f]||function(){(b[f].q=b[f].q||[]).push(arguments)},b[f].l=1*new Date,g=c.createElement(d),h=c.getElementsByTagName(d)[0],g.async=1,g.src=e,h.parentNode.insertBefore(g,h)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga"), ga("create","${MS_GA}","auto","SM"),ga("SM.send","pageview")}},!1),function(a){var b=a.pushState;a.pushState=function(c){return"function"==typeof a.onpushstate&&a.onpushstate({state:c}),setTimeout(function(){window.ga&&ga.create&&(ga("SM.set","page",location.pathname+location.search+location.hash),ga("SM.set","title",document.title),ga("SM.send","pageview"))},1500),b.apply(a,arguments)}}(window.history);

            </script>
            ${linkAlternateText}
        </head>
        
        <body class="${ bodyClass}">
            <!-- Google Tag Manager (noscript) -->
            <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5RD9PL"
            height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
            <!-- End Google Tag Manager (noscript) -->
            <div id="app">${ reactDom.reactDom}</div>
                <script src="/dist/js/${ jsBundle}.js" charset="UTF-8"></script>
                ${bundles.map(bundle => {
      return ((bundle.file).indexOf(".js") && (bundle.file).indexOf(jsBundle + '.js') > 1) ? `<script src="/dist/js/${bundle.file}"></script>` : ''
    }).join(' ')}
            <script>
            el=document.createElement("script");el.type="application/ld+json",el.text=JSON.stringify({"@context":"http://schema.org","@type":"Organization",name:"ServiceMarket",url:'https://servicemarket.com/${currentLang}/${currentCity}',description:"Book online, get exclusive offers, get free quotes, and read customer reviews of over 150 licensed moving, cleaning, insurance, and other service companies in ${commonHelper.toTitleCase(data.currentCity.replace("-", " "))}",logo:"https://servicemarket.com/dist/images/servicemarket-main-logo.svg",address:{"@type":"PostalAddress",addressCountry:"United Arab Emirates",addressLocality:"Dubai",postalCode:"28258",streetAddress:"2203, Reef Tower, JLT"},founder:"Bana Shomali, Wim Torfs",sameAs:["https://www.facebook.com/myServiceMarket","https://www.twitter.com/myServiceMarket","http://www.instagram.com/myServiceMarket","https://plus.google.com/+MovesouqGCC","https://www.linkedin.com/company/servicemarket","https://www.youtube.com/channel/UCAQQbqkK1MuEQxHo1mx1Iog"],contactPoint:{"@type":"ContactPoint",telephone:"+971 4 4229639",email:"mailto:hello@servicemarket.com",contactType:"customer service"}}),document.querySelector("body").appendChild(el);
                
            </script>
            <script>
                if('${serviceProviderTemplate.length > 0}' == 'true') {
                    var el = document.createElement('script');
                    el.type = 'application/ld+json';
                    el.text = '${serviceProviderTemplate}';
                    document.querySelector('body').appendChild(el);
                }
                
            </script>
        </body>
        </html>
    `;
  //console.log('initial html',result);
  const minifiedHtml = minify(result, {
    removeAttributeQuotes: true
  });
  //alert('after minify html',result1);
  return minifiedHtml;

}
function getHomePageUrl() {
  var url = window.location.protocol + '//' + window.location.host;
  return url;
}
function getUserToken(req) {
  var userDetialsCookie = req.universalCookies.get('_user_details') || {};

  var access_token = typeof userDetialsCookie.access_token != "undefined" ? userDetialsCookie.access_token : '';

  return access_token;
}
function getCookieName(params, name) {
  var cookieName = params.lang + "." + params.city + "." + params.slug + "." + name;
  return cookieName;
}
function fetchAllApiServicesPromiseCall(store, city, lang = DEFAULT_LANG) {
  var cityCode = typeof cityCodes[city] != "undefined" ? cityCodes[city] : 'DXB';
  const fetchAllApiServicesPromise = store.dispatch(fetchAllApiServices(cityCode, lang));
  return fetchAllApiServicesPromise;
}
function reloadIfDataIsEmpty(reduxState) {
  if (reduxState.allServices.length == 0 || reduxState.serviceConstants.length == 0 ||
    reduxState.newDataConstants.length == 0 ||
    (Object.keys(reduxState.dataConstants).length == 0 || Object.keys(reduxState.apiServices).length == 0 || Object.keys(reduxState.bookingServices).length == 0)) {
    return true;
  }
  return false;
}
function getDefaultCity(req) {
  var user_city = req.universalCookies.get('user_city') || DEFAULT_CITY_NAME;

  return user_city;
}
function getDefaultLang(req) {
  var user_city = req.universalCookies.get('user_lang') || DEFAULT_LANG;

  return user_city;
}
function getAllowedLang(city) {
  let allowedLang = ['en'];
  if (arabicAllowedCities.includes(city)) {
    allowedLang.push('ar');
  }
  return allowedLang;
}
function redirectDefaultLangPage(req, res) {
  var { city, lang } = req.params;
  let originalUrl = req.originalUrl;
  let redirectUrl = originalUrl.replace('/' + LANG_AR + '/', '/' + LANG_EN + '/')
  res.redirect(redirectUrl);
}
function removeLocalStorage(req) {
  let bookingDataCookieName = getCookieName(req.params, 'booking_data');
  let confirmationDataCookieName = getCookieName(req.params, 'confirmation_summary');
  let couponDataCookieName = getCookieName(req.params, 'coupon_data');
  let submittedDataCookieName = getCookieName(req.params, 'submitted_data');

  if (isLocalStorageAvailable()) {
    if (typeof localStorage === "undefined" || localStorage === null) {
      var LocalStorage = require('node-localstorage').LocalStorage;
      let localStorage = new LocalStorage('./scratch');
      localStorage.removeItem(bookingDataCookieName);
      localStorage.removeItem(confirmationDataCookieName);
      localStorage.removeItem(couponDataCookieName);
      localStorage.removeItem(submittedDataCookieName);
    }
  }
}
function getCookieValue(req, nameOfcookie, cookieName = null) {
  var cookieValue = {};
  cookieName = cookieName == null ? getCookieName(req.params, nameOfcookie) : cookieName;

  if (isLocalStorageAvailable()) {
    if (typeof localStorage === "undefined" || localStorage === null) {
      var LocalStorage = require('node-localstorage').LocalStorage;
      let localStorage = new LocalStorage('./scratch');
      cookieValue = JSON.parse(localStorage.getItem(cookieName)) || {};

      //console.log("cookieValue",localStorage.getItem('testBitServer'));
      //en.dubai.cleaning-maid-services.confirmation_summary
      //en.dubai.cleaning-maid-services.confirmation_summary
    }
  } else {
    cookieValue = req.universalCookies.get(cookieName) || {};
  }

  return cookieValue;

  /*if (Object.keys(confirmationSummary).length == 0) {
      res.redirect('/' + req.params.lang + '/' + req.params.city);
  }*/
}
function isLocalStorageAvailable() {
  let localStorage = null;
  if (typeof localStorage === "undefined" || localStorage === null) {
    var LocalStorage = require('node-localstorage').LocalStorage;
    localStorage = new LocalStorage('./scratch');
  }
  let hasLocalStorage = null;
  try {
    localStorage.setItem("testBitServer", 1);
    hasLocalStorage = true;
  } catch (e) {
    hasLocalStorage = false;
  }
  return hasLocalStorage;
}