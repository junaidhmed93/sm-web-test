const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
//const MomentLocalesPlugin = require('moment-locales-webpack-plugin');
const dotenv = require("dotenv");
const webpack = require("webpack");
const MomentTimezoneDataPlugin = require("moment-timezone-data-webpack-plugin");
const currentYear = new Date().getFullYear();
const BundleAnalyzerPlugin = require("webpack-bundle-analyzer").BundleAnalyzerPlugin;
const ReactLoadablePlugin = require('react-loadable/webpack').ReactLoadablePlugin;
//const stringConstants = require('../react-web/src/constants/stringConstants'); 
const minYear = 2011;
dotenv.config();
// const envKeys = (env) => {
//   Object.keys(env).reduce((prev, next) => {
//     prev[`process.env.${next}`] = JSON.stringify(env[next]);
//     return prev;
//   }, {});
// };
module.exports = env => ({
  mode: "production",
  node: {
    fs: "empty"
  },
  entry: {
    app: "./src/App.jsx"
  },
  output: {
    filename: 'appv51.js',
    publicPath: '/dist/js/',
    path: __dirname + '/dist/js'
  },
  watch: true,
  optimization: {
    // splitChunks: {
    //   chunks: 'all',
    // },
    minimizer: [
      new UglifyJsPlugin({
        uglifyOptions: {
          output: {
            comments: false
          }
        }
      }),
      new OptimizeCSSAssetsPlugin({})
    ]
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader
          },
          {
            loader: "css-loader",
            options: {
              url: false,
              import: false
            }
          }
        ]
      },
      {
        test: /\.jsx$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["react", "env", "stage-0"]
          }
        }
      }
    ]
  },
  resolve: {
    extensions: [".js", ".jsx"]
  },
  plugins: [
    new ReactLoadablePlugin({
        filename: './dist/js/react-loadable.json',
        publicPath: __dirname + '/dist/js'
    }),
    new MiniCssExtractPlugin({
      filename: "../css/bundle.css"
    }),
    //new webpack.IgnorePlugin(/\.\/locale$/),
    // To strip all locales except “en”, and arabic
    // (“en” is built into Moment and can’t be removed)
    // new MomentLocalesPlugin({
    //     localesToKeep: ['en','ar-AE'],
    // }),

    // load `moment/locale/ja.js` and `moment/locale/it.js`
    new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /en|ar/),
    new MomentTimezoneDataPlugin({
      //matchZones: /Europe\/(Belfast|London|Paris|Athens)/,
      startYear: minYear,
      endYear: currentYear + 3
    }),
    //new BundleAnalyzerPlugin()
    //new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),

    //new webpack.DefinePlugin({ 'process.env.NODE_ENV': JSON.stringify(env.NODE_ENV) || JSON.stringify('development') })
    //new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /en-gb|ar/),
  ]
});
