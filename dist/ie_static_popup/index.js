const ieStaticPopupContent = `
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<title>Service Market</title>

	<link href="/dist/ie_static_popup/jquery.reject.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
	* {
		margin: 0;
		padding: 0;
	}
	body {
		background: #f7f7f7;
		font-size: 14px;
		font-family: 'Arial', sans-serif;
	}
	a {
		text-decoration: none;
		color: #025167;
	}
	a:hover {
		text-decoration: underline;
	}
	a:visited {
		color: #025167;
	}
	a[rel="external"] {
		font-weight: bold;
	}
	strong {
		color: #262626;
		text-decoration: underline;
	}
	h1 {
		color: #5DB0E6;
		margin-bottom: 20px;
	}
	h2 {
		color: #a61700;
		margin: 20px 0;
	}
	h3 {
		color: #0082a9;
		text-decoration: underline;
	}
	hr {
		outline: 0;
		border: 0;
		width: 50%;
		height: 1px;
		margin: 20px auto;
		background: #CCC;
	}
	p {
		margin: 10px 0;
	}
	pre {
		width: 100%;
		font-family: monospace;
	}
	acronym {
		border-bottom: 1px dashed #000;
		cursor: help;
	}
	#wrapper {
		margin: 0 auto;
		width: 960px;
		border: 1px solid #088de0;
		border-top: 0;
		border-bottom: 0;
	}
	#content {
		width: 940px;
		padding: 10px;
		background: #f5f5f5;
	}
	.box, pre.javascript {
		width: 90%;
		border: 1px dashed #0082a9;
		background: #cecccc;
		padding: 10px;
	}
	.box {
		width: 80%;
	}
	.example {
		margin: 30px 0;
	}
	.example h3 {
		display: inline;
	}
	a.demo {
		font-weight: bold;
		display: inline;
		margin-left: 10px;
	}
	a.demo:active {
		color: red;
	}
	</style>

	<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
	<script src="/dist/ie_static_popup/jquery.reject.js"></script>
	<script>
	$(function() {
    
    $(document).ready(function() {
      $.reject({
        reject: {
          safari: true, // Apple Safari
          chrome: true, // Google Chrome
          firefox: true, // Mozilla Firefox
          msie: true, // Microsoft Internet Explorer
          opera: true, // Opera
          konqueror: true, // Konqueror (Linux)
          unknown: true // Everything else
        }
      }); // Customized Browsers
    });
    

		// Handle displaying of option links
		$('em.option').css({
			cursor: 'pointer',
			fontSize: '1.1em',
			color: '#333',
			letterSpacing: '1px',
			borderBottom: '1px dashed #BBB'
		}).click(function() {
			var self = $(this);
			var opt = $.trim(self.text());

			$('ol.dp-c:first').children('li').children('span').each(function() {
				var self = $(this);
				var text = self.text();
				var srch = opt+':';

				// If found, highlight and jump window to position on page
				if (text.search(srch) !== -1) {
					self.css('color','red');
					window.location.hash = '#c-'+opt;
					window.scrollTo(0,self[0].offsetTop);
				}
			});
		});

	});
	</script>
</head>
<body>
	<img src="/dist/ie_static_popup/IE-template.png" />
</body>
</html>`

export default ieStaticPopupContent;