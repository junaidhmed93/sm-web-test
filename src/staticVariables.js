const dotenv = require('dotenv');
dotenv.config();
//console.log('process',process.env.NODE_ENV);
let BE_API,MIDDLEWARE_URL,GOOGLE_APP_ID,FACEBOOK_APP_ID ='', IMGIX_WP_BASE_URL = 'https://servicemarketwp.imgix.net',
IMGIX_REACT_BASE_URL = 'https://servicemarket.imgix.net',WORDPRESS_URL = 'https://blog.servicemarket.com/en/blog';

let NODE_ENV = process.env.NODE_ENV; // For server side rendering
if(typeof env != "undefined"){ // env JS variable from global variable
    NODE_ENV = env;
}
if (typeof NODE_ENV != "undefined") {
    switch (NODE_ENV) {
        case 'PROD':{
            BE_API = 'https://api.servicemarket.com',
            MIDDLEWARE_URL = 'https://middleware.servicemarket.com/index.php';
            GOOGLE_APP_ID = '343478171408-2of41fcld9sj904ulijt37eln1jhpt9r.apps.googleusercontent.com',
            FACEBOOK_APP_ID = 224183481068339;
            break;
        }
        case 'uat': {
            BE_API = 'https://uat1-api.servicemarket.com',
            MIDDLEWARE_URL = 'https://uat1-middleware.servicemarket.com/index.php';
            /* Using this Google ID as its whitelisted for sm.aratech.ae 
            GOOGLE_APP_ID = '1097484187557-7ju1k29vng6tltmqdn3jqsgrdv997ct7.apps.googleusercontent.com', */
            /* '449837965633-b8giuq8fuead0lfqlu91mgorvlfc92m6.apps.googleusercontent.com',*/
            GOOGLE_APP_ID = '449837965633-b8giuq8fuead0lfqlu91mgorvlfc92m6.apps.googleusercontent.com',
            FACEBOOK_APP_ID = 1767998170193799;
            IMGIX_WP_BASE_URL = 'https://sm-uat1-blog.imgix.net',
            IMGIX_REACT_BASE_URL = 'https://sm-uat1.imgix.net'
            WORDPRESS_URL = 'https://uat1.servicemarket.com/en/blog';
            break;
        }
        case 'aratech': {
            BE_API = 'https://aratech-api.servicemarket.com',
            MIDDLEWARE_URL = 'https://aratech-middleware.servicemarket.com/index.php';
            GOOGLE_APP_ID = '1097484187557-7ju1k29vng6tltmqdn3jqsgrdv997ct7.apps.googleusercontent.com',
            FACEBOOK_APP_ID = 1767998170193799;
            break;
        }
        case 'local': {
            BE_API = 'https://uat2-api.servicemarket.com',
            MIDDLEWARE_URL = 'http://localhost/middleware/index.php';
            GOOGLE_APP_ID = '1097484187557-7ju1k29vng6tltmqdn3jqsgrdv997ct7.apps.googleusercontent.com',
            FACEBOOK_APP_ID = 1767998170193799;
            IMGIX_WP_BASE_URL = 'https://sm-uat1-blog.imgix.net',
            IMGIX_REACT_BASE_URL = 'https://sm-uat1.imgix.net'
            WORDPRESS_URL = 'https://uat1.servicemarket.com/en/blog';
            break;
        }
        default: 
            BE_API = 'https://uat1-api.servicemarket.com',
            MIDDLEWARE_URL = 'https://uat1-middleware.servicemarket.com/index.php';
            GOOGLE_APP_ID = '449837965633-b8giuq8fuead0lfqlu91mgorvlfc92m6.apps.googleusercontent.com',
            FACEBOOK_APP_ID = 1767998170193799;
            IMGIX_WP_BASE_URL = 'https://sm-uat1-blog.imgix.net',
            IMGIX_REACT_BASE_URL = 'https://sm-uat1.imgix.net'
            WORDPRESS_URL = 'https://uat1.servicemarket.com/en/blog';
            break;
    }
}

//console.log('IMGIX_WP_BASE_URL', IMGIX_WP_BASE_URL, WORDPRESS_URL, NODE_ENV)
const staticVariables = {
    CORE_API: BE_API,
    WORDPRESS_URL: WORDPRESS_URL,
    MIDDLEWARE_URL: MIDDLEWARE_URL,

    IMGIX_WP_BASE_URL: IMGIX_WP_BASE_URL,
    IMGIX_REACT_BASE_URL: IMGIX_REACT_BASE_URL,

    STATIC_CONTENT_WHY_SM_DATA: 'why-service-market',
    STATIC_CONTENT_HOW_IW_DATA: 'how-it-works',
    STATIC_CONTENT_LOGOS_DATA: 'partners',
    STATIC_CONTENT_FOOTER_DATA: 'footer-items',
    STATIC_CONTENT_POP_UP_DATA: 'popup-menu',

    STATIC_PAGE_ABOUT: 'about-us',
    STATIC_PAGE_BE_PARTNER: 'become-a-servicemarket-partner',
    STATIC_PAGE_REVIEW: 'review',
    STATIC_PAGE_TEAM: 'be-a-part-of-our-team',
    STATIC_PAGE_FAQS: 'frequently-asked-questions',
    STATIC_PAGE_PRIVACY: 'privacy-policy',
    STATIC_PAGE_COMPANIES: 'partners-list',
    STATIC_PAGE_DISCLAIMER: 'disclaimer',
    STATIC_PAGE_TERMS: 'terms-and-conditions',
    STATIC_PAGE_PRP: 'payment-and-refund-policy',

    GOOGLE_APP_ID: GOOGLE_APP_ID,
    FACEBOOK_APP_ID: FACEBOOK_APP_ID,
    BUGSNAG_CLIENT_APP_ID: 'af7189c7afe607cc87c9a91ee19e3f15',
    BUGSNAG_SERVER_APP_ID: '62f88762320a7d8d83daae372fb9984b',

    ENCRYPTION_PASS: 'fnej#jrnJFEu338fNEUfe?#32'
}


export default staticVariables;