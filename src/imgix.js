import staticVariables from "./staticVariables";

export const imgixReactBase = staticVariables.IMGIX_REACT_BASE_URL;
export const imgixWPBase = staticVariables.IMGIX_WP_BASE_URL;
export const quality = 46;

// Convert WP Image URL to relative
export function relativeURL(url){
	return typeof(url) == "string" ? url.replace('http://localhost/karthi_code/ms_new_blog','').replace('https://aratech-blog.servicemarket.com','').replace('https://blog.servicemarket.com','').replace('https://uat1-blog.servicemarket.com','') : '';
}