import React from "react";
import SectionTitle from "./SectionTitle";
import ReactHtmlParser from 'react-html-parser';

class TitleTextBlock extends React.Component{
    constructor(props) {
        super(props);
    }
    createMarkup(htmlString) {
        return {__html: "<div>" + htmlString + "</div>"};
    }
    render() {
        return (
            <div className={this.props.parentClass}>
                <div className={this.props.childClass}>
                    {this.props.title.length ? <SectionTitle title={this.props.title}/> : ''}
                    {this.props.text.length ? ( <p>{ReactHtmlParser(this.props.text)}</p> ) : ''}
                </div>
            </div>
        );
    }
}

TitleTextBlock.defaultProps = {
  parentClass: 'row mb-4',
  childClass: 'col',
  title: '',
  text: ''
};
export default TitleTextBlock;
