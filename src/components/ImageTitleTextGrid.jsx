import React from "react";
import {imgixWPBase, relativeURL, quality} from '../imgix';
import LazyLoad from 'react-lazyload';
import PlaceholderComponent from '../components/Placeholder';

class ImageTitleTextGrid extends React.Component{
    constructor(props) {
        super(props);
    }
    createMarkup(htmlString) {
        return {__html: htmlString};
      }
    renderList(){
        return this.props.items.map((item, index) => {
            return(
                <div key={"itg"+index} className={this.props.childClass}>
                    <LazyLoad offset={500} height={140} placeholder={<PlaceholderComponent />}>
                        <img src={imgixWPBase+relativeURL(item.image)+"?fit=crop&h=140&auto=format,compress&q="} height="140" className="mw-100" alt="" />
                    </LazyLoad>
                    <p className="h3 mt-3 text-uppercase">{item.title}</p>
                    <p className="m-0 text-sm" dangerouslySetInnerHTML={this.createMarkup(item.description)}></p>
                </div>
            );
        });
    }

    render() {
        return (
            <div className={this.props.parentClass}>
                {this.renderList()}
            </div>
        );
    }
}
ImageTitleTextGrid.defaultProps = {
  parentClass: 'row mb-5',
  childClass: 'col-12 col-md-4 text-center',
  items: []
};

export default ImageTitleTextGrid;
