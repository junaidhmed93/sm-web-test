import React, { PureComponent } from "react";
import {Link, Redirect} from "react-router-dom";
import { withCookies } from "react-cookie";
import { Sticky, StickyContainer } from 'react-sticky';
import { isMobile } from "../actions";
import {connect} from "react-redux";
import locationHelper from "../helpers/locationHelper";
class StickyCTA extends PureComponent{
    constructor(props) {
        super(props);
        this.renderButtons = this.renderButtons.bind(this);
        this.renderMobileButtons = this.renderMobileButtons.bind(this);
        this.renderLinks = this.renderLinks.bind(this);
        this.locationHelperInit = locationHelper.init.bind(this);
        this.locationHelperInit();
    }
    handleScroll() {
        if(document.getElementsByClassName("sticky-cta").length && document.getElementsByClassName("sticky-cta")[0] != "undefined"){
            var cls = document.getElementsByClassName("sticky-cta")[0].className;
            var mainEle = document.getElementById("app");
            if(cls.indexOf("is-sticky") > -1){
                mainEle.classList.add("is-sticky-yes");
            }else{
                mainEle.classList.remove("is-sticky-yes");
            }
        }
    }
    componentDidMount(){
        window.addEventListener('scroll', this.handleScroll);
    }
    
    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    }
    
    setQuotesServiceId(btn){
        if(typeof btn.serviceId != "undefined" && btn.serviceId != ""){
            const { cookies } = this.props;
            cookies.set('landingPageServiceId', btn.serviceId, {path: '/', maxAge:300});
        }
    }
    renderButtons(){
        var {btnLinks} = this.props;
        return btnLinks.map((btn, index) => {
            return this.renderLinks(btn, index)
        });
    }
    renderMobileButtons(){
        var {btnLinks} = this.props;
        var cols = btnLinks.length == 1 ?  "col-12" : "col-6";
        return btnLinks.map((btn, index) => {
            return (
                <div className={cols+" px-0 d-xs-flex text-center"}>
                    {this.renderLinks(btn, index)}
                </div>
            );
        });
    }
    renderLinks(btn, index){
        //var btnText = (isMobile() && btn.isQuote) ? "Request Quotes" : btn.title;
        var btnText = btn.title;
        var greyCls = isMobile() ? "btn-dark" : "btn-dark";
        let arrowClass = locationHelper.translate("ARROW_CLASS");
        if(btn.isExternalLink){
            return (
                <a key={index} onClick={ () => this.setQuotesServiceId(btn) }  href={btn.link} className={index == 0 ? "btn btn-warning mr-4" : "btn "+greyCls}>
                    {btnText} <i className={"d-none d-sm-block d-md-block "+arrowClass}></i>
                </a>
            );
        }else{
            return (
                <Link key={index} onClick={ () => this.setQuotesServiceId(btn) }  to={btn.link} className={index == 0 ? "btn btn-warning mr-4" : "btn "+greyCls}>
                    {btnText} <i className={"d-none d-sm-block d-md-block "+arrowClass}></i>
                </Link>
            );
        }
    }
    render() {
        var {title} = this.props;
        return (
            <Sticky disableCompensation bottomOffset={-400}>
                {({ style, isSticky,calculatedHeight }) => (
                    //var clsName = isSticky ? "is-sticky" : "";
                    <div className={ isSticky ? "footer-buttons container-fluid bg-light shadow-1 sticky-cta is-sticky" : "footer-buttons container-fluid bg-white sticky-cta"} style={style}>
                        <div className="btn-fixed py-sm-4 py-md-3 text-center">
                            <div className="text-right d-none d-md-inline-block mr-4">
                                <div className="cta-text">{title}</div>
                            </div>
                            <div className="text-left d-block d-md-inline-block">
                                { isMobile() ? (<div className="row m-0">{this.renderMobileButtons()}</div>) : this.renderButtons() }
                            </div>
                        </div>
                    </div>)}
            </Sticky>
        );
    }
}
StickyCTA.defaultProps = {
    btnLinks:[]
  };
function mapStateToProps(state){
	return {
        currentCity: state.currentCity,
        lang: state.lang
    }
}
export default withCookies( connect(mapStateToProps)(StickyCTA) );
