import React from "react";

class PageIntro extends React.Component{
    constructor(props) {
        super(props);
    }
    createMarkup(htmlString) {
        return {__html: htmlString};
    }
    render() {
        return (
            <section className="bg-white py-5">
                <div className="container">
                    <h1 className="text-center font-weight-bold">{this.props.title}</h1>
                    {this.props.description.length ? ( <h2 className="text-center" dangerouslySetInnerHTML={this.createMarkup(this.props.description)}></h2> ) : ''}
                </div>
            </section>
        );
    }
}
PageIntro.defaultProps = {
  title: '',
  description: ''
};

export default PageIntro;
