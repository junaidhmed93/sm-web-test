import React from "react";
import {imgixWPBase, relativeURL, quality} from '../imgix';
import SelectInput from "./Form_Fields/SelectInput";
import commonHelper from "../helpers/commonHelper";

class DefaultBanner extends React.Component{
    constructor(props) {
        super(props);
        //console.log(this.props);
        this.state = {
            selectedService: {value:null},
            selectedSubService: {value:null},
            parentsServices: [],
            nestedServices: [],

        }
        this.handleInputChange = this.handleInputChange.bind(this);
    }
    componentDidMount(){
        // console.log((this.props.parentsServices[0]).id);
        // console.log(this.state.nestedServices[]);
        if( typeof this.props.parentsServices != "undefined" && this.props.parentsServices.length){
            this.setState({
                parentsServices: this.props.parentsServices,
                nestedServices: this.props.nestedServices,
                selectedService: this.props.parentsServices[0],
                selectedSubService: (this.props.nestedServices[(this.props.parentsServices[0]).id])?(this.props.nestedServices[(this.props.parentsServices[0]).id])[0]:'',
            });
            this.props.handleInputChange('selectedService', this.props.parentsServices[0])
            this.props.handleInputChange('selectedSubService', (this.props.nestedServices[(this.props.parentsServices[0]).id])?(this.props.nestedServices[(this.props.parentsServices[0]).id])[0]:'')
        }
    }
    createMarkup(htmlString) {
        return {__html: htmlString};
    }
    handleInputChange(name, value){
        this.setState({
            [name]: value
        });
        this.props.handleInputChange(name, value);
    }
    render() {
        let {parentsServices, selectedSubService, selectedService} = this.state;
        return (
            <section className="provider-banner hero py-4" style={{background: "url("+imgixWPBase+relativeURL(this.props.background)+"?fit=crop&w=1349&h=434&auto=format,compress&q="+quality+(this.props.imageBlurBlend ? '&exp=-3.9' : '')+") center/cover no-repeat", minHeight: "300px"}}>
                <div className="container py-5">
                    <h1 className="text-center text-white font-weight-bold">{this.props.title}</h1>
                    { (typeof parentsServices != "undefined" && parentsServices.length) ? (
                        <div className="select-service row mt-5">
                            <div className="col-sm-3 offset-sm-3 text-center">
                                <SelectInput name="selectedService" inputValue={selectedService} label="Service" options={parentsServices} onInputChange={this.handleInputChange}/>
                            </div>
                            {
                                this.state.nestedServices[this.state.selectedService.id]?(
                                    <div className="col-sm-3 text-center mt-3 mt-md-0">
                                        <SelectInput name="selectedSubService" inputValue={selectedSubService} label="Subservice" options={this.state.selectedService.id ? this.state.nestedServices[this.state.selectedService.id]:[]} onInputChange={this.handleInputChange}  validation={{required: 'required'}}/>
                                    </div>
                                ): ''
                            }
                        </div> ) : ''
                    }
                </div>
            </section>
        );
    }
}
DefaultBanner.defaultProps = {
  background: '',
  title: '',
  description: '',
  imageBlurBlend: true,
    nestedServices:[],
    parentsServices:[]
};

export default DefaultBanner;
