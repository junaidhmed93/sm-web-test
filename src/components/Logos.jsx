import React from "react";
import {imgixWPBase, relativeURL, quality} from '../imgix';
import LazyLoad from 'react-lazyload';
import PlaceholderComponent from '../components/Placeholder';

class Logos extends React.Component{
    constructor(props) {
        super(props);
    }
    renderList(){
        return this.props.items.map((item, index) => {
            return(
                <div key={"logo"+index} className={this.props.childClass}>
                    <a href={item.url} target={"_blank"}>
                    <LazyLoad height={75} offset={500} placeholder={<PlaceholderComponent className="img-fluid" isImage={true} height={75} placeholderSrc={imgixWPBase+relativeURL(item.image)}/>}>
                        <img className="img-fluid" src={imgixWPBase+relativeURL(item.image)+"?fit=crop&h=150&auto=format,compress"} alt="" />
                    </LazyLoad>
                    </a>
                </div>
            );
        });
    }

    render() {
        return (
            <div id="featured_logos" className={this.props.parentClass}>
                {this.renderList()}
            </div>
        );
    }
}
Logos.defaultProps = {
  parentClass: '',
  childClass: '',
  items: []
};

export default Logos;
