import React from "react";
import {imgixWPBase, relativeURL, quality, imgixReactBase} from '../imgix';
import LazyLoad from 'react-lazyload';
import PlaceholderComponent from '../components/Placeholder';
import LazyLoadImage from "./LazyLoadImage";

class HowItWorks extends React.Component{
    constructor(props) {
        super(props);
    }
    renderItems(){
		return this.props.items.map((item, index) => {
            return(
                <div key={'hiw'+index} className="card border-0">
                    <LazyLoad height={185} offset={500} placeholder={<PlaceholderComponent placeholderSrc={imgixWPBase+relativeURL(item.image)} isImage={true} height="185" width="260" className="mw-100 mx-auto" />}>
                        <img className="mw-100 mx-auto" src={imgixWPBase+relativeURL(item.image)+"?fit=crop&w=260&h=185&auto=format,compress&q="+quality} width="260" />
                    </LazyLoad>
                    <div className="card-body text-center">
                        <h5 className="h4 font-weight-bold card-title">{item.title}</h5>
                        <p className="card-text">{item.description}</p>
                    </div>
                </div>
            );
        });
	}
    render() {
        return (
           <div className={this.props.parentClass}>
                <div className={this.props.childClass}>
                    <div className="card-deck">
                        {this.renderItems()}
                    </div>
                </div>
            </div>
        );
    }
}
HowItWorks.defaultProps = {
  parentClass: '',
  childClass: '',
  items: [],
  isBotData: false
};

export default HowItWorks;
