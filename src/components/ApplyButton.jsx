import React from "react";
import locationHelper from '../helpers/locationHelper'

class ApplyButton extends React.Component{
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="row">
                <div className="col text-center">
                    <p className="text-center">{locationHelper.translate('CAREERS_PAGE_TITlE_TXT')}</p>
                    <a href="#" className="btn btn-primary btn-lg px-5 rounded-0">{locationHelper.translate('APPLY')}</a>
                </div>
            </div>
    );
    }
}

export default ApplyButton;
