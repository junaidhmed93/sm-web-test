import React from "react";
import ContentTooltip from "./ContentTooltip";
import ContentTooltipImage from "./ContentTooltipImage";
import {imgixWPBase, relativeURL, quality} from '../imgix';

class LandingMidContent extends React.Component{
    constructor(props) {
        super(props);
    }
    createMarkup(htmlString) {
      return {__html: htmlString};
    }
    renderList(){
        return this.props.items.map((item, index) => {
            switch( item.acf_fc_layout ){
                case "left_box_right_image":
                return(
                    <section key={'mid'+index} >
                        <div className="row mb-5 align-items-center">
                            <ContentTooltipImage customClass="order-md-2 stack-adjust--right" imageURL={imgixWPBase+relativeURL(item.image)+"?fit=crop&w=510&h=340&auto=format,compress&q="+quality} />
                            <ContentTooltip customClass="order-md-1 stack-adjust--left" title={item.title} text={item.description} />
                        </div>
                    </section>
                );
                break;
                case "only_box":
                return(
                    <section key={'mid'+index} >
                        <div className="row my-4">
                            <div className="col px-5 px-md-3">
                                <h4 className="font-weight-bold">{item.title}</h4>
                                <div dangerouslySetInnerHTML={this.createMarkup(item.description)}></div>
                            </div>
                        </div>
                    </section>
                );
                break;
                case "right_box_left_image":
                return(
                    <section key={'mid'+index} >
                        <div className="row mb-5 align-items-center">
                            <ContentTooltipImage customClass="stack-adjust--left" imageURL={imgixWPBase+relativeURL(item.image)+"?fit=crop&w=510&h=340&auto=format,compress&q="+quality} />
                            <ContentTooltip customClass="stack-adjust--right" title={item.title} text={item.description} />
                        </div>
                    </section>
                );
                break;
            }
        });
    }
    render() {
        return (
            <div>
                {this.renderList()}
            </div>
        );
    }
}

LandingMidContent.defaultProps = {
    items: []
}
export default LandingMidContent;
