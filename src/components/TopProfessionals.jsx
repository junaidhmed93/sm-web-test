import React from "react";

class TopProfessionals extends React.Component{
    constructor(props) {
        super(props);
        this.renderStars = this.renderStars.bind(this);
    }
    renderStars(count){
       return Array.apply( null, Array( parseInt(count) ) ).map((item,index) => {
            return(
                    <i key={'start'+index} className="fa fa-star text-warning"></i>
                );
        });
    }
    renderItems(){
        return this.props.items.map((item,index) => {
            return(
                <div key={index} className="card bg-light with-img-round">
                    <div className="card-body">
                        <div className="card-img-round top-center"></div>
                        <p className="card-title text-center font-weight-bold">{item.title}</p>
                        <div className="card-text text-center">
                            <p className="text-primary m-0">{item.jobs_completed} jobs completed</p>
                            <p>
                                {this.renderStars(item.rate)}
                            </p>
                        </div>
                        <div className="card-text">
                            <p className="m-0">{item.description}</p>
                        </div>
                    </div>
                </div>
                );
        });
    }

    render() {
        return (
            <div className="row mb-md-4">
                <div className="col px-5 px-md-3">
                    <div className="card-deck">
                        {this.renderItems()}
                    </div>
                </div>
            </div>
        );
    }
}

TopProfessionals.defaultProps = {
    items : []
}

export default TopProfessionals;
