import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import GeneralModal from "./GeneralModal";
import Loader from "./Loader";
import FormFieldsTitle from "./FormFieldsTitle";
import TextFloatInput from "./Form_Fields/TextFloatInput";
import SelectInput from "./Form_Fields/SelectInput";
import CheckRadioBoxInput from "./Form_Fields/CheckRadioBoxInput";
import TextareaInput from "./Form_Fields/TextareaInput";
import SubmitElement from "./Form_Fields/SubmitElement";
import RateInput from "./Form_Fields/RateInput";
import {toggleLoginModal, isValidSection, fetchAllProviders, fetchAllParentServices, fetchServiceProviderLocations, submitAddReview} from "../actions/index";
import commonHelper from "../helpers/commonHelper";
import {imgixReactBase} from "../imgix";
import PhoneInput from "./Form_Fields/PhoneInput";
import locationHelper from '../helpers/locationHelper';

class ReviewForm extends React.Component{
    _isMounted = false;
    constructor(props) {
        super(props);
        //console.log("props.urlParams 123", props);
        this.locationHelperInit = locationHelper.init.bind(this);
        this.locationHelperInit();
        this.state = {
            company: {value:null},
            service: {value: "", label: ""},
            city: {value: "", label: ""},
            title: '',
            rating: '',
            recommend: {'id': 1, value: "yes", label: locationHelper.translate('YES')},
            message: '',
            providerOptions: [],
            serviceOptions: [],
            cityOptions: [],
            showModal: false,
            sucess: false,
            isSubmited: false,
            showUserDetailsModal: false,
        }

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleSubmitAsGuest = this.handleSubmitAsGuest.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.setModal = this.setModal.bind(this);
    }
    componentWillUnmount(){
        this._isMounted = false;
    }
    componentDidMount(){
        this._isMounted = true;
        var companySlug  = typeof commonHelper.urlArgs(2) != "undefined" ? commonHelper.urlArgs(2) : "";
        var selectedCompany = {value:null};
        let {lang} = this.props;
        if( !this.state.providerOptions.length ){
            fetchAllProviders(false, lang).then(res=>{
                if (this._isMounted) {
                    var providers = [];
                    var services = [];
                    var cities = [];
                    var provider = {};
                    res.forEach((item, index) => {
                        provider = {
                            id: item.id,
                            value: item.id,
                            label: item.name,
                            offeredServices: item.offeredServices
                        }
                        if( companySlug != "" && item.url == companySlug ){
                            selectedCompany = provider;
                        }
                        providers.push(provider);
                    });
                    fetchAllParentServices(lang).then(cat => {
                        cat.parentServices.forEach((item, index) => {
                            services.push({id: item.id, value: item.id, label: item.name});
                        });
                    });
                    this.props.cities.forEach((item, index) => {
                        cities.push({id: item.id, value: item.id, label: item.name, code: item.code});
                    });
                    this.setState({
                        providerOptions: providers,
                        serviceOptions: services,
                        cityOptions: cities,
                    });
                    if( typeof selectedCompany.id != "undefined" ){
                        this.handleInputChange('company', selectedCompany);
                    }
                }
            })
        }

        // console.log(this.props.userProfile);
    }
    setModal(visibility){
        this.setState({
            showModal: visibility,
            sucess: false
        });
    }
    handleInputChange(name, value){
        switch(name){
            case 'company':
                var serviceOptions = [];
                value.offeredServices.forEach(item=>{
                    serviceOptions.push({id: item.id, value: item.id, label: item.name});
                })
                this.setState({
                    company: value,
                    service: {value: "", label: ""},
                    serviceOptions: serviceOptions
                });
            break;
            case 'service':
                var cityOptions = [];
                fetchServiceProviderLocations(this.state.company.id, value.id).then(res=>{
                    if(res != null && typeof res == "object"){
                        res.forEach(item=>{
                            cityOptions.push({id: item.id, value: item.id, label: item.name, code: item.code});
                        });
                    }
                    this.setState({
                        service: value,
                        city: {value: "", label: ""},
                        cityOptions: cityOptions
                    });
                })
            break;
            default:
        }
        this.setState({
            [name]: value
        });
    }
    hideModal() {
        this.setState({
            showUserDetailsModal: false,
            showModal: false
        })
    }
    handleSubmit(event) {
        event.preventDefault();
        this.setState({
            isSubmited: true,
        });
        if( isValidSection('review-form') && this.state.rating != ''){
            // Check if signed in
            var data = {};
            if( this.props.signInDetails.customer ){
                var contactInformationModel = {
                    "personName": this.props.userProfile.customerFirstName + ' ' + this.props.userProfile.customerLastName,
                    "personEmail": this.props.userProfile.email,
                    "personPhone": this.props.userProfile.address && this.props.userProfile.address.phoneNumber ? this.props.userProfile.address.phoneNumber : 'Not Available'
                };
                data = {
                    "rating": this.state.rating,
                    "reviewTitle": this.state.title,
                    "reviewText": this.state.message,
                    "moverId": this.state.company.id,
                    "serviceId": this.state.service.id,
                    "serviceLocationId": this.state.city.id,
                    "contactInformationModel": contactInformationModel,
                };
            // Show Modal and Submit
            this.setModal(true);
            //console.log("data", data)
            submitAddReview(this.props.signInDetails.access_token, this.props.signInDetails.customer.id, data).then(res=>{
                // console.log(res);
                if( res.success ){
                    this.setState({
                        sucess: true,
                    });
                } else {

                }
            });

            }else{
                this.setState({
                    showUserDetailsModal: true
                });
                // this.props.toggleLoginModal({visibility:true, guest:false});
            }
            
            
        }
    }
    handleSubmitAsGuest(event) {
        // console.log('zzzzzz');
        event.preventDefault();
        if( isValidSection('user-details-dialog')){
            this.setState({
                showUserDetailsModal:false
            })
            // Check if signed in
            var data = {};

            var contactInformationModel = {
                "personName": this.state.fullname,
                "personEmail": this.state.email,
                "personPhone": this.state.phone ? this.state.phone : 'Not Available'
            };
            data = {
                "rating": this.state.rating,
                "reviewTitle": this.state.title,
                "reviewText": this.state.message,
                "moverId": this.state.company.id,
                "serviceId": this.state.service.id,
                "serviceLocationId": this.state.city.id,
                "contactInformationModel": contactInformationModel,
            };
            // Show Modal and Submit
            this.setModal(true);
            submitAddReview("", 0, data).then(res=>{
                // console.log(res);
                if( res.success ){
                    this.setState({
                        sucess: true,
                    });
                } else {

                }
            });

        }
    }

    render() {
        var recommendOptions = [{'id': 1, value: "yes", label: locationHelper.translate('YES')}, {'id': 2, value: "no", label: locationHelper.translate('NO')}];
        var {company, service, city, rating, recommend, message, title, providerOptions, serviceOptions, cityOptions} = this.state;

        return (
            <main>
            <form id="review-form" onSubmit={this.handleSubmit}>
                <FormFieldsTitle title={locationHelper.translate('COMPANY_TO_RATE')} />
                <div className="row mb-4">
                    <div className="col-6 pr-2 pr-sm-3 mb-3">
                        <SelectInput name="company" inputValue={company} label={locationHelper.translate('SELECT_A_COMPANY')} options={providerOptions} onInputChange={this.handleInputChange} validationClasses="required" />
                    </div>
                    <div className="col-6 pl-2 pl-sm-3 mb-3">
                        <SelectInput name="service" inputValue={service} label={locationHelper.translate('SELECT_A_SERVICE')} options={serviceOptions} onInputChange={this.handleInputChange} validationClasses="required" />
                    </div>
                </div>
                <FormFieldsTitle title={locationHelper.translate('SERVICE_PROVIDED_CITY')} />
                <div className="row mb-4">
                    <div className="col-6 pr-2 pr-sm-3 mb-3">
                        <SelectInput name="city" inputValue={city} label={locationHelper.translate('CITY')} options={cityOptions} onInputChange={this.handleInputChange} validationClasses="required"  />
                    </div>
                </div>
                <FormFieldsTitle title={locationHelper.translate('RATE_SERVICE')} />
                <div className="row mb-4 big-rating-stars">
                    <div className="col">
                        <RateInput ratingDetails={true} name="rating" inputValue={rating} onInputChange={this.handleInputChange} validationClasses="required"/>
                        {((this.state.rating == '') && this.state.isSubmited)?<p className={'clear text-danger d-block'}>{locationHelper.translate('RATE_SERVICE_PROVIDER_TXT')}</p>:''}
                    </div>
                </div>

                <FormFieldsTitle title={locationHelper.translate('RATE_SERVICE_FEEDBACK')} />
                <div className="row">
                    <div className="col-6 pr-2 pr-sm-3 mb-3">
                        <TextFloatInput maxLength="128" InputType="text" name="title" inputValue={title} label={locationHelper.translate('TITLE_REVIEW_FEEDBACK')} onInputChange={this.handleInputChange} validationClasses="required" />
                    </div>
                    <div className="col-12 pr-2 pr-sm-3 mb-3">
                        <TextareaInput maxLength="400" name="message" inputValue={message} placeholder={locationHelper.translate('YOUR_REVIEW')} validationClasses="required" onInputChange={this.handleInputChange} />
                    </div>

                </div>
                <FormFieldsTitle title={locationHelper.translate('RECOMMEND_TO_OTHERS')} />
                <CheckRadioBoxInput InputType="radio" name="recommend" inputValue={recommend} items={recommendOptions} onInputChange={this.handleInputChange}/>
                <SubmitElement />
                { this.state.showModal && (
                    <div data-login-source="" className={"modal modal-customer-new open collapse show"} tabIndex="-1"
                          role="dialog" aria-labelledby="myLargeModalLabel">
                        <div className="socialmodal">
                            <div className="row">
                                <div className="search-loader-container hidden">
                                    <div className="sk-cube-grid">
                                        <div className="sk-cube sk-cube1"></div>
                                        <div className="sk-cube sk-cube2"></div>
                                        <div className="sk-cube sk-cube3"></div>
                                        <div className="sk-cube sk-cube4"></div>
                                        <div className="sk-cube sk-cube5"></div>
                                        <div className="sk-cube sk-cube6"></div>
                                        <div className="sk-cube sk-cube7"></div>
                                        <div className="sk-cube sk-cube8"></div>
                                        <div className="sk-cube sk-cube9"></div>
                                    </div>
                                </div>
                                <div className="col-xs-12 col-md-12">
                                    <div className="socialmodal modal-body">
                                        <div className={"card login-with-email-card "}>
                                            <div className="tab-content">
                                                <button onClick={()=> this.hideModal()} type="button" className="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                                <h4 className="title">{locationHelper.translate('THANK_YOU_REVIEW_MSG')}</h4>
                                                <Loader completed={this.state.sucess} />
                                                {
                                                    this.state.sucess?(
                                                        <h4 className={'text-center'} style={{'fontSize':'20px','margin-top':'-1rem'}}>{locationHelper.translate('REVIEW_SUBMITTED_MSG')}</h4>
                                                    ):''
                                                }

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                ) }

            </form>
                { this.state.showUserDetailsModal &&
                (
                    <form id="user-details-dialog" data-login-source="" className={"modal modal-customer-new open collapse show"} tabIndex="-1"
                          role="dialog" aria-labelledby="myLargeModalLabel">
                        <div className="socialmodal">
                            <div className="row">
                                <div className="search-loader-container hidden">
                                    <div className="sk-cube-grid">
                                        <div className="sk-cube sk-cube1"></div>
                                        <div className="sk-cube sk-cube2"></div>
                                        <div className="sk-cube sk-cube3"></div>
                                        <div className="sk-cube sk-cube4"></div>
                                        <div className="sk-cube sk-cube5"></div>
                                        <div className="sk-cube sk-cube6"></div>
                                        <div className="sk-cube sk-cube7"></div>
                                        <div className="sk-cube sk-cube8"></div>
                                        <div className="sk-cube sk-cube9"></div>
                                    </div>
                                </div>
                                <div className="col-xs-12 col-md-12">
                                    <div className="socialmodal modal-body">
                                        <div className={"card login-with-email-card "}>
                                            <div className="tab-content">
                                                <button onClick={()=> this.hideModal()} type="button" className="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                                <h4 className="title">{locationHelper.translate('THANK_YOU_REVIEW_MSG')}</h4>

                                                {
                                                    this.state.showModal?(
                                                        <main>
                                                            <Loader completed={this.state.sucess} />
                                                            {this.state.sucess?(
                                                                <h4 className={'text-center'} style={{'fontSize':'20px','marginTop':'-1rem'}}>{locationHelper.translate('REVIEW_SUBMITTED_MSG')}</h4>
                                                            ):''}
                                                        </main>
                                                    ):(
                                                        [
                                                            <p className="click-takes text-center">{locationHelper.translate('LEAVE_A_MSG_TXT')}</p>,
                                                            <div action="" id="user-details-dialog" autoComplete="false" className="mth" noValidate="novalidate">
                                                                <div className={"mb-4"}>
                                                                    <TextFloatInput id="fullname" name="fullname" label={locationHelper.translate('FULL_NAME')} inputValue={this.state.fullname} onInputChange={this.handleInputChange} validationClasses="required" />
                                                                </div>
                                                                <div className={"mb-4"}>
                                                                    <PhoneInput name="phone" inputValue={this.state.phone} onInputChange={this.handleInputChange} validation={{required: 'required'}} />
                                                                </div>
                                                                <div className={"mb-4"}>
                                                                    <TextFloatInput InputType="email" id="email" name="email" label={locationHelper.translate('EMAIL')} inputValue={this.state.email} onInputChange={this.handleInputChange} validationClasses="required email" />
                                                                </div>
                                                                <div className="button-socialmodal mb-4">
                                                                    <button onClick={this.handleSubmitAsGuest} id="review-as-guest"><span>{locationHelper.translate('SUBMIT')}</span></button>
                                                                </div>

                                                            </div>
                                                            ]
                                                    )
                                                }

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                )
                }
            </main>

        );
    }
}

function mapStateToProps(state){
    return {
        cities: state.cities,
        mainServices: state.mainServices,
        signInDetails: state.signInDetails,
        userProfile: state.userProfile,
        lang: state.lang,
        currentCity: state.currentCity,
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({toggleLoginModal}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(ReviewForm);
