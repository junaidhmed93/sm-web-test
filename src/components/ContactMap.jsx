import React from "react";
import GoogleMapReact from 'google-map-react';

class ContactMap extends React.Component{
    constructor(props) {
        super(props);
    }
    renderMarkers(map, maps) {
        let marker = new maps.Marker({
            position: {
                lat: 25.073987,
                lng: 55.143331
            },
            map,
            title: 'ServiceMarket'
        });
    }
    render() {
        return (
            <section className="hero">
                {/*<iframe className="d-block" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3613.8228757594848!2d55.14114231503207!3d25.073991842841007!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f6ca6c3a55555%3A0xaf640ee17dbd5d6!2sServiceMarket!5e0!3m2!1sen!2s!4v1530882591838" width="100%" height="430" frameBorder="0" style={{border: "0"}} allowFullScreen=""></iframe>*/}
                <div style={{ height: '430px', width: '100%' ,position: 'relative'}}>
                    <GoogleMapReact
                        bootstrapURLKeys={{ key: 'AIzaSyDwybxbKzLFkyGQgCPR2UrkTWg4nH44t14\n' }}
                        defaultCenter={{
                            lat: 25.073987,
                            lng: 55.143331
                        }}
                        defaultZoom={16}
                        options={(maps)=>{return {
                            panControl: false,
                            zoomControl: false,
                            mapTypeControl: false,
                            scrollwheel: false
                        };}}
                        onGoogleApiLoaded={({map, maps}) => this.renderMarkers(map, maps)}

                    >

                    </GoogleMapReact>
                    <div className="placeDiv">
                        <div className="placecard__container">
                            <div className="placecard__left">
                                <p className="placecard__business-name">ServiceMarket</p>
                                <p className="placecard__info">Reef Tower, Cluster O, Jumeirah Lakes Towers, Dubai - United Arab Emirates</p>
                                <a className="placecard__view-large" target="_blank"
                                   href="https://maps.google.com/maps?ll=25.073987,55.143331&z=16&t=m&hl=en-US&gl=US&mapclient=embed&cid=789890175996777942"
                                   id="A_41">View larger map</a>
                            </div>
                            <div className="placecard__right">
                                <a className="placecard__direction-link" target="_blank"
                                   href="https://maps.google.com/maps?ll=25.073987,55.143331&z=16&t=m&hl=en-US&gl=US&mapclient=embed&daddr=ServiceMarket%20Reef%20Tower%2C%20Cluster%20O%2C%20Jumeirah%20Lakes%20Towers%20Dubai%20United%20Arab%20Emirates@25.073987,55.143331"
                                   id="A_9">
                                    <div className="placecard__direction-icon"></div>
                                    Directions
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default ContactMap;
