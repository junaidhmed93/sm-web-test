import React from "react";
import {imgixWPBase, relativeURL, quality} from '../imgix';
import {getAllCreditCard, registerCreditCard, removeCreditCard} from "../actions";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import Loader from "../components/Loader";
import LazyLoad from 'react-lazyload';
import PlaceholderComponent from '../components/Placeholder';
import {withCookies} from "react-cookie";
import locationHelper from '../helpers/locationHelper';

class CreditCardManagement extends React.Component{
    _isMounted = false;
    constructor(props) {
        super(props);

        this.state = {
            loader: false
        }
        this.registerCreditCardHandler = this.registerCreditCardHandler.bind(this);
        this.deleteCreditCardHandler = this.deleteCreditCardHandler.bind(this);
    }
    componentDidMount() {
        this._isMounted = true;
        this.props.getAllCreditCard(this.props.signInDetails.access_token)
            .then(() => {

                if (this._isMounted) {
                    this.setState({
                        loader: false
                    })
                }
            });
        this.setState({
            loader: true
        })
    }
    componentWillUnmount() {
        this._isMounted = false;
    }
    registerCreditCardHandler() {
        const { cookies } = this.props;
        cookies.set('register_credit_card', "1", {path: '/'});

        var url = window.location.protocol + '//' + window.location.host;
        this.props.registerCreditCard(this.props.signInDetails.access_token, this.props.signInDetails.customer.id, url + "/en/dubai/profile/cancel", url + "/en/dubai/profile/decline", url + "/en/dubai/profile/success")
            .then(() => {
                this.setState({
                    loader: false
                });
                window.location = this.props.myCreditCardsData.redirect_url;
            });
        this.setState({
            loader: true
        })
    }
    renderCreditCardImage(type){
        if (type.toLowerCase() === 'visa credit') {
            return (
                <LazyLoad offset={500} height={150} placeholder={<PlaceholderComponent />}>
                    <img src="/dist/images/new_visa_updated.png"/>
                </LazyLoad>
            );
        }
        return (
            <LazyLoad offset={500} height={150} placeholder={<PlaceholderComponent />}>
                <img src="/dist/images/new_master.png"/>
            </LazyLoad>
        );
    }
    deleteCreditCardHandler(creditCardId){
        removeCreditCard(this.props.signInDetails.access_token, creditCardId)
            .then(() => {
                this.props.getAllCreditCard(this.props.signInDetails.access_token)
                    .then(() => {
                        this.setState({
                            loader: false
                        })
                    });
            })
            .catch(() => {
                this.setState({
                    loader: false
                })
            });
        this.setState({
            loader: true
        })
    }
    renderList() {
        // console.log(this.props.myCreditCardsData.data);
        return this.props.myCreditCardsData.data.map((card, index) => {
            return (
                <div key={index} className="credit-card my-services row mbl">
                    <div className="col-md-3 col-sm-3 col-xs-12 image my-services-left-col">
                        {this.renderCreditCardImage(card.cardPlatform)}
                    </div>
                    <div className="col-md-9 col-sm-9 col-xs-12">
                        <div className="row">
                            <div className="col-md-12 col-sm-12 col-xs-12 mvm">
                                <div className="row">
                                    <div className="col-md-12 col-sm-12 col-xs-12">
                                        <h5 className="card-item-title">{locationHelper.translate('CREDIT_CARDS_MANAGEMENT')}</h5>
                                        <button onClick={() => this.deleteCreditCardHandler(card.id)} className="btn btn-grey creditcard-delete">{locationHelper.translate('DELETE')}</button>
                                        <hr className="clear"/>
                                        {/*<div className="row card-action-buttons pvm mbm">*/}
                                            {/*<div className="col-md-12 col-sm-12 col-xs-12">*/}
                                                {/*<a className="delete-credit-card btn btn-default-blue cancel-event-details">DELETE</a>*/}
                                            {/*</div>*/}
                                        {/*</div>*/}
                                        <div className="card-detail-items">
                                            <div className="card-detail-item pbm pln"><strong>{locationHelper.translate('CARD')} </strong>
                                                <span>{card.cardPlatform}</span></div>
                                            <div className="card-detail-item pbm pln"><strong>{locationHelper.translate('CARD_NUMBER')} </strong>
                                                <span>**** **** **** {card.last4Char}</span></div>
                                            <div className="card-detail-item pbm pln"><strong>{locationHelper.translate('CARD_EXPIRY')} </strong>
                                                <span>{card.expiryDate}</span></div>
                                        </div>
                                        <div className="after-border hidden"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        });
    }
    render() {
        if (!this.state.loader) {
            if (this.props.myCreditCardsData.data && this.props.myCreditCardsData.data.length > 0) {
                return (
                    <div className="col-lg-9 col-md-9 col-sm-12 col-xs-12 user-right-con cc-management">
                        <div className="card container-fluid mb-3 shadow-sm">
                            {this.renderList()}
                        </div>
                    </div>
                );
            } else {
                return (
                    <div className="col-lg-9 col-md-9 col-sm-12 col-xs-12 user-right-con">
                        <div className="card container-fluid mb-3 shadow-sm">
                            <p>
                                {locationHelper.translate('NO_CARD_ADDED')}
                            </p>
                            <p>
                                {locationHelper.translate('NO_CARD_ADDED_HELP_TXT')}
                            </p>
                            <button onClick={() => this.registerCreditCardHandler()} className="btn btn-warning btn-lg font-weight-bold px-4">{locationHelper.translate('ADD_A_CARD')}</button>
                        </div>
                    </div>
                );
            }
        } else {
            return (
                <main className={"col-lg-9 col-md-9 col-sm-12 col-xs-12 user-right-con " + (!this.state.loader ? "show": "hide")}><Loader/></main>
            );
        }

    }
}


function mapStateToProps(state) {
    return {
        signInDetails: state.signInDetails,
        myCreditCardsData: state.myCreditCardsData
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({getAllCreditCard, registerCreditCard}, dispatch);
}

export default withCookies(connect(mapStateToProps, mapDispatchToProps)(CreditCardManagement));
