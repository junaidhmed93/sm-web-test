import React from "react";
import {imgixReactBase, imgixWPBase, relativeURL} from '../imgix';
import LazyLoad from 'react-lazyload';
import PlaceholderComponent from '../components/Placeholder';
import LazyLoadImage from "./LazyLoadImage";

class WhyServiceMarket extends React.Component{
    constructor(props) {
        super(props);
    }
    renderItems(){
		return this.props.items.map((item, index) => {
            return(
                <div key={"wsm"+index} className="card">
                    <div className="card-body d-flex flex-column align-items-center justify-content-center text-center">
                        <LazyLoad offset={500} height={48} placeholder={<PlaceholderComponent width={48} placeholderSrc={imgixWPBase+relativeURL(item.image)} isImage={true} height="48" className="mb-3" />}>
                            <img src={imgixWPBase+relativeURL(item.image)+"?fit=crop&h=48&auto=format,compress"} height="48" className="mb-3" />
                        </LazyLoad>
                        <h5 className="h4 font-weight-bold card-title">{item.title}</h5>
                        <p className="card-text">{item.description}</p>
                    </div>
                </div>
            );
        });
	}
    render() {
        return (
           <div className={this.props.parentClass}>
                <div className={this.props.childClass}>
                    <div className="card-group">
                        {this.renderItems()}
                    </div>
                </div>
            </div>
        );
    }
}
WhyServiceMarket.defaultProps = {
  parentClass: '',
  childClass: '',
  items: [],
    isBotData: false
};

export default WhyServiceMarket;
