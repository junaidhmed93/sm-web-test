import React from "react";

class Nav extends React.Component{
    constructor(props) {
        super(props);
        this.setClass = this.setClass.bind(this);
    }
    setNode(label,index){
        return(
            <li className={"step col-auto text-center pointer "+this.setClass(index)} onClick={()=> this.props.jumpTo(index)} >
                <span className="step-icon"></span>
                <p className="step-label h6 m-0 text-nowrap">{label}</p>
            </li>
        )
    }
    setClass(index){
        var {currentStep} = this.props;
        if( currentStep == index ){
            return 'active';
        }
        if( currentStep > index ){
            return 'complete';
        }
    }
    render() {
        var {steps, serviceTitle} = this.props;
        var Line = <li className="step-lines col"></li>;
        var index = 1;
        return (
            <nav className="steps bg-white pb-4 pt-3 border-bottom sticky hide-on-focus">
                <div className="container">
                    <ul className="steps px-4 list-unstyled m-0 d-flex align-items-center justify-content-between">
                        {this.setNode(serviceTitle+' details', index++)}
                        {Line}
                        { steps == 4 ? this.setNode('Select companies', index++) : ''}
                        { steps == 4 ? Line : '' }
                        {this.setNode('Contact details', index++)}
                        {Line}
                        {this.setNode('Payment', index++)}
                    </ul>
                </div>
            </nav>
        );
    }
}

Nav.defaultProps = {
    steps: 4,
    serviceTitle: ''
};
export default Nav;
