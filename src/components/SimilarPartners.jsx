import React from "react";
import StarRating from "../components/StarRating";
import { Link } from "react-router-dom";


class SimilarPartners extends React.Component {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.state = {
      partners: props.partners
    };

  }

  componentDidMount() {


  }
  render() {

    return (
      <div>
        <div className="row mb-5">
          <div className='d-inline-block font-weight-bold mx-2'>
            <i className="fa fa-building"></i>
            <span className="d-inline-block font-weight-bold mx-2">Similar Companies </span>
          </div>
        </div>
        {(this.props.partners && this.props.partners.length) ? this.props.partners.map((p) => {
          const rate = !isNaN(p.rating) && !isNaN(p.numberOfReviews) ? Number((p.rating / p.numberOfReviews).toFixed(2)) : p.StarRating;

          return (
            <div className="row mb-3  partners clear" style={{ marginRight: '-80px' }}>
              <div className="col25">
                <div className="related-provider-img">
                  <img src={p.logo} alt="Gulf Agency Company (Kuwait) Ltd" className="service-logo" />
                </div>
              </div>
              <div className="col75">
                <div className="row" >
                  <p className="h4 color-black" >
                    {p.url && p.url != '' ? (<a className='color-black mouse-pointer' onClick={() => this.props.changeCompany(p.url)} >{p.serviceProviderName}</a>) : p.serviceProviderName}
                  </p>
                </div>
                <div className="row" style={{ paddingLeft: '0 !important' }}>
                  <div className="col-auto" >
                    <StarRating rating={isNaN(rate) ? 'NaN' : rate} />
                  </div>
                  <p className="h4 m-0 text-primary" style={{ paddingRight: '15px' }}>{(rate && rate != 'NaN') ? (rate + '/5') : 'No rating available'}</p>
                  <div className="col-auto text-left">
                    {/* <p className="h3 m-0 text-primary">{rate}/5</p> */}
                  </div><p className="m-0">{p.numberOfReviews} Reviews</p>
                </div>
              </div>
              <span className="seperator"></span>
            </div>)
        }) :
          <p className="row mb-3">No companies available</p>
        }
      </div>
    );
  }
}
export default SimilarPartners;
