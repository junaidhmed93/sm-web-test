import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {fetchServices, toggleMainMenu} from "../actions/index";
import { Link } from "react-router-dom";
import {imgixReactBase, imgixWPBase, quality, relativeURL} from '../imgix';
import {toggleLoginModal, toggleSignInForm} from "../actions";
import LazyLoad from 'react-lazyload';
import PlaceholderComponent from '../components/Placeholder';
import commonHelper from "../helpers/commonHelper";
import locationHelper from "../helpers/locationHelper";
import { DEFAULT_LANG, showLangNav } from "../actions/index";

class PopupMenu extends React.Component{
    constructor(props) {
		super(props);
		this.locationHelperInit = locationHelper.init.bind(this);
        this.locationHelperInit();
		this.renderList = this.renderList.bind(this);
		this.renderLinks = this.renderLinks.bind(this);
		this.hideMenu = this.hideMenu.bind(this);
        this.showLoginModal = this.showLoginModal.bind(this);
		this.showChildMenu = this.showChildMenu.bind(this);
	}
	componentDidMount(){
        if ( !this.props.mainServices.length ) {
            this.props.fetchServices(this.props.currentCity, this.props.lang);
		}
	}

	hideMenu(){
		this.props.toggleMainMenu(false);
        document.body.classList.remove('prevent-scroll-mobile');
	}

    showLoginModal(){
        this.props.toggleLoginModal({visibility:true, guest:false});
    }
	showChildMenu(event){
		let allCollapseElements = document.getElementById('accordionExample').querySelectorAll('.collapse.show');
		let currentCollapseElement = event.target.parentNode.querySelector('.collapse');
		let hasShowClass = currentCollapseElement.className.indexOf('show') != -1;
		// Close all tabs first
		allCollapseElements.forEach((item)=> {
			item.className = item.className.replace(' show', '');
		});
		// Process current tab
		if( hasShowClass ){
			currentCollapseElement.className = currentCollapseElement.className.replace(' show', '');
		}else{
			currentCollapseElement.className += ' show';
		}
	}
	renderList(){
		return this.props.mainServices.map((links, index)=> {
            var label = '';
            if(links.length) {
                for (var i = 0; i < links[0].header_category.length; i++) {
                    if (links[0].header_category[i].value == commonHelper.categoriesOrder()[index]) {
                        label = links[0].header_category[i].label;
                    }
                }
            }
			if(links.length){
				return (
					<div key={"category"+index} className="card rounded-0">
						<button className="btn btn-link text-dark py-3 py-md-0 px-4 collapsed no-underline dropdown-toggle d-flex align-items-center justify-content-between" type="button" data-toggle="collapse" aria-expanded="false" onClick={this.showChildMenu} >
						{label}
						</button>
						<div className="collapse">
							<div className="card-body rounded-0 p-0">
								<div className="list-group rounded-0">
									{this.renderLinks(links)}
								</div>
							</div>
						</div>
					</div>
				)
			}
		});
	}
	renderLinks(links){
		const {currentCity,lang} = this.props;
		return links.map((link, index)=> {
			return (
				<Link onClick={()=> this.hideMenu()} key={"link"+index} to={"/"+lang+"/"+currentCity+"/"+link.url_slug} className="list-group-item list-group-item-action rounded-0 py-2 px-5 border-left-0 border-right-0">{link.service_preview_title}</Link>
			)
		});
	}
	langSwitch(){
		const { currentCity, lang, langSwitchVisibility, location } = this.props;
		let pathname = location.pathname;
		let langVisibility =  showLangNav(currentCity, commonHelper.urlArgs("", pathname));
		let switchURL = lang == DEFAULT_LANG ?  pathname.replace("/"+DEFAULT_LANG+"/", "/ar/") : pathname.replace("/ar/", "/"+DEFAULT_LANG+"/");
		return langVisibility ?
		(<div className="card rounded-0 d-block d-lg-none d-sm-none d-md-none ">
			<div className="row">
				<div className="col">
					<a href={switchURL} className="ttext-dark btn btn-link text-dark py-3 d-block py-md-0 px-4 clear">
						<span className="float-left">
							<i className="fa fa-globe"></i> {locationHelper.translate("LANGUAGE")}
						</span>
						<span className="float-right">
							 {locationHelper.translate("LANG_SWITCH")}
						</span>
					</a>
				</div>
			</div>
		</div>) : '';
	}
    render() {
		return (
			<React.Fragment>
			{this.props.showMainMenu ? (
				<div onClick={()=> this.hideMenu()} className="modal-backdrop show" style={{zIndex: '1020'}}></div>
			) : ''}
			<div className={"drawer fixed-top bg-white h-100 collapse"+ (this.props.showMainMenu ? ' show': '')} id="drawer" role="navigation">
				<div className="triangle"></div>
                <a onClick={()=> this.hideMenu()}  className="menu-close close hidden-xs" href="javascript:void(0)">
                    <span className="text-white">×</span>
				</a>
				<div className="d-block d-lg-flex" >
                        <a onClick={()=> {this.showLoginModal();this.hideMenu();}} className={(this.props.signInDetails.customer?'':'show')+' btn btn-block btn-link text-dark text-left py-3 px-4 border border-bottom-0 rounded-0 no-underline d-lg-none align-items-center justify-content-between collapse'} role="button" href="javascript:void(0)"  data-toggle="collapse" data-target="#drawer1">
                            <strong>Login</strong>
                        </a>
                        <Link  onClick={()=> this.hideMenu()} to="/en/profile" className={(this.props.signInDetails.customer?'show':'')+' btn btn-block btn-link text-dark text-left py-3 px-4 border border-bottom-0 rounded-0 no-underline d-lg-none align-items-center justify-content-between collapse'}>{this.props.signInDetails.customer?this.props.signInDetails.customer.firstName:''
                        }</Link>
					<div className="accordion rounded-0" id="accordionExample">
					{this.renderList()}
					{this.langSwitch()}
					<div className="card rounded-0 py-3 px-4">
						<div className="row">
							<div className="col pr-2">
								<a href="https://servicemarket.onelink.me/R9tZ/47e24049">
									<LazyLoad height={37} offset={500} placeholder={<PlaceholderComponent />}>
										<img src={imgixReactBase + "/dist/images/apple-itunes.png?fit=crop&w=123&h=37&auto=format,compress"} className="mw-100" alt="" />
									</LazyLoad>
								</a>
							</div>
							<div className="col pl-2">
								<a href="https://servicemarket.onelink.me/R9tZ/2bf82707">
									<LazyLoad height={37} offset={500} placeholder={<PlaceholderComponent />}>
										<img src={imgixReactBase + "/dist/images/google-play.png?fit=crop&w=123&h=37&auto=format,compress"} className="mw-100" alt="" />
									</LazyLoad>
								</a>
							</div>
						</div>
					</div>
					</div>
					<div className="flex-fill py-5 px-2 d-none d-md-block popup-apps">
						{/*<div className="placeholder bg-primary rect rounded"></div>*/}
						{/*{ typeof this.props.popupData.acf != "undefined" && <a href={this.props.popupData.acf.header_offer_url}><img src={imgixWPBase+relativeURL(this.props.popupData.acf.header_offer_image)+"?fit=crop&h=183&auto=format,compress"}/></a> }*/}
						<p className="download-app">{locationHelper.translate("DOWNLOAD_OUR_APP")}</p>
                        <div className="row">
                            <div className="col pr-4">
                                <a href="https://servicemarket.onelink.me/R9tZ/47e24049">
									<img src={imgixReactBase + "/dist/images/apple-itunes.png?fit=crop&w=123&h=37&auto=format,compress"} className="mw-100" alt="" />
                                </a>
                            </div>
                            <div className="col pl-4">
                                <a href="https://servicemarket.onelink.me/R9tZ/2bf82707">
									<img src={imgixReactBase + "/dist/images/google-play.png?fit=crop&w=123&h=37&auto=format,compress"} className="mw-100" alt="" />
                                </a>
                            </div>
                        </div>
					</div>
				</div>
			</div>
			</React.Fragment>
        );
    }
}


function mapStateToProps(state){
	return {
		currentCity: state.currentCity,
		mainServices: state.mainServices,
		showMainMenu: state.showMainMenu,
        signInDetails: state.signInDetails,
		popupData: state.popupData,
		lang: state.lang,
		langSwitchVisibility : state.langSwitchVisibility
	}
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({fetchServices, toggleMainMenu, toggleLoginModal, toggleSignInForm}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(PopupMenu);
