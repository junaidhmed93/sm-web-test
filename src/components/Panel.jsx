import React from "react";
import ReactDOM from "react-dom";
import {connect} from "react-redux";
import locationHelper from "../helpers/locationHelper";
import { LANG_AR } from "../actions";
class Panel extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            height: 0
        }
        this.locationHelperInit = locationHelper.init.bind(this);
        this.locationHelperInit();
        this.setHeight = this.setHeight.bind(this);
        this.createMarkup = this.createMarkup.bind(this);
    }
    createMarkup(htmlString) {
      return {__html: htmlString, sanitize : true};
    }
    componentDidMount(){
        // Set Initial Height For Collapsed Content
        var height = ReactDOM.findDOMNode(this).querySelector('.collapsing').scrollHeight;
        this.setState({
            height
        });
        
    }
    setHeight(index){
        // Set Height For Collapsed Content and Change active panel
        if( this.props.activeTab == index){
            this.setState({
                height: 0
            });
            this.props.panelClicked(-1);
        }else{
            var height = ReactDOM.findDOMNode(this).querySelector('.collapsing').scrollHeight;
            this.setState({
                height
            });
            this.props.panelClicked(index);
        }
    }
    render() {
        var {title, index, activeTab, simplePanel, lang} = this.props;
        var contentHeight = activeTab == index ? this.state.height : 0;
        var activeTabClass = activeTab == index ? " active-tab" : "";
        let arrowClass = lang == LANG_AR ? "pull-left" : "pull-right"
        return (
            <div className={simplePanel ? "card border-0"+activeTabClass : "accordion-panel"+activeTabClass}>
                {simplePanel ? (
                    <div className="card-header p-0">
                        <h5 className="mb-0">
                            <button className="btn btn-block text-left bg-white text-xs m-0" type="button" data-toggle="collapse" onClick={()=> this.setHeight(index)} aria-expanded={activeTab == index} >
                                {title}
                                <span ariashow="false" className={"fa fa-angle-down mt-1 "+arrowClass}></span>
                                <span ariashow="true" className={"fa fa-angle-up mt-1 "+arrowClass}></span>
                            </button>
                        </h5>
                    </div>
                ) : (
                    <div className="card-header bg-white px-0">
                        <h5 className="mb-0">
                            <button className="btn btn-link no-underline px-0 w-100 d-flex align-items-center justify-content-between" type="button" data-toggle="collapse" onClick={()=> this.setHeight(index)} aria-expanded={activeTab == index} >
                                <strong className="mr-2 text-left">{title}</strong>
                                <span ariashow="false" className="fa fa-plus"></span>
                                <span ariashow="true" className="fa fa-minus"></span>
                            </button>
                        </h5>
                    </div>
                )}

                <div className="collapsing" style={{height: contentHeight+'px'}}>
                    <div className={"card-body" + (simplePanel ? ' text-xs' : ' px-0')} dangerouslySetInnerHTML={this.createMarkup(this.props.children)} >
                        
                    </div>
                </div>
            </div>
        );
    }
}
function mapStateToProps(state){
    return {
        currentCity: state.currentCity,
        lang: state.lang
    }
}
export default connect(mapStateToProps)(Panel);
