import React from "react";

class PlansLabelIcon extends React.Component{
    constructor(props) {
        super(props);
    }

    renderList(){
        return this.props.items.map((item) => {
            return(
                <p key={item.title} className="d-flex align-items-center justify-content-between my-3">
                    <span>{item.title}</span>
                    <span className={item.color}>
                        {item.icons.map((icon) => {
                            return(
                            <i key={item.title+"icon"} className={icon}></i>
                            );
                        })}
                    </span>
                </p>
            );
        });
    }
    render() {
        return (
            <div>
                {this.renderList()}
            </div>
        );
    }
}


export default PlansLabelIcon;
