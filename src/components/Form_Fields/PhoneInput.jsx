import React from "react";
import ReactTelInput from "react-telephone-input";
import { updateSingleInputValid, LANG_AR} from "../../actions/index";
import { getCountryCodeByCity } from '../../utils/commonUtils';
import { connect } from "react-redux";
import TextFloatInput from "./TextFloatInput";
import locationHelper from "../../helpers/locationHelper";
class PhoneInput extends React.Component {
    constructor(props) {
        super(props);
        this.locationHelperInit = locationHelper.init.bind(this);
        this.locationHelperInit();
        this.state = {
            city: props.city ? props.city : '',
        }
        this.refInput = React.createRef();
        this.focusTextInput = this.focusTextInput.bind(this);
        this.onChangeHandler = this.onChangeHandler.bind(this);
        this.setDefaultCountry = this.setDefaultCountry.bind(this);
        this.onChangePhoneHandler = this.onChangePhoneHandler.bind(this);
    }

    focusTextInput() {
        this.refInput.current.focus();
    }
    onChangePhoneHandler(Elename,telNumber) {
        const { onInputChange,name, lang} = this.props;
        //console.log(this.node.__domNode.querySelector('input'));
        onInputChange(name, telNumber);
    }
    onChangeHandler(telNumber, selectedCountry) {
        const { lang } = this.props;
        if(lang != LANG_AR){
            updateSingleInputValid(this.node.__domNode.querySelector('input'), lang);
        }
        const { onInputChange, name } = this.props;
        onInputChange(name, telNumber);
    }
    setDefaultCountry() {
        const { city } = this.props;
        let code = 'ae'
        if (!!city) {
            const country = getCountryCodeByCity(city);
            console.log('code', code);
            (!!country && !!country.code)
            code = country.code;
        }
        return code;
    }
    setDefaultDialCode() {

        const { city } = this.props;
        let code = '+971'
        if (!!city) {
            const country = getCountryCodeByCity(city);
            //console.log('code', code);
            (!!country && !!country.dialCode)
            code = country.dialCode;
        }
        return code;
    }
    render() {
        var { inputValue, name, city, currentCityData, lang} = this.props;
        var autoFormat = false;

        let val = inputValue;
        let code = (currentCityData.length && typeof currentCityData[0].cityDto.countryDto) ? (currentCityData[0].cityDto.countryDto.countryCode).toLowerCase() : 'ae';
        code == 'SA' ? 'ksa' : code.toLowerCase();
        let obj = '';
        //countryCode
        if (!!city) {
            obj = getCountryCodeByCity(city);
            code = obj.code;

            if (inputValue == null || inputValue == '')
                val = obj.dialCode;
        }
        
        if(lang == LANG_AR){
            return (<TextFloatInput 
                type="text" 
                name={name} 
                inputValue={val} 
                onInputChange={this.onChangePhoneHandler} 
                label= {locationHelper.translate("PHONE")}
                parseAR = {true}
                validationClasses = "required ph-valid ltr text-left"
                //className="required btn-radio form-control phone-control-input border input-placeholder checkpoint required phone min9"
                />);
        }else{
            return (
                <ReactTelInput
                    autoFormat={autoFormat}
                    ref={node => { this.node = node; }}
                    value={val}
                    classNames="input-group phone-control"
                    defaultCountry={code}
                    flagsImagePath='/dist/images/flags.png'
                    onChange={this.onChangeHandler}
                    inputProps={{ className: "btn-radio form-control phone-control-input border input-placeholder checkpoint required phone min9" }}
                />
            );
        }
    }
}
function mapStateToProps(state) {
    return {
        currentCity: state.currentCity,
        lang: state.lang,
        currentCityData:state.currentCityData
    }
}
export default connect(mapStateToProps)(PhoneInput);