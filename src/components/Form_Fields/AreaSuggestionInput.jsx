import React from "react";
import {updateSingleInputValid} from "../../actions/index";
import { connect } from "react-redux";
import locationHelper from "../../helpers/locationHelper";
class AreaSuggestionInput extends React.Component{
    constructor(props) {
        super(props);
        var itemSelectedFromList = ((typeof props.inputValue == "object"  && props.inputValue != null) && typeof props.inputValue.title != "undefined") ? true : false;
        //var itemSelectedFromList = (typeof props.inputValue == "object" && typeof props.inputValue.title != "undefined") ? true : false;
        
        this.state = {
            term:'', results: [],
            showDropdown: false,
            selectedResult: 0,
            itemSelectedFromList: itemSelectedFromList
        };
        this.refInput = React.createRef();
        this.focusTextInput = this.focusTextInput.bind(this);
        this.onChangeHandler = this.onChangeHandler.bind(this);
        this.searchArea = this.searchArea.bind(this);
        this.visiblity = this.visiblity.bind(this);
        this.renderAreas = this.renderAreas.bind(this);
        this.containerClicked = this.containerClicked.bind(this);
        this.handleOutsideClick = this.handleOutsideClick.bind(this);
        this.setArea = this.setArea.bind(this);
        this.onBlurOut = this.onBlurOut.bind(this);
        this.locationHelperInit = locationHelper.init.bind(this);
        this.locationHelperInit();

    }
    componentWillReceiveProps (newProps) {
        if( newProps.inputValue !== this.props.inputValue ) {
            var itemSelectedFromList = ( newProps.inputValue != null && (typeof newProps.inputValue == "object" && typeof newProps.inputValue.title != "undefined") ) ? true : false;
            this.setState({
                itemSelectedFromList: itemSelectedFromList
            })
        }
    }
    componentWillUnmount() {
        document.removeEventListener('click', this.handleOutsideClick, false);
    }
    containerClicked(e){
        if( !this.state.showDropdown ){
            document.addEventListener('click', this.handleOutsideClick, false);
        }else{
            document.removeEventListener('click', this.handleOutsideClick, false);
        }
    }
    handleOutsideClick(e){
        if( this.node.contains(e.target) ){
            return
        }
        // To ensure that one Select component is listen to the event
        document.removeEventListener('click', this.handleOutsideClick, false);
        this.setState({
            showDropdown: false
        });
    }
    focusTextInput(){
        this.refInput.focus();
    }

    onChangeHandler(event){
       const {onInputChange} = this.props;
       onInputChange(event.target.name, event.target.value);
    }
    visiblity(){
        const {results, term, showDropdown} = this.state;
        return (results.length && term.length) && showDropdown;
    }
    showAreasList(){

    }
    searchArea(event){
        const {inputValue, allowNonArea, name, onInputChange, lang} = this.props;
        
        //console.log("this.refs.myInput",  this.refInput);

        updateSingleInputValid(this.refInput, lang);

        var areas = this.props.areas;

        let term = (event.target.value).toLowerCase();

        var matchedAreas = [];

        if( typeof areas == "object" && ( areas != null && areas.length )){

            matchedAreas = areas.filter(function(item){
                //console.log((item.title).match(/al/i))
                return ((item.title).toLowerCase().indexOf(term) != -1)
            });

            matchedAreas.sort(function(a, b){
                if(a.title < b.title) return -1;
                if(a.title > b.title) return 1;
                return 0;
            })

            matchedAreas =  matchedAreas.slice(0,9);
        }

        this.setState({
            results: matchedAreas,
            term: term,
            showDropdown: true
        })

        if(matchedAreas.length){
            onInputChange(name, term);
        }else{
            onInputChange(event.target.name, event.target.value);
            /*if(allowNonArea){
                onInputChange(event.target.name, event.target.value);
            }*/
        }
        /*this.onBlurOut(event.target.value)*/
    }
    setArea(item){
        const {name, onInputChange, allowNonArea, userAreaField, lang} = this.props;

        updateSingleInputValid(this.refInput, lang);
        //onInputChange(event.target.name, event.target.value);
        this.setState({
            results: [],
            term: item.title
        })
        //console.log(item)
        
        var return_data = "";
        
        if(allowNonArea && !userAreaField){
            return_data = item.title
        }else{
            return_data = item;
        }
        onInputChange(name,return_data);
        //console.log();
        this.refInput.value = item.title;

        this.onBlurOut(this.refInput.value)

    }
    renderAreas(){
        const {currentCity} = this.props;
        let limit = 10;
        {/*ref={'result'+(index+1)}*/}
        return this.state.results.map((item, index) => {
            return(
                <a key={'result'+index} onClick={() => this.setArea(item)} className="dropdown-item" href="javascript:void(0)">{item.title}</a>
            );
        });
    }
    onBlurOut(inputValue) {
        var {allowNonArea, areas} = this.props;
        var itemSelectedFromList = false;
        var matchedAreas = [];
        if(!allowNonArea && (inputValue != "")){
            matchedAreas = areas && areas.filter(function(item){
                return item.title == inputValue
            });
        }
        if(matchedAreas.length){
            itemSelectedFromList = true;
        }else{
            itemSelectedFromList = false;
        }
        this.setState({
            itemSelectedFromList: itemSelectedFromList
        })
    }
    render() {
        var {inputValue, name, InputType, validationClasses, label, allowNonArea, areas} = this.props;
        var {itemSelectedFromList} = this.state;
        var showNonAreaError = (!allowNonArea && !itemSelectedFromList);
        var areaVal = "";
        if( inputValue != "" && inputValue != null ){
            //console.log("areaVal", inputValue);
            areaVal = typeof inputValue.title != "undefined" ? inputValue.title : inputValue;
            //areaVal = ( !allowNonArea && typeof inputValue.title != "undefined" ) ? inputValue.title : inputValue;
            //areaVal = inputValue;
        } 
        let dateValidationMsg = typeof this.props.validationMessage == "undefined" ? locationHelper.translate("THIS_FIELD_IS_REQUIRED") : this.props.validationMessage;
        //console.log(this.refInput);
        return (
            <div className={"floating-label " + (inputValue ? ' active' : '')}  ref={node => { this.node = node; }} onClick={(e)=> this.containerClicked(e)}>
                <input 
                autoComplete="off"
                type={InputType} 
                name={name}
                ref={(input) => { this.refInput = input; }}
                onChange={this.searchArea}
                value={ areaVal }
                data-validationmsg={ typeof this.props.validationMessage != "undefined" ? this.props.validationMessage : dateValidationMsg }
                className={"btn-radio form-control p-3 border rounded"  + (validationClasses.length ? ' checkpoint '+ validationClasses : '') + (showNonAreaError ? ' fromlist-only': '')}
                />
                <div className={"dropdown-menu shadow w-100"+(this.visiblity() ? ' show': '')} aria-labelledby="dropdownMenuButton">
                    {this.renderAreas()}
                </div>
                <i className="fa fa-check text-success input-valid"></i>
                <label onClick={this.focusTextInput}>{label}</label>
                <span className="error-msg"></span>
            </div>
        );
    }
}

AreaSuggestionInput.defaultProps = {
    allowNonArea: false,
    validationClasses: '',
    userAreaField: false
};

function mapStateToProps(state) {
    return {
        currentCity: state.currentCity,
        lang: state.lang
    }
}
export default connect(mapStateToProps)(AreaSuggestionInput);
