import React from "react";
import {connect} from "react-redux";
import { isMobile, DEFAULT_LANG } from "../../actions";
import locationHelper from "../../helpers/locationHelper";

class QuotesNextStep extends React.Component{
    constructor(props) {
        super(props);
        this.howToProceed = this.howToProceed.bind(this);
        this.locationHelperInit = locationHelper.init.bind(this);
        this.locationHelperInit();
    }
    howToProceed(isSkipCompany = true){
        
        if(isSkipCompany){
            this.props.howToProceed(isSkipCompany);
        }else{
            this.props.howToProceed(isSkipCompany);
        }
    }
    render() {
        const {formCurrentStep, lang} = this.props;
        
        var countinueBtnCls = this.props.disabled ? " grey-button" : "";
        return (
            <footer className="footer-buttons fixed-bottom container-fluid bg-white shadow-1">
                {formCurrentStep == '1' ? (
                <div className="row btn-fixed">
                    <div className="col">
                        <div className={lang == DEFAULT_LANG ? "row d-none d-md-flex d-lg-flex" : "row d-none"} >
                            <div className="col-6 px-0 d-xs-flex text-right">
                                <button className="login-step-button btn-how-proceed btn btn-inline-block btn-warning py-2 text-uppercase font-weight-bold skip-company-selection" onClick={ () => this.howToProceed()}>
                                    <span className="text-uppercase">{locationHelper.translate("LET_SERVICEMARKET_SELECT_COMPANIES_FOR_ME")}</span>
                                </button>
                            </div>
                            <div className="col-6 px-0 d-xs-flex">
                                <button className="grey-button btn-how-proceed btn btn-inline-block btn-warning py-2 text-uppercase font-weight-bold" onClick={() => this.howToProceed(false)}>
                                    <span className="text-uppercase">{locationHelper.translate("I_WANT_TO_SELECT_MY_COMPANIES")}</span>
                                </button>
                            </div>
                        </div>
                        <div className={lang == DEFAULT_LANG ? "row d-md-none d-lg-none": "row"}>
                            <div className={lang == DEFAULT_LANG ? "col px-0 d-xs-flex text-right" : "col text-center px-0"}>
                                <button className="login-step-button btn-how-proceed btn btn-inline-block btn-warning py-2 text-uppercase font-weight-bold skip-company-selection" onClick={() => this.howToProceed()}>
                                    <span className="text-uppercase">{locationHelper.translate("CONTINUE")}</span>
                                    <span className="d-block"><small>{locationHelper.translate("NEXT_STEP_CONTACT_DETAILS")}</small></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                ) : ''}
                {formCurrentStep == '2' ? (
                <div className="row btn-fixed">
                    <div className="col">
                        <div className="row">
                            <div className="col-12 px-0 d-xs-flex text-center">
                                <button className="login-step-button orange-btn-content btn btn-inline-block btn-warning py-2 text-uppercase font-weight-bold" onClick={()=> this.props.moveNext('3')}>
                                    <span className="text-uppercase">{locationHelper.translate("GET_YOUR_FREE_QUOTES")}</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                ) : ''}
                
                {formCurrentStep == '3' ? (
                <div className="row btn-fixed">
                    <div className="col">
                        <div className="row">
                            <div className="col-12 px-0 d-xs-flex text-center">
                                <button className={"login-step-button orange-btn-content btn btn-inline-block btn-warning py-2 text-uppercase font-weight-bold" +countinueBtnCls} disabled={this.props.disabled} onClick={()=> this.props.submitData()}>
                                    <span className={isMobile() ? "text-uppercase h3" : "text-uppercase"}>{locationHelper.translate("SUBMIT")}</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                ) : ''}
            </footer>
        );
    }
}
/*
 <div className="row pb-5 d-none">
 <div className="col mx-auto text-center">
 <button className="btn btn-block btn-warning py-2 text-uppercase font-weight-bold d-none" onClick={()=> this.props.moveNext(this.props.toStep)}>
 <span className="text-uppercase">Get your free quote</span>
 </button>
 </div>
 </div>
 <div className="row pb-5 d-none">
 <div className="col mx-auto text-center">
 <button className="btn btn-block btn-warning py-2 text-uppercase font-weight-bold" onClick={()=> this.props.moveNext(this.props.toStep)}>
 <span className="text-uppercase">Submit</span>
 </button>
 </div>
 </div>
 */
QuotesNextStep.defaultProps = {
    disabled: false
};
function mapStateToProps(state){
    return {
        currentCity: state.currentCity,
        lang: state.lang
    }
}
export default connect(mapStateToProps)(QuotesNextStep);
