import React from "react";

class CheckRadioBoxInputWithPrice extends React.Component{
    constructor(props) {
        super(props);
        this.renderList = this.renderList.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.isChecked = this.isChecked.bind(this);
        this.isValid = this.isValid.bind(this);

    }
    handleChange(event, item){
        var {InputType, name, onInputChange, inputValue} = this.props;

        if( InputType === 'radio' ){ // If Radio Input then update the value
            onInputChange(name, item);
        }else{ // If Checkbox group
            if(event.target.checked){ // Add checkbox value to the Array and call to update
                inputValue.push(item);
                onInputChange(name, inputValue);
            }else{ // Remove checkbox value from the Array and call to update
                inputValue = inputValue.filter((obj)=> obj.value != item.value);
                onInputChange(name, inputValue);
            }
        }
    }
    isChecked(item){
        var {InputType, inputValue} = this.props;
        if( InputType === 'radio' ){
            return inputValue.value == item.value;
        }else{
            return inputValue.filter((obj)=> obj.value == item.value).length > 0;
        }
    }
    isValid(){
        var {InputType, inputValue, validationClasses} = this.props;

        if(  validationClasses == 'radio-required' ){
            if(typeof inputValue == "object" && Object.keys(inputValue).length == 0 ){
                return 'radio-required';
            }else {
                return inputValue.length == 0 ? 'radio-required' : '';
            }
        }
    }

    renderList(){
        var price_span = "";
        if(this.props.name == "hours_required"){
            //price_span = (<sup>/hr</sup>);
            price_span = "/hr";
        }
        let {isNoonPage} = this.props
        return this.props.items.map((item) => {
            /*if( typeof item.isPerVisit != "undefined" && item.isPerVisit == true  == "windowSize"){
                price_span = " per visit";
            }*/
            if(typeof item.is_hourly_price != "undefined" && item.is_hourly_price == true){
                price_span = "/hr";
            }else{
                price_span = "";
            }
            
            return(
                <div key={item.id} className={this.props.childClass}>
                    <label className="btn-block pointer">
                        <input type={this.props.InputType}
                               name={this.props.name}
                               className="d-none" value={item.value}
                               onChange={(e)=> this.handleChange(e, item)}
                               checked={this.isChecked(item)}
                               label={item.label} price={this.props.price} />
                        <span className="btn-radio border rounded">
                            {item.label}
                            {
                                ( !isNoonPage && ( typeof item.price !="undefined" && item.price != 0 ) ) && <span className="price text-warning text-center">{item.currentCurrency+" "+item.price}
                                {price_span}{ this.props.isTaxApplicable && (<sup className="text-warning">*</sup>) }</span>
                            }
                        </span>
                    </label>
                </div>
            );
        });
    }
    render() {
        const {validationClasses, InputType} = this.props;
        //console.log("isTaxApplicable", this.props.isTaxApplicable);
        return (
            <React.Fragment>
                <div className={this.props.parentClass}>
                { validationClasses.length ? (
                	<div className="col-12 d-flex">
                        <div className={"checkpoint "+this.isValid()} {...(typeof this.props.validationMessage != "undefined" ? {'data-validationmsg': this.props.validationMessage} : {} ) }></div></div>
                ) : '' }
                {this.renderList()}
                </div>
                { this.props.isTaxApplicable && ( <div className="row mb-4 excludes-vat"><div className="col-12"><sup>*</sup> Excludes VAT{ this.props.moreTaxText !== "" && ". "+this.props.moreTaxText }</div></div> ) }
            </React.Fragment>
        );
    }
}

CheckRadioBoxInputWithPrice.defaultProps = {
  isTaxApplicable : true,
  parentClass: 'row mb-2',
  childClass: 'col-4 col-sm-3 mb-3 d-flex',
  validationClasses: '',
  moreTaxText : '',
  isNoonPage: false
};

export default CheckRadioBoxInputWithPrice;
