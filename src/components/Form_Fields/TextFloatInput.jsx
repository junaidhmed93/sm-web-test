import React from "react";
import {updateSingleInputValid, parseArabic} from "../../actions/index";
import { connect } from "react-redux";
import locationHelper from "../../helpers/locationHelper";
class TextFloatInput extends React.Component{
    constructor(props) {
        super(props);
        this.locationHelperInit = locationHelper.init.bind(this);
        this.locationHelperInit();
        this.refInput = React.createRef();
        this.focusTextInput = this.focusTextInput.bind(this);
        this.onChangeHandler = this.onChangeHandler.bind(this);
        this.onBlurHandler = this.onBlurHandler.bind(this);
    }
    focusTextInput(){
        this.refInput.current.focus();
    }
    onBlurHandler(event){
       const {onInputChange, lang} = this.props;
       updateSingleInputValid(this.refInput.current, lang);
       onInputChange(event.target.name, event.target.value);
    }

    onChangeHandler(event){
        //updateSingleInputValid(this.refInput.current);
       let {parseAR, lang} = this.props;
       let value = parseAR ? parseArabic(event.target.value) : event.target.value;
       if(parseAR){
            updateSingleInputValid(this.refInput.current, lang);
       }
       const {onInputChange} = this.props;
       onInputChange(event.target.name, value);
    }

    render() {
        var {inputValue, name, InputType, label, validationClasses, min,max, readOnlyInput, maxlength, autoCompleteDisabled} = this.props;
        let dateValidationMsg = typeof this.props.validationMessage == "undefined" ? locationHelper.translate("THIS_FIELD_IS_REQUIRED") : this.props.validationMessage;
        return (
            <div className={"floating-label " + (inputValue ? ' active' : '')} ref={node => { this.node = node; }} >
                <input 
                type={InputType} 
                name={name} 
                min={min>0? min : 0}
                max={max>0 ? max :0}
                maxLength={maxlength?maxlength:-1}
                value={inputValue} 
                ref={this.refInput}
                onBlur={this.onBlurHandler}
                onChange={this.onChangeHandler} 
                className={"btn-radio form-control p-3 border rounded" + (validationClasses.length ? ' checkpoint '+ validationClasses : '')}
                data-validationmsg={ typeof this.props.validationMessage != "undefined" ? this.props.validationMessage : dateValidationMsg }
                { ...( readOnlyInput ? {"readOnly":"readOnly"} : {} )}
                { ...( autoCompleteDisabled ? {"autoComplete":"off"} : {} )}
                />
                <i className="fa fa-check text-success input-valid"></i>
                <label onClick={this.focusTextInput}>{label}</label>
                <span className="error-msg"></span>
            </div>
        );
    }
}

TextFloatInput.defaultProps = {
    validationClasses: '',
    readOnlyInput: false,
    autoCompleteDisabled : false,
    parseAR: false
};
function mapStateToProps(state) {
    return {
        currentCity: state.currentCity,
        lang: state.lang
    }
}
export default connect(mapStateToProps)(TextFloatInput);
