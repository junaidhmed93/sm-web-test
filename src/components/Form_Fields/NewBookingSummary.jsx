import React from "react";
import ServiceQuestions from "./ServiceQuestions";
import GeneralModal from "../GeneralModal";
import {
	CREDITCARD_COUPON,
	isMobile,
	UAE_ID,
	DOHA,
	QATAR_ID
} from "../../actions";
import { connect } from "react-redux";
import { withCookies } from "react-cookie";
import locationHelper from "../../helpers/locationHelper";
var currentCurrency = "";
//import BookingCoupons from "../../pages/forms/booking/BookingCoupons"
class NewBookingSummary extends React.Component{
    constructor(props) {
        super(props);
		this.state = {
			coupon : ''
		}
        this.renderList = this.renderList.bind(this);
		this.renderCompanyList = this.renderCompanyList.bind(this);
		this.handleCouponValidate = this.handleCouponValidate.bind(this);
		this.renderMobileList = this.renderMobileList.bind(this);
		this.deleteCompany = this.deleteCompany.bind(this);
		this.showCoupon = this.showCoupon.bind(this);
		this.dryCleaningDesc = this.dryCleaningDesc.bind(this);
	}
	componentDidMount(){
        currentCurrency = locationHelper.getCurrentCurrency();
    }
	deleteCompany(company){
		const {selectedCompanies,selectChange} = this.props;
		const temp = selectedCompanies.filter(item => item.id == company.id);
		if( temp.length ){
			selectChange( selectedCompanies.filter(item=> item.id != company.id) );
			//console.log( selectedCompanies.filter(item=> item.id != company.id) );
		}
	}
	getPaymentLabel(paymentMethod){
		var label = "";
		if(paymentMethod == "credit"){
			label = "Credit Card"
		}
		else if(paymentMethod == "cash"){
			label = "Cash on delivery";
		}
		return label;
	}
	renderList(){
		var strikeClass = "";
		//cut-ellipsis
		var valueLabel = "";
        var priceLabels = ["VAT", "Subtotal"];
        let lineSpace = false;
		return this.props.items.map((item, index) => {
			lineSpace = false;
			strikeClass = item.text_strike == "yes" ? " strike" : "";
			strikeClass += typeof item.classNme != "undefined" ? " "+item.classNme : "";
			valueLabel = item.value;
			if(item.label == "Payment"){
				valueLabel = this.getPaymentLabel(item.value);
			}
			if(typeof valueLabel == "undefined"){
				valueLabel = "Not selected";
			}
			
			if( valueLabel.indexOf("\n") !== -1 || ( typeof valueLabel == "object" && valueLabel.length )){
				lineSpace = true;
				valueLabel = typeof valueLabel == "object" ? valueLabel : valueLabel.split('\n');
				valueLabel = valueLabel.map(i => {
					return <p className="m-0 mb-1 small">{i}</p>
				});
			}
			//locationHelper.getCurrentCountryId() == QATAR_ID
			if( (typeof valueLabel != "undefined" && valueLabel != "") ) {
				return (
					<div key={'sum' + index} className={ typeof item.conClass != "undefined" ? item.conClass : "col-12 mb-2" }>
						<div className="row">
							<div className={ typeof item.itemClass != "undefined" && item.itemClass ? item.itemClass : "col-5"}>
								<small
									className={ typeof item.titleCase != "undefined" && item.titleCase ? "title-case" : ""}>
									<b>{item.label}</b>
								</small>
							</div>
							<div className={ ( typeof item.itemClass != "undefined" && item.itemClass ) ? item.itemClass : "col-7 ml-auto"}>
							<span className={"text-secondary" + strikeClass}>
								{ !lineSpace ? (<small>{valueLabel}</small>) : valueLabel}
							</span>
							</div>
						</div>
					</div>
				);
			}
        });
	}
	renderMobileList(){
		var strikeClass = "";
		var items =  this.props.items;
		var label = "";
		return items.map((item, index) => {
			strikeClass = item.text_strike == "yes" ? " strike" : "";
			label = item.value;
			if(item.label == "Payment"){
				label = this.getPaymentLabel(item.value);
			}
			if(typeof label == "undefined"){
				label = "Not selected";
			}
			if(typeof label != "undefined" && label != "" ) {
				return (
					<div key={'mobsum' + index} className="row my-3">
						<div className="col-auto">
							<strong className="text-secondary">{item.label}:</strong>
						</div>
						<div className="col-5 ml-auto">
							<span className={"text-secondary" + strikeClass}>{label}</span>
						</div>
					</div>
				)
			}
		});
	}
	renderCompanyList(){
		var {formCurrentStep, typeOfJourney} = this.props;
		
		//console.log("this.props.selectedCompanies", this.props.selectedCompanies);

		return this.props.selectedCompanies.map((item, index) => {
			return(
				<div className="requested-company pt-2" key={'company-'+index} id={'company-'+item.id}>
					<span className="text-secondary small">{item.name}</span>
					{ ((typeOfJourney == 1 && formCurrentStep == 2 ) || typeOfJourney == 2 ) && (<button className="text-uppercase btn-remove pull-right" onClick={()=> this.deleteCompany(item)}>
						 <i className="fa fa-trash"></i>
					</button>)}
				</div>
			);
		});
	}
	renderCompanies(){

			return(
				<div className="row my-3 your-request">
					<div className="col-12 ml-auto"> <small> <b>Selected companies:</b> </small></div>
					<div className="col-12 ml-auto detail">
						{
							this.renderCompanyList()
						}
					</div>
				</div>
			);
	}
	handleCouponValidate(){

		var {isCoreBooking, isLW} = this.props;

		var coupon = document.getElementsByName("coupon");

		var value = coupon[0].value;

		//console.log("handleCouponValidate", value);

		if(isLW){
			this.props.handleCouponChange("voucherCode", value);
		}else if(isCoreBooking){
			this.props.handleCouponChange("couponCode", value);
		}else{
			this.props.handleCouponChange(value);
		}

	}
	componentWillReceiveProps (newProps) {
		/*if( newProps.cleaningData !== this.props.cleaningData ) {
			var coupon = document.getElementsByName("coupon");
			var value = coupon[0].value;
			console.log(value);
			this.props.handleCouponChange(value);
		}*/
	}
	showCoupon(couponConClass, message){
		let {showPromo, showPrice, selectedCompanies, discountData, couponValue, isLW} = this.props;
	
		return (<li className="list-group-item p-2 px-3 pb-3 coupon-con border-0">
			<div className={couponConClass}>
				<input type="text"
					   className="form-control border-0 input-placeholder"
					   value={couponValue}
					   name="coupon"
					   placeholder="Promo code"
					   onChange={() => this.handleCouponValidate()}
					   onBlur={() => this.handleCouponValidate()} 
					   />
				<div className="input-group-append">
					<button className="btn border bg-white text-primary rounded px-3 font-weight-bold apply-btn"
							name="coupon_btn" type="button"
							onClick={() => this.handleCouponValidate()}>
						<span className="circle-loader"></span>
						<span className="apply-text">Apply</span>
					</button>
				</div>

			</div>
			{message}
		</li>);
		
	}
	dryCleaningDesc(){
		let {isDryCleaning, walletAvailable, userWalletAmount} = this.props;
		let WalletText = "";
		/*if( walletAvailable ){
			WalletText = (<p className="text-primary small mb-1"><sup>*</sup> Your<span className="wallet-amount-span text-warning">{currentCurrency+" "+ userWalletAmount}</span>wallet credit will count against final price</p>);
		}*/
		
		return (typeof isDryCleaning != "undefined" && isDryCleaning) ? (
			<li className="list-group-item">
				<div className="row">
					<div className="col-auto pr-0">
						<p className="h5 m-0 font-weight-bold">Price</p>
					</div>
				<div className="col-8 pl-0 ml-auto d-block text-muted small">
					<p className="mb-1">Final price confirmed after pickup. You will only be charged after receiving your laundry back</p>
					{walletAvailable ? (<p className="mb-1"><sup>*</sup> Your<span className="wallet-amount-span text-warning">{currentCurrency+" "+ userWalletAmount}</span>wallet credit will count against final price</p>) : ''}
				</div>
			</div>
		</li>) : '';

		/*return (typeof isDryCleaning != "undefined" && isDryCleaning) ? (<div className="mt-2">
			<p className="text-primary small mb-1"><sup>*</sup> Prices confirmed on pickup.</p>
			{WalletText}
			<p className="text-primary small"><sup>*</sup> Only be charged after delivery.</p>
		</div>) : '';*/
	}
    render() {
		let {showPromo, showPrice, selectedCompanies, discountData, couponValue, isLW, items, walletAvailable, total, isSubscription, isNoonPage, voucherLoader, isDryCleaning} = this.props;
		//let isMonthlyPrice = false;
		var couponConClass = "input-group rounded p-1 border";
		var message = "";
		var loadingClass = "";
		var btnDisabled = "";

		//showPrice = typeof this.props.total != "undefined" && this.props.total != 0 ? true : false;

		showPrice = typeof total != "undefined" && (total != 0 || walletAvailable) ? true : false;

		showPrice = ( (typeof isDryCleaning != "undefined" && isDryCleaning) || isNoonPage ) ? false : showPrice;  //&& (typeof total != "undefined" && total == 0) ? false : showPrice;

		if(isLW && (typeof discountData.data != "undefined" && typeof discountData.data.error != "undefined" )){
			discountData = discountData.data;
		}

		//console.log("discountData",discountData);

		if( typeof discountData != "undefined" ) {

			//console.log(discountData);

			if (discountData == "load" || voucherLoader == true) {
				//console.log("loading-coupon");
				couponConClass += " loading-coupon";
				btnDisabled = 'disabled="disabled"';
				if (typeof discountData.success != "undefined" && discountData.success) {
					couponConClass += " border-success coupon-valid";
					message = (<p className="text-success small mt-2 mb-0">{discountData.description}</p>);
				}
			}
			else if ( (typeof discountData.error != "undefined" && discountData.error) && ( couponValue != "" )) {
				couponConClass += " border-danger coupon-invalid";
				message = (<p className="text-danger mt-2 mb-0">{discountData.error_message}</p>);
			}
			else if (discountData.success) {
				couponConClass += " border-success coupon-valid";
				message = (<p className="text-success small mt-2 mb-0">{discountData.description}</p>);
			}
		}
		if( (couponValue == "" || couponValue == CREDITCARD_COUPON) ){
			message = "";
			couponConClass = "input-group rounded p-1 border";
		}
		var headingTitle = this.props.typeOfFlow == "booking" ? "Booking Summary" : "Request Summary";
		// Mobile Summary Content Start

		var priceLoader = this.props.priceLoader == true ? (<span className="circle-loader price-calc-loader"></span>) : '';

		var totalText = 0; 
		
		if(typeof this.props.total != "undefined" && this.props.total){
			totalText = this.props.total < 0 && this.props.walletAvailable ? 0.00 : 
				!Number.isInteger(this.props.total) ? (this.props.total).toFixed(2) : this.props.total;
		}
		
		let showVat = true;

		if(locationHelper.getCurrentCountryId() == QATAR_ID ){
			showVat = false
		}
		let vatLabel = "VAT";
		let {numberOfHours, userWalletAmount} = this.props;
		if(showVat){
			let taxPlanReducer = this.props.TaxPlanReducer;
			let taxPercentage = ( typeof taxPlanReducer.taxRateDtoList != "undefined" && taxPlanReducer.taxRateDtoList.length ) ? taxPlanReducer.taxRateDtoList[0].value : '';

			vatLabel += " ( "+taxPercentage+" % )";
		}
		let WalletText = "";
		/* WEB-2489 */
		let TotalToPayTxt = isSubscription ? "Monthly total" : "Total to pay" ;

		if( ( ( (typeof numberOfHours !="undefined" && typeof numberOfHours.id !="undefined") && numberOfHours.id == 0) || isSubscription) && walletAvailable ){
			let perHourPrice = "";
			if( (typeof numberOfHours !="undefined" && typeof numberOfHours.id !="undefined") && numberOfHours.id == 0){
				perHourPrice = typeof numberOfHours.per_hour_price != "undefined" ? numberOfHours.per_hour_price : numberOfHours.price;
			}
			if(isSubscription){
				perHourPrice = this.props.packagePrice;
			}
			totalText = !Number.isInteger(perHourPrice) ? (perHourPrice).toFixed(2) : perHourPrice;
			WalletText = (<div className="wallet-text small my-2">Your<span className="wallet-amount-span">{currentCurrency+" "+ userWalletAmount}</span>wallet credit will count against final price</div>);
		}
		
		let {vat} = this.props;
		
		if(vat){
			vat = vat < 0 ? 0: !Number.isInteger(vat) ? (vat).toFixed(2) : vat;
		}
		
		var modalContent = (
			<div className="row booking-summary">
				<div className="col">
					<div className="card">
						<h5 className="h3 card-header text-primary bg-white p-3">{headingTitle}</h5>
						<div className="card-body">
							{ /*this.renderMobileList()*/
							this.renderList() }
							{
								( typeof this.props.showMinMessage != "undefined" && this.props.showMinMessage) &&
								<div className="col my-3"><div className="border border-warning text-warning p-1 m-1">Minimum charge {currentCurrency} 149</div></div>
							}
							
							{selectedCompanies.length ? this.renderCompanies() : ''}
						</div>
						<ul className="list-group list-group-flush">
						{ showPromo && this.showCoupon(couponConClass, message) }
							{showPrice ? (
								<React.Fragment>
									<li className="list-group-item">
										<div className="row align-items-center">
											<div className="col-auto pr-0 mr-auto">
												<p className="h5 m-0 font-weight-bold">{TotalToPayTxt}</p>
											</div>
											<div className="col-auto pl-0 ml-auto text-warning d-flex align-items-center">
												{
													priceLoader
												}
												<span className="h5 mr-2 mt-2 mb-0">{currentCurrency}</span>
												<span className="h2 font-weight-bold m-0">{totalText}</span>
												{isSubscription && <span className="h5 mt-2 mb-0 d-none">/Month</span>}
												{this.props.isHourlyRate && <span className="h5 mt-2 mb-0">/HR</span>}
											</div>
										</div>
										{WalletText}
									</li>
									{ showVat ? (<li className="list-group-item">
										<div className="row align-items-center">
											<div className="col-auto pr-0 mr-auto">
												<p className="h5 m-0 font-weight-bold">{vatLabel}</p>
											</div>
											<div className="col-auto pl-0 ml-auto text-warning d-flex align-items-center">
												<span className="mr-2 mt-2 mb-0">{currentCurrency+" "+ vat}</span>
											</div>
										</div>
									</li>) : ''}
							</React.Fragment>			
							) : ''}
							{this.dryCleaningDesc()}
						</ul>
					</div>
				</div>
			</div>
		);
		// Mobile Summary Content End
		if(!isMobile()){
			
			return (
				<React.Fragment>
				<div className="row sticky-sidebar d-lg-block d-none booking-summary">
					<div className="col-12">
						<div className="card mb-3">
							<h5 className="h3 card-header text-primary bg-white p-3">{headingTitle}</h5>
							<div className="card-body px-3 py-0">
								<div className="row mt-3">
									{this.renderList()}
									{
										( typeof this.props.showMinMessage != "undefined" && this.props.showMinMessage) &&
										<div className="col"><div className="border border-warning text-warning p-1 m-1">Minimum charge {currentCurrency} 149</div></div>
									}
								</div>
								{selectedCompanies.length ? this.renderCompanies() : ''}
							</div>
							<ul className="list-group list-group-flush">
								{ showPromo && this.showCoupon(couponConClass, message) }
								{showPrice ? (
									<React.Fragment>
										<li className="list-group-item border-top-1 border-bottom-0 py-1">
											<div className="row align-items-center">
												<div className="col-auto pr-0 mr-auto">
													<p className="h5 m-0 font-weight-bold">{TotalToPayTxt}</p>
												</div>
												<div className="col-auto pl-0 ml-auto text-warning d-flex align-items-center">
													{ priceLoader }
													<span className="h5 mt-2 mb-1 mr-1">{currentCurrency}</span>
													<span className="h2 font-weight-bold m-0">{ totalText }</span>
													{isSubscription && <span className="h5 mt-2 mb-0 d-none">/Month</span>}
													{ this.props.isHourlyRate && <span className="h5 mt-2 mb-0">/HR</span> }
												</div>
											</div>
											{WalletText}
										</li>
										{showVat ? (
											<li className="list-group-item border-0 pt-1">
												<div className="row align-items-center">
													<div className="col-auto pr-0 mr-auto">
														<p className="h5 m-0">{vatLabel}</p>
													</div>
													<div className="col-auto pl-0 ml-auto d-flex align-items-center small mr-1">
														{currentCurrency+" "+ vat}
													</div>
												</div>
											</li>) : ''}
									</React.Fragment>
								) : ''}
								{this.dryCleaningDesc()}
							</ul>
						</div>
					</div>
					<div className="col-12">
						<ServiceQuestions typeOfFlow={this.props.typeOfFlow} slug = {this.props.serviceSlug}/>
					</div>
				</div>
				</React.Fragment>
			);
		}else{
			return (
				<React.Fragment>
					{this.props.showMobileSummary && <GeneralModal title="Booking Information" modalBody={modalContent} setModal={this.props.setModal} />}
				</React.Fragment>
			);
		}
    }
}
NewBookingSummary.defaultProps = {
	total : 0,
	priceLoader : false,
	isLW:false,
	isCoreBooking: false,
	isSpecializedCleaning: false,
	hourlyRate:false,
    items : [],
    showPromo: true,
	showPrice: true,
	selectedCompanies: [],
	typeOfFlow : "booking",
	typeOfJourney : 1,
	serviceSlug: "",
	isSubscription: false,
	isNoonPage: false,
	voucherLoader: false
}
function mapStateToProps(state){
    return {
        TaxPlanReducer: state.TaxPlanReducer
    }
}
export default withCookies(connect(mapStateToProps)(NewBookingSummary));
//export default NewBookingSummary;
