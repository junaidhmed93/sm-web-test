import React from "react";
import FormFieldsTitle from "../FormFieldsTitle";
import DatePicker from 'react-datepicker';
import { connect } from "react-redux";
import moment from 'moment';
import locationHelper from "../../helpers/locationHelper";
import { LANG_AR } from "../../actions";
class DatePick extends React.Component {
    constructor(props) {
        super(props);

        var booking_date = props.inputValue == "" ? "" : (props.inputValue).split('-').reverse().join('-');

        var selectedDate = booking_date == "" ? null : moment(booking_date);

        var startDate = props.dateAndTime;

        this.state = {
            selectedDate: selectedDate,
            currentDate: moment(startDate),
            isBooking: true,
            pathname: ""
        };


        this.locationHelperInit = locationHelper.init.bind(this);
        this.locationHelperInit();
        this.handleChange = this.handleChange.bind(this);
        this.openCalender = this.openCalender.bind(this);
        this.isNotValid = this.isNotValid.bind(this);
        this.isBookingPage = this.isBookingPage.bind(this);
    }
    isBookingPage(pathname) {
		var pathname = typeof pathname != "undefined" ? pathname : this.state.pathname;
		
		var pathArray = pathname.split('/');

		if (pathArray.includes('book-online')) {
			return true;
		} else {
			return false;
		}
	}
    componentDidMount() {
        this.node.input.readOnly = true;
        var pathname = window.location.pathname;
        this.setState({
			pathname: pathname
		});
        this.disableHoliday();
    }
    componentWillReceiveProps(newProps){
        if (newProps.dateAndTime !== this.props.dateAndTime) {
            this.setState({
                currentDate: newProps.dateAndTime
            })
        }
    }
    disableHoliday() {
        var bookingHolidays = this.props.bookingHolidays;
        var holidays = [];
        var holiday = "";
        if(typeof bookingHolidays != "undefined" && bookingHolidays != null ){
            bookingHolidays.map((item, index) => {
                holiday = moment(new Date(item.year + "-" + item.month + "-" + item.day));
                holidays.push(holiday);
            })
        }
        /*var disable_this_week_friday = this.disableThisWeekFriday();

        if (disable_this_week_friday) {
            holidays.push(disable_this_week_friday);
        }*/
        return holidays;
    }
    disableFridays = (date) => {
        const day = date.day();
        var disableFriday = this.props.disableFriday;
        if (disableFriday) {
            return day !== 5;
        }
        return day == 0 || day;
    }
    handleChange(date) {
        var { name, onInputChange } = this.props;

        var stringDate = moment(date).format('DD-MM-YYYY');
        this.setState({
            selectedDate: date
        });
        onInputChange(name, stringDate);
    }
    openCalender() {
        this.node.setOpen(true)
    }
    isNotValid() {
        var { validationClasses, inputValue } = this.props;

        var { selectedDate } = this.state;
        if (validationClasses == 'required minDate') {
            return 'required';
        }
        else if (validationClasses.trim() == 'required') {
            return inputValue.length == 0 ? 'required' : '';
        }

    }
    componentWillReceiveProps (newProps) {
        if (newProps.startDate !== this.props.startDate) {
            this.setState({
                currentDate: moment(newProps.startDate)
            })
        }
    }
    render() {
        var { startDate, validationClasses, lang, popperClassName, popperPlacement } = this.props;
        
        var currentDate = this.state.currentDate;

        if( typeof startDate == "undefined"){
            var now = moment(currentDate),
            startDate = now.add(1, 'day');
        }
        var excludeDates = this.isBookingPage() ? this.disableHoliday() : [];

        var allowSameDay = this.props.allowSameDay ? this.props.allowSameDay : true;
        var filterDate = this.props.filterDate ? this.props.filterDate : this.disableFridays;
        //<DatePicker filterDate={filterDate} minDate={startDate} selected={this.state.selectedDate} onChange={this.handleChange} className="bg-white form-control p-3 border rounded btn-date input-placeholder" placeholderText="Select date" />
        //className={"checkpoint "+this.isNotValid()}
        var validationCls = "position-relative border rounded " + (validationClasses.length ? ' checkpoint ' + this.isNotValid() : "");

        let dateValidationMsg = locationHelper.translate("PLEASE_SELECT_DATE");  
        
        return (
            <div className={this.props.childClass}>
                {this.props.title != "" && <FormFieldsTitle title={this.props.title} />}
                <div className={validationCls} {...(typeof this.props.validationMessage != "undefined" ? { 'data-validationmsg': dateValidationMsg } : {})}>
                    <DatePicker
                        /*locale = {lang}*/
                        dateFormat="DD/MM/YYYY"
                        name={this.props.name}
                        ref={node => this.node = node}
                        filterDate={filterDate}
                        minDate={startDate}
                        selected={this.state.selectedDate}
                        excludeDates={excludeDates}
                        onChange={this.handleChange}
                        className={"bg-white form-control border-0 p-3 btn-date input-placeholder"}
                        placeholderText={this.props.placeholderText ? this.props.placeholderText : locationHelper.translate('SELECT_DATE')}
                        { ...( popperClassName != "" ? {"popperClassName": popperClassName} : {} )}
                        { ...( popperPlacement != "" ? {"popperPlacement": popperPlacement} : {} )}
                        />
                        <div className={lang == LANG_AR ? "input-group-append is-a middle icon-left" : "input-group-append is-a middle icon-right"}>
                            <span onClick={this.openCalender}>
                                <i className="fa fa-calendar text-secondary"></i>
                            </span>
                        </div>
                </div>
            </div>
        );
    }
    //+ (validationClasses.length ? ' checkpoint ' + validationClasses : '')
}
DatePick.defaultProps = {
    title: '',
    validationClasses: '',
    disableFriday: true,
    parentClass: 'row mb-4',
    childClass: 'col-12 col-sm-6 mb-3 date-container',
    placeholderText: '',
    inputValue: "",
    validationMessage: "Please select date",
    popperClassName:"bottom-placement",
    popperPlacement:""
};
function mapStateToProps(state) {
    return {
        service_constants: state.serviceConstants,
        data_values: state.dataValues,
        bookingHolidays: state.bookingDateTimeAvailabilityReducer.bookingHolidays,
        currentCity: state.currentCity,
        lang: state.lang,
        dateAndTime: state.dateAndTime
    }
}
export default connect(mapStateToProps)(DatePick);
