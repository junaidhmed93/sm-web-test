import React from "react";
import PlansLabelIcon from "./PlansLabelIcon";

class Plans extends React.Component{
    constructor(props) {
        super(props);

        this.renderList = this.renderList.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.isChecked = this.isChecked.bind(this);
    }
    handleChange(event){
        var {name, onInputChange} = this.props;
        onInputChange(name, event.target.value);
    }
    isChecked(value){
        var {inputValue} = this.props;
        return inputValue === value;
    }

    renderList(){
        return this.props.items.map((item) => {
            return(
            <div key={item.id} className={this.props.childClass}>
                <div className="mb-3">
                    <label className="d-block package payment pointer">
                        <input type="radio" name={this.props.name} className="d-none" value={item.value} onChange={this.handleChange} checked={this.isChecked(item.value)} />
                        <div className="card">
                            <div className="card-body p-3 package-title">
                                <h5 className="card-title m-0 h3">{item.title}</h5>
                                <h6 className="card-subtitle m-0 h5">{item.subTitle}</h6>
                            </div>
                            <ul className="list-group list-group-flush">
                                <li className="list-group-item px-3 d-flex d-md-none align-items-end justify-content-between">
                                    <div>
                                        <p className="m-0">
                                            <span className="text-warning h2 m-0 font-weight-bold">{item.price}</span>
                                            <span className="text-warning m-0">{item.currency}</span>
                                        </p>
                                    </div>
                                    <div>
                                        <p className="m-0 text-center text-black">*excluding VAT</p>
                                    </div>
                                </li>
                                <li className="list-group-item px-3 py-0">
                                    <PlansLabelIcon items={item.rateSection} />
                                </li>
                                <li className="list-group-item px-3 py-0 collapse standard-detail">
                                    <PlansLabelIcon items={item.features} />
                                </li>
                                <li className="list-group-item d-none d-md-block p-3">
                                    <p className="d-flex align-items-center justify-content-center m-0">
                                        <span className="text-warning">{item.currency}</span>
                                        <span className="text-warning h1 font-weight-bold">{item.price}</span>
                                    </p>
                                    <p className="m-0 text-center text-black h5">Excluding VAT</p>
                                </li>
                            </ul>
                            <div data-toggle="collapse" aria-expanded="false" data-target=".standard-detail" className="card-body border-bottom py-1 d-md-none text-center">
                                <div ariashow="false">
                                    <p className="text-center m-0 text-primary">
                                        <span>Show Details</span>
                                        <i className="fa fa-angle-down"></i>
                                    </p>
                                </div>
                                <div ariashow="true">
                                    <p className="text-center m-0 text-primary">
                                        <span>Hide Details</span>
                                        <i className="fa fa-angle-up"></i>
                                    </p>
                                </div>
                            </div>
                            <div className="card-body package-select text-uppercase text-center">
                                Select Package
                            </div>
                        </div>
                    </label>
                </div>
            </div>
            );
        });
    }
    render() {
        return (
            <div className={this.props.parentClass}>
                {this.renderList()}
            </div>
        );
    }
}

Plans.defaultProps = {
  parentClass: 'row',
  childClass: 'col-12 col-sm-6 col-md-4'
};

export default Plans;
