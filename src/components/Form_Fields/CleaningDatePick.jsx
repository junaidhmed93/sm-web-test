import React from "react";
import DatePicker from 'react-datepicker';
import moment from 'moment';
import {connect} from "react-redux";
var currentCity = "";
var dataValues = "";
class CleaningDatePick extends React.Component{
    constructor(props) {
        super(props);

        var booking_date = props.inputValue == "" ? "" : (props.inputValue).split('-').reverse().join('-');

        var selectedDate = booking_date == "" ? null : moment( booking_date );

        var startDate = typeof this.props.startDate != "undefined" ? this.props.startDate : props.dateAndTime

        this.state = {
            selectedDate: selectedDate,
            startDate: moment(startDate),
            excludeDates:[],
            days_to_show:[],
            currentDate: moment(startDate)
        };
        //console.log("dateAndTime", props.dateAndTime)
        this.handleChange = this.handleChange.bind(this);
        this.disableStartDate = this.disableStartDate.bind(this);
        this.disableHoliday = this.disableHoliday.bind(this);
        this.disableFridays = this.disableFridays.bind(this);
        this.disableThisWeekFriday = this.disableThisWeekFriday.bind(this);
        this.disableUnselectedDays = this.disableUnselectedDays.bind(this);
        this.isNotValid = this.isNotValid.bind(this);
    }
    disableHoliday(){
        var booking_holidays = typeof this.props.booking_holidays != "undefined" ? this.props.booking_holidays : [];
        var holidays = [];
        var holiday = "";
        let dateExclude = ["2019-6-6"];
        booking_holidays.map((item,index) => {
            let genDate = item.year+"-"+item.month+"-"+item.day;
            if(!dateExclude.includes(genDate)){
                holiday = moment(new Date(genDate));
                holidays.push(holiday);
            }
        })
        var disable_this_week_friday = this.disableThisWeekFriday();

        if(disable_this_week_friday){
            holidays.push(disable_this_week_friday);
        }
        return holidays;
    }
    disableStartDate(){

        var disable_today = false;
        var now = moment(this.state.currentDate),
            now_hour = now.hour(),
            now_day = now.day();
        if ( now_hour >= 12 ){
            disable_today = true;
        }
        if(disable_today){
            var next_day = this.props.startDateThresholdIfTodayIsDisabled? this.props.startDateThresholdIfTodayIsDisabled : 1;
            var disable_this_week_friday = this.disableThisWeekFriday();
            var startDate = moment(this.state.currentDate).add(next_day, 'days');
            this.setState({
                startDate: startDate
            })
        }

    }
    componentDidMount() {
        currentCity = this.props.currentCity;
        dataValues = this.props.dataValues;
        this.disableStartDate();
        this.disableHoliday();
        //this.disableUnselectedDays()
        this.node.input.readOnly = true;
    }
    handleChange(date){
        var {name, onInputChange} = this.props;
        var stringDate = moment(date).format('DD-MM-YYYY');
        this.setState({
            selectedDate: date
        });
        onInputChange(name, stringDate);
    }
    disableThisWeekFriday(){
        var now = moment(this.state.currentDate),
            now_hour = now.hour(),
            now_day = now.day();

        var after_two_days = now.add(2,'day');

        var disable_this_week_friday = false; // flag for disable friday.

        if( now_day >= 3){ //checking today is wednesday or above wednesday
            disable_this_week_friday = true;

            if(now_day == 3 && now_hour < 17){ // if today is wednesday and time is less then 5PM. then don't disable friday.
                disable_this_week_friday = false;
            }
        }

        if( disable_this_week_friday && currentCity == "dubai"){

            var this_week_friday = moment(this.state.currentDate).weekday(5); // getting this week's friday

            return this_week_friday;
        }
        return false;
    }
    disableUnselectedDays(selected_days){
        var {typeOfSubscription} = this.props;
        let currentCity = this.props.currentCity,
        calender_days = [0,1,2,3,4,6],
        day_value = "",
        show_days =[];
        
        if(selected_days.length){
            selected_days.map((item, index) => {
                day_value = item.value == 7 ? 0 : item.value;
                show_days.push(day_value);
            } )
        }
        var days_to_show = [];
        if(show_days.length){
            calender_days.map((item, index) => {
                if(show_days.includes(item)){
                    days_to_show.push(parseInt(item));
                }
            } )
        }
        if(typeOfSubscription == "daily"){
            days_to_show = [];
            if(selected_days.length == 6){
                days_to_show.push(6);
            }
            if(selected_days.length == 5){
                days_to_show.push(0);
            }
        }
        
        return days_to_show;
    }
    disableFridays = (date) => {
        var isSubscription = typeof this.props.isSubscription != "undefined" ? this.props.isSubscription : false;
        var dataValues = this.props.dataValues;
        const day = date.day();

        var {hours_required, number_of_cleaners, service_needed,selected_days} = this.props;

        selected_days = selected_days == "undefined" ? [] : selected_days;

        var currentCity = this.props.currentCity;

        var show_days = this.disableUnselectedDays(selected_days);

        var disable_friday = false;

        if( isSubscription || (service_needed.value == dataValues.DATA_VALUE_MULTIPLE_TIMES_WEEK || ( hours_required && hours_required.value <= 2) ) || currentCity != "dubai" ){
            disable_friday = true;
        }
        if( service_needed.value == dataValues.DATA_VALUE_MULTIPLE_TIMES_WEEK && show_days.length != 0 ){
            return show_days.includes(day);
        }
        if( disable_friday || this.props.disableFriday ) {
            return day !== 5;
        }
        return day == 0 || day;
    }
    isNotValid(){
        var {validationClasses, inputValue} = this.props;

        var {selectedDate} = this.state;

        if( validationClasses == 'required' ){
            return inputValue.length == 0 ? 'required' : '';
        }

    }
    componentWillReceiveProps (newProps) {
        if (newProps.startDate !== this.props.startDate) {
            this.setState({
                currentDate: moment(newProps.startDate)
            })
        }
    }
    render() {
        var {startDate} = this.state;
        var excludeDates = this.disableHoliday();
        var {validationClasses} = this.props;

        var show_error = this.props.show_error;
        var datePickerDisabled = show_error != "" ? true : false;
        var filterDate = this.props.filterDate ? ((date)=>{return this.props.filterDate(date) && this.disableFridays(date) }): this.disableFridays; 

        var validationCls = "position-relative border rounded "+ (validationClasses.length ? ' checkpoint ' + this.isNotValid() : "");
        // className={"bg-white form-control p-3 border rounded btn-date input-placeholder"  + (validationClasses.length ? ' checkpoint '+ validationClasses : '')} 
        return (
            <div className={this.props.childClass}>
                <div className={validationCls} {...(typeof this.props.validationMessage != "undefined" ? {'data-validationmsg': this.props.validationMessage} : {} ) }>
                    <DatePicker 
                    dateFormat="DD/MM/YYYY"
                    ref={ node=> this.node = node } 
                    disabled={datePickerDisabled} 
                    filterDate={filterDate} minDate={startDate}  
                    selected={this.state.selectedDate} 
                    excludeDates={excludeDates} onChange={this.handleChange}
                    className={"bg-white form-control border-0 p-3 btn-date input-placeholder"}
                    placeholderText="Select date" />
                    <div className="input-group-append is-a middle icon-right">
                        <span>
                            <i className="fa fa-calendar text-secondary"></i>
                        </span>
                    </div>
                </div>
                { /* show_error != "" && <p className="text-danger mt-2">{show_error}</p> */ }
            </div>
        );
    }
}
CleaningDatePick.defaultProps = {
  validationClasses: '',
  selected_days: [],
  parentClass: 'row mb-4',
  childClass: 'col-12 col-sm-6 mb-3 date-container',
  booking_holidays: []
};
function mapStateToProps(state){
    return {
        service_constants: state.serviceConstants,
        booking_holidays: state.bookingDateTimeAvailabilityReducer.bookingHolidays,
        currentCity:state.currentCity,
        dateAndTime: state.dateAndTime
    }
}
export default connect(mapStateToProps)(CleaningDatePick);
//export default CleaningDatePick;
