import React from "react";
import locationHelper from "../../helpers/locationHelper";
var currentCurrency = "";
class BookNextStep extends React.Component{
    constructor(props) {
        super(props);
    }
    componentDidMount(){
        currentCurrency = locationHelper.getCurrentCurrency()
    }
    render() {
        var countinueBtnCls = this.props.disabled ? " grey-button" : "";
        var btnSubText = this.props.noNextStep ? "Next step ("+this.props.title+")" : this.props.title;
        let total = ( typeof this.props.total != "undefined" && this.props.total != 0 ) ? this.props.total : 0;
        
        total = !Number.isInteger(total) ? (total).toFixed(2) : total;
        
        return (
            <footer className="footer-buttons fixed-bottom container-fluid bg-white shadow-1">
                <div className="btn-fixed row ">
                    <div className="col">
                        <div className="row d-none d-md-block">
                            <div className="col-12 px-0 d-xs-flex text-center">
                                <button className={"btn btn-inline-block btn-warning py-2 font-weight-bold"+countinueBtnCls} disabled={this.props.disabled} onClick={()=> this.props.moveNext(this.props.toStep)}>
                                    <span className="text-uppercase">Continue Booking</span>
                                    <span className="d-block">
                                        <small>{ btnSubText }</small>
                                    </span>
                                </button>
                            </div>
                        </div>
                        <div className="row d-md-none d-lg-none d-lg-hide">
                            <div className="col-6 px-0 d-xs-flex text-center mobile-your-summery">
                                <button className="clear-focus btn btn-block rounded-0 btn-dark py-3 font-weight-bold" onClick={()=> this.props.setModal(true)}>
                                    <i className="fa fa-2x fa-angle-up font-weight-bold d-inline-block mx-2"></i>
                                    <span className="text-uppercase"> {
                                        ( typeof this.props.total != "undefined" && this.props.total != 0 ) ? currentCurrency+" "+total : "Summary"
                                    }</span>
                                </button>
                            </div>
                            <div className="col-6 px-0 d-xs-flex text-center">
                                <button className="btn btn-inline-block btn-warning py-2 font-weight-bold" disabled={this.props.disabled} onClick={()=> this.props.moveNext(this.props.toStep)}>
                                    <span className="text-uppercase">Continue Booking</span>
                                    <span className="d-block">
                                        <small>{ btnSubText }</small>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        );
    }
}
BookNextStep.defaultProps = {
    disabled: false,
    noNextStep : true
};
export default BookNextStep;
