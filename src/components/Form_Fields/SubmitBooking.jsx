import React from "react";

class SubmitBooking extends React.Component{
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="row d-none d-lg-block">
                <div className="col-lg-8">
                    <div className="row pt-4 pb-5">
                        <div className="col-lg-6 ml-auto">
                            <button className="btn btn-block btn-warning py-3 text-uppercase font-weight-bold" onClick={this.props.submitData}>
                            <span className="text-uppercase">Book Now</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default SubmitBooking;
