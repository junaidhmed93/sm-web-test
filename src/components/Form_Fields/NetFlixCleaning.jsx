import React from "react";
import PackageBox from "./PackageBox";
var current_currency = "AED";
class NetFlixCleaning extends React.Component{
    constructor(props) {
        super(props);
		this.netflixCleaningPackages = this.netflixCleaningPackages.bind(this);
    }
	netflixCleaningPackages(packageType = "weekly", noOfCleaners = 1 , noOfHours = null){
		var selectedPackage = this.state.packages;
		var netflixPackages = [];
		var packages = bookingPricingPlan.filter(
			(item) => ( item.planChargesDtoList[0].constantJson.PACKAGE_TYPE == packageType && item.planChargesDtoList[0].constantJson.NO_OF_CLEANERS == noOfCleaners ) && ( (noOfHours != null && noOfHours == item.planChargesDtoList[0].constantJson.HOURS) || noOfHours == null )
		);
		var newPackage = [];
		if(packages.length){
			packages.sort(compare);
			var planChargesDtoList = "";
			var features = {};
			var singlePackage = [];
			var noOfCleaners = 0,
				noOfHours = 0,
				packageName = "",
				weekdays_nos = [],
				weekdays = [];
			packages.map((item,i) => {
				features = {};
				singlePackage = [];
				planChargesDtoList = item.planChargesDtoList[0];
				noOfCleaners = planChargesDtoList.constantJson.NO_OF_CLEANERS;
				noOfHours =  planChargesDtoList.constantJson.HOURS;
				singlePackage["id"] = item.id;
				packageName = noOfHours+" Hrs Every Week";
				if(packageType == "daily"){
					packageName = noOfHours+" Hrs " + (planChargesDtoList.constantJson.WEEKDAYS).length+" days a week";
				}
				singlePackage["name"] = packageName;
				if(packageType == "weekly") {
					features[noOfCleaners + " Cleaner"] = "Yes";
					features[noOfHours + " Hours every week"] = "Yes";
					features["Monthly flat fee for a weekly cleaning service"] = "Yes";
				}
				if(packageType == "daily") {
					features["Same cleaner everyday"] = "Yes";
					features["Pay monthly, enjoy daily"] = "Yes";
					var hourlyRate = (planChargesDtoList.rate /
					((planChargesDtoList.constantJson.HOURS * (planChargesDtoList.constantJson.WEEKDAYS).length * 4.33))).toFixed(2);
					features["Hourly rate of "+current_currency+" "+ hourlyRate + " per hour"] = "Yes";
					weekdays_nos = planChargesDtoList.constantJson.WEEKDAYS;
					singlePackage["data_weekdays"] = weekdays_nos;
					weekdays = field_multiple_times_week_options.filter( (item) => ( weekdays_nos.includes(item.value) ));
					//console.log(weekdays);
					singlePackage["weekdays"] = weekdays;
					singlePackage["data_first_weekday"] = planChargesDtoList.constantJson.WEEKDAYS[0];
				}
				singlePackage["features"] = features;
				singlePackage["rate"] = planChargesDtoList.rate;
				singlePackage["per_month"] = "Yes";
				singlePackage["data_no_of_hours"] = {
					id: noOfHours,
					value: noOfHours,
					label: noOfHours + " hours"
				};
				singlePackage["data_no_of_cleaners"] = {id: noOfCleaners, value: noOfCleaners, label: noOfCleaners+" Cleaner"};
				/*singlePackage["data_weekdays"] = planChargesDtoList.constantJson.WEEKDAYS.join(",")
				 singlePackage["data_first_weekday"] = planChargesDtoList.constantJson.WEEKDAYS[0];*/
				singlePackage["data_cleaning_materials"] = item.planAdditionalChargesAsMap.CLEANING_MATERIAL;
				singlePackage["data_frequency"] = {id: "3", value: data_values.DATA_VALUE_MULTIPLE_TIMES_WEEK, label: "Several times a week",desc:"Same cleaner every time"};

				//data_no_of_hours data_no_of_cleaners , data_weekdays, data_first_weekday, data_cleaning_materials , data_frequency
				newPackage.push(singlePackage);
				// netflixPackages.push(this.package(item));
			})
		}

		return newPackage;
		//<PackageBox />
		//return netflixPackages;
	}
    render() {
    	const {validationClasses} = this.props;
        return (
			<div className="row">
				{ validationClasses.length ? (
                	<div className="col-12 d-flex"><div className={"checkpoint "+this.isValid()}></div></div>
				) : '' }
				{this.renderList()}
			</div>
        );
    }
}

NetFlixCleaning.defaultProps = {
  validationClasses: ''
};
export default NetFlixCleaning;
