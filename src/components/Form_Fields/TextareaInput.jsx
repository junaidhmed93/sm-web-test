import React from "react";
import {updateSingleInputValid} from "../../actions/index";
import locationHelper from "../../helpers/locationHelper";
import { connect } from "react-redux";
class TextareaInput extends React.Component{
    constructor(props) {
        super(props);
        this.locationHelperInit = locationHelper.init.bind(this);
        this.locationHelperInit();
        this.refInput = React.createRef();
        this.onChangeHandler = this.onChangeHandler.bind(this);
    }

    onChangeHandler(event){
        updateSingleInputValid(this.refInput.current);
       const {onInputChange} = this.props;
       onInputChange(event.target.name, event.target.value);
    }
    render() {
        var {name, placeholder, validationClasses} = this.props;
        let dateValidationMsg = typeof this.props.validationMessage == "undefined" ? locationHelper.translate("THIS_FIELD_IS_REQUIRED") : this.props.validationMessage;
        return (
            <div className="position-relative">
                <textarea 
                name={name} 
                ref={this.refInput} 
                cols="30" 
                rows="4" 
                placeholder={placeholder} 
                onChange={this.onChangeHandler}
                maxLength={this.props.maxLength}
                {...(typeof this.props.validationMessage != "undefined" ? {'data-validationmsg': this.props.validationMessage} : {} ) }
                className={"form-control py-3 mb-3 border input-placeholder " + (validationClasses.length ? ' checkpoint '+ validationClasses : '')}></textarea>
                <span className="error-msg"></span>
            </div>
        );
    }
}

TextareaInput.defaultProps = {
    validationClasses: '',
    maxLength: 400
};
function mapStateToProps(state) {
    return {
        currentCity: state.currentCity,
        lang: state.lang
    }
}
export default connect(mapStateToProps)(TextareaInput);