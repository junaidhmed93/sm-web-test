import React from "react";
import {connect} from "react-redux";
import {
    URLCONSTANT
} from "../../actions";
import commonHelper from "../../helpers/commonHelper";

import FormTitleDescription from "../../components/FormTitleDescription";

class PaymentMethods extends React.Component{
    constructor(props) {
        super(props);

        this.renderList = this.renderList.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.isChecked = this.isChecked.bind(this);
        this.haveCreditCard = this.haveCreditCard.bind(this);
        this.getDescription = this.getDescription.bind(this);

    }
    handleChange(event){
        var {name, onInputChange} = this.props;
        onInputChange(name, event.target.value);
    }
    isChecked(value){
        var {inputValue} = this.props;
        return inputValue === value;
    }
    haveCreditCard() {
        if(this.props.myCreditCardsData.data && this.props.myCreditCardsData.data.length){
            return 'Your saved card ending with '+this.props.myCreditCardsData.data[0].last4Char+' will be used for this booking. You can update it from your account';
        }else{
            return 'Verify your card now, pay only when service is delivered. Your card will be verified for 1 AED and then the 1 AED will be reversed';
        }
    }
    renderList(){
        var {items, status,showCOD,isSubscription} = this.props;

        if(!showCOD) {
            items = [items[0]];
        }

        items[0].text = this.haveCreditCard();


        var discription = this.getDescription();

        

        //discription = this.props.isCreditCardCouponApplied ? "\u00A0" : discription;

        return items.map((item) => {
            return(
            <div key={item.id} className={this.props.childClass}>
                <div className="mb-3">
                    <label className="d-block package payment pointer">
                        <input type="radio" name={this.props.name} className="d-none" value={item.value} onChange={this.handleChange} checked={this.isChecked(item.value)} />
                        <div className="card">
                            <div className="card-body p-4 package-title border-bottom text-center">
                                <h5 className="card-title m-0 h3">{item.label}</h5>
                            </div>
                            <div className="card-body text-center">
                                <p className="package-detail">{ item.value == "cash" ? discription[item.value] : item.text}</p>
                                <hr />
                                <p className="text-primary">{ ( this.props.isCreditCardCouponApplied || isSubscription ) ? "\u00A0" : item.value == "credit" ? discription[item.value] : item.desc }</p>
                                <img className="mx-auto" src={"/dist/images/"+item.image} alt="" height="32" />
                            </div>
                            <div className="card-body border-top package-select text-uppercase text-center">
                                Select
                            </div>
                        </div>
                    </label>
                </div>
            </div>
            );
        });
    }
    getDescription(){
        var desc = {credit:"Get 10% off on first booking when pay by card", cash:"No card? No problem, Just pay our technician cash when the job is done."};
        
        var slug = commonHelper.urlArgs(2);

        var allDesc = {};
        
        allDesc[URLCONSTANT.CLEANING_MAID_PAGE_URI] = {credit:"Get 10% off on first cleaning when pay by card", cash:"Please ensure you have exact change to give the cleaner when they finish."};

        allDesc[URLCONSTANT.WINDOW_CLEANING_PAGE_URI] = {credit:"Get 10% off on first cleaning when pay by card", cash:"Please ensure you have exact change to give the cleaner when they finish."};

        allDesc[URLCONSTANT.POOL_CLEANING_PAGE_URI] = {credit:"Get 10% off on first cleaning when pay by card", cash:"Please ensure you have exact change to give the cleaner when they finish."};

        allDesc[URLCONSTANT.DEEP_CLEANING_PAGE_URI] = {credit:"Get 10% off on first cleaning when pay by card", cash:"Please ensure you have exact change to give the cleaner when they finish."};

        allDesc[URLCONSTANT.WATER_TANK_CLEANING_PAGE_URI] = {credit:"Get 10% off on first cleaning when pay by card", cash:"Please ensure you have exact change to give the cleaner when they finish."};

        allDesc[URLCONSTANT.SOFA_AND_UPHOLSTERY_CLEANING_PAGE_URI] = {credit:"Get 10% off on first cleaning when pay by card", cash:"Please ensure you have exact change to give the cleaner when they finish."};

        allDesc[URLCONSTANT.CARPET_CLEANING_PAGE_URI] = {credit:"Get 10% off on first cleaning when pay by card", cash:"Please ensure you have exact change to give the cleaner when they finish."};

        allDesc[URLCONSTANT.MATTRESS_CLEANING_PAGE_URI] = {credit:"Get 10% off on first cleaning when pay by card", cash:"Please ensure you have exact change to give the cleaner when they finish."};

        allDesc[URLCONSTANT.MAINTENANCE_PAGE_URI] = {credit:"Get 10% off on first booking when pay by card", cash:"Please ensure you have exact change to give the handyman when they finish."};

        allDesc[URLCONSTANT.AC_MAINTENANCE_PAGE_URI] = {credit:"Get 10% off on first booking when pay by card", cash:"Please ensure you have exact change to give the AC technician when they finish."};

        allDesc[URLCONSTANT.ELECTRICIAN_PAGE_URI] = {credit:"Get 10% off on first booking when pay by card", cash:"Please ensure you have exact change to give the technician when they finish."};

        allDesc[URLCONSTANT.PLUMBING_PAGE_URI] = {credit:"Get 10% off on first booking when pay by card", cash:"Please ensure you have exact change to give the technician when they finish."};

        allDesc[URLCONSTANT.PEST_CONTROL_PAGE_URI] = {credit:"Get 10% off on first booking when pay by card", cash:"Please ensure you have exact change to give the technician when they finish."};

        //allDesc[URLCONSTANT.PAINTERS_PAGE_URI] = {credit:"Get 10% off on first booking when pay by card", cash:"Please ensure you have exact change to give the technician when they finish."} ;

        return typeof slug != "undefined" && typeof allDesc[slug] != "undefined" ? allDesc[slug] : desc;

    }
    render() {
        var {items, status, showCOD} = this.props;

        var cardError = "";

        if(status == "cancel" || status == "decline"){
            cardError = "An error occurred while trying to add your Credit Card"
        }
        return (
            <React.Fragment>
                <FormTitleDescription title="How would you like to pay?" desc="Please choose your preferred payment method." />
                <div className={this.props.parentClass}>
                    {this.renderList()}
                </div>
                {cardError != "" && (<div className="row"><div className="col text-center text-danger border border-danger p-1">{cardError}</div></div>) }
                <div className="row mt-2 goback">
                    <div className="col-12">
                        <a href="#2" className="text-black"> <i className="fa fa-angle-left"></i> Back</a>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}
function mapStateToProps(state){
    return {
        myCreditCardsData: state.myCreditCardsData
    }
}
PaymentMethods.defaultProps = {
    showCOD: true,
    status:null,
    bid:null,
    tt:null,
    parentClass: 'row',
    childClass: 'col-md-6 mb-3',
    isCreditCardCouponApplied: false,
    items : [
        {id: 0, value: "credit", label: "Credit card", desc: "Get 10% off on first cleaning when pay by card", image: "visa.png", text: ''},
        {id: 1, value: "cash", label: "Cash on delivery", desc: '\u00A0', image: "money.svg", text: "Please ensure you have exact change to give the cleaner when they finish."}
    ],
    isSubscription: false
};
export default connect(mapStateToProps)(PaymentMethods);