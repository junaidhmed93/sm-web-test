import React from "react";
import commonHelper from "../../helpers/commonHelper";
var current_currency = "AED";
class PackageBox extends React.Component{
    constructor(props) {
		super(props);
		this.state = {
			items: props.items
		}
        this.renderList = this.renderList.bind(this);
		this.renderRating = this.renderRating.bind(this);
		this.renderFeature = this.renderFeature.bind(this);
		this.renderStar = this.renderStar.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.isChecked = this.isChecked.bind(this);
		this.isValid = this.isValid.bind(this);
		this.showDetails= this.showDetails.bind(this);
		this.hideDetails= this.hideDetails.bind(this);
	}
	componentWillReceiveProps (newProps) {
		if ( newProps.items !== this.props.items ) {
			this.setState({
				items: newProps.items
			})
		}
	}
    isValid(){
		var {inputValue, validationClasses} = this.props;
		if( validationClasses == 'radio-required' ){
			return Object.keys(inputValue).length == 0 ? 'radio-required' : '';
		}

    }
    renderRating(rating){
		var rating_item  = [];
		var this_var = this;
		Object.keys(rating).forEach(function (key) {
			var rating_element = (<p className="d-flex align-items-center justify-content-between my-3">
				<span>{key}</span>
				<span className="text-primary">
					{ this_var.renderStar(rating[key]) }
				</span>
			</p>);
			rating_item.push(rating_element)
		});

		return rating_item;
	}
	renderStar(rate){
		var stars = [];
		var star = "";
		for(var i = 1; i <= rate; i++){
			star = (<i className="fa fa-star"></i>);
			stars.push(star);
		}
		return(<span className="text-primary">{stars}</span>);
	}
	renderFeature(features){
		var feature_item  = [];
		var itemClass = "";
		var spanClass = "";
		Object.keys(features).forEach(function (key) {
			itemClass = features[key] == "Yes" ? "fa fa-check" : "fa fa-times";
			spanClass = features[key] == "Yes" ? "text-success" : "text-danger";
			var feature_element = (<p className="d-flex align-items-center justify-content-between my-3">
				<span>{key}</span>
				<span className={spanClass}>
					<i className={itemClass}></i>
				</span>
			</p>);
			feature_item.push(feature_element)
		});

		return feature_item;
	}

    handleChange(event, item){
        var {name, onInputChange, typeOfPackage} = this.props;
        if(typeOfPackage == "booking"){
        	var returnVal = {
        		id:item.id,
				name: item.name,
				rate: item.rate,
				data_no_of_cleaners: item.data_no_of_cleaners,
                data_no_of_hours: item.data_no_of_hours,
                data_cleaning_materials: item.data_cleaning_materials
			}
            if(typeof item.weekdays != "undefined") {
                returnVal["weekdays"] = item.weekdays
            }
            onInputChange(name, returnVal);
		}else if(typeOfPackage == "movingBooking"){
			var returnVal = {
        		id:item.id,
				name: item.name,
				move_provider_category: item.move_provider_category,
				new_pricing: item.new_pricing
			}
			
			onInputChange(name, returnVal);
            
		}else{
			onInputChange(name, item);
		}

    }
    isChecked(item){
        var {inputValue} = this.props;
		//console.log(inputValue, item);
        return inputValue.id == item.id;
	}
	/*handleCustomChange = (idx) => (name, value) => {
        var new_name = name.replace("_"+idx,"")

        const newMattress = this.state.mattress.map((mattres, sidx) => {
            if (idx !== sidx) return mattres;
            return { ...mattres, [new_name]: value };
		});
	}*/
	showDetails = (idx) => () => {
		const newItems  =  this.state.items.map((item, sidx) => {
			if (idx !== sidx) return item;
			return { ...item, ["show_details"]: true };
		});
		// console.log("showDetails",newItems);

		this.setState({ items: newItems });
		
	}
	hideDetails = (idx) => () => {
		const newItems  =  this.state.items.map((item, sidx) => {
			if (idx !== sidx) return item;
			return { ...item, ["show_details"]: false };
		});
		
		this.setState({ items: newItems });
	}
	renderList(){
		var transport_fee = typeof this.props.transport_fee !="undefined" ? this.props.transport_fee : 0;
		var total_price = 0;

        var {inputValue, typeOfPackage} = this.props;

		//console.log("renderList", inputValue);
		
		var priceItem = "";

		var showMoreDetails = false;
	
		return this.state.items.map((item, index) => {
			if(item.new_pricing || (typeof item.rate != "undefined" && item.rate)){
				total_price = typeof item.new_pricing !="undefined" ? parseInt(item.new_pricing)+parseInt(transport_fee) : item.rate;
				priceItem = (<li className="list-group-item d-none d-md-block p-3">
					<p className="d-flex align-items-center justify-content-center m-0">
						<span className="text-warning">{current_currency}</span>
						<span className="text-warning h1 font-weight-bold">{total_price}</span>
						{typeof item.per_month != "undefined" && <span className="text-warning per-month"> /Month</span>}
					</p>
					<p className="m-0 text-center text-black h5">Excluding VAT</p>
				</li>);
				showMoreDetails = typeof item.show_details == "undefined" || !item.show_details ? false : true;
	            return(
					<div key={'pack'+index} className={"col-12 col-sm-6 col-md-4 "+commonHelper.slugify(item.name)}>
						<div className="mb-3">
							<input id={"pa-"+item.id} className="d-none" type="radio" value={item.id} name={this.props.name} onChange={(e)=> this.handleChange(e, item)} checked={this.isChecked(item)} />
								<label className="d-block package payment pointer" htmlFor={"pa-"+item.id}>
									<div className="card">
										<div className="card-body p-3 text-center package-title">
											<h5 className="card-title m-0 h3">{item.name}</h5>
											{ /*typeof item.tagline != "undefined" && <h6 className="card-subtitle m-0 h5">{item.tagline}</h6>*/}
											{ typeof item.tagline != "undefined" && (
												<div className="my-2"><span className={"tag btn-inline-block btn py-1 "+item.btnClass}>{item.tagline}</span></div>
											)}
										</div>
										<ul className="list-group list-group-flush">
										{ typeOfPackage == "movingBooking" && priceItem }
											<li className="list-group-item px-3 d-flex d-md-none align-items-end justify-content-between">
												<div>
													<p className="m-0">
														<span className="text-warning h2 m-0 font-weight-bold">{total_price}</span>
														<span className="text-warning m-0">{current_currency}</span>
													</p>
												</div>
												<div>
													<p className="m-0 text-center text-black">*excluding VAT</p>
												</div>
											</li>
											{ typeof item.great_for != "undefined" && (<li className="list-group-item great-for-item p-3 text-center">
												<span className="text-primary m-0 text-uppercase h5">What you'll get</span>
												<div className="great-for mt-2 text-secondary">{item.great_for}</div>
											</li>)}
											{ typeof item.rating != "undefined" && (<li className={typeOfPackage == "movingBooking" ? "list-group-item px-3 py-0 border-bottom-0" : "list-group-item px-3 py-0"}>
												{this.renderRating(item.rating)}
											</li>)}
											{ typeof item.features != "undefined" && (<li className={ typeOfPackage == "movingBooking" && !showMoreDetails ? "list-group-item px-3 py-0 collapse-item standard-detail d-none" : "list-group-item px-3 py-0 collapse-item border-bottom-0 standard-detail" }>
												{this.renderFeature(item.features)}
											</li>)}
												
											{ typeOfPackage != "movingBooking" && priceItem }
										</ul>
										{ ( typeOfPackage == "movingBooking" && typeof item.features != "undefined" ) && (
										<div data-toggle="collapse" aria-expanded="false" data-target=".standard-detail" className="card-body border-bottom py-2 text-center">
											<div>
												<p className={ !showMoreDetails ? "text-center m-0 text-primary show-details" : "text-center m-0 text-primary show-details d-none" } onClick={ this.showDetails(index) }>
													<span>Show Details</span>
													<i className="fa fa-angle-down ml-2"></i>
												</p>
											</div>
											<div>
												<p className={ showMoreDetails ? "text-center m-0 text-primary show-details" : "text-center m-0 text-primary show-details d-none" } onClick={ this.hideDetails(index) }>
													<span>Hide Details</span>
													<i className="fa fa-angle-up ml-2"></i>
												</p>
											</div>
										</div>) }
										<div className="card-body package-select text-uppercase text-center">
											{ this.isChecked(item) ? "Selected" : "Select Package" }
										</div>
									</div>
								</label>
						</div>
					</div>
	            );
            }
        });
	}
    render() {
    	const {validationClasses, packageError, inputValue} = this.props;
		var erroStyle = {position: 'relative', top: 'auto', margin: 0};
        return (
			<React.Fragment>
				{ packageError && (typeof inputValue == "object" && Object.keys( inputValue ).length == 0 ) ? (
					<div className="row my-3">
						<div className="col-12 d-flex">
							<p className="error-msg d-block" style={erroStyle}>Please select package.</p>
						</div>
					</div>
				) : '' }
				<div className="row">
					{this.renderList()}
				</div>
			</React.Fragment>
        );
    }
}

PackageBox.defaultProps = {
  validationClasses: '',
  typeOfPackage: ''
};
export default PackageBox;
