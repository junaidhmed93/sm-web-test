import React from "react";
import ReactDOM from "react-dom";
import { withRouter } from 'react-router';
import {connect} from "react-redux";
import {fetchBookingFaqs} from "../../actions/index";
import Accordion from "../Accordion";
import Loader from "../Loader";
import locationHelper from "../../helpers/locationHelper";

class ServiceQuestions extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            show: false,
            questions: [],
            showLoader: false
        }
        this.locationHelperInit = locationHelper.init.bind(this);
        this.locationHelperInit();
        this.toggleCollapse = this.toggleCollapse.bind(this);
    }
    toggleCollapse(){
        this.setState({
            show: !this.state.show,
        })
        // Fetch Data if Not Exist
        if(this.state.questions && !this.state.questions.length ){
            this.setState({
                showLoader: true
            })
            var slug = ( typeof this.props.slug != "undefined" && this.props.slug != "" ) ? this.props.slug  : this.props.match.params.slug;

            fetchBookingFaqs(this.props.match.params.city, slug, this.props.typeOfFlow, this.props.lang).then((res)=>{
                this.setState({
                    questions: res,
                    showLoader: false
                })
            })
        }
    }

    render() {
        const {show, questions} = this.state;
        const simplePanel = true;
        return (
            <div className="service-questions accordion">
                <div className="card border-0">
                    <div className="card-header border p-0">
                        <h5 className="mb-0">
                        <button className="btn bg-white p-3 d-flex w-100 align-items-center justify-content-between rounded-0" onClick={()=> this.toggleCollapse()} >
                        <span>{locationHelper.translate("HAVE_A_QUESTION")}</span>
                        <i className="fa fa-angle-down"></i>
                        </button>
                        </h5>
                    </div>
                    <div className={"border p-2 collapse" + (show ? ' show' : '')}>
                        {(questions && questions.length) ? (<Accordion panels={questions} design="simple" simplePanel={simplePanel} />) : (this.state.showLoader?<Loader/>:'')}
                        {(!this.state.showLoader && (!questions || questions.length == 0))?<button className={"btn btn-block text-left bg-white text-xs m-0"}>For queries please contact +97144229639.</button>:''}
                    </div>
                </div>
            </div>
        );
    }
}

ServiceQuestions.defaultProps = {
    typeOfFlow : "booking"
}
function mapStateToProps(state){
    return {
        currentCity: state.currentCity,
        lang: state.lang
    }
}
export default withRouter(connect(mapStateToProps)(ServiceQuestions));
