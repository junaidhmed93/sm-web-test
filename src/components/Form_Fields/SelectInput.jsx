import React from "react";

class SelectInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            searchKey: '',
            isSearchable: props.searchable ? props.searchable : false,
            options: !!props.options ? props.options : [],
            titleClass: !!props.titleClass ? props.titleClass:null
        };

        this.timeOutId = null;

        this.onClickHandler = this.onClickHandler.bind(this);
        this.renderList = this.renderList.bind(this);
        this.setOption = this.setOption.bind(this);
        this.showLabel = this.showLabel.bind(this);
        this.containerClicked = this.containerClicked.bind(this);
        this.handleOutsideClick = this.handleOutsideClick.bind(this);
        this.search = this.search.bind(this);

    }
    componentWillUnmount() {
        document.removeEventListener('click', this.handleOutsideClick, false);
    }
    containerClicked(e) {        
        // console.log('containerClicked');
        if (e.target.name == 'searchBox')
            return;
        if (!this.state.isOpen) {
            document.addEventListener('click', this.handleOutsideClick, false);
        } else {
            document.removeEventListener('click', this.handleOutsideClick, false);
        }
    }
    handleOutsideClick(e) {
        if (this.node.contains(e.target)) {
            return
        }
        // To ensure that one Select component is listen to the event
        document.removeEventListener('click', this.handleOutsideClick, false);
        this.setState({
            isOpen: false
        });
    }
    search(e) {
        this.setState({
            searchKey: e.target.value
        })


    }
    renderList(val) {

        const results = this.props.options.map((item,index) => {
            const classItem = !!this.state.titleClass ? 'dropdown-item '+this.state.titleClass : 'dropdown-item' ;
            return (
                <a key={index} name={item.label} onClick={() => this.setOption(item)} className={classItem}>{item.label}</a>
            );
        });
       
        let filtered = results;
        if (val != '') {
            let data = results.filter(b=> b.props.name.toLowerCase().indexOf(val.toLowerCase()) != -1);
            
            if (data.length == 0)
                
            data.push(<a className="dropdown-item">No results matched "{val}"</a>);
            filtered = data;
        }
        return filtered;
    }
    onClickHandler() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
    setOption(item) {
        this.props.onInputChange(this.props.name, item);
        this.setState({
            isOpen: false
        });
    }
    showLabel() {
        if (this.props.inputValue) {
            for (var i = 0; i < this.props.options.length; i++) {
                if (this.props.options[i].value == this.props.inputValue.value) {
                    return this.props.options[i].label;
                    break;
                }
            }
        }
        return this.props.label;
    }

    render() {


        var errorClass = "";

        if (typeof this.props.inputValue.value != "undefined") {
            if (this.props.inputValue.value) {
                errorClass = "";
            } else {
                errorClass = " invalid-value";
            }
        } else {
            errorClass = "";
        }

        //( this.props.inputValue.value ? '': ' invalid-value')
        var searchable =  typeof this.props.searchable != "undefined" ? this.props.searchable : false;
        const showSearch = (this.state.isSearchable || searchable );
        const searchPhrase = this.state.searchKey;
        let results = this.renderList(searchPhrase);
        
        
        return (
            <div className="dropdown" ref={node => { this.node = node; }} onClick={(e) => this.containerClicked(e)}>
                <button onClick={() => this.onClickHandler()} className={`btn-radio flex-row border rounded btn-block justify-content-between dropdown-toggle px-3 pointer"  ${errorClass}  ${this.state.titleClass}`} type="button">
                    {this.showLabel()}

                </button>

                <div className={"dropdown-menu w-100 limit-dropdown-height" + (this.state.isOpen ? " show" : '')}>
                    {showSearch ? <div className="dropdown-item"><input type="text" name="searchBox" onChange={this.search} className="form-control"  autoComplete="off" /></div>
                        : null}
                    {results}
                </div>
            </div>
        );
    }
}
SelectInput.defaultProps = {
    searchable : false
};
export default SelectInput;
