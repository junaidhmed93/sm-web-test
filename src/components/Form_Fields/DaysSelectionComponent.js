import React from "react";
import CheckRadioBoxInput from './CheckRadioBoxInput';

class DaysSelectionComponent  extends CheckRadioBoxInput{
    selectedOptionIsValid(selectedItem,inputValue){
        var result = true;
        if(inputValue && inputValue instanceof Array){
            for(const input of inputValue){
                const daysValueDifference=Math.abs(selectedItem.id-input.id);
               const roundLoopCount = this.props.items.length - 1;
                if(Math.abs(daysValueDifference)==1 || daysValueDifference ==roundLoopCount){
                    result = false;
                    // Please select two nonconsecutive days.
                    break;
                }
            }
        }
        return result;
    }
}


export default DaysSelectionComponent;
