import React from "react";
import locationHelper from '../../helpers/locationHelper';

class RateInput extends React.Component{
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange(event){
        this.props.onInputChange(this.props.name, event.target.value);
    }
    render() {
        var {name, inputValue} = this.props;
        
        return (
            <fieldset className={this.props.ratingClass+" ratable"}>
                <input type="radio" id="star5" name={name} value="5" defaultChecked ={ inputValue == "5"} onChange={this.handleChange} />
                <label htmlFor="star5"></label>
                <input type="radio" id="star4" name={name} value="4" defaultChecked ={ inputValue == "4"} onChange={this.handleChange}/>
                <label htmlFor="star4"></label>
                <input type="radio" id="star3" name={name} value="3" defaultChecked ={ inputValue == "3"} onChange={this.handleChange}/>
                <label htmlFor="star3"></label>
                <input type="radio" id="star2" name={name} value="2" defaultChecked ={ inputValue == "2"} onChange={this.handleChange}/>
                <label htmlFor="star2"></label>
                <input type="radio" id="star1" name={name} value="1" defaultChecked ={ inputValue == "1"} onChange={this.handleChange}/>
                <label htmlFor="star1"></label>
                {this.props.ratingDetails?<div className="rating-details"><span className="left">{locationHelper.translate('POOR')}</span><span className={'right'}>{locationHelper.translate('EXCELLENT')}</span></div>:''}
            </fieldset>
        );
    }
}
RateInput.defaultProps = {
    ratingClass : "inline",
    ratingDetails: false
};

export default RateInput;
