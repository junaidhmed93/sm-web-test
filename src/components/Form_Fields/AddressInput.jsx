import React from "react";
import {updateSingleInputValid} from "../../actions/index";
import { connect } from "react-redux";
class AddressInput extends React.Component{
    constructor(props) {
        super(props);

        this.refInput = React.createRef();
        this.focusTextInput = this.focusTextInput.bind(this);
        this.onChangeHandler = this.onChangeHandler.bind(this);
    }
    componentDidMount(){
		const addressInput = this.refInput.current;
		const {onInputChange, lang} = this.props;
        var autocomplete = new google.maps.places.Autocomplete(addressInput);

        // Set initial restrict to the greater list of countries.
        autocomplete.setComponentRestrictions(
            {'country': ['AE']});

        // Specify only the data fields that are needed.
        autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);

        autocomplete.addListener('place_changed', function() {
          updateSingleInputValid(addressInput, lang);
          onInputChange(addressInput.name, addressInput.value);
        });
	}
    focusTextInput(){
        this.refInput.current.focus();
    }
    onChangeHandler(event){
        const {onInputChange, lang} = this.props;
       updateSingleInputValid(this.refInput.current, lang);
       onInputChange(event.target.name, event.target.value);
    }

    render() {
        var {inputValue, name, InputType, label, validationClasses} = this.props;

        return (
            <div className={"floating-label " + (inputValue ? ' active' : '')} ref={node => { this.node = node; }} >
                <input 
                type={InputType} 
                name={name} 
                value={inputValue} 
                ref={this.refInput}
                onChange={this.onChangeHandler} 
                placeholder= '' 
                className={"btn-radio form-control p-3 border rounded" + (validationClasses.length ? ' checkpoint '+ validationClasses : '')}
                />
                <i className="fa fa-check text-success input-valid"></i>
                <label onClick={this.focusTextInput}>{label}</label>
                <span className="error-msg"></span>
            </div>
        );
    }
}

AddressInput.defaultProps = {
    validationClasses: ''
};
function mapStateToProps(state) {
    return {
        currentCity: state.currentCity,
        lang: state.lang
    }
}
export default connect(mapStateToProps)(AreaSuggestionInput);
