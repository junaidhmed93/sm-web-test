import React from "react";
import Logos from "../Logos";
import locationHelper from "../../helpers/locationHelper";
import { connect } from "react-redux";
class SubmitElement extends React.Component{
    constructor(props) {
        super(props);
        this.locationHelperInit = locationHelper.init.bind(this);
        this.locationHelperInit();
    }


    render() {
        return (
            <div className="row">
                <div className={"col " + (this.props.floatedRight ? "text-right" :"text-center")}>
                    <input type="submit" className="btn btn-primary btn-lg px-5 mb-3 mt-5 rounded" value={locationHelper.translate("SUBMIT")} />
                </div>
            </div>
        );
    }
}

SubmitElement.defaultProps = {
    floatedRight: false
};
function mapStateToProps(state) {
    return {
        currentCity: state.currentCity,
        lang: state.lang
    }
}

export default connect(mapStateToProps)(SubmitElement);
