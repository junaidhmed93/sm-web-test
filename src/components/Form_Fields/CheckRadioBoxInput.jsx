import React from "react";
import {withCookies} from "react-cookie";
class CheckRadioBoxInput extends React.Component{
    constructor(props) {
        super(props);
        this.renderList = this.renderList.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.isChecked = this.isChecked.bind(this);
        this.selectedOptionIsValid = this.selectedOptionIsValid.bind(this);
        this.isValid = this.isValid.bind(this);

    }
    isValid(){
		var {InputType, inputValue, validationClasses} = this.props;
		if( InputType == 'checkbox' ){
			if( validationClasses == 'min-check-1' ){
				return inputValue.length == 0 ? 'min-check-1' : '';
			}
			if( validationClasses == 'min-check-2' ){
				return inputValue.length < 2 ? 'min-check-2' : '';
			}
		}else{
            if( validationClasses == 'radio-required' ){
                return inputValue.length == 0 ? 'radio-required' : '';
            }
        }

    }
    maxItemsNotSelected(maxSelectionCount,inputValue){
       return !maxSelectionCount || ( maxSelectionCount &&  maxSelectionCount > inputValue.length) ;
    }
    //components can override this functionality to check if selected option is valid or not
    selectedOptionIsValid(item,inputValue){
        return true;
    }
    handleChange(event, item){
        var {InputType, name, onInputChange, inputValue} = this.props;

        if( InputType === 'radio' ){ // If Radio Input then update the value
            if( typeof item.isQuotes !="undefined" && item.isQuotes ){
                onInputChange(name, item);
                window.location = item.quotesLink;
            }else {
                onInputChange(name, item);
            }
        }else{ // If Checkbox group
            if(event.target.checked){ // Add checkbox value to the Array and call to update
                if(typeof this.props.maxSelectionCount != "undefined") {
                    if ((this.maxItemsNotSelected(this.props.maxSelectionCount, inputValue) && this.selectedOptionIsValid(item, inputValue))) {
                        inputValue.push(item);
                        onInputChange(name, inputValue);
                    }
                    else {
                        // Please uncheck previously selected day to select a new one.

                        //TODO need to show error for max limit reached. please unselect some items.
                    }
                }else{
                    inputValue.push(item);
                    onInputChange(name, inputValue);
                }
            }else{ // Remove checkbox value from the Array and call to update
                inputValue = inputValue.filter((obj)=> obj.value != item.value);
                onInputChange(name, inputValue);
            }
        }
    }
    handleQuotesLinkClick(event, item){
        var {InputType, name, onInputChange, inputValue} = this.props;
        if( typeof item.isQuotes !="undefined" && item.isQuotes ){
            onInputChange(name, item);
            //window.location = item.quotesLink;
        }
    }
    isChecked(item){
        var {InputType, inputValue} = this.props;
        if( InputType === 'radio' ){
            return inputValue.value == item.value;
        }else{
            return inputValue.filter((obj)=> obj.value == item.value).length > 0;
        }
    }

    renderList(){
        //console.log(this.props.items);

        return this.props.items.map((item) => {
            return(
                <div key={item.id} className={this.props.childClass}>
                    <label className="btn-block pointer">
                        <input type={this.props.InputType} name={this.props.name} className="d-none" value={item.value} onChange={(e)=> this.handleChange(e, item)} checked={this.isChecked(item)} label={item.label} />
                        <span className="btn-radio border rounded">
                            { typeof item.tag != "undefined" && <span className="tag btn-inline-block btn btn-warning py-1 mb-2">{item.tag}</span> }
                            {item.label}
                            { ( typeof item.isQuotes !="undefined" && item.isQuotes ) && (<a onClick={(e)=> this.handleChange(e, item)} href={item.quotesLink} className="text-primary">Get Quotes</a>)}
                            { typeof item.desc != "undefined" && <span className={typeof item.descClass ? item.descClass : "desc mt-2"}>{item.desc}</span> }
                        </span>
                    </label>
                </div>
            );
        });
    }
    render() {
    	const {validationClasses, InputType} = this.props;
        //validationClasses.length && InputType=='checkbox'
        return (
            <div className={this.props.parentClass}>
            	{ validationClasses.length ? (
                	<div className="col-12 d-flex">
                        <div className={"checkpoint "+this.isValid()} {...(typeof this.props.validationMessage != "undefined" ? {'data-validationmsg': this.props.validationMessage} : {} ) }></div></div>
				) : '' }
				
                {this.renderList()}
            </div>
        );
    }
}

CheckRadioBoxInput.defaultProps = {
  parentClass: 'row mb-2',
  childClass: 'col-4 col-sm-3 mb-3 d-flex',
  validationClasses: ''
};

export default CheckRadioBoxInput;
