import React from "react";
import TextFloatInput from "./TextFloatInput";
import {connect} from "react-redux";
import moment from 'moment';
import CheckRadioBoxInput from "./CheckRadioBoxInput";
import {updateSingleInputValid} from "../../actions/index";
var timings = [];
class TimePicker extends React.Component{
    constructor(props) {
        super(props);

        this.refInput = React.createRef();
        this.handleChange = this.handleChange.bind(this);
        this.toggleTimeSlot = this.toggleTimeSlot.bind(this);
        this.hideTimeSlot = this.hideTimeSlot.bind(this);
        this.getTimeSlot = this.getTimeSlot.bind(this);
        this.setWrapperRef = this.setWrapperRef.bind(this);
        this.handleClickOutside = this.handleClickOutside.bind(this);
        this.isSlotAvailable = this.isSlotAvailable.bind(this);
        

        var bookingTime = props.inputValue == "" ? {value:'', label: ''} : props.inputValue;
        var startDate = typeof props.startDate != "undefined" ? props.startDate : props.dateAndTime

        this.state = {
            slots:[],
            show_time_slot : false,
            booking_time : bookingTime,
            currentDate: moment(startDate)
        }
    }
    componentWillReceiveProps (newProps) {
        if (newProps.bookingDateTimeAvailabilityReducer !== this.props.bookingDateTimeAvailabilityReducer) {
            let bookingDateTimeAvailability = newProps.bookingDateTimeAvailabilityReducer;
            timings = bookingDateTimeAvailability.bookingTimings != "undefined" ? bookingDateTimeAvailability.bookingTimings : [];
            if(timings.length){
                //console.log("bookingDateTimeAvailability.bookingTimings", bookingDateTimeAvailability.bookingTimings);
                this.renderTimingSlot(timings);
            }
        }
        if (newProps.startDate !== this.props.startDate) {
            this.setState({
                currentDate: moment(newProps.startDate)
            })
        }
        if (newProps.booking_date !== this.props.booking_date) {
            //console.log('newProps.booking_date', newProps.booking_date)
            this.renderTimingSlot(timings, newProps.booking_date);
        }
    }
    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
        var bookingDateTimeAvailability = this.props.bookingDateTimeAvailabilityReducer;
        //console.log("bookingDateTimeAvailability", bookingDateTimeAvailability)
        timings = typeof bookingDateTimeAvailability.bookingTimings != "undefined" ? bookingDateTimeAvailability.bookingTimings : [];
        this.renderTimingSlot(timings);
    }
    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }
    componentDidUpdate(prevProps, prevState){
        let {lang} = this.props;
        if( prevState.booking_time != this.state.booking_time ){
            updateSingleInputValid(this.refInput.current, lang);
        }
    }
    handleChange(){
        //console.log(this.props);
        /*var {name, onInputChange} = this.props;
         var dateFormat = day.getUTCDate() + '/' + (day.getUTCMonth() + 1) + '/' + day.getUTCFullYear();
         this.props.onInputChange(name, dateFormat);*/
    }
    timeSlotElements($time){

        var $time_label = $time >= 12 ? ($time-12)+" PM" : $time+" AM";

        if($time_label == "0 PM"){
            $time_label = "12 PM";
        }

        var $timeId = $time < 10 ? "0"+$time+"00" : $time+"00" ;

        var $timeValue = $time < 10 ? "0"+$time+":00" : $time+":00" ;

        var slot = {"id": $timeId, "value": $timeValue, "label": $time_label};

        return slot;
    }
    /*componentWillReceiveProps(newProps) {
        
    }*/
    renderTimingSlot(new_timings = [], newBookingDate = ""){
        let {currentCity, bookingDateTimeAvailabilityReducer, booking_date, dateAndTime} = this.props;
        let {booking_time}= this.state;
        var timing_index = 0;
        var slots = [];
        var $timings = ( typeof new_timings != "undefined" && new_timings.length ) ? new_timings : timings; //typeof bookingDateTimeAvailabilityReducer.bookingTimings != "undefined" ? bookingDateTimeAvailabilityReducer.bookingTimings : [];
        var $now = new Date();
        if($timings.length){
            var $m = $timings.length;
            let isDateFallonRamdan = false;

            booking_date =  newBookingDate != "" ? newBookingDate : booking_date;

            booking_date = (typeof booking_date != "undefined" && booking_date != "") ? booking_date.split('-').reverse().join('-') : moment(dateAndTime).add(1, 'days');

            let selectedDate = moment(booking_date) ;

            var ramdanTiming = $timings[$m-1];

            let unixTime = selectedDate.format('x');
            
            if ((unixTime <= ramdanTiming.till && unixTime >= ramdanTiming.from)) {
                timing_index = $m-1;
                isDateFallonRamdan = true;
            }
            
            var $defaultTime = $m != 0 ? $timings[timing_index] : []; // default
            var $s = "";
            var $e = "";
            var $time = "";

            var startHour = parseInt( $defaultTime.startHour );

            var workingHours = typeof this.props.workingHours != "undefined" ? this.props.workingHours : parseInt( $defaultTime.workingHours );

            var endTime =  startHour + workingHours;

            for(var i = startHour; i<= endTime; i++){
                var slot = this.timeSlotElements(i);
                slots.push(slot);
            }

            if(this.props.disableFirstSlot){
                slots.shift()
            }
            
            this.setState({
                slots: slots
            })
        }
    }
    isSlotAvailable(slots = ""){
        var booking_time = {value:'', label: ''};
        var selectedSlotAvailable = [];
        slots = slots == "" ? this.state.slots : slots;
        if(this.props.booking_date != ""){
            slots =  slots;
        }
        if(this.state.booking_time != "") {
            selectedSlotAvailable = slots.filter(
                (item) => (item.label == this.state.booking_time.label
            ));
            if( selectedSlotAvailable.length ){
                booking_time = this.state.booking_time;
            }
        }
        return booking_time;
    }
    toggleTimeSlot(){
        this.setState({
            show_time_slot: !this.state.show_time_slot
        });

    }
    hideTimeSlot(){
        this.setState({
            show_time_slot: false
        });

    }
    getTimeSlot(name, value){
        var {InputType, onInputChange, lang} = this.props;
        updateSingleInputValid(this.refInput.current, lang);
        this.setState({
            booking_time: value,
            show_time_slot: !this.state.show_time_slot
        })
        onInputChange(name, value);
    }

    /**
     * Set the wrapper ref
     */
    setWrapperRef(node) {
        this.wrapperRef = node;
    }

    handleClickOutside(event) {
        if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
            this.hideTimeSlot();
        }
    }
    render() {
        
        var {slots, booking_time} = this.state;// this.renderTimingSlot();
        
        /*if(this.props.booking_date != ""){
            slots =  this.updateTimingSlot(this.props.booking_date);
        }*/
        var {validationClasses, timeSlotDesc} = this.props;
        var isTimeSlotShow = !this.state.show_time_slot ? "dropdown-menu radio-btn-container booking-time-slots clndr-parent bg-white p-4 border" : "dropdown-menu radio-btn-container booking-time-slots clndr-parent bg-white p-4 border show";
        booking_time = this.isSlotAvailable(slots);
        return (
            <div className={this.props.childClass} ref={this.setWrapperRef}>
                <div className="dropdown">
                    <div className="input-group is-r">
                        <input type="text" 
                        ref={this.refInput} 
                        value={booking_time.label} 
                        className={"btn-radio bg-white form-control p-3 border rounded btn-date input-placeholder"  + (validationClasses.length ? ' checkpoint '+ validationClasses : '')} 
                        show_time_slot={this.state.show_time_slot} placeholder="Select time" readOnly  
                        onClick={ (e)=> this.toggleTimeSlot() }  
                        name={this.props.name}
                        {...(typeof this.props.validationMessage != "undefined" ? {'data-validationmsg': this.props.validationMessage} : {} ) }
                        />
                        <div className="is-a middle icon-right" style={{zIndex: '4'}}>
                            <span>
                                <a href="#" onClick={ (e)=> {this.toggleTimeSlot(); e.preventDefault()} }>
                                    <i className="fa fa-clock-o text-secondary"></i>
                                </a>
                            </span>
                        </div>
                        <span className="error-msg"></span>
                    </div>
                    <div className={isTimeSlotShow}>
                        <CheckRadioBoxInput InputType="radio" name="job_time" inputValue={this.state.booking_time} items={slots} onInputChange = { this.getTimeSlot } parentClass="row" childClass="col-6" />
                    </div>
                </div>
                { typeof timeSlotDesc != "undefined" ?  (<div className="small mt-2 px-2">{timeSlotDesc}</div>) : '' }
            </div>
        );
    }

}

TimePicker.defaultProps = {
    validationClasses: '',
    disableFirstSlot: true, // for to disable the 8 clock, 8 clock should not available for lite weight booking
    parentClass: 'row mb-4',
    childClass: 'col-12 col-sm-6 mb-3',
    inputValue: "",
    validationMessage:"Please select time"
};
function mapStateToProps(state){
    return {
        bookingDateTimeAvailabilityReducer: state.bookingDateTimeAvailabilityReducer,
        currentCity: state.currentCity,
        lang: state.lang,
        dateAndTime: state.dateAndTime
    }
}
export default connect(mapStateToProps)(TimePicker);