import React from "react";
import TextFloatInput from "./TextFloatInput";
import {connect} from "react-redux";
import moment from 'moment';
import CheckRadioBoxInput from "./CheckRadioBoxInput";
import {updateSingleInputValid, fetchDateTime, DUBAI} from "../../actions/index";
var timings = [];
/*var timings = [
    {"id":"1","from":null,"till":null,"startHour":"8","workingHours":"9","hours_required":"5","priority":null,"description":"Default timings","status":"1"},
    {"id":"4","from":"2018-05-16 00:00:00","till":"2018-06-16 00:00:00","startHour":"8","workingHours":"7","hours_required":"5","priority":null,"description":"Ramazan 2018","status":"1"}
];*/
class CleaningTimePicker extends React.Component{
    constructor(props) {
        super(props);

        this.refInput = React.createRef();

        this.handleChange = this.handleChange.bind(this);

        this.toggleTimeSlot = this.toggleTimeSlot.bind(this);
        this.hideTimeSlot = this.hideTimeSlot.bind(this);

        this.getTimeSlot = this.getTimeSlot.bind(this);

        this.updateTimingSlot = this.updateTimingSlot.bind(this);

        this.setWrapperRef = this.setWrapperRef.bind(this);
        this.handleClickOutside = this.handleClickOutside.bind(this);
        this.isSlotAvailable = this.isSlotAvailable.bind(this);

        var startDate = typeof this.props.startDate != "undefined" ? this.props.startDate : props.dateAndTime

        var booking_date = props.booking_date == "" ? moment(startDate).format("YYYY-MM-DD") : props.booking_date;

        var bookingTime = props.inputValue == "" ? {value:'', label: ''} : props.inputValue;

        this.state = {
            slots:[],
            show_time_slot : false,
            booking_time : bookingTime,
            booking_date: booking_date,
            currentDate: moment(startDate)
        }
    }
    dateChanged(){

    }
    handleChange(){
        //console.log(this.props);
        /*var {name, onInputChange} = this.props;
         var dateFormat = day.getUTCDate() + '/' + (day.getUTCMonth() + 1) + '/' + day.getUTCFullYear();
         this.props.onInputChange(name, dateFormat);*/
    }
    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
        var bookingDateTimeAvailability = this.props.bookingDateTimeAvailabilityReducer;
        //console.log("bookingDateTimeAvailability", bookingDateTimeAvailability)
        timings = typeof bookingDateTimeAvailability.bookingTimings != "undefined" ? bookingDateTimeAvailability.bookingTimings : [];
        this.renderTimingSlot(timings);
    }
    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }
    /**
     * Set the wrapper ref
     */
    setWrapperRef(node) {
        this.wrapperRef = node;
    }

    handleClickOutside(event) {
        if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
            this.hideTimeSlot();
        }
    }
    componentDidUpdate(prevProps, prevState){
        let {lang} = this.props;
        if( prevState.booking_time != this.state.booking_time ){
            updateSingleInputValid(this.refInput.current, lang);
        }
    }
    timeSlotElements($time){

        var $time_label = $time >= 12 ? ($time-12)+" PM" : $time+" AM";

        if($time_label == "0 PM"){
            $time_label = "12 PM";
        }

        var $timeId = $time < 10 ? "0"+$time+"00" : $time+"00" ;

        var $timeValue = $time < 10 ? "0"+$time+":00" : $time+":00" ;

        var slot = {"id": $timeId, "value": $timeValue, "label": $time_label};

        return slot;
    }
    disable_this_week_friday() {
        var currentDate = this.state.currentDate;
        var now = moment(currentDate),
            now_hour = now.hour(),
            now_day = now.day();

        var after_two_days = now.add(2,'day');

        var diable_this_week_friday = false; // flag for disable friday.

        if( now_day >= 3){ //checking today is wednesday or above wednesday
            diable_this_week_friday = true;

            if(now_day == 3 && now_hour < 17){ // if today is wednesday and time is less then 5PM. then don't disable friday.
                diable_this_week_friday = false;
            }
        }

        if( diable_this_week_friday && currentMSBlogCity == "dubai"){

            var this_week_friday = moment(currentDate).weekday(5); // getting this week's friday

            var this_week_friday_selector = ".calendar-day-"+this_week_friday.format('YYYY-MM-DD');

            $(this_week_friday_selector).addClass("inactive"); // disabling this week friday.
        }
    }
    updateTimingSlot(booking_date){
        let {currentCity} = this.props
        var time = timings;

        var new_slots = [];

        var i = 0; // Time indexing to Select Time 0 for Default Timing and 1 for Ramdan
        var isDateFallonRamdan = false;

        booking_date = booking_date.split('-').reverse().join('-');

        //console.log("updateTimingSlot" , time);

        var currentDate = this.state.currentDate;

        //console.log("updateTimingSlot time", time);

        if(time.length) {

            var selectedDate = moment(booking_date),
                now = moment(currentDate), // server time should pass
                nowDay = now.day();
            let unixTime = selectedDate.format('x');
            var $m = time.length;
            var ramdanTiming = time[$m-1];
            if ((unixTime <= ramdanTiming.till && unixTime >= ramdanTiming.from) && currentCity != DUBAI) {
                i = $m-1;
                isDateFallonRamdan = true;
            }

            var day = selectedDate.day(),
                diff = selectedDate.add(1, 'day').diff(now, 'days'),
                hour = now.hour(),
                minutes = now.minutes(),
                startHour = parseInt(time[i].startHour),
                workingHours = parseInt(time[i].workingHours),
                closingHour = startHour + workingHours,
                //selectedTime = $('input[name=job_time]:checked').val(),
                selectableHourStart = startHour,
                hourRequired = parseInt(time[i].hoursRequired),
                closedTime = 0;

            if (hour < startHour) {
                hour = startHour;
                closedTime = 1;
            } else if (hour > closingHour) {
                hour = closingHour;
                closedTime = 1;
            }


            if (diff == 0) {
                selectableHourStart = hour + hourRequired;
                if (minutes > 1 && closedTime == 0) {
                    selectableHourStart++; //skip currently running hour
                }

            } else if (diff == 1 || (diff == 2 && day == 6)) {

                if (diff == 1 && day == 6) { // if its saturday and today is friday( 1 day different )the slot should start from 1pm.
                    selectableHourStart = 13;
                } else {
                    // next day should start by compensating the remaining required hours
                    selectableHourStart = startHour - closingHour + (hour + hourRequired);
                }

            }
            
            //console.log("selectableHourStart", selectableHourStart, closingHour);

            for (selectableHourStart; selectableHourStart <= closingHour; selectableHourStart++) {
                if (selectableHourStart >= startHour) {
                    var slot = this.timeSlotElements(selectableHourStart);
                    new_slots.push(slot)
                }
            }
        }
        //console.log(new_slots);

        return new_slots;
    }
    renderTimingSlot(new_timings = [], selectedDate = ""){
        let {currentCity, dateAndTime} = this.props;
        var timing_index = 0;
        var slots = [];
        var $timings = ( typeof new_timings != "undefined" && new_timings.length ) ? new_timings : timings;
        var $now = new Date();
        var $m = $timings.length;
        var isDateFallonRamdan = false;
        if($m){
            selectedDate = selectedDate == "" ? moment(dateAndTime) : moment(selectedDate);

            var ramdanTiming = $timings[$m-1];

            let unixTime = selectedDate.format('x');
            
            if ((unixTime <= ramdanTiming.till && unixTime >= ramdanTiming.from) && currentCity != DUBAI) {
                timing_index = $m-1;
                isDateFallonRamdan = true
            }

            //console.log(selectedDate, unixTime, isDateFallonRamdan, "isDateFallonRamdan", timing_index)

            var $defaultTime = $m != 0 ? $timings[timing_index] : []; // default
            var $s = "";
            var $e = "";
            var $time = "";
            //timing_index

            var startHour = parseInt( $defaultTime.startHour );

            var workingHours = parseInt( $defaultTime.workingHours );

            var endTime =  startHour + workingHours;

            for(var i = startHour; i<= endTime; i++){
                var slot = this.timeSlotElements(i);
                slots.push(slot);
            }
            this.setState({
                slots: slots
            })
        }
    }
    toggleTimeSlot(){
        this.setState({
            show_time_slot: !this.state.show_time_slot
        });

    }
    hideTimeSlot(){
        this.setState({
            show_time_slot: false
        });

    }
    getTimeSlot(name, value){
        var {InputType, onInputChange} = this.props;
        this.setState({
            booking_time: value,
            show_time_slot: !this.state.show_time_slot
        })
        onInputChange(name, value);
    }
    isSlotAvailable(){
        var booking_time = "";
        var selectedSlotAvailable = [];
        var slots = this.state.slots;
        if(this.props.booking_date != ""){
            slots =  this.updateTimingSlot(this.props.booking_date);
        }
        if(this.state.booking_time != "") {
            selectedSlotAvailable = slots.filter(
                (item) => (item.label == this.state.booking_time.label
            ));
            if( selectedSlotAvailable.length ){
                booking_time = this.state.booking_time.label
            }else{
                booking_time = "";
            }
        }
        return booking_time;
    }
    componentWillReceiveProps (newProps) {
        if (newProps.bookingDateTimeAvailabilityReducer !== this.props.bookingDateTimeAvailabilityReducer) {
            let bookingDateTimeAvailability = newProps.bookingDateTimeAvailabilityReducer;
            timings = bookingDateTimeAvailability.bookingTimings != "undefined" ? bookingDateTimeAvailability.bookingTimings : [];
            if(timings.length){
                this.renderTimingSlot(timings);
            }
        }
        if (newProps.startDate !== this.props.startDate) {
            this.setState({
                currentDate: moment(newProps.startDate)
            })
        }
        if (newProps.booking_date !== this.props.booking_date) {
            this.setState({
                booking_date: newProps.booking_date
            })
        }
    }
    render() {
        //console.log("CleaningTimePickerComponent"+this.props.booking_date);
        var slots = this.state.slots;
        if(this.props.booking_date != ""){
            slots =  this.updateTimingSlot(this.props.booking_date);
        }
        var selectedSlotAvailable = [];

        var booking_time = this.isSlotAvailable();
        
        var {validationClasses} = this.props;

        var isTimeSlotShow = !this.state.show_time_slot ? "dropdown-menu radio-btn-container booking-time-slots clndr-parent bg-white p-4 border" : "dropdown-menu radio-btn-container booking-time-slots clndr-parent bg-white p-4 border show";

        return (
            <div className={this.props.childClass}  ref={this.setWrapperRef}>
                <div className="dropdown">
                    <div className="input-group is-r">
                        <input type="text" ref={this.refInput} value={booking_time} className={"btn-radio bg-white form-control p-3 border rounded btn-date input-placeholder" + (validationClasses.length ? ' checkpoint '+ validationClasses : '')} show_time_slot={this.state.show_time_slot} placeholder="Select time" readOnly  onClick={ (e)=> this.toggleTimeSlot() }  name={this.props.name}/>
                        <div className="is-a middle icon-right" style={{zIndex: '4'}}>
                            <span>
                                <i className="fa fa-clock-o text-secondary"></i>
                            </span>
                        </div>
                        <span className="error-msg"></span>
                    </div>
                    <div className={isTimeSlotShow}>
                        <CheckRadioBoxInput InputType="radio" name="job_time" inputValue={booking_time} items={slots} onInputChange = { this.getTimeSlot } parentClass="row" childClass="col-6" />
                    </div>
                </div>
            </div>
        );
    }

}

CleaningTimePicker.defaultProps = {
  validationClasses: '',
  parentClass: 'row mb-4',
  childClass: 'col-12 col-sm-6 mb-3'
};
function mapStateToProps(state){
    return {
        bookingDateTimeAvailabilityReducer: state.bookingDateTimeAvailabilityReducer,
        dateAndTime: state.dateAndTime,
        currentCity: state.currentCity,
        lang: state.lang
    }
}
export default connect(mapStateToProps)(CleaningTimePicker);