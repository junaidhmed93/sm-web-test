import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Loader from "../components/Loader";
import { withCookies } from "react-cookie";
import stringConstants from '../constants/stringConstants';
import { fetchWalletHistoy } from "../actions";
import locationHelper from "../helpers/locationHelper";

class WalletComponent extends React.Component {
  _isMounted = false;
  constructor(props) {
    super(props);

    this.state = {
      loader: false,
      // walletHistory: props.userProfile.walletHistory,
    }
    this.getWalletHistory = this.getWalletHistory.bind(this);
  }
  componentDidMount() {
    this._isMounted = true;
    this.setState({ loader: true });
    this.getWalletHistory(this.props.signInDetails.access_token);
  }

  getWalletHistory(token) {
    let {fetchWalletHistoy, lang} = this.props;
    fetchWalletHistoy(token, lang).then(res => {

      this.setState({
        loader: false,

      });
    });
  }
  componentWillUnmount() {
    this._isMounted = false;
  }

  renderList() {
    {
      if (this.props.userProfile && this.props.userProfile.walletHistory) {
        const data = this.props.userProfile.walletHistory;
        return (

          <div className="col-lg-9 col-md-9 col-sm-12 col-xs-12 user-right-con">
            <div className='wallet-div'>
              <span className='my-wallet' style={{ marginLeft: '0 !important' }} >{locationHelper.translate('MY_WALLET')}</span>
              <span className='wallet-amount-span wallet-amount-userarea' style={{float:this.props.lang == 'en' ? 'right':'left'}} >{` ${this.props.lang == 'en' ? '+': ''} ${data.currency.code}  ${data.availableBalance} ${this.props.lang == 'ar' ? '+': ''}`} </span>
              <div style={{ clear: 'both' }}></div>
            </div>
            <p style={{ fontSize: '18px', marginTop: '15px', marginBottom: '20px' }}>{locationHelper.translate('USER_AREA_WALLET_TXT')}</p>
            <p style={{ fontSize: '18px', fontWeight: 'bold' }}>{locationHelper.translate('TRANSACTION_HISTORY_TXT')}</p>
                   
            {
              data.transactions && data.transactions.map(item => {
                const date = new Date(item.transactionDate);
                //const type = item.walletActivityType;
                const symbol = item.walletActivityType == stringConstants.CREDIT ? '+ ' : '- ';
                return (
                  <React.Fragment>
                    <div className='wallet-div' style={{ textAlign: 'center' }}>
                      <span style={{ float: this.props.lang == 'en' ? 'left' : 'right' }}>{date.toLocaleDateString()}</span>
                      <span style={this.props.lang == 'en' ?{marginLeft: '15%', float:'left'}:{marginRight: '15%', float:'right' }}>
                        {item.walletActivityType === stringConstants.CREDIT ? locationHelper.translate('WALLET_AMOUNT_RECEIVED') : locationHelper.translate('BOOKING_ID')  + ' ' + item.uuid}
                      </span>
                      <span style={item.walletActivityType === stringConstants.CREDIT ? { marginRight: '8%',float: this.props.lang == 'en' ? 'right':'left', color: '#3bec53' } : { marginRight: '8%',float: this.props.lang == 'en' ?'right':'left', color: '#f92313' }}>
                        {this.props.lang == 'en' ? symbol:''}  {data.currency.code}  {item.amount} {this.props.lang == 'ar' ? symbol:''}
                      </span>
                      <div style={{ clear: 'both' }}></div>
                    </div>
                  </React.Fragment>
                );
              })
            }
          </div>
        );
      } else {
        return (
          <div className="col-lg-9 col-md-9 col-sm-12 col-xs-12 user-right-con">
            <div className='wallet-div'>
              <img src={"../../../../dist/images/wallet-filled-money-tool-dark.png"} className="mh-100" height="48" alt="" />
              <span className='margin-left10 my-wallet' >{locationHelper.translate('MY_WALLET')}</span>
              <span className='wallet-amount-span wallet-amount-userarea'> {` + 0.00`} </span>
              <div style={{ clear: 'both' }}></div>
            </div>
            <p style={{ fontSize: '18px' }}>{locationHelper.translate('EMPTY_WALLET_TXT')}</p>
          </div>
        );
      }
    }

  }

  render() {
    if (!this.state.loader) {
      return (
        this.renderList()
      );
    } else {
      return (
        <main
          className={"col-lg-9 col-md-9 col-sm-12 col-xs-12 user-right-con " + (!this.state.loader ? "show" : "hide")}>
          <Loader /></main>
      );
    }
  }


}


function mapStateToProps(state) {
  return {
    signInDetails: state.signInDetails,
    userProfile: state.userProfile,
    lang : state.lang
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    fetchWalletHistoy
  }, dispatch);
}

export default withCookies(connect(mapStateToProps, mapDispatchToProps)(WalletComponent));
