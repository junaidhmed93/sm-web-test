import React from "react";
import {connect} from "react-redux";
let greatForText = {
    "greatFor":{
        "pest-control-companies": {
            "Cockroaches": [
                "Safe cockroach pest control",
                "Home and garden",
                "Same day service"
            ],
            "Bedbugs": [
                "Safe ant pest control",
                "Home and garden",
                "Same day service"
            ],
            "Ants": [
                "Getting rid of bed bugs",
                "Safe and efficient treatment",
                "Free follow up visit"
            ],
            "General": [
                "Preventative pest control",
                "Pre-move treatment",
                "Treating several pests at once",
            ]
        },
        "maintenance-handyman-companies":{
            "General Handyman":[
                "TV Mounting",
                "Furniture Assembly",
                "Hanging Curtain/Frames",
                "Installing/Replacing lightbulbs",
            ],
            "AC Technician":[
                "AC Cleaning",
                "AC Installation",
                "AC Repair",
            ],
            "Electrician":[
                "In house Cabling",
                "Outlets and switches",
                "Installing light fixtures"
            ],
            "Plumber":[
                "Water heaters/ tanks",
                "Fixing taps, toilets, showers",
                "Installing sinks, tubs, baths"
            ],
        },
        "handyman":{
            "TV Mounting":[
                "TV cabling",
                "TV wall mounting",
                "TV stand mounting",
            ],
            "Furniture Assembly":[
                "Assembly of furniture",
                "Disassembly of furniture",
                "Assembly of IKEA furniture"
            ],
            "Curtain Hanging":[
                "Installing blinds",
                "Hanging curtains",
                "Installing shutters"
            ],
            "Light Bulbs / Spotlights":[
                "Installing spotlights",
                "Replacing lightbulbs",
                "Installing ceiling lights"
            ]
        },
        "ac-maintenance":{
            "AC Cleaning":[
                "Regular cleaning",
                "Flush drain pipes",
                "Periodic maintenance",
            ],
            "AC Repair":[
                "Reducing AC noise",
                "Fixing AC cooling problem",
                "Getting rid of bad smell"
            ],
            "AC Installation":[
                "Installation of any AC",
                "Disassembly of AC systems",
                "Big and small jobs"
            ],
            "AC Duct Cleaning":[
                "Deep cleaning of AC",
                "Removing dust from AC",
                "Removing mold from AC"
            ]
        }
    },
};
class GreatForPopUp extends React.Component{
    constructor(props) {
        super(props);
        this.getLiteWeightGreatFor = this.getLiteWeightGreatFor.bind(this);
        this.renderGreatFor = this.renderGreatFor.bind(this);

    }
    
    renderGreatFor() {
        var {service, subService} = this.props;

        var defaultCls = "col-6 col-sm-6 col-lg-3 border py-4";

        if( typeof service != "undefined") {

            var greatForTexts = this.getLiteWeightGreatFor(service);

            return Object.keys(greatForTexts).map((item, index) => {
                //console.log(item)
                return (
                    <div className={ ( ( index + 1 ) % 2 == 0 ) ? defaultCls+" even" : defaultCls+""}>
                        <div className="text-primary h3 text-center great-for-tle">
                            <span>{item}</span>
                        </div>
                        <hr />
                        <div className="great-for-label h4 text-uppercase my-3 text-center">Great for</div>
                        <ul className="list-unstyled text-center mt-2">
                            {
                                greatForTexts[item].map((text, i) => {
                                    return ( <li key={"link"+i} className="mb-1">{text}</li> );
                                })
                            }
                        </ul>
                    </div>
                )
            });
        }
    }
    getLiteWeightGreatFor(service, subService = "" ){
        const lite_weight_prices = greatForText;

        //console.log("getLiteWeightGreatFor", service, lite_weight_prices["greatFor"][service]);

        return subService != "" ? lite_weight_prices["greatFor"][service][subService] : lite_weight_prices["greatFor"][service];
    }
    render() {
        return (<div className="great-for"><div className="row">{this.renderGreatFor()}</div></div>);
    }
}

function mapStateToProps(state){
    return {
        service_constants: state.serviceConstants,
        dataConstants: state.dataConstants,
        lite_weight_prices: state.liteWeightPrices,
    }
}

export default connect(mapStateToProps)(GreatForPopUp);
