import React from "react";
import { imgixWPBase, relativeURL, quality } from '../imgix';
import { fetchCustomerQuotesHistory, fetchLeadQuotes, DEFAULT_LANG, LANG_AR } from "../actions";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import Loader from "./Loader";
import commonHelper from "../helpers/commonHelper";
import locationHelper from "../helpers/locationHelper";
import LazyLoad from 'react-lazyload';
import StarRating from "./StarRating";
import PlaceholderComponent from '../components/Placeholder';
import moment from "moment/moment";

class MyQuotes extends React.Component {
    _isMounted = false;
    constructor(props) {
        super(props);
        this.locationHelperInit = locationHelper.init.bind(this);
        this.locationHelperInit();
        this.state = {
            loader: false,
            leadsQuotes: [],
            openedLead: [],
            openContactDetails: [],
        }
    }

    componentDidMount() {
        this._isMounted = true;
        let {lang, fetchCustomerQuotesHistory} = this.props
        fetchCustomerQuotesHistory(this.props.signInDetails.access_token, lang).then(() => {
                if (this._isMounted) {
                    this.setState({
                        loader: false
                    })
                }
            });
        this.setState({
            loader: true
        })
    }

    componentWillUnmount() {
        this._isMounted = false;
    }
    fetchQuotesOfLead(leadID, noOfQuotes) {
        let {lang} = this.props;
        var newOpenedLead = this.state.openedLead;
        if (noOfQuotes > 0) {
            if (this.state.openedLead[leadID]) {
                newOpenedLead[leadID] = false;
            } else {
                newOpenedLead[leadID] = true;
                fetchLeadQuotes(this.props.signInDetails.access_token, leadID, lang).then((res) => {
                    var newLeadQuotes = this.state.leadsQuotes;
                    newLeadQuotes[leadID] = res;
                    this.setState({
                        leadsQuotes: newLeadQuotes
                    })
                });
            }
            this.setState({
                openedLead: newOpenedLead
            });
        }
    }
    openContactDetails(quoteID) {
        var newOpenContactDetails = this.state.openContactDetails;
        if (this.state.openContactDetails[quoteID]) {
            newOpenContactDetails[quoteID] = false;
        } else {
            newOpenContactDetails[quoteID] = true;
        }
        this.setState({
            openContactDetails: newOpenContactDetails
        })

    }
    renderLeadsQuotes(quotes) {
        let {lang} = this.props;
        if (typeof quotes != 'undefined' && quotes.data.isHouseholdRequest && quotes.data.householdQuotes) {
            return quotes.data.householdQuotes.map((quote, index) => {
                if ((quote.status == '1' && quote.quotePrice != 'NaN') || quote.status == '3') {
                    return (
                        <div className="card mb-3 container">
                            <div className="row">
                                <div className="col-md-2 pr-0">
                                    <div className="py-3">
                                        <div className="company-logo pointer">
                                            <img className="img-fluid" src={quote.serviceProviderDto.logo} />
                                        </div>
                                    </div>
                                </div>
                                <div className="col py-3 d-flex flex-column">
                                    <div className="row company-detail">
                                        <div className="col company-name">
                                            <span className={"h3 pointer m-0 text-black"}>{quote.serviceProviderDto.name}</span>
                                            <span className={lang == LANG_AR ? "h3 pointer m-0 text-black left": "h3 pointer m-0 text-black right"}>{quote.status == '1' ? quote.quotePrice + ' ' + quote.quoteUnit : locationHelper.translate('SURVEY_REQUIRED')}</span>
                                        </div>
                                    </div>
                                    <div className="row mt-2">
                                        <div className="col-12 col-md-4 col-lg-6 pb-1 pb-md-0">
                                            <div className="d-lg-flex d-md-flex d-sm-flex">
                                                <StarRating rating={quote.serviceProviderDto.averageRating} />
                                                <span className={"pt-1"}>{"(" + (quote.serviceProviderDto.averageRating ? (Math.round(quote.serviceProviderDto.averageRating * 100) / 100) : '0') + '/5)'}</span>
                                            </div>
                                            <button onClick={() => this.openContactDetails(quote.id)} className="btn btn-blue btn-sm mt-2">{(this.state.openContactDetails[quote.id] ? locationHelper.translate("HIDE_CONTACT_DETAILS") : locationHelper.translate("SHOW_CONTACT_DETAILS"))}</button>
                                            <div className={"col-12 mt-2 collapse " + (this.state.openContactDetails[quote.id] ? 'show' : 'hide')}>
                                                <div className="card-detail-items">
                                                    <div className="card-detail-item pbm pln"><strong>{locationHelper.translate("EMAIL")}</strong><span>{quote.serviceProviderDto.contactInformation.email}</span></div>
                                                    <div className="card-detail-item pbm pln"><strong>{locationHelper.translate("PHONE")}</strong><span>{quote.serviceProviderDto.contactInformation.phone}</span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    );
                }

            });
        } else if (typeof quotes != 'undefined' && !quotes.data.isHouseholdRequest && quotes.data.quotes) {
            return quotes.data.quotes.map((quote, index) => {
                if (quote.quotePrice != 'NaN') {
                    return (
                        <div className="card mb-3 container">
                            <div className="row">
                                <div className="col-md-2 pr-0">
                                    <div className="py-3">
                                        <div className="company-logo pointer">
                                            <img className="img-fluid" src={quote.serviceProviderDto.logo} />
                                        </div>
                                    </div>
                                </div>
                                <div className="col py-3 d-flex flex-column">
                                    <div className="row company-detail">
                                        <div className="col company-name">
                                            <span className="h3 pointer m-0 text-black">{quote.serviceProviderDto.name}</span>
                                            <span className={lang == LANG_AR ? "h3 pointer m-0 text-black left": "h3 pointer m-0 text-black right"}>{quote.status == '1' ? quote.quotePrice + ' ' + quote.quoteUnit : locationHelper.translate('SURVEY_REQUIRED')}</span>
                                        </div>
                                    </div>
                                    <div className="row mt-2">
                                        <div className="col-12 col-md-4 col-lg-6 pb-1 pb-md-0">
                                            <div className="d-lg-flex d-md-flex d-sm-flex">
                                                <StarRating rating={quote.serviceProviderDto.averageRating} />
                                                <span className={"pt-1"}>{"(" + (quote.serviceProviderDto.averageRating ? (Math.round(quote.serviceProviderDto.averageRating * 100) / 100) : '0') + '/5)'}</span>
                                            </div>
                                            <button onClick={() => this.openContactDetails(quote.id)} className="btn btn-blue btn-sm mt-2">{(this.state.openContactDetails[quote.id] ? locationHelper.translate("HIDE_CONTACT_DETAILS") : locationHelper.translate("SHOW_CONTACT_DETAILS"))}</button>
                                            <div className={"col-12 mt-2 collapse " + (this.state.openContactDetails[quote.id] ? 'show' : 'hide')}>
                                                <div className="card-detail-items">
                                                    <div className="card-detail-item pbm pln"><strong>{locationHelper.translate("EMAIL")}</strong><span>{quote.serviceProviderDto.contactInformation.email}</span></div>
                                                    <div className="card-detail-item pbm pln"><strong>{locationHelper.translate("PHONE")}</strong><span>{quote.serviceProviderDto.contactInformation.phone}</span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    );
                }

            });
        } else {
            return (
                <main className={"col-12 show"}><Loader /></main>
            );
        }
    }


    renderList() {
        if (!!this.props.myQuotesData && !!this.props.myQuotesData.data && this.props.myQuotesData.data.length) {
            return this.props.myQuotesData.data.map((lead, index) => {
                let address =
                    (lead.address.line1 && lead.address.line1.trim() != '' && lead.address.line1 != 'N/A' ? (lead.address.line1 + ", ") : '') +
                    (lead.address.apartment && lead.address.apartment.trim() != '' && lead.address.apartment != 'N/A' ? (lead.address.apartment + ", ") : '') +
                    (lead.address.building && lead.address.building.trim() != '' && lead.address.building != 'N/A' ? (lead.address.building + ", ") : '') +
                    (lead.address.area && lead.address.area.trim() != '' && lead.address.area != 'N/A' ? (lead.address.area + ", ") : '') +
                    ((lead.address.city && lead.address.city.trim() != '' && lead.address.city != 'N/A') ? (lead.address.city + ", ") : '') +
                    ((lead.address.country && lead.address.country.trim() != '' && lead.address.country != 'N/A') ? (lead.address.country) : '');

                address += '.';
                // console.log('address is:' + address);
                return (
                    <div className="my-services row mbl my-quotes">
                        <div className="col-md-3 col-sm-3 col-xs-12 image my-services-left-col">
                            <LazyLoad height={300} offset={500} placeholder={<PlaceholderComponent />}>
                                <img src={"/dist/images/services_pics/" + lead.services[0].id + ".jpg"} height="219" />
                            </LazyLoad>
                        </div>
                        <div className="col-md-9 col-sm-9 col-xs-12">
                            <div className="row pl-4 pr-4">
                                <div className="col-md-12 col-sm-12 col-xs-12 mvm">
                                    <div className="row">
                                        <div className="col-md-12 col-sm-12 col-xs-12">
                                            <h5 className="card-item-title">{lead.services[0].name}</h5>
                                            <h2 className="card-item-title">ID#{lead.referenceId}</h2>
                                            <div className="row">
                                                <div className="col-md-6 col-sm-6 col-xs-6 pr-6">
                                                    <div className="card-detail-items">
                                                        <div className="card-detail-item pbm pln"><strong>{locationHelper.translate("REQUEST_ON")}: </strong>
                                                            <span>{lead.requestDate ? moment.unix(lead.requestDate / 1000).format('MMMM DD, YYYY') : ''}</span>
                                                        </div>
                                                        <div className="card-detail-item pbm pln"><strong>{locationHelper.translate("YOU_NEED_THIS_SERVICE_ON")}: </strong>
                                                            <span>{lead.requiredDateOfService ? moment(lead.requiredDateOfService, 'DD-MM-YYYY').format('MMMM DD, YYYY') : ''}</span></div>
                                                    </div>
                                                </div>
                                                <div className="col-md-6 col-sm-6 col-xs-6">
                                                    <div className="card-detail-items">
                                                        <div className="card-detail-item pbm pln">
                                                            <strong>{locationHelper.translate("STATUS")}: </strong>
                                                            <span>{lead.requestStatus}</span></div>
                                                        <div className="card-detail-item pbm pln">
                                                            <strong>{locationHelper.translate("ADDRESS")}: </strong>
                                                            <span className='text-align-left'>{address}</span></div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="after-border hidden"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr style={{ width: "100%" }} />
                        <div className={"col-12 more-details"} onClick={() => this.fetchQuotesOfLead(lead.referenceId, lead.noOfQuotes)}>
                            <span style={{ "fontSize": "1.2rem" }} className={"title"}><strong>{lead.noOfQuotes == 0 ? locationHelper.translate('NO_QUOTES_RECEIVED_YET') : lead.noOfQuotes +' '+locationHelper.translate('QUOTES_RECEIVED')}</strong></span>
                        </div>
                        <div className={"col-12 pt-3 companies-quotes collapse " + (this.state.openedLead[lead.referenceId] ? 'show' : 'hide')}>
                            {this.renderLeadsQuotes(this.state.leadsQuotes[lead.referenceId])}
                        </div>

                    </div>
                );
            });
        } else {
            return (
                <div className={"text-center pt-5 ml-5 mr-5"}>
                    <img src={"/dist/images/empty-quotes.png"} />
                    <p className={"pt-3"} style={{ color: "#ccc" }}>{locationHelper.translate("LOOKS_LIKE_YOU_HAVEN_REQUESTED_ANY_CUSTOM_QUOTES_YET")} <br /> {locationHelper.translate("ONCE_YOU_REQUEST_QUOTES_ALL_THE_DETAILS_WILL_APPEAR_HERE")}</p>
                </div>
            )
        }
    }

    render() {
        // console.log(this.props.myQuotesData);
        if (!this.state.loader) {
            return (
                <div className="col-lg-9 col-md-9 col-sm-12 col-xs-12 user-right-con">
                    {this.renderList()}
                </div>
            );
        } else {
            return (
                <main
                    className={"col-lg-9 col-md-9 col-sm-12 col-xs-12 user-right-con " + (!this.state.loader ? "show" : "hide")}>
                    <Loader /></main>
            );
        }
    }
}

function mapStateToProps(state) {
    return {
        myQuotesData: state.myQuotesData,
        signInDetails: state.signInDetails,
        currentCity: state.currentCity,
        lang: state.lang
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ fetchCustomerQuotesHistory }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(MyQuotes);

