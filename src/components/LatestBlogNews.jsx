import React from "react";
import Slider from "react-slick";
import {imgixWPBase, relativeURL, quality} from '../imgix';
import LazyLoad from 'react-lazyload';
import PlaceholderComponent from '../components/Placeholder';

class LatestBlogNews extends React.Component{
    constructor(props) {
        super(props);
    }
	renderItems(){
		return this.props.items.map((item) => {
            return(
                <a href={item.permalink} className='blog-cta-btn' key={"link"+item.ID}>
					<div key={item.ID} className="post card item">
						<LazyLoad offset={500} placeholder={<PlaceholderComponent />}>
							<div className="placeholder rect bg-primary" style={{background: "url("+imgixWPBase+relativeURL(item.image)+"?fit=crop&w=322&h=215&auto=format,compress&q="+quality+") center/cover no-repeat"}}></div>
						</LazyLoad>
						<div className="card-body">
							<h5 className="card-title">{item.post_title}</h5>
							<p className="card-text">{(item.post_content).replace(/<[^>]+>/g, '').substring(0, 175) + '...'}</p>
						</div>
						<div className="card-footer bg-transparent border-top-0">
							<a href={item.permalink} className="text-dark font-weight-bold blog-cta-btn">Read more&nbsp;
								<i className="fa fa-arrow-right"></i>
							</a>
						</div>
					</div>
				</a>
            );
        });
	}
    render() {
    	var settings = { dots: false, arrows: true, infinite: true, speed: 500, slidesToShow: 3, slidesToScroll: 1, infinite: false, responsive: [ { breakpoint: 1199, settings: { slidesToShow: 2 } }, { breakpoint: 767, settings: { unslick: true } } ] };
        return (
        	<div className={'blog-slider '+this.props.parentClass}>
			    <div className={this.props.childClass}>
			        <div className="sm-slider">
						<Slider {...settings}>
							{this.renderItems()}
						</Slider>
			        </div>
			    </div>
			</div>
        );
    }
}
LatestBlogNews.defaultProps = {
  parentClass: 'row mb-5',
  childClass: 'col px-4 px-md-3 blog',
  items: []
};


export default LatestBlogNews;
