import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import SocialButton from "../components/SocialButton";
import {
    isValidSection,
    getLogo
} from "../actions/index";
import {Link} from "react-router-dom";
import Loader from "../components/Loader";
import {withCookies} from 'react-cookie';
import {fetchUserProfile, toggleForgetPasswordModal, updateCustomerPassword} from "../actions";
import TextFloatInput from "./Form_Fields/TextFloatInput";
import locationHelper from "../helpers/locationHelper";

class PopupForgetPassword extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            loader: false,
            passwordDoesntMatch: false,
            completed: false,
            error: false
        }
        this.locationHelperInit = locationHelper.init.bind(this);
        this.locationHelperInit();
        this.onInputChange = this.onInputChange.bind(this);
        this.hideMenu = this.hideMenu.bind(this);
        this.resetPassword = this.resetPassword.bind(this);

    }

    componentDidMount() {
        var resetPasswordCookies = this.props.cookies.get('_user_details_reset_password') || {};
        if (this.props.resetUserDetails && this.props.resetUserDetails.data) {
            this.setState({
                emailAddress: this.props.resetUserDetails.data.emailAddress,
                id: this.props.resetUserDetails.data.id
            });
        }
    }

    componentDidUpdate() {

    }

    resetPassword() {
        if (isValidSection('password-reset-form')) {
            if(this.state.password == this.state.confirm_password) {
                this.setState({
                    loader: true,
                    passwordDoesntMatch: false
                });
                this.props.updateCustomerPassword(this.state.emailAddress, this.state.password, this.state.id, this.props.showForgetPasswordMenu.resetToken, '').then(res=>{
                    if(res.success) {
                        this.setState({
                            completed: true
                        });
                        this.props.cookies.remove("_user_details_reset_password", { path: '/' });
                        window.location = '/';
                    } else {
                        this.setState({
                            loader: false,
                            error: true
                        })
                    }
                }).catch(error=>{
                    this.setState({
                        loader: false,
                        error: true
                    })
                });
            } else {
                this.setState({
                    passwordDoesntMatch: true,
                })
            }

        }

    }
    onInputChange(name, value){
        this.setState({
            [name]: value
        });
    }
    hideMenu() {
        this.props.toggleForgetPasswordModal({visibility: false})
    }

    render() {
        var {loader} = this.state;
        return !loader ?
            (
                <div id="reset-password-dialog" className={"modal-ss modal fade modal-customer open collapse" + (this.props.showForgetPasswordMenu.visibility ? ' show': '')}>
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div style={{'padding': '1rem'}}>
                                <button onClick={()=> this.hideMenu()} type="button" className="close" data-dismiss="modal"
                                        aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <a className="logo" href="javascript:void(0)" onClick={()=> this.hideMenu()} title="Home">
                                    <h5>{locationHelper.translate('WELCOME_TO')}</h5>
                                    <object data={getLogo(this.props.lang)} height={'75px'} alt={locationHelper.translate('SERVICE_MARKET')} type="image/svg+xml"></object>
                                </a>
                            </div>
                            <div className="modal-body">
                                <div className="search-loader-container hidden">
                                    <div className="sk-cube-grid">
                                        <div className="sk-cube sk-cube1"></div>
                                        <div className="sk-cube sk-cube2"></div>
                                        <div className="sk-cube sk-cube3"></div>
                                        <div className="sk-cube sk-cube4"></div>
                                        <div className="sk-cube sk-cube5"></div>
                                        <div className="sk-cube sk-cube6"></div>
                                        <div className="sk-cube sk-cube7"></div>
                                        <div className="sk-cube sk-cube8"></div>
                                        <div className="sk-cube sk-cube9"></div>
                                    </div>
                                </div>
                                <div className="reset-content">
                                    <div className="customer-login">
                                        {(this.state.emailAddress && this.state.emailAddress != '' && this.state.emailAddress != 'null')?(
                                            <div>
                                                <h5 className="text-center">{locationHelper.translate('RESET_YOUR_PASSWORD')}</h5>
                                                <form onSubmit={ (e) => {this.resetPassword(); e.preventDefault();}} id="password-reset-form" noValidate="novalidate">
                                                    <div className="form-group phn mvm">
                                                        <strong>{locationHelper.translate('User ID')}: </strong>{this.state.emailAddress}
                                                        <input type="hidden" name="email" id="email" value={this.state.emailAddress} />
                                                    </div>
                                                    <div className="form-group phn">
                                                        <TextFloatInput InputType="password" id="password" name="password" label={locationHelper.translate("NEW_PASSWORD")} inputValue={this.state.password} onInputChange={this.onInputChange} validationClasses="required" />
                                                    </div>
                                                    <div className="form-group phn">
                                                        <TextFloatInput InputType="password" id="confirm_password" name="confirm_password" label={locationHelper.translate("CONFIRM_NEW_PASSWORD")} inputValue={this.state.confirm_password} onInputChange={this.onInputChange} validationClasses="required" />
                                                    </div>
                                                    {this.state.passwordDoesntMatch?<p className="text-danger">{locationHelper.translate('PASSWORD_AND_CONFIRM_PASSWORD_DO_NOT_MATCH')}</p>:''}
                                                    {this.state.error?<p className="text-danger">{locationHelper.translate('SOMETHING_WENT_WRONG_PLEASE_TRY_AGAIN_LATER')}</p>:''}
                                                    <button type="submit" onClick={this.resetPassword} className="btn btn-default" id="reset-password-button">{locationHelper.translate('RESET_PASSWORD')}</button>
                                                </form>
                                            </div>
                                        ):(
                                            <p className="mar-no">{locationHelper.translate('NO_PASSWORD_RESET_REQUEST_FOUND')}</p>
                                        )}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )
            : (
                <div id="reset-password-dialog" className={"modal-ss modal fade modal-customer open collapse" + (this.props.showForgetPasswordMenu.visibility ? ' show': '')}>
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <Loader completed={this.state.completed} />
                        </div>
                    </div>
                </div>
            );
    }
}

function mapStateToProps(state) {
    return {
        showForgetPasswordMenu: state.showForgetPasswordMenu,
        resetUserDetails: state.resetUserDetails,
        lang:state.lang,
        currentCity: state.currentCity
    }
}


function mapDispatchToProps(dispatch) {
    return bindActionCreators({toggleForgetPasswordModal,updateCustomerPassword
    }, dispatch);
}

export default withCookies(connect(mapStateToProps, mapDispatchToProps)(PopupForgetPassword));
