import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import GeneralModal from "./GeneralModal";
import Loader from "./Loader";
import FormFieldsTitle from "./FormFieldsTitle";
import TextFloatInput from "./Form_Fields/TextFloatInput";
import SelectInput from "./Form_Fields/SelectInput";
import CheckRadioBoxInput from "./Form_Fields/CheckRadioBoxInput";
import TextareaInput from "./Form_Fields/TextareaInput";
import PhoneInput from "./Form_Fields/PhoneInput";
import SubmitElement from "./Form_Fields/SubmitElement";
import { isValidSection, submitBecomeAPartner, fetchAllParentServices, submitBecomeAPartnerZoho } from "../actions/index";
import commonHelper from "../helpers/commonHelper";
import locationHelper from '../helpers/locationHelper';
import stringConstants from '../constants/stringConstants';

class BecomePartnerForm extends React.Component {
    _isMounted = false;
    constructor(props) {
        super(props);
        this.state = {
            company: '',
            empcount: { value: '', label: '' },
            fullname: '',
            email: '',
            phone: '',
            website: '',
            cities: [],
            services: [],
            feedback: '',
            serviceOptions: [],
            showModal: false,
            sucess: false
        }

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.setModal = this.setModal.bind(this);
    }
    componentDidMount() {
        this._isMounted = true;
        let { lang } = this.props
        if (!this.state.serviceOptions.length) {
            var services = [];
            fetchAllParentServices(lang).then(res => {
                if (this._isMounted) {
                    res.parentServices.forEach((item, index) => {
                        services.push({ id: item.id, value: item.id, label: item.name });
                    });

                    this.setState({
                        serviceOptions: services
                    })
                }
            });
        }
    }
    componentWillUnmount() {
        this._isMounted = false;
    }
    setModal(visibility) {
        this.setState({
            showModal: visibility,
            sucess: false
        });
    }
    handleInputChange(name, value) {
        this.setState({
            [name]: value
        });
    }
    handleSubmit(event) {
        event.preventDefault();
        let {lang} = this.props;
        if (isValidSection('beparner-form', lang)) {
            // Submit action
            var fullname = this.state.fullname;
            var personName = commonHelper.splitString(fullname, " ", 1);

            var firstName = "";
            var lastName = "";

            if (personName.length == 1) {
                firstName = personName[0];
                lastName = firstName;
            } else {
                firstName = personName[0];
                lastName = personName[1];
            }

            var data = {
                "companyName": this.state.company,
                "companyWebsite": this.state.website,
                "contactInformationModel": {
                    "personEmail": this.state.email,
                    "personLastName": firstName == lastName ? "" : lastName,
                    "personName": firstName,
                    "personPhone": this.state.phone,
                    "primary": true
                },
                "numberOfEmployees": this.state.empcount.value,
                "otherDetails": this.state.feedback,
                "serviceLocations": this.state.cities.map(item => item.label),
                "services": this.state.services.map(item => item.label)
            };



            var zohoData = [{
                "Company": this.state.company,
                "Website": this.state.website,
                "First_Name": firstName,
                "Last_Name": lastName,
                "Email": this.state.email,
                "Phone": this.state.phone,
                "Mobile": this.state.phone,
                "Website": this.state.website,
                "Lead_Source": "Become A Partner Form",
                "Lead_Status": "Not Contacted",
                "Services_offered": this.state.services.map(item => item.label),
                "Service_cities": this.state.cities.map(item => item.label),
                "Number_of_employees": String(this.state.empcount.value),
                "Description": this.state.feedback
            }];
            this.setModal(true);
            submitBecomeAPartner((this.props.signInDetails && this.props.signInDetails.access_token) ? this.props.signInDetails.access_token : '', (this.props.signInDetails && this.props.signInDetails.customer) ? this.props.signInDetails.customer.id : '', data).then(res => {
                if (res.success) {
                    submitBecomeAPartnerZoho(zohoData).then(res => {
                        this.setState({
                            company: '',
                            empcount: { value: '', label: '' },
                            fullname: '',
                            email: '',
                            phone: '',
                            website: '',
                            cities: [],
                            services: [],
                            feedback: '',
                            serviceOptions: [],
                            sucess: true,
                        });
                    }).catch(error => {

                    })

                }
            })
        }
    }

    render() {
        var numberOfEmp = [{ 'id': 1, value: 10, label: "1-10" }, { 'id': 2, value: 50, label: "11-50" }, { 'id': 3, value: 100, label: "51-100" }, { 'id': 4, value: 250, label: "101-250" }, { 'id': 5, value: 500, label: "251-500" }, { 'id': 6, value: 501, label: "500 +" }];
        var cityOptions = [];
        this.props.cities.forEach((item, index) => {
            cityOptions.push({ id: item.id, value: item.id, label: item.name, code: item.code });
        });
        var { company, empcount, fullname, email, phone, website, cities, services, feedback, serviceOptions } = this.state;
        return (
            <form id="beparner-form" onSubmit={this.handleSubmit}>
                <FormFieldsTitle title={locationHelper.translate('BECOME_PARTNER_TITLE')} />
                <div className="row">
                    <div className="col-6 pr-2 pr-sm-3 mb-3">
                        <TextFloatInput
                            InputType="text" name="company"
                            inputValue={company}
                            label={locationHelper.translate('COMPANY_NAME')}
                            onInputChange={this.handleInputChange}
                            validationClasses="required"
                            validationMessage={locationHelper.translate('COMPANY_NAME_REQUIRED')} />
                    </div>
                    <div className="col-6 pl-2 pl-sm-3 mb-3">
                        <SelectInput name="empcount" inputValue={empcount} label={locationHelper.translate('NO_OF_EPLOYEES')} options={numberOfEmp} onInputChange={this.handleInputChange} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 pr-2 pr-sm-3 mb-3">
                        <TextFloatInput
                            InputType="text"
                            name="fullname"
                            inputValue={fullname}
                            label={locationHelper.translate('CONTACT_PERSON')}
                            onInputChange={this.handleInputChange}
                            validationClasses="required"
                            validationMessage={locationHelper.translate('CONTACT_PERSON_REQUIRED')} />
                    </div>
                    <div className="col-6 pl-2 pl-sm-3 mb-3">
                        <TextFloatInput
                            InputType="email"
                            name="email"
                            inputValue={email}
                            label={locationHelper.translate('CONTACT_EMAIL')}
                            onInputChange={this.handleInputChange}
                            validationClasses="required email"
                            validationMessage={locationHelper.translate('CONTACT_EMAIL_REQUIRED')} />
                    </div>
                </div>
                <div className="row mb-4">
                    <div className="col-6 pr-2 pr-sm-3 mb-3">
                        <PhoneInput
                            name="phone"
                            inputValue={phone}
                            onInputChange={this.handleInputChange}
                            validationClasses="required phone" />
                    </div>
                    <div className="col-6 pl-2 pl-sm-3 mb-3">
                        <TextFloatInput InputType="text" name="website" inputValue={website}
                            label={locationHelper.translate('COMPANY_WEBSITE')} onInputChange={this.handleInputChange}
                            validationClasses="required" />
                    </div>
                </div>
                <FormFieldsTitle title={locationHelper.translate('BECOME_PARTNER_CITY')} />
                <CheckRadioBoxInput
                    InputType="checkbox"
                    name="cities"
                    inputValue={cities}
                    items={cityOptions}
                    onInputChange={this.handleInputChange}
                    validationClasses="min-check-1"
                    validationMessage={locationHelper.translate('CITY_REQUIRED')} />

                <FormFieldsTitle title={locationHelper.translate('BECOME_PARTNER_SERVICES')} />
                <CheckRadioBoxInput
                    InputType="checkbox"
                    name="services"
                    inputValue={services}
                    items={serviceOptions}
                    onInputChange={this.handleInputChange}
                    validationClasses="min-check-1"
                    validationMessage={locationHelper.translate('SERVICES_REQUIRED')} />

                <FormFieldsTitle title={locationHelper.translate('BECOME_PARTNER_OTHER_INFO')} />
                <TextareaInput name="feedback" inputValue={feedback} placeholder={locationHelper.translate('MESSAGE')}
                    onInputChange={this.handleInputChange} />

                <SubmitElement />
                {this.state.showModal && <GeneralModal title={locationHelper.translate("SENDING_DETAILS")} modalBody={<div><Loader completed={this.state.sucess} /> <div>{locationHelper.translate('PARTNER_WELCOME_TXT')}</div></div>} setModal={this.setModal} />}
            </form>
        );
    }
}


function mapStateToProps(state) {
    return {
        cities: state.cities,
        signInDetails: state.signInDetails,
        lang: state.lang
    }
}

export default connect(mapStateToProps)(BecomePartnerForm);
