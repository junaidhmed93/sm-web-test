import React from "react";

class Loader extends React.Component{
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className="loader-cont">
                <div className={"circle-loader my-5 mx-auto" + (this.props.completed ? ' load-complete' : '') }>
                    <div className="checkmark draw"></div>
                </div>
            </div>
        );
    }
}

Loader.defaultProps = {
    completed: false
};

export default Loader;
