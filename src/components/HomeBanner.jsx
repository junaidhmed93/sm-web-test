import React from "react";
import Search from "./Search";
import { imgixReactBase, quality } from '../imgix';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import commonHelper from "../helpers/commonHelper";
import locationHelper from "../helpers/locationHelper";
import stringConstants from '../constants/stringConstants';

class HomeBanner extends React.Component{
    constructor(props) {
        super(props);
        this.locationHelperInit = locationHelper.init.bind(this);
        this.locationHelperInit();
        let headingText = locationHelper.translate("HOME_PAGE_H1");
        let headingSubText = locationHelper.translate("HOME_PAGE_H2");
        this.state = {
            headingText: headingText,
            headingSubText: headingSubText
        }
    }
    componentWillReceiveProps(newProps) {
        if (newProps.currentCity !== this.props.currentCity) {
            locationHelper.Props = newProps;
            let headingText = locationHelper.translate("HOME_PAGE_H1");
            let headingSubText = locationHelper.translate("HOME_PAGE_H2");
            this.setState({
                headingText: headingText,
                headingSubText: headingSubText
            })
        }
    }
    render() {
        let {headingText, headingSubText} = this.state;
        return (
            <section className="hero py-5" style={{ background: "url(" + imgixReactBase + "/dist/images/homepage/" + commonHelper.getHomePageImage(this.props.currentCity) + "?fit=crop&w=1349&h=442&auto=format,compress&exp=-3.9&q=" + quality + ") center/cover no-repeat", minHeight: "442px" }}>
                <div className="py-5">
                    <div className="container py-5 px-4">
                        <div className="row">
                            <div className="col">
                                <h1 className="text-center text-white font-weight-bold">{headingText}</h1>
                                <h2 className="text-white text-center">{headingSubText}</h2>
                            </div>
                        </div>
                        <div className="row mt-4">
                            <div className="col-12 col-md-8 mx-auto d-flex">
                                <Search type="home" />
                            </div>
                        </div>
                        {/* <div className='row mt-4' style={{ marginTop: '0.5rem!important' }}>
                            <div className="input-group justify-content-center">
                                <h5 class="text-white text-center">{stringConstants.SEARCH_HELP_TXT}</h5>
                            </div>
                        </div> */}
                    </div>
                </div>
            </section>
        );
    }
}

function mapStateToProps(state) {
    return {
        currentCity: state.currentCity,
        lang: state.lang
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(HomeBanner);

