import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import Loader from "../components/Loader";
import Loadable from 'react-loadable';
const loading = () => (<Loader />);
const SocialButton = Loadable({
    loader: () => import('../components/SocialButton'),
    loading
});
import {
    toggleLoginModal,
    toggleSignInForm,
    setSignIn,
    isValidSection,
    fetchResetPassword
} from "../actions/index";
import { Link } from "react-router-dom";
import {fetchSignUp, fetchSignIn, showLoader, hideLoader, fetchSocialSignInCustomer} from "../actions/index";

import { withCookies } from 'react-cookie';
import {fetchUserProfile} from "../actions";
import TextFloatInput from "./Form_Fields/TextFloatInput";
import {imgixReactBase} from "../imgix";
import staticVariables from "../staticVariables";
import locationHelper from "../helpers/locationHelper";

class PopupLogin extends React.Component {
    
    constructor(props) {
        super(props);
        this.locationHelperInit = locationHelper.init.bind(this);
        this.locationHelperInit();
        this.state = {
            rerender: false,
            formType: 0,
            loading: false,
            errorLogIn: false,
            errorSignUp: false,
            pathname: '',
            popupTitle: locationHelper.translate("WELCOME_BACK_PLEASE_SIGN_IN"),
            popupSubTitle: locationHelper.translate("SIGN_IN_TO_ACCESS_YOUR_BOOKINGS_AND_QUOTES"),
            email: '',
            password:'',
            signup_email: '',
            signup_name: '',
            signup_last_name: '',
            signup_password: '',
            emailSent: false,
        }

        this.hideMenu = this.hideMenu.bind(this);
        this.hideSignInForm = this.hideSignInForm.bind(this);
        this.setUserLogedIn = this.setUserLogedIn.bind(this);
        this.getUserProfile = this.getUserProfile.bind(this);
        this.customLoginForm = this.customLoginForm.bind(this);
        this.resetFormType = this.resetFormType.bind(this);
        this.signUpForm = this.signUpForm.bind(this);
        this.forgetPasswordForm = this.forgetPasswordForm.bind(this);
        this.continueAsGuest = this.continueAsGuest.bind(this);
        this.forgetPassword = this.forgetPassword.bind(this);
        this.onInputChange = this.onInputChange.bind(this);

    }

    componentDidMount() {
        var pathArray = location.pathname.split( '/' );
        if( pathArray.includes('book-online') ||  pathArray.includes('journey1') ){
			this.setState({
                pathname: location.pathname,
				popupTitle: locationHelper.translate("SIGN_IN_FOR_SIMPLE_CHECKOUT"),
                popupSubTitle: locationHelper.translate("THE_EASIEST_WAY_TO_MANAGE_YOUR_REQUESTS")
			})
		}
    }
    componentDidUpdate(){
        if( location.pathname !=  this.state.pathname ){
            var pathArray = location.pathname.split( '/' );
            if( pathArray.includes('book-online') ||  pathArray.includes('journey1') ){
                this.setState({
                    pathname: location.pathname,
                    popupTitle: locationHelper.translate("SIGN_IN_FOR_SIMPLE_CHECKOUT"),
                    popupSubTitle: locationHelper.translate("THE_EASIEST_WAY_TO_MANAGE_YOUR_REQUESTS")
                })
            }else{
                this.setState({
                    pathname: location.pathname,
                    popupTitle: locationHelper.translate("WELCOME_BACK_PLEASE_SIGN_IN"),
                    popupSubTitle: locationHelper.translate("SIGN_IN_TO_ACCESS_YOUR_BOOKINGS_AND_QUOTES")
                })
            }
        }
        
    }
    continueAsGuest(){
        this.props.toggleLoginModal({visibility:false, guest:true});
    }
    hideMenu() {
        this.props.toggleLoginModal({visibility:false, guest:false});
    }

    hideSignInForm(visibile) {
        this.props.toggleSignInForm(visibile);
    }

    setUserLogedIn(details) {
        if ( (typeof details.data != "undefined" && typeof details.data.access_token != "undefined" ) && 
             ( details.data.access_token && details.data.access_token.length > 0)) {
        //if (details.data.access_token && details.data.access_token.length > 0) {
            this.getUserProfile(details.data.access_token);
            this.props.setSignIn(details.data);
            const { cookies } = this.props;
            cookies.set('_user_details', JSON.stringify(details.data), {path: '/', maxAge:details.data.expires_in});
        }

    }

    resetFormType() {
        this.setState({
            formType: 0,
        });
    }
    customLoginForm() {
        if( typeof this.props.showLoginMenu != "undefined" && (typeof this.props.showLoginMenu.email != "undefined" && typeof this.props.showLoginMenu.email != "")){
            this.setState({
                email: this.props.showLoginMenu.email,
                formType: 1,
                emailSent: false
            })
        }else {
            this.setState({
                formType: 1,
                emailSent: false
            });
        }
    }
    signUpForm() {
        this.setState({
            formType: 2,
        });
    }
    forgetPasswordForm() {
        this.setState({
            formType: 3,
        });
    }
    signIn() {
        // this.props.showLoader();
        if (isValidSection('login-form')) {
            this.setState({
                loading: true
            });
            fetchSignIn(this.state.email, this.state.password).then((res)=> {
                // console.log(res);
                if(res.data.error && res.data.error.length) {
                    this.setState({
                        loading: false,
                        errorLogIn: true,
                    });
                } else {
                    this.setUserLogedIn(res);
                }

            }).catch(()=>{
                this.setState({
                    loading: false,
                    errorLogIn: true,
                });
            });
        }

    }
    forgetPassword() {
        // this.props.showLoader();
        if (isValidSection('reset-form')) {
            this.setState({
                loading: true
            });
            fetchResetPassword(this.state.email, (this.props.signInDetails && this.props.signInDetails.access_token)?this.props.signInDetails.access_token:'').then((res)=> {
                // console.log(res);
                // this.setUserLogedIn(res);
                if (res.success) {
                    const { cookies } = this.props;
                    cookies.set('_user_details_reset_password', JSON.stringify(res.data), {path: '/'});
                }
                this.setState({
                    loading: false,
                    emailSent: true
                });
            }).catch(()=>{
                this.setState({
                    loading: false,
                });
            });
        }

    }

    getUserProfile(token) {
        this.props.fetchUserProfile(token).then(res => {
            this.setState({
                loading: false
            });
        });
        this.hideMenu();
    }

    signUp() {
        if (isValidSection('customer-signup-form')) {
            this.signUpThenSignIn(this.state.signup_email,this.state.signup_name,this.state.signup_last_name,this.state.signup_password);
        }
    }

    handleFacebookSocialLogin = (user) => {
        // console.log(this);
        // console.log(user);
        // console.log(user._profile);

        this.socialSignIn(user._token.accessToken, "facebook");
    }

    handleGoogleSocialLogin = (user) => {
        // console.log(this);
        // console.log(user);
        // console.log(user._profile);
        this.socialSignIn(user._token.accessToken, "google");
    }

    signUpThenSignIn(email, firstname, lastname, password) {
        this.setState({
            loading: true,
        });
        let {lang} = this.props;
        fetchSignUp(email, firstname, lastname, password, lang)
            .then((result)=> {
                // console.log(result.data.registerCustomer);
                var parsedRes = result.data.registerCustomer?JSON.parse(result.data.registerCustomer):'';
                // console.log(parsedRes);
                var status = parsedRes?parsedRes.status:'BAD_REQUEST';
                // console.log(JSON.parse(result.data));
                // console.log(status);
                if (status != 'BAD_REQUEST') {
                    fetchSignIn(email, password).then((res)=> {
                        // console.log('res');
                        // console.log(res);
                        this.setUserLogedIn(res);
                    }).catch(() => {
                        this.setState({
                            loading: false,
                            errorSignUp: true
                        });
                    });
                } else {
                    this.setState({
                        loading: false,
                        errorSignUp: true
                    });
                }

            });
    }

    socialSignIn(accessToken, type) {
        this.setState({
            loading: true,
        });
        fetchSocialSignInCustomer(accessToken, type).then((res)=> {
            // console.log(res);
            this.setUserLogedIn(res);
        }).catch(() => {
            this.setState({
                loading: false
            });
        });
    }

    handleSocialLoginFailure = (err) => {
        console.error(err);
        this.setState(
            { rerender: true },
            () => { this.setState({ rerender: false });}
            );
    }

    guest(){
        if (this.props.showLoginMenu.guest) {
            return (
                <div className="continue-as-guest">
                    <div className="">
                        <span>or</span>
                        <a className="continue-as-guest-link" href="javascript:void(0)" onClick={()=> this.hideMenu()} >{locationHelper.translate("CONTINUE_AS_GUEST")}</a>
                    </div>
                </div>
            );
        }
    }

    // Get Sign in from local storage
    getSignIn() {
        // console.log("getSignIn");

        const { cookies } = this.props;
        var signInDetails = cookies.get('_user_details') || {};
        return signInDetails;
    }
    onInputChange(name, value){
        this.setState({
            [name]: value
        });
    }
    loadCards(){
        var formType = this.state.formType;
        //console.log(this.props);
        var email = "";


        if (!this.state.loading) {
            return (
                <main>
                    <div className={"card default " + ((formType !== 0) ? 'hidden' : '')}>
                        <button onClick={()=> this.hideMenu()} type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <h4 className="title">{this.state.popupTitle}</h4>
                        <p className="click-takes">{this.state.popupSubTitle}</p>
                        <div className="tab-content">
                            <div id="exiting_socialmodal" className="tab-pane fade in active show">
                                <div className="socialmodal_options">
                                    <SocialButton
                                        className="socialmodal_options_g"
                                        provider='google'
                                        id="google-login-button"
                                        appId={staticVariables.GOOGLE_APP_ID}
                                        onLoginSuccess={this.handleGoogleSocialLogin}
                                        onLoginFailure={this.handleSocialLoginFailure}
                                    >
                                        <span className="row">
                                            <span className="col-3 p-0 pr-2 icon">
                                                <img src="/dist/images/sociallogin_g.svg" width="25px" /> 
                                            </span>
                                            <span className="col-9 p-0 text">
                                                {locationHelper.translate("CONTINUE_WITH_GOOGLE")}
                                            </span>
                                        </span>
                                    </SocialButton>
                                    <SocialButton
                                        className="socialmodal_options_f" id="facebook-login-button"
                                        provider='facebook'
                                        appId={staticVariables.FACEBOOK_APP_ID}
                                        onLoginSuccess={this.handleFacebookSocialLogin}
                                        onLoginFailure={this.handleSocialLoginFailure}
                                    >
                                        <span className="row">
                                            <span className="col-3 p-0 pr-2 icon">
                                                <img src="/dist/images/sociallogin_f.svg" width="25px" />
                                            </span>
                                            <span className="col-9 p-0 text">
                                                {locationHelper.translate("CONTINUE_WITH_FACEBOOK")}
                                            </span>
                                        </span>
                                    </SocialButton>
                                    <a onClick={this.customLoginForm} className="socialmodal_options_e login-with-email" href="javascript:void(0)">
                                        <span className="social-btn-center">
                                            <span className="row">
                                                <span className="col-3 p-0 pr-2 icon">
                                                    <img src="/dist/images/sociallogin_e.svg" width="25px" /> 
                                                </span>
                                                <span className="col-9 p-0 text">
                                                    {locationHelper.translate("CONTINUE_WITH_EMAIL")}
                                                </span>
                                            </span>
                                      </span>    
                                    </a>
                                    {this.guest()}
                                </div>
                                
                                <div className="invalid-signup hidden"
                                     style={{padding: 0 + '!important'}}>{locationHelper.translate("SORRY_WE_DO_NOT_RECOGNIZE_YOUR_EMAIL_OR_PASSWORD_PLEASE_TRY_AGAIN")}
                                </div>
                            </div>
                        </div>
                        <div className="footer">
                            <p>{locationHelper.translate("BY_LOGGING_IN_TO_YOUR_ACCOUNT_YOU_AGREE_TO_OUR")} <Link
                                to="/en/terms-and-conditions" onClick={()=> this.hideMenu()}>{locationHelper.translate("TERMS_AND_CONDITIONS")}</Link> {locationHelper.translate("AND")} <Link onClick={()=> this.hideMenu()}
                                to="/en/privacy-policy">{locationHelper.translate("PRIVACY_POLICY")}</Link>
                            </p>
                        </div>
                    </div>
                    <div className={"card login-with-email-card " + ((formType !== 1) ? 'hidden' : '')}>
                        <div className="tab-content">
                            <button onClick={()=> this.hideMenu()} type="button" className="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <a href="javascript:void(0)" onClick={this.resetFormType} className="sm_arrow_back"><img src={imgixReactBase + "/dist/images/sm_arrow_back.png?w=17&h=15&auto=format,compress"}/></a>
                            <h4 className="title">{locationHelper.translate("LOG_IN_WITH_EMAIL")}</h4>
                            <form id="login-form" autoComplete="false"
                                 className="mth" noValidate="novalidate">
                                <input type="hidden" id="redirect_url" name="redirect_url"
                                       value="" /><input type="hidden" id="go_to_url" name="go_to_url"
                                                         value="" />
                                <div className={"mb-4"}>
                                    <TextFloatInput InputType="email" id="username" name="email" label={locationHelper.translate("EMAIL_ADDRESS")} inputValue={this.state.email} onInputChange={this.onInputChange} validationClasses="required email" />
                                </div>
                                <div className={"mb-4"}>
                                    <TextFloatInput InputType="password" id="password" name="password" label={locationHelper.translate("PASSWORD")} inputValue={this.state.password} onInputChange={this.onInputChange} validationClasses="required" />
                                </div>
                                    <div className={this.state.errorLogIn ? 'error-login' : "hidden"} id="invalid-credentials"
                                         style={{padding: 0 + '!important'}}>{locationHelper.translate("SORRY_WE_DO_NOT_RECOGNIZE_YOUR_EMAIL_OR_PASSWORD_PLEASE_TRY_AGAIN")}
                                    </div>
                                <div className="button-socialmodal">
                                    <button type="submit" onClick={(e) => {e.preventDefault();this.signIn()}} id="login-button"><span>{locationHelper.translate("LOGIN")}</span></button>
                                </div>
                                <div className="my-2 forget-password-row">
                                    <a className="socialmodal_options_e signup-with-email" onClick={this.forgetPasswordForm} href="javascript:void(0)">{locationHelper.translate("FORGOT_PASSWORD")}</a>
                                </div>
                                <div className="my-2 forget-password-row">
                                    <a className="socialmodal_options_e signup-with-email" onClick={this.signUpForm} href="javascript:void(0)">{locationHelper.translate("DONT_HAVE_AN_ACCOUNT_CREATE_ONE")}</a>
                                </div>

                            </form>
                            <div className="important-socialmodal ">
                                {/*<p><b>Insurance customer?</b> Please <a href="/en/uae/insurance/login">login*/}
                                {/*here</a> to see your quotes and policies</p>*/}
                            </div>
                            <div className="footer">
                                <p>{locationHelper.translate("BY_LOGGING_IN_TO_YOUR_ACCOUNT_YOU_AGREE_TO_OUR")} <Link
                                    to="/en/terms-and-conditions" onClick={()=> this.hideMenu()}>{locationHelper.translate("TERMS_AND_CONDITIONS")}</Link> {locationHelper.translate("AND")} <Link onClick={()=> this.hideMenu()}
                                    to="/en/privacy-policy">{locationHelper.translate("PRIVACY_POLICY")}</Link>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className={"card signup-with-email-card " + ((formType !== 2) ? 'hidden' : '') }>
                        <button onClick={()=> this.hideMenu()} type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <a href="javascript:void(0)" onClick={this.resetFormType} className="sm_arrow_back"><img src={imgixReactBase + "/dist/images/sm_arrow_back.png?w=17&h=15&auto=format,compress"}/></a>
                        <h4 className="title aac">{locationHelper.translate("CREATE_AN_ACCOUNT_USING_EMAIL")}</h4>
                            <form onSubmit={ (e) => {this.signUp(); e.preventDefault();}}>
                                <div id="customer-signup-form" className={"row"}>
                                    <div className="mb-4 col-6">
                                        <TextFloatInput InputType="text" id="customer-signup-form-name" name="signup_name" label={locationHelper.translate("FIRST_NAME")} inputValue={this.state.signup_name} onInputChange={this.onInputChange} validationClasses="required" />
                                    </div>
                                    <div className="mb-4 col-6">
                                        <TextFloatInput InputType="text" id="customer-signup-form-last-name" name="signup_last_name" label={locationHelper.translate("LAST_NAME")} inputValue={this.state.signup_last_name} onInputChange={this.onInputChange} validationClasses="required" />
                                    </div>
                                    <div className="mb-4 col-12">
                                        <TextFloatInput InputType="email" id="customer-signup-form-email" name="signup_email" label={locationHelper.translate("EMAIL_ADDRESS")} inputValue={this.state.signup_email} onInputChange={this.onInputChange} validationClasses="required email" />
                                    </div>
                                    <div className="mb-4 col-12">
                                        <TextFloatInput InputType="password" id="customer-signup-form-password" name="signup_password" label={locationHelper.translate("PASSWORD")} inputValue={this.state.signup_password} onInputChange={this.onInputChange} validationClasses="required" />
                                    </div>
                                    <div className={"is-invalid col-12 " + (this.state.errorSignUp ? 'error-signup' : "hidden")} id="signup-error-sp"
                                        style={{padding: 0 + '!important'}}>{locationHelper.translate("LOOKS_LIKE_THERE_ALREADY_AN_ACCOUNT_WITH_THIS_EMAIL")}</div>
                                    <div className="button-socialmodal col-12">
                                        <button type="submit" onClick={() => this.signUp()} id="popup-signup-button"><span>{locationHelper.translate("SIGN_UP")}</span>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        <div className="important-socialmodal ">
                        </div>
                    </div>
                    <div className={"customer-login card row " + ((formType !== 3) ? 'hidden' : '')}>
                        <button onClick={()=> this.hideMenu()} type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <div className="col-auto">
                            <a onClick={this.customLoginForm} href="javascript:void(0)" data-modaltype="login">{locationHelper.translate("BACK_TO_LOGIN")}</a>
                        </div>
                        <form onSubmit={ (e) => {this.forgetPassword(); e.preventDefault();}} id="reset-form" noValidate="novalidate">
                            <div className="form-group col-sm-offset-2 col-sm-8 col-xs-12">
                                <TextFloatInput InputType="email" name="email" label={locationHelper.translate("EMAIL_ADDRESS")} inputValue={this.state.email} onInputChange={this.onInputChange} validationClasses="required email" />
                            </div>
                            {
                                this.state.emailSent ? (
                                    <p className={'text-success'}>
                                        {locationHelper.translate("EMAIL_SENT_SUCCESSFULLY")}
                                    </p>
                                ) : ''
                            }
                            <div className="col-xs-12 col-sm-8 col-sm-offset-2 ">
                                <p className="d-none">{locationHelper.translate("IMPORTANT_IF_YOU_WOULD_LIKE_TO_ACCESS_YOUR_INSURANCE_QUOTES")} <a href="/en/uae/insurance/login" style={{"marginTop": "0px"}}>{locationHelper.translate("HERE")}</a>.
                                </p>
                                <p className="mt-1 small">
                                    {locationHelper.translate("WHEN_YOU_CLICK_THE_BUTTON_BELOW")}
                                </p>
                            </div>
                            <div className="col-xs-12 col-sm-8 col-sm-offset-2">
                                <button type="submit" onClick={this.forgetPassword} className="btn btn-default" id="forgot-password-button">{locationHelper.translate("RESET_YOUR_PASSWORD")}</button>
                            </div>


                        </form>

                    </div>
                </main>
            );
        } else {
            return (
                <div loader={this.state.loader ? "show" : "hide"}><Loader/></div>
            );
        }
    }

    render() {
        const { rerender } = this.state;
        var {loader} = this.props;
        return !rerender ?
            (
                <div id="login-dialog" data-login-source="" className={"modal modal-customer-new open collapse" + (this.props.showLoginMenu.visibility ? ' show': '')} tabIndex="-1"
                     role="dialog" aria-labelledby="myLargeModalLabel">
                    <div className="socialmodal">
                        <div className="row">
                            <div className="search-loader-container hidden">
                                <div className="sk-cube-grid">
                                    <div className="sk-cube sk-cube1"></div>
                                    <div className="sk-cube sk-cube2"></div>
                                    <div className="sk-cube sk-cube3"></div>
                                    <div className="sk-cube sk-cube4"></div>
                                    <div className="sk-cube sk-cube5"></div>
                                    <div className="sk-cube sk-cube6"></div>
                                    <div className="sk-cube sk-cube7"></div>
                                    <div className="sk-cube sk-cube8"></div>
                                    <div className="sk-cube sk-cube9"></div>
                                </div>
                            </div>
                            <div className="col-xs-12 col-md-12">
                                <div className="socialmodal modal-body">
                                    {this.loadCards()}
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            )
            : null;
    }
}

function mapStateToProps(state) {
    return {
        loader: state.loader,
        showLoginMenu: state.showLoginMenu,
        showSignInForm: state.showSignInForm,
        signInDetails: state.signInDetails,
        lang: state.lang
    }
}


function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        showLoader,
        hideLoader,
        toggleLoginModal,
        toggleSignInForm,
        setSignIn,
        fetchUserProfile,
        fetchSocialSignInCustomer
    }, dispatch);
}

export default withCookies(connect(mapStateToProps, mapDispatchToProps)(PopupLogin));
