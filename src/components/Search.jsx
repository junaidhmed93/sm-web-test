import React from "react";
import ReactDOM from 'react-dom';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import { Link } from "react-router-dom";
import { withRouter } from 'react-router';
import commonHelper from "../helpers/commonHelper";
import locationHelper from "../helpers/locationHelper";
import {setCurrentCity, fetchSearch, fetchAllCities, fetchServices, showLoader, hideLoader, cityCodes, fetchAllServices, fetchAllApiServices, DEFAULT_LANG, LANG_AR, KSA_ID, setCurrentCityData, LANG_EN,setCurrentLang, fetchAllReviews} from "../actions/index";
import { withCookies } from "react-cookie";

class Search extends React.Component{
    _isMounted = false;
    constructor(props) {
        super(props);
        this.state = {term:'', results: [], showCities: false, selectedResult: 0, cuuerentSearchTerm: '', noResultFound: false};
        this.refInput = React.createRef();

        this.setCity = this.setCity.bind(this);
        this.searchTerm = this.searchTerm.bind(this);
        this.visiblity = this.visiblity.bind(this);
        this.toggleMenu = this.toggleMenu.bind(this);
        this.selectOption = this.selectOption.bind(this);
        this.containerClicked = this.containerClicked.bind(this);
        this.handleOutsideClick = this.handleOutsideClick.bind(this);
        this.changeResultListFocus = this.changeResultListFocus.bind(this)
        this.locationHelperInit = locationHelper.init.bind(this);
        this.locationHelperInit();

    }
    componentDidMount(){
        this._isMounted = true;
        if( this.props.cities.length <= 0 ){
            this.props.fetchAllCities();
        }
        const urlCity    = this.props.match.params.city;
        const reduxtCity = this.props.currentCity;
        const {lang} = this.props;
        if( typeof(urlCity) != "undefined" && typeof(reduxtCity) != "undefined"){
            if( urlCity != reduxtCity){
                this.props.showLoader();
                this.props.setCurrentCity(urlCity);
                this.props.fetchServices(urlCity, lang).then(()=> {
                    if (this._isMounted) {
                        this.props.hideLoader();
                    }
                });
            }
        }

    }
    componentDidUpdate(prevProps){
        // Fetch Service on Route Change between different Cities
        const prevCity    = prevProps.match.params.city;
        const currentCity = this.props.match.params.city;
        const {lang} = this.props;
        if( typeof(prevCity) != "undefined" && typeof(currentCity) != "undefined"){
            if( prevCity != currentCity){
                this.props.showLoader();
                this.props.setCurrentCity(currentCity);
                let fetchServices = this.props.fetchServices(currentCity, lang);
                let cityCode = typeof cityCodes[currentCity] != "undefined" ? cityCodes[currentCity] : 'DXB';
                const fetchReviews = this.props.fetchAllReviews(cityCode, '', lang);
                Promise.all([fetchServices,fetchReviews]).then(() => {
                    this.props.hideLoader()
                });
                //this.props.fetchServices(currentCity, lang).then(()=> this.props.hideLoader());
            }
        }

        // console.log('search', prevCity, currentCity);
	}
    componentWillUnmount() {
        document.removeEventListener('click', this.handleOutsideClick, false);
        this._isMounted = false;
    }
    containerClicked(e){
        if( !this.state.showCities ){
            document.addEventListener('click', this.handleOutsideClick, false);
        }else{
            document.removeEventListener('click', this.handleOutsideClick, false);
        }
    }
    handleOutsideClick(e){
        if( this.node.contains(e.target) ){
            return
        }
        // To ensure that one Select component is listen to the event
        document.removeEventListener('click', this.handleOutsideClick, false);
        this.setState({
            showCities: false
        });
    }
    renderResults(){
        const {currentCity, lang} = this.props;
        /*const {lang} = this.props.match.params;*/
        if (this.state.results.length > 0) {
            return this.state.results.map((item, index) => {
                var textSection = commonHelper.divideStringByterm(item._source.name, this.state.cuuerentSearchTerm);
                if (textSection.exist) {
                    return(
                        <Link onKeyDown={this.changeResultListFocus} ref={'result'+(index+1)} key={'result'+index} to={"/"+lang+"/"+currentCity+"/"+item._source.URL_Slug} className="dropdown-item" onClick={()=> this.selectOption(item._source.name)} ><span>{textSection.subBefore}</span><strong>{textSection.subHighlited}</strong><span>{textSection.subAfter}</span></Link>
                    );
                } else {
                    var indexInATerm = item._source.search_term.toLowerCase().indexOf(this.state.cuuerentSearchTerm.toLowerCase());
                    if(indexInATerm !== -1){
                        var aTerm = (commonHelper.getWordAtPosition(item._source.search_term, indexInATerm)).trim();
                        var textSection = commonHelper.divideStringByterm(aTerm, this.state.cuuerentSearchTerm);
                        return(
                            <Link onKeyDown={this.changeResultListFocus} ref={'result'+(index+1)} key={'result'+index} to={"/"+lang+"/"+currentCity+"/"+item._source.URL_Slug} className="dropdown-item" onClick={()=> this.selectOption(item._source.name)} ><span>{item._source.name + ' (' + textSection.subBefore}</span><strong>{textSection.subHighlited}</strong><span>{textSection.subAfter + ')'}</span></Link>
                        );

                    }
                }
            });
        }

    }
    renderCities(){
        return this.props.cities.map((item, index) => {
            return(
                <a key={item.id} className="dropdown-item" href="javascript:void(0)" onClick={()=> this.setCity(item)} >{item.name}</a>
            );
        });
    }
    updateLangSwitch(){
        /*let showLangSwitch = locationHelper.getCurrentCountryId() == KSA_ID ? true : false;
        if(showLangSwitch){
            this.setState({
                showLangSwitch: showLangSwitch
            })
        }*/
    }
    setCity(city){
        let {lang, cookies, cities, location, currentCity,userProfile} = this.props;

        this.props.showLoader();
        const current = city.slug;
        var currentCityVal = city.slug;
        
        cookies.set("user_city", current,{ path: '/'});

        var cityCode = typeof cityCodes[currentCityVal] != "undefined" ? cityCodes[currentCityVal] : 'DXB';
        
        var current_city_data = cities.filter((item) => item.slug == city.slug);

        if(current_city_data.length){
            let supportedLanguages =  current_city_data[0].supportedLanguages.filter((item) => item.language == lang);
            let pathname = location.pathname;
            let switchURL = "";
            let RedirectPage = false;
            if(!supportedLanguages.length){
                let defaltLanguage = current_city_data[0].supportedLanguages.filter((item) => item.default == 1);
                lang = defaltLanguage.length ? defaltLanguage[0].language : DEFAULT_LANG;
                switchURL = pathname.replace("/"+LANG_AR+"/", "/"+DEFAULT_LANG+"/");
                RedirectPage = true;
            }else if(commonHelper.isUserLoggedIn()){
                //console.log("userProfile", userProfile);
                if(typeof userProfile.preferredLanguage != "undefined"){
                    let custLang = userProfile.preferredLanguage.code;
                    if(custLang == LANG_AR && lang != LANG_AR){
                        switchURL = "";
                        switchURL = pathname.replace("/"+DEFAULT_LANG+"/", "/"+custLang+"/");
                        RedirectPage = true;
                    }
                }
            }
            if(RedirectPage){
                if(this.props.bodyClass == "home"){
                    switchURL = switchURL.replace("/"+currentCity, "/"+current);
                }else{
                    switchURL = switchURL.replace("/"+currentCity+"/", "/"+current+"/");
                }
                this.setState({
                    showCities: false
                });
                window.location = switchURL;
                return false;
            }
        }
        
        this.props.setCurrentCity(current);

        const currentLang = this.props.setCurrentLang(lang);
        const currentCityPromisse = this.props.setCurrentCity(currentCityVal);
        const servicesPromise = this.props.fetchServices(currentCityVal, lang);
        const fetchAllApiServicesPromise = this.props.fetchAllApiServices(cityCode, lang);
        let citiesPromise = this.props.fetchAllCities(currentCityVal, lang);
        
        let dispatchCall = [currentLang, citiesPromise, currentCityPromisse, servicesPromise, fetchAllApiServicesPromise];

        Promise.all(dispatchCall).then(() => {
            //console.log(servicesPromise);
            this.props.hideLoader();
        });

        switch(this.props.bodyClass){
            case 'home':
            this.props.history.push("/"+lang+"/"+current);
            break;
            case 'landing':
            this.props.history.push("/"+lang+"/"+current+"/"+this.props.match.params.slug);
            break;
            case 'companies':
            this.props.history.push("/en"+"/"+current+"/partners-list");
            break;
            case 'service_reviews':
            this.props.history.push("/"+lang+"/"+current+"/"+this.props.match.params.slug+"/reviews");
            break;
            case 'paymentconfirmation':
            window.location = "/"+lang+"/"+current;
            break;
            case 'quotesconfirmation':
                window.location = "/"+lang+"/"+current;
            break;
        }
        this.setState({
            showCities: false
        });
    }
    searchTerm(event){
        const term = event.target.value;
        let {currentCity, lang} = this.props;
        fetchSearch(currentCity, term, lang).then((results)=>
            this.setState({
                cuuerentSearchTerm: term,
                results,
                term,
                noResultFound: !results.length
            })
        );
        this.setState(state=> ({
            ...state,
            selectedResult: 0
        }));
    }
    visiblity(){
        const {results, term} = this.state;
        return results.length && term.length;
    }
    toggleMenu(){
        this.setState({
            showCities: !this.state.showCities
        })
    }
    selectOption(option){
        this.setState({
            results: [],
            term: option
        })
        this.refInput.current.value = option;
    }
    changeResultListFocus(e){
        if (e.keyCode === 40 || e.keyCode === 38) {
            var rate  = 1;
            if(e.keyCode === 38) rate = -1;

            e.preventDefault();
            if(this.state.results.length > 0) {
                var nextElement = (this.state.selectedResult + rate) % this.state.results.length;
                if(nextElement <= 0) {
                    nextElement = this.state.results.length;
                }
                ReactDOM.findDOMNode(this.refs['result' + nextElement]).focus();
                this.setState(state=> ({
                    ...state,
                    selectedResult: nextElement
                }));
            }
        }
            // const { cursor, result } = this.state
            // // arrow up/down button should select next/previous list element
            // if (e.keyCode === 38 && cursor > 0) {
            //     this.setState( prevState => ({
            //         cursor: prevState.cursor - 1
            //     }))
            // } else if (e.keyCode === 40 && cursor < result.length - 1) {
            //     this.setState( prevState => ({
            //         cursor: prevState.cursor + 1
            //     }))
            // }
    }
    componentWillReceiveProps (newProps) {
        if(newProps.mainServices != this.props.mainServices){
            window.REDUX_DATA["mainServices"] = newProps.mainServices;
        }
        if(newProps.currentCity != this.props.currentCity){
            window.REDUX_DATA["currentCity"] = newProps.currentCity;
        }
        if(newProps.currentCityData != this.props.currentCityData){
            window.REDUX_DATA["currentCityData"] = newProps.currentCityData;
        }
        if(newProps.bookingServices != this.props.bookingServices){
            window.REDUX_DATA["bookingServices"] = newProps.bookingServices;
        }
        if(newProps.currentCityID != this.props.currentCityID){
            window.REDUX_DATA["currentCityID"] = newProps.currentCityID;
        }
        if(newProps.lang != this.props.lang){
            window.REDUX_DATA["lang"] = newProps.lang;
        }
        
    }
    render() {
        // id="search-input" removed for chorome console bug
        let {currentCityData} = this.props;
        return (
            <div className="input-group justify-content-center">
                <div className="input-group-prepend" ref={node => { this.node = node; }} onClick={(e)=> this.containerClicked(e)}>
                    <button id={"cities-search-header"} className="btn btn-light dropdown-toggle px-md-4 border" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onClick={()=> this.toggleMenu()}>
                    <span className="mr-2 text-capitalize">{ currentCityData.length ? currentCityData[0].name : ''}</span>
                    </button>
                    <div className={"dropdown-menu"+(this.state.showCities ? ' show': '')}>
                        {this.renderCities()}
                    </div>
                </div>
                <div className="dropdown flex-fill search-result">
                    <input onKeyDown={this.changeResultListFocus} data-toggle="dropdown" type="text" ref={this.refInput} className="form-control clear-focus input-placeholder rounded-0 d-flex" placeholder={locationHelper.translate("START_TYPING_TO_FIND_SERVICE")} onChange={this.searchTerm} />
                    <div className={"dropdown-menu shadow w-100"+(this.visiblity() ? ' show': '')} aria-labelledby="dropdownMenuButton">
                        {this.renderResults()}
                    </div>
                    {
                        (this.state.noResultFound) ? (
                            <div className={"dropdown-menu shadow w-100 show"} aria-labelledby="dropdownMenuButton">
                                <p className="dropdown-item no-results">{locationHelper.translate("SORRY_WE_WERE_UNABLE_TO_FIND_THE_SERVICE_YOU_ARE_LOOKING_FOR")}</p>
                            </div>
                        ) : ''
                    }
                    
                </div>
                <div className="input-group-append">
                    <span className="input-group-text bg-white border-left-0">
                        <i className="fa fa-search"></i>
                    </span>
                </div>
                
            </div>
        );
    }
}

/*
window.REDUX_DATA["mainServices"] = reduxState.mainServices;
            window.REDUX_DATA["apiServices"] = reduxState.allServices;
            window.REDUX_DATA["bookingServices"] = reduxState.bookingServices;
            window.REDUX_DATA["currentCity"] = reduxState.currentCity;
            window.REDUX_DATA["currentCityData"] = reduxState.currentCityData;*/
function mapStateToProps(state){
	return {
        cities: state.cities,
        currentCity: state.currentCity,
        bodyClass: state.bodyClass,
        mainServices: state.mainServices,
        bookingServices: state.bookingServices,
        currentCityData: state.currentCityData,
        currentCityID: state.currentCityID,
        lang: state.lang,
        userProfile: state.userProfile,
	}
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({setCurrentCity, fetchSearch, fetchAllCities, fetchServices, showLoader, hideLoader, cityCodes, fetchAllServices, fetchAllApiServices, setCurrentLang, fetchAllReviews}, dispatch);
}

export default withRouter( withCookies(connect(mapStateToProps, mapDispatchToProps)(Search)));
