import React from "react";
import SocialLogin from 'react-social-login'

//class SocialButton extends React.Component{
//    constructor(props) {
//        super(props);
//    }
//
//    render({ children, triggerLogin, ...props }) {
//        return (
//            <button onClick={triggerLogin} {...props}>
//                { children }
//            </button>
//        );
//    }
//}
//
//export default SocialLogin(SocialButton);
const Button = ({ children, triggerLogin, ...props }) => (
    <button onClick={triggerLogin} {...props}>
        <span className="social-btn-center">{ children }</span>
    </button>
)

export default SocialLogin(Button)