import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import Loader from "../components/Loader";
import {isValidSection, sendFeedback, toggleFeedbackModal} from "../actions";
import TextFloatInput from "./Form_Fields/TextFloatInput";
import PhoneInput from "./Form_Fields/PhoneInput";
import TextareaInput from "./Form_Fields/TextareaInput";
import SelectInput from "./Form_Fields/SelectInput";
import commonHelper from "../helpers/commonHelper";
import locationHelper from '../helpers/locationHelper';

class PopupFeedback extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            loader: false,
            completed: false,
            error: false,
            service: {value: null},
            issuedescription: ''
        }

        this.onInputChange = this.onInputChange.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.hideMenu = this.hideMenu.bind(this);
        this.sendFeedbackComplaiment = this.sendFeedbackComplaiment.bind(this);
        this.locationHelperInit = locationHelper.init.bind(this);
        this.locationHelperInit();

    }

    componentDidMount() {
    }

    componentDidUpdate() {

    }

    onInputChange(name, value){
        this.setState({
            [name]: value
        });
    }

    hideMenu() {
        this.setState({
            loader: false,
            completed: false,
        })
        this.props.toggleFeedbackModal({visibility: false})
    }

    handleInputChange(name, value) {
        this.setState({
            [name]: value,
        });
    }
    sendFeedbackComplaiment() {
        if (isValidSection('footer-feedback')) {
            this.setState({
                loader: true
            });
            sendFeedback(this.state.email,this.state.name,this.state.issuedescription,this.state.service.value,this.state.service.label,this.state.input_phone,this.state.reference_number).then((res) => {
                if (res.data.ticket) {
                    this.setState({
                        completed: true
                    });
                } else {
                    this.setState({
                        loader: false
                    });
                }
            }).catch(error => {
                this.setState({
                    loader: false
                })
            })
        }

    }
    render() {
        var {loader} = this.state;
        let {lang} = this.props;
        var zendeskServicesOptions = commonHelper.zendeskMapForServices(lang);
        return !loader ?
            (
                <div id="footer-feedback" className={"modal-ss modal fade modal-customer open collapse" + (this.props.showFeedbackMenu.visibility ? ' show': '')}>
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <button onClick={()=> this.hideMenu()} type="button" className="close text-center" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <div className="footer-feedback-container">
                                <div className="footer-feedback-form">
                                    <div className="footer-feedback-form-container">
                                        <div className="footer-feedback-form-header">
                                            <div className="footer-feedback-form-title">
                                                {locationHelper.translate('FEEDBACK_TITLE_TXT')}
                                                <p>{locationHelper.translate('FEEDBACK_DESCRIBE_TXT')}</p>
                                            </div>

                                        </div>
                                        <div className="footer-feedback-content">
                                            <div className="row py-3">
                                                <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    <div className="footer-feedback-form-field">
                                                        <div className="footer-feedback-form-input">
                                                            <SelectInput name="service" inputValue={this.state.service} onInputChange={this.handleInputChange}
                                                            label={locationHelper.translate('SELECT_A_SERVICE')} options={zendeskServicesOptions}  validationClasses="required" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    <div className="footer-feedback-form-field">
                                                       <div className="footer-feedback-form-input">
                                                           <TextFloatInput InputType="text" id="reference_number" inputValue={this.state.reference_number}  
                                                           name="reference_number" label={locationHelper.translate('REFERENCE_NO')} onInputChange={this.onInputChange} 
                                                           validationClasses="required" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="footer-feedback-form-field">
                                                <div className="footer-feedback-form-input">
                                                    <TextareaInput name="issuedescription" placeholder={locationHelper.translate('DESCRIBE_YOUR_ISSUE')}
                                                     inputValue={this.state.issuedescription} onInputChange={this.onInputChange} validationClasses="required" validationMessage={locationHelper.translate("THIS_FIELD_IS_REQUIRED")} />
                                                </div>
                                            </div>
                                            <div className="row mb-3">
                                                <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    <div className="footer-feedback-form-field">
                                                        <div className="footer-feedback-form-input">
                                                            <TextFloatInput InputType="text" id="name" name="name" label={locationHelper.translate('NAME')}
                                                             inputValue={this.state.name} onInputChange={this.onInputChange} validationClasses="required" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    <div className="footer-feedback-form-field">
                                                        <div className="footer-feedback-form-input">
                                                            <TextFloatInput InputType="email" id="email" name="email" label={locationHelper.translate('EMAIL_ADDRESS')} 
                                                            inputValue={this.state.email} onInputChange={this.onInputChange} validationClasses="required email" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="footer-feedback-form-field mb-3">
                                                <PhoneInput name="input_phone" inputValue={this.state.input_phone} onInputChange={this.onInputChange} validationClasses="required phone" />
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div
                                                className="col-lg-12 col-md-12 col-sm-12 col-xs-12 cc-successfull ticket-creation-success hidden">
                                                <div className="text-center pth">
                                                    <i className="successfull-icon mrs"></i>
                                                    <p className="mtm">{locationHelper.translate('REQUEST_SUBMITTED_TXT')}</p>
                                                </div>
                                                <br />
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div
                                                className="col-lg-12 col-md-12 col-sm-12 col-xs-12 cc-decline ticket-creation-error hidden">
                                                <div className="text-center pth">
                                                    <i className="decline-icon mrs"></i>
                                                    <p className="mtm">{locationHelper.translate('FEEDBACK_ERROR_MSG')}</p>
                                                </div>
                                                <br />
                                            </div>
                                        </div>
                                        <div className="footer-feedback-form-footer text-center col-4 m-auto py-3">
                                            <button onClick={this.sendFeedbackComplaiment} className="btn btn-block btn-warning font-weight-bold 
                                            text-uppercase" id="reset-password-button">{locationHelper.translate('SEND_BTN_TXT')}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )
            : (
                <div id="footer-feedback" className={"modal-ss modal fade modal-customer open collapse" + (this.props.showFeedbackMenu.visibility ? ' show': '')}>
                    <div className="modal-dialog">
                        <div className="modal-content">
                            {
                                this.state.completed ? [
                                    <button onClick={()=> this.hideMenu()} type="button" className="close text-center" data-dismiss="modal"
                                            aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>,
                                    <div className="footer-feedback-container">
                                        <div className="footer-feedback-form mb-3">
                                            <div className="footer-feedback-form-container">
                                                <div className="footer-feedback-form-header">
                                                    <div className="footer-feedback-form-title">
                                                        {locationHelper.translate('THANK_YOU_FOR_YOUR_FEEDBACK_OUR_CUSTOMER_SERVICE_TEAM_WILL_GET_IN_TOUCH_WITH_YOU')}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                ] : ''
                            }
                            <Loader completed={this.state.completed} />
                            {
                                this.state.completed ? (
                                    <p className="text-center mb-5">{locationHelper.translate('REQUEST_SUBMITTED_TXT')}</p>
                                ) : ''
                            }
                        </div>
                    </div>
                </div>
            );
    }
}

function mapStateToProps(state) {
    return {
        showFeedbackMenu: state.showFeedbackMenu,
        lang: state.lang,
        currentCity: state.currentCity
    }
}


function mapDispatchToProps(dispatch) {
    return bindActionCreators({toggleFeedbackModal}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(PopupFeedback);
