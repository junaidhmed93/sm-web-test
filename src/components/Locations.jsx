import React from "react";
import { Link } from "react-router-dom";
import {imgixReactBase, imgixWPBase, quality, relativeURL} from '../imgix';
import LazyLoad from 'react-lazyload';
import PlaceholderComponent from '../components/Placeholder';
import OurApps from "./OurApps";
import LazyLoadImage from "./LazyLoadImage";
import { withCookies } from "react-cookie";
import { connect } from "react-redux";
import locationHelper from "../helpers/locationHelper";
import { showLangNav, LANG_AR, ABU_DHABI, LANG_EN, DUBAI, SHARJAH, DOHA, JEDDAH, RIYADH, MUSCAT, KUWAIT,DAMMAM } from "../actions/index";
class Locations extends React.Component{
    constructor(props) {
        super(props);
        this.locationHelperInit = locationHelper.init.bind(this);
        this.locationHelperInit();
        this.renderLink = this.renderLink.bind(this);
    }
    renderLink(city, cityText){
        let {lang, currentCity} = this.props;
        let langVisibility =  showLangNav(city);
        if(lang == LANG_AR && langVisibility){
            return (<Link className="text-dark" to={"/"+lang+"/"+city}>{locationHelper.translate(cityText)}</Link>);
        }
        else if(lang == LANG_EN){
            //console.log("s dsad sadsa d");
            return (<Link className="text-dark" to={"/"+lang+"/"+city}>{locationHelper.translate(cityText)}</Link>);
        }else{
            return (<a className="text-dark" href={"/"+LANG_EN+"/"+city}>{locationHelper.translate(cityText)}</a>);
        }
        
    }
    render() {
        
        return (
           <div className="row pb-5">
                <div className="col px-4 px-md-3 col-md-9">
                    <div className="row">
                        <div className="col-6 col-md">
                            <ul className="list-unstyled">
                                <li className="mb-3">
                                    <strong>{locationHelper.translate("UNITED_ARAB_EMIRATES")}</strong>
                                </li>
                                <li className="mb-3">
                                    {this.renderLink(ABU_DHABI,"ABU_DHABI")}
                                </li>
                                <li className="mb-3">
                                    {this.renderLink(DUBAI,"DUBAI")}
                                </li>
                                <li className="mb-3">
                                {this.renderLink(SHARJAH,"SHARJAH")}
                                </li>
                            </ul>
                        </div>
                        <div className="col-6 col-md">
                            <ul className="list-unstyled">
                                <li className="mb-3">
                                    <strong>{locationHelper.translate("QATAR")}</strong>
                                </li>
                                <li className="mb-3">
                                    {this.renderLink(DOHA,"DOHA")}
                                </li>
                            </ul>
                        </div>
                        <div className="col-6 col-md">
                            <ul className="list-unstyled">
                                <li className="mb-3">
                                    <strong>{locationHelper.translate("SAUDI_ARABIA")}</strong>
                                </li>
                                <li className="mb-3">
                                    {this.renderLink(JEDDAH,"JEDDAH")}
                                </li>
                                <li className="mb-3">
                                    {this.renderLink(RIYADH,"RIYADH")}
                                </li>
                                <li className="mb-3">
                                    {this.renderLink(DAMMAM,"DAMMAM")}
                                </li>
                            </ul>
                        </div>
                        <div className="col-6 col-md mt-2 mt-md-0">
                            <ul className="list-unstyled">
                                <li className="mb-3">
                                    <strong>{locationHelper.translate("OMEN")}</strong>
                                </li>
                                <li className="mb-3">
                                    {this.renderLink(MUSCAT,"MUSCAT")}
                                </li>
                            </ul>
                        </div>
                        <div className="col-6 col-md mt-2 mt-md-0">
                            <ul className="list-unstyled">
                                <li className="mb-3">
                                    <strong>{locationHelper.translate("KUWAIT")}</strong>
                                </li>
                                <li className="mb-3">
                                    {this.renderLink(KUWAIT,"KUWAIT_CITY")}
                                </li>
                            </ul>
                        </div>
                        <div className="col-6 col-md mt-2 mt-md-0">
                            <ul className="list-unstyled">
                                <li className="mb-3">
                                    <strong>{locationHelper.translate("BAHRAIN")}</strong>
                                </li>
                                <li className="mb-3">
                                    {this.renderLink("bahrain","MANAMA")}
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="col-3 d-none d-md-block">
                    <LazyLoad height={226} offset={500} placeholder={<PlaceholderComponent isImage={true} placeholderSrc={imgixReactBase+"/dist/images/map.png"} width="342" className="mw-100" />}>
                        <img src={imgixReactBase+"/dist/images/map.png?fit=crop&w=342&h=226&auto=format,compress&q="+quality} className="mw-100" alt="" />
                    </LazyLoad>
                </div>
            </div>
        );
    }
}

Locations.defaultProps = {
    isBotData: false
};
function mapStateToProps(state){
    return {
        currentCity: state.currentCity,
        lang: state.lang
    }
}
export default withCookies( connect(mapStateToProps) (Locations)) ;