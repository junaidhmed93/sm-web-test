import React from "react";
import IconWithLink from './IconWithLink';
import IconWithModal from './IconWithModal';


class IconTextGrid extends React.Component{
    constructor(props) {
        super(props);
    }
    renderList(){
        const {data, hasModal} = this.props;
        return data.map((item, index) => {
            if( hasModal ){
                return(<IconWithModal key={item.id} item={item} />);
            }else{
                return(<IconWithLink key={item.id} item={item} />);
            }
        });
    }

    render() {
        return (
            <div className="row mb-4">
                <div className="col">
                    <div className="row">
                        {this.renderList()}
                    </div>
                </div>
            </div>
        );
    }
}

IconTextGrid.defaultProps = {
  title: '',
  hasModal: false,
  data: []
};
export default IconTextGrid;
