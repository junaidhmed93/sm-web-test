import React from "react";
import {imgixWPBase, relativeURL, quality} from '../imgix';

class DefaultBanner extends React.Component{
    constructor(props) {
        super(props);
    }
    createMarkup(htmlString) {
        return {__html: htmlString};
    }
    render() {
        return (
            <section className={"hero "  + (this.props.isServiceLanding?"py-5":"py-3")} style={{background: "url("+imgixWPBase+relativeURL(this.props.background)+"?fit=crop&w=1349&h="+ (this.props.isServiceLanding?"442":"300") + "&auto=format,compress&q="+quality+(this.props.imageBlurBlend ? '&exp=-7' : '')+") center/cover no-repeat", minHeight: (this.props.isServiceLanding?"442px":"300px"), display: 'flex', 'alignItems':'center'}}>
                <div className={"container " + (this.props.isServiceLanding?"py-5":"py-3")}>
                    <h1 className="text-center text-white font-weight-bold">{ this.props.h1Text != "" ? this.props.h1Text : this.props.title}</h1>
                    {(this.props.description.length && this.props.showSecondHeader)  ? ( <h2 className="text-white text-center" dangerouslySetInnerHTML={this.createMarkup( this.props.h2Text != "" ? this.props.h2Text : this.props.description)}></h2> ) : ''}
                    {this.props.children}
                </div>
            </section>
        );
    }
}
DefaultBanner.defaultProps = {
    h1Text:'',
    h2Text:'',
  background: '',
  title: '',
  description: '',
  imageBlurBlend: true,
    isServiceLanding: false,
    showSecondHeader: true
};

export default DefaultBanner;
