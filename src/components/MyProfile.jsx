import React from "react";
import {imgixWPBase, relativeURL, quality} from '../imgix';
import FormTitleDescription from "./FormTitleDescription";
import TextFloatInput from "./Form_Fields/TextFloatInput";
import SelectInput from "./Form_Fields/SelectInput";
import PhoneInput from "./Form_Fields/PhoneInput";
import {bindActionCreators} from "redux";
import {setSignIn, fetchUserProfile, putUpdateCustomerProfile, isValidSection, DUBAI, LANG_AR,fetchAllCities} from "../actions";
import Loader from "../components/Loader";
import {connect} from "react-redux";
import locationHelper from "../helpers/locationHelper";
import AreaSuggestionInput from "./Form_Fields/AreaSuggestionInput";
import commonHelper from "../helpers/commonHelper";

class MyProfile extends React.Component{
    constructor(props) {
        super(props);

        var cityOptions = locationHelper.fetchCitiesforUserProfile();
        this.locationHelperInit = locationHelper.init.bind(this);
        this.locationHelperInit();
        this.state = {
            loader: false,
            input_email: props.userProfile.email,
            input_phone: props.userProfile.address?props.userProfile.address.phoneNumber:'',
            input_name: props.userProfile.customerFirstName,
            input_last_name: props.userProfile.customerLastName,
            city: '',
            area: '',
            building: props.userProfile.address?props.userProfile.address.building:'',
            apartment: props.userProfile.address?props.userProfile.address.apartment:'',
            cityOptions: cityOptions
        };
        this.fetchDataIfNeeded = this.fetchDataIfNeeded.bind(this);
        this.onInputChange = this.onInputChange.bind(this);
        this.updateProfile = this.updateProfile.bind(this);
        this.getUserProfile = this.getUserProfile.bind(this);
        this.setUserArea = this.setUserArea.bind(this);
        this.getUserSelectedArea = this.getUserSelectedArea.bind(this);
        this.fetchDataIfNeeded(props);

    }
    setUserArea(){
        var area = '';
        const {userProfile, lang} = this.props;
        
        if(userProfile.address != null){
            let userCity = (userProfile.address && userProfile.address.city) ? userProfile.address.city : '';
            userCity = (userCity != '' && lang == LANG_AR) ? userCity : commonHelper.slugify(userCity);
            var city =  locationHelper.getLocationByName(userCity);
            if( typeof city.id == "undefined" && typeof userProfile.customerCityId != "undefined"){ //RW-1129
                city = locationHelper.getLocationById(userProfile.customerCityId);
                if( typeof city.id == "undefined" ){
                    let serviceLocationById = locationHelper.getServiceLocationById(userProfile.customerCityId,'itemId');
                    city = locationHelper.getLocationById(serviceLocationById);
                }
            }
            if(userProfile.address != null && userProfile.address.area_id == null){
                area = userProfile.address.area;
            }else{
                var from_areas = locationHelper.getAreasByCity(city.id);
                if(from_areas && from_areas.length && userProfile.address.area != null ){
                    area = locationHelper.getAreaByName(from_areas, userProfile.address.area,lang);
                }
                else{
                    area = "";
                }
            }
        }else{ //Default
            city = locationHelper.getLocationByName(DUBAI);
            area = "";
        }
        if( typeof city.relativeId == "undefined" ){
            //let cityValue = locationHelper.getServiceLocationById(city.id)
            //console.log("city", city, this.state.cityOptions);
            city["relativeId"] = city.cityId;
            //console.log("city 1332", city);
        }
        this.setState({
            city: city,
            area: area,
        });
    }
    componentWillReceiveProps(newProps) {
        if (newProps.cities !== this.props.cities) {
            window.REDUX_DATA["cities"] = newProps.cities;
            locationHelper.allCites = newProps.cities;
            //var areaData = this.setUserArea();
            this.setUserArea();
        }
    }
    fetchDataIfNeeded(props){
        this._isMounted = true;
        var lang = props.lang;
        var promiseFetch = [];
        let city = this.props.currentCity;
        const citiesPromise =  this.props.fetchAllCities(city, lang, false);
        promiseFetch.push(citiesPromise);
        Promise.all(promiseFetch).then(() => {
            if (this._isMounted) {
                this.setState({
                    loader: false
                })
            }
        }).catch(function (error) {
            console.log("Error fetchDataIfNeeded", error);
        });
        this.setState({
            loader: true
        });
    }

    componentDidMount() {
       this.setUserArea() 
    }

    onInputChange(name, value){
        this.setState({
            [name]: value
        });
        if(name == 'city') {
            this.setState({
                'area': ''
            })
        }
    }
    getUserProfile(token, lang) {
        this.props.fetchUserProfile(token, lang).then(res => {
            this.setState({
                loader: false
            });
        });
    }
    getUserSelectedArea(stateData, returnText = false){
        let areaId = (stateData.area != null && typeof stateData.area.id != "undefined") ? stateData.area.id : null;
        //areaId = ( typeof stateData.userAddress != "undefined" && (stateData.userAddress.area_id != null && typeof stateData.userAddress.area_id != "undefined")) ? stateData.userAddress.area_id : areaId;
        return returnText ? stateData.area : areaId;
    }
    updateProfile() 
    { 
        const { lang } = this.props;
        if(isValidSection('profile-form')) {
            this.setState({
                loader: true
            });
            let stateData = this.state;

            let userSelectedArea = this.getUserSelectedArea(stateData);
            let data = {
                "apartment": this.state.apartment,
                //"area": this.state.area.id,
                "building": this.state.building,
                "cityId": this.state.city.relativeId,
                "phoneNumber": this.state.input_phone,
            }
            if(userSelectedArea == null){
                //"area": this.getUserSelectedArea(stateData),
                data['area'] = null;
                data['areaTitle'] = this.getUserSelectedArea(stateData, true);
            }else{
                data['area'] = userSelectedArea;
            }

            console.log(data)

            putUpdateCustomerProfile(this.props.signInDetails.access_token,
                this.props.signInDetails.customer.id,
                this.state.input_email,
                this.state.input_name,
                this.state.input_last_name, 
                data,
                lang).then((res) => {
                this.getUserProfile(this.props.signInDetails.access_token, lang);
            })
                .catch(error => {

                    this.setState({
                        loader: false
                    });

                });
        }

    }
    render() {
        var from_areas = locationHelper.getAreasByCity(this.state.city.id);
        
        if (!this.state.loader) {
            return (
                <form id="profile-form" className="col-lg-9 col-md-9 col-sm-12 col-xs-12 user-right-con" >
                    <section>
                        <FormTitleDescription title={locationHelper.translate("CONTACT_INFORMATION")} desc=""/>
                    </section>
                    <section>
                        <div className="row">
                            <div className="col-12 col-md-6 pr-2 pr-sm-3 mb-3">
                                <TextFloatInput InputType="text" name="input_name" label={locationHelper.translate("FIRST_NAME")} inputValue={this.state.input_name} onInputChange={this.onInputChange} validationClasses="required"/>
                            </div>
                            <div className="col-12 col-md-6 pr-2 pr-sm-3 mb-3">
                                <TextFloatInput InputType="text" name="input_last_name" label={locationHelper.translate("LAST_NAME")} inputValue={this.state.input_last_name} onInputChange={this.onInputChange} validationClasses="required"/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-md-6 pr-2 pr-sm-3 mb-3">
                                <TextFloatInput InputType="email" name="input_email" label={locationHelper.translate("EMAIL")} inputValue={this.state.input_email} readOnlyInput={true} onInputChange={this.onInputChange} validationClasses="required email"  />
                            </div>
                            <div className="col-12 col-md-6 pr-2 pr-sm-3 mb-3">
                                <PhoneInput name="input_phone" inputValue={this.state.input_phone} onInputChange={this.onInputChange} validationClasses="required phone" />
                            </div>
                        </div>
                    </section>
                    <section>
                        <FormTitleDescription title={locationHelper.translate("ADDRESS_INFORMATION")} desc=""/>
                    </section>
                    <section>
                        <div className="row">
                            <div className="col-12 col-md-6 pr-2 pr-sm-3 mb-3">
                                <SelectInput name="city" label={locationHelper.translate("CITY")} inputValue={this.state.city} options={this.state.cityOptions} onInputChange={this.onInputChange} validationClasses="required" />
                            </div>
                            {
                                (from_areas && from_areas.length>0) ?
                                    (
                                        <div className="col-12 col-md-6 pr-2 pr-sm-3 mb-3">
                                            <AreaSuggestionInput 
                                                InputType="text" 
                                                areas={from_areas} 
                                                inputValue={this.state.area} 
                                                name="area" 
                                                label={locationHelper.translate("AREA")} 
                                                onInputChange={this.onInputChange}
                                                validationClasses="required"
                                                allowNonArea = {true}
                                                userAreaField = {true}
                                                />
                                        </div>
                                    ):
                                    ''
                            }
                        </div>
                        <div className="row">
                            <div className="col-12 col-md-6 pr-2 pr-sm-3 mb-3">
                                <TextFloatInput InputType="text" name="building"
                                                label={locationHelper.translate("BUILDING_OR_STREET_NO")} validation={{required: 'required'}}
                                                inputValue={this.state.building} onInputChange={this.onInputChange}/>
                            </div>
                            <div className="col-12 col-md-6 pr-2 pr-sm-3 mb-3">
                                <TextFloatInput InputType="text" name="apartment"
                                                label={locationHelper.translate("APARTMENT_VILLA_NO")} validation={{required: 'required'}}
                                                inputValue={this.state.apartment} onInputChange={this.onInputChange}/>
                            </div>
                        </div>
                    </section>
                    <div className="pt-5" style={{"display":"flex","justify-content": "center"}}>
                        <a onClick={this.updateProfile} href="javascript:void(0)" className="btn btn-blue btn-lg px-5">{locationHelper.translate("UPDATE")}</a>
                    </div>
                </form>
            );
        } else {
            return (
                <main className={"col-lg-9 col-md-9 col-sm-12 col-xs-12 user-right-con " + (!this.state.loader ? "show": "hide")}><Loader/></main>
            );
        }
    }
}

function mapStateToProps(state) {
    return {
        signInDetails: state.signInDetails,
        userProfile: state.userProfile,
        lang:state.lang,
        currentCity: state.currentCity,
        currentCityID: state.currentCityID,
        currentCityData: state.currentCityData,
        dataConstants: state.dataConstants,
        newDataConstants: state.newDataConstants,
        cities: state.cities
    }
}


function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        setSignIn,
        fetchUserProfile,
        fetchAllCities,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(MyProfile);
