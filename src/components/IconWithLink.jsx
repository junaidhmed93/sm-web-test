import React from "react";
import {imgixWPBase, relativeURL} from "../imgix";
import LazyLoad from 'react-lazyload';
import PlaceholderComponent from '../components/Placeholder';
import { Link } from "react-router-dom";

class IconWithLink extends React.Component{
    constructor(props) {
        super(props);
    }

    render() {
        const {item} = this.props;
        return (
            <div className="col-6 col-md-3 text-center mb-3">
                <Link to={item.url==''?'#':'/en/'+item.url} className="p">
                    {item.image ?
                        <LazyLoad offset={500} height={64} placeholder={<PlaceholderComponent />}><img width={64} height={64} src={imgixWPBase+relativeURL(item.image)+"?fit=crop&h=64&auto=format,compress"}/></LazyLoad>
                        :
                        <i className="fa fa-4x fa-google-wallet text-warning"></i>
                    }
                    <p>{item.name}</p>
                </Link>
            </div>
        );
    }
}

export default IconWithLink;
