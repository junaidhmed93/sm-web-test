import React from "react";

class CenteredText extends React.Component{
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className={this.props.parentClass}>
                <div className={this.props.childClass}>
                    <p className="h3">{this.props.FirstText}</p>
                    <div className="row">
                        <div className="col col-md10 mx-auto">
                            <p>{this.props.SecondText}</p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

CenteredText.defaultProps = {
  parentClass: 'row my-5',
  childClass: 'col text-center my-4',
  FirstText: '',
  SecondText: ''
};

export default CenteredText;
