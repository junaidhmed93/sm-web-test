import React from "react";
import {imgixReactBase, imgixWPBase, quality, relativeURL} from '../imgix';
import LazyLoad from 'react-lazyload';
import PlaceholderComponent from '../components/Placeholder';
import LazyLoadImage from "./LazyLoadImage";
import locationHelper from "../helpers/locationHelper";
import {connect} from "react-redux";

class OurApps extends React.Component{
    constructor(props) {
        super(props);
        this.locationHelperInit = locationHelper.init.bind(this);
        this.locationHelperInit();
    }

    render() {
        return (
           <div className={this.props.parentClass}>
                <div className={this.props.childClass}>
                    <div className="card bg-light">
                        <div className="card-body pb-0">
                            <div className="row">
                                <div className="col-8 col-md-10">
                                    <div className="row align-items-center justify-content-between">
                                        <div className="col-12 col-md-7">
                                            <p className="h3 text-primary font-weight-bold">{locationHelper.translate("BOOK_AND_MANAGE_INOUR_APP")}</p>
                                            <p className="text-muted">{locationHelper.translate("DOWNLOAD_THE_APPAND_ENJOY")}</p>
                                        </div>
                                        <div id="apps_logos" className="col-12 col-md-5 text-md-right mb-3">
                                            <a href="https://servicemarket.onelink.me/R9tZ/2bf82707">
                                                <LazyLoad height={70} offset={500} placeholder={<PlaceholderComponent className="img-fluid" placeholderSrc={imgixReactBase+"/dist/images/google-play.png"} isImage={true} width="47%"/>}>
                                                    <img className="img-fluid" src={imgixReactBase+"/dist/images/google-play.png?w=200&h=70&auto=format,compress"} width="47%" alt="" />
                                                </LazyLoad>
                                            </a>
                                            <a href="https://servicemarket.onelink.me/R9tZ/47e24049">
                                                <LazyLoad height={70} offset={500} placeholder={<PlaceholderComponent className="img-fluid" placeholderSrc={imgixReactBase+"/dist/images/apple-itunes.png"} isImage={true} width="47%"/>}>
                                                    <img className="img-fluid" src={imgixReactBase+"/dist/images/apple-itunes.png?w=200&h=70&auto=format,compress"} width="47%" alt="" />
                                                </LazyLoad>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-4 col-md-2 pl-0 d-flex align-items-end">
                                    <LazyLoad height={160} offset={500} placeholder={<PlaceholderComponent height={160} placeholderSrc={imgixReactBase+"/dist/images/phones.png"} className="d-block mw-100" style={{position: "absolute", bottom: "0", right: "0"}} />}>
                                        <img style={{position: "absolute", bottom: "0", right: "0"}} className="d-block mw-100" src={imgixReactBase+"/dist/images/phones.png?fit=crop&w=168&h=159&auto=format,compress"} alt="" />
                                    </LazyLoad>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
OurApps.defaultProps = {
  parentClass: '',
  childClass: '',
    isBotData: false
};
function mapStateToProps(state){
	return {
		currentCity: state.currentCity,
		lang: state.lang
	}
}
export default connect(mapStateToProps)(OurApps);
