import React from "react";


class SocialMedia extends React.Component{
    constructor(props) {
        super(props);
    }
    renderList(){
        // var socialItems = [{id: 1, faClass: 'fa-phone', URL: 'tel:+97144229639'}, {id: 2, faClass: 'fa-instagram', URL: 'https://www.instagram.com/myservicemarket/?hl=en'}, {id: 3, faClass: 'fa-facebook', URL: 'https://www.facebook.com/myservicemarket'}, {id: 4, faClass: 'fa-envelope', URL: 'mailto:support@servicemarket.com'}];
        var socialItems = [{id: 1, faClass: 'fa-phone', URL: 'tel:+97144229639', label: '+97144229639'}, {id: 2, faClass: 'fa-envelope', URL: 'mailto:support@servicemarket.com', label: 'support@servicemarket.com'}];

        return socialItems.map((item, index) => {
            return(
                <a key={item.id} href={item.URL} className={"mr-3 d-block d-md-inline no-underline " + (index!=0?'pl-md-5':'')}>
                    <span className="fa-stack fa-lg">
                        <i className="fa fa-circle text-primary fa-stack-2x"></i>
                        <i className={"fa fa-stack-1x text-white " + item.faClass} ></i>
                    </span>
                    <span className="pl-3">{item.label}</span>
                </a>
            );
        });
    }

    render() {
        return (
        <div className="row mb-5">
            <div className="col">
                {this.renderList()}
            </div>
        </div>
        );
    }
}

export default SocialMedia;
