import React from "react";
import Search from "./Search";
import { Link } from "react-router-dom";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { imgixReactBase, quality } from '../imgix';
import LazyLoad from 'react-lazyload';
import PlaceholderComponent from '../components/Placeholder';
import { toggleMainMenu, toggleLoginModal, toggleSignInForm, scrollToTop, getLogo, KSA_ID, DEFAULT_LANG,showLangNav } from "../actions/index";
import { setSignIn, setUserProfile } from "../actions";
import { withCookies } from "react-cookie";
import locationHelper from "../helpers/locationHelper";
import commonHelper from "../helpers/commonHelper";

class Header extends React.Component {
    constructor(props) {
        super(props);

        this.showMenu = this.showMenu.bind(this);
        this.showLoginModal = this.showLoginModal.bind(this);
        this.showMobileSearch = this.showMobileSearch.bind(this);
        this.locationHelperInit = locationHelper.init.bind(this);
        this.locationHelperInit();
        this.state = {
            servicesPopup: false,
            cityObj: null,
            hashValue : ''
        }
    }
    componentDidMount() {
        const {cities, currentCity, location} = this.props;
        const cityObj = cities.find((a) => { return a.slug.toLowerCase() == currentCity });
        
        let hashValue = (typeof location.hash != "undefined" && location.hash != "") ? location.hash : ""; 

        if (cityObj != null) {
            this.setState({ 
                cityObj: cityObj,
                hashValue: hashValue
            });
        }
        
    }
    componentWillReceiveProps(nextProps) {
        //console.log('nextProps',nextProps);
        let currentCity = nextProps.currentCity;
        if (currentCity.toLowerCase() == 'abu-dhabi')
            currentCity= 'abu dhabi';
        if (nextProps.cities) {
            const cityObj = nextProps.cities.find((a) => { return a.name.toLowerCase() == currentCity });
            if (cityObj != null) {
                this.setState({ cityObj: cityObj });
            }
        }
        if( nextProps.location !== this.props.location ) {
            let location = nextProps.location;
            let hashValue = (typeof location.hash != "undefined" && location.hash != "") ? location.hash : ""; 
            this.setState({ 
                hashValue: hashValue
            });
        }
    }
    showMobileSearch(event) {
        let searchElement = document.getElementById('navbarSupportedContent');
        let hasShowClass = searchElement.className.indexOf('show') != -1;
        if (hasShowClass) {
            searchElement.className = searchElement.className.replace(' show', '');
        } else {
            searchElement.className += ' show';
        }
    }
    showSearchBar() {
        if (this.props.bodyClass == 'home' || this.props.bodyClass == 'booking' || this.props.bodyClass == 'quotes') {
            return false;
        } else {
            return true;
        }
    }
    showMenu() {
        this.props.toggleMainMenu(!this.state.servicesPopup);
        if (!this.state.servicesPopup) {
            document.body.classList.add('prevent-scroll-mobile');
        } else {
            document.body.classList.remove('prevent-scroll-mobile');
        }
        this.setState({
            servicesPopup: !this.state.servicesPopup
        });
    }
    showLoginModal() {
        this.props.toggleLoginModal({ visibility: true, guest: false });
    }
    logout() {
        this.props.setSignIn({});
        const { cookies } = this.props;
        cookies.remove('_user_details', { path: '/' });
        cookies.remove('_booking_data', { path: '/' });
        cookies.remove('_confirmation_summary', { path: '/' });
        cookies.remove('_coupon_data', { path: '/' });
        setUserProfile({});
        // this.props.history.push("/" + this.props.match.params.lang + "/" + this.props.currentCity);
        window.location.reload();
    }
    render() {
        const { cityObj, hashValue } = this.state;
        const { currentCity, lang, langSwitchVisibility, location } = this.props;
        let pathname = location.pathname;
    
        let langVisibility =  showLangNav(currentCity, commonHelper.urlArgs("", pathname));

        let switchURL = lang == DEFAULT_LANG ?  pathname.replace("/"+DEFAULT_LANG+"/", "/ar/") : pathname.replace("/ar/", "/"+DEFAULT_LANG+"/");

        switchURL += hashValue; 

        return (
            <header className={this.props.bodyClass} >
                <nav className="navbar navbar-expand-lg navbar-light bg-white py-lg-0 shadow-1">
                    <div className="d-flex align-items-center justify-content-between">
                        <span onClick={() => this.showMenu()} className="pointer drawer-toggle d-md-none" data-toggle="collapse" data-target="#drawer">
                            <i className="fa fa-bars mr-1 text-secondary" style={{ fontSize: "1.3em" }}></i>
                        </span>
                        <Link to={"/"+lang+"/" + currentCity} className="navbar-brand px-3 py-3 mr-0">
                            <object id={"main-logo-object"} data={getLogo(lang)} height={'32px'} alt="ServiceMarket" type="image/svg+xml"></object>
                        </Link>
                    </div>
                    {/* <div className="d-flex d-md-none align-items-center justify-content-between">
                        <a href="tel:0097144229639" className="text-dark">
                            <span className="h5 m-0 d-block">+971 4 4229639</span>
                            <span className="h6 m-0 d-block">Sat - Thu: 8am - 8pm Fri: 9am - 6pm</span>
                        </a>
                    </div> */}
                    {cityObj ?
                        <div className="d-flex d-md-none align-items-center justify-content-between">
                            <a href={'tel:' + cityObj.phoneNumber} className="text-dark">
                                <span className="h5 m-0 d-block ltr">{cityObj.phoneNumber.substr(0, 4) + ' ' + cityObj.phoneNumber.substr(4, 1) + ' ' + cityObj.phoneNumber.substr(5)}</span>
                                <span className="h6 m-0 d-block">{cityObj.hoursInWeek}</span>
                            </a>
                        </div> :
                        null}
                    <button className="navbar-toggler border-0 d-none" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse show" id="navbarSupportedContent">
                        <ul className="navbar-nav align-items-lg-center justify-content-between flex-fill">
                            <li className="nav-item dropdown py-lg-3 px-3 d-none d-md-block">
                                <a onClick={() => this.showMenu()} className="nav-link dropdown-toggle h5 font-weight-bold m-0 drawer-toggle" href="javascript:void(0)" role="button">
                                    {locationHelper.translate("OUR_SERVICES")}&nbsp;
                                </a>
                            </li>
                            <li className={"nav-item flex-fill px-md-3 my-2 my-md-0 mobile-search" + (!this.showSearchBar() ? ' d-none d-md-none' : '')}>
                                <Search />
                            </li>
                            {
                                langVisibility ? (<li className="nav-item px-2 ml-auto d-none d-lg-block lang-switch">
                                    <a href={switchURL} className="text-dark ">
                                        <i className="fa fa-globe"></i> {locationHelper.translate("LANG_SWITCH")}
                                    </a>
                                </li>) : ""
                            }
                            
                            {cityObj ?
                                <li className={langVisibility ? "nav-item px-3 d-none d-lg-block border-left border-dark" : "nav-item px-3 ml-auto d-none d-lg-block"}>
                                    <a href={'tel:' + cityObj.phoneNumber} className="text-dark">
                                        <span className="h5 m-0 d-block ltr">{cityObj.phoneNumber.substr(0, 4) + ' ' + cityObj.phoneNumber.substr(4, 1) + ' ' + cityObj.phoneNumber.substr(5)}</span>
                                        <span className="h6 m-0 d-block">{cityObj.hoursInWeek}</span>
                                    </a>
                                </li> :
                                null}

                            <li id={"profile-header"} className="dropdown py-lg-3 px-3 d-none d-md-block">
                                <a onClick={() => this.showLoginModal()} className={(this.props.signInDetails.access_token ? '' : 'show') + ' h5 font-weight-bold m-0 text-dark collapse'} role="button" href="javascript:void(0)" data-toggle="collapse" data-target="#drawer1">
                                    <strong>{locationHelper.translate("LOGIN")}</strong>
                                </a>
                                <div className={(this.props.signInDetails.access_token ? 'show' : '') + ' collapse'}>
                                    <Link className={"nav-link"} to="/en/profile">{this.props.signInDetails.customer ? this.props.signInDetails.customer.firstName : ''
                                    }</Link>
                                    <ul className="dropdown-menu profile-menu">
                                        <li><Link className={"font-weight-bold"} to={"/"+lang+"/profile#bookings"} rel="nofollow, noindex">{locationHelper.translate("MY_BOOKINGS")}</Link>
                                        </li>
                                        <li><Link className={"font-weight-bold"} to={"/"+lang+"/profile#quotes"}rel="nofollow, noindex">{locationHelper.translate("MY_QUOTES")}</Link>
                                        </li>
                                        <li><Link className={"font-weight-bold"} to={"/"+lang+"/profile#details"} rel="nofollow, noindex">{locationHelper.translate("MY_PROFILE")}</Link></li>
                                        <li><a className={"font-weight-bold"} href="javascript:void(0)" onClick={() => this.logout()} rel="nofollow, noindex">{locationHelper.translate("LOGOUT")}</a></li>
                                    </ul>
                                </div>

                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
        );
    }
}
function mapStateToProps(state) {
    return {
        bodyClass: state.bodyClass,
        showMainMenu: state.showMainMenu,
        showLoginMenu: state.showLoginMenu,
        showSignInForm: state.showSignInForm,
        currentCity: state.currentCity,
        cities: state.cities,
        signInDetails: state.signInDetails,
        lang: state.lang,
        langSwitchVisibility : state.langSwitchVisibility
        //currentCityData: state.currentCityData.length > 0 ? state.currentCityData[0] : null,
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({ toggleMainMenu, toggleLoginModal, toggleSignInForm, setSignIn, setUserProfile }, dispatch);
}

export default withCookies(connect(mapStateToProps, mapDispatchToProps)(Header));
