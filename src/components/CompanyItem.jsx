import React from "react";
import GeneralModal from "./GeneralModal";
import ProviderDetails from "./ProviderDetails";
import StarRating from "./StarRating"
import LazyLoad from 'react-lazyload';
import PlaceholderComponent from '../components/Placeholder';
import commonHelper from "../helpers/commonHelper";

class CompanyItem extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            readMore: false
        };
        this.setModal = this.setModal.bind(this);
        this.readMore = this.readMore.bind(this);
    }
    
    setModal(visibility){
        const {isOffer} = this.props;
        if( !isOffer ){
            this.setState({
                showModal: visibility
            });
        }
    }
    getRating(rating, numberOfReviews){
        var rate = (rating / numberOfReviews).toFixed(1);
        return rate;
    }
    readMore(event){
        this.setState({
            readMore: !this.state.readMore
        });
    }
    lazyLoadCompany(logo) {
        var logo = typeof logo != "undefined" ? logo : "/dist/images/logo_dark.png";
        if(this.props.lazyLoad) {
            return (
                <LazyLoad offset={500} placeholder={<PlaceholderComponent />}>
                    <div className="border rounded placeholder square pointer" onClick={()=> this.setModal(true)} style={{background: "url("+logo+") center/cover no-repeat"}}></div>
                </LazyLoad>
            );
        } else {
            return (
                <div className="border rounded placeholder square pointer" onClick={()=> this.setModal(true)} style={{background: "url("+logo+") center/cover no-repeat"}}></div>
            );
        }

    }
    render() {
        const {item, isChecked, canSelect, cardClass, isOffer} = this.props;
        let logo = isOffer ? item.serviceProviderLogo : item.logo;
        let name = isOffer ? item.serviceName+' - '+item.promoText : item.name;
        let numberOfReviews = item.reviewsSummaryDto.numberOfReviews;
        var newClass = "";

        if(!isOffer) {
            var newClass = item.sponsoredProviderPriority != 1000 ? ' gray-selected' : '';
        }

        var rating = !isNaN(item.reviewsSummaryDto.averageRating ) ? Number(( item.reviewsSummaryDto.averageRating ).toFixed(2) ) : item.reviewsSummaryDto.averageRating;

        return (
            <div className={cardClass+newClass}>
                <div className="row">
                    <div className="col-3 col-md-2 pr-0">
                        <div className="py-3">
                            <div className="company-logo pointer">
                                { isOffer ? this.lazyLoadCompany(logo) : (<img width="150" src={logo} className="border rounded square" />)}
                                { /*this.lazyLoadCompany(logo) */}
                            </div>
                        </div>
                    </div>
                    <div className="col py-3 pt-sm-0 d-flex flex-column">
                        <div className="row company-detail">
                            <div className="col company-name">
                                <span className="h3 pointer m-0 text-black" onClick={()=> this.setModal(true)}>{name}</span>
                                {isOffer ? 
                                    item.dealMovingSizeRateDtoList.map((mSize, index)=> {
                                        return (
                                            <p key={"mSize"+index} className="h5 mt-1 mb-0 text-secondary">{item.serviceName} for {mSize.movingSize.displayName}</p>
                                        )
                                    })
                                 : ''}
                                 {isOffer ? (<p className="h4 mt-1 mb-0">Company: {item.serviceProviderName}</p>) : ''}
                                 {isOffer ? (
                                    <p className="h4 mt-1 mb-0">
                                        <span className="line-through d-inline-block mr-1">AED {item.dealMovingSizeRateDtoList[0].oldPrice}</span>
                                        <span className="text-danger d-inline-block">AED {item.dealMovingSizeRateDtoList[0].dealPrice}</span>
                                    </p>
                                 ) : ''}
                            </div>
                        </div>
                        <div className="row mt-2">
                            <div className="col-12 col-md-4 col-lg-6 pb-1 pb-md-0">
                                <div className="d-lg-flex d-md-flex d-sm-flex flex-sm-column">
                                    <StarRating rating={rating}/>
                                    <p className="mt-2 mr-2 mb-0">
                                        {isOffer ? (
                                            <span>
                                                <span>{(rating && rating != 'NaN')? (rating + '/5') : 'No rating available, '}</span> <span>{numberOfReviews} Reviews</span>
                                            </span>
                                            ) : (
                                        <a href="javascript:void(0)" className="m-0 order-sm-1" onClick={()=> this.setModal(true)}>
                                            <span>{(rating && rating != 'NaN')? (rating + '/5') : 'No rating available, '}</span> <span>{numberOfReviews} Reviews</span>
                                        </a>
                                        )}
                                    </p>
                                </div>
                            </div>
                            {!isOffer ? (
                            <div className="col-auto col-md order-2 col-md">
                                <div className="reviews-details-j2">
                                    { (typeof item.providerClass != "undefined" && item.providerClass !=  null) && (<span className="pts">
                                        <i className="fa fa-shield"></i>
                                        <strong className="text-capitalize d-inline-block ml-1">
                                            {item.providerClass}
                                        </strong>
                                    </span>)
                                    }
                                </div>
                            </div>
                            ) : ''}
                        </div>
                        {isOffer ? (
                        <div className="row mt-2">
                            <div className="col-12">
                                <a href="javascript:void(0)" className="m-0" onClick={(event)=> this.readMore(event)}>
                                    <strong>
                                        Offer details 
                                        <i className="ml-1 fa fa-angle-down"></i>
                                    </strong>
                                </a>
                                <div className={this.state.readMore ? '' : 'd-none'}><p>{item.offerDetails}</p></div>
                            </div>
                        </div>
                        ) : ''}
                    </div>
                    {canSelect ? (
                    <div className="col-2 col-md-1 bg-light d-flex align-items-center justify-content-center">
                        <div className="d-flex flex-column align-items-center">
                            <label>
                                <input className="big-check" type="checkbox" checked={isChecked} onChange={()=> this.props.selectChange(item)} />
                                <span className="big-check-label"></span>
                            </label>
                            <small className="mt-1 text-warning">{isChecked ? 'Selected' : 'Select'}</small>
                        </div>
                    </div>
                    ) : ''}
                </div>
                {
                    item.promotionResponseDto != null && !isOffer &&
                    (<div className="row mobile-promo bg-primary rounded py-2">
                        <div className="col">
                            <div className="promo-bar text-white mr-3">
                                <i className="fa fa-gift"></i>
                                <span
                                    className="promo-body d-inline-block ml-2">{item.promotionResponseDto.promotionTitle}</span>
                            </div>
                        </div>
                    </div>)
                }

                {canSelect ? (
                this.state.showModal && !isOffer && <GeneralModal title="Company detail" modalSize="modal-lg" modalBody={<ProviderDetails providerID={item.id} isChecked={isChecked} selectChange={()=> this.props.selectChange(item)} />} setModal={this.setModal} />
                ) : (
                this.state.showModal && !isOffer && <GeneralModal title="Company detail" modalSize="modal-lg" modalBody={<ProviderDetails providerID={item.id} canSelect={canSelect} />} setModal={this.setModal} />
                )}
            </div>
        )
    }
}

CompanyItem.defaultProps = {
    cardClass: "card container-fluid mb-3 shadow-sm service-provider-container",
    canSelect: true,
    isOffer: false,
    lazyLoad: true
};

export default CompanyItem;
