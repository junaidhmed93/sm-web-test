import React from "react";
import Slider from "react-slick";
import { Link } from "react-router-dom";
import {imgixWPBase, relativeURL} from '../imgix';
import LazyLoad from 'react-lazyload';
import PlaceholderComponent from '../components/Placeholder';
import {isBotData} from "../actions";
import {connect} from "react-redux";

class PopularServices extends React.Component{
    constructor(props) {
		super(props);
		this.state= {
			showMore: false
		};
		this.showMoreCard = this.showMoreCard.bind(this);
    }
	renderItems(){
		// console.log(this.props.isBotData)
		return this.props.items.map((item,index) => {
            return(
				<div key={"popular-service-"+index} className={ ( index < 8 || this.state.showMore ) ? "col-6 col-md-3 col-sm-4 mb-4 px-2" : "col-6 col-md-3 col-sm-4 mb-3 px-2 hidden"}>
					<Link to={"/"+this.props.lang+"/"+this.props.currentCity+"/"+item.url_slug} className="text-sm card p-2" key={"link"+item.ID} title={item.service_preview_title}>
						<div key={item.ID} className="item">
							<LazyLoad scroll={false} height={121} placeholder={<PlaceholderComponent height={121} placeholderSrc={imgixWPBase+relativeURL(item.service_preview_image)} />}>
								<div className="carousel-placeholder placeholder rect-sm bg-light mb-1" style={item.service_preview_image?{background: "url("+imgixWPBase+relativeURL(item.service_preview_image)+"?fit=crop&w=237&h=121&auto=format,compress) center/cover no-repeat"}:{}}></div>
							</LazyLoad>
							<p className="title font-weight-bold mt-2 mb-1 text-center">
								<div  className="btn btn-primary d-block">
									<span className="d-inline-block text-uppercase">
										{item.service_preview_title} <i className="fa fa-long-arrow-right mt-1"></i>
									</span>
								</div>
							</p>
						</div>
					</Link>
				</div>
            );
        });
	}
	showMoreCard(){
		this.setState({
			showMore: true
		})
	}
	renderViewMoreBtn(){
		return(<div className="row mt-3 justify-content-center view-more">
					<div className="col-12 d-flex col-sm-6 justify-content-center col-md-8">
						<div className="text-center d-flex flex-fill flex-column align-items-center justify-content-center rounded">
							<a href="javascript:void(0)" onClick = {() => this.showMoreCard() } className="btn px-4 text-uppercase  border btn-white spread-quotes view-more-btn">
								<span className="text-sm">View more services</span>
							</a>
						</div>
					</div>
				</div>);
	}
	render() {
		var items = this.props.items;
    	return (
        	<div className={'service-slider ' + this.props.parentClass}>
			    <div className={this.props.childClass}>
			        <div className="sm-slider row">
						{this.renderItems()}
			        </div>
					{ ( items.length > 8 && !this.state.showMore ) ? this.renderViewMoreBtn() : ""}
			    </div>
			</div>
        );
    }
}
PopularServices.defaultProps = {
  parentClass: 'row mb-4 md-md-5',
  childClass: 'col px-4 px-md-3 gallery',
  items: [],
  settings: {},
    isBotData: false
};
function mapStateToProps(state){
	return {
		currentCity: state.currentCity,
		lang: state.lang
	}
}
export default connect(mapStateToProps)(PopularServices);
