import React from "react";

class GeneralModal extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            scrollWidth: '0px'
        }
        this.setModel = this.setModel.bind(this);
    }
    componentDidMount(){
        document.body.className += ' modal-open';
        if( window.innerWidth > 767 ){
            this.setState({
                scrollWidth: '15px'
            })
        }
    }
    componentWillUnmount(){
        document.body.className = document.body.className.replace(' modal-open', '');
    }
    setModel(){
        if( typeof this.props.setModal != "undefined" ){
            this.props.setModal(false);
        }
    }
    render() {
        const {modalSize, title, modalBody, modalClass, showHeader, titleClass, headerClass, setModal} = this.props;
        let isSetModel = typeof setModal !="undefined" ? true : false;
        console.log("isSetModel", isSetModel);
        return (
            <div className={"general-modal " + modalClass}>
                <div className="modal show">
                    <div className="modal-backdrop show" onClick={() => this.setModel()} style={{right: this.state.scrollWidth}} ></div>
                    <div className={"modal-dialog "+modalSize} style={{zIndex: '1045'}}>
                        <div className="modal-content">
                            { showHeader ? (<div className={"modal-header d-flex align-items-center "+headerClass}>
                                <h5 className={typeof titleClass != "undefined" ? titleClass : "modal-title"}>{title}</h5>
                                { isSetModel ? (<button type="button" className="close" aria-label="Close" onClick={() => this.setModel()} >
                                    <span aria-hidden="true">×</span>
                                </button>) : ''}
                            </div>) : (<button type="button" className="close" aria-label="Close" onClick={() => this.setModel()} >
                                <span aria-hidden="true">×</span>
                                </button>)}
                            <div className="modal-body p-4 p-md-3">
                                {modalBody}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

GeneralModal.defaultProps = {
    title: '',
    modalSize: '',
    modalBody: '',
    modalClass: '',
    showHeader : true,
    headerClass: ''
};

export default GeneralModal;
