import React from "react";

class SectionTitle extends React.Component{
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className={this.props.parentClass}>
                <div className={this.props.childClass}>
                    <h3 className="font-weight-bold has-line-after">{this.props.title} <span className={'text-capitalize'}>{this.props.titleCapitalized}</span></h3>
                    {this.props.subTitle.length ? (<p>{this.props.subTitle}</p> ) : ''}
                </div>
            </div>
        );
    }
}

SectionTitle.defaultProps = {
  title: '',
  subTitle: '',
  titleCapitalized: '',
  parentClass: 'row',
  childClass: 'col px-4 px-md-3'
};

export default SectionTitle;
