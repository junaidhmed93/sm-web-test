import React from "react";
import Search from "./Search";
import { Link } from "react-router-dom";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {imgixReactBase, quality} from '../imgix';
import LazyLoad from 'react-lazyload';
import PlaceholderComponent from '../components/Placeholder';
import {toggleMainMenu, toggleLoginModal, toggleSignInForm, scrollToTop, DEFAULT_LANG, getLogo} from "../actions/index";
import {setSignIn, setUserProfile} from "../actions";
import {withCookies} from "react-cookie";

class SimpleHeader extends React.Component{
    constructor(props) {
        super(props);
    }
    render() {
        const { currentCity, lang, currentCityData } = this.props;
        let cityObj = currentCityData.length ? currentCityData[0] : null;
        return (
            <header className={this.props.bodyClass} >
                <nav className="navbar navbar-expand-lg navbar-light bg-white py-lg-0 shadow-1">
                    <div className="d-flex align-items-center justify-content-between">
                        <Link to={"/"+lang+"/"+currentCity} className="navbar-brand px-3 py-3 mr-0">
                            <object id={"main-logo-object"} data={getLogo(lang)} height={'32px'} alt="ServiceMarket" type="image/svg+xml"></object>
                    	</Link>
                    </div>
                    <div className="d-flex d-md-none align-items-center justify-content-between">
                    { /*cityObj ? 
                        (<a href={'tel:' + cityObj.phoneNumber} className="text-dark">
                            <span className="h5 m-0 d-block">{cityObj.phoneNumber.substr(0, 4) + ' ' + cityObj.phoneNumber.substr(4, 1) + ' ' + cityObj.phoneNumber.substr(5)}</span>
                            <span className="h6 m-0 d-block">{cityObj.hoursInWeek}</span>
                        </a>) : null */
                     }
                        <a href="tel:0097144229639" className="text-dark">
                            <span className="h5 m-0 d-block ltr">+971 4 4229639</span>
                            <span className="h6 m-0 d-block">Sat - Thu: 8am - 8pm Fri: 9am - 6pm</span>
                        </a>
                    </div>
                    <button className="navbar-toggler border-0 d-none" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav align-items-lg-center justify-content-between flex-fill">
                            <li className="nav-item px-3 ml-auto d-none d-lg-block">
                                <a href="tel:0097144229639" className="text-dark">
                                    <span className="h5 m-0 d-block ltr">+971 4 4229639</span>
                                    <span className="h6 m-0 d-block">Sat - Thu: 8am - 8pm Fri: 9am - 6pm</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
        );
    }
}
function mapStateToProps(state){
	return {
        bodyClass: state.bodyClass,
        currentCity: state.currentCity,
        currentCityData: state.currentCityData,
        lang:state.lang
	}
}
export default withCookies(connect(mapStateToProps)(SimpleHeader));