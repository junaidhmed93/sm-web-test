import React from "react";
import {imgixReactBase, imgixWPBase, quality, relativeURL} from '../imgix';
import LazyLoad from 'react-lazyload';
import PlaceholderComponent from '../components/Placeholder';
import locationHelper from '../helpers/locationHelper';
import LazyLoadImage from "./LazyLoadImage";
import {connect} from 'react-redux';
import { DEFAULT_LANG, LANG_AR } from "../actions";
import moment from 'moment';

class AllGoogleReviews extends React.Component{
    constructor(props) {
        super(props);
        this.locationHelperInit = locationHelper.init.bind(this);
        this.locationHelperInit();
    }

    render() {
        let {lang} = this.props;
        return (
          <div className={this.props.parentClass}>
            <div className={this.props.childClass}>
                <div className="card">
                    <div className="card-body py-0">
                        <div className="row">
                            <div className="col-12 col-md-2 d-flex align-items-center justify-content-center p-4">
                                <LazyLoad height={70}  offset={500} placeholder={<PlaceholderComponent placeholderSrc={imgixReactBase + "/dist/images/google-reviews.png"} isImage={true} height="48" className="mh-100" />}>
                                    <img src={imgixReactBase + "/dist/images/google-reviews.png?w=140&h=70&auto=format,compress"} className="mh-100" height="48" alt="" />
                                </LazyLoad>
                            </div>
                            <div className="col-auto border-right p-0 d-none d-md-block"></div>
                            <div className="col-12 border-bottom d-md-none"></div>
                            <div className="col-12 d-flex align-items-center col-md py-3">
                                <div className="row flex-fill align-items-center justify-content-between text-center text-md-left">
                                    <div className="col-12 col-md-auto">
                                        <h3 className="h4 font-weight-bold">{ locationHelper.translate("WHAT_DO_CUST_SAY_ABOUT") }</h3>
                                        <p className="mtm" itemScope="true" itemType="http://schema.org/Product">
                                            <span itemProp="name" content={this.props.product_name}>
                                                <span itemProp="description"> {locationHelper.translate("SERVICEMARKET_HAS_BEEN_RATED")}
                                                    <span itemProp="aggregateRating" itemScope="true" itemType="http://schema.org/AggregateRating">
                                                        <span itemProp="ratingValue"> 4.3
                                                        </span> {locationHelper.translate("OUT_OF")}
                                                        <span itemProp="bestRating"> 5</span> {locationHelper.translate("BASED_ON")}
                                                        <span itemProp="ratingCount"> 705 </span>
                                                    </span>
                                                    {locationHelper.translate("REVIEWS_AS_OF")} {moment().format('MMMM YYYY')}.
                                                </span>
                                            </span>
                                        </p>
                                    </div>
                                    <div className="col-12 col-md-auto">
                                        <a target={"_blank"} className="text-dark font-weight-bold" href="https://www.google.ae/maps/place/ServiceMarket/@25.0734718,55.1315495,14z/data=!4m5!3m4!1s0x3e5f6ca6c3a55555:0xaf640ee17dbd5d6!8m2!3d25.073987!4d55.143331">{locationHelper.translate("READ_ALL_CUST_REVIEW")}
                                            <i className={lang == LANG_AR ? "ml-1 fa fa-arrow-left" : "ml-1 fa fa-arrow-right"}></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        );
    }
}
function mapStateToProps(state){
	return {
        currentCity: state.currentCity,
        lang: state.lang
	}
}
AllGoogleReviews.defaultProps = {
  parentClass: 'row mb-4 md-md-5',
  childClass: 'col px-4 px-md-3',
    isBotData: false,
    product_name: 'Home Services'
};
export default connect(mapStateToProps)(AllGoogleReviews);
