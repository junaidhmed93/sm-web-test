import React from "react";
import Slider from "react-slick";
import { Link } from "react-router-dom";
import {imgixWPBase, relativeURL} from '../imgix';
import LazyLoad from 'react-lazyload';
import PlaceholderComponent from '../components/Placeholder';
import {isBotData, LANG_AR} from "../actions";
import { withCookies } from "react-cookie";
import {connect} from "react-redux";

class ContentSlider extends React.Component{
    constructor(props) {
        super(props);
    }
	renderItems(){
    	// console.log(this.props.isBotData)
		return this.props.items.map((item) => {
            return(
                <Link to={"/"+this.props.lang+"/"+this.props.currentCity+"/"+item.url_slug} className="text-sm" key={"link"+item.ID}>
					<div key={item.ID} className="item">
						<LazyLoad scroll={false} height={121} placeholder={<PlaceholderComponent height={121} placeholderSrc={imgixWPBase+relativeURL(item.service_preview_image)} />}>
							<div className="carousel-placeholder placeholder rect-sm border bg-light rounded mb-1" style={item.service_preview_image?{background: "url("+imgixWPBase+relativeURL(item.service_preview_image)+"?fit=crop&w=237&h=121&auto=format,compress) center/cover no-repeat"}:{}}></div>
						</LazyLoad>
						<p className="title font-weight-bold mt-0 mb-1">{item.service_preview_title}</p>
						{/*<h6>{item.service_preview_description}</h6>*/}
					</div>
				</Link>
            );
        });
	}
	mergeSettings(){
		let {lang} = this.props;
		var defaultSettings = { dots: false, infinite: true, speed: 500, slidesToShow: 4, slidesToScroll: 1, infinite: false, responsive: [ { breakpoint: 1199, settings: { slidesToShow: 3, arrows: false } }, { breakpoint: 767, settings: { unslick: true } } ] };
		/*if( lang == LANG_AR){
			defaultSettings['rtl'] = true;
		}*/
		return {...defaultSettings, ...this.props.settings};
	}
    render() {
    	var settings = this.mergeSettings();
        return (
        	<div className={'service-slider ' + this.props.parentClass}>
			    <div className={this.props.childClass}>
			        <div className="sm-slider">
						<Slider {...settings}>
							{this.renderItems()}
						</Slider>
			        </div>
			    </div>
			</div>
        );
    }
}
ContentSlider.defaultProps = {
  parentClass: 'row mb-4 md-md-5',
  childClass: 'col px-4 px-md-3 gallery',
  items: [],
  settings: {},
    isBotData: false
};
function mapStateToProps(state){
	return {
        bodyClass: state.bodyClass,
        currentCity: state.currentCity,
        lang:state.lang
	}
}
//export default withCookies(connect(mapStateToProps)(SimpleHeader));
export default withCookies(connect(mapStateToProps)(ContentSlider)) ;
