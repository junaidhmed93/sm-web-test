import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import CompanyItem from "./CompanyItem";
import GeneralModal from "./GeneralModal";
import Loader from "./Loader";
import commonHelper from "../helpers/commonHelper";
import { fetchProvidersByService, getCityCode } from "../actions/index";
import CheckRadioBoxInput from "./Form_Fields/CheckRadioBoxInput";
import {
    fetchAllCities,
    fetchServices,
    hideLoader,
    setCurrentCity,
    setJourneySubServiceData,
    showLoader,
    URLCONSTANT
} from "../actions";

class SelectCompanies extends React.Component {
    _isMounted = false;

    constructor(props) {
        super(props);
        this.state = {
            providers: [],
            filteredProviders: [],
            noResults: false,
            category_filter: 'all',
            service_filter: 'all',
            rating: 'all',
            promotions: 'all',
            showFiltersModal: false,
            loadingProviders: false,
            showMoreFilters: true
        }

        this.renderResults = this.renderResults.bind(this);
        this.setFilter = this.setFilter.bind(this);
        this.setService = this.setService.bind(this);
        this.setModal = this.setModal.bind(this);
        this.setSelected = this.setSelected.bind(this);
        this.renderCuisineFilter = this.renderCuisineFilter.bind(this);
        this.cuisineFilterChange = this.cuisineFilterChange.bind(this);
        this.toggleShowMoreFilters = this.toggleShowMoreFilters.bind(this);
        this.setCity = this.setCity.bind(this);
        this.toggleMenu = this.toggleMenu.bind(this);
        this.containerClicked = this.containerClicked.bind(this);


    }
    componentDidUpdate(prevProps) {
        /*console.log("component Did Update");
        console.log(prevProps);
        console.log(this.props);*/
        const prevCity = prevProps.currentCity;
        const currentCity = this.props.currentCity;
        if ((prevCity != currentCity)) {
            const { cities, serviceID, filterService } = this.props;

            const cityCode = getCityCode(currentCity, cities);

            this.setState({
                loadingProviders: true
            });

            fetchProvidersByService(cityCode, serviceID).then((res) => {
                var service_filter = 'all';

                if (filterService != 0) {
                    service_filter = filterService;
                }

                this.setState({
                    providers: res,
                    filteredProviders: res,
                    service_filter: service_filter,
                    loadingProviders: false,
                    noResults: res.length == 0
                })
            });
        }
    }
    componentDidMount() {
        this._isMounted = true;
        const { cities, currentCity, serviceID, filterService } = this.props;

        const cityCode = getCityCode(currentCity, cities);

        this.setState({
            loadingProviders: true
        });

        fetchProvidersByService(cityCode, serviceID).then((res) => {
            if (this._isMounted) {
                var service_filter = 'all';

                if (filterService != 0) {
                    service_filter = filterService;
                }

                this.setState({
                    providers: res,
                    filteredProviders: res,
                    service_filter: service_filter,
                    loadingProviders: false
                })
            }
        });
    }

    componentWillReceiveProps(newProps) {
        if (newProps.filterService !== this.props.filterService) {
            this.setState({
                service_filter: newProps.filterService
            })

        }
        if (newProps.cuisineFilterValue !== this.props.cuisineFilterValue) {
            console.log(" calling Here ");
            this.setFilter({ target: { name: 'cuisine', value: newProps.cuisineFilterValue } })
        }
    }
    setModal(visibility) {
        this.setState({
            showFiltersModal: visibility
        });
    }
    setSelected(name, item) {

        var selected = false;
        /* Default selecion of All is removed as Stefan raised out in Ticket #375 */
        /*if( name == "service_filter"){
            if( this.state.service_filter != "all" && (this.state.service_filter == item.value)){
                selected = "selected";
            }
        }*/

        if ((name == "category_filter" || name == 'rating' || name == 'promotions' || name == "service_filter")) {
            if (item.label.indexOf('Filter') > -1) {
                selected = "selected";
            }
        }
        return selected;
    }
    setMandatorySelected(name, item, index) {
        // console.log('this.props.journey3SubserviceDataReducer');
        // console.log(this.props.journey3SubserviceDataReducer);
        // console.log(name == 'service_filter');
        // console.log(item);
        if (name == 'all_services_filter' && item.id == this.props.serviceID) {
            return "selected";
        }
        if (name == 'service_filter' && this.props.journey3SubserviceDataReducer && item.id == this.props.journey3SubserviceDataReducer.id) {
            return "selected";
        }


    }

    cuisineFilterChange(name, value) {
        this.props.cuisineFilterChange(name, value);
        this.setFilter({ target: { name: 'cuisine', value: value } })
    }
    renderFilters() {
        var filterName = false;
        return this.props.filters.map((item, index1) => {
            if (item.options.length) {
                filterName = item.name;
                return (
                    <div key={'filter' + index1} className="col-6 col-md">
                        <select name={item.name} className="form-control mb-3" onChange={this.setFilter} >
                            {item.options.map((item, index2) => {
                                return (
                                    <option key={'op' + index1 + '' + index2} value={item.value}
                                        disabled={item.disabled} selected={this.setSelected(filterName, item)}>{item.label}</option>
                                )
                            })}
                        </select>
                    </div>
                )
            }
        });
    }
    renderServiceReviewFilters() {
        if (this.state.showMoreFilters) {
            var filterName = false;
            return this.props.filters.map((item, index1) => {
                if (item.options.length) {
                    filterName = item.name;
                    if (filterName != 'service_filter') {
                        return (
                            <div key={'filter' + index1} className="d-none d-md-block col-6 col-md">
                                <select name={item.name} className="form-control mb-3" onChange={this.setFilter} >
                                    {item.options.map((item, index2) => {
                                        return (
                                            <option key={'op' + index1 + '' + index2} value={item.value}
                                                disabled={item.disabled} selected={this.setSelected(filterName, item)}>{item.label}</option>
                                        )
                                    })}
                                </select>
                            </div>
                        )

                    }
                }
            });
        }
    }
    renderServiceReviewMandatoryFilters() {
        var filterName = false;
        return this.props.mandatoryFilters.map((item, index1) => {
            if (item.options.length) {
                filterName = item.name;
                return (
                    <div key={'filter' + index1} className="col-6 col-md">
                        <select name={item.name} className="form-control mb-3" onChange={this.setService} >
                            {item.options.map((item, index2) => {
                                return (
                                    <option key={'op' + index1 + '' + index2} value={item.value}
                                        disabled={item.disabled} selected={this.setMandatorySelected(filterName, item, index2)}>{item.label}</option>
                                )
                            })}
                        </select>
                    </div>
                )
            }
        });
    }
    renderMobileFilters() {
        console.log('this.props.filters');
        console.log(this.props.filters);
        const filterElements = this.props.filters.map((item, index1) => {
            if(item.options.length) {
                return (
                    <div key={'mobfilter' + index1} className="col-6 col-md px-2">
                        <select name={item.name} className="form-control mb-3" onChange={this.setFilter}>
                            {item.options.map((item, index2) => {
                                return (
                                    <option key={'op' + index1 + '' + index2} value={item.value}
                                            disabled={item.disabled} selected={item.disabled}>{item.label}</option>
                                )
                            })}
                        </select>
                    </div>
                )
            }
        });
        const modalBody = (
            <div className="row">
                {filterElements}
                <div className="col-12 px-2">
                    <button type="button" className="btn btn-lg btn-block btn-primary text-uppercase" onClick={() => this.setModal(false)}>Apply Filters</button>
                </div>
            </div>
        );
        return modalBody;
    }
    setService(event) {
        // console.log('event');
        if (event.target.name == 'all_services_filter') {
            const { currentCity, cities, serviceID, filterService } = this.props;
            const newServiceID = event.target.value;
            const cityCode = getCityCode(currentCity, cities);
            // console.log('set Service');
            this.props.history.push("/" + this.props.match.params.lang + "/" + currentCity + "/" + commonHelper.getServiceSlugByParentID(URLCONSTANT, this.props.service_constants, parseInt(event.target.value)) + "/reviews");
            this.setState({
                loadingProviders: true
            });
            fetchProvidersByService(cityCode, event.target.value).then((res) => {
                var service_filter = 'all';

                if (filterService != 0) {
                    service_filter = filterService;
                }

                this.setState({
                    providers: res,
                    filteredProviders: res,
                    service_filter: service_filter,
                    loadingProviders: false,
                    noResults: res.length == 0
                })
                this.props.setService(newServiceID);
            });
        } else {

            var index = event.nativeEvent.target.selectedIndex;
            var newSubServiceID = event.target.value;
            var newSubServiceName = event.nativeEvent.target[index].text;
            this.props.setJourneySubServiceData({ 'id': newSubServiceID, 'value': newSubServiceID, 'name': newSubServiceName });
            this.setFilter(event);
        }
    }
    toggleShowMoreFilters() {

        this.setState({
            showMoreFilters: !this.state.showMoreFilters
        })
    }
    setFilter(event) {
        let { providers, category_filter, service_filter, rating, promotions } = this.state;

        const filterName = event.target.name;
        const filterVal = event.target.value;
        //provider = this.state.filteredProviders;
        this.setState({
            filteredProviders: [],
            [filterName]: filterVal
        });
        // Get the current value of filters as setState is async method
        if (filterName == 'category_filter') { category_filter = filterVal }
        if (filterName == 'service_filter') { service_filter = filterVal }
        if (filterName == 'rating') { rating = filterVal }
        if (filterName == 'promotions') { promotions = filterVal }

        //console.log("SelectCompanies providers", providers);

        var { cuisineFilterValue, showCuisineFilter } = this.props;

        if (showCuisineFilter && cuisineFilterValue.length) {
            var cuisineFilter = [];
            if (cuisineFilterValue.length) {
                cuisineFilterValue.map((item) => {
                    cuisineFilter.push(item.id);
                })
            }
            //console.log("cuisineFilter", cuisineFilter);

            providers.filter((provider) => provider.serviceConstants.filter(cuisine => cuisineFilter.includes(cuisine.id)).length > 0);

            //console.log("cuisineFilter 123", providers);
        }

        if (category_filter != 'all') {
            providers = providers.filter((provider) => provider.providerClass == category_filter);
        }
        if (service_filter != 'all') {
            providers = providers.filter((provider) => provider.offeredServices.filter(sub => sub.id == service_filter).length > 0);
        }
        if (rating != 'all') {
            providers = providers.filter((provider) => (provider.reviewsSummaryDto.rating / provider.reviewsSummaryDto.numberOfReviews) >= parseInt(rating) - 1 && (provider.reviewsSummaryDto.rating / provider.reviewsSummaryDto.numberOfReviews) <= parseInt(rating));
        }
        if (promotions != 'all') {
            if (promotions == 'yes') {
                providers = providers.filter((provider) => provider.promotionResponseDto != null);
            } else {
                providers = providers.filter((provider) => provider.promotionResponseDto == null);
            }
        }
        //console.log(providers);
        this.setState({
            filteredProviders: providers,
            noResults: providers.length == 0
        });
        
        if (event.target.name == 'service_filter') {
            {   var index = event.nativeEvent.target.selectedIndex;
                var newSubServiceID = event.target.value;
                var newSubServiceName = event.nativeEvent.target[index].text;
                this.props.setJourneySubServiceData({ 'id': newSubServiceID, 'value': newSubServiceID, 'name': newSubServiceName });
                //this.setFilter(event);
                
            }

        }
    }
    renderResults() {
        const { selectedCompanies } = this.props;
        var { filteredProviders } = this.state;
        let count = 1;

        var cardClass = "card container-fluid mb-3 shadow-sm service-provider-container";

        return filteredProviders.map((item, index) => {
            const cheched = selectedCompanies.filter(company => company.id == item.id).length ? true : false;
            //console.log("cheched", cheched);
            //index > 10 ? cardClass+' d-none' : cardClass
            return (
                <CompanyItem key={item.id} item={item} cardClass={cardClass} selectChange={this.props.selectChange} isChecked={cheched} />
            )
        });
    }
    renderCuisineFilter() {
        if (this.props.showCuisineFilter) {
            var cateringCuisineConstant = commonHelper.getNewDataValues("cateringCuisineConstant");
            return (<div className="col-12 cuisine-filter">
                <CheckRadioBoxInput
                    InputType="checkbox"
                    name="cuisine"
                    inputValue={this.props.cuisineFilterValue}
                    items={cateringCuisineConstant}
                    parentClass="mb-1"
                    childClass="d-inline-block mr-1"
                    onInputChange={this.cuisineFilterChange}
                />
            </div>);
        }
    }
    componentWillReceiveProps(newProps) {
        /*if ( newProps.selectedCompanies !== this.props.selectedCompanies ) {
            console.log("componentWillReceiveProps");
        }*/
    }
    renderCities(){
        return this.props.cities.map((item, index) => {
            return(
                <option key={'op' + index} value={index} selected={commonHelper.slugify(item.name)==this.props.currentCity} disabled={item.disabled}>{item.name}</option>
            );
        });

    }
    setCity(event){
        var city = this.props.cities[event.target.value];
        let lang = this.props;
        // console.log(city);
        this.props.showLoader();
        const current = city.name.toLocaleLowerCase().split(" ").join("-");
        this.props.setCurrentCity(current);
        this.props.fetchServices(current, lang).then(()=> this.props.hideLoader());
        switch(this.props.bodyClass){
            case 'home':
                this.props.history.push("/"+this.props.match.params.lang+"/"+current);
                break;
            case 'landing':
                this.props.history.push("/"+this.props.match.params.lang+"/"+current+"/"+this.props.match.params.slug);
                break;
            case 'companies':
                this.props.history.push("/en"+"/"+current+"/partners-list");
                break;
            case 'service_reviews':
                this.props.history.push("/"+this.props.match.params.lang+"/"+current+"/"+this.props.match.params.slug+"/reviews");
                break;
        }
        this.setState({
            showCities: false
        });
    }
    toggleMenu(){
        this.setState({
            showCities: !this.state.showCities
        })
    }
    componentWillUnmount() {
        this._isMounted = false;
        document.removeEventListener('click', this.handleOutsideClick, false);
    }
    containerClicked(e){
        if( !this.state.showCities ){
            document.addEventListener('click', this.handleOutsideClick, false);
        }else{
            document.removeEventListener('click', this.handleOutsideClick, false);
        }
    }
    handleOutsideClick(e){
        if( this.node.contains(e.target) ){
            return
        }
        // To ensure that one Select component is listen to the event
        document.removeEventListener('click', this.handleOutsideClick, false);
        this.setState({
            showCities: false
        });
    }

    render() {
        const { providers, filteredProviders, noResults, showFiltersModal } = this.state;
        // console.log(filteredProviders.length);
        // console.log(noResults);
        // console.log( (!filteredProviders.length && !noResults ) + " ");

        // console.log('render');
        // console.log(this.props.subServiceID);
        return (
            <div>
                <section>
                    <div className={"row d-md-flex " + (this.props.showServiceReviewsFilters?'':'d-none')}>
                        {this.props.showServiceReviewsFilters ? (
                            <div id={"journey2-services"} className={"col-12"}>
                                <div className="row">
                                    <div key={'filter'} className="col-6 col-md">
                                        <select name={'city'} className="form-control mb-3" onChange={this.setCity} >
                                            {this.renderCities()}
                                        </select>
                                    </div>
                                    {this.renderServiceReviewMandatoryFilters()}
                                </div>
                            </div>
                        ) : ''}
                        {this.props.showServiceReviewsFilters ? this.renderServiceReviewFilters() : this.renderFilters()}
                    </div>
                    <div className="row d-md-flex">
                        {(!this.props.showServiceReviewsFilters || this.state.showMoreFilters) ? this.renderCuisineFilter() : ''}
                    </div>
                    <div className="row d-md-none">
                        <div className="col text-center">
                            <a className="d-block mb-3 text-secondary" href="javascript:void(0)" onClick={() => this.setModal(true)}>
                                <i className="fa fa-sliders mr-1"></i>
                                <span>Click here to apply filters</span>
                            </a>
                        </div>
                    </div>
                    {showFiltersModal && <GeneralModal title="Apply filters" modalBody={this.renderMobileFilters()} setModal={this.setModal} />}
                </section>
                {(typeof providers != "undefined" && providers.length && !this.state.loadingProviders) ? (
                    <section>
                        {filteredProviders.length ? (<div id="change-companies-ajax" className="pb-3 mb-3">{this.renderResults()}</div>) : ''}
                        {!filteredProviders.length && !noResults ? <Loader /> : ''}
                        {noResults ? (<p>Uh-oh! We can't find a company that matches your selection. Please try again with fewer filters.</p>) : ''}
                    </section>
                ) : (
                            <section>
                            {
                                !this.state.loadingProviders?(<p className="text-center">No reviews are available for the selected service.</p>):(<Loader />)
                            }
                            </section>
                            )}
            </div>
        );
    }
}
/*
 {filteredProviders.length > 10 && (<div className="col d-none text-center my-3">
 <div className="btn btn-inline-block border border-black text-uppercase" id="provider-load-more" onClick={}>Load More</div>
 </div>)}
 */
SelectCompanies.defaultProps = {
    serviceID: 0,
    filters: [],
    currentCity: '',
    showCuisineFilter: false,
    showServiceReviewsFilters: false,
    mandatoryFilters: []
};

function mapStateToProps(state) {
    return {
        cities: state.cities,
        service_constants: state.serviceConstants,
        journey3SubserviceDataReducer: state.journey3SubserviceDataReducer,
        lang:state.lang
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ setJourneySubServiceData,setCurrentCity, fetchAllCities, fetchServices,showLoader, hideLoader }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SelectCompanies);
