import React from "react";


class ContentTooltip extends React.Component{
    constructor(props) {
        super(props);
    }
    createMarkup(htmlString) {
      return {__html: htmlString};
    }
    render() {
        var {customClass, title, text} = this.props;
        return (
            <div className={"col-12 col-md-6 px-5 px-md-0 pr-md-0 stack-bottom "+customClass}>
                <div className="bg-light rounded border p-5">
                    <div className="row">
                        <div className="col-12">
                            <h4 className="mb-4 font-weight-bold">{title}</h4>
                        </div>
                        <div className="col-12" dangerouslySetInnerHTML={this.createMarkup(text)}>

                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

ContentTooltip.defaultProps = {
    title: '',
    text: '',
    customClass: ''
}
export default ContentTooltip;
