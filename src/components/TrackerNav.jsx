import React from "react";
import ReactDOM from "react-dom";
import { isMobile, LANG_AR } from "../actions";
import commonHelper from "../helpers/commonHelper";
import locationHelper from "../helpers/locationHelper";
import {connect} from "react-redux";
class TrackerNav extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            Steps : props.Steps,
            currentStep: props.formCurrentStep
        }
        this.setClass = this.setClass.bind(this);
        this.trackerItems = this.trackerItems.bind(this);
        this.mobileList = this.mobileList.bind(this);
    }
    step(step,i){
        var activeClass = this.setClass(i);

        var title = commonHelper.jsUcfirst(step.title); //commonHelper.toTitleCase(step.title);

        return (<li className={ "step col-auto text-center "+activeClass} key={step.id} data-identifier={step.id}>
                <span className="step-icon"></span>
                <p className="step-label h6 m-0 text-nowrap">{title}</p>
            </li>
        );
    };
    stepLine(i = 0){
        return (<li key={"col-line-"+i} className="step-lines col"></li>);
    };
    setClass(index){
        var currentStep = this.props.formCurrentStep;
        if( currentStep == index ){
            return 'active';
        }
        if( currentStep > index ){
            return 'complete';
        }
    }
    setSteps(Steps){
        this.setState({Steps: Steps});
    }
    trackerItems(){
        var stepLine = "";//  this.stepLine();
        var Steps = this.state.Steps;
        var StepList = [];
        var i = 1;
        Steps.forEach((step)=>{
            stepLine =  this.stepLine(i);
            var li = "";
            li = this.step( step, i);
            StepList.push(li);
            if(Steps.length != i){
                StepList.push(stepLine);
            };
            i++;
        })
        return StepList;
    }
    mobileList(){
        var stepLine = ""; //this.stepLine();
        var Steps = this.state.Steps;
        var StepList = [];
        var i = 1;
        Steps.forEach((step)=>{
            var li = "";
            stepLine =  this.stepLine(i);
            li = this.step( step, i);
            if( i != 2 ){
                StepList.push(li);
            }
            if(Steps.length != i && i != 2){
                StepList.push(stepLine);
            };
            i++;
        })
        return StepList;
    }
    render() {
        var { typeOfFlow, typeOfJourney, lang} = this.props;
        
        var StepList = ( ( typeOfFlow == 'quotes' && typeOfJourney == 1 ) && (isMobile() || lang == LANG_AR ) ) ? this.mobileList() : this.trackerItems();
        var i = 1;
        return (
            <nav className="steps bg-white pb-4 pt-3 border-bottom sticky hide-on-focus">
                <div className="container">
                    <ul className="steps px-4 list-unstyled m-0 d-flex align-items-center justify-content-between">
                        { StepList }
                    </ul>
                </div>
            </nav>
        );
    }
}
TrackerNav.defaultProps = {
    typeOfFlow: 'booking',
    typeOfJourney : 1
};
function mapStateToProps(state){
    return {
        currentCity: state.currentCity,
        lang: state.lang
    }
}
export default connect(mapStateToProps)(TrackerNav);
