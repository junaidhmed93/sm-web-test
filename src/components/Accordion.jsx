import React from "react";
import ReactDOM from "react-dom";
import Panel from "./Panel";
import {connect} from "react-redux";
class Accordion extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            activeTab: -1
        };
        this.renderList = this.renderList.bind(this);
        this.activateTab = this.activateTab.bind(this);
    }
    renderList(){
        return this.props.panels.map((panel, index) => {
            return(
                <Panel key={'panel'+index} index={index} title={panel.title} activeTab={this.state.activeTab} panelClicked={this.activateTab} simplePanel={this.props.simplePanel} >
                    {panel.description}
                </Panel>
            );
        });
    }
    activateTab(index) {
        this.setState(prev => ({
            activeTab: index
        }));
    }

    render() {
        return (
            <div className={"accordion "+this.props.parentClass}>
                <div className={this.props.childClass}>
                    {this.renderList()}
                </div>
            </div>
        );
    }
}
Accordion.defaultProps = {
    panels : [],
    parentClass: '',
    childClass: '',
    simplePanel: false
}

function mapStateToProps(state){
    return {
        currentCity: state.currentCity,
        lang: state.lang
    }
}
export default connect(mapStateToProps)(Accordion);
