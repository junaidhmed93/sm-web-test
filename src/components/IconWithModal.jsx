import React from "react";
import {imgixWPBase, relativeURL} from "../imgix";
import LazyLoad from 'react-lazyload';
import GeneralModal from "./GeneralModal";
import PlaceholderComponent from '../components/Placeholder';

class IconWithModal extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            show: false
        };
        this.setModal = this.setModal.bind(this);
    }
    setModal(boolVal){
        // console.log(boolVal);
        // this.setState({
        //     show: boolVal
        // });
    }
    render() {
        const {item} = this.props;
        return (
            <div className="col-6 col-md-3 text-center mb-3 pointer">
                <div onClick={()=> this.setModal(true)}>
                    {item.image ?
                        <LazyLoad offset={500} height={64} placeholder={<PlaceholderComponent />}><img width={64} height={64} src={imgixWPBase+relativeURL(item.image)+"?fit=crop&h=64&auto=format,compress"}/></LazyLoad>
                        :
                        <i className="fa fa-4x fa-google-wallet text-warning"></i>
                    }
                    <p>{item.name}</p>
                </div>
                {this.state.show && <GeneralModal title={item.name} modalBody={item.description} setModal={this.setModal} />}
            </div>
        );
    }
}

export default IconWithModal;
