import React from "react";

class FormTitleDescription extends React.Component{
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className={this.props.parentClass}>
                <div className={this.props.childClass}>
                    <h3 className="m-0 font-weight-normal">{this.props.title}</h3>
                    <p className="text-secondary mt-1">{this.props.desc}</p>
                </div>
            </div>
        );
    }
}

FormTitleDescription.defaultProps = {
  title: '',
  desc: '',
  parentClass: 'row',
  childClass: 'col'
};

export default FormTitleDescription;
