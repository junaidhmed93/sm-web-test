import React from 'react';
import Loader from "./Loader";
import {imgixWPBase, relativeURL} from "../imgix";

export default function Placeholder(props) {
    var styleCss = {};
    if(props.style) {
        styleCss = props.style;
    }
    styleCss.width = props.width;
    styleCss.height = props.height;
    if (props.isImage) {
        return (
            <img style={styleCss} className={'placeholder ' + props.className} src={props.placeholderSrc + "?fit=crop&w=40&auto=format,compress&q=0&lossless=0"} />
        );
    }
    return (
        <div className="placeholder" style={props.placeholderSrc?{height:props.height,background: "url("+props.placeholderSrc+"?fit=crop&w=40&auto=format,compress&q=0&lossless=0) center/cover no-repeat"}:{}}>
            {!props.placeholderSrc?
                (
                    <div className="spinner">
                        <Loader/>
                    </div>
                ):
            ''}

        </div>
    );
}