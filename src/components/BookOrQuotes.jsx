import React from "react";
import {Link, Redirect} from "react-router-dom";
import { withCookies } from "react-cookie";
import {connect} from "react-redux";
import locationHelper from "../helpers/locationHelper";
import { URLCONSTANT, UAE_ID, QATAR_ID } from "../actions";
import {imgixReactBase} from '../imgix';
class BookOrQuotes extends React.Component{
    constructor(props) {
        super(props);
        this.getBooknowLink = this.getBooknowLink.bind(this);
        this.locationHelperInit = locationHelper.init.bind(this);
        this.locationHelperInit();
    }
    setQuotesServiceId(serviceId){
        if(serviceId != ""){
            const { cookies } = this.props;
            cookies.set('landingPageServiceId', serviceId, {path: '/', maxAge:300});
        }
    }
    getBooknowLink(currentCity, bookNowJourney, stickerLink = false){
        let {promo,lang} = this.props;
        let bookNowJourneyURL = "/"+lang+"/"+currentCity+"/"+bookNowJourney+"/book-online";
        promo = (stickerLink && promo == "") ? "WELCOME20" : promo;
        bookNowJourneyURL += promo != "" ? "?promo="+promo : "";
        return bookNowJourneyURL;
    }
    render() {
        var {leftBoxTitle, leftBoxDesc, rightBoxTitle, rightBoxDesc, bookNowJourney, getQuoteJourney, currentCity, serviceId, promo, lang} = this.props;
        const isSingleButtonService = (rightBoxTitle.length && getQuoteJourney != '-- Select if any --') && (leftBoxTitle.length && bookNowJourney != '-- Select if any --') ? false : true;
        let arrowClass = locationHelper.translate("ARROW_CLASS");
        let { h1Text,description, title, match, currentCityData} = this.props;
        let {city,slug} =  match.params;
        let isServiceLanding = ( slug == URLCONSTANT.CLEANING_MAID_PAGE_URI && (currentCityData.length && currentCityData[0].cityDto.countryDto.id == UAE_ID || currentCityData[0].cityDto.countryDto.id == QATAR_ID));
        return (
            <div className="row mt-4 justify-content-center">
                {(leftBoxTitle.length && bookNowJourney != '-- Select if any --') ? (
                <div className={"col-12 d-flex col-sm-6 "+( rightBoxTitle.length ? 'col-md-5' : 'justify-content-center col-md-8' )} >
                    <div style={(rightBoxTitle.length && getQuoteJourney != '-- Select if any --') ?{backgroundColor: "rgba(255,255,255,0.95)"}:{}} className={isSingleButtonService?'' :'p-4 '+" text-center d-flex flex-fill flex-column align-items-center justify-content-center rounded mb-4 mb-md-0"}>
                        {
                            (rightBoxTitle.length && getQuoteJourney != '-- Select if any --') ? (
                                <div>
                                    <h3 className="mt-0 font-weight-bold">{leftBoxTitle}</h3>
                                    <h2 className="p">{leftBoxDesc}</h2>
                                </div>
                            ) : ''
                        }
                        {
                            (serviceId === 'SERVICE_HOME_INSURANCE' || serviceId === 'SERVICE_CAR_INSURANCE') ?
                            (<a href={"/"+bookNowJourney} className={"btn btn-warning font-weight-bold px-4 text-uppercase btn-warning "}>Book Now</a>)
                            :

                            (<Link onClick={ () => this.setQuotesServiceId(serviceId) } to={this.getBooknowLink(currentCity, bookNowJourney)} className={"btn btn-warning font-weight-bold px-4 text-uppercase btn-warning "+ (isSingleButtonService ? ' cta-btn ' : '') + ((rightBoxTitle.length && getQuoteJourney != '-- Select if any --')?"":"spread-quotes")}>Book Now
                                {isSingleButtonService ? <i className={arrowClass}></i>:''}
                            </Link>)
                        }
                        {isServiceLanding ? (<div className="cleaning-stamp book-now-stamp">
                            <Link onClick={ () => this.setQuotesServiceId(serviceId) } to={this.getBooknowLink(currentCity, bookNowJourney, true)} title={ h1Text != "" ? h1Text : title}>
                                <img src={imgixReactBase+"/dist/images/cleaning-stamp.png"} alt={ h1Text != "" ? h1Text : title} />
                            </Link>
                        </div>) : ''}
                    </div>
                </div>
                ) : ''}
                {(rightBoxTitle.length && getQuoteJourney != '-- Select if any --')? (
                <div className={"col-12 d-flex col-sm-6 " + ( leftBoxTitle.length ? 'col-md-5' : 'justify-content-center col-md-8' )}>
                    <div style={(leftBoxTitle.length && bookNowJourney != '-- Select if any --') ?{backgroundColor: "rgba(255,255,255,0.95)"}:{}} className={isSingleButtonService?'' :'p-4 '+ " text-center d-flex flex-fill flex-column align-items-center justify-content-center rounded"}>
                        {
                            (leftBoxTitle.length && bookNowJourney != '-- Select if any --') ? (
                                <div>
                                    <h3 className="mt-0 font-weight-bold">{rightBoxTitle}</h3>
                                    <h2 className="p">{rightBoxDesc}</h2>
                                </div>
                            ) : ''
                        }
                        {
                            (serviceId === 'SERVICE_HOME_INSURANCE' || serviceId === 'SERVICE_CAR_INSURANCE') ?
                            (<a href={"/"+getQuoteJourney} className={"btn font-weight-bold px-4 text-uppercase btn-warning spread-quotes" +(isSingleButtonService ? ' cta-btn ' : '')}>{leftBoxTitle.length ? locationHelper.translate("GET_QUOTES") : locationHelper.translate("GET_FREE_QUOTES")}
                            {isSingleButtonService ?<i className={arrowClass}></i> : ''}</a>)
                            :
                            (<Link onClick={ () => this.setQuotesServiceId(serviceId) }  to={"/"+lang+"/"+currentCity+"/"+getQuoteJourney+"/journey1"} className={"btn font-weight-bold px-4 text-uppercase " +(isSingleButtonService ? ' cta-btn ' : '') +  ((leftBoxTitle.length && bookNowJourney != '-- Select if any --')?"btn-outline-dark":"btn-warning spread-quotes")}>{(leftBoxTitle.length && bookNowJourney != '-- Select if any --') ? locationHelper.translate("GET_QUOTES") : locationHelper.translate("GET_FREE_QUOTES")}
                            {isSingleButtonService ?<i className={arrowClass}></i> : ''}
                            </Link>)
                        }
                        
                    </div>
                </div>
                ) : ''}
            </div>
        );
    }
}
function mapStateToProps(state){
	return {
        currentCity: state.currentCity,
        currentCityData: state.currentCityData,
        lang: state.lang
    }
}
BookOrQuotes.defaultProps = {
    serviceId: '',
    leftBoxTitle: '',
    leftBoxDesc: '',
    rightBoxTitle: '',
    rightBoxDesc: '',
    promo:''
  };
export default withCookies( connect(mapStateToProps)(BookOrQuotes) );