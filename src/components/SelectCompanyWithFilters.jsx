import React from "react";
import SelectCompanies from "./SelectCompanies";
import commonHelper from "../helpers/commonHelper";
import GeneralModal from "./GeneralModal";
import CompanyItem from "./CompanyItem";
import { withCookies } from "react-cookie";
import { connect } from "react-redux";
import SectionTitle from "./SectionTitle";
import TitleTextBlock from "./TitleTextBlock";
import decode from 'unescape';

class SelectCompanyWithFilters extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showMaxCompanyModel: false,
            showSelectCompanyError: false,
            maxSelection: !!props.maxSelection ? props.maxSelection : 5,
        };
        this.selectChange = this.selectChange.bind(this);
        this.showOrHideModalMaxCompany = this.showOrHideModalMaxCompany.bind(this);
        this.getshowMaxCompanyModelContent = this.getshowMaxCompanyModelContent.bind(this);
        this.hideModalSelectCompanyError = this.hideModalSelectCompanyError.bind(this);
        this.cuisineFilterChange = this.cuisineFilterChange.bind(this);
        this.setService = this.setService.bind(this);

    }
    componentWillMount() {
        var citiesOptions = [];
        var serviceOptions = [];

        this.props.cities.map((item) => {
            citiesOptions.push({ id: item.id, label: item.name, value: item.name.toLocaleLowerCase().split(" ").join("-") })
        });

        this.props.allServices.parentServices.map((item) => {
            serviceOptions.push({ id: item.id, label: item.label, value: item.id })
        });

        this.setState({
            citiesOptions: citiesOptions,
            serviceOptions: serviceOptions,
            serviceID: this.props.serviceID,
            subServiceId: this.state.subServiceId?this.state.subServiceId:this.props.subServiceID,
        })
    }
    componentDidMount() {
        var maxLimit = commonHelper.getSeoFilterTextArray(this.state.serviceID, 'maxLimit');

        // console.log("maxLimit", maxLimit);

        if (typeof maxLimit == "number") {
            this.setState({
                maxSelection: maxLimit
            })
        }


    }
    componentDidUpdate(prevProps) {
        if (prevProps.serviceID != this.props.serviceID) {
            this.setState({
                serviceID: this.props.serviceID
            })
        }
    }
    selectChange(company) {
        const { selectedCompanies, selectChange } = this.props;
        const { maxSelection } = this.state;
        const temp = selectedCompanies.filter(item => item.id == company.id);

        //company = {id:company.id, name:company.name};

        if (temp.length) {
            selectChange(selectedCompanies.filter(item => item.id != company.id));
            if (this.state.showMaxCompanyModel) {
                //console.log("Calling Here");
                this.showOrHideModalMaxCompany(false);
            }
        } else {
            if ((selectedCompanies.length + 1) > maxSelection) {
                this.showOrHideModalMaxCompany(true);
            } else {
                selectedCompanies.push(company);
                selectChange(selectedCompanies);
            }
        }
    }
    showOrHideModalMaxCompany(boolVal) {
        this.setState({
            showMaxCompanyModel: boolVal
        })
    }
    cuisineFilterChange(name, value) {
        this.props.cuisineFilterChange("cuisine", value)
    }
    setService(value) {
        this.setState({
            serviceID: value
        })
    }
    getshowMaxCompanyModelContent() {
        var cardClass = "card container-fluid mb-3 shadow-sm service-provider-container";
        const cheched = true;

        return (
            <div>
                <p className="text-center error-desc mb-3">If you wish to add another company, please remove one of the companies listed below first.</p>
                <div className="d-none d-md-block">
                    {this.props.selectedCompanies.map((item, index) => {
                        return (
                            <CompanyItem lazyLoad={false} key={item.id} item={item} cardClass={cardClass} selectChange={this.selectChange} isChecked={cheched} />
                        )
                    })}
                </div>
            </div>
        );
    }
    hideModalSelectCompanyError(boolVal) {
        this.props.showOrHideModalSelectCompanyError(false);
    }

    /*replaceCurrentCityName(text){
        var currentCityData = this.props.currentCityData;
        var cityName = "";
        if(currentCityData.length){
            cityName = currentCityData[0].name;
            return text.replace(/%s/g, cityName)
        }
    }*/
    render() {
        const { currentCity, filterService } = this.props;
        var rating_filter_options = [
            { label: "Filter by rating", value: '', disabled: true },
            { label: "All", value: 'all', disabled: false },
            { label: "4 - 5 Star", value: '5', disabled: false },
            { label: "3 - 3.9 Star", value: '4', disabled: false },
            { label: "2 - 2.9 Star", value: '3', disabled: false },
            { label: "1 - 1.9 Star", value: '2', disabled: false }
        ],
            category_filter_options = [],

            default_service_filter_options = [
                { label: "Filter by service", value: '', disabled: true },
                { label: "All", value: 'all', disabled: false }
            ];

        const default_mandatory_service_filter_options = [
            { label: "Service", value: '', disabled: true },
        ];

        if (commonHelper.isInteractiveService(this.state.serviceID)) {
            category_filter_options = [
                { label: "Filter by price", value: '', disabled: true },
                { label: "All", value: 'all', disabled: false },
                { label: "Budget", value: 'Budget', disabled: false },
                { label: "Premium", value: 'Premium', disabled: false },
                { label: "Standard", value: 'Standard', disabled: false }
            ]
        }

        var lookupServices = commonHelper.lookupServices('parent_service_id', this.state.serviceID);
        var service_filter_options = default_service_filter_options.concat(lookupServices);
        var mandatory_service_filter_options = default_mandatory_service_filter_options.concat(lookupServices);
        //console.log(service_filter_options);

        const filters = [
            { name: "rating", options: rating_filter_options },
            {
                name: "promotions", options: [{ label: "Filter by promotion", value: '', disabled: true },
                { label: "All", value: 'all', disabled: false },
                { label: "Yes", value: 'yes', disabled: false },
                { label: "No", value: 'no', disabled: false }]
            },
            { name: "category_filter", options: category_filter_options },
            { name: "service_filter", options: service_filter_options },
        ];
        const mandatoryFilters = [
            { name: "all_services_filter", options: this.state.serviceOptions },
            { name: "service_filter", options: mandatory_service_filter_options },
        ];
        const { selectedCompanies, showSelectCompanyError } = this.props;

        var seoText = commonHelper.getSeoFilterTextArray(this.state.serviceID);

        var seoTitle = Object.keys(seoText).length != 0 ? commonHelper.replaceCodedCurrentCityName(seoText.seoTitle, this.props.currentCity) : "";
        var seoDescription = Object.keys(seoText).length != 0 ? commonHelper.replaceCodedCurrentCityName(seoText.seoDescription, this.props.currentCity) : "";
        //console.log("showIn", this.props.showIn);

        return (
            <React.Fragment>
                { ( this.props.showIn == "quotes" ) ? (
                <div className="seo-text-description mb-4">
                    <div className="seo-text-title">
                        <p className="h3">{decode(seoTitle,'all')}</p>
                        <p>{decode(seoDescription,'all')}</p>                        
                    </div>
                </div>) : (<div className="seo-text-description-reviews">
                    <SectionTitle childClass='col px-3 px-md-3' title={seoTitle} />
                    <TitleTextBlock text={seoDescription} parentClass="row mb-1 mb-md-4"/>
                </div>)
            }

                <SelectCompanies
                    filterService={filterService}
                    mandatoryFilters={mandatoryFilters}
                    showServiceReviewsFilters={this.props.showServiceReviewsFilters}
                    serviceID={this.state.serviceID}
                    subServiceId={this.state.subServiceId}
                    currentCity={currentCity}
                    filters={filters}
                    match={this.props.match}
                    history={this.props.history}
                    selectChange={this.selectChange}
                    selectedCompanies={selectedCompanies}
                    showCuisineFilter={this.props.showCuisineFilter}
                    cuisineFilterValue={this.props.cuisineFilterValue}
                    cuisineFilterChange={this.cuisineFilterChange}
                    setService={this.setService}
                />
                {this.props.showBack ? (
                    <div className="row mt-2 goback">
                        <div className="col-12">
                            <a href="#1" className="text-black"> <i className="fa fa-angle-left"></i> Back</a>
                        </div>
                    </div>
                ) : ''}
                {this.state.showMaxCompanyModel && <GeneralModal
                    title={`You have already selected  
                                ${this.state.maxSelection == 3 ? ' three ' : 'five '}  
                                companies for your request`}
                    modalClass="max-selected-modal"
                    modalSize="modal-lg"
                    modalBody={this.getshowMaxCompanyModelContent()}
                    setModal={this.showOrHideModalMaxCompany} />
                }
                {showSelectCompanyError && <GeneralModal
                    showHeader={false}
                    modalBody="Please select at least one company to proceed."
                    modalClass="max-selected-modal model-no-header"
                    modalSize="modal-lg"
                    setModal={this.hideModalSelectCompanyError} />}
            </React.Fragment>
        );
    }
}

SelectCompanyWithFilters.defaultProps = {
    filterService: 0,
    showCuisineFilter: false,
    cuisineFilterValue: [],
    maxSelection: 5,
    showBack: true,
    showIn: 'quotes',
    showServiceReviewsFilters: false
};
function mapStateToProps(state) {
    return {
        serviceConstants: state.serviceConstants,
        currentCityData: state.currentCityData,
        cities: state.cities,
        currentCity: state.currentCity,
        allServices: state.allServices,
    }
}
export default withCookies(connect(mapStateToProps)(SelectCompanyWithFilters));
//export default SelectCompanyWithFilters;

//export default connect(mapStateToProps)(SelectCompanyWithFilters);
