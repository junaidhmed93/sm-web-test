import React from "react";
import Slider from "react-slick";
import LazyLoad from 'react-lazyload';
import PlaceholderComponent from '../components/Placeholder';
import SelectInput from "./Form_Fields/SelectInput";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {
    fetchAllReviews,
    getCityCode,
    isValidSection,
    setCurrentCity,
    fetchServices,
    showLoader,
    hideLoader,
    setJourneySubServiceData,
    LANG_EN,
    URLCONSTANT
} from "../actions";
import commonHelper from "../helpers/commonHelper";
import locationHelper from "../helpers/locationHelper";
import moment from "moment/moment";
import SectionTitle from "./SectionTitle";
import  decode from 'unescape';

var selectOptions = [{'id':1,'label':'as','value':'sa'},{'id':2,'label':'as','value':'sa'},{'id':3,'label':'as','value':'sa'}];

class GoogleReviews extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
        	city:{value:null},
			service:{value:null},
            citiesOptions: [],
        	serviceOptions: []
		}
        this.handleInputChange = this.handleInputChange.bind(this);
        this.moveNext = this.moveNext.bind(this);
        this.setCityAndService = this.setCityAndService.bind(this);
        this.setCity = this.setCity.bind(this);
        this.locationHelperInit = locationHelper.init.bind(this);
        this.locationHelperInit();
    }
    componentDidMount() {
        var citiesOptions = [];
        var serviceOptions = [];
        let {reviewsData, currentCity, cities, serviceID, service_constants, lang,allServices} = this.props;
        
        cities.map((item)=>{
            citiesOptions.push({id: item.id, label:item.name, value: commonHelper.slugify(item.nameEn)})
        });

        if (allServices.parentServices) {
            allServices.parentServices.map((item)=>{
                serviceOptions.push({id: item.id, label:item.label, value: item.id, subServices: item.subServices})
            });
        }

        this.setState({
            citiesOptions:citiesOptions,
            serviceOptions:serviceOptions
        })
        //console.log("calling 123 componentDidMount");
        /*if(!reviewsData.length){
            this.props.fetchAllReviews(getCityCode(currentCity, cities), serviceID != '' ? service_constants[serviceID] : serviceID, lang);
        }*/
        //console.log(this.props);
        this.setCityAndService(citiesOptions, serviceOptions);
	}
    handleInputChange(name, value) {
        this.setState({
            [name]: value
        });
    }
    moveNext() {
        let {lang} = this.props;
        if (isValidSection('read-reviews')) {
            this.props.setJourneySubServiceData({});
            var slug = commonHelper.getServiceSlugByParentID(URLCONSTANT, this.props.service_constants, this.state.service.value);
            if (this.props.allServices.subServices) {
                this.props.allServices.subServices.map((item)=>{
                    if(item.id == this.state.service.value)
                        this.props.setJourneySubServiceData(this.state.service);
                });
            }

            if (slug == '') {
                slug = this.props.getQuoteJourney;
            }
            let redirectUrl = "/en/" + this.props.currentCity + "/" + slug + '/reviews';
            if(lang == LANG_EN){
                this.props.history.push(redirectUrl);
            }else{
                window.location = redirectUrl;
            }
        }
    }
    /*componentWillReceiveProps(newProps){
        console.log('componentWillReceiveProps', this.props.serviceID);
        let {cities, service_constants, lang} = this.props;
        if ( (newProps.currentCity !== this.props.currentCity) || ( newProps.serviceID !== this.props.serviceID) ) {
            let {currentCity, serviceID} = newProps;
            console.log('serviceID', serviceID);
            serviceID != '' ? service_constants[serviceID] : serviceID;
            this.props.fetchAllReviews(getCityCode(currentCity, cities), serviceID , lang);
        }
    }*/
    componentDidUpdate(prevProps){
        const prevCity    = prevProps.currentCity;
        const currentCity = this.props.currentCity;
        const prevServiceID    = prevProps.serviceID;
        const currentServiceID = this.props.serviceID;
        //console.log("component Did Update", currentServiceID, prevServiceID);
        if( (prevCity !== currentCity) || (prevServiceID !== currentServiceID) ){
            /*console.log("component Did Update 123", currentServiceID, prevServiceID);
            this.props.fetchAllReviews(getCityCode(currentCity, this.props.cities), (currentServiceID && currentServiceID != '') ? this.props.service_constants[currentServiceID] : '', this.props.lang);*/
            this.setCityAndService(this.state.citiesOptions, this.state.serviceOptions);
        }

    }

    setCityAndService(citiesOptions,serviceOptions) {
        var sID = (this.props.serviceID && this.props.serviceID != '') ? this.props.service_constants[this.props.serviceID] : '';
        // console.log('this.props.currentCity');
        // console.log(this.props.currentCity);
        citiesOptions.map(item => {
            // console.log(item.value);
            if (item.value == this.props.currentCity) {
                this.setState({
                    city: item
                });
            }

        });

        var notParentService = true;
        var subService = {};
        var itsParentService = -1;
        serviceOptions.map(item => {
            // console.log(item.value);
            if (item.subServices.indexOf(sID) != -1) {
                itsParentService = item
            }
            if (item.id == sID) {
                notParentService = false;
                this.setState({
                    service: item
                });
            }
        });
        if (notParentService && itsParentService != -1) {
            var subSrviceOptions = [];
            this.props.allServices.subServices.map((item)=>{
                if(itsParentService.subServices.indexOf(item.id) != -1){
                    subSrviceOptions.push({id: item.id, label:item.label, value: item.id})
                    if (item.id == sID) {
                        subService = {id: item.id, label:item.label, value: item.id};
                    }
                }

            });
            this.setState({
                serviceOptions: subSrviceOptions,
                service: subService
            })
        }
    }
    renderItems(){
        var items = [];
        if (this.props.reviewsData && this.props.reviewsData.length > 0) {
            items[0] = this.props.reviewsData[0];
        }
        if (this.props.reviewsData && this.props.reviewsData.length > 1) {
            items[1] = this.props.reviewsData[1];
        }
        return items.map((item) => {
            return (
                <div key={item.id} className="d-none d-md-block col px-4 px-md-3 item google-customer-review">
                    <div className="card bg-light">
                        <div className="card-body">
                            <p className="card-title font-weight-bold">{decode(item.approvedTitle,'all')}</p>
                            <div className="card-text">
                                <p>{decode(item.approvedReview,'all')}</p>
                                <div className="row align-items-center">
                                    <div className="col-8">
                                        <h5>{decode(item.reviewerName,'all')}</h5>
                                        <h6>{moment(item.creationDate).format("DD/MM/YYYY")}</h6>
                                        <p className="m-0">
                                            <i className="fa fa-star text-warning"></i>
                                            <i className="fa fa-star text-warning"></i>
                                            <i className="fa fa-star text-warning"></i>
                                            <i className="fa fa-star text-warning"></i>
                                            <i className="fa fa-star text-warning"></i>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        });
    }
    renderSliderItems(){
        var items = [];
        if (this.props.reviewsData && this.props.reviewsData.length > 0) {
            items[0] = this.props.reviewsData[0];
        }
        if (this.props.reviewsData && this.props.reviewsData.length > 1) {
            items[1] = this.props.reviewsData[1];
        }
        return items.map((item) => {
            return (
                <div key={item.id} className="d-md-none col px-0 px-md-3 item google-customer-review">
                    <div className="card bg-light">
                        <div className="card-body">
                            <p className="card-title font-weight-bold">{item.approvedTitle}</p>
                            <div className="card-text">
                                <p>{decode(item.approvedReview,'all')}</p>
                                <div className="row align-items-center">
                                    <div className="col-8">
                                        <h5>{item.reviewerName}</h5>
                                        <h6>{moment(item.creationDate).format("DD/MM/YYYY")}</h6>
                                        <p className="m-0">
                                            <i className="fa fa-star text-warning"></i>
                                            <i className="fa fa-star text-warning"></i>
                                            <i className="fa fa-star text-warning"></i>
                                            <i className="fa fa-star text-warning"></i>
                                            <i className="fa fa-star text-warning"></i>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        });
    }
    setCity(name, value){
        var city = value;
        this.props.showLoader();
        const current = city.value;
        this.props.setCurrentCity(current);
        let {lang} = this.props;
        this.props.fetchServices(current, lang).then(()=> this.props.hideLoader());
        switch(this.props.bodyClass){
            case 'home':
                this.props.history.push("/"+this.props.match.params.lang+"/"+current);
                break;
            case 'landing':
                this.props.history.push("/"+this.props.match.params.lang+"/"+current+"/"+this.props.match.params.slug);
                break;
            case 'companies':
                this.props.history.push("/en"+"/"+current+"/partners-list");
                break;
            case 'service_reviews':
                this.props.history.push("/"+this.props.match.params.lang+"/"+current+"/"+this.props.match.params.slug+"/reviews");
                break;
        }
        this.setState({
            showCities: false,
            city: city
        });
    }
    render() {
        // console.log(this.props);
        let {lang} = this.props;
        if (lang == LANG_EN && (this.props.reviewsData && this.props.reviewsData.length)) {
            var settings = { dots: false, arrows: false, infinite: true, speed: 500, slidesToShow: 2, slidesToScroll: 1, infinite: false, responsive: [ { breakpoint: 1199, settings: { slidesToShow: 2 } }, { breakpoint: 767, settings: { unslick: true } } ] };
            return (
                [
                    <SectionTitle title={locationHelper.translate("VERIFIED_CUSTOMER_REVIEWS")} subTitle={locationHelper.translate("READ_OVER_3000_VERIFIED_REVIEWS")} />
                    ,
                    <div className="row customer-reviews mb-3">
                        <div style={{'paddingRight': 0}} className={'d-md-none col-sm-8 reviews-slider ' + this.props.parentClass}>
                            <div className={this.props.childClass}>
                                <div className="sm-slider">
                                    <Slider {...settings}>
                                        {this.renderSliderItems()}
                                    </Slider>
                                </div>
                            </div>
                        </div>
                        {this.renderItems()}
                        <div className="d-none d-md-block col px-4 px-md-3 item">
                            <div id="read-reviews" className="card bg-light">
                                <div className="card-body">
                                    <p className="card-title mar-no text-center"><strong>{locationHelper.translate("PUT_YOUR_MIND_AT_EASE_BEFORE_HIRING")}</strong></p>
                                    <div className="card-text">
                                        <p className="text-center">{locationHelper.translate("READ_OVER_3000_VERIFIED_REVIEWS")}</p>
                                    </div>
                                    <div className="row">
                                        <div className="col-sm-12 mb-2">
                                            <SelectInput name="city" inputValue={this.state.city} label={locationHelper.translate("CITY")} options={this.state.citiesOptions} onInputChange={this.setCity} />
                                        </div>
                                        <div className="col-sm-12 mb-2">
                                            <SelectInput name="service" inputValue={this.state.service} label={locationHelper.translate("CHOOSE_A_SERVICE")} options={this.state.serviceOptions} onInputChange={this.handleInputChange} />
                                        </div>
                                        <div className="col-sm-12 mb-2">
                                            <button className="btn btn-block btn-blue font-weight-bold text-uppercase" onClick={()=> this.moveNext()}>
                                                <span>{locationHelper.translate("READ_REVIEWS")}</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    ]
            );
        } else {
            return '';
        }

    }
}
GoogleReviews.defaultProps = {
  parentClass: 'row mb-4 md-md-5',
  childClass: 'col px-4 px-md-3 reviews',
  items: [{id: 1, image: 'user-1.png'},{id: 2, image: 'user-2.png'},{id: 3, image: 'user-3.png'},{id: 4, image: 'user-1.png'}],
  serviceID: '',
  getQuoteJourney: ''
};


function mapStateToProps(state){
    return {
        cities: state.cities,
        currentCity: state.currentCity,
        allServices: state.allServices,
        service_constants: state.serviceConstants,
        reviewsData: state.reviewsData,
        bodyClass: state.bodyClass,
        lang:state.lang
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({setJourneySubServiceData, fetchAllReviews, setCurrentCity, fetchServices, showLoader, hideLoader}, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(GoogleReviews);
