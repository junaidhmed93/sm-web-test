import React from "react";

class StarRating extends React.Component{
    constructor(props) {
        super(props);
    }
    render() {
        var {rating, numberOfReviews,calculatedRating} = this.props;
        var present = 0;
        /*console.log(rating, numberOfReviews,calculatedRating);
        if((typeof calculatedRating != "undefined" && calculatedRating != 'N/A') && parseInt(calculatedRating) > 0){
            rating = calculatedRating;
        }else{
            if(rating != 'N/A') {
                rating = (rating / numberOfReviews).toFixed(1)
            }
        }
        rating = rating == 'N/A' ? 0 : rating;*/
        rating = rating == 'NaN' ? 0 : rating;
        if(rating != 0){
            present = (rating * 100) / 5;
        }

        var widthFill = {width:present+"%"};

        return (
            <div className="m-0 mr-2 star-rating">
                <div className="back-stars">
                    <i className="fa fa-star text-black"></i>
                    <i className="fa fa-star text-black"></i>
                    <i className="fa fa-star text-black"></i>
                    <i className="fa fa-star text-black"></i>
                    <i className="fa fa-star text-black"></i>
                    <div className="front-stars" style={widthFill}>
                        <i className="fa fa-star text-warning"></i>
                        <i className="fa fa-star text-warning"></i>
                        <i className="fa fa-star text-warning"></i>
                        <i className="fa fa-star text-warning"></i>
                        <i className="fa fa-star text-warning"></i>
                    </div>
                </div>
            </div>
        );
    }
}

export default StarRating;
