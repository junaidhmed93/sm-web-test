import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withCookies } from "react-cookie";
import CompanyItem from "./CompanyItem";
import FormTitleDescription from "./FormTitleDescription";
import { fetchOffersData, getCityCode } from "../actions/index";

class SelectOffers extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            offers: []
        };

        this.renderResults = this.renderResults.bind(this);
        this.selectChange = this.selectChange.bind(this);
        this.fetchDeals = this.fetchDeals.bind(this);
    }
    componentDidMount() {
        const { cities, currentCity, movingSize, serviceID } = this.props;
        const cityCode = getCityCode(currentCity, cities);
        var userToken = typeof this.props.signInDetails.access_token != "undefined" ? this.props.signInDetails.access_token : "";

        this.fetchDeals();

    }
    componentDidUpdate(prevProps) {
        if ((prevProps.movingSize != this.props.movingSize) || (prevProps.serviceID != this.props.serviceID)) {
            this.fetchDeals();
        }
    }
    fetchDeals() {
        var userToken = typeof this.props.signInDetails.access_token != "undefined" ? this.props.signInDetails.access_token : "";
        var response = [];
        const { cities, currentCity, movingSize, serviceID } = this.props;
        const cityCode = getCityCode(currentCity, cities);
        fetchOffersData(userToken, cityCode, serviceID, encodeURIComponent(movingSize)).then(res => {
            var response = res ? res.data : []

            this.setState({
                offers: response
            })
        });
    
}
renderResults() {
    const { selectedOffers } = this.props;
    const { offers } = this.state;
    const isOffer = true;
    var cardClass = "card container-fluid mb-3 shadow-sm service-provider-container";

    return offers.map((item, index) => {
        const cheched = selectedOffers.filter(company => company.id == item.id).length ? true : false;
        return (
            <CompanyItem lazyLoad={false} key={"offer" + item.id} item={item} isChecked={cheched} cardClass={cardClass} selectChange={this.selectChange} isOffer={isOffer} />
        )
    });
}
selectChange(offer) {
    var { selectedOffers, selectOffersChange } = this.props;
    var temp = selectedOffers.filter(item => item.id == offer.id);
    if (temp.length) {
        selectOffersChange(selectedOffers.filter(item => item.id != offer.id));
    } else {
        selectOffersChange([...selectedOffers, ...[offer]]);
    }
}
render() {
    const { offers } = this.state;
    return (
       ( typeof offers != "undefined" && offers.length )? (<section>
            <FormTitleDescription 
            title="Special offers" 
            desc="We have hand-picked the following special offers for you. Select the offer you are interested in and you will be contacted by the supplier to schedule the service." />
            <div className="row">
                <div className="col">
                    {this.renderResults()}
                </div>
            </div>
        </section>) : (<div>&nbsp;</div>)
    );
}
}

SelectOffers.defaultProps = {
    serviceID: 0,
    movingSize: '',
    currentCity: ''
};

function mapStateToProps(state) {
    return {
        signInDetails: state.signInDetails,
        cities: state.cities
    }
}

export default withCookies(connect(mapStateToProps)(SelectOffers));
