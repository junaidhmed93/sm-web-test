import React from "react";
import LazyLoad from 'react-lazyload';
import PlaceholderComponent from '../components/Placeholder';

class ContentTooltipImage extends React.Component{
    constructor(props) {
        super(props);
    }
    render() {
        var {customClass, imageURL} = this.props;
        return (
            <div className={"col-12 col-md-6 pl-md-0 stack-top "+ customClass}>
                <LazyLoad offset={500} placeholder={<PlaceholderComponent />}>
                    <div className="placeholder rect bg-primary rounded" style={{background: "url("+imageURL+") center/cover no-repeat"}}></div>
                </LazyLoad>
            </div>
        );
    }
}

ContentTooltipImage.defaultProps = {
    imageURL: '',
    customClass: ''
}
export default ContentTooltipImage;
