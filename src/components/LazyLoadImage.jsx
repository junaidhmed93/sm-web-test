import React from "react";

class LazyLoadImage extends React.Component{
    constructor(props) {
        super(props);
    }
    render() {
        // console.log('this.props.src');
        // console.log(this.props.src);
        return (
            <img style={this.props.style} width={this.props.width} className={"lazy " + this.props.className} src={this.props.dataSrc + "?fit=crop&w=40&auto=format,compress&q=0&lossless=0"} data-src={this.props.src + '48'}
                 data-srcset={this.props.src} />
        );
    }
}

LazyLoadImage.defaultProps = {
    src: '',
    className: '',
    width: '',
    dataSrc: '',
    style: {}
};

export default LazyLoadImage;
