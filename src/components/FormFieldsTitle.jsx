import React from "react";

class FormFieldsTitle extends React.Component{
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className={this.props.parentClass}>
                <div className={this.props.childClass}>
                    <p className={this.props.titleClass}>{this.props.title}</p>
                </div>
            </div>
        );
    }
}

FormFieldsTitle.defaultProps = {
  title: '',
  parentClass: 'row',
  childClass: 'col',
  titleClass: 'h3 mb-4 mobile-margin'
};

export default FormFieldsTitle;
