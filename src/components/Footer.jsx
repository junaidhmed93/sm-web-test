import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {fetchServices, toggleFeedbackModal} from "../actions/index";
import { Link } from "react-router-dom";
import {imgixReactBase} from '../imgix';
import LazyLoad from 'react-lazyload';
import PlaceholderComponent from '../components/Placeholder';
import commonHelper from "../helpers/commonHelper";
import locationHelper from "../helpers/locationHelper";

class Footer extends React.Component{
    constructor(props) {
        super(props);
        this.locationHelperInit = locationHelper.init.bind(this);
        this.locationHelperInit();
    }
    componentDidMount(){
        let {lang} = this.props;
        if ( !this.props.footerServices.length ) {
            this.props.fetchServices(this.props.currentCity, lang);
        }
	}
    renderList(){
		return this.props.footerServices.map((links, index)=> {
            var label = '';
            if(links.length) {
                for (var i = 0; i < links[0].footer_category.length; i++) {
                    if (links[0].footer_category[i].value == commonHelper.footerCategoriesOrder()[index]) {
                        label = links[0].footer_category[i].label;
                    }
                }
            }
			if(links.length){
				return (
					<div key={"category"+index} className="col col-md-2">
                        <h5 className="h3 mb-4 text-white font-weight-bold">{label}</h5>
                        <ul className="list-unstyled">
                        {this.renderLinks(links)}
                        </ul>
                    </div>
				)
			}
		});
    }
    renderLinks(links){
        const {currentCity,lang} = this.props;
		return links.map((link, index)=> {
			return (
                <li key={"link"+index} className="mt-3">
				<Link to={"/"+lang+"/"+currentCity+"/"+link.url_slug} className="text-white">{link.service_preview_title}</Link>
                </li>
            )
		});
    }

    render() {
        const {lang} = this.props;
        return (
            <footer>
                { typeof this.props.footerItemsData.acf != "undefined" &&
                (<React.Fragment>
                    { this.props.showFooterTop && (
                    <div className="footer-top d-none d-md-block">
                        <div className="container">
                            <div className="row pt-5">
                            {this.renderList()}

                            </div>
                        </div>
                    </div>) }
                    <div className="footer-bottom">
                    <div className="container">
                        <div className="row py-5">
                            <div className="col-12 col-md-5">
                                <h5 className="h3 mb-4 text-white font-weight-bold">{locationHelper.translate("ADDITIONAL_INFORMATION")}</h5>
                                <div className="row">
                                    <div className="col-6">
                                        <ul className="list-unstyled">
                                            <li className="mb-3">
                                                <Link to={"/"+lang+"/contact-us"} className="text-white">{this.props.footerItemsData.acf.footer_contact_us_name}</Link>
                                            </li>
                                            <li className="mb-3">
                                                <Link className="text-white" to={"/"+lang+"/faqs"}>{this.props.footerItemsData.acf.footer_frequently_asked_questions}</Link>
                                            </li>
                                            <li className="mb-3">
                                                <a className="text-white" href="https://servicemarket.com/en/blog">{this.props.footerItemsData.acf.footer_blog_name}</a>
                                            </li>
                                            <li className="mb-3">
                                                <Link to={"/"+lang+"/write-a-review"} className="text-white">{this.props.footerItemsData.acf.footer_write_a_review_name}</Link>
                                            </li>
                                            <li className="mb-3">
                                                <a href="javascript:void(0)" role="button" onClick={() => this.props.toggleFeedbackModal({visibility:true})} className="text-white">{this.props.footerItemsData.acf.footer_service_complaints_feedback}</a>
                                            </li>
                                            <li className="mb-3">
                                                <Link to={"/"+lang+"/terms-and-conditions"} className="text-white">{this.props.footerItemsData.acf.footer_terms_and_conditions_name}</Link>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="col-6">
                                        <ul className="list-unstyled">
                                            <li className="mb-3">
                                                <Link to={"/"+lang+"/about-us"} className="text-white">{this.props.footerItemsData.acf.footer_about_name}</Link>
                                            </li>
                                            <li className="mb-3">
                                                <Link to={"/"+lang+"/careers"} className="text-white">{this.props.footerItemsData.acf.footer_careers_name}</Link>
                                            </li>
                                            <li className="mb-3">
                                                <Link to={"/"+lang+"/privacy-policy"} className="text-white">{this.props.footerItemsData.acf.footer_privacy_policy_name}</Link>
                                            </li>
                                            <li className="mb-3">
                                                <Link to={"/"+lang+"/disclaimer"} className="text-white">{this.props.footerItemsData.acf.footer_disclaimer_name}</Link>
                                            </li>
                                            <li className="mb-3">
                                                <Link to={"/"+lang+"/payment-and-refund-policy"} className="text-white">{this.props.footerItemsData.acf.footer_payment_and_refund_policy_name}</Link>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                            <div className="d-md-none col-6">
                                <h5 className="h3 mb-4 text-white font-weight-bold">{locationHelper.translate("PARTNERS")}</h5>
                                <ul className="list-unstyled">
                                    <li className="mb-3">
                                        <Link to={"/"+lang+"/become-a-partner"} className="text-white">{this.props.footerItemsData.acf.footer_become_a_partner_name}</Link>
                                    </li>
                                    <li className="mb-3">
                                        <Link to={"/"+lang+"/"+this.props.currentCity+"/partners-list"} className="text-white">{this.props.footerItemsData.acf.footer_our_partners_name}</Link>
                                    </li>
                                </ul>
                            </div>
                            <div className="col-12 col-md-7">
                                <div className="row">
                                    <div className="d-none d-md-block col-6">
                                        <h5 className="h3 mb-4 text-white font-weight-bold">{locationHelper.translate("PARTNERS")}</h5>
                                        <ul className="list-unstyled">
                                            <li className="mb-3">
                                                <Link to={"/"+lang+"/become-a-partner"} className="text-white">{this.props.footerItemsData.acf.footer_become_a_partner_name}</Link>
                                            </li>
                                            <li className="mb-3">
                                                <Link to={"/"+lang+"/"+this.props.currentCity+"/partners-list"} className="text-white">{this.props.footerItemsData.acf.footer_our_partners_name}</Link>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="col-12 col-md-6 mt-4 mt-md-0">
                                        <h5 className="h3 mb-4 text-white font-weight-bold">{locationHelper.translate("GET_IN_TOUCH")}</h5>
                                        <ul className="list-unstyled">
                                            <li className="mt-3 text-white">{locationHelper.translate("ADDRESS")}: {locationHelper.translate("REEF_TOWER_JLT_DUBAI_UAE")}</li>
                                            <li className="text-white">{locationHelper.translate("EMAIL")}: <a className="text-white"
                                                                                 href="mailto:support@servicemarket.com">support@servicemarket.com</a>
                                            </li>
                                            <li className="text-white">{locationHelper.translate("PHONE")}: <span className="ltr d-inline-block">+971 4 422 9639</span></li>
                                            <li className="text-white">{locationHelper.translate("FAX")}: <span className="ltr d-inline-block">+971 4 422 9823</span></li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-6">
                                        <h5 className="h3 mb-4 text-white font-weight-bold">{locationHelper.translate("FOLLOW_US")}</h5>
                                        <a href="https://www.facebook.com/myservicemarket" target="_blank">
                                                    <span className="fa-stack fa-lg">
                                                        <i className="fa fa-circle text-white fa-stack-2x"></i>
                                                        <i className="fa fa-facebook fa-stack-1x text-primary-dark"></i>
                                                    </span>
                                        </a>
                                        <a href="https://www.instagram.com/myservicemarket/" target="_blank">
                                                    <span className="fa-stack fa-lg">
                                                        <i className="fa fa-circle text-white fa-stack-2x"></i>
                                                        <i className="fa fa-instagram fa-stack-1x text-primary-dark"></i>
                                                    </span>
                                        </a>
                                        <a href="https://twitter.com/myservicemarket" target="_blank">
                                                    <span className="fa-stack fa-lg">
                                                        <i className="fa fa-circle text-white fa-stack-2x"></i>
                                                        <i className="fa fa-twitter fa-stack-1x text-primary-dark"></i>
                                                    </span>
                                        </a>
                                        <a href="https://www.linkedin.com/company/servicemarket"
                                           target="_blank">
                                                    <span className="fa-stack fa-lg">
                                                        <i className="fa fa-circle text-white fa-stack-2x"></i>
                                                        <i className="fa fa-linkedin fa-stack-1x text-primary-dark"></i>
                                                    </span>
                                        </a>
                                        <a href="https://www.google.ae/maps/place/ServiceMarket/@25.0734718,55.1315495,14z/data=!4m5!3m4!1s0x3e5f6ca6c3a55555:0xaf640ee17dbd5d6!8m2!3d25.073987!4d55.143331"
                                           target="_blank">
                                                    <span className="fa-stack fa-lg">
                                                        <i className="fa fa-circle text-white fa-stack-2x"></i>
                                                        <i className="fa fa-google fa-stack-1x text-primary-dark"></i>
                                                    </span>
                                        </a>
                                    </div>
                                    <div className="col-6">
                                        <h5 className="h3 mb-4 text-white font-weight-bold">{locationHelper.translate("SECURE_PAYMENTS")}</h5>
                                        <img
                                            src={imgixReactBase + "/dist/images/newvisa.png?w=70&auto=format,compress"}
                                            className="mw-100 mr-2 ml-2" alt=""/>
                                        <img
                                            src={imgixReactBase + "/dist/images/newmastercard.png?fit=crop&w=70&auto=format,compress"}
                                            className="mw-100 ml-2" alt=""/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </React.Fragment>)}
            </footer>
        );
    }
}
Footer.defaultProps ={
    showFooterTop: true
}
function mapStateToProps(state){
	return {
		currentCity: state.currentCity,
		footerServices: state.footerServices,
        footerItemsData: state.footerItemsData,
        lang: state.lang
	}
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({fetchServices, toggleFeedbackModal}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Footer);
