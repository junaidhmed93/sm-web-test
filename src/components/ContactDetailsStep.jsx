import React from "react";
import BookNextStep from "./Form_Fields/BookNextStep";
import QuotesNextStep from "./Form_Fields/QuotesNextStep";
import TextFloatInput from "./Form_Fields/TextFloatInput";
import AreaSuggestionInput from "./Form_Fields/AreaSuggestionInput";
import SelectInput from "./Form_Fields/SelectInput";
import PhoneInput from "./Form_Fields/PhoneInput";
import FormTitleDescription from "./FormTitleDescription";
import locationHelper from "../helpers/locationHelper";
import commonHelper from "../helpers/commonHelper";
import Loader from "./Loader";

import {getAllCreditCard, CREDITCARD_COUPON,toggleLoginModal, isEmailExist, validEmail, URLCONSTANT} from "../actions/index";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
let allServiceConstant = {};
let URLConstant = {};
let currentCity = "dubai";
class ContactDetailsStep extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            isLocationDetailShow: false,
            loginRequired: false,
            checkingEmail: false,
            isUserLoggedIn: false
        }

        var contact_details = props.contact_details;

        var contact_state = {
            input_email: contact_details.input_email,
            input_phone: contact_details.input_phone,
            input_name: contact_details.input_name,
            input_last_name: contact_details.input_last_name
        };

        if(props.isLocationDetailShow == "yes"){
            contact_state.input_address_city = contact_details.input_address_city;
            contact_state.input_address_area = contact_details.input_address_area;
            contact_state.input_address_area_building_name = contact_details.input_address_area_building_name;
            contact_state.input_address_area_building_apartment = contact_details.input_address_area_building_apartment;
        }
        

        this.state = contact_state;
        this.state.city = props.currentCity;
        this.locationHelperInit = locationHelper.init.bind(this);
        this.locationHelperInit();
        this.locationElements = this.locationElements.bind(this);
        this.onInputChange = this.onInputChange.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.contactMoveNext = this.contactMoveNext.bind(this);
        

        //this.isLocationDetailShow = this.isLocationDetailShow(this);
    }
    componentDidMount() {
        allServiceConstant = this.props.service_constants;
        URLConstant =  this.props.url_constants;
        currentCity = this.props.currentCity;
        var isUserLoggedIn = commonHelper.isUserLoggedIn();
        this.setState({
            isUserLoggedIn: isUserLoggedIn
        })
    }
    onInputChange(name, value){
        this.props.handleInputChange(name, value);
    }
    handleEmailChange(name, value){
        if( !this.props.signInDetails.customer){
            if( validEmail(value) ){
                this.setState({
                    checkingEmail: true
                })
                var request = isEmailExist(value).then((res)=> {
                    if( value == this.props.contact_details.input_email ){
                        this.setState({
                            loginRequired: !res,
                        })
                    }
                    this.setState({
                        checkingEmail: false
                    })
                });
            }else{
                this.setState({
                    loginRequired: false
                })
            }
        }
        this.props.handleInputChange(name, value);
    }
    isLocationDetailShow(){
        var isLocationDetailShow = props.isLocationDetailShow === "yes" ? true : false;
        return isLocationDetailShow;
    }
    componentWillReceiveProps (newProps) {
        var {loginRequired} = this.state;
        var isLocationDetailShow = this.props.isLocationDetailShow;

        //console.log(newProps.userProfile);

        if( newProps.userProfile !== this.props.userProfile ) {

            var userProfile = newProps.userProfile;

            this.onInputChange("input_email",userProfile.email);
            this.onInputChange("input_name",userProfile.customerFirstName);
            this.onInputChange("input_last_name",userProfile.customerLastName);

            if (userProfile.address && userProfile.address.phoneNumber && userProfile.address.phoneNumber.length) {
                this.onInputChange("input_phone", userProfile.address.phoneNumber);
            }
            if(isLocationDetailShow == "yes") {
                let area = userProfile.address ? userProfile.address.area : '';
                var building_name = userProfile.address ? userProfile.address.building : '';
                var area_building_apartment = userProfile.address ? userProfile.address.apartment : '';
                //this.onInputChange("input_address_area", area);
                this.onInputChange("input_address_area_building_name", building_name);
                this.onInputChange("input_address_area_building_apartment", area_building_apartment);
            }
            if(this.props.couponValue != ""){
                this.props.handleCouponChange(this.props.couponValue);
            }
            var isUserLoggedIn = commonHelper.isUserLoggedIn();
            this.setState({
                loginRequired: false,
                isUserLoggedIn: isUserLoggedIn
            })
            
            if(this.props.isBooking){
                this.props.getAllCreditCard(this.props.signInDetails.access_token)
                    .then(() => {
                        this.setState({
                            loader: false
                        })
                });
            }
        }
    }
    locationElements(){
        var {cityOptions, autocompleteShow, areasOptions, userProfile} = this.props;
        var contact_details = this.props.contact_details;
        //var profileCity  = (userProfile.address && userProfile.address.city) ? userProfile.address.city.toLowerCase().replace(' ','-') : '';
        var profileCity  = ''; // don't set user city here, Please ask me if you're setting. Don't set forms are breaking.
        var fetchedCities = [];
        let showAllCity = false;
        if( (commonHelper.urlArgs(2) == URLCONSTANT.CLEANING_MAID_PAGE_URI || commonHelper.urlArgs(2) == URLCONSTANT.NOON_TV_INSTALLATION_PAGE_URI ) && this.props.isBooking){
            fetchedCities = locationHelper.fetchCities();
            showAllCity = true;
        }else{
            if(!this.props.isCurrentCityOnly) {
                fetchedCities.push(locationHelper.getLocationByName(profileCity !== '' ? profileCity : currentCity));
            }else{
               // console.log(locationHelper.getLocationByName(currentCity));

                fetchedCities.push(locationHelper.getLocationByName(currentCity));
            }
        }

        var city = [];

        var city_name = "";

        fetchedCities.map(function(item){
            city.push({id: item.id, value: item.value, label: item.name, code: item.code});
        });

        var from_emirate_value = contact_details.input_address_city;

        var from_emirate = {};

        if( Object.keys(from_emirate_value).length ){
            from_emirate = from_emirate_value;
        }else{
            if(!this.props.isCurrentCityOnly) {
                from_emirate = locationHelper.getLocationByName(profileCity !== '' ? profileCity : currentCity)
            }else{
                from_emirate = locationHelper.getLocationByName( currentCity );
            }
        }

        var from_city_id = from_emirate.id;

        var from_areas =  locationHelper.getAreasByCity(from_city_id);

        var {loginRequired} = this.state;

        var userArea = contact_details.input_address_area;

        let selectedCity = contact_details.input_address_city;

        /*if(Object.keys(selectedCity).length && showAllCity){
            let cityFound = city.filter(item => item.code == selectedCity.code);
           if(!cityFound.length){
             selectedCity = locationHelper.getLocationByName( currentCity ) ;
           }
        }*/

        return (
            <React.Fragment>
                <div className={"hidden-personal-contact-fields" + (loginRequired ? " d-none" : "")}>
                    <div className="row">
                        <div className="col-12 col-md-6 pr-sm-3 mb-3">
                            <SelectInput name="input_address_city" 
                                label={locationHelper.translate("CITY")} 
                                inputValue={selectedCity} 
                                options={city} onInputChange={this.onInputChange} />
                        </div>
                        <div className="col-12 col-md-6 pr-sm-3 mb-3">
                            <AreaSuggestionInput
                                InputType="text" areas={from_areas}
                                inputValue={userArea}
                                name="input_address_area"
                                label={locationHelper.translate("AREA")}
                                onInputChange={this.onInputChange}
                                validationClasses="required"
                                allowNonArea = {true}
                                userAreaField = {true}
                                />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12 col-md-6 pr-sm-3 mb-3">
                            <TextFloatInput 
                                InputType="text" 
                                name="input_address_area_building_name" 
                                inputValue={contact_details.input_address_area_building_name} 
                                label={locationHelper.translate("BUILDING_OR_STREET_NO")} onInputChange={this.onInputChange} 
                                validationClasses="required"/>
                        </div>
                        <div className="col-12 col-md-6 pr-sm-3 mb-3">
                            <TextFloatInput 
                                InputType="text" 
                                name="input_address_area_building_apartment" 
                                inputValue={contact_details.input_address_area_building_apartment} 
                                label={locationHelper.translate("APARTMENT_VILLA_NO")} onInputChange={this.onInputChange} 
                                validationClasses="required"/>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
    contactMoveNext(){
        //this.onInputChange
        this.props.moveNext();
    }
    render(){
        var {loginRequired,checkingEmail, isUserLoggedIn} = this.state;
        var {isLocationDetailShow, contact_details, isBooking, showDialogToChangeServiceDate} = this.props;

        var isDisabled = false;

        if(showDialogToChangeServiceDate || loginRequired){
            isDisabled = true
        }
        var buttonNextStepText = this.props.buttonNextStepText;
        var reqText = this.props.isBooking ? locationHelper.translate("WE_NEED_YOUR_CONTACT_INFORMATION_BOOKING") : locationHelper.translate("WE_NEED_YOUR_CONTACT_INFORMATION");
        var stepDesc = reqText;

        var backButtonLink = "#1";

        var {isSkipCompany} = this.props;

        if(!isBooking && typeof isSkipCompany != "undefined"){

            backButtonLink = isSkipCompany ? "#1" : "#2";
        }
        //console.log("contact_details.input_phone", contact_details.input_phone);
        return (
            <React.Fragment>
                    <section>
                        <FormTitleDescription title={locationHelper.translate("YOUR_CONTACT_DETAILS_AND_LOCATION")} desc={stepDesc} />
                    </section>
                    <section>
                        <div className="row contact-email">
                            <div className="col-12 col-md-6 pr-sm-3 mb-3">
                                <TextFloatInput
                                    InputType="email"
                                    name="input_email"
                                    label={locationHelper.translate("EMAIL")}
                                    inputValue={contact_details.input_email}
                                    validationClasses="required email"
                                    onInputChange={this.handleEmailChange} 
                                    { ...( isUserLoggedIn ? {"readOnlyInput":true} : {} )}
                                    />
                                {checkingEmail && <Loader />}
                            </div>
                            <div className="col-12 col-md-6 pr-sm-3 mb-3">
                                { loginRequired ? (
                                    <div>
                                        <p className="h3 font-weight-normal mb-0">{locationHelper.translate("WELCOME_BACK")}</p>
                                        <p className="text-secondary mb-0">
                                            Please <a onClick={()=> this.props.toggleLoginModal({visibility:true, guest:false, email:contact_details.input_email})} className="pointer font-weight-bold text-primary">sign in</a> to access your saved information and confirm your booking.</p>
                                    </div>
                                ) : (
                                <PhoneInput 
                                    name="input_phone" 
                                    inputValue={contact_details.input_phone} 
                                    onInputChange={this.onInputChange} 
                                    validationClasses="required phone" 
                                    city={this.state.city}/>
                                )}
                            </div>
                        </div>
                        <div className={"row" + (loginRequired ? " d-none" : "")}>
                            <div className="col-6 pr-sm-3 mb-3">
                                <TextFloatInput InputType="text" name="input_name" label={locationHelper.translate("FIRST_NAME")} inputValue={contact_details.input_name} onInputChange={this.onInputChange} validationClasses="required"/>
                            </div>
                            <div className="col-6 pr-sm-3 mb-3">
                                <TextFloatInput InputType="text" name="input_last_name" label={locationHelper.translate("LAST_NAME")} inputValue={contact_details.input_last_name} onInputChange={this.onInputChange} validationClasses="required"/>
                            </div>
                        </div>
                        { isLocationDetailShow == "yes" && this.locationElements()}
                        <div className={"row" + (loginRequired ? " d-none" : "")}>
                            {/* <div className="col">
                                <p><small>* We value your privacy! This information will only be used to contact you about your {reqText}.</small></p>
                            </div> */}
                        </div>
                        { this.props.backButton && <div className="row mt-2 goback">
                            <div className="col-12">
                                <a href={backButtonLink} className="text-black"> <i className="fa fa-angle-left"></i> {locationHelper.translate("BACK")}</a>
                            </div>
                        </div>}
                    </section>
                    { isBooking ? (<section>
                        <BookNextStep 
                            total={this.props.total} 
                            title={buttonNextStepText} 
                            moveNext={this.contactMoveNext} 
                            formCurrentStep={this.props.formCurrentStep} 
                            toStep={this.props.toStep} 
                            setModal={this.props.mobileSummary} 
                            disabled={isDisabled}
                            noNextStep = {this.props.noNextStep}/>
                    </section>)
                    : (<QuotesNextStep 
                            moveNext={this.contactMoveNext} 
                            formCurrentStep={this.props.formCurrentStep} 
                            disabled={isDisabled} 
                            submitData={this.props.submitData} />) }
            </React.Fragment>
        )
    }
}
ContactDetailsStep.defaultProps = {
    isCurrentCityOnly: true,
    buttonNextStepText: "Select payment method",
    couponValue: "",
    autocompleteShow: false,
    cityOptions: [],
    areasOptions: [],
    loginRequired: false,
    isBooking:false,
    backButton: true,
    noNextStep : true
};
function mapStateToProps(state){
    return {
        service_constants: state.serviceConstants,
        currentCity: state.currentCity,
        userProfile: state.userProfile,
        myCreditCardsData: state.myCreditCardsData,
        lang: state.lang
    }
}
function mapDispatchToProps(dispatch){
    return bindActionCreators({toggleLoginModal, getAllCreditCard}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(ContactDetailsStep);