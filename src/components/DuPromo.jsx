import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import { Link } from "react-router-dom";
import { withCookies } from 'react-cookie';
import TextFloatInput from "./Form_Fields/TextFloatInput";
import DatePick from "./Form_Fields/DatePick";
import {
    togglePopupPromoForm,
    submitPromo,
    isValidSection,
    isMobile
} from "../actions/index";
import Loader from "./Loader";

class DuPromo extends React.Component {

    constructor(props) {
        super(props);
        this.state = { 
            name: (typeof props.quotesMoveDetails !="undefined" && props.quotesMoveDetails.custName !="undefined") ? props.quotesMoveDetails.custName : '',
            email: (typeof props.quotesMoveDetails !="undefined" && props.quotesMoveDetails.custEmail !="undefined") ? props.quotesMoveDetails.custEmail : '',
            phone: (typeof props.quotesMoveDetails !="undefined" && props.quotesMoveDetails.custPhone !="undefined") ? props.quotesMoveDetails.custPhone : '',
            new_address: (typeof props.quotesMoveDetails !="undefined" && props.quotesMoveDetails.custAddress !="undefined") ? props.quotesMoveDetails.custAddress : '',
            move_date: (typeof props.quotesMoveDetails !="undefined" && props.quotesMoveDetails.moveDate !="undefined") ? props.quotesMoveDetails.moveDate : '',
            loading: false,
            success_booking: '',
            isMobile: false
        };

        this.promoForm = this.promoForm.bind(this);
        this.onInputChange = this.onInputChange.bind(this);
        this.showMenu = this.showMenu.bind(this);
        this.hideMenu = this.hideMenu.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        
    }
    componentDidMount() {
        this.setState({
            isMobile: isMobile()
        })
    }
    onInputChange(name, value){
        this.setState({
            [name]:value
        });
    }
    componentWillReceiveProps(newProps){
        if((newProps.showPopUp !== this.props.showPopUp) && newProps.showPopUp == true){
            this.showMenu();
        }
    }
    handleSubmit(event) {
        event.preventDefault();
        let valid = isValidSection('du-promo-form');
        if(valid){
            var data = {
                type_of_lead:'du_promo',
                name: this.state.name,
                email:this.state.email,
                phone:this.state.phone,
                new_address:this.state.new_address,
                move_date:this.state.move_date,
            }
            this.setState({
                loading: true
            })
            let success_booking = "A member of the DU team will contact you shortly to share more details about this offer.";
            submitPromo(data).then(res => {
                /*if(res == true){
                   
                }*/
                this.setState({
                    loading: false,
                    success_booking: success_booking
                })
                let thisVar = this;
                setTimeout(function(){
                    thisVar.hideMenu();
                },2000)
               
            })
        }
    }
    promoForm(){
        if(this.state.loading){
            return (<Loader />)
        }else if(this.state.success_booking != ""){
            return(<div className="promo-form mb-3">
                    <div className='text-success' style={{height:'300px'}}>
                        <p className="text-success text-center pt-5">{this.state.success_booking}</p>
                    </div>
                </div>
            );
        }else{
        return (
            <div className="promo-form mb-3">
                <form id="du-promo-form" onSubmit={this.handleSubmit}>
                    <div className="row">
                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div className="promo-form-field clearfix">
                                <div className="promo-form-input mb-2">
                                    <TextFloatInput 
                                        label="Name:"
                                        inputValue={this.state.name}
                                        name="name"
                                        autoCompleteDisabled={true}
                                        InputType="text"
                                        validationClasses="required"
                                        onInputChange={this.onInputChange} />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div className="promo-form-field clearfix">
                                <div className="promo-form-input mb-2">
                                    <TextFloatInput 
                                        label="Email:"
                                        name="email" 
                                        inputValue={this.state.email}
                                        autoCompleteDisabled={true}
                                        InputType="email"  
                                        validationClasses="required email"
                                        onInputChange={this.onInputChange} />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div className="promo-form-field clearfix">
                                <div className="promo-form-input mb-2">
                                <TextFloatInput 
                                    label="Phone:" 
                                    name="phone" 
                                    inputValue={this.state.phone}
                                    autoCompleteDisabled={true}
                                    InputType="tel" 
                                    validationClasses="required"
                                    onInputChange={this.onInputChange} />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div className="promo-form-field clearfix">
                                <div className="promo-form-input mb-2">
                                <TextFloatInput 
                                    label="Address (New):"
                                    InputType="text" 
                                    name="new_address" 
                                    inputValue={this.state.new_address}
                                    autoCompleteDisabled={true}
                                    validationClasses="required"
                                    onInputChange={this.onInputChange} />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row mbl">
                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div className="promo-form-field clearfix">
                                <div className="promo-form-input mb-2">
                                <DatePick
                                    parentClass=''
                                    childClass= 'date-container'
                                    inputValue={this.state.move_date}
                                    name="move_date"
                                    selectedDate={this.state.move_date}
                                    onInputChange={this.onInputChange}
                                    validationClasses="required"
                                    validationMessage = "Please select date"
                                    popperClassName = 'promo-date-picker'
                                    popperPlacement = 'top-end'
                                    disableFriday={false}
                                />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row my-3">
                        <div className="col-12 text-center">
                            <button className="btn btn-inline-block btn-warning py-2 text-uppercase font-weight-bold">
                                <span className="text-uppercase">Contact me for offer</span>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        );
        }
    }
    showMenu(){
        this.props.togglePopupPromoForm({visibility:true});
    }
    hideMenu() {
        //console.log("called hideMenu");
        this.props.togglePopupPromoForm({visibility:false});
    }
    render() {
        //console.log("isMobile", isMobile);
        let {isMobile} = this.state; 
        return (<div className={"modal fade open "+ (this.props.showPromoModal.visibility ? ' show': '')} id="etisalat-promo" role="dialog">
            <div className="modal-dialog modal-lg">
                <div className="modal-content">
                    <div className="modal-body p-0">
                        <button onClick={()=> this.hideMenu()} type="button" className="close-btn btn btn-inline-block btn-warning" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <div className="row">
                            <div className={isMobile ? "col-lg-6 col-md-6 col-sm-6 col-xs-12 promo-col-image mx-3 pt-1 pb-5" : "col-lg-6 col-md-6 col-sm-6 col-xs-12 promo-col-image pt-5"}>
                                { /*<img src="/dist/images/du_promo_checkout.jpg" className="d-none d-md-block mw-100 mx-auto" />
                                <img src="/dist/images/du-promo-mobile.jpg" className="d-sm-none d-md-none"/>*/ }
                                <div className={isMobile ? "text-white text-center mb-5" : "text-white mt-5 text-center"}>
                                    <div className="h1 mb-2">du Home Plans</div>
                                    <div className="h3 m-auto" style={{width:'80%'}}>Free landline calls, fast internet and premium entertainment</div>
                                </div>
                            </div>
                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12 promo-form-col">
                                <div className={isMobile ? "promo-form-row p-3" : "promo-form-row pt-3 pr-3"} >
                                    <h4 className="text-uppercase h2 font-weight-bold">Special Du Promo for <span className="text-warning">ServiceMarket</span> users!</h4>
                                    <p className="small m-0">Get 10% off for 12 months and AED 200 installation fee waived off when you switch to DU Internet</p>
                                    <ul className="list-unstyled font-weight-bold">
                                        {/*<li><i className="fa fa-check mr-2 text-info"></i>Super fast internet</li>
                                        <li><i className="fa fa-check mr-2 text-info"></i>10% off for 12 months</li>
                                        <li><i className="fa fa-check mr-2 text-info"></i>AED 200 installation fee waived off</li>*/
                                        }
                                        <li><i className="fa fa-check mr-2 text-info"></i>Premium TV with OSN Get Started</li>
                                        <li><i className="fa fa-check mr-2 text-info"></i>High speed internet</li>
                                        <li><i className="fa fa-check mr-2 text-info"></i>AED 379/month with du Home</li>
                                        
                                    </ul>
                                    <p className="small m-0">Now available all over Dubai</p>
                                    <div className="more-info small my-2">
                                        <a href="https://www.du.ae/personal/at-home/moving-to-a-new-home" target="_blank" className="text-primary"> Click for more info </a>
                                    </div>
                                    { this.promoForm() }
                                </div>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>   );
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({togglePopupPromoForm}, dispatch);
    
}
function mapStateToProps(state) {
    return {
        loader: state.loader,
        showPromoModal : state.showPromoModal,
    }
}
DuPromo.defaultProps = {
    showPopUp: false
}
export default withCookies(connect(mapStateToProps, mapDispatchToProps)(DuPromo));
//showPopUp