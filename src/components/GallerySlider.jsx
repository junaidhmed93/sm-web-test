import React from "react";
import Slider from "react-slick";
import { Link } from "react-router-dom";
import {imgixWPBase, relativeURL} from '../imgix';
import LazyLoad from 'react-lazyload';
import PlaceholderComponent from '../components/Placeholder';
import {isBotData} from "../actions";
import Lightbox from "react-images"
class GallerySlider extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
			lightboxIsOpen: false,
			currentImage: 0,
		};

		this.closeLightbox = this.closeLightbox.bind(this);
		this.gotoNext = this.gotoNext.bind(this);
		this.gotoPrevious = this.gotoPrevious.bind(this);
		this.gotoImage = this.gotoImage.bind(this);
		this.handleClickImage = this.handleClickImage.bind(this);
		this.openLightbox = this.openLightbox.bind(this);
    }
    openLightbox (index, event) {
		event.preventDefault();
		this.setState({
			currentImage: index,
			lightboxIsOpen: true,
		});
	}
	closeLightbox () {
		this.setState({
			currentImage: 0,
			lightboxIsOpen: false,
		});
	}
	gotoPrevious () {
		this.setState({
			currentImage: this.state.currentImage - 1,
		});
	}
	gotoNext () {
		this.setState({
			currentImage: this.state.currentImage + 1,
		});
	}
	gotoImage (index) {
		this.setState({
			currentImage: index,
		});
	}
	handleClickImage () {
		if (this.state.currentImage === this.props.images.length - 1) return;

		this.gotoNext();
	}
	renderItems(){
        // console.log(this.props.isBotData)
        //var i = 1;
		return this.props.images.map((item, i) => {
            return(
                <a href="javascript:void(0)" onClick={(e) => this.openLightbox(i, e)}>
                    <div key={'photo-'+i} className="item">
                        <LazyLoad scroll={false} height={121} placeholder={<PlaceholderComponent height={121} placeholderSrc={item.src} />}>
                            <div className="carousel-placeholder placeholder rect-sm border bg-light rounded mb-1" style={item.src != "" ?{background: "url("+item.src+") center/cover no-repeat"}:{}}></div>
                        </LazyLoad>
                        {/*<p className="title font-weight-bold mt-0 mb-1">{item.service_preview_title}</p> <h6>{item.service_preview_description}</h6>*/}
                    </div>
                </a>
            );
            //i++;
        });
	}
	mergeSettings(){
		var defaultSettings = { dots: false, infinite: true, speed: 500, slidesToShow: 4, slidesToScroll: 1, infinite: false, responsive: [ { breakpoint: 1199, settings: { slidesToShow: 3, arrows: false } }, { breakpoint: 767, settings: { unslick: true } } ] };
		return {...defaultSettings, ...this.props.settings};
	}
    render() {
    	var settings = this.mergeSettings();
        return (
        	<div className={'service-slider ' + this.props.parentClass}>
			    <div className={this.props.childClass}>
			        <div className="sm-slider">
						<Slider {...settings}>
							{this.renderItems()}
						</Slider>
                        <Lightbox
                            currentImage={this.state.currentImage}
                            images={this.props.images}
                            isOpen={this.state.lightboxIsOpen}
                            onClickImage={this.handleClickImage}
                            onClickNext={this.gotoNext}
                            onClickPrev={this.gotoPrevious}
                            onClickThumbnail={this.gotoImage}
                            onClose={this.closeLightbox}
                            preventScroll={this.props.preventScroll}
                            showThumbnails={this.props.showThumbnails}
                            spinner={this.props.spinner}
                            spinnerColor={this.props.spinnerColor}
                            spinnerSize={this.props.spinnerSize}
                            theme={this.props.theme}
                        />
			        </div>
			    </div>
			</div>
        );
    }
}
GallerySlider.defaultProps = {
  parentClass: 'row mb-4 md-md-5',
  childClass: 'col px-4 px-md-3 gallery',
  items: [],
  settings: {},
    isBotData: false
};


export default GallerySlider;
