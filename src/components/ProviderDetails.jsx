import React from "react";
import Loader from "./Loader";
import {fetchProvider} from "../actions/index";
import moment from 'moment';
import LazyLoad from 'react-lazyload';
import PlaceholderComponent from '../components/Placeholder';
import StarRating from "./StarRating"

class ProviderDetails extends React.Component{
    _isMounted = false;
    constructor(props) {
        super(props);  
        this.state = {
            provider: null,
            showPhoneNo: false
        };
        this.togglePhoneNo = this.togglePhoneNo.bind(this);
    }
    componentDidMount(){
        this._isMounted = true;
        fetchProvider(this.props.providerID).then((res)=> {
            if (this._isMounted) {
                this.setState({
                    provider: res
                })
            }
        });
    }

    componentWillUnmount() {
        this._isMounted = false;
    }
    getRating(rating, numberOfReviews){
        var rate = (rating / numberOfReviews).toFixed(1);
        return rate;
    }
    togglePhoneNo(){
        this.setState({
            showPhoneNo: true
        })
    }
    reviewReplies(replies = []){
        var replyData = []
        if(replies.length){
            replies.map((item) =>{
                replyData.push(
                    <div className="col ml-3 container-reply container-reply">
                        <div className="review-details">
                            <strong>{item.nameOfPerson}</strong>
                            <p className="m-0">{moment(item.dateOfReply).format("DD/MM/YYYY")}</p>
                            <div className="py-2 reply-text">{item.reply}</div>
                        </div>
                    </div>
                );
            })
        }
        return replyData;
    }
    render() {
        const {provider, showPhoneNo} = this.state;
        const {canSelect} = this.props;
        if( provider ){
            var offeredServices = []
            if(provider.offeredServices.length){
                provider.offeredServices.map((item) => {
                    if(item) {
                        offeredServices.push(item.serviceName);
                    }
                })

            }
        var showPhoneClass = "text-black border btn mr-3 text-capitalize show-phone-number telephone mrm phone-number";
        var toggleClass = "text-black border btn mr-3 show-phone-number telephone d-none mrm";


        if(showPhoneNo){
            showPhoneClass += " d-none";
            toggleClass += " d-inline-block";
        }else{
            showPhoneClass += " d-inline-block";
            toggleClass += " d-none";
        }

        //const rate = this.getRating(provider.rating ,provider.numberOfReviews)
        const rate =  !isNaN(provider.averageRating ) ? Number(( provider.averageRating ).toFixed(2) ) : provider.averageRating;
        return(
            <div className="modal-body p-2 p-md-4 desktop-company-details">
                <div className="row mb-5">
                    <div className="col-3">
                        <LazyLoad offset={500} placeholder={<PlaceholderComponent />}>
                            <div className="border">
                                <img src={provider.logo}  alt={provider.name} className="service-logo" />
                            </div>
                        </LazyLoad>
                    </div>
                    <div className="col">
                        <div className="row">
                            <div className="col">
                                <p className="h2">{provider.name}</p>
                            </div>
                            {canSelect ? (
                            <div className="col-auto mb-2 d-none d-md-block">
                                <button className="btn btn-primary" data-dismiss="modal" onClick={this.props.selectChange}>{this.props.isChecked ? 'Selected' : 'Select'}</button>
                            </div>
                            ) : ''}
                        </div>
                        <div className="row">
                            <div className="col-auto text-left">
                                <StarRating rating={rate}/>
                            </div>
                            <div className="col-auto px-0 border-right"></div>
                            <div className="col-auto text-left">
                                <p className="h3 m-0 text-primary">{(rate && rate != 'NaN')? (rate + '/5') : 'No rating available'}</p>
                                <p className="m-0">
                                    <small>{provider.numberOfReviews} Reviews</small>
                                </p>
                            </div>

                        </div>
                        { provider.contactInformation !=null && (<div className="row mt-3 contact-phone-website d-none d-md-block d-sm-block d-lg-block">
                                <div className="col-lg-12 col-md-12 hidden-xs">
                                    <a href="javascript:void(0)" className={showPhoneClass} id="show-phone" onClick={()=>{this.togglePhoneNo()}}>show phone number</a>
                                    <a className={toggleClass} id="phone-number" href={"tel:"+provider.contactInformation.phone}>
                                        <span className="field-descp telephone-number" itemprop="telephone">{provider.contactInformation.phone}</span>
                                    </a>
                                    <a target="_blank" href={"http://"+provider.website} className="text-black website-url border d-inline-block btn text-capitalize d-none">
                                        visit website
                                    </a>
                                </div>
                            </div>)
                        }

                    </div>
                </div>
                { provider.contactInformation != null && (
                    <div className="row my-3 contact-phone-website d-md-none d-sm-none d-lg-none">
                        <div className="col-6 text-center">
                            <a href="javascript:void(0)" className={showPhoneClass} id="show-phone" onClick={()=>{this.togglePhoneNo()}}>show phone number</a>
                            <a className={toggleClass} id="phone-number" href={"tel:"+provider.contactInformation.phone}>
                                <span className="field-descp telephone-number" itemprop="telephone">{provider.contactInformation.phone}</span>
                            </a>
                        </div>
                        <div className="col-6 text-center d-none">
                            <a target="_blank" href={"http://"+provider.website}
                               className="d-block text-black website-url border p-2 text-capitalize">
                                visit website
                            </a>
                        </div>
                    </div>)
                }
                <div className="row">
                    <div className="col">
                        <p className="h3 text-primary">About</p>
                        <hr />
                    </div>
                </div>
                <div className="row align-items-center mb-1">
                    <div className="col">
                        <div className="d-inline-block">
                            <i className="fa fa-building"></i>
                            <span className="d-inline-block font-weight-bold mx-2">Established </span>
                            {provider.establishedIn}
                        </div>
                        <div className="d-inline-block ml-2">
                            <i className="fa fa-user"></i>
                            <span className="d-inline-block font-weight-bold mx-2">Contact Person </span>
                            {provider.contactInformation ? provider.contactInformation.name : ''}
                        </div>
                        <div className="d-block my-3">
                            <i className="fa fa-map-marker"></i>
                            <span className="d-inline-block font-weight-bold mx-2">Location </span>
                            {provider.headOffice.addressResponse.line1 + " " + provider.headOffice.addressResponse.city + " " + provider.headOffice.addressResponse.country}
                        </div>
                        <div className="row align-items-center mb-3 d-none d-md-block">
                            <div className="col"><span className="font-weight-bold">Services offered : </span>
                                {offeredServices.toString()}
                            </div>
                        </div>
                        {provider.accreditation.length ? (
                            <div className="row align-items-center mb-1 d-none d-md-block">
                                <div className="col"><span className="font-weight-bold">Accreditations : </span>{provider.accreditation[0]["accreditation"]}</div>
                            </div>) : ''}
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <p className="h3 text-primary">Reviews ({provider.numberOfReviews})</p>
                        <hr />
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                    {provider.customerReviews.map((review, index)=> {
                        if(review.approvedReview) {
                            return (
                                <div key={'review'+index} >
                                    <div className="review">
                                        <div className="row">
                                            <div className="col-5 col-md pl-0 pl-md-3 flex-column justify-content-between">
                                                <p className="h4 text-capitalize">{review.reviewerName}</p>
                                                <StarRating rating={review.rating}/>
                                                <span className={"lh-1"}>
                                                    {(review.rating && review.rating != 'NaN') ? (review.rating + '/5') : 'No rating available'}
                                                </span>
                                            </div>
                                            <div className="col-5 col-md d-flex flex-column justify-content-center">
                                                <p className="h4 m-0">{review.serviceName}</p>
                                                <p className="m-0">{moment(review.creationDate).format("DD/MM/YYYY")}</p>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col">
                                                <p className="h4  my-3 font-weight-bold">{'"'+review.approvedTitle+'"'}</p>
                                                {review.approvedReview ? (<div className="m-0">{review.approvedReview}</div>) : ''}
                                            </div>
                                        </div>
                                    </div>
                                    { review.reviewRepliesDtos.length !=0 && <div className="row reply-container">{ this.reviewReplies(review.reviewRepliesDtos) }</div> }
                                    <hr />
                                </div>
                            )
                        }

                    })}
                    </div>
                </div>
            </div>
        )
        }else{
            return(
                <Loader />
            )
        }
    }
}
/*
 {provider.accreditation.length ? (
 <div className="row">
 <div className="col">
 <p className="h3 text-primary">Accreditations</p>
 <hr />
 </div>
 </div>
 ) : ''}
 {provider.accreditation.length ? (
 <div className="row align-items-center mb-1 d-none d-md-block">
 <div className="col"><span className="font-weight-bold">Accreditations : </span>{provider.accreditation[0]["accreditation"]}</div>
 </div>) : ''}
 {provider.offeredServices.length ? (
 <div className="row">
 <div className="col">
 <p className="h3 text-primary">Services Offered</p>
 <hr />
 </div>
 </div>
 ) : ''}
 */
ProviderDetails.defaultProps = {
    canSelect: true
};

export default ProviderDetails;
