import React from "react";
import {
    fetchCustomerBookingHistory,
    cancelBookingRequest,
    fetchCancelingReason,
    unsubscribeBookingRequest,
    freezeBookingRequest,
    changeBookingRequest,
    newChangeBookingRequest,
    cancelBookingEventRequest,
    isValidSection, 
    fetchDateTime,
    fetchZohoMigratedService,
    fetchDataDictionaryValues,
    fetchDataConstantValues,
    fetchAllCities
} from "../actions";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import Loader from "../components/Loader";
import LazyLoad from 'react-lazyload';
import PlaceholderComponent from '../components/Placeholder';
import { imgixReactBase, relativeURL } from "../imgix";
import commonHelper from "../helpers/commonHelper";
import locationHelper from "../helpers/locationHelper";
import GeneralModal from "./GeneralModal";
import TextFloatInput from "./Form_Fields/TextFloatInput";
import DatePick from "./Form_Fields/DatePick";
import SelectInput from "./Form_Fields/SelectInput";
import CleaningTimePicker from "./Form_Fields/CleaningTimePicker";
import CleaningDatePick from "./Form_Fields/CleaningDatePick";
import TimePicker from "./Form_Fields/TimePicker";
import moment from "moment";
import StarRating from "./StarRating";
import stringConstants from '../constants/stringConstants';
let bookingEventStatusDataConstants = {};
let bookingStatusDataConstants = {};
let changeRequestDataConstants = {};
let dataValuesbookingEventStatus = {};
let dataValuesbookingStatus = {};
let dataValueschangeRequest = {};

class MyBookings extends React.Component {
    _isMounted = false;

    constructor(props) {
        super(props);
        this.locationHelperInit = locationHelper.init.bind(this);
        this.locationHelperInit();
        this.state = {
            loader: false,
            moreDetails: -1,
            showModal: false,
            booking_date: '',
            booking_time: '',
            showFreezeModal: false,
            cancelLoader: false,
            cancellationReason: -1,
            freezeDuration: 1,
            selectedEditOption: { value: null },
            selectedNewDuration: { value: null },
            selectedNumberOfCleaners: { value: null },
            other: '',
            editDateOfService: '',
            job_time: '',
            freezeStartDate: '',
            bookToCancel: {},
            bookToFreeze: {},
            bookToEdit: {},
            bookToUnsubscribe: {},
            showEventReasonsModal: false,
            hours_required: 1,
            field_service_needed: false,
            number_of_cleaners: 2,
            field_multiple_times_week: 3,
            isSubscription: true,
            startDate: props.dateAndTime,
            dropOff: { value: null },
        }
        this.fetchDataIfNeeded = this.fetchDataIfNeeded.bind(this);
        
        this.cancelRequest = this.cancelRequest.bind(this);
        this.setModal = this.setModal.bind(this);
        this.setErrorModal = this.setErrorModal.bind(this);
        this.setMessageErrorModal = this.setMessageErrorModal.bind(this);
        this.setEventReasonsModal = this.setEventReasonsModal.bind(this);
        this.setFreezeModal = this.setFreezeModal.bind(this);
        this.setUnsubscribeModal = this.setUnsubscribeModal.bind(this);
        this.setEditModal = this.setEditModal.bind(this);
        this.setEditAllModal = this.setEditAllModal.bind(this);
        this.onInputChange = this.onInputChange.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.isChecked = this.isChecked.bind(this);
        this.bookDetailsFirstSection = this.bookDetailsFirstSection.bind(this);
        this.bookDetailsSecondSection = this.bookDetailsSecondSection.bind(this);
        this.upcomingBookDetailsFirstSection = this.upcomingBookDetailsFirstSection.bind(this);
        this.upcomingBookDetailsSecondSection = this.upcomingBookDetailsSecondSection.bind(this);
        this.isZohoMigratedServices = this.isZohoMigratedServices.bind(this);
        this.itemDetail = this.itemDetail.bind(this);
        this.mattressDetails = this.mattressDetails.bind(this);
        this.renderHistoryBookings = this.renderHistoryBookings.bind(this);
        //this.fetchDataIfNeeded(props);

    }
    fetchDataIfNeeded(props){
        this._isMounted = true;
        var lang = props.lang;
        var promiseFetch = [];
        let city = this.props.currentCity;
        const citiesPromise =  this.props.fetchAllCities(city, lang, false);
        promiseFetch.push(citiesPromise);
        const fetchDataDictionaryValues = props.fetchDataDictionaryValues(lang);
        promiseFetch.push(fetchDataDictionaryValues);
        const fetchDataConstantValues = props.fetchDataConstantValues(lang);
        promiseFetch.push(fetchDataConstantValues);
        const currentDateTime = props.fetchDateTime();
        promiseFetch.push(currentDateTime);
        const zohoMigratedService = props.fetchZohoMigratedService(lang);
        promiseFetch.push(zohoMigratedService);
        const fetchCustomerBookingHistory = props.fetchCustomerBookingHistory(props.signInDetails.access_token, lang);
        promiseFetch.push(fetchCustomerBookingHistory);
        
        Promise.all(promiseFetch).then(() => {
            if (this._isMounted) {
                this.setState({
                    loader: false
                })
            }
        }).catch(function (error) {
            console.log("Error fetchDataIfNeeded", error);
        });
        this.setState({
            loader: true
        });
        
    }
    componentWillReceiveProps(newProps) {
        if (newProps.zohoMigratedServices !== this.props.zohoMigratedServices) {
            window.REDUX_DATA["zohoMigratedServices"] = newProps.zohoMigratedServices;
        }
        if (newProps.dataConstants !== this.props.dataConstants) {
            window.REDUX_DATA["dataConstants"] = newProps.dataConstants;
            let {dataConstants} = newProps;
            //console.log("dataConstants", dataConstants);
            bookingEventStatusDataConstants = dataConstants["BOOKING_EVENT_STATUS"];
            bookingStatusDataConstants = dataConstants["BOOKING_STATUS"];
            changeRequestDataConstants = dataConstants["CHANGE_REQUEST_INITIATED_BY"];
            dataValuesbookingEventStatus = commonHelper.processDataValues(bookingEventStatusDataConstants);
            dataValuesbookingStatus = commonHelper.processDataValues(bookingStatusDataConstants);
            dataValueschangeRequest = commonHelper.processDataValues(changeRequestDataConstants);
        }
    }
    componentDidMount() {
        this._isMounted = true;
        let {lang, fetchCustomerBookingHistory} = this.props;
        this.fetchDataIfNeeded(this.props);
        /*fetchCustomerBookingHistory(this.props.signInDetails.access_token, lang)
            .then(() => {
                if (this._isMounted) {
                    this.setState({
                        loader: false
                    });
                }
            });
        this.setState({
            loader: true
        });*/

        
        this.interval = setInterval(() => commonHelper.getCurrentTime(this), 20000);
        // console.log(this.props.service_constants);
    }
    componentWillUnmount() {
        this._isMounted = false;
    }
    openCancelingReasons(book) {
        this.setState({
            cancelLoader: true,
            bookToCancel: book
        });
        fetchCancelingReason(this.props.signInDetails.access_token, book.service.id, book.subscription ? "RECURRING_BOOKING_CANCELLATION" : "ONCE_BOOKING_CANCELLATION", "BOOKING",this.props.lang)
            .then((res) => {
                var reasons = [];
                res.data.map((reasonsGroup, id) => {
                    if (reasonsGroup.categoryName == 'Customer Causes') {
                        reasons = reasonsGroup.feedbackReasons;
                    }
                });
                this.setState({
                    reasons: reasons,
                    cancelLoader: false
                });
            });
        this.setModal(true);
    }
    openEventCancelingReasons(book) {
        this.setState({
            cancelLoader: true,
            bookToCancel: book
        });
        fetchCancelingReason(this.props.signInDetails.access_token, book.service.id, book.subscription ? "RECURRING_EVENT_CANCELLATION" : "ONCE_EVENT_CANCELLATION", "EVENT")
            .then((res) => {
                var reasons = [];
                res.data.map((reasonsGroup, id) => {
                    if (reasonsGroup.categoryName == 'Customer Causes') {
                        reasons = reasonsGroup.feedbackReasons;
                    }
                });
                this.setState({
                    reasons: reasons,
                    cancelLoader: false
                });
            });
        this.setEventReasonsModal(true);
    }
    openFreezeModal(book) {
        this.setState({
            bookToFreeze: book
        });
        this.setFreezeModal(true);
    }
    openUnsubscribeModal(book) {
        this.setState({
            bookToUnsubscribe: book
        });
        this.setUnsubscribeModal(true);
    }
    openEditModal(book) {
        this.setState({
            bookToEdit: book
        });
        this.setEditModal(true);
    }
    openEditAllModal(book) {
        this.setState({
            bookToEdit: book
        });
        this.setEditAllModal(true);
    }

    cancelRequest() {
        var book = this.state.bookToCancel;

        let feedbackReason = (this.state.reasons.length !== 0) ? this.state.reasons[this.state.cancellationReason].value : '';

        let feedbackType = book.subscription ? "RECURRING_BOOKING_CANCELLATION" : "ONCE_BOOKING_CANCELLATION";

        let feedbackReasonId = (this.state.reasons.length !== 0) ? this.state.reasons[this.state.cancellationReason].id : 0;

        let feedbackDto = {
            "feedbackReason": feedbackReason,
            "feedbackReasonId": feedbackReasonId,
            "feedbackSource": "CUSTOMER_WEB",
            "feedbackType": feedbackType,
            "id": book.bookingId
        }
        //console.log(this.state.reasons[this.state.cancellationReason], feedbackDto);

        //return false;
        this.setState({
            cancelLoader: true,
            other: '',
        })

        let signedInUserId = this.props.signInDetails && this.props.signInDetails.userId ? this.props.signInDetails.userId : 8;

        this.props.cancelBookingRequest(this.props.signInDetails.customer.id, this.props.signInDetails.access_token, book.bookingId, feedbackReason, this.state.other, dataValuesbookingStatus.DATA_VALUE_BOOKING_STATUS_CANCELLED, feedbackDto, signedInUserId)
            .then((res) => {
                this.setState({
                    cancelLoader: false
                });
                if (!res.success) {
                    this.setMessageErrorModal(stringConstants.CANCEL_BOOKING_FAIL_TXT);
                }
                else if (res.success) {
                    this.setMessageErrorModal(stringConstants.CANCEL_BOOKING_SUCCESS_TXT);
                }
                this.setModal(false);
            }).catch(() => {
                this.setState({
                    cancelLoader: false
                });
                this.setErrorModal(true);
                this.setModal(false);
            });

    }
    cancelEventRequest() {
        var book = this.state.bookToCancel;
        // console.log('events');
        let feedbackReason = (this.state.reasons.length !== 0) ? this.state.reasons[this.state.cancellationReason].value : '';

        let feedbackType = book.subscription ? "RECURRING_EVENT_CANCELLATION" : "ONCE_EVENT_CANCELLATION";

        let feedbackReasonId = (this.state.reasons.length !== 0) ? this.state.reasons[this.state.cancellationReason].id : 0;

        let feedbackDto = {
            "feedbackReason": feedbackReason,
            "feedbackReasonId": feedbackReasonId,
            "feedbackSource": "CUSTOMER_WEB",
            "feedbackType": feedbackType,
            "id": book.bookingEventId
        }
        this.setState({
            cancelLoader: true
        });

        this.props.cancelBookingEventRequest(this.props.signInDetails.customer.id, this.props.signInDetails.access_token, book.bookingEventId, (this.state.reasons.length !== 0) ? this.state.reasons[this.state.cancellationReason].value : '', this.state.other, dataValuesbookingEventStatus.DATA_VALUE_BOOKING_EVENT_STATUS_CANCELLED, feedbackDto)
            .then((res) => {
                console.log("cancelBookingEventRequest", res);
                this.setState({
                    cancelLoader: false
                });
                if (!res.success) {
                    this.setMessageErrorModal(res.error);
                }
                this.setErrorModal(false);
                this.setEventReasonsModal(false);
            }).catch(() => {
                this.setState({
                    cancelLoader: false
                })
                this.setErrorModal(true);
                this.setEventReasonsModal(false);
            });
    }
    freezeRequest() {
        var book = this.state.bookToFreeze;
        if (isValidSection('freeze-booking-form')) {
            var endDate = moment(this.state.freezeStartDate, 'DD-MM-YYYY').add(this.state.freezeDuration, 'M').format('YYYY-MM-DD');
            this.props.freezeBookingRequest(this.props.signInDetails.access_token, book.bookingId, moment(this.state.freezeStartDate, 'DD-MM-YYYY').format('YYYY-MM-DD'), endDate)
                .then((res) => {
                    this.setState({
                        cancelLoader: false
                    });
                    var errormsg = res.error;
                    if (!res.error && res.errors && res.errors.length) {
                        errormsg = res.errors[0].errorMessage;
                    }
                    if (!res.success) this.setMessageErrorModal(errormsg);
                    this.setFreezeModal(false);
                }).catch(() => {
                    this.setState({
                        cancelLoader: false
                    })
                    this.setErrorModal(true);
                    this.setFreezeModal(false);
                });
            this.setState({
                cancelLoader: true
            })
        }

    }
    editBookingRequest(book) {
        var book = this.state.bookToEdit;

        // newDate, newTime, newDuration, newNumberOfWorkers
        // selectedNewDuration, selectedNumberOfCleaners, editDateOfService, editBookingTime
        if (isValidSection('edit-booking-form')) {
            if(book.service.serviceCode == "LDR"){
                let {editDateOfService, dropOff, job_time, selectedEditOption} = this.state;
                let newRequest = {
                    booking_data:{}
                };
                if(selectedEditOption.id == 1){
                    if(editDateOfService != ''){
                        newRequest["booking_data"]["start_date"]=moment(editDateOfService, 'DD-MM-YYYY').format('YYYY-MM-DD');
                    }
                    if(job_time != ''){
                        newRequest["booking_data"]["time"]= job_time.value + ":00"
                    }
                }
                if(Object.keys(dropOff).length && selectedEditOption.id == 2){
                    newRequest["booking_data"]["service_detail"] = {};
                    newRequest["booking_data"]["service_detail"]["delivery_option"] = dropOff.value;
                }

                let changeRequest ={
                    "booking_id": book.bookingId,
                    "new_data": newRequest,
                    "service_code": "LDR",
                    "creator_comment": "",
                    "requested_by": "CUSTOMER",
                    "apply_on_future_events": true
                }

                //console.log("newRequest", newRequest, changeRequest);
                
                this.props.newChangeBookingRequest(
                    this.props.signInDetails.access_token,
                    this.props.signInDetails.customer.id,
                    changeRequest
                ).then((res) => {
                    this.setState({
                        cancelLoader: false
                    });
                    if (!res.success) this.setMessageErrorModal(res.error);
                    this.setEditModal(false);
                }).catch(() => {
                    this.setState({
                        cancelLoader: false
                    })
                    this.setErrorModal(true);
                    this.setEditModal(false);
                });
            this.setState({
                cancelLoader: true
            });
            }else{
                this.props.changeBookingRequest(
                    this.props.signInDetails.access_token,
                    book.bookingId,
                    this.props.signInDetails.customer.id,
                    book.bookingEventId,
                    this.state.selectedEditOption.id,
                    moment(this.state.editDateOfService, 'DD-MM-YYYY').format('YYYY-MM-DD'),
                    this.state.job_time,
                    this.state.selectedNewDuration.value,
                    this.state.selectedNumberOfCleaners.value,
                    dataValueschangeRequest.DATA_VALUE_CHANGE_REQUEST_INITIATED_BY_CUSTOMER
                )
                    .then((res) => {
                        this.setState({
                            cancelLoader: false
                        });
                        if (!res.success) this.setMessageErrorModal(res.error);
                        this.setEditModal(false);
                    }).catch(() => {
                        this.setState({
                            cancelLoader: false
                        })
                        this.setErrorModal(true);
                        this.setEditModal(false);
                    });
                this.setState({
                    cancelLoader: true
                })
            }
    }

    }
    editAllBookingRequest() {
        var book = this.state.bookToEdit;
        if (isValidSection('edit-all-booking-form')) {
            this.props.changeBookingRequest(
                this.props.signInDetails.access_token,
                book.bookingId,
                this.props.signInDetails.customer.id,
                '',
                this.state.selectedEditOption.id,
                moment(this.state.editDateOfService, 'DD-MM-YYYY').format('YYYY-MM-DD'),
                this.state.job_time,
                this.state.selectedNewDuration.value,
                this.state.selectedNumberOfCleaners.value,
                dataValueschangeRequest.DATA_VALUE_CHANGE_REQUEST_INITIATED_BY_CUSTOMER
            )
                .then((res) => {
                    this.setState({
                        cancelLoader: false
                    });
                    if (!res.success) this.setMessageErrorModal(res.error);
                    this.setEditAllModal(false);
                }).catch(() => {
                    this.setState({
                        cancelLoader: false
                    })
                    this.setErrorModal(true);
                    this.setEditAllModal(false);
                });
            this.setState({
                cancelLoader: true
            })
        }
    }
    unsubscribeRequest(book) {
        // console.log('zzzzz unsubscribe');
        // console.log(book.bookingId);
        this.props.unsubscribeBookingRequest(this.props.signInDetails.access_token, book.bookingId)
            .then((res) => {
                this.setState({
                    cancelLoader: false
                });

                var errormsg = res.error;
                if (!res.error && res.errors && res.errors.length) {
                    errormsg = res.errors[0].errorMessage;
                }
                if (!res.success) this.setMessageErrorModal(errormsg);
                this.setUnsubscribeModal(false);
            }).catch(() => {
                this.setState({
                    cancelLoader: false
                })
                this.setErrorModal(true);
                this.setUnsubscribeModal(false);
            });
        this.setState({
            cancelLoader: true
        })
    }
    moreDetails(bookingEventId) {
        if (this.state.moreDetails == bookingEventId) {
            this.setState({
                moreDetails: -1
            });
        } else {
            this.setState({
                moreDetails: bookingEventId
            });
        }
    }
    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }
    onInputChange(name, value) {
        // console.log(name);
        // console.log(value);
        if (name == 'editBookingTime') {
            console.log(value);
        }
        this.setState({
            [name]: value
        });
    }
    isChecked(name, value) {
        return this.state[name] == value;
    }
    renderEditField() {
        
        //booking_date:book.date, booking_time:book.time
        if(this.state.selectedEditOption.id == 1 && (typeof this.state.selectedEditOption.service != "undefined" && this.state.selectedEditOption.service == "LDR") ){
            let {selectedEditOption} = this.state;
            return (<React.Fragment>
                    <div id="dateTimeOfService" className="collapse show">
                        <p className="mb-0 font-weight-bold">Time of pickup</p>
                        <p className="mbl small">Old Pickup time: 
                            <span> {moment(selectedEditOption.booking_date, 'YYYY-MM-DD').format('DD-MM-YYYY')+", "+moment(selectedEditOption.booking_date + " " + selectedEditOption.booking_time, 'YYYY-MM-DD h:mm:ss a').format('h A')}</span>
                        </p>
                        <div className="mb-3 row">
                            <DatePick 
                                name="editDateOfService" 
                                validationMessage=" " 
                                inputValue={this.state.editDateOfService} 
                                childClass={"col-12 date-container"} 
                                onInputChange={this.onInputChange} 
                                validationClasses="required"
                                disableFriday={false} />
                        </div>
                        <div className="mb-3 row">
                            <TimePicker 
                                name="job_time" 
                                validationClasses="required" 
                                validationMessage=" " 
                                inputValue={this.state.job_time} 
                                childClass={"col-12"} 
                                booking_date={this.state.editDateOfService}
                                onInputChange={this.onInputChange} />
                        </div>
                    </div>
                </React.Fragment>);
        }
        else if(this.state.selectedEditOption.id == 2 && (typeof this.state.selectedEditOption.service != "undefined" && this.state.selectedEditOption.service == "LDR") ){
            let {selectedEditOption} = this.state;
            let dropOffOptions = [
                { id: 'free', value: 48, label: "Standard (48 hours) - Free Delivery"},
                { id: 'express', value: 24, label: "Express (24 hours) + AED 50"}
            ];
            return (<React.Fragment>
                    <div id="dateTimeOfService" className="collapse show">
                        <p className="mb-0 font-weight-bold">Select your delivery preference</p>
                        <p className="mbl small">Old delivery preference: 
                            { selectedEditOption.selectedDropOff == 48 ? 'Standard (48 hours) - Free Delivery' : 'Express (24 hours)'}
                        </p>
                        <div>
                            <SelectInput 
                                name="dropOff" 
                                inputValue={this.state.dropOff} 
                                label={locationHelper.translate('PLEASE_SELECT')} 
                                options={dropOffOptions} 
                                onInputChange={this.onInputChange} />
                        </div>
                    </div>
                </React.Fragment>);
        }
        else if (this.state.selectedEditOption.id == 1) {
            return (
                <div id="dateOfService" className="collapse show">
                    <p className="mbl font-weight-bold">{locationHelper.translate('SELECT_NEW_DATE')}</p>
                    <DatePick 
                        name="editDateOfService" 
                        validationMessage=" " 
                        inputValue={this.state.editDateOfService} 
                        childClass={"col-12 date-container"} 
                        onInputChange={this.onInputChange} 
                        validationClasses="required"
                         />
                </div>
            );
        } else if (this.state.selectedEditOption.id == 2) {
            var newDurationOptions = [{ 'id': 1, value: 2, label: locationHelper.translate('TWO_HOURS') }, { 'id': 2, value: 3, label:locationHelper.translate('THREE_HOURS') }, { 'id': 3, value: 4, label: locationHelper.translate('FOUR_HOURS') }, { 'id': 4, value: 5, label: locationHelper.translate('FIVE_HOURS') }, { 'id': 5, value: 6, label: locationHelper.translate('SIX_HOURS') }, { 'id': 6, value: 7, label: locationHelper.translate('SEVEN_HOURS') }, { 'id': 7, value: 8, label: locationHelper.translate('EIGHT_HOURS') }];
            var numOfCleanersOptions = [{ 'id': 1, value: 1, label: locationHelper.translate('ONE_CLEANER') }, { 'id': 2, value: 2, label: locationHelper.translate('TWO_CLEANER') }, { 'id': 3, value: 3, label: locationHelper.translate('THREE_CLEANER') }, { 'id': 4, value: 4, label: locationHelper.translate('FOUR_CLEANER') }];
            return (
                <div>
                    <div id="newDuration" className="collapse show">
                        <p className="mbl font-weight-bold">{locationHelper.translate('SELECT_DURATION')}</p>
                        <div className="col-12">
                            <SelectInput name="selectedNewDuration" 
                            inputValue={this.state.selectedNewDuration} 
                            label={locationHelper.translate('PLEASE_SELECT')} 
                            options={newDurationOptions} 
                            onInputChange={this.onInputChange} />
                        </div>
                    </div>
                    <div id="numberOfCleaners" className="collapse show">
                        <p className="mbl font-weight-bold">{locationHelper.translate('HOW_MANY_CLEANERS')}</p>
                        <div className="col-12">
                            <SelectInput name="selectedNumberOfCleaners" inputValue={this.state.selectedNumberOfCleaners} label={locationHelper.translate('PLEASE_SELECT')} options={numOfCleanersOptions} onInputChange={this.onInputChange} />
                        </div>
                    </div>
                </div>
            );
        } else if (this.state.selectedEditOption.id == 3) {
            return (
                <div id="timeOfService" className="collapse show">
                    <p className="mbl font-weight-bold">Select a new time of service</p>
                    <TimePicker validationClasses="required" validationMessage=" " inputValue={this.state.job_time} childClass={"col-12"} onInputChange={this.onInputChange} />
                </div>
            );
        }
    }
    renderReasons() {
        return this.state.reasons.map((reason, index) => {
            return (
                <div className="cancellation-reason-radio clearfix">
                    <input type="radio" id={reason.id} name={"cancellationReason"} value={index} onChange={this.handleChange} checked={this.isChecked("cancellationReason", index)} />
                    <label className="lbl pl-2" htmlFor={reason.id}>{reason.value}</label>
                </div>
            )
        });
    }
    renderOtherInput() {
        if ((this.state.reasons.length === 0) || (this.state.cancellationReason != -1 && this.state.reasons[this.state.cancellationReason].value == 'Other')) {
            return (
                <div>
                    <TextFloatInput InputType="text" name="other"
                        label="Any other comments you would like to include?" validation={{ required: 'required' }}
                        inputValue={this.state.other} onInputChange={this.onInputChange} />
                </div>
            )
        }
    }
    isCanceButtonShow(book){
        /*BOOKING_EVENT_STATUS_IN_PROGRESS
        BOOKING_EVENT_STATUS_PENDING_IN_PROGRESS*/
        let showPrice = true;
        if(book.service.serviceCode == "LDR"){
            /*if(book.uuid == "20190523000017LDRDXB"){
                console.log("book.status", book.status, book.uuid, book);
            }*/
            
            showPrice = (book.status == "In Progress" || book.status == "Pending In Progress" || book.status == "Booking Event Pending In Progress" || book.status == "Booking Event Pending Delivery Confirmation" );
        }

        //return ( book.service.serviceCode == "LDR" && ( book.status != "Pending Booking Confirmation" || book.status == "Change Requested" ) ) ? true : false;
        return showPrice;
    }
    renderNewBookings() {
        let currencyText = "";
        let showPrice = true;
        if (this.props.myBookingsData.newBookings.length > 0) {
            return this.props.myBookingsData.newBookings.map((book, index) => {
                //console.log("book", book);
                
                if (typeof book.bookingPrice != "undefined" && book.bookingPrice != null ) {
                    currencyText = book.bookingPrice.currency;
                }
                if (typeof book.bookingEventPrice != "undefined" && book.bookingEventPrice != null) {
                    currencyText = book.bookingEventPrice.currency;
                }
                showPrice = this.isCanceButtonShow(book);
                return (
                    <div className="card mb-4">
                        <div className="card-body">
                            <div className="row">
                                <div className="col-md-3 text-center">
                                    <LazyLoad offset={500} height={300} placeholder={<PlaceholderComponent />}>
                                        <img src={"/dist/images/services_pics/" + book.service.id + ".jpg"} height="219" />
                                    </LazyLoad>
                                </div>
                                <div className="col">
                                    <div className="row">
                                        <div className={showPrice ? "col-md-9" : "col-md-8"}>
                                            <strong>{!this.isZohoMigratedServices(book) ? book.frequency + " " : ""}{(book.subscription ? ' Subscription ' : ' ') + book.service.name}</strong>
                                            <p className="m-0">{locationHelper.translate('ID')}#{book.uuid}</p>
                                        </div>
                                        { showPrice ? (<div className="col-md-3">
                                            <p className="text-center"><strong>{locationHelper.translate('TOTAL_PRICE')}</strong></p>
                                            <p className="text-center">
                                                {`${book.bookingPrice.totalNetPrice ? book.bookingPrice.totalNetPrice : book.grossPayment}  ${currencyText}`}

                                            </p>
                                        </div>) : <div className="col-md-4">
                                                    <p className="text-center m-0">Price confirmed after pickup.</p></div>}
                                    </div>
                                    <div className="row">
                                        <div className="col-md-4 offset-md-8 align-self-center collapse show mt-2">
                                            <button className="btn btn-grey btn-block text-white text-uppercase" type="button"
                                                onClick={() => this.openCancelingReasons(book)}>{locationHelper.translate('CANCEL_BOOKING')}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <hr style={{ width: "100%" }} />
                                <div className={"col-12 more-details"} onClick={() => this.moreDetails(book.bookingId)}>
                                    <span className={"title"}><strong>{locationHelper.translate('DETAILS')}</strong></span>
                                    <strong><a href="javascript:void(0)" className={"right dropdown-toggle"} aria-expanded={this.state.moreDetails == book.bookingId}></a></strong>
                                </div>
                                <div className={"col-12 details collapse " + (this.state.moreDetails == book.bookingId ? 'show' : '')}>
                                    <div className="row">
                                        <div className="col-md-6 col-sm-6 col-xs-6 pr-md-5">
                                            {this.bookDetailsFirstSection(book)}
                                        </div>
                                        <div className="col-md-6 col-sm-6 col-xs-6">
                                            {this.bookDetailsSecondSection(book)}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                );
            });
        }
    }
    itemDetail(label, value) {
        if (label == "pillow") {
            label = "Pillow/ Cushions";
        } else if (label == "king_mattress") {
            label = "King Mattress";
        } else if (label == "queen_mattress") {
            label = "Queen Mattress";
        } else if (label == "single_mattress") {
            label = "Single Mattress";
        } else {
            label = label;
        }
        return (<div className="card-detail-item pbm pln text-capitalize">
            {label}
            <span> {value} </span>
        </div>)
    }
    mattressDetails(book) {
        if (typeof book.serviceDetails.no_of_king_mattress_for_shampoo_cleaning != "undefined") {
            let mattressDetails = book.serviceDetails;
            //console.log("mattressDetails", mattressDetails);
            let keys = [
                'king_mattress',
                'pillow',
                'queen_mattress',
                'single_mattress'
            ];
            let shampooCleaningItems = [];
            let itemDetail = "";
            keys.map((key) => {
                if (mattressDetails['no_of_' + key + '_for_shampoo_cleaning'] != 0) {
                    itemDetail = this.itemDetail(key, mattressDetails['no_of_' + key + '_for_shampoo_cleaning']);
                    shampooCleaningItems.push(itemDetail);
                }
            })
            let steamCleaningItems = [];
            keys.map((key) => {
                if (mattressDetails['no_of_' + key + '_for_steam_cleaning'] != 0) {
                    itemDetail = this.itemDetail(key, mattressDetails['no_of_' + key + '_for_steam_cleaning']);
                    steamCleaningItems.push(itemDetail);
                }
            })
            return (<React.Fragment>
                {shampooCleaningItems.length ? (<div className="card-detail-item pbm pln">
                    <div className="font-weight-bold">{locationHelper.translate('SHAMPOO_CLEANING')}</div>
                    {shampooCleaningItems.map((item) => {
                        return item;
                    })}
                </div>) : ""}
                {steamCleaningItems.length ? (<div className="card-detail-item pbm pln">
                    <div className="font-weight-bold">{locationHelper.translate('STEAM_CLEANING')}</div>
                    {steamCleaningItems.map((item) => {
                        return item;
                    })}
                </div>) : ""}
            </React.Fragment>);
        }
    }
    zohoMigratedBookingDetails(book) {
        var details = '';
        let key_label = "";
        /*if(book.service.serviceCode == "LDR"){
            console.log("book.serviceDetails", book);
        }*/
        let currencyText = "AED";

        if (typeof book.bookingPrice != "undefined" && book.bookingPrice != null ) {
            currencyText = book.bookingPrice.currency;
        }
        if (typeof book.bookingEventPrice != "undefined" && book.bookingEventPrice != null) {
            currencyText = book.bookingEventPrice.currency;
        }
        
        if (typeof book.serviceDetails.tv_size_in_inches != "undefined") {
            let tvSize = book.serviceDetails.tv_size_in_inches;
            if (tvSize == '1-54') {
                key_label = "Below 55 inches";
            } else if (tvSize == '55-64') {
                key_label = "55 to 65 inches";
            }
            else {
                key_label = "Above 65 inches";
            }
        }
        let sofaDetails = [];
        let itemDetail = "";
        if (typeof book.serviceDetails.no_of_seat_for_steam_cleaning != "undefined") {
            if (book.serviceDetails.no_of_seat_for_shampoo_cleaning) {
                itemDetail = this.itemDetail("Shampoo Cleaning No.of Sofa Seats", book.serviceDetails.no_of_seat_for_shampoo_cleaning);
                sofaDetails.push(itemDetail);
            }
            if (book.serviceDetails.no_of_seat_for_steam_cleaning) {
                itemDetail = this.itemDetail("Steam Cleaning No.of Sofa Seats", book.serviceDetails.no_of_seat_for_steam_cleaning);
                sofaDetails.push(itemDetail);
            }
        }
        
        //console.log("book.serviceDetails",book, book.serviceDetails);
        let scrubbingDetails = "";
        let steamCleaningDetails = "";
        let deepCleaningFurnishedDetails = "";
        //Grout cleaning & tile scrubbing
        if(typeof book.serviceDetails.steam_cleaning != "undefined" && book.serviceDetails.steam_cleaning){
            steamCleaningDetails = (<div className="card-detail-item pbm pln">
                Grout & Tile Scrubbing
            <span>{locationHelper.translate('YES')}</span>
            </div>)
        }
        if(typeof book.serviceDetails.scrubbing != "undefined" && book.serviceDetails.scrubbing){
            scrubbingDetails = (<div className="card-detail-item pbm pln">
                Steam Cleaning
                <span>{locationHelper.translate('YES')}</span>
            </div>)
        }
        if(typeof book.serviceDetails.is_home_furnished != "undefined" && book.serviceDetails.is_home_furnished){
            deepCleaningFurnishedDetails = (<div className="card-detail-item pbm pln">
                Furnished
                <span>{locationHelper.translate('YES')}</span>
            </div>)
        }

        details = (
            <div className="card-detail-items">
                { book.service.serviceCode != "LDR" ? (<React.Fragment>
                <div className="card-detail-item pbm pln">
                    {locationHelper.translate('SERVICE_DATE')}
                    <span> {moment(book.date, 'YYYY-MM-DD').format('DD-MM-YYYY')}</span>
                </div>
                {(typeof book.time != "undefined" && (book.time != "" && book.time != null)) && (
                    <div className="card-detail-item pbm pln">{locationHelper.translate('TIME')}
                        <span> {moment(book.date + " " + book.time, 'YYYY-MM-DD h:mm:ss a').format('h:mm A')}</span>
                    </div>)}
                {typeof book.serviceDetails.no_of_rooms != "undefined" && (<div className="card-detail-item pbm pln text-capitalize">{locationHelper.translate('HOME_SIZE')}
                    <span> {book.serviceDetails.no_of_rooms == 0 ? "Studio " : book.serviceDetails.no_of_rooms + "BR "} {book.serviceDetails.property_type} </span>
                </div>)
                }
                {steamCleaningDetails}
                {scrubbingDetails}
                {deepCleaningFurnishedDetails}
                </React.Fragment>) : (<React.Fragment>
                        <div className="card-detail-item pbm pln">
                            Pickup time 
                            <span> {moment(book.date, 'YYYY-MM-DD').format('DD-MM-YYYY')+", "+moment(book.date + " " + book.time, 'YYYY-MM-DD h:mm:ss a').format('h A')}</span>
                        </div>
                        <div className="card-detail-item pbm pln">
                            Delivery preference
                            <span> { book.serviceDetails.delivery_option == 48 ? 'Standard (48 hours) - Free Delivery' : 'Express (24 hours) + '+currencyText+" 50"}</span>
                        </div>
                </React.Fragment>) }
                {typeof book.serviceDetails.no_of_units != "undefined" && (<React.Fragment>
                    <div className="card-detail-item pbm pln text-capitalize">
                        {locationHelper.translate('NO_OF_AC')}
                        <span> {book.serviceDetails.no_of_units} </span>
                    </div>
                    <div className="card-detail-item pbm pln text-capitalize">
                        {locationHelper.translate('PROPERTY_TYPE')}
                        <span> {book.serviceDetails.property_type} </span>
                    </div>
                </React.Fragment>)}
                {typeof book.serviceDetails.tv_size_in_inches != "undefined" &&
                    (<div className="card-detail-item pbm pln text-capitalize">{locationHelper.translate('NO_OF_HOURS')}
                        <span>{key_label}</span>
                    </div>)}
                {typeof book.serviceDetails.no_of_hours != "undefined" && (<div className="card-detail-item pbm pln text-capitalize">{locationHelper.translate('NO_OF_HOURS')}
                    <span> {book.serviceDetails.no_of_hours == 0 ? "Not Sure" : book.serviceDetails.no_of_hours}</span>
                </div>)}
                {this.mattressDetails(book)}
                {sofaDetails.length ? sofaDetails.map((item) => {
                    return item;
                }) : ""}
            </div>
        )
        return details;
    }
    bookDetailsFirstSection(book) {
        var details = '';
        var servicesConstants = this.props.service_constants;
        if (book.service.id == servicesConstants.SERVICE_WINDOW_CLEANING) {
            details = (
                <div className="card-detail-items">
                    <div className="card-detail-item pbm pln">{locationHelper.translate('HOW_OFTEN')}
                        <span> {book.frequency ? book.frequency : ''}</span>
                    </div>
                    <div className="card-detail-item pbm pln">{locationHelper.translate('SERVICE_DAYS')}
                        <span> {book.weekDays ? (commonHelper.numbersToDays(book.weekDays.split(','))).slice(0, -2) : ''}</span>
                    </div>
                    <div className="card-detail-item pbm pln">{locationHelper.translate('SERVICE_DATE')}
                        <span> {book.date ? book.date : ''}</span>
                    </div>
                    <div className="card-detail-item pbm pln">{locationHelper.translate('HOME_SIZE')}
                        <span> {book.roomSize ? book.roomSize : ''}</span>
                    </div>
                </div>
            );
        } else if (book.service.id == servicesConstants.SERVICE_HOME_CLEANING) {
            details = (
                <div className="card-detail-items">
                    <div className="card-detail-item pbm pln">{locationHelper.translate('HOW_OFTEN')}
                        <span> {book.cleaningBooking ? book.cleaningBooking.frequency : book.frequency}</span>
                    </div>
                    <div className="card-detail-item pbm pln">{locationHelper.translate('SERVICE_DAYS')}
                        <span> {book.cleaningBooking ? (commonHelper.numbersToDays(book.cleaningBooking.weekDays.split(','))).slice(0, -2) : ''}</span>
                    </div>
                    <div className="card-detail-item pbm pln">{locationHelper.translate('PREFERRED_TIME')}
                        <span> {book.cleaningBooking ? book.cleaningBooking.time : book.time}</span>
                    </div>
                    <div className="card-detail-item pbm pln">{locationHelper.translate('CLEANING_MATERIALS')}
                        <span> {book.needMaterial == 0 ? 'No' : 'YES'}</span>
                    </div>
                    <div className="card-detail-item pbm pln">{locationHelper.translate('DURATION')}
                        <span> {(((book.duration && book.duration.indexOf('.')) ? (book.duration.substr(0, book.duration.indexOf('.'))) : book.duration) + " Hours")}</span>
                    </div>
                    {
                        book.subscription ? (
                            <div className="card-detail-item pbm pln">{locationHelper.translate('SUBSCRIPTION_START_DATE')}
                                <span> {book.cleaningBooking.startDate ? book.cleaningBooking.startDate : ''}</span>
                            </div>
                        ) : ''
                    }
                </div>
            );

        } else if (book.service.id == servicesConstants.SERVICE_POOL_CLEANING_SERVICE) {
            details = (
                <div className="card-detail-items">
                    <div className="card-detail-item pbm pln">{locationHelper.translate('HOW_OFTEN')}
                        <span> {book.frequency ? book.frequency : ''}</span>
                    </div>
                    <div className="card-detail-item pbm pln">{locationHelper.translate('SERVICE_DAYS')}
                        <span> {book.weekDays ? (commonHelper.numbersToDays(book.weekDays.split(','))).slice(0, -2) : ''}</span>
                    </div>
                    <div className="card-detail-item pbm pln">{locationHelper.translate('POOL_SIZE')}
                        <span> {book.poolSize ? book.poolSize : ''}</span>
                    </div>
                </div>
            );
        } else if (book.service.id == servicesConstants.SERVICE_HOME_PAINTING) {
            details = (
                <div className="card-detail-items">
                    <div className="card-detail-item pbm pln">{locationHelper.translate('DURATION')}
                        <span> {book.paintingBooking.duration ? book.paintingBooking.duration : ''}</span>
                    </div>
                    <div className="card-detail-item pbm pln">{locationHelper.translate('COLOR_OF_WALLS')}
                        <span> {book.paintingBooking.oldWallColor ? book.paintingBooking.oldWallColor : ''}</span>
                    </div>
                    <div className="card-detail-item pbm pln">{locationHelper.translate('WALL_COLOR_REQUIRED')}
                        <span> {book.paintingBooking.newWallColor ? book.paintingBooking.newWallColor : ''}</span>
                    </div>
                    <div className="card-detail-item pbm pln">{locationHelper.translate('TYPE_OF_HOME')}
                        <span> {book.paintingBooking.typeOfHome ? book.paintingBooking.typeOfHome : ''}</span>
                    </div>
                    <div className="card-detail-item pbm pln">{locationHelper.translate('SIZE_OF_HOME')}
                        <span> {book.paintingBooking.sizeOfHome ? book.paintingBooking.sizeOfHome : ''}</span>
                    </div>
                    <div className="card-detail-item pbm pln">{locationHelper.translate('PAINT_ADDITIONAL_ROOMS')}
                        <span> {book.paintingBooking.paintAdditionalRooms ? book.paintingBooking.paintAdditionalRooms : ''}</span>
                    </div>
                    <div className="card-detail-item pbm pln">{locationHelper.translate('PAINT_CEILINGS_REQUIRED')}
                        <span> {book.paintingBooking.needCeilingsPainted ? book.paintingBooking.needCeilingsPainted : ''}</span>
                    </div>
                </div>
            );
        } else if (this.isZohoMigratedServices(book)) {
            details = this.zohoMigratedBookingDetails(book)
        }
        return details;
    }
    bookDetailsSecondSection(book) {
        var details = '';
        var servicesConstants = this.props.service_constants;

        let currencyText = "AED";

        if (typeof book.bookingPrice != "undefined" && book.bookingPrice != null ) {
            currencyText = book.bookingPrice.currency;
        }
        if (typeof book.bookingEventPrice != "undefined" && book.bookingEventPrice != null) {
            currencyText = book.bookingEventPrice.currency;
        }

        if (book.service.id == servicesConstants.SERVICE_WINDOW_CLEANING) {
            details = (
                <div className="card-detail-items">
                    <div className="card-detail-item pbm pln">
                        {locationHelper.translate('PAYMENT_METHOD')}
                        <span>{book.paymentMethodDto.value}</span></div>
                    <div className="card-detail-item pbm pln">
                        {locationHelper.translate('VAT')}
                        <span>{`${book.bookingPrice.totalTax ? book.bookingPrice.totalTax.toFixed(2)
                            : book.taxAmount.toFixed(2)}  ${currencyText}`}</span></div>
                    <div className="card-detail-item pbm pln">
                        {locationHelper.translate('SUB_TOTAL')}
                        <span>
                            {`${book.bookingPrice.subTotal ? book.bookingPrice.subTotal : book.payment.toFixed(2)} ${currencyText}`}
                        </span></div>
                    {book.walletAmount ?
                        <div className="card-detail-item pbm pln">
                            {locationHelper.translate('WALLET_BOOKING_DETAIL_TXT')}
                            <span>{`${book.bookingPrice.walletDebitAmount ? book.bookingPrice.walletDebitAmount.toFixed(2)
                                : book.walletAmount.toFixed(2)} ${currencyText}`}</span></div>
                        : null}
                    {
                        book.couponCode ? [
                            <div className="card-detail-item pbm pln">
                                {locationHelper.translate('COUPON')}
                                <span>{book.couponCode}</span>
                            </div>,
                            <div className="card-detail-item pbm pln">
                                {locationHelper.translate('DISCOUNT')}
                                <span>
                                    {
                                        `${(book.bookingPrice.totalDiscount ? book.bookingPrice.totalDiscount.toFixed(2) :
                                            book.discountPrice ? (book.payment - book.discountPrice).toFixed(2) : 0)}  ${currencyText}`
                                    }
                                </span>
                            </div>
                        ] : ''
                    }

                </div>
            )
        } else if (book.service.id == servicesConstants.SERVICE_HOME_CLEANING) {
            details = (
                <div className="card-detail-items">
                    <div className="card-detail-item pbm pln">
                        {locationHelper.translate('PAYMENT_METHOD')}
                        <span>{book.paymentMethodDto.value}</span></div>
                    <div className="card-detail-item pbm pln">
                        {locationHelper.translate('HOURLY_FEE')}
                        <span>{book.cleaningBooking ? book.cleaningBooking.hourlyFees : 0}{' ' + currencyText}</span></div>
                    <div className="card-detail-item pbm pln">
                        {locationHelper.translate('VAT')}
                        <span>{book.bookingPrice.totalTax ? book.bookingPrice.totalTax.toFixed(2) : book.taxAmount.toFixed(2) + ' ' + currencyText}</span></div>
                    <div className="card-detail-item pbm pln">
                        {locationHelper.translate('SUB_TOTAL')}
                        <span>{`${book.bookingPrice.subTotal ? book.bookingPrice.subTotal : book.payment.toFixed(2)} ${currencyText}`}</span></div>
                    {book.walletAmount ?
                        <div className="card-detail-item pbm pln">
                            {locationHelper.translate('WALLET_BOOKING_DETAIL_TXT')}
                            <span>{`${book.bookingPrice.walletDebitAmount ? book.bookingPrice.walletDebitAmount.toFixed(2)
                                : book.walletAmount.toFixed(2)} ${currencyText}`}</span></div>
                        : null}
                    {
                        book.couponCode ? [
                            <div className="card-detail-item pbm pln">
                                {locationHelper.translate('COUPON')}
                                <span>{book.couponCode}</span>
                            </div>,
                            <div className="card-detail-item pbm pln">
                                {locationHelper.translate('DISCOUNT')}
                                <span>{`${book.bookingPrice.totalDiscount ? book.bookingPrice.totalDiscount.toFixed(2) :
                                    book.discountPrice ? (book.payment - book.discountPrice).toFixed(2) : 0} ${currencyText}`}</span></div>
                        ] : ''
                    }
                </div >
            )

        } else if (book.service.id == servicesConstants.SERVICE_POOL_CLEANING_SERVICE) {
            details = (
                <div className="card-detail-items">
                    <div className="card-detail-item pbm pln">
                        {locationHelper.translate('PAYMENT_METHOD')}
                        <span>{book.paymentMethodDto.value}</span></div>
                    {book.walletAmount ?
                        <div className="card-detail-item pbm pln">
                            {stringConstants.WALLET_BOOKING_DETAIL_TXT}
                            <span>{`${book.bookingPrice.walletDebitAmount ? book.bookingPrice.walletDebitAmount.toFixed(2)
                                : book.walletAmount.toFixed(2)}  ${currencyText}`}</span></div>
                        : null}
                    <div className="card-detail-item pbm pln">
                        {locationHelper.translate('VAT')}
                        <span>{`${book.bookingPrice.totalTax ? book.bookingPrice.totalTax.toFixed(2)
                            : book.taxAmount.toFixed(2)} ${currencyText}`}</span></div>
                    <div className="card-detail-item pbm pln">
                        {locationHelper.translate('SUB_TOTAL')}
                        <span>{`${book.bookingPrice.subTotal ? book.bookingPrice.subTotal : book.payment.toFixed(2)}  
                        ${currencyText}`}</span></div>
                    {
                        book.couponCode ? [
                            <div className="card-detail-item pbm pln">
                                {locationHelper.translate('COUPON')}
                                <span>{book.couponCode}</span>
                            </div>,
                            <div className="card-detail-item pbm pln">
                                {locationHelper.translate('DISCOUNT')}
                                <span>{`${book.bookingPrice.totalDiscount ? book.bookingPrice.totalDiscount.toFixed(2) :
                                    book.discountPrice ? (book.payment - book.discountPrice).toFixed(2) : 0} ${currencyText}`}</span></div>
                        ] : ''
                    }

                </div >
            )
        } else if (book.service.id == servicesConstants.SERVICE_HOME_PAINTING) {
            details = (
                <div className="card-detail-items">
                    <div className="card-detail-item pbm pln">
                        {locationHelper.translate('HOW_MANY_CEILINGS')}
                        <span>{book.paintingBooking.numberOfCeilingsToPaint}</span></div>
                    <div className="card-detail-item pbm pln">
                        {locationHelper.translate('FURNISHED_APARTMENT')}
                        <span>{book.paintingBooking.homeFurnished}</span></div>
                    <div className="card-detail-item pbm pln">
                        {locationHelper.translate('DATE')}
                        <span>{book.date}</span></div>
                    <div className="card-detail-item pbm pln">
                        {locationHelper.translate('VAT')}
                        <span>{`${book.bookingPrice.totalTax ? book.bookingPrice.totalTax.toFixed(2)
                            : book.taxAmount.toFixed(2)} ${currencyText}`}</span></div>
                    <div className="card-detail-item pbm pln">
                        {locationHelper.translate('SUB_TOTAL')}
                        <span>{`${book.bookingPrice.subTotal ? book.bookingPrice.subTotal :
                            book.payment.toFixed(2)} ${currencyText}`}</span></div>
                    {
                        book.walletAmount ?
                            <div className="card-detail-item pbm pln">
                                {stringConstants.WALLET_BOOKING_DETAIL_TXT}
                                <span>{`${book.bookingPrice.walletDebitAmount ? book.bookingPrice.walletDebitAmount
                                    : book.walletAmount.toFixed(2)}  ${currencyText}`}</span></div>
                            : null
                    }
                    {
                        book.couponCode ? [
                            <div className="card-detail-item pbm pln">
                                {locationHelper.translate('COUPON')}
                                <span>{book.couponCode}</span>
                            </div>,
                            <div className="card-detail-item pbm pln">
                                {locationHelper.translate('DISCOUNT')}
                                <span>{`${book.bookingPrice.totalDiscount ? book.bookingPrice.totalDiscount.toFixed(2) :
                                    book.discountPrice ? (book.payment - book.discountPrice).toFixed(2) : 0} ${currencyText}`}</span></div>
                        ] : ''
                    }

                </div >
            )
        } else if (this.isZohoMigratedServices(book) ) {
            let showPrice = this.isCanceButtonShow(book);
            details = (
                <div className="card-detail-items">
                    <div className="card-detail-item pbm pln">
                        {locationHelper.translate('PAYMENT_METHOD')}
                        <span>{book.paymentMethodDto.value}</span></div>
                    { showPrice ? (<div className="card-detail-item pbm pln">
                        {locationHelper.translate('PRICE')}
                        <span>{book.bookingPrice.subTotal ? book.bookingPrice.subTotal : book.bookingPrice.basicServicePrice} {book && book.bookingPrice && book.bookingPrice.currency ? book.bookingPrice.currency : ''}</span>
                    </div>) : '' }
                    {
                        book.couponCode ? [
                            <div className="card-detail-item pbm pln">
                                {locationHelper.translate('COUPON')}
                                <span>{book.couponCode}</span>
                            </div>,
                            <div className="card-detail-item pbm pln">
                                {locationHelper.translate('DISCOUNT')}
                                <span>{book.bookingEventPrice && book.bookingEventPrice.totalDiscount ? book.bookingEventPrice.totalDiscount.toFixed(2) :
                                    (book.bookingPrice.totalDiscount ? book.bookingPrice.totalDiscount.toFixed(2) :
                                        book.discountPrice ? (book.payment - book.discountPrice).toFixed(2) : 0) + ' ' + currencyText}
                                </span ></div >
                        ] : ''
                    }
                    {
                        book.walletAmount ?
                            <div className="card-detail-item pbm pln">
                                {stringConstants.WALLET_BOOKING_DETAIL_TXT}
                                <span>{`${book.bookingPrice.walletDebitAmount ? book.bookingPrice.walletDebitAmount : book.walletAmount.toFixed(2)}  ${currencyText}`}</span></div>
                            : null
                    }
                    {showPrice ? (<React.Fragment>
                        <div className="card-detail-item pbm pln">
                            {locationHelper.translate('VAT')}
                            <span>{`${book.bookingPrice.totalTax ? book.bookingPrice.totalTax.toFixed(2)
                                : book.taxAmount.toFixed(2)} ${currencyText}`} </span>
                        </div>
                        <div className="card-detail-item pbm pln">
                            {locationHelper.translate('TOTAL')}
                            <span>{book.grossPayment.toFixed(2) + ' ' + currencyText}</span>
                        </div>
                    </React.Fragment>) : (<React.Fragment>
                        { /*
                            book.couponCode ? [
                                <div className="card-detail-item pbm pln">
                                    {locationHelper.translate('COUPON')}
                                    <span>{book.couponCode}</span>
                                </div>] : ''
                        }
                        {
                            
                            book.walletAmount ?
                                <div className="card-detail-item pbm pln">
                                    {stringConstants.WALLET_BOOKING_DETAIL_TXT}
                                    <span>{`${book.bookingPrice.walletDebitAmount ? book.bookingPrice.walletDebitAmount : book.walletAmount.toFixed(2)}  ${currencyText}`}</span></div>
                                : null 
                        <div className="card-detail-item pbm pln text-center">
                            <span>Price confirmed after pickup</span>
                        </div>*/
                        }
                    </React.Fragment>)}
                </div>
            )

        }
        return details;

    }

    upcomingBookDetailsFirstSection(book) {
        var details = '';
        var servicesConstants = this.props.service_constants;
        let currencyText = "AED";

        if (typeof book.bookingPrice != "undefined" && book.bookingPrice != null ) {
            currencyText = book.bookingPrice.currency;
        }
        if (typeof book.bookingEventPrice != "undefined" && book.bookingEventPrice != null) {
            currencyText = book.bookingEventPrice.currency;
        }

        if (book.service.id == servicesConstants.SERVICE_WINDOW_CLEANING) {
            details = (
                <div className="card-detail-items">
                    <div className="card-detail-item pbm pln">{locationHelper.translate('HOW_OFTEN')}
                        <span> {book.frequency ? book.frequency : ''}</span>
                    </div>
                    <div className="card-detail-item pbm pln">{locationHelper.translate('SERVICE_DATE')}
                        <span> {book.date ? book.date : ''}</span>
                    </div>
                    <div className="card-detail-item pbm pln">{locationHelper.translate('HOME_SIZE')}
                        <span> {book.roomSize ? book.roomSize : ''}</span>
                    </div>
                </div>
            );
        } else if (book.service.id == servicesConstants.SERVICE_HOME_CLEANING) {
            details = (
                <div className="card-detail-items">
                    <div className="card-detail-item pbm pln">{locationHelper.translate('HOW_OFTEN')}
                        <span> {book ? book.frequency : book.frequency}</span>
                    </div>
                    <div className="card-detail-item pbm pln">{locationHelper.translate('PREFERRED_TIME')}
                        <span> {book ? book.time : book.time}</span>
                    </div>
                    <div className="card-detail-item pbm pln">{locationHelper.translate('CLEANING_MATERIALS')}
                        <span> {book.needMaterial == 0 ? 'No' : 'YES'}</span>
                    </div>
                    <div className="card-detail-item pbm pln">{locationHelper.translate('DURATION')}
                        <span> {(((book.duration && book.duration.indexOf('.')) ? (book.duration.substr(0, book.duration.indexOf('.'))) : book.duration) + " Hours")}</span>
                    </div>

                </div>
            );

        } else if (book.service.id == servicesConstants.SERVICE_POOL_CLEANING_SERVICE) {
            details = (
                <div className="card-detail-items">
                    <div className="card-detail-item pbm pln">{locationHelper.translate('HOW_OFTEN')}
                        <span> {book.frequency ? book.frequency : ''}</span>
                    </div>
                    <div className="card-detail-item pbm pln">{locationHelper.translate('POOL_SIZE')}
                        <span> {book.poolSize ? book.poolSize : ''}</span>
                    </div>
                </div>
            );
        } else if (book.service.id == servicesConstants.SERVICE_HOME_PAINTING) {
            details = (
                <div className="card-detail-items">
                    <div className="card-detail-item pbm pln">{locationHelper.translate('DURATION')}
                        <span> {book.duration ? book.duration : ''}</span>
                    </div>
                    <div className="card-detail-item pbm pln">{locationHelper.translate('COLOR_OF_WALLS')}
                        <span> {book.oldColorOfWalls ? book.oldColorOfWalls : ''}</span>
                    </div>
                    <div className="card-detail-item pbm pln">{locationHelper.translate('WALL_COLOR_REQUIRED')}
                        <span> {book.newColorOfWalls ? book.newColorOfWalls : ''}</span>
                    </div>
                    <div className="card-detail-item pbm pln">{locationHelper.translate('TYPE_OF_HOME')}
                        <span> {book.typeOfHome ? book.typeOfHome : ''}</span>
                    </div>
                    <div className="card-detail-item pbm pln">{locationHelper.translate('SIZE_OF_HOME')}
                        <span> {book.sizeOfHome ? book.sizeOfHome : ''}</span>
                    </div>
                    <div className="card-detail-item pbm pln">{locationHelper.translate('NO_OF_ADDITIONAL_ROOMS')}
                        <span> {book.numberOfAdditionalRooms ? book.numberOfAdditionalRooms : ''}</span>
                    </div>
                </div>
            );
        }
        else if (this.isZohoMigratedServices(book)) {
            details = this.zohoMigratedBookingDetails(book)
        }
        return details;
    }
    upcomingBookDetailsSecondSection(book) {
        var details = '';
        var servicesConstants = this.props.service_constants;

        //console.log("upcomingBookDetailsSecondSection", book);
        let currencyText = "AED";

        if (typeof book.bookingPrice != "undefined" && book.bookingPrice != null ) {
            currencyText = book.bookingPrice.currency;
        }
        if (typeof book.bookingEventPrice != "undefined" && book.bookingEventPrice != null) {
            currencyText = book.bookingEventPrice.currency;
        }
        if (book.service.id == servicesConstants.SERVICE_WINDOW_CLEANING) {
            details = (
                <div className="card-detail-items">
                    <div className="card-detail-item pbm pln">
                        {locationHelper.translate('PAYMENT_METHOD')}
                        <span>{book.paymentMethodDto.value}</span></div>
                    <div className="card-detail-item pbm pln">
                        {locationHelper.translate('VAT')}
                        <span>{book.taxAmount.toFixed(2) + ' ' + currencyText}</span></div>
                    <div className="card-detail-item pbm pln">
                        {locationHelper.translate('SUB_TOTAL')}
                        <span>{book.payment.toFixed(2) + ' ' + currencyText}</span></div>
                    {book.walletAmount ?
                        <div className="card-detail-item pbm pln">
                            {locationHelper.translate('WALLET_BOOKING_DETAIL_TXT')}
                            <span>{book.walletAmount.toFixed(2) + ' ' + currencyText}</span></div>
                        : null}
                    {
                        book.couponCode ? [
                            <div className="card-detail-item pbm pln">
                                {locationHelper.translate('COUPON')}
                                <span>{book.couponCode}</span>
                            </div>,
                            <div className="card-detail-item pbm pln">
                                {locationHelper.translate('DISCOUNT')}
                                <span>{(book.discountPrice ? (book.payment - book.discountPrice).toFixed(2) : 0) + ' ' + currencyText}</span></div>
                        ] : ''
                    }

                </div>
            )
        } else if (book.service.id == servicesConstants.SERVICE_HOME_CLEANING) {
            details = (
                <div className="card-detail-items">
                    <div className="card-detail-item pbm pln">
                        {locationHelper.translate('PAYMENT_METHOD')}
                        <span>{book.paymentMethodDto.value}</span></div>
                    <div className="card-detail-item pbm pln">
                        {locationHelper.translate('HOURLY_FEE')}
                        <span>{book ? book.hourlyFees : 0}{' ' + currencyText}</span></div>
                    <div className="card-detail-item pbm pln">
                        {locationHelper.translate('VAT')}
                        <span>{book.taxAmount.toFixed(2) + ' ' + currencyText}</span></div>
                    <div className="card-detail-item pbm pln">
                        {locationHelper.translate('SUB_TOTAL')}
                        <span>{`${book.bookingEventPrice && book.bookingEventPrice.subTotal ? book.bookingEventPrice.subTotal.toFixed(2)
                            : book.payment.toFixed(2)} ${currencyText}`}</span></div>


                    {
                        book.walletAmount ?
                            <div className="card-detail-item pbm pln">
                                {stringConstants.WALLET_BOOKING_DETAIL_TXT} {locationHelper.translate('PAYMENT_METHOD')}
                                <span>{book.walletAmount.toFixed(2) + ' ' + currencyText}</span></div>
                            : null
                    }
                    {
                        book.couponCode ? [
                            <div className="card-detail-item pbm pln">
                                {locationHelper.translate('COUPON')}
                                <span>{book.couponCode}</span>
                            </div>,
                            <div className="card-detail-item pbm pln">
                                {locationHelper.translate('DISCOUNT')}
                                <span>{`${book.promoDiscount ? book.promoDiscount.toFixed(2)
                                    : (book.discountPrice ? (book.payment - book.discountPrice).toFixed(2) : 0)} ${currencyText}`}</span></div>
                        ] : ''
                    }


                </div >
            )

        } else if (book.service.id == servicesConstants.SERVICE_POOL_CLEANING_SERVICE) {
            details = (
                <div className="card-detail-items">
                    <div className="card-detail-item pbm pln">
                        {locationHelper.translate('PAYMENT_METHOD')}
                        <span>{book.paymentMethodDto.value}</span></div>
                    {book.walletAmount ?
                        <div className="card-detail-item pbm pln">
                            {stringConstants.WALLET_BOOKING_DETAIL_TXT}
                            <span>{book.walletAmount.toFixed(2) + ' ' + currencyText}</span></div>
                        : null
                    }
                    <div className="card-detail-item pbm pln">
                        {locationHelper.translate('VAT')}
                        <span>{book.taxAmount.toFixed(2) + ' ' + currencyText}</span></div>
                    <div className="card-detail-item pbm pln">
                        {locationHelper.translate('SUB_TOTAL')}
                        <span>{book.payment.toFixed(2) + ' ' + currencyText}</span></div>
                    {
                        book.couponCode ? [
                            <div className="card-detail-item pbm pln">
                                {locationHelper.translate('COUPON')}
                                <span>{book.couponCode}</span>
                            </div>,
                            <div className="card-detail-item pbm pln">
                                {locationHelper.translate('DISCOUNT')}
                                <span>{`${book.discountPrice ? (book.payment - book.discountPrice).toFixed(2)
                                    : 0} ${currencyText}`}</span></div>
                        ] : ''
                    }

                </div>
            )
        } else if (book.service.id == servicesConstants.SERVICE_HOME_PAINTING) {
            details = (
                <div className="card-detail-items">
                    <div className="card-detail-item pbm pln">
                        {locationHelper.translate('HOW_MANY_CEILINGS')}
                        <span>{book.numberOfCeilingsToPaint}</span></div>
                    <div className="card-detail-item pbm pln">
                        {locationHelper.translate('FURNISHED_APARTMENT')}
                        <span>{book.homeFurnished}</span></div>
                    <div className="card-detail-item pbm pln">
                        {locationHelper.translate('DATE')}
                        <span>{book.date}</span></div>
                    <div className="card-detail-item pbm pln">
                        {locationHelper.translate('VAT')}
                        <span>{book.taxAmount.toFixed(2) + ' ' + currencyText}</span></div>
                    <div className="card-detail-item pbm pln">
                        {locationHelper.translate('SUB_TOTAL')}
                        <span>{book.payment.toFixed(2) + ' ' + currencyText}</span></div>

                    {book.walletAmount ?
                        <div className="card-detail-item pbm pln">
                            {stringConstants.WALLET_BOOKING_DETAIL_TXT}
                            <span>{book.walletAmount.toFixed(2) + ' ' + currencyText}</span></div>
                        : null}
                    {
                        book.couponCode ? [
                            <div className="card-detail-item pbm pln">
                                {locationHelper.translate('COUPON')}
                                <span>{book.couponCode}</span>
                            </div>,
                            <div className="card-detail-item pbm pln">
                                {locationHelper.translate('DISCOUNT')}
                                <span>{`${book.discountPrice ? (book.payment - book.discountPrice).toFixed(2)
                                    : 0} ${currencyText}`}</span></div>
                        ] : ''
                    }

                </div >
            )
        } else if (this.isZohoMigratedServices(book)) {
            //+ ' '+ currencyText}
            let showPrice = this.isCanceButtonShow(book);
            details = (
                <div className="card-detail-items">
                    <div className="card-detail-item pbm pln">
                        {locationHelper.translate('PAYMENT_METHOD')}
                        <span>{book.paymentMethodDto.value}</span></div>
                    { showPrice ? (<div className="card-detail-item pbm pln">
                        {locationHelper.translate('SUB_TOTAL')}
                        <span>{`${book.bookingEventPrice ? book.bookingEventPrice.basicServicePrice : 0} ${currencyText}`}</span>
                        </div>) : (<div className="card-detail-item pbm pln text-right hidden">Price confirmed after pickup.</div>)}
                    {
                        book.couponCode ? [
                            <div className="card-detail-item pbm pln">
                                {locationHelper.translate('COUPON')}
                                <span>{book.couponCode}</span>
                            </div>,
                            <div className="card-detail-item pbm pln">
                                {locationHelper.translate('DISCOUNT')}
                                <span>{`${book.bookingEventPrice && book.bookingEventPrice.totalDiscount ? book.bookingEventPrice.totalDiscount.toFixed(2) :
                                    (book.discountPrice ? (book.payment - book.discountPrice).toFixed(2) : 0)} ${currencyText}`}
                                </span ></div>
                        ] : ''
                    }
                    { showPrice ? (<React.Fragment>
                        <div className="card-detail-item pbm pln">
                            {locationHelper.translate('VAT')}
                            <span>{book.taxAmount.toFixed(2)} {' ' + currencyText} </span></div>
                        {
                            book.walletAmount ?
                                <div className="card-detail-item pbm pln">
                                    {stringConstants.WALLET_BOOKING_DETAIL_TXT}
                                    <span>{book.walletAmount.toFixed(2) + ' ' + currencyText}</span></div>
                                : null
                        }
                        <div className="card-detail-item pbm pln">
                            {locationHelper.translate('TOTAL')}
                            <span>{book.grossPayment.toFixed(2)} {' ' + currencyText}</span></div>
                    </React.Fragment>) : '' }
                </div>
            )

        }
        return details;

    }
    renderWorkers(book) {
        return (
            <div className={"row"}>
                {
                    book.eventWorkers.map(eventWorker => {
                        return (
                            <div className="col-12 col-md-4 mb-3">
                                <div className="assigned-maid">
                                    <div className="maid-profile">
                                        <div className="maid-img showCover" style={{ 'backgroundImage': 'url(' + eventWorker.profilePictureUrl + ')' }}>
                                        </div>
                                        <div className="maid-details pl-3">
                                            <div className="maid-name">{eventWorker.firstName + ' ' + eventWorker.lastName}
                                            </div>
                                            <div className="maid-review">
                                                <StarRating rating={eventWorker.rating} />
                                                <div className="reviews-num">
                                                    {`${eventWorker.numberOfReviews}  ${locationHelper.translate('VERIFIED_REVIEWS')}`}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )
                    })
                }
            </div>
        )
    }
    renderEditButton(book) {
        var bookingTypeEditable = (book.service.id == this.props.service_constants.SERVICE_HOME_CLEANING) || (book.service.id == this.props.service_constants.SERVICE_WINDOW_CLEANING) || (book.service.id == this.props.service_constants.SERVICE_POOL_CLEANING_SERVICE) || book.service.serviceCode == "LDR";
        var changeRequested = (book.status == "Change Requested");
        //return '';
        if (bookingTypeEditable) {
            if( book.service.serviceCode == "LDR" && ( !changeRequested && this.isCanceButtonShow(book) ) ){
                return '';
            }else if (!changeRequested) {
                return (
                    <button className="btn btn-grey btn-block text-white mar-no" type="button"
                        onClick={() => this.openEditModal(book)}>Edit
                    </button>
                );
            }else {
                if(book.service.serviceCode == "LDR"){
                    return (
                        <p className="text-right"><b>Change Requests:</b> 
                            {locationHelper.translate('PENDING_CONFIRMATION')}
                        </p>);
                }else{
                    return (<p>{locationHelper.translate('PENDING_CONFIRMATION')}</p>)
                }
            }
        }
    }
    renderUnsubscribeButton(book) {
        var changeRequested = (book.status == "Change Requested");
        if (!changeRequested) {
            return (
                <div className={(book.subscription ? "col-md-4 align-self-center collapse show mt-2" : "col-md-4 align-self-center collapse ")}>
                    <button className="btn btn-grey btn-block text-white" type="button"
                        onClick={() => this.openUnsubscribeModal(book)}>{locationHelper.translate('UNSUBSCRIBE_BOOKING')}
                    </button>
                </div>
            );
        }
    }
    renderFreezeButton(book) {
        var changeRequested = (book.status == "Change Requested");
        if (!changeRequested) {
            return (
                <div className={"col-md-4 align-self-center collapse mt-2 " + (book.subscription ? "show" : "")}>
                    <button className="btn btn-grey btn-block text-white" type="button"
                        onClick={() => this.openFreezeModal(book)}>{locationHelper.translate('FREEZE_BOOKING')}
                    </button>
                </div>
            );
        }
    }
    renderEvents(events) {
        var slicedEvents = events.slice(0, 5);
        //+(book && book.bookingPrice && book.bookingPrice.currency ? book.bookingPrice.currency:'')
        //console.log("events", events);
        let currencyText = "";
        return slicedEvents.map(event => {
            currencyText = event.bookingEventPrice.currency;
            return (
                <tr>
                    <td>{event.date}</td>
                    <td>{moment(event.date, 'YYYY-MM-DD').format('dddd')}</td>
                    <td>{event.time}</td>
                    <td>{event.grossPayment + ' ' + currencyText} </td>
                    <td>
                        {!event.subscription ? this.renderEditButton(event) : ''}
                    </td>
                    <td>
                        {
                            (event.service.id == this.props.service_constants.SERVICE_HOME_CLEANING) || (event.service.id == this.props.service_constants.SERVICE_WINDOW_CLEANING) || (event.service.id == this.props.service_constants.SERVICE_POOL_CLEANING_SERVICE) ?
                                (
                                    <button className="btn btn-grey btn-block text-white mar-no" type="button" onClick={() => this.openEventCancelingReasons(event)}>{locationHelper.translate('CANCEL')}</button>
                                ) : ''
                        }

                    </td>
                </tr>
            );
        });
    }
    renderUpComingBookings() {
        if (this.props.myBookingsData.upcomingBookings.length > 0) {
            var events = this.props.myBookingsData.upcomingBookings;
            var nestedBookings = events.reduce((accumulator, pilot) => {
                if (!accumulator[pilot.bookingId]) {
                    accumulator[pilot.bookingId] = [];
                }
                accumulator[pilot.bookingId].push(pilot);
                return accumulator;
            }, []);
            // console.log('nested');
            // console.log(nestedBookings);
            let currencyText = "AED";
            let showPrice = true;
            return nestedBookings.map((nestedBook, index) => {
                var book = nestedBook[0];
                //currencyText = ( book && book.bookingPrice && book.bookingPrice.currency ) ? book.bookingPrice.currency : "AED";
                if (typeof book.bookingPrice != "undefined" && book.bookingPrice != null ) {
                    currencyText = book.bookingPrice.currency;
                }
                if (typeof book.bookingEventPrice != "undefined" && book.bookingEventPrice != null) {
                    currencyText = book.bookingEventPrice.currency;
                }
                showPrice = this.isCanceButtonShow(book);
                return (
                    <div className="card mb-4">
                        <div className="card-body">
                            <div className="row">
                                <div className="col-md-3 text-center">
                                    <LazyLoad offset={500} height={300} placeholder={<PlaceholderComponent />}>
                                        <img src={"/dist/images/services_pics/" + book.service.id + ".jpg"} height="219" />
                                    </LazyLoad>
                                </div>
                                <div className="col">
                                    <div className="row">
                                        <div className={showPrice ? "col-md-9" : "col-md-8"}>
                                            <strong>{!this.isZohoMigratedServices(book) ? book.frequency + " " : ""}{(book.subscription ? ' Subscription ' : ' ') + book.service.name}</strong>
                                            <p className="m-0">{locationHelper.translate('ID')}#{book.uuid}</p>
                                        </div>
                                        {showPrice ? (<div className="col-md-3">
                                            <p className="text-center"><strong>{locationHelper.translate('TOTAL_PRICE')}</strong></p>
                                            <p className="text-center">
                                                { /* book.grossPayment ? book.grossPayment + ' ' + currencyText : ''*/}
                                                { book.grossPayment + ' ' + currencyText }
                                            </p>
                                        </div>) : (<div className="col-md-4"><p className="text-center m-0">Price confirmed after pickup.</p></div>)}
                                    </div>
                                    <div className="row">
                                        {this.renderUnsubscribeButton(book)}
                                        {this.renderFreezeButton(book)}
                                    </div>
                                    { /*( this.isZohoMigratedServices(book) && this.isCanceButtonShow(book) )*/ 
                                        (this.isZohoMigratedServices(book) && !this.isCanceButtonShow(book) ) ? (<div className="row">
                                        <div className="col-md-4 offset-md-8 align-self-center collapse show mt-2">
                                            <button className="btn btn-grey btn-block text-white" type="button" onClick={() => this.openCancelingReasons(book)}>
                                                {locationHelper.translate('CANCEL_BOOKING')}
                                            </button>
                                        </div>
                                    </div>) : ''}
                                </div>
                                <hr style={{ width: "100%" }} />
                                <div className={"col-12 more-details"}
                                    onClick={() => this.moreDetails(book.bookingEventId)}>
                                    <span className={"title"}><strong>{locationHelper.translate('DETAILS')}</strong></span>
                                    <strong><a href="javascript:void(0)" className={"right dropdown-toggle"}
                                        aria-expanded={this.state.moreDetails == book.bookingEventId}></a></strong>
                                </div>
                                <div
                                    className={"col-12 details collapse " + (this.state.moreDetails == book.bookingEventId ? 'show' : '')}>
                                    <div className="row">
                                        <div className="col-md-6 col-sm-6 col-xs-6 pr-md-5">
                                            {this.upcomingBookDetailsFirstSection(book)}
                                        </div>
                                        <div className="col-md-6 col-sm-6 col-xs-6">
                                            {this.upcomingBookDetailsSecondSection(book)}
                                        </div>
                                        <div className="col-sm-12">
                                            {this.renderWorkers(book)}
                                        </div>
                                        {/*this.renderEditButton(event)*/}
                                        { (book.service.serviceCode == "LDR" && book.status == "Change Requested") ?
                                            (<div className="col-12"><p className="font-weight-bold text-warning">We received your request to edit this booking &amp; our team confirming changes with our partners</p></div>) : ''
                                        }
                                        { 
                                            (book.service.serviceCode == "LDR" && book.status != "Change Requested") ? (<React.Fragment>
                                                <div className="col-9"></div>
                                                <div className="col-3">{this.renderEditButton(nestedBook[0])}</div>
                                            </React.Fragment>) : book.service.serviceCode != "LDR" ? (<React.Fragment>
                                            <div className="col-sm-12">
                                            <p><strong>{locationHelper.translate('MY_UPCOMING_SERVICE_DATES')}</strong></p>
                                            <table className="responsive-table table">
                                                <thead>
                                                    <tr>
                                                        <th>{locationHelper.translate('DATE')}</th>
                                                        <th>{locationHelper.translate('WEEKDAY')}</th>
                                                        <th>{locationHelper.translate('PREFERRED_TIME')}</th>
                                                        <th>{locationHelper.translate('PRICE')}</th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {this.renderEvents(nestedBook)}
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>
                                                            {
                                                                (book.service.id == this.props.service_constants.SERVICE_HOME_CLEANING) || (book.service.id == this.props.service_constants.SERVICE_WINDOW_CLEANING) || (book.service.id == this.props.service_constants.SERVICE_POOL_CLEANING_SERVICE) ?
                                                                    (
                                                                        <button className="btn btn-grey btn-block text-white"
                                                                            type="button"
                                                                            onClick={() => this.openEditAllModal(book)}>{locationHelper.translate('EDIT_ALL')}
                                                                        </button>
                                                                    ) : ''
                                                            }
                                                        </td>
                                                        <td>
                                                            {
                                                                (book.service.id == this.props.service_constants.SERVICE_HOME_CLEANING) || (book.service.id == this.props.service_constants.SERVICE_WINDOW_CLEANING) || (book.service.id == this.props.service_constants.SERVICE_POOL_CLEANING_SERVICE) ?
                                                                    (
                                                                        <button className="btn btn-grey btn-block text-white"
                                                                            type="button"
                                                                            onClick={() => this.openCancelingReasons(book)}>{locationHelper.translate('CANCEL_ALL')}
                                                                        </button>
                                                                    ) : ''
                                                            }

                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div></React.Fragment>) : ''}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                );
            });
        }
    }
    renderHistoryBookings() {
        let {historyBookings} = this.props.myBookingsData;
        if (historyBookings.length > 0) {
           var events = historyBookings.length ? historyBookings.filter((item) => { return item.service.serviceCode == "LDR" && ( item.status == "Booking Event Pending Delivery Confirmation" || item.status == "Booking Event Pending In Progress" || item.status == "In Progress")}) : [];
           //console.log("events", historyBookings.filter((item) => { return item.service.serviceCode == "LDR"}));
           var nestedBookings = events.reduce((accumulator, pilot) => {
                if (!accumulator[pilot.bookingId]) { 
                    accumulator[pilot.bookingId] = [];
                }
                accumulator[pilot.bookingId].push(pilot);
                return accumulator;
            }, []);
            // console.log('nested');
            //console.log(nestedBookings);
            let currencyText = "AED";
            let showPrice = true;
            return nestedBookings.map((nestedBook, index) => {
                var book = nestedBook[0];
                if(book.uuid == "20190527000026LDRDXB"){
                    console.log("book.status", book.status, book.uuid, book);
                }
                if (typeof book.bookingPrice != "undefined" && book.bookingPrice != null) {
                    currencyText = book.bookingPrice.currency;
                }
                if (typeof book.bookingEventPrice != "undefined" && book.bookingEventPrice != null) {
                    currencyText = book.bookingEventPrice.currency;
                }
                showPrice = this.isCanceButtonShow(book);
                return (
                    <div className="card mb-4">
                        <div className="card-body">
                            <div className="row">
                                <div className="col-md-3 text-center">
                                    <LazyLoad offset={500} height={300} placeholder={<PlaceholderComponent />}>
                                        <img src={"/dist/images/services_pics/" + book.service.id + ".jpg"} height="219" />
                                    </LazyLoad>
                                </div>
                                <div className="col">
                                    <div className="row">
                                        <div className={showPrice ? "col-md-9" : "col-md-8"}>
                                            <strong>{!this.isZohoMigratedServices(book) ? book.frequency + " " : ""}{(book.subscription ? ' Subscription ' : ' ') + book.service.name}</strong>
                                            <p className="m-0">{locationHelper.translate('ID')}#{book.uuid}</p>
                                        </div>
                                        {showPrice ? (<div className="col-md-3">
                                            <p className="text-center"><strong>{locationHelper.translate('TOTAL_PRICE')}</strong></p>
                                            <p className="text-center">
                                                { /* book.grossPayment ? book.grossPayment + ' ' + currencyText : ''*/}
                                                { book.grossPayment + ' ' + currencyText }
                                            </p>
                                        </div>) : (<div className="col-md-4"><p className="text-center m-0">Price confirmed after pickup.</p></div>)}
                                    </div>
                                    { /*( this.isZohoMigratedServices(book) && this.isCanceButtonShow(book) )*/ 
                                        (this.isZohoMigratedServices(book) && !this.isCanceButtonShow(book) ) ? (<div className="row">
                                        <div className="col-md-4 offset-md-8 align-self-center collapse show mt-2">
                                            <button className="btn btn-grey btn-block text-white" type="button" onClick={() => this.openCancelingReasons(book)}>
                                                {locationHelper.translate('CANCEL_BOOKING')}
                                            </button>
                                        </div>
                                    </div>) : ''}
                                </div>
                                <hr style={{ width: "100%" }} />
                                <div className={"col-12 more-details"}
                                    onClick={() => this.moreDetails(book.bookingEventId)}>
                                    <span className={"title"}><strong>{locationHelper.translate('DETAILS')}</strong></span>
                                    <strong><a href="javascript:void(0)" className={"right dropdown-toggle"}
                                        aria-expanded={this.state.moreDetails == book.bookingEventId}></a></strong>
                                </div>
                                <div className={"col-12 details collapse " + (this.state.moreDetails == book.bookingEventId ? 'show' : '')}>
                                    <div className="row">
                                        <div className="col-md-6 col-sm-6 col-xs-6 pr-md-5">
                                            {this.upcomingBookDetailsFirstSection(book)}
                                        </div>
                                        <div className="col-md-6 col-sm-6 col-xs-6">
                                            {this.upcomingBookDetailsSecondSection(book)}
                                        </div>
                                        <div className="col-sm-12">
                                            {this.renderWorkers(book)}
                                        </div>
                                        { (book.service.serviceCode == "LDR" && book.status == "Change Requested") ?
                                            (<div className="col-12"><p className="font-weight-bold text-warning">We received your request to edit this booking &amp; our team confirming changes with our partners</p></div>) : ''
                                        }
                                        { 
                                            (book.service.serviceCode == "LDR" && book.status != "Change Requested") ? (<React.Fragment>
                                                <div className="col-9"></div>
                                                <div className="col-3">{this.renderEditButton(nestedBook[0])}</div>
                                            </React.Fragment>) : ''}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                );
            });
        }
    }
    renderEmptyBookings() {
        if (!this.props.myBookingsData.newBookings.length && !this.props.myBookingsData.upcomingBookings.length) {
            return (
                <div className={"text-center pt-5 ml-5 mr-5"}>
                    <img src={"/dist/images/empty-booking.png"} />
                    <p className={"pt-3"} style={{ color: "#ccc" }}>{locationHelper.translate('LOOKS_LIKE_YOU_HAVEN_REQUESTED_ANY_BOOKING_YET')} <br /> {locationHelper.translate('ONCE_YOU_REQUEST_BOOKING_ALL_THE_DETAILS_WILL_APPEAR_HERE')}</p>
                </div>
            )
        }
    }
    cancelReasonsModal(book) {
        var startTime = moment(this.state.startDate);
        var duration = moment.duration(moment(this.state.bookToCancel.date, 'YYYY-MM-DD').diff(startTime));
        var hours = duration.asHours();
        // console.log(hours);
        if (hours > 24) {
            if (!this.state.cancelLoader) {
                return (
                    <div>
                        <div name="cancel-booking-form" id="cancel-booking-form">
                            <div className="cancel-booking-content" style={{ "visibility": "visible" }}>

                                <h2 className={"text-center"}>{locationHelper.translate('SURE_TO_CANCEL')} <span
                                    className="event-date">{locationHelper.translate('FUL_BOOKING')}</span>{locationHelper.translate('QUESTION_MARK')}
                                </h2>
                                <p className="full-booking-instruction mvm">{locationHelper.translate('CONTACT_US_FOR_CHANGES')} <span
                                    className="uae-location phone-no-link"><a
                                        className="booking-call mx-1 mbn ltr d-inline-block" href="tel://0097144229639"
                                        title="">+971 4 4229639</a></span><span
                                            className="qatar-location phone-no-link hidden"><a
                                                className="booking-call mx-1 mbn ltr d-inline-block" href="tel://0097444294875"
                                                title="">+974 4429 4875</a></span> <span
                                                    className="uae-location phone-no-link">{locationHelper.translate('CONTACT_TIMINGS')}</span><span
                                                        className="qatar-location phone-no-link hidden">{locationHelper.translate('CONTACT_TIMINGS_QATAR')}</span>
                                </p>
                                <div className="booking-cancel-reason">
                                    <div className="booking-cancel-reasons">
                                        <p className="mbl font-weight-bold">{locationHelper.translate('PROVIDE_CANCELLATION_REASON')}</p>
                                        {this.renderReasons()}
                                        {this.renderOtherInput()}
                                    </div>
                                </div>
                                <div className="cancel-booking-buttons mtl mt-2">
                                    <a href="javascript:void(0)" onClick={() => {
                                        this.cancelRequest(book)
                                    }} className="btn btn-warning font-weight-bold px-4 text-uppercase btn-warning">{locationHelper.translate('CANCEL_MY_BOOKINGS')}</a>
                                    <a href="javascript:void(0)" onClick={() => this.setModal(false)}
                                        className="btn font-weight-bold px-4 text-uppercase btn-outline-dark">{locationHelper.translate('CLOSE')}</a>
                                </div>
                            </div>
                        </div>
                        <p className="pbm cancel-booking-desc px-3 pt-4">{locationHelper.translate('CALL_US_SPECIAL')} <span className="uae-location phone-no-link"><a
                            className="booking-call mx-1 mbn ltr d-inline-block" href="tel://0097144229639"
                            title="">+971 4 4229639</a></span><span
                                className="qatar-location phone-no-link hidden"><a
                                    className="booking-call mx-1 mbn ltr d-inline-block" href="tel://0097444294875"
                                    title="">+974 4429 4875</a></span>
                            <span className="uae-location phone-no-link">{locationHelper.translate('CONTACT_TIMINGS')}</span><span
                                className="qatar-location phone-no-link hidden">{locationHelper.translate('CONTACT_TIMINGS_QATAR')}</span>
                        </p>
                    </div>
                );
            } else {
                return (
                    <main className={"col-12 " + (!this.state.cancelLoader ? "show" : "hide")}><Loader /></main>
                );
            }
        } else {
            return (
                <div>
                    <div name="cancel-booking-form" id="cancel-booking-form">
                        <div className="cancel-booking-content" style={{ "visibility": "visible" }}>
                            <h2 className={"text-center"} dangerouslySetInnerHTML={{__html: locationHelper.translate('CANCEL_HELP_TEXT')}}></h2>
                        </div>
                    </div>
                    <p className="pbm cancel-booking-desc px-3 pt-4">{locationHelper.translate('CALL_US_SPECIAL')} <span className="uae-location phone-no-link"><a
                        className="booking-call mx-1 mbn ltr d-inline-block" href="tel://0097144229639" title="">+971 4 4229639</a></span><span
                            className="qatar-location phone-no-link hidden"><a
                                className="booking-call mx-1 mbn ltr d-inline-block" href="tel://0097444294875" title="">+974 4429 4875</a></span>
                        <span className="uae-location phone-no-link">{locationHelper.translate('CONTACT_TIMINGS')}</span><span
                            className="qatar-location phone-no-link hidden">{locationHelper.translate('CONTACT_TIMINGS_QATAR')}</span>
                    </p>
                </div>
            )
        }

    }
    cancelEventReasonsModal(book) {
        var startTime = moment(this.state.startDate);
        var duration = moment.duration(moment(this.state.bookToCancel.date, 'YYYY-MM-DD').diff(startTime));
        var hours = duration.asHours();
        // console.log(hours);
        if (hours > 24) {
            if (!this.state.cancelLoader) {
                return (
                    <div>
                        <div name="cancel-booking-form" id="cancel-booking-form">
                            <div className="cancel-booking-content" style={{ "visibility": "visible" }}>
                                <h2 className={"text-center"}>{locationHelper.translate('SURE_TO_CANCEL')} <span
                                    className="event-date">{locationHelper.translate('BOOKING')}</span>{locationHelper.translate('QUESTION_MARK')}
                                </h2>
                                <p className="full-booking-instruction mvm">{locationHelper.translate('CONTACT_US_FOR_CHANGES')} <span
                                    className="uae-location phone-no-link"><a
                                        className="booking-call mx-1 mbn ltr d-inline-block" href="tel://0097144229639"
                                        title="">+971 4 4229639</a></span><span
                                            className="qatar-location phone-no-link hidden"><a
                                                className="booking-call mx-1 mbn ltr d-inline-block" href="tel://0097444294875"
                                                title="">+974 4429 4875</a></span> <span
                                                    className="uae-location phone-no-link">{locationHelper.translate('CONTACT_TIMINGS')}</span><span
                                                        className="qatar-location phone-no-link hidden">{locationHelper.translate('CONTACT_TIMINGS_QATAR')}</span>
                                </p>
                                <div className="booking-cancel-reason">
                                    <div className="booking-cancel-reasons">
                                        <p className="mbl font-weight-bold">{locationHelper.translate('PROVIDE_CANCELLATION_REASON')}</p>
                                        {this.renderReasons()}
                                        {this.renderOtherInput()}
                                    </div>
                                </div>
                                <div className="cancel-booking-buttons mtl mt-2">
                                    <a href="javascript:void(0)" onClick={() => {
                                        this.cancelEventRequest(book)
                                    }} className="btn btn-warning font-weight-bold px-4 text-uppercase btn-warning">{locationHelper.translate('CANCEL_MY_BOOKINGS')}</a>
                                    <a href="javascript:void(0)" onClick={() => this.setEventReasonsModal(false)}
                                        className="btn font-weight-bold px-4 text-uppercase btn-outline-dark">{locationHelper.translate('CLOSE')}</a>
                                </div>
                            </div>
                        </div>
                        <p className="pbm cancel-booking-desc px-3 pt-4">{locationHelper.translate('CALL_US_SPECIAL')} <span className="uae-location phone-no-link"><a
                            className="booking-call mx-1 mbn ltr d-inline-block" href="tel://0097144229639"
                            title="">+971 4 4229639</a></span><span
                                className="qatar-location phone-no-link hidden"><a
                                    className="booking-call mx-1 mbn ltr d-inline-block" href="tel://0097444294875"
                                    title="">+974 4429 4875</a></span>
                            <span className="uae-location phone-no-link">{locationHelper.translate('CONTACT_TIMINGS')}</span><span
                                className="qatar-location phone-no-link hidden">{locationHelper.translate('CONTACT_TIMINGS_QATAR')}</span>
                        </p>
                    </div>
                );
            } else {
                return (
                    <main className={"col-12 " + (!this.state.cancelLoader ? "show" : "hide")}><Loader /></main>
                );
            }
        } else {
            return (
                <div>
                    <div name="cancel-booking-form" id="cancel-booking-form">
                        <div className="cancel-booking-content" style={{ "visibility": "visible" }}>
                        <h2 className={"text-center"} dangerouslySetInnerHTML={{__html: locationHelper.translate('CANCEL_HELP_TEXT')}}></h2>
                        </div>
                    </div>
                    <p className="pbm cancel-booking-desc px-3 pt-4">{locationHelper.translate('CALL_US_SPECIAL')} <span className="uae-location phone-no-link"><a
                        className="booking-call mx-1 mbn ltr d-inline-block" href="tel://0097144229639" title="">+971 4 4229639</a></span><span
                            className="qatar-location phone-no-link hidden"><a
                                className="booking-call mx-1 mbn ltr d-inline-block" href="tel://0097444294875" title="">+974 4429 4875</a></span>
                        <span className="uae-location phone-no-link">{locationHelper.translate('CONTACT_TIMINGS')}</span><span
                            className="qatar-location phone-no-link hidden">{locationHelper.translate('CONTACT_TIMINGS_QATAR')}</span>
                    </p>
                </div>
            )
        }

    }
    freezeModal(book) {
        if (!this.state.cancelLoader) {
            return (
                <div>
                    <div name="cancel-booking-form" id="freeze-booking-form">
                        <div className="cancel-booking-content" style={{ "visibility": "visible" }}>
                            <h2 className={"text-center"}>{locationHelper.translate('FREEZE_BOOKING_CONFIRM')} <span className="event-date">{locationHelper.translate('BOOKING')}</span>{locationHelper.translate('QUESTION_MARK')}
                            </h2>
                            <div className="booking-cancel-reason">
                                <div className="booking-cancel-reasons">
                                    <p className="mbl font-weight-bold">{locationHelper.translate('SELECT_FREEZING_DURATION')}</p>
                                    <div className="freezing-duration-radio clearfix">
                                        <input type="radio" id="month1" name={"freezeDuration"} value="1" onChange={this.handleChange} checked={this.isChecked("freezeDuration", 1)} />
                                        <label className="lbl pl-2" htmlFor="month1">{locationHelper.translate('ONE_MONTH')}</label>
                                    </div>
                                    <div className="freezing-duration-radio clearfix">
                                        <input type="radio" id="month2" name={"freezeDuration"} value="2" onChange={this.handleChange} checked={this.isChecked("freezeDuration", 2)} />
                                        <label className="lbl pl-2" htmlFor="month2">{locationHelper.translate('TWO_MONTH')}</label>
                                    </div>
                                    <p className="mbl font-weight-bold">{locationHelper.translate('SELECT_FREEZING_DATE')}</p>
                                    <DatePick name="freezeStartDate" inputValue={this.state.freezeStartDate} validationMessage=" " validationClasses="required" childClass={"col-12 date-container"} onInputChange={this.onInputChange} />
                                </div>
                            </div>
                            <div className="cancel-booking-buttons mtl mt-2">
                                <a href="javascript:void(0)" onClick={() => { this.freezeRequest(book) }} className="btn btn-warning font-weight-bold px-4 text-uppercase btn-warning">{locationHelper.translate('FREEZE_MY_BOOKING')}</a>
                                <a href="javascript:void(0)" onClick={() => this.setFreezeModal(false)} className="btn font-weight-bold px-4 text-uppercase btn-outline-dark">{locationHelper.translate('CLOSE')}</a>
                            </div>
                        </div>
                    </div>
                    <p className="pbm cancel-booking-desc px-3 pt-4">{locationHelper.translate('CALL_US_SPECIAL')} <span className="uae-location phone-no-link"><a
                        className="booking-call mx-1 mbn ltr d-inline-block" href="tel://0097144229639" title="">+971 4 4229639</a></span><span
                            className="qatar-location phone-no-link hidden"><a
                                className="booking-call mx-1 mbn ltr d-inline-block" href="tel://0097444294875" title="">+974 4429 4875</a></span>
                        <span className="uae-location phone-no-link">{locationHelper.translate('CONTACT_TIMINGS')}</span><span
                            className="qatar-location phone-no-link hidden">{locationHelper.translate('CONTACT_TIMINGS_QATAR')}</span>
                    </p>
                </div>
            );
        } else {
            return (
                <main className={"col-12 " + (!this.state.cancelLoader ? "show" : "hide")}><Loader /></main>
            );
        }

    }
    editModal() {
        var book = this.state.bookToEdit;
        var editOptions = []; 
        if(book.service.serviceCode == "LDR"){
            editOptions = [
                { 'id': 1, value: 1, label: "Time of pickup", service:"LDR", booking_date:book.date, booking_time:book.time},
                { 'id': 2, value: 2, label: "Delivery option", service:"LDR",selectedDropOff: book.serviceDetails.delivery_option }
            ]
        }else{
            editOptions = [{ 'id': 1, value: 1, label: "Date of service" }, { 'id': 3, value: 3, label: "Time of service" }];
            if (!book.subscription) {
                editOptions.push({ 'id': 2, value: 2, label: "Number of hours or cleaners" });
            }
        }
        
        var startTime = moment(this.state.startDate);
        var duration = moment.duration(moment(book.date, 'YYYY-MM-DD').diff(startTime));
        var hours = duration.asHours();
        // console.log(hours);
        if (hours > 24) {
            if (!this.state.cancelLoader) {
                return (
                    <div>
                        <div name="cancel-booking-form" id="edit-booking-form">
                            <div className="cancel-booking-content" style={{ "visibility": "visible" }}>
                                { /*<h2 className={"text-center"}>{locationHelper.translate('MAKE_A_CHANGE')}</h2> */ }
                                <div className="booking-cancel-reason">
                                    <div className="booking-cancel-reasons">
                                        <div className="row">
                                            <div className="col-12 pl-2 pl-sm-3 mb-3">
                                                <p className="mbl font-weight-bold">{locationHelper.translate('DETAIL_OF_CHANGE')}</p>
                                                <SelectInput 
                                                    name="selectedEditOption" 
                                                    inputValue={this.state.selectedEditOption} 
                                                    label={locationHelper.translate('PLEASE_SELECT')} 
                                                    options={editOptions} 
                                                    onInputChange={this.onInputChange} />
                                            </div>
                                        </div>
                                        {this.renderEditField()}
                                        
                                    </div>
                                </div>
                                <div className="cancel-booking-buttons mtl mt-4">
                                    <a href="javascript:void(0)" onClick={() => { this.editBookingRequest(book) }} className="btn btn-warning font-weight-bold px-4 text-uppercase btn-warning">{locationHelper.translate('REQUEST')}</a>
                                    <a href="javascript:void(0)" onClick={() => this.setEditModal(false)} className="btn font-weight-bold px-4 text-uppercase btn-outline-dark">{locationHelper.translate('CLOSE')}</a>
                                </div>
                            </div>
                        </div>
                        <p className="pbm cancel-booking-desc px-3 pt-4">{locationHelper.translate('CALL_US_SPECIAL')} <span className="uae-location phone-no-link"><a
                            className="booking-call mx-1 mbn ltr d-inline-block" href="tel://0097144229639" title="">+971 4 4229639</a></span><span
                                className="qatar-location phone-no-link hidden"><a
                                    className="booking-call mx-1 mbn ltr d-inline-block" href="tel://0097444294875" title="">+974 4429 4875</a></span>
                            <span className="uae-location phone-no-link">{locationHelper.translate('CONTACT_TIMINGS')}</span><span
                                className="qatar-location phone-no-link hidden">{locationHelper.translate('CONTACT_TIMINGS_QATAR')}</span>
                        </p>
                    </div>
                );
            } else {
                return (
                    <main className={"col-12 " + (!this.state.cancelLoader ? "show" : "hide")}><Loader /></main>
                );
            }
        } else {
            return (
                <div>
                    <div name="cancel-booking-form" id="cancel-booking-form">
                        <div className="cancel-booking-content" style={{ "visibility": "visible" }}>
                        <h2 className={"text-center"} dangerouslySetInnerHTML={{__html: locationHelper.translate('CANCEL_HELP_TEXT')}}></h2>
                        </div>
                    </div>
                    <p className="pbm cancel-booking-desc px-3 pt-4">{locationHelper.translate('CALL_US_SPECIAL')} <span className="uae-location phone-no-link"><a
                        className="booking-call mx-1 mbn ltr d-inline-block" href="tel://0097144229639" title="">+971 4 4229639</a></span><span
                            className="qatar-location phone-no-link hidden"><a
                                className="booking-call mx-1 mbn ltr d-inline-block" href="tel://0097444294875" title="">+974 4429 4875</a></span>
                        <span className="uae-location phone-no-link">{locationHelper.translate('CONTACT_TIMINGS')}</span><span
                            className="qatar-location phone-no-link hidden">{locationHelper.translate('CONTACT_TIMINGS_QATAR')}</span>
                    </p>
                </div>
            )
        }


    }
    editAllModal() {
        var book = this.state.bookToEdit;
        var editOptions = [{ 'id': 3, value: 3, label: "Time of service" }];
        if (!book.subscription) {
            editOptions.push({ 'id': 2, value: 2, label: "Number of hours or cleaners" });
        }
        var startTime = moment(this.state.startDate);
        var duration = moment.duration(moment(book.date, 'YYYY-MM-DD').diff(startTime));
        var hours = duration.asHours();
        // console.log(hours);
        if (hours > 24) {
            if (!this.state.cancelLoader) {
                return (
                    <div>
                        <div name="cancel-booking-form" id="edit-all-booking-form">
                            <div className="cancel-booking-content" style={{ "visibility": "visible" }}>
                                <h2 className={"text-center"}>{locationHelper.translate('MAKE_A_CHANGE')}</h2>
                                <div className="booking-cancel-reason">
                                    <div className="booking-cancel-reasons">
                                        <p className="mbl font-weight-bold">{locationHelper.translate('DETAIL_OF_CHANGE')}</p>
                                        <div className="col-12 pl-2 pl-sm-3 mb-3">
                                            <SelectInput name="selectedEditOption" inputValue={this.state.selectedEditOption} label={locationHelper.translate('PLEASE_SELECT')} options={editOptions} onInputChange={this.onInputChange} />
                                        </div>
                                        {this.renderEditField()}
                                    </div>
                                </div>
                                <div className="cancel-booking-buttons mtl mt-2">
                                    <a href="javascript:void(0)" onClick={() => { this.editAllBookingRequest(book) }} className="btn btn-warning font-weight-bold px-4 text-uppercase btn-warning">{locationHelper.translate('REQUEST')}</a>
                                    <a href="javascript:void(0)" onClick={() => this.setEditAllModal(false)} className="btn font-weight-bold px-4 text-uppercase btn-outline-dark">{locationHelper.translate('CLOSE')}</a>
                                </div>
                            </div>
                        </div>
                        <p className="pbm cancel-booking-desc px-3 pt-4">{locationHelper.translate('CALL_US_SPECIAL')} <span className="uae-location phone-no-link"><a
                            className="booking-call mx-1 mbn ltr d-inline-block" href="tel://0097144229639" title="">+971 4 4229639</a></span><span
                                className="qatar-location phone-no-link hidden"><a
                                    className="booking-call mx-1 mbn ltr d-inline-block" href="tel://0097444294875" title="">+974 4429 4875</a></span>
                            <span className="uae-location phone-no-link">{locationHelper.translate('CONTACT_TIMINGS')}</span><span
                                className="qatar-location phone-no-link hidden">{locationHelper.translate('CONTACT_TIMINGS_QATAR')}</span>
                        </p>
                    </div>
                );
            } else {
                return (
                    <main className={"col-12 " + (!this.state.cancelLoader ? "show" : "hide")}><Loader /></main>
                );
            }
        } else {
            return (
                <div>
                    <div name="cancel-booking-form" id="cancel-booking-form">
                        <div className="cancel-booking-content" style={{ "visibility": "visible" }}>
                        <h2 className={"text-center"} dangerouslySetInnerHTML={{__html: locationHelper.translate('CANCEL_HELP_TEXT')}}></h2>
                        </div>
                    </div>
                    <p className="pbm cancel-booking-desc px-3 pt-4">{locationHelper.translate('CALL_US_SPECIAL')} <span className="uae-location phone-no-link"><a
                        className="booking-call mx-1 mbn ltr d-inline-block" href="tel://0097144229639" title="">+971 4 4229639</a></span><span
                            className="qatar-location phone-no-link hidden"><a
                                className="booking-call mx-1 mbn ltr d-inline-block" href="tel://0097444294875" title="">+974 4429 4875</a></span>
                        <span className="uae-location phone-no-link">{locationHelper.translate('CONTACT_TIMINGS')}</span><span
                            className="qatar-location phone-no-link hidden">{locationHelper.translate('CONTACT_TIMINGS_QATAR')}</span>
                    </p>
                </div>
            )
        }


    }
    unsubscribeModal() {
        var book = this.state.bookToUnsubscribe;
        var editOptions = [{ 'id': 1, value: 1, label: "Date of service" }, { 'id': 3, value: 3, label: "Time of service" }];
        if (!book.subscription) {
            editOptions.push({ 'id': 2, value: 2, label: "Number of hours or cleaners" });
        }
        if (!this.state.cancelLoader) {
            return (
                <div>
                    <div name="cancel-booking-form" id="cancel-booking-form">
                        <div className="cancel-booking-content" style={{ "visibility": "visible" }}>
                            <h2 className={"text-center"}>{locationHelper.translate('UNSUBSCRIBE_BOOKING')}</h2>

                            <div className="cancel-booking-buttons mtl mt-2">
                                <a href="javascript:void(0)" onClick={() => { this.unsubscribeRequest(book) }} className="btn btn-warning font-weight-bold px-4 text-uppercase btn-warning">{locationHelper.translate('UNSUBSCRIBE_MY_BOOKING')}</a>
                                <a href="javascript:void(0)" onClick={() => this.setUnsubscribeModal(false)} className="btn font-weight-bold px-4 text-uppercase btn-outline-dark">{locationHelper.translate('CLOSE')}</a>
                            </div>
                        </div>
                    </div>
                    <p className="pbm cancel-booking-desc px-3 pt-4">{locationHelper.translate('CALL_US_SPECIAL')} <span className="uae-location phone-no-link"><a
                        className="booking-call mx-1 mbn ltr d-inline-block" href="tel://0097144229639" title="">+971 4 4229639</a></span><span
                            className="qatar-location phone-no-link hidden"><a
                                className="booking-call mx-1 mbn ltr d-inline-block" href="tel://0097444294875" title="">+974 4429 4875</a></span>
                        <span className="uae-location phone-no-link">{locationHelper.translate('CONTACT_TIMINGS')}</span><span
                            className="qatar-location phone-no-link hidden">{locationHelper.translate('CONTACT_TIMINGS_QATAR')}</span>
                    </p>
                </div>
            );
        } else {
            return (
                <main className={"col-12 " + (!this.state.cancelLoader ? "show" : "hide")}><Loader /></main>
            );
        }

    }
    errorModal() {
        return (
            <div>
                <div name="cancel-booking-form" id="cancel-booking-form">
                    <div className="cancel-booking-content" style={{ "visibility": "visible" }}>
                        <h2 className={"text-center"}>{this.state.errorMessage}</h2>
                    </div>
                </div>
                <p className="pbm cancel-booking-desc px-3 pt-4">{locationHelper.translate('CALL_US_SPECIAL')} <span className="uae-location phone-no-link"><a
                    className="booking-call mx-1 mbn ltr d-inline-block" href="tel://0097144229639" title="">+971 4 4229639</a></span><span
                        className="qatar-location phone-no-link hidden"><a
                            className="booking-call mx-1 mbn ltr d-inline-block" href="tel://0097444294875" title="">+974 4429 4875</a></span>
                    <span className="uae-location phone-no-link">{locationHelper.translate('CONTACT_TIMINGS')}</span><span
                        className="qatar-location phone-no-link hidden">{locationHelper.translate('CONTACT_TIMINGS_QATAR')}</span>
                </p>
            </div>
        );
    }
    setModal(visibility) {
        const { isOffer } = this.props;
        if (!isOffer) {
            this.setState({
                showModal: visibility,
                other: ''
            });
        }
    }
    setEventReasonsModal(visibility) {
        const { isOffer } = this.props;
        if (!isOffer) {
            this.setState({
                showEventReasonsModal: visibility
            });
        }
    }
    setFreezeModal(visibility) {
        const { isOffer } = this.props;
        if (!isOffer) {
            this.setState({
                showFreezeModal: visibility
            });
        }
    }
    setEditModal(visibility) {
        const { isOffer } = this.props;
        /*selectedEditOption: { value: null },
        selectedNewDuration: { value: null },
        selectedNumberOfCleaners: { value: null },*/
        if (!isOffer) {
            this.setState({
                showEditModal: visibility,
                selectedEditOption: { value: null },
                selectedNewDuration: { value: null },
                selectedNumberOfCleaners: { value: null },
                dropOff: { value: null },
                editDateOfService: '',
                job_time: '',
            });
        }
    }
    setEditAllModal(visibility) {
        const { isOffer } = this.props;
        if (!isOffer) {
            this.setState({
                showEditAllModal: visibility,
                selectedEditOption: { value: null },
                selectedNewDuration: { value: null },
                selectedNumberOfCleaners: { value: null },
                dropOff: { value: null },
                editDateOfService: '',
                job_time: '',
            });
        }
    }
    setUnsubscribeModal(visibility) {
        const { isOffer } = this.props;
        if (!isOffer) {
            this.setState({
                showUnsubscribeModal: visibility
            });
        }
    }
    setErrorModal(visibility) {
        const { isOffer } = this.props;
        if (!isOffer) {
            this.setState({
                showErrorModal: visibility,
                errorMessage: 'Something went wrong, Please contact ServiceMarket.'
            });
        }
    }
    setMessageErrorModal(message) {
        const { isOffer } = this.props;
        if (!isOffer) {
            this.setState({
                showErrorModal: true,
                errorMessage: message
            });
        }
    }
    isZohoMigratedServices(booking) {
        let { zohoMigratedServices } = this.props;
        let iszohoMigratedServices = typeof zohoMigratedServices != null ? zohoMigratedServices.find((item) => { return ( item.serviceCode == booking.service.serviceCode || booking.service.serviceCode == "LDR" ) }) : false;
        return (typeof iszohoMigratedServices != "undefined" && iszohoMigratedServices != false) ? true : false;
    }
    render() {
        // console.log(this.props.myBookingsData);
        if (!this.state.loader) {
            return (
                <div className="col-lg-9 col-md-9 col-sm-12 col-xs-12 user-right-con my-bookings">
                    {this.renderNewBookings()}
                    {this.renderUpComingBookings()}
                    {this.renderHistoryBookings()}
                    {this.renderEmptyBookings()}
                    {this.state.showModal && <GeneralModal title="" modalBody={this.cancelReasonsModal()} setModal={this.setModal} />}
                    {this.state.showEventReasonsModal && <GeneralModal title="" modalBody={this.cancelEventReasonsModal()} setModal={this.setEventReasonsModal} />}
                    {this.state.showFreezeModal && <GeneralModal title="" modalBody={this.freezeModal()} setModal={this.setFreezeModal} />}
                    {this.state.showEditModal && <GeneralModal titleClass="modal-title h4 text-center d-block" title={locationHelper.translate('MAKE_A_CHANGE')} modalBody={this.editModal()} setModal={this.setEditModal} modalClass="edit-booking"/>}
                    {this.state.showEditAllModal && <GeneralModal title="" modalBody={this.editAllModal()} setModal={this.setEditAllModal} />}
                    {this.state.showUnsubscribeModal && <GeneralModal title="" modalBody={this.unsubscribeModal()} setModal={this.setUnsubscribeModal} />}
                    {this.state.showErrorModal && <GeneralModal  modalBody={this.errorModal()} setModal={this.setErrorModal} />}
                </div>
            );
        } else {
            return (
                <main className={"col-lg-9 col-md-9 col-sm-12 col-xs-12 user-right-con " + (!this.state.loader ? "show" : "hide")}><Loader /></main>
            );
        }
    }

}


function mapStateToProps(state) {
    return {
        myBookingsData: state.myBookingsData,
        signInDetails: state.signInDetails,
        dataConstants: state.dataConstants,
        service_constants: state.serviceConstants,
        dateAndTime: state.dateAndTime,
        zohoMigratedServices: state.zohoMigratedServices,
        userProfile: state.userProfile,
        currentCity: state.currentCity,
        lang: state.lang,
        currentCityID: state.currentCityID,
        currentCityData: state.currentCityData,
        dataConstants: state.dataConstants,
        newDataConstants: state.newDataConstants,
        cities: state.cities,
        bookingDateTimeAvailabilityReducer: state.bookingDateTimeAvailabilityReducer,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ fetchCustomerBookingHistory, cancelBookingRequest, fetchDateTime, cancelBookingEventRequest, unsubscribeBookingRequest, freezeBookingRequest, changeBookingRequest, fetchZohoMigratedService, newChangeBookingRequest,fetchDataDictionaryValues, fetchDataConstantValues, fetchAllCities }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(MyBookings);
