import React from "react";
import TextFloatInput from "./Form_Fields/TextFloatInput";
import TextareaInput from "./Form_Fields/TextareaInput";
import SubmitElement from "./Form_Fields/SubmitElement";
import GeneralModal from "./GeneralModal";
import Loader from "./Loader";
import PhoneInput from "./Form_Fields/PhoneInput";
import {isValidSection, submitContactUs} from "../actions/index";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import locationHelper from '../helpers/locationHelper';


class ContactForm extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            fname: '',
            lname: '',
            email: '',
            phone: '',
            message: '',
            showModal: false,
            sucess: false
        }

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.setModal = this.setModal.bind(this);
        this.locationHelperInit = locationHelper.init.bind(this);
        this.locationHelperInit();
    }
    setModal(visibility){
        this.setState({
            showModal: visibility,
            sucess: false
        });
    }
    handleInputChange(name, value) {
        this.setState({
            [name]: value,
        });
    }
    handleSubmit(event) {
        event.preventDefault();
        let {lang} = this.props
        if( isValidSection('contactus-form') ){
            // Write submit action here
            this.setModal(true);
            var {fname, lname, email, phone, message} = this.state;
            submitContactUs(this.props.signInDetails.access_token || '', message, email, fname, lname, phone).then(res=> {
                if( res.success ){
                	this.setState({
	                    sucess: true,
	                });
                }
                
            });
        }
    }

    render() {
        var {fname, lname, email, phone, message} = this.state;
         return (
            <form id="contactus-form" onSubmit={this.handleSubmit}>
                <div className="row">
                    <div className="col-6 pr-2 pr-sm-3 mb-3">
                        <TextFloatInput InputType="text" name="fname" inputValue={fname} label={locationHelper.translate('FIRST_NAME')} onInputChange={this.handleInputChange} validationClasses="required" />
                    </div>
                    <div className="col-6 pl-2 pl-sm-3 mb-3">
                        <TextFloatInput InputType="text" name="lname" inputValue={lname} label={locationHelper.translate('LAST_NAME')} onInputChange={this.handleInputChange} validationClasses="required" />
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 pr-2 pr-sm-3 mb-3">
                        <TextFloatInput InputType="email" name="email" inputValue={email} label={locationHelper.translate('EMAIL')} onInputChange={this.handleInputChange} validationClasses="required email" />
                    </div>
                    <div className="col-6 pl-2 pl-sm-3 mb-3">
                        <PhoneInput name="phone" inputValue={phone} onInputChange={this.handleInputChange} validationClasses="required phone" />
                    </div>
                </div>
                
                <TextareaInput name="message" placeholder={locationHelper.translate('MESSAGE')} inputValue={message} onInputChange={this.handleInputChange}/>
                <SubmitElement floatedRight={true} />

                { this.state.showModal && <GeneralModal title={locationHelper.translate("SENDING_DETAILS")} modalBody={<Loader completed={this.state.sucess} />} setModal={this.setModal} /> }
            </form>
        );
    }
}


function mapStateToProps(state) {
    return {
        signInDetails: state.signInDetails,
        currentCity: state.currentCity,
        lang: state.lang,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({submitContactUs}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ContactForm);
