import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import { Provider as ReduxProvider } from "react-redux";
import createStore from "./store";
import Layout from "./pages/Layout";
import {CookiesProvider} from "react-cookie";
import bugsnag from '@bugsnag/js';
import bugsnagReact from '@bugsnag/plugin-react';
import staticVariables from "./staticVariables";
import Loadable from 'react-loadable';
if( typeof env != "undefined" && env == "PROD" ){
    const bugsnagClient = bugsnag(staticVariables.BUGSNAG_CLIENT_APP_ID);
    bugsnagClient.use(bugsnagReact, React);
    const ErrorBoundary = bugsnagClient.getPlugin('react');
}

const store = createStore( window.REDUX_DATA );
let jsx = "";
if(typeof ErrorBoundary != "undefined"){
    jsx = (
            <ErrorBoundary>
                <ReduxProvider store={ store }>
                    <BrowserRouter>
                        <CookiesProvider>
                            <Layout />
                        </CookiesProvider>
                    </BrowserRouter>
                </ReduxProvider>
            </ErrorBoundary>
        );
}else{
    jsx = (
        <ReduxProvider store={ store }>
            <BrowserRouter>
                <CookiesProvider>
                    <Layout />
                </CookiesProvider>
            </BrowserRouter>
        </ReduxProvider>
    );
}

Loadable.preloadReady().then(() => {
    ReactDOM.hydrate( jsx,document.getElementById('app'));
});

