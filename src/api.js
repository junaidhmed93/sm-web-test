import qs from 'qs';
import staticVariables from "./staticVariables";
import { DEFAULT_LANG, updateLangParams, LANG_EN } from './actions';
if(typeof process.env.NODE_ENV != "undefined" && process.env.NODE_ENV == "LOCAL"){
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
}
export const endPoints = {
    emailAvailable: function (email,lang) {
        return {
            "url": staticVariables.CORE_API + "/v1.0/read/customer/customerEmailAvailable?customerEmail=" + encodeURIComponent(email),
            "name": "email_available"+lang,
            "responseName": "email_available",
            "headers":
                [
                    "Content-Type: application/json"
                ],
            "method": "get",
            "body": "",
            "public": true,
            "time": "0"
        }
    },
    citisAreas: function(lang){ 
        return {
            "url": staticVariables.CORE_API + "/v1.0/read/country-city-area",
            "name": "country_city_area"+lang,
            "responseName": "country_city_area",
            "headers":[
                "Content-Type: application/json",
                "Accept-Language:"+ lang
            ],
            "method": "get",
            "body": "",
            "public": true,
            "time": "1"
        }
    },
    providersAll: function sortByPriorityProvider(sortByPriorityProvider, lang) {
        return {
            "url": staticVariables.CORE_API + "/v1.0/service-provider/all" + '?sortByPriorityProvider=' + sortByPriorityProvider,
            "name": "providers_all_"+lang,
            "responseName": "providersAll",
            "headers":[
                "Content-Type: application/json",
                "Accept-Language:"+ lang
            ],
            "method": "get",
            "body": "",
            "public": true,
            "time": "1"
        }
    },
    serviceProviderLocations: function (providerID, serviceID, lang) {
        return {
            "url": staticVariables.CORE_API + "/v1.0/service-provider/" + providerID + "/" + serviceID,
            "name": "serviceProviderLocations_" + providerID + "_" + serviceID+lang,
            "responseName": "locations",
            "headers": [
                "Content-Type: application/json"
            ],
            "method": "get",
            "body": "",
            "public": true,
            "time": "1"
        }
    },
    providersByService: function (cityCode, id, lang) {
        return {
            "url": staticVariables.CORE_API + "/v1.0/service-provider/all?serviceId=" + id + "&locationCode=" + cityCode,
            "name": "providers_" + id + "_" + cityCode+lang,
            "responseName": "providers",
            "headers": [
                "Content-Type: application/json",
                "Accept-Language:"+ lang
            ],
            "method": "get",
            "body": "",
            "public": true,
            "time": "1"
        }
    },
    providersByCity: function (cityCode, lang) {
        return {
            "url": staticVariables.CORE_API + "/v1.0/service-provider/list/" + cityCode,
            "name": "providers_" + cityCode+lang,
            "responseName": "providers",
            "headers": [
                "Content-Type: application/json",
                "Accept-Language:"+ lang
            ],
            "method": "get",
            "body": "",
            "public": true,
            "time": "1"
        }
    },
    provider: function (id,lang) {
        return {
            "url": staticVariables.CORE_API + "/v1.0/service-provider/" + id,
            "name": "provider_" + id+lang,
            "responseName": "provider",
            "headers": [
                "Content-Type: application/json",
                "Accept-Language:"+ lang
            ],
            "method": "get",
            "body": "",
            "public": true,
            "time": "1"
        }
    },
    providerByURL: function (url, lang) {
        return {
            "url": staticVariables.CORE_API + "/v1.0/service-provider/detail/" + url,
            "name": "provider_" + url+lang,
            "responseName": "provider",
            "headers": [
                "Content-Type: application/json",
                "Accept-Language:"+ lang
            ],
            "method": "get",
            "body": "",
            "public": true,
            "time": "1"
        }
    },
    cities: function(lang) {
        return {
            "url": staticVariables.CORE_API + "/v1/constants/service-locations",
            "name": "cities_"+lang,
            "responseName": "cities",
            "headers": [
                "Content-Type: application/json",
                "Accept-Language:"+ lang
            ],
            "method": "get",
            "body": "",
            "public": true,
            "time": "1"
        }
    },
    search: function (city, term, lang) {
        let searchUrl = "https://smmiddleware.aratech.co/elastic/servicemarket/service/_search?q=(+name:*" + term + "*+search_term:*" + term + "*)%20AND%20City:" + city
        return {
            "url": searchUrl,
            "headers": {
                "Content-Type": "application/json"
            },
            "method": "post",
            "data": {
                "size": "10",
                "sort": {
                    "order": { "order": "asc" }
                }
            }
        }
    },
    apiServices: function (city, lang) {
        return {
            "url": staticVariables.CORE_API + "/v1.0/custom-services/" + city,
            "name": "apiservices" + city + lang,
            "responseName": "apiservices",
            "headers": [
                "Content-Type: application/json",
                "Accept-Language:"+ lang
            ],
            "method": "get",
            "body": "",
            "public": true,
            "time": "1"
        }
    },
    wpServices: function (city ,lang = DEFAULT_LANG ) {
        let wpServicesUrl = staticVariables.WORDPRESS_URL + "/wp-json/react-api/v2/cityservices?city=" + city +"&v=" + (Math.random() * 10000).toFixed(0);
        if(DEFAULT_LANG != lang){
            wpServicesUrl +="&lang="+lang
        }
        return {
            "url": wpServicesUrl,
            "name": "wpservices_" + city+"_"+lang,
            "responseName": "wpservices",
            "headers": [
                "Content-Type: application/json",
                "Accept-Language:"+ lang
            ],
            "method": "get",
            "body": "",
            "public": false,
            "time": "1"
        }

    },
    wpAllServicesWithCity: function (lang = DEFAULT_LANG) {
        let url = staticVariables.WORDPRESS_URL + "/wp-json/react-api/v2/allservices?v=" + (Math.random() * 10000).toFixed(0)
        if(DEFAULT_LANG != lang){
            url +="&lang="+lang
        }
        return {
            "url": url,
            "name": "wpAllServicesWithCity"+lang,
            "responseName": "wpAllServicesWithCity",
            "headers": [
                "Content-Type: application/json",
                "Accept-Language:"+ lang
            ],
            "method": "get",
            "body": "",
            "public": false,
            "time": "1"
        }

    },
    landingData: function (city, slug, lang = DEFAULT_LANG) {
        let url = staticVariables.WORDPRESS_URL + "/wp-json/react-api/v2/landing?url_slug=" + slug + "&city=" + city + "&v=" + (Math.random() * 10000).toFixed(0)
        if(DEFAULT_LANG != lang){
            url +="&lang="+lang
        }
        //console.log("landingData", url);
        return {
            "url": url,
            "name": city + "_"+lang+ "_" + slug,
            "responseName": "landing",
            "headers": [
                "Content-Type: application/json",
                "Accept-Language:"+ lang
            ],
            "method": "get",
            "body": "",
            "public": false,
            "time": "1"
        }

    },
    whySMData : function(lang = DEFAULT_LANG) {
        let url = staticVariables.WORDPRESS_URL + "/wp-json/wp/v2/static-content?slug=" + staticVariables.STATIC_CONTENT_WHY_SM_DATA + "&v=" + (Math.random() * 10000).toFixed(0);
        url = updateLangParams(url, lang);
        return {
            "url": url,
            "name": "wsm_" + lang,
            "responseName": "whySMData",
            "headers": [
                "Content-Type: application/json",
                "Accept-Language:"+ lang
            ],
            "method": "get",
            "body": "",
            "public": false,
            "time": "1"
        }
    },
    howIWData : function(lang = DEFAULT_LANG) {
        let url = staticVariables.WORDPRESS_URL + "/wp-json/wp/v2/static-content?slug=" + staticVariables.STATIC_CONTENT_HOW_IW_DATA + "&v=" + (Math.random() * 10000).toFixed(0);
        url = updateLangParams(url, lang);
        return {
            "url": url,
            "name": "hiw_"+lang,
            "responseName": "howIWData",
            "headers": [
                "Content-Type: application/json",
                "Accept-Language:"+ lang
            ],
            "method": "get",
            "body": "",
            "public": false,
            "time": "1"
        }
    },
    logosData : function(lang = DEFAULT_LANG) {
        let url = staticVariables.WORDPRESS_URL + "/wp-json/wp/v2/static-content?slug=" + staticVariables.STATIC_CONTENT_LOGOS_DATA + "&v=" + (Math.random() * 10000).toFixed(0);
        url = updateLangParams(url, lang);
        return {
            "url": url,
            "name": "logos_"+lang,
            "responseName": "logosData",
            "headers": [
                "Content-Type: application/json",
                "Accept-Language:"+ lang
            ],
            "method": "get",
            "body": "",
            "public": false,
            "time": "1"
        }
    },
    footerItemsData : function(lang = DEFAULT_LANG) {
        let footerItemsDataUrl = staticVariables.WORDPRESS_URL + "/wp-json/wp/v2/static-content?slug=" + staticVariables.STATIC_CONTENT_FOOTER_DATA + "&v=" + (Math.random() * 10000).toFixed(0);
        footerItemsDataUrl = updateLangParams(footerItemsDataUrl, lang);
        return {
            "url": footerItemsDataUrl,
            "name": "footerItems_"+lang,
            "responseName": "footerItemsData",
            "headers": [
                "Content-Type: application/json",
                "Accept-Language:"+ lang
            ],
            "method": "get",
            "body": "",
            "public": false,
            "time": "1"
        }
    },
    staticPageData: function (id, name, lang = DEFAULT_LANG) {
        let staticPageDataUrl = staticVariables.WORDPRESS_URL + "/wp-json/wp/v2/pages-content?slug=" + id + "&v=" + (Math.random() * 10000).toFixed(0);
        
        staticPageDataUrl = updateLangParams(staticPageDataUrl, lang);

        return {
            "url": staticPageDataUrl,
            "name": name+"_"+lang,
            "responseName": name,
            "headers": [
                "Content-Type: application/json",
                "Accept-Language:"+ lang
            ],
            "method": "get",
            "body": "",
            "public": false,
            "time": "1"
        }
    },
    popupOfferData: function (lang = DEFAULT_LANG) {
        return {
            "url": staticVariables.WORDPRESS_URL + "/wp-json/wp/v2/static-content?slug=" + staticVariables.STATIC_CONTENT_POP_UP_DATA,
            "name": "popupOffer_"+lang,
            "responseName": "popupOffer",
            "headers": [
                "Content-Type: application/json",
                "Accept-Language:"+ lang
            ],
            "method": "get",
            "body": "",
            "public": true,
            "time": "1"
        }
    },
    getCustomerBookingHistory: function (token, lang) {
        return {
            "url": staticVariables.CORE_API + "/v1.0/read/customer/booking",
            "headers":[
                "Content-Type: application/json",
                "Accept-Language:"+ lang,
                "Authorization: Bearer " + token
            ],
            "name": "getCustomerBookingHistory"+lang,
            "responseName": "getCustomerBookingHistory",
            "method": "get",
            "body": "",
            "public": false,
            "time": "0"
        }
    },
    getCustomerQuotesHistory: function (token, lang) {
        return {
            "url": staticVariables.CORE_API + "/v1.0/leads/list",
            
            "headers":[
                "Content-Type: application/json",
                "Accept-Language:"+ lang,
                "Authorization: Bearer " + token
            ],
            "name": "getCustomerQuotesHistory"+lang,
            "responseName": "getCustomerQuotesHistory",
            "method": "get",
            "body": "",
            "public": false,
            "time": "0"
        }
    },
    getLeadQuotes: function (token, id, lang) {
        return {
            "url": staticVariables.CORE_API + "/v1.0/leads/detail/" + id,
            "headers": [
                "Content-Type: application/json",
                "Accept-Language:"+ lang,
                "Authorization: Bearer " + token
            ],
            "name": "getLeadQuotes"+lang,
            "responseName": "getLeadQuotes",
            "method": "get",
            "body": "",
            "public": false,
            "time": "0"
        }
    },
    getCancelingReason: function (token, serviceId, feedbackType, feedbackRequestType, lang) {
        return {
            "url": staticVariables.CORE_API + "/v1/feedback/reasons?serviceId=" + serviceId + "&platform=WEB&feedbackRequestType=" + feedbackRequestType + "&feedbackType=" + feedbackType,
            "headers": [
                "Content-Type: application/json",
                "Accept-Language:"+ lang,
                "Authorization: Bearer " + token
            ],
            "method": "get",
            "name": "getCancelingReason"+lang,
            "responseName": "getCancelingReason",
            "body": "",
            "public": false,
            "time": "0"
        }
    },
    cancelCustomerBooking: function (customerId, token, bookingId, cancellationReason, comment, statusId, feedbackDto, signedInUserId, lang) {
        return {
            "url": staticVariables.CORE_API + "/v1.0/booking/status",
            
            "headers": [
                "Content-Type: application/json", 
                "accessToken: " + token, 
                "signedInCustomerId: " + customerId, 
                "signedInUserId: " + signedInUserId,
                "Accept-Language:"+ lang
            ],
            "method": "put",
            "body": JSON.stringify({
                "customerId": customerId,
                "statusId": statusId,
                "bookingId": bookingId,
                "cancellationReason": cancellationReason,
                "comments": comment,
                "feedbackDto": feedbackDto,
                "requestedBy": "customer"
            }),
            "name": "cancelCustomerBooking"+lang,
            "responseName": "cancelCustomerBooking",
            "public": false,
            "time": "0"
        }
    },
    cancelCustomerBookingEvent: function (customerId, token, eventId, cancellationReason, comment, statusId, feedbackDto, lang) {
        return {
            "url": staticVariables.CORE_API + "/v2/bookingEvent/status",
            "headers": [
                "Content-Type: application/json", 
                "accessToken: " + token, "signedInCustomerId: " + customerId,
                "Accept-Language:"+ lang
            ],
            "method": "put",
            "body": JSON.stringify([{
                "customerId": customerId,
                "statusId": statusId,
                "eventId": eventId,
                "cancellationReason": cancellationReason,
                "comments": comment,
                "feedbackDto": feedbackDto
            }]),
            "name": "cancelCustomerBookingEvent"+lang,
            "responseName": "cancelCustomerBookingEvent",
            "public": false,
            "time": "0"
        }
    },
    submitContactUsForm: function (token, message, email, firstname, lastname, phone, lang) {
        var isPublic = (token === '');
        var headers = [
            "Content-Type: application/json",
            "isCustomerLoggedIn:" + (!isPublic),
            "Accept-Language:"+ lang
        ];

        if (!isPublic) {
            headers.push("accessToken: " + token)
        }

        var data = {
            "contactInformationModel": {
                "personEmail": email,
                "personLastName": lastname,
                "personName": firstname,
                "personPhone": phone
            },
            "message": message,
        };

        data = JSON.stringify(data);
        return {
            "url": backend_url("v1.0") + "frontendControls/contact-us",
            "name": "submitContactUs"+lang,
            "responseName": "submitContactUs",
            "headers": headers,
            "method": "post",
            "body": data,
            "public": isPublic,
            "time": "0"
        }
    },
    registerCustomer: function (email, firstName, lastName, password, lang) {
        return {
            "url": staticVariables.CORE_API + "/v2/customer/register",
            "headers": [
                "Content-Type: application/json",
                "Accept-Language:"+ lang
            ],
            "name": "registerCustomer"+lang,
            "responseName": "registerCustomer",
            "method": "post",
            "body": JSON.stringify({
                "email": email,
                "firstName": firstName,
                "lastName": lastName,
                "password": password,
                "preferredLanguageCode": lang
            }),
            "public": false,
            "time": "0"
        }
    },
    updateCustomer: function (accessToken, customerId, emailAddress, firstName, lastName, address, lang ) {
        //"emailAddress": emailAddress, no need.
        return {
            "url": staticVariables.CORE_API + "/v1.0/customer/update-customer-profile",
            "headers": [
                "Content-Type: application/json", 
                "accessToken: " + accessToken, 
                "isCustomerLoggedIn: " + 1,
                "Accept-Language:"+ lang
            ],
            "method": "put",
            "name": "updateCustomer"+lang,
            "responseName": "updateCustomer",
            "body": JSON.stringify({
                "address": address,
                "firstName": firstName,
                "lastName": lastName,
                "customerId": customerId,
                "languageCode": lang
            }),
            "public": false,
            "time": "0"
        }
    },
    signInCustomer: function (email, password, lang) {
        return {
            "url": staticVariables.CORE_API + "/oauth/token",
            "headers": 
            ["Content-Type: application/x-www-form-urlencoded",
            "Accept-Language:"+ lang],
            "method": "post",
            "body": qs.stringify({
                "username": email,
                "client_id": "SM-WEB-CLIENT",
                "grant_type": "password",
                "password": password
            }),
            "name": "signInCustomer"+lang,
            "responseName": "signInCustomer",
            "public": false,
            "time": "0"
        }
    },
    resetPasswordCustomer: function (email, accessToken, lang) {
        var isPublic = (accessToken === '');
        var headers = [
            "Content-Type: application/json",
            "isCustomerLoggedIn:" + (!isPublic),
            "Accept-Language:"+ lang
        ];

        if (!isPublic) {
            headers.push("accessToken: " + accessToken)
        }

        return {
            "url": backend_url("v1.0") + "customer/" + email + "/reset-customer-login-password",
            "name": "resetPassword"+lang,
            "responseName": "resetPassword",
            "headers": headers,
            "method": "put",
            "body": "",
            "public": isPublic,
            "time": "0"
        }
    },
    userDetailsByResetTokenCustomer: function (resetToken, lang) {
        var headers = [
            "Content-Type: application/json",
            "Accept-Language:"+ lang
        ];
        return {
            "url": backend_url("v1.0") + "customer/reset-password?token=" + resetToken,
            "name": "userDetailsByResetToken"+lang,
            "responseName": "userDetailsByResetToken",
            "headers": headers,
            "method": "get",
            "body": "",
            "public": true,
            "time": "0"
        }
    },
    updateCustomerPassword: function (email, newPassword, customerID, resetToken, accessToken, lang) {
        var isPublic = (accessToken === '');
        var headers = [
            "Content-Type: application/json",
            "isCustomerLoggedIn: " + (!isPublic),
            "signedInCustomerId: " + customerID,
            "Accept-Language:"+ lang
        ];
        if (!isPublic) {
            headers.push("accessToken: " + accessToken)
        }

        return {
            "url": backend_url("v1.0") + "customer/update-customer-login-password",
            "name": "updatePassword"+lang,
            "responseName": "updatePassword",
            "headers": headers,
            "method": "put",
            "body": JSON.stringify({
                "customerId": customerID,
                "newPassword": newPassword,
                "resetToken": resetToken
            }),
            "public": isPublic,
            "time": "0"
        }
    },
    // client_id=SM-WEB-CLIENT&grant_type=token_exchange&access_token=token_data&social_login_type=google
    socialSignInCustomer: function (token, type, lang) {
        return {
            "url": staticVariables.CORE_API + "/oauth/token",
            "headers": ["Content-Type: application/x-www-form-urlencoded","Accept-Language:"+ lang],
            "method": "post",
            "body": qs.stringify({
                "access_token": token,
                "client_id": "SM-WEB-CLIENT",
                "grant_type": "token_exchange",
                "social_login_type": type
            }),
            "name": "socialSignInCustomer",
            "responseName": "socialSignInCustomer",
            "public": false,
            "time": "0"
        }
    },
    getCreditCard: function (accessToken, lang) {
        return {
            "url": backend_url("v2") + "creditCards/",
            "headers": [
                "Content-Type: application/json",
                "Authorization: Bearer " + accessToken,
                "Accept-Language:"+ lang
            ],
            "method": "get",
            "name": "getCreditCard"+lang,
            "responseName": "getCreditCard",
            "body": "",
            "public": false,
            "time": "0"
        }
    },
    verifyCreditCard: function (verifyToken, accessToken, lang) {
        
        return {
            "url": backend_url("v2") + "creditCards/verify/" + verifyToken,
            "headers": [
                "Content-Type: application/json",
                "Authorization: Bearer " + accessToken,
                "Accept-Language:"+ lang
            ],
            "method": "post",
            "name": "verifyCreditCard"+lang,
            "responseName": "verifyCreditCard",
            "body": "",
            "public": false,
            "time": "0"
        }
    },
    registerCreditCard: function (accessToken, signedInCustomerId, cancelURL, declineURL, successURL, lang) {
        return {
            "url": backend_url("v2") + "creditCards/registration",
            "headers": [
                "Content-Type: application/json",
                "Authorization: Bearer " + accessToken,
                "signedInCustomerId: " + signedInCustomerId,
                "Accept-Language:"+ lang
            ],
            "name": "registerCreditCard"+lang,
            "responseName": "registerCreditCard",
            "method": "post",
            "body": JSON.stringify({
                "cancelURL": cancelURL,
                "declineURL": declineURL,
                "successURL": successURL
            }),
            "public": false,
            "time": "0"
        }
    },
    addRequest: function (accessToken, data, url, signedInUserId, lang = LANG_EN ) {

        var isPublic = (accessToken === '');
        var headers = [
            "Content-Type: application/json",
            "isCustomerLoggedIn:" + (!isPublic),
            "signedInUserId:" + (signedInUserId != 0 && parseInt(signedInUserId) > 0 ? parseInt(signedInUserId) : 0),
            "Accept-Language:"+ lang
        ];
        
        if (!isPublic) {
            headers.push("accessToken: " + accessToken)
        }

        return {
            "url": backend_url("v1.0") + url,
            "name": "addRequest"+lang,
            "responseName": "addRequest",
            "headers": headers,
            "method": "post",
            "body": data,
            "public": isPublic,
            "time": "0"
        }
    },
    createBookingRequestDto: function (accessToken, data, signedInUserId, lang) {

        var isPublic = (accessToken === '');
        var headers = [
            "Content-Type: application/json",
            "isCustomerLoggedIn:" + (!isPublic),
            "signedInUserId:" + (signedInUserId != 0 && parseInt(signedInUserId) > 0 ? parseInt(signedInUserId) : 0),
            "Accept-Language:"+ lang
        ];

        if (!isPublic) {
            headers.push("accessToken: " + accessToken)
        }

        return {
            "url": backend_url("v2")+"booking",
            "name": "addBookingRequest"+lang,
            "responseName": "addBookingRequest",
            "headers": headers,
            "method": "post",
            "body": data,
            "public": isPublic,
            "time": "0"
        }
    },
    unsubscribeBooking: function (accessToken, bookingID, lang) {
        var headers = [
            "Content-Type: application/json",
            "Authorization: Bearer " + accessToken,
            "Accept-Language:"+ lang
        ];

        return {
            "url": backend_url("v1.0") + "booking/" + bookingID + "/unsubscribe",
            "name": "unsubscribeBooking"+lang,
            "responseName": "unsubscribeBooking",
            "headers": headers,
            "method": "put",
            "public": false,
            "time": "0"
        }
    },
    freezeBooking: function (accessToken, bookingID, startDate, endDate, lang) {

        let data = {
            "bookingId": bookingID,
            "startDate": startDate,
            "endDate": endDate
        };

        data = JSON.stringify(data);
        
        var headers = [
            "Content-Type: application/json",
            "Authorization: Bearer " + accessToken,
            "Accept-Language:"+ lang
        ];

        return {
            "url": backend_url("v1.0") + "booking/freeze",
            "name": "freezeBooking"+lang,
            "responseName": "freezeBooking",
            "headers": headers,
            "method": "put",
            "body": data,
            "public": false,
            "time": "0"
        }
    },
    newChangeBookingRequest:function(accessToken, initiatorId, newData, lang){
        let data = JSON.stringify(newData);
        var headers = [
            "Content-Type: application/json",
            "Authorization: Bearer " + accessToken,
            /*"accessToken: " + accessToken,
            "signedInUserId: " + initiatorId,*/
            "Accept-Language:"+ lang
        ];

        return {
            "url": backend_url("v2") + "booking-change-request",
            "name": "changeRequest"+lang,
            "responseName": "changeRequest",
            "headers": headers,
            "method": "post",
            "body": data,
            "public": false,
            "time": "0"
        }
    },
    changeBookingRequest: function (accessToken, bookingID, initiatorId, bookingEventId, changeType, newDate, newTime, newDuration, newNumberOfWorkers, initiatedBy, lang) {
        let data = {
            "bookingId": bookingID,
            "initiatedBy": initiatedBy,
            "initiatorId": initiatorId
        };

        if (bookingEventId !== '') {
            data["bookingEventId"] = bookingEventId;
        }

        // 1 for changing date, 2 for changing duration and workers, 3 for changing time
        if (changeType == 1) {
            data['newDate'] = newDate;
        } else if (changeType == 2) {
            data['newDuration'] = newDuration;
            data['newNumberOfWorkers'] = newNumberOfWorkers;
        } else if (changeType == 3) {
            data['newTime'] = newTime.value;
        }

        data = JSON.stringify(data);
        
        var headers = [
            "Content-Type: application/json",
            "Authorization: Bearer " + accessToken,
            "accessToken: " + accessToken,
            "signedInUserId: " + initiatorId,
            "Accept-Language:"+ lang
        ];

        return {
            "url": backend_url("v1.0") + "booking-change-request",
            "name": "changeRequest"+lang,
            "responseName": "changeRequest",
            "headers": headers,
            "method": "post",
            "body": data,
            "public": false,
            "time": "0"
        }
    },
    addPhotographyRequest: function (accessToken, data, signedInUserId, lang) {

        let isPublic = (accessToken === '');
        
        let headers = [
            "Content-Type: application/json",
            "signedInUserId:" + (signedInUserId && parseInt(signedInUserId) > 0 ? parseInt(signedInUserId) : 0),
            "isCustomerLoggedIn:" + (!isPublic),
            "Accept-Language:"+ lang
        ];

        if (!isPublic) {
            headers.push("accessToken: " + accessToken)
        }

        return {
            "url": backend_url("v1.0") + "photography",
            "name": "addRequest"+lang,
            "responseName": "addRequest",
            "headers": headers,
            "method": "post",
            "body": data,
            "public": isPublic,
            "time": "0"
        }
    },
    addLWRequest: function (data, lang) {
        var isPublic = true;

        var headers = [
            "Content-Type: application/json",
            "Accept-Language:"+ lang
        ]

        return {
            "name": "addLWRequest"+lang,
            "responseName": "addLWRequest",
            "headers": headers,
            "method": "post",
            "body": data,
            "public": isPublic,
            "time": "0"
        }
    },
    updateLWRequest: function (data, lang) {
        var isPublic = true;

        var headers = [
            "Content-Type: application/json",
            "Accept-Language:"+ lang
        ]

        return {
            "name": "updateLWRequest"+lang,
            "responseName": "updateLWRequest",
            "headers": headers,
            "method": "post",
            "body": data,
            "public": isPublic,
            "time": "0"
        }
    },
    deleteLWRequest: function (data, lang) {
        var isPublic = true;

        var headers = [
            "Content-Type: application/json",
            "Accept-Language:"+ lang
        ]

        return {
            "name": "deleteLWRequest"+lang,
            "responseName": "deleteLWRequest",
            "headers": headers,
            "method": "DELETE",
            "public": isPublic,
            "time": "0"
        }
    },
    activateRequest: function (accessToken, signedInCustomerId, bookingId, creditCardId = 0, paymentMethod, couponValidationModel = {}, isNewBookingEngine = false, lang) {
        let data = {};
        if(isNewBookingEngine){
            let cardId = creditCardId == 0 ? null : creditCardId;
            data = {
                "payment_method": {
                    "creditCardId": cardId,
                    "paymentMethod": paymentMethod
                }
            };
            if (Object.keys(couponValidationModel).length) {
                data["voucher"] = couponValidationModel;
            }
       }else{
            data = {
                "bookingID": bookingId,
                "paymentModel": {
                    "creditCardId": creditCardId,
                    "paymentMethod": paymentMethod
                }
            }
            if (Object.keys(couponValidationModel).length) {
                data["couponValidationModel"] = couponValidationModel;
            }
        }
        let endPoint = "activateSoftlySavedBooking";
        if(isNewBookingEngine){
            endPoint = "complete-booking-request";
        }
        return {
            "url": backend_url("v2") + "booking/" + bookingId + "/"+endPoint,
            "headers": [
                "Content-Type: application/json",
                "Authorization: Bearer " + accessToken,
                "Accept-Language:"+ lang
            ],
            "method": "put",
            "body": JSON.stringify(data),
            "name": "activateRequest"+lang,
            "responseName": "activateRequest",
            "public": false,
            "time": "0"
        }
    },
    removeCreditCard: function (accessToken, creditCardId, lang) {
        return {
            "url": backend_url("v2") + "creditCards/remove/" + creditCardId,
            "headers": [
                "Content-Type: application/json",
                "Authorization: Bearer " + accessToken,
                "Accept-Language:"+ lang
            ],
            "method": "put",
            "name": "removeCreditCard"+lang,
            "responseName": "removeCreditCard",
            "body": "",
            "public": false,
            "time": "0"
        }
    },
    getAllServices: function (lang) {
        var service_url = backend_url() + "custom-services/all";
        return {
            "url": service_url,
            "name": "servicesAPI"+lang,
            "responseName": "services",
            "headers": [
                "Content-Type:application/json",
                "Accept-Language:"+lang
            ],
            "method": "get",
            "body": "",
            "public": true,
            "time": "1"
        }
    },
    getCountriesListAPI: function (lang) {
        var service_url = backend_url() + "read/country-city-area";
        return {
            "url": service_url,
            "name": "CountriesListAPI"+lang,
            "responseName": "countries",
            "headers": [
                "Content-Type:application/json",
                "Accept-Language:"+lang
            ],
            "method": "get",
            "body": "",
            "public": true,
            "time": "1"
        }
    },
    getCarModelYearsAPI: function (lang) {
        var service_url = backend_url() + "car-model/years";
        return {
            "url": service_url,
            "name": "carYearsAPI"+lang,
            "responseName": "carYears",
            "headers": [
                "Content-Type:application/json",
                "Accept-Language:"+lang
            ],
            "method": "get",
            "body": "",
            "public": true,
            "time": "1"
        }
    },
    getCarDetailsAPI: function (year, lang) {
        var service_url = backend_url() + 'car-model'//"car-model/model-detail/" + year;
        return {
            "url": service_url,
            "name": "CarDetailsAPI",
            "responseName": "CarDetails",
            "headers": [
                "Content-Type:application/json",
                "Accept-Language:"+lang
            ],
            "method": "get",
            "body": "",
            "public": true,
            "time": "1"
        }
    },
    getDeals: function (token, locationCode, pageServiceID, movingSize, lang) {
        var service_url = backend_url() + "deal?locationCode=" + locationCode + "&pageServiceId=" + pageServiceID + "&reportingSize=" + movingSize;
        var header = [
            "Content-Type: application/json",
            "Accept-Language:"+ lang
        ]
        var isPublic = true;

        if (token != "") {
            header.push("Authorization: Bearer " + token);
            isPublic = false;
        }

        return {
            "url": service_url,
            "name": "deal" + locationCode + pageServiceID + movingSize.split(' ').join('-')+lang,
            "responseName": "offers",
            "headers": header,
            "method": "get",
            "body": "",
            "public": isPublic,
            "time": "0"
        }
    },
    getCustomerDetails: function (token, lang) {
        return {
            "url": staticVariables.CORE_API + "/v1.0/read/customerDetails",
            "headers": [
                "Content-Type: application/x-www-form-urlencoded", 
                "Authorization: Bearer " + token,
                "Accept-Language:"+ lang
            ],
            "method": "get",
            "name": "getCustomerDetails"+lang,
            "responseName": "getCustomerDetails",
            "public": false,
            "time": "0"
        }
    },
    getWalletHistory: function (token, lang) {
        return {
            "url": staticVariables.CORE_API + "/v1.0/wallet/transactions",
            "headers": [
                "Content-Type: application/x-www-form-urlencoded", 
                "Authorization: Bearer " + token,
                "Accept-Language:"+ lang
            ],
            "method": "get",
            "name": "getWalletHistory"+lang,
            "responseName": "walletHistory",
            "public": false,
            "time": "0"
        }
    },
    pricePlan: function (serviceId, cityId, isSubscription = false, lang) {
        var price_plan_url = backend_url() + "pricePlan/" + serviceId + "/" + cityId;
        if (isSubscription == true) {
            price_plan_url = backend_url() + "pricePlan/subscription/" + serviceId + "/" + cityId + "/1";
        }
        return {
            "url": price_plan_url,
            "name": "pricePlan" + serviceId + "_" + cityId + "_" + isSubscription+"_"+lang,
            "responseName": "pricePlan",
            "headers": [
                "Content-Type: application/json",
                "Accept-Language:"+ lang
            ],
            "method": "get",
            "body": "",
            "public": true,
            "time": "1"
        }
    },
    taxPlan: function (cityId, lang) {
        var tax_plan_url = backend_url("v1") + "taxRate/" + cityId;
        return {
            "url": tax_plan_url,
            "name": "taxPlanAPI" + cityId+lang,
            "responseName": "taxPlan",
            "headers": [
                "Content-Type: application/json",
                "Accept-Language:"+ lang
            ],
            "method": "get",
            "body": "",
            "public": true,
            "time": "1"
        }
    },
    couponValidate: function (data, isCustomerLoggedIn, user_token, signedInUserId, signedInCustomerId, lang) {
        var coupon_validate_url = backend_url() + "coupon/validate";
        var header = [
            "Content-Type: application/json",
            "isCustomerLoggedIn:" + isCustomerLoggedIn,
            "Accept-Language:"+ lang
        ]
        var isPublic = true;

        if (user_token != "") {
            header.push("accessToken:" + user_token);
            header.push("signedInUserId:" + signedInUserId);
            header.push("signedInCustomerId:" + signedInCustomerId);
            isPublic = false;
        }
        return {
            "url": coupon_validate_url,
            "name": "couponValidate"+lang,
            "responseName": "coupon",
            "headers": header,
            "method": "post",
            "body": JSON.stringify(data),
            "public": isPublic,
            "time": "0"
        }
    },
    addReview: function (accessToken, signedInCustomerId, data, lang) {
        var header = [
            "Content-Type: application/json",
            "Accept-Language:"+ lang
        ]

        var isPublic = true;

        if (accessToken != "") {
            header.push("accessToken:" + accessToken);
            header.push("signedInCustomerId:" + signedInCustomerId);
            isPublic = false;
        }
        header.push("isCustomerLoggedIn:" + !isPublic);
        return {
            "url": backend_url("v1.0") + "review/add.json",
            "headers": header,
            "method": "put",
            "public": isPublic,
            "time": "0",
            "body": JSON.stringify({
                "rating": data.rating,
                "reviewTitle": data.reviewTitle,
                "reviewText": data.reviewText,
                "moverId": data.moverId,
                "serviceId": data.serviceId,
                "serviceLocationId": data.serviceLocationId,
                "serviceLocationIdsToAdd": [
                    data.serviceLocationId
                ],
                "status": data.status,
                "contactInformationModel": data.contactInformationModel
            }),
            "name": "addReview",
            "responseName": "addReview",
        }
    },
    getReviews: function (cityCode, serviceID, lang) {
        return {
            "url": backend_url("v1.0") + "review/" + cityCode + "?serviceId=" + serviceID,
            "headers": [
                "Content-Type: application/json",
                "Accept-Language:"+ lang
            ],
            "name": "reviews" + cityCode + serviceID+lang,
            "responseName": "reviews",
            "method": "get",
            "body": "",
            "public": true,
            "time": "1"
        }
    },
    becomeAPartner: function (accessToken, signedInCustomerId, data, lang) {
        var isPublic = (accessToken === '');
        var headers = [
            "Content-Type: application/json",
            "isCustomerLoggedIn:" + (!isPublic),
            "Accept-Language:"+ lang
        ];

        if (!isPublic) {
            headers.push("accessToken: " + accessToken)
            headers.push("signedInCustomerId: " + signedInCustomerId)
        }

        return {
            "url": backend_url("v1.0") + "frontendControls/become-a-partner",
            "name": "becomeAPartnerPost"+lang,
            "responseName": "becomeAPartnerPost",
            "headers": headers,
            "method": "post",
            "body": JSON.stringify(data),
            "public": isPublic,
            "time": "0"
        }
    },
    voucherValidate: function (data, userToken, lang) {
        var coupon_validate_url = backend_url() + "voucher/validate";

        var isPublic = false;

        var headers = [
            "Content-Type: application/json",
            "Accept-Language:"+ lang
        ];

        if (userToken != "") {
            isPublic = true;
            headers.push("authorization: bearer " + userToken);
        }

        return {
            "url": coupon_validate_url,
            "name": "voucherValidate"+lang,
            "responseName": "voucher",
            "headers": headers,
            "method": "post",
            "body": data,
            "public": true,
            "time": "0"
        }
    },
    voucherRedemption: function (data, userToken, lang) {
        var coupon_validate_url = backend_url() + "voucher/redeem";

        var headers =[
            "Content-Type: application/json",
            "Accept-Language:"+ lang
        ]

        var isPublic = false;

        if (userToken != "") {
            headers.push("authorization: bearer " + userToken);
        }

        return {
            "url": coupon_validate_url,
            "name": "voucherRedemption"+lang,
            "responseName": "voucherRedemption",
            "headers": headers,
            "method": "post",
            "body": data,
            "public": isPublic,
            "time": "0"
        }
    },
    dateTimeAvailability: function (accessToken = '', lang) {
        var date_time_availability_url = backend_url() + "booking/date-time-availability";

        var isPublic = (accessToken === '');
        var headers = [
            "Content-Type: application/json",
            "Accept-Language:"+ lang,
            "isCustomerLoggedIn:" + (!isPublic)
        ];

        if (!isPublic) {
            headers.push("accessToken: " + accessToken)
        }

        return {
            "url": date_time_availability_url,
            "name": "dateTimeAvailability"+lang,
            "responseName": "DateTimeAvailability",
            "headers": headers,
            "method": "get",
            "body": "",
            "public": isPublic,
            "time": "0"
        }
    },

    getPricer(data, isCustomerLoggedIn, user_token, lang) {
        var pricerUrl = backend_url() + "pricer/get-price";
        var header = [
            "Content-Type: application/json",
            "Accept-Language:"+ lang,
            "isCustomerLoggedIn:" + isCustomerLoggedIn
        ]

        var isPublic = true;

        if (user_token != "") {
            header.push("accessToken:" + user_token);
            isPublic = false;
        }

        return {
            "url": pricerUrl,
            "name": "getPrice"+lang,
            "responseName": "price",
            "headers": header,
            "method": "post",
            "body": data,
            "public": isPublic,
            "time": "0"
        }
    },
    dataDictionaryValues: function (lang) {
        var data_dictionary_url = backend_url() + "read/data-dictionary-values";
        return {
            "url": data_dictionary_url,
            "name": "dataDictionary"+lang,
            "responseName": "dataConstants",
            "headers": [
                "Content-Type: application/json",
                "Accept-Language:"+ lang
            ],
            "method": "get",
            "body": "",
            "public": true,
            "time": "1"
        }
    },
    dataConstantValues: function (lang) {
        var data_dictionary_url = backend_url("v1") + "constants" + "?v=" + (Math.random() * 10000).toFixed(0);
        return {
            "url": data_dictionary_url,
            "name": "dataConstant"+lang,
            "responseName": "dataConstants",
            "headers": [
                "Content-Type: application/json",
                "Accept-Language:"+ lang
            ],
            "method": "get",
            "body": "",
            "public": true,
            "time": "1"
        }
    },
    getSimilarCompanies: function (partnerId, lang) {
        const getSimilarPartnersUrl = staticVariables.CORE_API + "/v1.0/service-provider/similar/" + partnerId;
        return {
            "url": getSimilarPartnersUrl,
            "name": "similar12312312"+lang,
            "responseName": "similarProviders",
            "headers": [
                "Content-Type: application/json",
                "Accept-Language:"+ lang
            ],
            "method": "get",
            "body": "",
            "public": true,
            "time": "0"
        }
    },
    getWorkersDetails: function (bookingEventId, lang) {
        var getWorkersDetailsUrl = backend_url() + "service-provider/bookedEventWorker";
        var headers = [
            "Content-Type: application/json",
            "bookingEventId:" + bookingEventId,
            "Accept-Language:"+ lang
        ]
        return {
            "url": getWorkersDetailsUrl,
            "name": "getWorkersDetails"+lang,
            "responseName": "workersDetails",
            "headers": headers,
            "method": "get",
            "public": true,
            "time": "0"
        }
    },
    isEventReviewed: function (bookingEventId, lang) {
        var eventReviewUrl = backend_url() + "customer/eventReview";
        
        var headers = [
            "Content-Type: application/json",
            "eventId:" + bookingEventId,
            "Accept-Language:"+ lang
        ];
        return {
            "url": eventReviewUrl,
            "name": "getEventReview"+lang,
            "responseName": "eventReview",
            "headers": headers,
            "method": "get",
            "public": true,
            "time": "0"
        }
    },
    submitServiceReview: function (data, lang) {
        var headers = [
            "Content-Type: application/json",
            "Accept-Language:"+ lang
        ];
        return {
            "name": "submitServiceReview"+lang,
            "responseName": "submitServiceReview",
            "headers": headers,
            "method": "post",
            "body": data,
            "public": true,
            "time": "0"
        }
    },
    isServiceReviewed: function (lang) {
        var headers = [
            "Content-Type: application/json",
            "Accept-Language:"+ lang
        ];
        return {
            "name": "isServiceReviewed"+lang,
            "responseName": "isServiceReviewed",
            "headers": headers,
            "method": "get",
            "public": true,
            "time": "0"
        }
    },
    submitMaidReview: function (data, lang) {
        var submitMaidUrl = backend_url() + "customer/workerReview";
        var headers =[
            "Content-Type: application/json",
            "Accept-Language:"+ lang
        ];
        return {
            "url": submitMaidUrl,
            "name": "submitMaid"+lang,
            "responseName": "submitMaidreview",
            "headers": headers,
            "method": "post",
            "public": true,
            "body": data,
            "time": "0"
        }
    },
    zohoMigratedService: function (lang) {
        var zohoMigratedServiceUrl = backend_url("v1.0") + "/custom-services/zoho-migrated-service/list";
        return {
            "url": zohoMigratedServiceUrl,
            "name": "zohoMigratedService_"+lang,
            "responseName": "zohoMigratedService",
            "headers": [
                "Content-Type: application/json",
                "Accept-Language:"+ lang
            ],
            "method": "get",
            "body": "",
            "public": true,
            "time": "1"
        }
    },
    

};

//export const SITE_ENVIRONMENT = "dev";

export function backend_url(apiVersion = 'v1.0') {

    var url = '';

    if (apiVersion === false) {
        apiVersion = '';
    } else {
        apiVersion += '/';
    }

    /* if ( typeof SITE_ENVIRONMENT != "undefined") {

        switch (SITE_ENVIRONMENT) {

            case 'dev':

                url = 'https://uat1-api.servicemarket.com/' . apiVersion;
                break;

            case 'test':
                url = 'https://uat2-api.servicemarket.com/' . apiVersion;
                break;

            case 'prod':
                url = 'https://api.servicemarket.com/' . apiVersion;
                break;
        }
    }
    else {*/

    url = staticVariables.CORE_API + '/' + apiVersion;

    //}

    return url;
}
