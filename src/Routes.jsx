import React from "react";
import Loadable from 'react-loadable';
import commonHelper from "./helpers/commonHelper";
//import GeneralModal from "./components/GeneralModal";
import Loader from "./components/Loader";

const loading = () => (<Loader />)

const HomePage = Loadable({
    loader: () => import('./pages/HomePage'),
    loading
});

const GeneralModal = Loadable({
    loader: () => import("./components/GeneralModal"),
    loading
});

const LandingPage = Loadable({
    loader: () => import('./pages/LandingPage'),
    loading
});

const AboutPage = Loadable({
    loader: () => import('./pages/AboutPage'),
    loading
});
const BookingFormPage = Loadable({
    loader: () => import('./pages/forms/BookingFormPage'),
    loading
});
const QuotesFormPage = Loadable({
    loader: () => import('./pages/forms/QuotesFormPage'),
    loading
});

/*const ContactPage = Loadable({
    loader: () => import('./pages/forms/ContactPage'),
    loading
});*/

const ContactPage = Loadable({
    loader: () => import('./pages/ContactPage'),
    loading
});
const PartnerPage = Loadable({
    loader: () => import('./pages/PartnerPage'),
    loading
});
const ReviewPage = Loadable({
    loader: () => import('./pages/ReviewPage'),
    loading
});
const TeamPage = Loadable({
    loader: () => import('./pages/TeamPage'),
    loading
});
const FaqsPage = Loadable({
    loader: () => import('./pages/FaqsPage'),
    loading
});

const PrivacyPage = Loadable({
    loader: () => import('./pages/PrivacyPage'),
    loading
});

const DisclaimerPage = Loadable({
    loader: () => import('./pages/DisclaimerPage'),
    loading
});

const TermsPage = Loadable({
    loader: () => import('./pages/TermsPage'),
    loading
});

const PrpPage = Loadable({
    loader: () => import('./pages/PrpPage'),
    loading
});

const LoginPage = Loadable({
    loader: () => import('./pages/LoginPage'),
    loading
});
const CompaniesPage = Loadable({
    loader: () => import('./pages/CompaniesPage'),
    loading
});
/*const CompaniesPage = Loadable({
    loader: () => import('./pages/CompaniesPage'),
    loading
});*/
const UserAreaPage = Loadable({
    loader: () => import('./pages/UserAreaPage'),
    loading
});
const PaymentConfirmation = Loadable({
    loader: () => import('./pages/PaymentConfirmation'),
    loading
});
const QuotesConfirmation = Loadable({
    loader: () => import('./pages/QuotesConfirmation'),
    loading
});
const ZohoAcceptBooking = Loadable({
    loader: () => import('./pages/ZohoAcceptBooking'),
    loading
});
const CompanyPage = Loadable({
    loader: () => import('./pages/CompanyPage'),
    loading
});
const ServiceReviewsPage = Loadable({
    loader: () => import('./pages/ServiceReviewsPage'),
    loading
});
const WriteReviewForMaidAndService = Loadable({
    loader: () => import('./pages/WriteReviewForMaidAndService'),
    loading
});
const DryCleaningPrice = Loadable({
    loader: () => import('./pages/forms/booking/DryCleaningPrice'),
    loading
});

  
const ViewBookingFormPage = (match) => {
    //console.log( match );
    var object_match = match.match;
    let showOnlyPriceList = false;
    if(match.location.search != ""){
        let priceList = commonHelper.getParameterByName("price_list", match.location.pathname+""+match.location.search);
        showOnlyPriceList = ( (priceList != null && priceList != "") && priceList == "yes" );
    }
    return showOnlyPriceList ? (<GeneralModal 
        title="Laundry &amp; Dry Cleaning Pricing"
        headerClass="bg-primary"
        titleClass="text-white h3 m-0"
        modalClass="price-list"
        showHeader={true} 
        modalSize="modal-lg" 
        modalBody={(<DryCleaningPrice />)} />) : (<BookingFormPage url_params={object_match} pathLocation={match.location} />);
}
const ViewQuotesFormPage = (match) => {
    //console.log( match );
    var object_match = match.match;
    
    return (<QuotesFormPage url_params={object_match} />);
}
const ViewQuotesConfirmationPage = (match) => {
    var object_match = match.match.params.rid;
    //console.log("match", match);
    //console.log(object_match, object_match.params.slug);
    return (<QuotesConfirmation rid={object_match} url_params={match.match} quotesParams = {match.location.search}/>);
}
const ViewBookingConfirmationPage = (match) => {
    var object_match = match.match.params.bookingID;
    //console.log( object_match );
    //console.log(object_match, object_match.params.slug);
    return (<PaymentConfirmation rid={object_match} url_params={match.match}  />);
}
const ZohoAcceptBookingPage = (match) => {
    var resp = {};
    return (<ZohoAcceptBooking rid="123" resp={resp} />);
}
const maidReview = (match) => {
    var object_match = match.match;
    return (<WriteReviewForMaidAndService urlParams={object_match}/>);
}
const serviceReview = (match) => {
    var object_match = match.match;
    return (<WriteReviewForMaidAndService urlParams={object_match}/>);
 }

 const writeReviewPage = (match) => {
    var object_match = match.match;
    //console.log("writeReviewPage", object_match);
    return (<ReviewPage urlParams={object_match}/>);
 }

export default [
    {
        path: "/reset/:resetKey",
        component: HomePage,
        exact: true
    },
    {
        path: "/service-provider/:provider",
        component: CompanyPage,
        exact: true
    },
    {
        path: "/:lang/rate-your-service/:leadId",
        component: serviceReview,
        exact: true
    },
    {
        path: "/:lang/users/:typeOfReview/:eventId",
        component: maidReview,
        exact: true
    },
    {
        path: "/:lang/profile",
        component: UserAreaPage,
        exact: true
    },
    {
        path: "/:lang/login",
        component: LoginPage,
        exact: true
    },
    {
        path: "/:lang/about-us",
        component: AboutPage,
        exact: true
    },
    {
        path: "/:lang/become-a-partner",
        component: PartnerPage,
        exact: true
    },
    {
        path: "/:lang/write-a-review/:url",
        component: writeReviewPage,
        exact: true
    },
    {
        path: "/:lang/write-a-review",
        component: writeReviewPage,
        exact: true
    },
    {
        path: "/:lang/careers",
        component: TeamPage,
        exact: true
    },
    {
        path: "/:lang/faqs",
        component: FaqsPage,
        exact: true
    },
    {
        path: "/:lang/contact-us",
        component: ContactPage,
        exact: true
    },
    {
        path: "/:lang/privacy-policy",
        component: PrivacyPage,
        exact: true
    },
    {
        path: "/:lang/disclaimer",
        component: DisclaimerPage,
        exact: true
    },
    {
        path: "/:lang/terms-and-conditions",
        component: TermsPage,
        exact: true
    },
    {
        path: "/:lang/payment-and-refund-policy",
        component: PrpPage,
        exact: true
    },
    {
        path: "/book-direct/zohosm",
        component: ZohoAcceptBookingPage,
        exact: true
    },
    {
        path: "/:lang/:city",
        component: HomePage,
        exact: true
    },
    {
        path: "/:lang/:city/partners-list",
        component: CompaniesPage,
        exact: true
    },
    {
        path: "/:lang/:city/:slug/",
        component: LandingPage,
        exact: true
    },
    {
        path: "/:lang/:city/:slug/reviews",
        component: ServiceReviewsPage,
        exact: true
    },
    {
        path: "/:lang/:city/:slug/book-online",
        component: ViewBookingFormPage,
        exact: true
    },
    {
        path: "/:lang/:city/:slug/:subservice/book-online",
        component: ViewBookingFormPage,
        exact: true
    },
    {
        path: "/:lang/:city/:slug/book-online/:subscription",
        component: ViewBookingFormPage,
        exact: true
    },
    {
        path: "/:lang/:city/:slug/journey1",
        component: ViewQuotesFormPage,
        exact: true
    },
    {
        path: "/:lang/:city/:slug/:journey/step2",
        component: ViewQuotesFormPage,
        exact: true
    },
    {
        path: "/:lang/:city/:slug/book-online/confirmation/:bookingID",
        component: ViewBookingConfirmationPage,
        exact: true
    },
    {
        path: "/:lang/:city/:slug/:subservice/book-online/confirmation/:bookingID",
        component: ViewBookingConfirmationPage,
        exact: true
    },
    {
        path: "/:lang/:city/:slug/book-online/:subscription/confirmation/:bookingID",
        component: ViewBookingConfirmationPage,
        exact: true
    },
    {
        path: "/:lang/:city/:slug/:journey/confirmation/:rid",
        component: ViewQuotesConfirmationPage,
        exact: true
    },
    {
        path: "/:lang/:city/profile/success/:verifyToken",
        component: UserAreaPage,
        exact: true
    },
    {
        path: "/:lang/:city/profile/decline/:verifyToken",
        component: UserAreaPage,
        exact: true
    },
    {
        path: "/:lang/:city/profile/cancel/:verifyToken",
        component: UserAreaPage,
        exact: true
    },
    {
        path: "/:lang/:city/:slug/book-online/:bookingID/success/:verifyToken",
        component: PaymentConfirmation,
        exact: true
    },
    {
        path: "/:lang/:city/:slug/book-online/cancel/:verifyToken",
        component: ViewBookingFormPage,
        exact: true
    },
    {
        path: "/:lang/:city/:slug/book-online/declined/:verifyToken",
        component: ViewBookingFormPage,
        exact: true
    }
    
    /*{
     path: "/:lang/:city/:slug/book-online/",
     component: BookMovingPage,
     exact: true
     },*/
];
