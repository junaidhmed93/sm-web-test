const stringConstants = {
  MY_WALLET: 'My Wallet',
  USER_AREA_WALLET_TXT: 'Your wallet amount is automatically deducted from your next booking charges.',
  EMPTY_WALLET_TXT: 'You\'re wallet does not have any credit at the moment.',
  YOU_HAVE_TXT: 'You have',
  WALLET_BALANCE_TXT: 'in your wallet. This amount will be adjusted from your total payment that is due.',
  WALLET: 'Wallet',
  WALLET_AMOUNT_RECEIVED:'Wallet Amount Received',
  BOOKING_ID:'Booking ID ',
  CREDIT :'CREDIT',
  PARTNER_WELCOME_TXT:'Thank you for your interest in joining ServiceMarket. Our sales team will contact within 1-2 working days to discuss your request',
  CANCEL_BOOKING_SUCCESS_TXT:'Booking cancelled. If you need to rebook you can contact us on +971 4 4229639 or email us on bookings@servicemarket.com.',
  CANCEL_BOOKING_FAIL_TXT: 'Something went wrong, Please contact ServiceMarket.',
  SEARCH_HELP_TXT: 'E.g. Cleaner, Moving, Plumbing, Handyman, Maid',
  YEAR_1950:1950,
  WALLET_BOOKING_DETAIL_TXT:'Wallet amount',
  WALLET_BOOKING_DETAIL_TXT:'Wallet credit',
  TRANSACTION_HISTORY_TXT:'Transaction history'
}

export default stringConstants