import axios from 'axios';
import { endPoints } from "../api";
//import commonHelper from "../helpers/commonHelper";
import locationHelper from "../helpers/locationHelper";
import staticVariables from "../staticVariables";
import stringConstants from '../constants/stringConstants';
/*const dotenv = require('dotenv');
dotenv.config();
if(typeof process.env.NODE_ENV != "undefined" && process.env.NODE_ENV == "local"){
	process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';
}*/
axios.defaults.baseURL = staticVariables.MIDDLEWARE_URL + '?url=/&time=60&type=api_set';
// Constants
export const SET_HOMEPAGE_DATA = 'SET_HOMEPAGE_DATA';
export const SET_LANDING_DATA = 'SET_LANDING_DATA';
export const SET_LANDING_CONFIRMATION_DATA = 'SET_LANDING_CONFIRMATION_DATA';
export const SET_ABOUT_DATA = 'SET_ABOUT_DATA';
export const SET_BOT = 'SET_BOT';
export const SET_BECOME_PARTNER_DATA = 'SET_BECOME_PARTNER_DATA';
export const SET_REVIEW_DATA = 'SET_REVIEW_DATA';
export const SET_TEAM_DATA = 'SET_TEAM_DATA';
export const SET_FAQS_DATA = 'SET_FAQS_DATA';
export const SET_PRIVACY_DATA = 'SET_PRIVACY_DATA';
export const SET_COMPANIES_DATA = 'SET_COMPANIES_DATA';
export const SET_DISCLAIMER_DATA = 'SET_DISCLAIMER_DATA';
export const SET_TERMS_DATA = 'SET_TERMS_DATA';
export const SET_PRP_DATA = 'SET_PRP_DATA';
export const SET_WHY_SM = 'SET_WHY_SM';
export const SET_HOW_IW = 'SET_HOW_IW';
export const SET_PARTNERS_LOGOS = 'SET_PARTNERS_LOGOS';
export const SET_FOOTER_ITEMS = 'SET_FOOTER_ITEMS';
export const SHOW_LOADER = 'SHOW_LOADER';
export const HIDE_LOADER = 'HIDE_LOADER';
export const SET_CITIES = 'SET_CITIES';
export const SET_ALL_REVIEWS = 'SET_ALL_REVIEWS';
export const SET_CURRENT_CITY_DATA = 'SET_CURRENT_CITY_DATA';
export const SET_CURRENT_CITY = 'SET_CURRENT_CITY';
export const SET_CURRENT_LANG = 'SET_CURRENT_LANG';
export const SET_PAGE_PARAMS = 'SET_PAGE_PARAMS';
export const SET_SERVICES = 'SET_SERVICES';
export const SET_FOOTER_SERVICES = 'SET_FOOTER_SERVICES';
export const SET_BODY_CLASS = 'SET_BODY_CLASS';
export const SET_BOOKING_HISTORY = 'SET_BOOKING_HISTORY';
export const MAIN_MENU_VISIBILITY = 'MAIN_MENU_VISIBILITY';
export const LOGIN_MODAL_VISIBILITY = 'LOGIN_MODAL_VISIBILITY';
export const FORGET_PASSWORD_MODAL_VISIBILITY = 'FORGET_PASSWORD_MODAL_VISIBILITY';
export const FEEDBACK_MODAL_VISIBILITY = 'FEEDBACK_MODAL_VISIBILITY';
export const SIGNIN_FORM_VISIBILITY = 'SIGNIN_FORM_VISIBILITY';
export const SET_SIGNIN = 'SET_SIGNIN';
export const SET_CLEANING_BOOKING_PRICING_PLAN = 'SET_CLEANING_BOOKING_PRICING_PLAN';
export const SET_TAX_PLAN = 'SET_TAX_PLAN';
export const SET_ZOHO_MIGRATED_SERVICE = 'SET_ZOHO_MIGRATED_SERVICE';
export const SET_SERVICE_CONSTANTS = 'SET_SERVICE_CONSTANTS';
export const SET_API_SERVICES = 'SET_API_SERVICES';
export const SET_BOOKING_SERVICES = 'SET_BOOKING_SERVICES';
export const SET_ALL_COUNTRIES = 'SET_ALL_COUNTRIES';
export const SET_ALL_SERVICE_CONSTANT = 'SET_ALL_SERVICE_CONSTANT';
export const SET_DATA_DICTIONARY_VALUES = 'SET_DATA_DICTIONARY_VALUES';
export const SET_DATA_CONSTANT_VALUES = 'SET_DATA_CONSTANT_VALUES';
export const SET_MY_BOOKINGS = 'SET_MY_BOOKINGS';
export const CANCEL_REQUEST = 'CANCEL_REQUEST';
export const CANCEL_EVENT_REQUEST = 'CANCEL_EVENT_REQUEST';
export const FREEZE_REQUEST = 'FREEZE_REQUEST';
export const EDIT_REQUEST = 'EDIT_REQUEST';
export const EDIT_ALL_REQUEST = 'EDIT_ALL_REQUEST';
export const UNSUBSCRIBE_REQUEST = 'UNSUBSCRIBE_REQUEST';
export const SET_MY_QUOTES = 'SET_MY_QUOTES';
export const SET_MY_CREDIT_CARDS = 'SET_MY_CREDIT_CARDS';
export const REGISTER_CREDIT_CARDS = 'REGISTER_CREDIT_CARDS';
export const API_PUBLIC_USERNAME = 'public';
export const API_PUBLIC_PASSWORD = '12a1ae1b8a7f9bd4c41bae2645032c30';
export const WEB_CLIENT_ID = 'SM-WEB-CLIENT';
export const GRANT_TYPE_PASSWORD = 'password';
export const SET_DATE_TIME_AVAILABILITY = 'SET_DATE_TIME_AVAILABILITY';
export const OXYCLEAN_PRICE = 250;
export const LANG_SWITCH_VISIBILITY = 'LANG_SWITCH_VISIBILITY';

export const SET_USER_PROFILE = 'SET_USER_PROFILE';
export const SET_RESET_USER_DETAILS = 'SET_RESET_USER_DETAILS';
export const SET_CITIS_AREAS = 'SET_CITIS_AREAS';
export const SET_POPUP_MENUE_DATA = 'SET_POPUP_MENUE_DATA';
export const PROMO_MODAL_VISIBILITY = 'PROMO_MODAL_VISIBILITY';
export const SET_CURRENT_PROVIDER = 'SET_CURRENT_PROVIDER';
export const SET_JOURNEY3_SUBSERVICE_DATA = 'SET_JOURNEY3_SUBSERVICE_DATA';
export const SET_DATE_TIME = 'SET_DATE_TIME';
export const SET_CURRENT_CITY_ID = 'SET_CURRENT_CITY_ID';
export const SET_WALLET_HISTORY = 'SET_WALLET_HISTORY';

/* defaults */
export const DEFAULT_CITY_ID = 1; // dubai id
export const DEFAULT_CITY_NAME = 'dubai'; // dubai name
export const DEFAULT_COUNTRY_NAME = 'UAE';
export const DEFAULT_COUNTRY_ID = 1;
/* location IDs  */

export const DUBAI = 'dubai';
export const ABU_DHABI = 'abu-dhabi';
export const SHARJAH = 'sharjah';
export const DOHA = 'doha';
export const JEDDAH = 'jeddah';
export const RIYADH = 'riyadh';
export const BAHRAIN = 'bahrain';
export const KUWAIT = 'kuwait';
export const DAMMAM = 'dammam';
export const MUSCAT = 'muscat';
var cityCodeData = {};
cityCodeData[DUBAI] = "DXB";
cityCodeData[ABU_DHABI] = "AUH";
cityCodeData[SHARJAH] = "SHJ";
cityCodeData[DOHA] = "DOH";
cityCodeData[JEDDAH] = "JED";
cityCodeData[RIYADH] = "RUH";
cityCodeData[BAHRAIN] = "BAH";
cityCodeData[KUWAIT] = "KWI";
cityCodeData[DAMMAM] = "DMM";
cityCodeData[MUSCAT] = "MCT"

export const SERVICE_PERSONAL_STORAGE = 70;
export const SERVICE_COMMERCIAL_STORAGE = 71;
export const cityCodes = cityCodeData;
export const locationConstants = {
	DEFAULT_CITY_ID :  1, // dubai id
	DEFAULT_CITY_NAME : 'dubai', // dubai name
	DEFAULT_COUNTRY_NAME : 'UAE',
	DEFAULT_COUNTRY_ID : 1,
	DEFAULT_CURRENCY : "AED",
	DUBAI_ID : 1,
	ABU_DHABI_ID : 2,
	SHARJAH_ID : 3,
	SHARJAH_MAPPED_ID :  6,
	DOHA_ID : 5,
	DOHA_CITY_ID : 13,
	JEDDAH_ID : 15,
	RIYADH_ID : 16,
	BAHRAIN_ID : 17,
	KUWAIT_ID : 18,
	SINGAPORE_ID : 19,
	SINGAPORE_SL_ID : 10,
	KUALA_LUMPUR_ID : 20,
	KUALA_LUMPUR_SL_ID : 11,
	HONGKONG_ID : 21,
	HONGKONG_SL_ID : 12,
	DAMMAM_ID : 22,
	MUSCAT_ID : 23,
	UAE_ID : 1,
	QATAR_ID : 178,
	KSA_ID : 192,
	KW_ID : 116, // KUWAIT country ID
	BH_ID : 18, // BAHRAIN country ID
	SP_ID : 197, // SINGAPORE country ID
	MALAYSIA_ID : 131, // KUALA LUMPUR country ID
	CHINA_ID : 97, // HONGKONG country ID
	OMAN_ID : 165, // MUSCAT country ID
	cityCodes: cityCodes
};
/* city IDs 
export const DUBAI_ID = 1;
export const ABU_DHABI_ID = 2;
export const SHARJAH_ID = 3
export const SHARJAH_MAPPED_ID = 6;
export const DOHA_ID = 5;
export const DOHA_CITY_ID = 13
export const JEDDAH_ID = 15;
export const RIYADH_ID = 16;
export const BAHRAIN_ID = 17;
export const KUWAIT_ID = 18;
export const SINGAPORE_ID = 19;
export const SINGAPORE_SL_ID = 10;
export const KUALA_LUMPUR_ID = 20;
export const KUALA_LUMPUR_SL_ID = 11;
export const HONGKONG_ID = 21;
export const HONGKONG_SL_ID = 12;*/
export const {
		DUBAI_ID,
		ABU_DHABI_ID,
		SHARJAH_ID,
		SHARJAH_MAPPED_ID,
		DOHA_ID,
		DOHA_CITY_ID,
		JEDDAH_ID,
		DAMMAM_ID,
		MUSCAT_ID, 
		RIYADH_ID,
		BAHRAIN_ID,
		KUWAIT_ID,
		SINGAPORE_ID,
		SINGAPORE_SL_ID,
		KUALA_LUMPUR_ID,
		KUALA_LUMPUR_SL_ID,
		HONGKONG_ID,
		HONGKONG_SL_ID,
		UAE_ID, 
		QATAR_ID, 
		KSA_ID, 
		KW_ID, 
		BH_ID, 
		SP_ID,
		MALAYSIA_ID,
		CHINA_ID,
		OMAN_ID
} = locationConstants;


export const CREDITCARD_COUPON = "CREDITCARD";

export const JOURNEY1 = "journey1";

export const JOURNEY2 = "journey2";

export const JOURNEY3 = "journey3";

export const DEFAULT_LANG = "en";

export const LANG_EN = "en";
export const LANG_AR = "ar";

export const LWBLowReviewQuestions = [
	{ id: 1, name: "The team did not show at all", value: "The team did not show at all" },
	{ id: 2, name: "The team was late", value: "The team was late" },
	{ id: 3, name: "The service quality was poor", value: "The service quality was poor" },
	{ id: 4, name: "The service was slow", value: "The service was slow" },
	{ id: 5, name: "The job was not finished/ completed", value: "The job was not finished/ completed" },
	{ id: 6, name: "They did not bring the required materials", value: "They did not bring the required materials" },
	{ id: 7, name: "Other", value: "Other" },
];

export const URLCONSTANT = {
	LOCAL_MOVER_PAGE_URI: "local-movers",
	INTERNATIONAL_MOVER_PAGE_URI: "international-movers",
	CLEANING_MAID_PAGE_URI: "cleaning-maid-services",
	CAR_SHIPPING_PAGE_URI: "car-shipping",
	STORAGE_COMPANIES_PAGE_URI: "storage-companies",
	PEST_CONTROL_PAGE_URI: "pest-control-companies",
	PAINTERS_PAGE_URI: "painters",
	CURTAINS_PAGE_URI: "curtains-blinds",
	MAINTENANCE_PAGE_URI: "maintenance-handyman-companies",
	AC_MAINTENANCE_PAGE_URI: "ac-maintenance",
	GARDENING_PAGE_URI: "gardening-companies",
	DEEP_CLEANING_PAGE_URI: "deep-cleaning",
	POOL_CLEANING_PAGE_URI: "pool-cleaning",
	WINDOW_CLEANING_PAGE_URI: "window-cleaning",
	HOME_INSURANCE_PAGE_URI: "home-insurance",
	CAR_INSURANCE_PAGE_URI: "car-insurance",
	BTM_INSURANCE_PAGE_URI: "uae/insurance",
	CATERING_PAGE_URI: "catering",
	PHOTOGRAPHY_PAGE_URI: "photography",
	BABYSITTING_PAGE_URI: "babysitting",
	PLUMBING_PAGE_URI: "plumbers",
	ELECTRICIAN_PAGE_URI: "electrician-services",
	WATER_TANK_CLEANING_PAGE_URI: "water-tank-cleaning",
	MATTRESS_CLEANING_PAGE_URI: "mattress-cleaning",
	CARPET_CLEANING_PAGE_URI: "carpet-cleaning",
	SOFA_AND_UPHOLSTERY_CLEANING_PAGE_URI: "sofa-upholstery-cleaning",
	OFFICE_CLEANING_PAGE_URI: "office-cleaning",
	FULL_TIME_MAIDS_PAGE_URI: "full-time-maids",
	PEST_CONTROL_PAGE_URI_ANTS_PAGE_URI: "pest-control-ants",
	PEST_CONTROL_PAGE_URI_COCKROACHES_PAGE_URI: "pest-control-cockroaches",
	PEST_CONTROL_PAGE_URI_RATS_PAGE_URI: "pest-control-rats-mice",
	PEST_CONTROL_PAGE_URI_TERMITES_PAGE_URI: "pest-control-termites",
	NOON_TV_INSTALLATION_PAGE_URI: 'noon-tv-installation',
	LAUNDRY_DRY_CLEANING: 'laundry-dry-cleaning'
}
export function isSmartBannerActive(serviceURL){
	let services = [
		URLCONSTANT.LOCAL_MOVER_PAGE_URI,
		URLCONSTANT.CLEANING_MAID_PAGE_URI,
		URLCONSTANT.PEST_CONTROL_PAGE_URI,
		URLCONSTANT.PAINTERS_PAGE_URI,
		URLCONSTANT.MAINTENANCE_PAGE_URI,
		URLCONSTANT.AC_MAINTENANCE_PAGE_URI,
		URLCONSTANT.DEEP_CLEANING_PAGE_URI,
		URLCONSTANT.POOL_CLEANING_PAGE_URI,
		URLCONSTANT.WINDOW_CLEANING_PAGE_URI,
		URLCONSTANT.PLUMBING_PAGE_URI,
		URLCONSTANT.ELECTRICIAN_PAGE_URI,
		URLCONSTANT.WATER_TANK_CLEANING_PAGE_URI,
		URLCONSTANT.MATTRESS_CLEANING_PAGE_URI,
		URLCONSTANT.CARPET_CLEANING_PAGE_URI,
		URLCONSTANT.SOFA_AND_UPHOLSTERY_CLEANING_PAGE_URI
	];
	return services.includes(serviceURL) ? true : false;
}

export function setLangVisibility(data) {
	return {
		type: LANG_SWITCH_VISIBILITY,
		payload: data
	}
}
export function setDateTime(data) {
	return {
		type: SET_DATE_TIME,
		payload: data
	}
}
export function setCitiesAreas(data) {
	return {
		type: SET_CITIS_AREAS,
		payload: data
	}
}

export function setAllConstants(data, lang) {
	return {
		type: SET_SERVICE_CONSTANTS,
		payload: data,
		lang:lang
	}
}
export function setAllAPIServices(data) {
	return {
		type: SET_API_SERVICES,
		payload: data
	}
}
export function setBookingServices(data) {
	return {
		type: SET_BOOKING_SERVICES,
		payload: data
	}
}
export function setAllCountries(data) {
	// console.log('data');
	return {
		type: SET_ALL_COUNTRIES,
		payload: data
	}
}
export function setAllServiceLookUpConstants(data) {
	return {
		type: SET_ALL_SERVICE_CONSTANT,
		payload: data
	}
}

export function setAlldataDictionaryValues(data) {
	return {
		type: SET_DATA_DICTIONARY_VALUES,
		payload: data
	}
}
export function setAlldataConstantValues(data) {
	return {
		type: SET_DATA_CONSTANT_VALUES,
		payload: data
	}
}

export function setCleaningPricePlan(data) {
	return {
		type: SET_CLEANING_BOOKING_PRICING_PLAN,
		payload: data
	}
}
// Actions
// Set Partner Logos Action
export function logosData(data) {
	return {
		type: SET_PARTNERS_LOGOS,
		payload: data
	}
}
// Set Partner Logos Action
export function footerItemsData(data) {
	return {
		type: SET_FOOTER_ITEMS,
		payload: data
	}
}
// Set journey 3 sub service
export function setJourneySubServiceData(data) {
	return {
		type: SET_JOURNEY3_SUBSERVICE_DATA,
		payload: data
	}
}
// Show Main menu Action
export function toggleMainMenu(b) {
	return {
		type: MAIN_MENU_VISIBILITY,
		payload: b
	}
}
// Show Login Popup Action
export function toggleLoginModal(b) {
	return {
		type: LOGIN_MODAL_VISIBILITY,
		payload: b
	}
}
// Show Login Popup Action
export function toggleForgetPasswordModal(b) {
	return {
		type: FORGET_PASSWORD_MODAL_VISIBILITY,
		payload: b
	}
}
// Show Feedback Popup Action
export function toggleFeedbackModal(b) {
	return {
		type: FEEDBACK_MODAL_VISIBILITY,
		payload: b
	}
}
// Show Signin Form Action
export function toggleSignInForm(b) {
	return {
		type: SIGNIN_FORM_VISIBILITY,
		payload: b
	}
}
// toggle Popup promo
export function togglePopupPromoForm(b) {
	return {
		type: PROMO_MODAL_VISIBILITY,
		payload: b
	}
}

// Set Signin Action
export function setSignIn(b) {
	return {
		type: SET_SIGNIN,
		payload: b
	}
}

// Set User Details Action
export function setUserProfile(b) {
	return {
		type: SET_USER_PROFILE,
		payload: b
	}
}

//
export function setWalletHistory(data) {
	return {
		type: SET_WALLET_HISTORY,
		payload: data
	}
}
// Set Reset User Details Action
export function setResetUserDetails(b) {
	return {
		type: SET_RESET_USER_DETAILS,
		payload: b
	}
}

// Set Landing-page Data Action
export function landingData(data) {
	return {
		type: SET_LANDING_DATA,
		payload: data
	}
}
// Set Landing-page Data Action
export function landingConfirmationData(data) {
	return {
		type: SET_LANDING_CONFIRMATION_DATA,
		payload: data
	}
}
// Set Homepage Data Action
export function homeData(data) {
	return {
		type: SET_HOMEPAGE_DATA,
		payload: data
	}
}
// Set About-page Data Action
export function aboutData(data) {
	return {
		type: SET_ABOUT_DATA,
		payload: data
	}
}
// Set Bot Data Action
export function isBotData(data) {
	return {
		type: SET_BOT,
		payload: data
	}
}
// Set Popup menu Data Action
export function popupMenuData(data) {
	return {
		type: SET_POPUP_MENUE_DATA,
		payload: data
	}
}


// Set Popup menu Data Action
export function setCurrentProvider(data) {
	return {
		type: SET_CURRENT_PROVIDER,
		payload: data
	}
}
// Set Become-Partner-page Data Action
export function bePartnerData(data) {
	return {
		type: SET_BECOME_PARTNER_DATA,
		payload: data
	}
}
// Review Page Action
export function reviewData(data) {
	return {
		type: SET_REVIEW_DATA,
		payload: data
	}
}
// Team Page Action
export function teamData(data) {
	return {
		type: SET_TEAM_DATA,
		payload: data
	}
}
// Faqs Page Action
export function faqsData(data) {
	return {
		type: SET_FAQS_DATA,
		payload: data
	}
}
// Privacy Page Action
export function privacyData(data) {
	return {
		type: SET_PRIVACY_DATA,
		payload: data
	}
}
// Companies Page Action
export function companiesData(data) {
	return {
		type: SET_COMPANIES_DATA,
		payload: data
	}
}
// Disclaimer Page Action
export function disclaimerData(data) {
	return {
		type: SET_DISCLAIMER_DATA,
		payload: data
	}
}
// Terms Page Action
export function termsData(data) {
	return {
		type: SET_TERMS_DATA,
		payload: data
	}
}
// PRP Page Action
export function prpData(data) {
	return {
		type: SET_PRP_DATA,
		payload: data
	}
}
// Why Service Market Action
export function whySMData(data) {
	return {
		type: SET_WHY_SM,
		payload: data
	}
}
// How it works Action
export function howIWData(data) {
	return {
		type: SET_HOW_IW,
		payload: data
	}
}
// My bookings Action
export function myBookingsAction(data) {
	return {
		type: SET_MY_BOOKINGS,
		payload: data
	}
}
// Cancel booking request Action
export function cancelBookingAction(id) {
	return {
		type: CANCEL_REQUEST,
		id: id
	}
}
// Cancel booking request Action
export function cancelBookingEventAction(id) {
	return {
		type: CANCEL_EVENT_REQUEST,
		id: id
	}
}
// freeze booking request Action
export function freezeBookingAction(id, startDate, endDate) {
	return {
		type: FREEZE_REQUEST,
		id: id,
		startDate: startDate,
		endDate: endDate
	}
}
// edit booking request Action
export function editBookingAction(id) {
	return {
		type: EDIT_REQUEST,
		id: id
	}
}
// edit booking request Action
export function editAllBookingAction(id) {
	return {
		type: EDIT_ALL_REQUEST,
		id: id
	}
}
export function unsubscribeBookingAction(id) {
	return {
		type: UNSUBSCRIBE_REQUEST,
		id: id
	}
}
// My quotes Action
export function myQuotesAction(data) {
	return {
		type: SET_MY_QUOTES,
		payload: data
	}
}
// Credit cards Action
export function creditcardsAction(data) {
	return {
		type: SET_MY_CREDIT_CARDS,
		payload: data
	}
}
// Credit cards Action
export function registerCreditcardsAction(data) {
	return {
		type: REGISTER_CREDIT_CARDS,
		payload: data
	}
}
// Show Loader Action
export function showLoader() {
	return {
		type: SHOW_LOADER
	}
}

// Hide Loader Action
export function hideLoader() {
	return {
		type: HIDE_LOADER
	}
}
// All Cities Action
export function setAllCities(cities, lang, removeArea) {
	return {
		type: SET_CITIES,
		payload: cities,
		lang: lang,
		removeArea: removeArea
	}
}
// All Reviews Action
export function setReviews(reviews) {
	return {
		type: SET_ALL_REVIEWS,
		payload: reviews
	}
}
//  Current City Data Action
export function setCurrentCityData(city_data,lang) {
	return {
		type: SET_CURRENT_CITY_DATA,
		payload: city_data,
		lang: lang
	}
}
export function setCurrentCityID(city_data) {
	return {
		type: SET_CURRENT_CITY_ID,
		payload: city_data
	}
}
// Current language Data Action
export function setCurrentLang(lang) {
	return {
		type: SET_CURRENT_LANG,
		payload: lang
	}
}
// Current City Data Action
export function setCurrentCity(city) {
	return {
		type: SET_CURRENT_CITY,
		payload: city
	}
}
// Current City Data Action
export function setPageParams(params) {
	return {
		type: SET_PAGE_PARAMS,
		payload: params
	}
}
// Services Per City Action
export function setServices(services) {
	return {
		type: SET_SERVICES,
		payload: services
	}
}
// Footer Services Per City Action
export function setFooterServices(services) {
	return {
		type: SET_FOOTER_SERVICES,
		payload: services
	}
}
// Set Page Class Action
export function setBodyClass(className) {
	return {
		type: SET_BODY_CLASS,
		payload: className
	}
}

// Customer Booking history Action
export function bookingHistory(data) {
	return {
		type: SET_BOOKING_HISTORY,
		payload: data
	}
}
// Customer Booking history Action
export function setTaxPlan(data) {
	return {
		type: SET_TAX_PLAN,
		payload: data
	}
}
export function setZohoMigratedService(data){
	return {
		type: SET_ZOHO_MIGRATED_SERVICE,
		payload: data
	}
}
export function setDateTimeAvailability(data) {
	return {
		type: SET_DATE_TIME_AVAILABILITY,
		payload: data
	}
}
// FETCHES
// Fetch All Providers
export function fetchAllProviders(sortByPriorityProvider = true, lang = DEFAULT_LANG) {
	return axios.post('', encryptBody({
		"apis": [endPoints.providersAll(sortByPriorityProvider, lang)]
	})).then(function (response) {
		// handle success
		return response.data.providersAll.data;
	}).catch(function (error) {
		// handle error
		console.log("Error Fetching fetchAllProviders");
	});
}
// Fetch Providers by serviceID and City Code
export function fetchProvidersByService(cityCode, id, lang = DEFAULT_LANG) {
	return axios.post('', encryptBody({
		"apis": [endPoints.providersByService(cityCode, id, lang)]
	})).then(function (response) {
		// handle success
		return response.data.providers.data;
	}).catch(function (error) {
		// handle error
		console.log("Error Fetching fetchProvidersByService");
	});
}
// Fetch Providers by City Code
export function fetchProvidersByCity(cityCode, lang = DEFAULT_LANG) {
	return axios.post('', encryptBody({
		"apis": [endPoints.providersByCity(cityCode, lang)]
	})).then(function (response) {
		// handle success
		return response.data.providers.data;
	}).catch(function (error) {
		// handle error
		console.log("Error Fetching fetchProvidersByCity");
	});
}
// Fetch Single Provider Details
export function fetchProvider(id, lang = DEFAULT_LANG) {
	return axios.post('', encryptBody({
		"apis": [endPoints.provider(id, lang)]
	})).then(function (response) {
		// handle success
		return response.data.provider.data;
	}).catch(function (error) {
		// handle error
		console.log("Error Fetching fetchProvider");
	});
}
// Fetch Single Provider Details by URL
export function fetchProviderByURLDispatch(url, lang = DEFAULT_LANG) {
	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": [endPoints.providerByURL(url, lang)]
		})).then(function (response) {
			// handle success
			// console.log(response.data.provider.data);
			dispatch(setCurrentProvider(response.data.provider.data))
			return response.data.provider.data;
		}).catch(function (error) {
			// handle error
			console.log("Error Fetching fetchProviderByURL");
		});
	}
}
// Fetch Single Provider Details by URL
export function fetchProviderByURL(url, lang = DEFAULT_LANG) {
	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": [endPoints.providerByURL(url, lang)]
		})).then(function (response) {
			// handle success
			// console.log(response.data.provider.data);
			dispatch(setCurrentProvider(response.data.provider.data))
			return response.data.provider.data;
		}).catch(function (error) {
			// handle error
			console.log("Error Fetching fetchProviderByURL");
		});
	}
}
// send feedback
export function sendFeedback(email, name, description, feedbackServiceName, selectedServiceText, phone, refID, lang = DEFAULT_LANG) {
	return axios.post(staticVariables.MIDDLEWARE_URL + '?url=/&time=60&type=feedback-complaint', encryptBody({
		"email": email,
		"name": name,
		"description": description,
		"feedback-service-name": feedbackServiceName,
		"selected-service-text": selectedServiceText,
		"phone": phone,
		"refID": refID
	})).then(function (response) {
		// handle success
		return response;
	}).catch(function (error) {
		// handle error
		console.log("Error Fetching fetchProviderByURL");
	});
}
// Fetch Elastic Search Results
export function fetchSearch(city, term, lang = DEFAULT_LANG) {
	//console.log("fetchSearch");
	return axios(endPoints.search(city, term, lang))
		.then(function (response) {
			// handle success
			return response.data.hits.hits;
		}).catch(function (error) {
			// handle error
			return [];
		});
}
// Fetch Sign up
export function fetchSignUp(email, firstname, lastname, password, lang = DEFAULT_LANG) {
	return axios.post('', encryptBody({
		"apis": [endPoints.registerCustomer(email, firstname, lastname, password, lang)]
	})).then(function (response) {
		// handle success
		return response;
	}).catch(function (error) {
		// handle error
		return [];
	});
}

// Update User Profile
export function putUpdateCustomerProfile(accessToken, customerId, emailAddress, firstName, lastName, address, lang = DEFAULT_LANG) {
	return axios.post('', encryptBody({
		"apis": [endPoints.updateCustomer(accessToken, customerId, emailAddress, firstName, lastName, address, lang)]
	})).then(function (response) {
		// handle success
		return response;
	}).catch(function (error) {
		// handle error
		return [];
	});
}

// activate Booking Request
export function activateBookingRequest(accessToken, signedInCustomerId, bookingId, creditCardId, paymentMethod, couponValidationModel, isNewBookingEngine = false, lang = DEFAULT_LANG){
	var couponValidationModel = typeof couponValidationModel != "undefined" ? couponValidationModel : {};
	var activateRequestPayload = endPoints.activateRequest(accessToken, signedInCustomerId, bookingId, creditCardId, paymentMethod, couponValidationModel, isNewBookingEngine, lang);
    return axios.post('', encryptBody({
        "apis" : [activateRequestPayload]
    })).then(function (response) {
        // handle success
        return {data: response.data.activateRequest, status: response.status};
    }).catch(function (error) {
        // handle error
        console.log("activateBookingRequestError",error);
        return [];
    });
}
// Fetch update customer Password
export function updateCustomerPassword(email, newPassword, customerID, resetToken, accessToken, lang = DEFAULT_LANG) {
	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": [endPoints.updateCustomerPassword(email, newPassword, customerID, resetToken, accessToken, lang)]
		})).then(function (response) {
			// handle success
			return response.data.updatePassword;
		}).catch(function (error) {
			// handle error
			console.log("Error Fetching reset password");
		});
	}
}
// Fetch Sign In
export function fetchSignIn(email, password, lang = DEFAULT_LANG) {
	return axios.post('', encryptBody({
		"apis": [endPoints.signInCustomer(email, password,lang)]
	})).then(function (response) {
		// handle success
		return { data: JSON.parse(response.data.signInCustomer), status: response.status };
	}).catch(function (error) {
		// handle error
		return [];
	});
}

// Fetch reset Password
export function fetchResetPassword(email, accessToken, lang = DEFAULT_LANG) {
	return axios.post('', encryptBody({
		"apis": [endPoints.resetPasswordCustomer(email, accessToken, lang)]
	})).then(function (response) {
		// handle success
		return response.data.resetPassword;
	}).catch(function (error) {
		// handle error
		console.log("Error Fetching reset password");
	});
}

// Fetch user Details By Reset Token Customer
export function fetchUserDetailsByResetTokenCustomer(resetToken, lang = DEFAULT_LANG) {
	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": [endPoints.userDetailsByResetTokenCustomer(resetToken, lang)]
		})).then(function (response) {
			// handle success
			dispatch(setResetUserDetails(response.data.userDetailsByResetToken));
			return response.data.userDetailsByResetToken;
		}).catch(function (error) {
			// handle error
			console.log("Error Fetching reset password");
		});
	}
}

// Fetch Sign In
export function fetchSocialSignInCustomer(token, type, lang = DEFAULT_LANG) {
	return axios.post('', encryptBody({
		"apis": [endPoints.socialSignInCustomer(token, type ,lang)]
	})).then(function (response) {
		// handle success
		return { data: JSON.parse(response.data.socialSignInCustomer), status: response.status };
	}).catch(function (error) {
		// handle error
		return [];
	});
}

// Fetch User Profile
export function fetchUserProfile(token, lang = DEFAULT_LANG) {
	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": [endPoints.getCustomerDetails(token,lang)]
		})).then(function (response) {
			// handle success	
			//console.log("response.data.getCustomerDetails", response);		
			if (response.data.getCustomerDetails && response.data.getCustomerDetails.success) {
				dispatch(setUserProfile(response.data.getCustomerDetails.data));
			}
		}).catch(function (error) {
			// handle error
			console.log("Error Fetching fetchUserProfile");
		});
	}
}

export function fetchWalletHistoy(token, lang = DEFAULT_LANG) {
	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": [endPoints.getWalletHistory(token,lang)]
		})).then(function (response) {
			// handle success
			if (response.data.walletHistory && response.data.walletHistory.success) {
				dispatch(setWalletHistory(response.data.walletHistory.data));
			}
			else if(response.data == null){
				dispatch(setWalletHistory(response.data)); // Empty wallet case
			}
		}).catch(function (error) {
			// handle error
			console.log("Error Fetching walletHistory");
		});
	}
}
// Check Customer Email if exist
export function isEmailExist(email, lang = DEFAULT_LANG) {
	return axios.post('', encryptBody({
		"apis": [endPoints.emailAvailable(email,lang)]
	})).then(function (response) {
		// handle success
		return response.data.email_available.success;
	}).catch(function (error) {
		// handle error
		console.log("Error Fetching isEmailExist");
	});
}
// Verify a credit card
export function submitVerifyCreditCard(verifyToken, accessToken, signedInCustomerId, lang = DEFAULT_LANG) {
	return axios.post('', encryptBody({
		"apis": [endPoints.verifyCreditCard(verifyToken, accessToken,lang)]
	})).then(function (response) {
		var res = JSON.parse(response.data.verifyCreditCard);
		// handle success

		if (res && res.success) {
			return res;
		} else return [];
	}).catch(function (error) {
		// handle error
		console.log("Error submitVerifyCreditCard");
	});
}
// Register a credit card
export function registerCreditCard(accessToken, signedInCustomerId, cancelURL, declineURL, successURL, lang = DEFAULT_LANG) {
	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": [endPoints.registerCreditCard(accessToken, signedInCustomerId, cancelURL, declineURL, successURL,lang)]
		})).then(function (response) {
			// handle success
			var res = JSON.parse(response.data.registerCreditCard);
			if (response.data.registerCreditCard && res.success) {
				dispatch(registerCreditcardsAction(res.data));
			}
		}).catch(function (error) {
			// handle error
			console.log("Error Fetching registerCreditCard");
		});
	}
}
// remove a credit card
export function removeCreditCard(accessToken, creditCardId, lang = DEFAULT_LANG) {
	return axios.post('', encryptBody({
		"apis": [endPoints.removeCreditCard(accessToken, creditCardId,lang)]
	})).then(function (response) {
		// handle success
		console.log("removing credit card");
	}).catch(function (error) {
		// handle error
		console.log("Error Fetching removeCreditCard");
	});
}
// Add a review
export function submitAddReview(accessToken, signedInCustomerId, data, lang = DEFAULT_LANG) {
	return axios.post('', encryptBody({
		"apis": [endPoints.addReview(accessToken, signedInCustomerId, data,lang)]
	})).then(function (response) {
		// handle success
		return response.data.addReview;
	}).catch(function (error) {
		// handle error
		console.log("Error Fetching submitAddReview");
	});
}
// become a partner
export function submitBecomeAPartner(accessToken, signedInCustomerId, data, lang = DEFAULT_LANG) {

	return axios.post('', encryptBody({
		"apis": [endPoints.becomeAPartner(accessToken, signedInCustomerId, data,lang)]
	})).then(function (response) {
		// handle success
		return JSON.parse(response.data.becomeAPartnerPost);
	}).catch(function (error) {
		// handle error
		console.log("Error Fetching submitBecomeAPartner");
	});
}

// Add Become a partener zoho Request
export function submitBecomeAPartnerZoho(data) {
	return axios.post(staticVariables.MIDDLEWARE_URL + '?time=0&type=become-a-partner', encryptBody(data)).then(function (response) {
		// handle success
		return response;
	})
		.catch(function (error) {
			// handle error
			console.log("Error Fetching postLWRequest");
			return {};
		});
}
export function submitPromo(data) {
	return axios.post(staticVariables.MIDDLEWARE_URL + '?time=0&type=submit_promo', encryptBody(data)).then(function (response) {
		// handle success
		return response;
	})
		.catch(function (error) {
			// handle error
			console.log("Error Fetching postLWRequest");
			return {};
		});
}
// get all credit card
export function getAllCreditCard(accessToken, lang = DEFAULT_LANG) {
	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": [endPoints.getCreditCard(accessToken,lang)]
		})).then(function (response) {
			console.log("Submitting credit card");
			// handle success
			if (response.data.getCreditCard) {
				dispatch(creditcardsAction(response.data.getCreditCard));
			}
		}).catch(function (error) {
			// handle error
			console.log("Error Fetching getAllCreditCard");
		});
	}
}

// Fetch All Reviews by city code and serviceID
export function fetchAllReviews(cityCode, serviceID, lang = DEFAULT_LANG) {
	//console.log("fetchAllReviews", endPoints.getReviews(cityCode, serviceID,lang))
	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": [endPoints.getReviews(cityCode, serviceID,lang)]
		})).then(function (response) {
			// handle success
			var data = response.data.reviews.data;
			//console.log("data", data);
			let reviews = data;
			if(reviews.length){
				reviews = reviews.slice(0, 2);
			}
			dispatch(setReviews(reviews));
			return reviews;
		}).catch(function (error) {
			// handle error
			console.log("Error Fetching fetchAllReviews");
		});
	}
}
// Fetch All Cities
export function fetchAllCitiesForRedirection() {
	return axios.post('', encryptBody({
		"apis": [endPoints.cities(DEFAULT_LANG)]
	})).then(function (response) {
		//console.log("fetchAllCitiesForRedirection", response);
		var data = response.data.cities.data;
		return data;
	}).catch(function (error) {
		// handle error
		console.log("Error Fetching fetchAllCitiesForRedirection");
	});
}
export function fetchAllCities(current_city = DEFAULT_CITY_NAME, lang = DEFAULT_LANG, removeArea = true) {
	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": [endPoints.cities(lang)]
		})).then(function (response) {
			// handle success
			var data = response.data.cities.data;
			dispatch(setAllCities(data, lang, removeArea));
			var current_city_data = data.filter((item) => item.slug == current_city);
			if (current_city_data.length) {
				//let langVisibility = current_city_data[0].cityDto.countryDto.id == KSA_ID ? true : false;
				if(typeof current_city_data[0].supportedLanguages !="undefined"){
					let langVisibility = current_city_data[0].supportedLanguages.length >= 2 ? true : false;
					dispatch(setLangVisibility(langVisibility)); // lang switch
				}
				dispatch(setCurrentCityID(current_city_data[0].id));
			}
			dispatch(setCurrentCityData(current_city_data, lang));

		}).catch(function (error) {
			// handle error
			console.log("Error Fetching fetchAllCities", error);
		});
	}
}
// Fetch Cities Areas
export function fetchCitiesAreas(lang = DEFAULT_LANG) {
	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": [endPoints.citisAreas(lang)]
		})).then(function (response) {
			// handle success
			var allCitiesAreas = [];
			response.data.country_city_area.data.forEach(item => {
				if (item.cities.length) {
					allCitiesAreas.push(item);
				}
			});
			dispatch(setCitiesAreas(allCitiesAreas));

		}).catch(function (error) {
			// handle error
			console.log("Error Fetching fetchCitiesAreas", error, endPoints.citisAreas(lang));
		});
	}
}
export function saveLocalStorage(data){
	return axios({
		method: 'post',
		url:window.location.origin+'/save-local-storage', 
		data: data,
		headers: {'Content-Type': 'application/json'}
	}).then(function (response) {
		console.log(response);
	}).catch(function (error) {
		// handle error
		console.log("Error Fetching saveLocalStorage");
	});
}
// Fetch All Parent Services
export function fetchAllParentServices(lang = DEFAULT_LANG) {
	return axios.post('', encryptBody({
		"apis": [endPoints.getAllServices(lang)]
	})).then(function (response) {
		// handle success
		return response.data.services.data;
	}).catch(function (error) {
		// handle error
		console.log("Error Fetching fetchAllParentServices");
	});
}
// Fetch All Parent Services
export function fetchServiceProviderLocations(providerID, serviceID, lang = DEFAULT_LANG) {
	return axios.post('', encryptBody({
		"apis": [endPoints.serviceProviderLocations(providerID, serviceID,lang)]
	})).then(function (response) {
		// handle success
		return response.data.locations.data;
	}).catch(function (error) {
		// handle error
		console.log("Error Fetching fetchServiceProviderLocations");
	});
}
// Fetch Services By City
export function fetchServices(city, lang = DEFAULT_LANG) {
	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": [endPoints.wpServices(city, lang)]
		})).then(function (response) {
			// handle success
			dispatch(setServices(processServices(response.data.wpservices)));
			dispatch(setFooterServices(footerServices(response.data.wpservices)));

		}).catch(function (error) {
			// handle error
			console.log("Error Fetching Services by City", error);
		});
	}
}
// Fetch wpAllServicesWithCity
export function fetchwpAllServicesWithCity(lang = DEFAULT_LANG) {
	return axios.post('', encryptBody({
		"apis": [endPoints.wpAllServicesWithCity(lang)]
	})).then(function (response) {
		// handle success
		return (response.data.wpAllServicesWithCity);

	}).catch(function (error) {
		// handle error
		console.log("Error Fetching wpAllServicesWithCity");
	});
}
// Fetch Homepage Data
export function fetchHomepageData(city, extraData, lang = DEFAULT_LANG) {
	let apis = [
		endPoints.whySMData(lang), 
		endPoints.howIWData(lang), 
		endPoints.logosData(lang), 
		endPoints.footerItemsData(lang)
	];
	//console.log("extraData", apis);
	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": apis
		})).then(function (response) {
			// handle success
			extraData.map((item) => dispatch(eval(item + "(response.data." + item + ")")));
		}).catch(function (error) {
				// handle error
				console.log("Error Fetching fetchHomepageData");
			});
	}
}
// Fetch Landingpage Data
export function fetchLandingData(city, slug, extraData, lang = DEFAULT_LANG) {
	let apis = [
		endPoints.landingData(city, slug, lang),
		endPoints.whySMData(lang), 
		endPoints.howIWData(lang), 
		endPoints.logosData(lang), 
		endPoints.footerItemsData(lang)
	];
	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": apis
		})).then(function (response) {
			// handle success
			dispatch(landingData(response.data.landing));
			extraData.map((item) => dispatch(eval(item + "(response.data." + item + ")")));
			return response.data.landing;
		})
			.catch(function (error) {
				// handle error
				console.log("Error Fetching fetchLandingData");
			});
	}
}
// Fetch Landingpage Confirmation Data
export function fetchLandingConfirmationData(city, slug, extraData, lang = DEFAULT_LANG) {
	let apis = [
		endPoints.landingData(city, slug, lang),
		endPoints.whySMData(lang), 
		endPoints.howIWData(lang), 
		endPoints.logosData(lang), 
		endPoints.footerItemsData(lang)
	];
	
	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": apis
		})).then(function (response) {
			// handle success
			dispatch(landingConfirmationData(response.data.landing));
			extraData.map((item) => dispatch(eval(item + "(response.data." + item + ")")));
		})
			.catch(function (error) {
				// handle error
				console.log("Error Fetching fetchLandingConfirmationData");
			});
	}
}
// Fetch Booking Faqs without dispatch
export function fetchBookingFaqs(city, slug, typeOfFlow, lang = DEFAULT_LANG) {
	const apis = [];
	return axios.post('', encryptBody({
		"apis": [endPoints.landingData(city, slug ,lang)]
	})).then(function (response) {
		// handle success
		if (response.data.landing.length) {
			if (typeOfFlow == 'booking') {
				return (response.data.landing[0].acf.booking_faqs && response.data.landing[0].acf.booking_faqs.length) ? response.data.landing[0].acf.booking_faqs : response.data.landing[0].acf.frequently_asked_questions;
			}
			return (response.data.landing[0].acf.quotes_faqs && response.data.landing[0].acf.quotes_faqs.length ? response.data.landing[0].acf.quotes_faqs : response.data.landing[0].acf.frequently_asked_questions);
		} else {
			return [];
		}
	})
		.catch(function (error) {
			// handle error
			console.log("Error Fetching fetchBookingFaqs");
		});
}
// Fetch About Page Data
export function fetchAboutData(lang = DEFAULT_LANG) {
	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": [endPoints.staticPageData(staticVariables.STATIC_PAGE_ABOUT, "about", lang)]
		})).then(function (response) {
			// handle success
			dispatch(aboutData(response.data.about[0]));
		}).catch(function (error) {
			// handle error
			console.log("Error Fetching fetchAboutData");
		});
	}
}
// Fetch About Page Data
export function fetchFooterItemsData(lang = DEFAULT_LANG) {
	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": [endPoints.footerItemsData(lang)]
		})).then(function (response) {
			// handle success
			dispatch(footerItemsData(response.data.footerItemsData));
		}).catch(function (error) {
			// handle error
			console.log("Error Fetching fetchFooterItemsData");
		});
	}
}
// Fetch About Page Data
export function fetchPopupData(lang = DEFAULT_LANG) {
	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": [endPoints.popupOfferData(lang)]
		})).then(function (response) {
			// handle success
			dispatch(popupMenuData(response.data.popupOffer));
		}).catch(function (error) {
			// handle error
			console.log("Error Fetching fetchPopupData");
		});
	}
}
// Fetch Become Partner Data
export function fetchBecomePartnerData(lang = DEFAULT_LANG) {
	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": [endPoints.staticPageData(staticVariables.STATIC_PAGE_BE_PARTNER, "bepartner", lang)]
		})).then(function (response) {
			// handle success
			let dispatchData = typeof response.data.bepartner[0] != "undefined" ? response.data.bepartner[0] : {};
			dispatch(bePartnerData(dispatchData));
		})
			.catch(function (error) {
				// handle error
				console.log("Error Fetching fetchBecomePartnerData");
			});
	}
}
// Fetch Review Data
export function fetchReviewData(lang = DEFAULT_LANG) {
	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": [endPoints.staticPageData(staticVariables.STATIC_PAGE_REVIEW, "review", lang)]
		})).then(function (response) {
			// handle success
			let dispatchData = typeof response.data.review[0] != "undefined" ? response.data.review[0] : {};
			dispatch(reviewData(dispatchData));
		})
			.catch(function (error) {
				// handle error
				console.log("Error Fetching fetchReviewData");
			});
	}
}
// Fetch Team Data
export function fetchTeamData(lang = DEFAULT_LANG) {
	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": [endPoints.staticPageData(staticVariables.STATIC_PAGE_TEAM, "team", lang)]
		})).then(function (response) {
			// handle success
			let dispatchData = typeof response.data.team[0] != "undefined" ? response.data.team[0] : {};
			dispatch(teamData(dispatchData));
		})
			.catch(function (error) {
				// handle error
				console.log("Error Fetching fetchTeamData");
			});
	}
}
// Fetch Faqs Data
export function fetchFaqsData(lang = DEFAULT_LANG) {
	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": [endPoints.staticPageData(staticVariables.STATIC_PAGE_FAQS, "faqs", lang)]
		})).then(function (response) {
			// handle success
			let dispatchData = typeof response.data.faqs[0] != "undefined" ? response.data.faqs[0] : {};
			dispatch(faqsData(dispatchData));
		})
			.catch(function (error) {
				// handle error
				console.log("Error Fetching fetchFaqsData");
			});
	}
}
// Fetch Privacy Data
export function fetchPrivacyData(lang = DEFAULT_LANG) {
	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": [endPoints.staticPageData(staticVariables.STATIC_PAGE_PRIVACY, "privacy", lang)]
		})).then(function (response) {
			// handle success
			let dispatchData = typeof response.data.privacy[0] != "undefined" ? response.data.privacy[0] : response.data.terms;
			dispatch(privacyData(dispatchData));
		})
			.catch(function (error) {
				// handle error
				console.log("Error Fetching fetchPrivacyData");
			});
	}
}
// Fetch Companies Page Data
export function fetchCompaniesPageData(lang = DEFAULT_LANG) {
	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": [endPoints.staticPageData(staticVariables.STATIC_PAGE_COMPANIES, "companiesPage", lang)]
		})).then(function (response) {
			// handle success
			let dispatchData = typeof response.data.companiesPage[0] != "undefined" ? response.data.companiesPage[0] : response.data.terms;
			dispatch(companiesData(dispatchData));
			//dispatch(companiesData(response.data.companiesPage));
		})
			.catch(function (error) {
				// handle error
				console.log("Error Fetching fetchCompaniesPageData");
			});
	}
}
// Fetch Disclaimer Data
export function fetchDisclaimerData(lang = DEFAULT_LANG) {
	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": [endPoints.staticPageData(staticVariables.STATIC_PAGE_DISCLAIMER, "disclaimer", lang)]
		})).then(function (response) {
			// handle success
			let dispatchData = typeof response.data.disclaimer[0] != "undefined" ? response.data.disclaimer[0] : response.data.terms;
			dispatch(disclaimerData(dispatchData));
			//dispatch(disclaimerData(response.data.disclaimer));
		})
			.catch(function (error) {
				// handle error
				console.log("Error Fetching fetchDisclaimerData");
			});
	}
}
// Fetch Terms Data
export function fetchTermsData(lang = DEFAULT_LANG) {
	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": [endPoints.staticPageData(staticVariables.STATIC_PAGE_TERMS, "terms", lang)]
		})).then(function (response) {
			// handle success
			let dispatchData = typeof response.data.terms[0] != "undefined" ? response.data.terms[0] : response.data.terms;
			dispatch(termsData(dispatchData));
		})
			.catch(function (error) {
				// handle error
				console.log("Error Fetching fetchTermsData");
			});
	}
}
// Fetch PRP Data
export function fetchPRPData(lang = DEFAULT_LANG) {
	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": [endPoints.staticPageData(staticVariables.STATIC_PAGE_PRP, "prp", lang)]
		})).then(function (response) {
			// handle success
			dispatch(prpData(response.data.prp[0]));
		})
			.catch(function (error) {
				// handle error
				console.log("Error Fetching fetchPRPData");
			});
	}
}
// Fetch Offers Data
export function fetchOffersData(token, locationCode, pageServiceId, movingSize, lang = DEFAULT_LANG) {
	return axios.post('', encryptBody({
		"apis": [endPoints.getDeals(token, locationCode, pageServiceId, movingSize, lang)]
	})).then(function (response) {
		// handle success
		return response.data.offers;
	})
		.catch(function (error) {
			// handle error
			console.log("Error Fetching Offers");
		});
}
// Add Request
export function postRequest(accessToken, data , url = "request/add", signedInUserId = 0 ,lang = DEFAULT_LANG){
		return axios.post('', encryptBody({
			"apis" : [endPoints.addRequest(accessToken, data, url ,lang)]
		})).then(function (response) {
			// handle success
            return response;
		})
		.catch(function (error) {
			// handle error
			console.log("Error Fetching postRequest", error);
			return {};
		});
}
export function postCreateBookingRequestDto(accessToken, data, lang = DEFAULT_LANG){
	return axios.post('', encryptBody({
		"apis" : [endPoints.createBookingRequestDto( accessToken, data ,lang)]
	})).then(function (response) {
		// handle success
		return response;
	})
	.catch(function (error) {
		// handle error
		console.log("Error Fetching postRequest");
		return {};
	});
}

export function unsubscribeBookingRequest(accessToken, bookingID, lang = DEFAULT_LANG){
    return (dispatch) => {
        return axios.post('', encryptBody({
            "apis": [endPoints.unsubscribeBooking(accessToken, bookingID ,lang)]
        })).then(function (response) {
            // handle success
            var result = response.data.unsubscribeBooking;
            try{
                result = JSON.parse(result);
			}catch (e) {

            }
			if(result.success)
            	dispatch(unsubscribeBookingAction(bookingID));
            return result;
        })
            .catch(function (error) {
                // handle error
                console.log("Error Fetching unsubscribeBookingRequest");
                return {};
            });
    }
}
export function postPhotographyRequest(accessToken, data, signedInUserId, lang = DEFAULT_LANG){
	return axios.post('', encryptBody({
		"apis": [endPoints.addPhotographyRequest(accessToken, data, signedInUserId ,lang)]
	})).then(function (response) {
		// handle success
		return response;
	})
		.catch(function (error) {
			// handle error
			console.log("Error Fetching postPhotographyRequest");
			return {};
		});
}
// Add Lite weight booking Request
export function postLWRequest(data, lang = DEFAULT_LANG) {
	return axios.post(staticVariables.MIDDLEWARE_URL + '?time=0&type=lite_wight', encryptBody({
		"apis": [endPoints.addLWRequest(data ,lang)]
	})).then(function (response) {
		// handle success
		return response;
	})
		.catch(function (error) {
			// handle error
			console.log("Error Fetching postLWRequest");
			return {};
		});
}
export function updateLWRequest(data, lang = DEFAULT_LANG) {
	return axios.post(staticVariables.MIDDLEWARE_URL + '?time=0&type=lite_weight_update', encryptBody({
		"apis": [endPoints.updateLWRequest(data ,lang)]
	})).then(function (response) {
		// handle success
		//var response = {"resp":false,"Number_of_months1":null,"Steam_Queen_Size":null,"Owner":{"name":"ServiceMarket Booking","id":"2354754000000112007"},"Orginal_Price1":436,"Material_charge":null,"Monthly_Price":null,"Shampoo_Cleaning_no_of_sq_meter":null,"Reschedule_notes":null,"Hourly_rate":92.65,"Voucher_Code":"MOHSIN15","OPS_Reference_no":null,"$process_flow":false,"Currency":"AED","Oxyclean_option":"No","id":"2354754000021054227","Type_of_handyman":null,"Steam_Pillow":null,"$approval":{"delegate":false,"approve":false,"reject":false,"resubmit":false},"Move_provider_category":null,"Move_from":null,"Need_packing":false,"First_Visited_URL":null,"Store_Till":null,"Created_Time":"2018-10-29T17:12:11+04:00","Steam_cleaning_no_of_sq_meter":null,"Subtotal":370.6,"Payment_Method":"Credit card","More_Info":null,"Move_request_notes":null,"Oxyclean":false,"App_version":"2.5","Storage_Provider_category":null,"Store_From":null,"Steam_cleaning_no_of_seats":null,"Agent_Name_Storage":null,"Last_Visited_Time":null,"Type_of_pest_control":null,"Oxyclean_charge":null,"Home_furnished":false,"Repeat_customer":false,"Number_Of_Chats":null,"Average_Time_Spent_Minutes":null,"Transport_Fee":null,"Salutation":null,"Type_of_cleaing":null,"Full_Name":"Mohsin Test","Record_Image":null,"Required_on":"2018-10-31T09:00:00+04:00","Worker":null,"Need_tile_scrubbing":false,"Number_of_units":null,"Company_Email":null,"Moving_Agent_Name":null,"Handyman_Total_price":null,"Rooms":null,"Required_when":"I need them at a specific day and time","Need_Scrubbing":"No","Layout":{"name":"Standard","id":"2354754000000091055"},"isTest":true,"Lead_Source":"Android","ServiceMarket_Commission_Storage":null,"Requested_Partner_email":null,"Tag":[],"Redemption_Id":"r_k1GTwGL3aGb3u6q1fTvH0IBc","Email":"mohsin29oct@mailinator.com","$currency_symbol":"AED","Need_pick_up_and_delivery":false,"Storage_Type":null,"Visitor_Score":null,"Booking_Status":"Open","Tax":18.53,"TV_Mounting_Size":null,"Last_Activity_Time":"2018-10-30T17:57:57+04:00","IsCreditCardError":false,"Normal_Queen_Size":null,"$converted":false,"Exchange_Rate":1,"Steam_Single_Size":null,"Estimated_CBM1":null,"isCouponApplied":"true","Scrubbing_charge":null,"$approved":true,"Items_needed_to_store":null,"Booking_status_changed_by":null,"Days_Visited":null,"$editable":true,"City":"Dubai","Move_size":null,"Area":"AbuHail","Supplier_comments":null,"Price":389.13,"No_of_Hours":4,"Book_again":false,"Company_Name":null,"Normal_Single_Size":null,"Cloned":false,"Partner_Id":null,"Message":null,"Requester_comments":null,"Request_acceptedd_on":null,"Furnished":"No","First_Name":"Mohsin","Payment_Status":"Pending","Normal_Pillow":null,"Phone":"+921234567890","Residence_Types":null,"Normal_King_Size":null,"Need_a_handyman":false,"Customer_Rating":null,"Move_to":null,"Apartment_Villa_Number":"Test Apartment, Test Building","Moving_reminder_sent":false,"Modified_Time":"2018-10-30T17:57:57+04:00","Required_date":"31\/10\/2018","Rating_Comments":null,"Company_Phone":null,"Steam_King_Size":null,"$converted_detail":[],"Service":"Plumbing","Auth_token":"3889","Prediction_Score":null,"Attachments1":null,"First_Visited_Time":null,"Last_Name":"Test","Additional_One_Time_Charges":null,"UserId":47991,"Requested_Worker":null,"Unit_size":null,"Referrer":null,"Moving_SM_Commission":null,"Shampoo_Cleaning_no_of_seats":null}

		return response;
	}).catch(function (error) {
		// handle error
		console.log("Error updateLWRequest");
		return {};
	});
}
// Delete Lite weight booking Request
export function deleteLWRequest(ids, lang = DEFAULT_LANG) {
	return axios.post(staticVariables.MIDDLEWARE_URL + '?time=0&type=lite_wight_delete&ids=' + ids).then(function (response) {
		// handle success
		return response;
	}).catch(function (error) {
		console.log("Error deleteLWRequest");
		return {};
	});
}
// Get City Code
export function getCityCode(cityName, cities, lang = DEFAULT_LANG) {
	const temp = cities.filter(city => city.slug.toLocaleLowerCase().split(" ").join("-") == cityName);
	if (temp.length) {
		return temp[0].code;
	}
	return 'dxb';
}
export function fetchCustomerBookingHistory(token, lang = DEFAULT_LANG) {
	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": [endPoints.getCustomerBookingHistory(token ,lang)]
		})).then(function (response) {
			// handle success
			dispatch(myBookingsAction(response.data.getCustomerBookingHistory.data));
		})
			.catch(function (error) {
				// handle error
				console.log("Error Fetching fetchCustomerBookingHistory");
			});
	}
}
export function fetchCustomerQuotesHistory(token, lang = DEFAULT_LANG) {
	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": [endPoints.getCustomerQuotesHistory(token ,lang)]
		})).then(function (response) {
			// handle success
			dispatch(myQuotesAction(response.data.getCustomerQuotesHistory));
		})
			.catch(function (error) {
				// handle error
				console.log("Error Fetching fetchCustomerQuotesHistory");
			});
	}
}
export function fetchLeadQuotes(token, id, lang = DEFAULT_LANG) {
	return axios.post('', encryptBody({
		"apis": [endPoints.getLeadQuotes(token, id ,lang)]
	})).then(function (response) {
		// handle success
		return response.data.getLeadQuotes;
	}).catch(function (error) {
		// handle error
		console.log("Error Fetching fetchCustomerQuotesHistory");
	});
}
export function fetchCancelingReason(token, serviceId, feedbackType, feedbackRequestType, lang = DEFAULT_LANG) {
	return axios.post('', encryptBody({
		"apis": [endPoints.getCancelingReason(token, serviceId, feedbackType, feedbackRequestType ,lang)]
	})).then(function (response) {
		// handle success
		return response.data.getCancelingReason;
	}).catch(function (error) {
		// handle error
		console.log("Error Fetching fetchCancelingReason");
	});
}
export function cancelBookingRequest(customerId, token, bookingId, cancellationReason, comment, statusId, feedbackDto, signedInUserId, lang = DEFAULT_LANG) {
	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": [endPoints.cancelCustomerBooking(customerId, token, bookingId, cancellationReason, comment, statusId, feedbackDto, signedInUserId ,lang)]
		})).then(function (response) {
			if (response.data.cancelCustomerBooking.success){
				dispatch(cancelBookingAction(bookingId));
			}
			return response.data.cancelCustomerBooking;
		})
		.catch(function (error) {
			// handle error
			console.log("Error Fetching cancelBookingRequest");
		});
	}
}
export function cancelBookingEventRequest(customerId, token, eventId, cancellationReason, comment, statusId, feedbackDto, lang = DEFAULT_LANG) {
	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": [endPoints.cancelCustomerBookingEvent(customerId, token, eventId, cancellationReason, comment, statusId, feedbackDto ,lang)]
		})).then(function (response) {
			console.log(response);
			if (response.data.cancelCustomerBookingEvent.success){
				dispatch(cancelBookingEventAction(eventId));
				return response.data.cancelCustomerBookingEvent;
			}
				
		}).catch(function (error) {
				// handle error
				console.log("Error Fetching cancelBookingEventRequest");
		});
	}
}
export function freezeBookingRequest(accessToken, bookingID, startDate, endDate, lang = DEFAULT_LANG) {
	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": [endPoints.freezeBooking(accessToken, bookingID, startDate, endDate ,lang)]
		})).then(function (response) {
			// handle success
			console.log(response);

			var result = response.data.freezeBooking;
			try {
				result = JSON.parse(result);
			} catch (e) {

			}

			if (result.success)
				dispatch(freezeBookingAction(bookingID, startDate, endDate));

			return result;
		}).catch(function (error) {
			// handle error
			console.log("Error Fetching freezeBookingRequest");
			console.log(error);
		});
	}
}
export function newChangeBookingRequest(accessToken,initiatorId, newRequest={}, lang = DEFAULT_LANG) {
	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": [endPoints.newChangeBookingRequest(accessToken,initiatorId, newRequest ,lang)]
		})).then(function (response) {
			// handle success
			//console.log(response);
			var result = response.data.changeRequest;
			try {
				result = JSON.parse(result);
			} catch (e) {

			}
			if (result.success){
				dispatch(editAllBookingAction(newRequest.booking_id));
			}
			return result;
		}).catch(function (error) {
			// handle error
			console.log("Error Fetching newChangeBookingRequest");
			console.log(error);
		});
	}
}
export function changeBookingRequest(accessToken, bookingID, initiatorId, bookingEventId, changeType, newDate, newTime, newDuration, newNumberOfWorkers, initiatedBy, lang = DEFAULT_LANG) {
	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": [endPoints.changeBookingRequest(accessToken, bookingID, initiatorId, bookingEventId, changeType, newDate, newTime, newDuration, newNumberOfWorkers, initiatedBy ,lang)]
		})).then(function (response) {
			// handle success
			//console.log(response);
			var result = response.data.changeRequest;
			try {
				result = JSON.parse(result);
			} catch (e) {

			}
			if (bookingEventId !== '') {
				if (result.success)
					dispatch(editBookingAction(bookingEventId));
			} else {
				if (result.success)
					dispatch(editAllBookingAction(bookingID));
			}

			console.log(result);
			return result;
		}).catch(function (error) {
			// handle error
			console.log("Error Fetching changeBookingRequest");
			console.log(error);
		});
	}
}
export function submitContactUs(token, message, email, firstname, lastname, phone, lang = DEFAULT_LANG) {
	return axios.post('', encryptBody({
		"apis": [endPoints.submitContactUsForm(token, message, email, firstname, lastname, phone ,lang)]
	})).then(function (response) {
		// handle success
		return JSON.parse(response.data.submitContactUs);
	}).catch(function (error) {
		// handle error
		console.log("Error Fetching submitContactUs");
	});
}
export function fetchBookingPricingPlan(serviceId, cityId, isSubscription = false, lang = DEFAULT_LANG) {
	var pricePlanPayload = endPoints.pricePlan(serviceId, cityId, isSubscription ,lang);
	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": [pricePlanPayload]
		})).then(function (response) {
			// handle success
			//console.log(response);
			var pricePlan = response.data.pricePlan;
			dispatch(setCleaningPricePlan(pricePlan.data));

		}).catch(function (error) {
			// handle error
			console.log("Error fetchBookingPricingPlan", error, pricePlanPayload);
		});
	}
}
export function fetchAllServices(lang = DEFAULT_LANG) {
	var servicesPayload = endPoints.getAllServices(lang);
	var get_all_service_url = servicesPayload.url;
	var headers = servicesPayload.headers;
	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": [servicesPayload]
		})).then(function (response) {
			var services = response.data.services;
			// handle success
			dispatch(setAllConstants(services.data, lang));
			dispatch(setAllServiceLookUpConstants(services.data));
			//console.log("fetchAllServices", response);

		}).catch(function (error) {
			// handle error
			console.log("Error fetchAllServices");
		});
	}
}
export function fetchAllApiServices(cityCode, lang = DEFAULT_LANG) {
	var apiServicesPayload = endPoints.apiServices(cityCode ,lang);
	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": [apiServicesPayload]
		})).then(function (response) {
			var services = response.data.apiservices;
			// handle success
			var data = services.data;
			dispatch(setAllAPIServices(data));
			dispatch(setBookingServices(data));

		}).catch(function (error) {
			// handle error
			console.log("Error fetchAllApiServices");
		});
	}
}

export function fetchSimilarPartners(partnerId, success, error, lang = DEFAULT_LANG) {
	const similarCompaniesPayload = endPoints.getSimilarCompanies(partnerId ,lang);

	return axios.post('', encryptBody({
		"apis": [similarCompaniesPayload]
	})).then(function (response) {
		// handle success
		const data = response.data.similarProviders;
		success(data);

	}).catch(function (err) {
		// handle error
		console.log("Error fetchSimilarPartners");
		error(err);

	});

}

export function fetchCountriesListAPI(success, erro, lang = DEFAULT_LANG) {
	
	var countriesListPayload = endPoints.getCountriesListAPI(lang);
	var get_all_data_constant_url = countriesListPayload.url;
	var headers = countriesListPayload.headers;
	//console.log(countriesListPayload);

	return axios.post('', encryptBody({
		"apis": [countriesListPayload]
	})).then(function (response) {
		// handle success
		var data = response.data.countries;
		//  console.log('console from api call');
		//  console.log(data);
		//dispatch(setAllCountries(data));
		success(data);
	}).catch(function (err) {
		// handle error
		console.log("Error fetchCountriesListAPI");
		//error(err);
	});

}


export function fetchCarYearsAPI() {

	// var CarYearsPayload = endPoints.getCarModelYearsAPI();
	// var get_all_data_constant_url = CarYearsPayload.url;
	// var headers = CarYearsPayload.headers;
	// //console.log(CarYearsPayload);

	// return axios.post('', encryptBody({
	// 	"apis": [CarYearsPayload]
	// })).then(function (response) {
	// 	// handle success
	// 	var data = response.data.carYears;
	// 	//  console.log('console from api call');
	// 	//console.log(data);
	// 	//dispatch(setAllCountries(data));
	// 	success(data);
	// }).catch(function (err) {
	// 	// handle error
	// 	console.log("Error fetchCarYearsAPI");
	// 	error(err);
	// });
	const d = new Date();
	let years=[];
	for (let i=d.getFullYear(); i>=stringConstants.YEAR_1950;i--)
		years.push(i);

		return years;

}


export function fetchCarDetailsAPI(year, success, error, lang = DEFAULT_LANG) {

	var CarDetailsPayload = endPoints.getCarDetailsAPI(year ,lang);
	var get_all_data_constant_url = CarDetailsPayload.url;
	var headers = CarDetailsPayload.headers;
	//console.log(countriesListPayload);

	return axios.post('', encryptBody({
		"apis": [CarDetailsPayload]
	})).then(function (response) {
		// handle success
		var data = response.data.CarDetails;

		success(data);
	}).catch(function (err) {
		// handle error
		console.log("Error fetchCarDetailsAPI");
		//error(err);
	});

}

export function fetchDataDictionaryValues( lang = DEFAULT_LANG ) {
	var dataDictionaryPayload = endPoints.dataDictionaryValues(lang);
	var get_all_data_constant_url = dataDictionaryPayload.url;
	var headers = dataDictionaryPayload.headers;
	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": [dataDictionaryPayload]
		})).then(function (response) {
			// handle success
			var dataConstants = response.data.dataConstants;
			dispatch(setAlldataDictionaryValues(dataConstants.data));

		}).catch(function (error) {
			// handle error
			console.log("Error fetchDataDictionaryValues");
		});
	}
}
export function fetchDataConstantValues(lang = DEFAULT_LANG) {
	var dataConstantPayload = endPoints.dataConstantValues(lang);
	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": [dataConstantPayload]
		})).then(function (response) {
			// handle success
			var dataConstants = response.data.dataConstants;
			//console.log("dataConstants", dataConstants)
			dispatch(setAlldataConstantValues(dataConstants.data));

		}).catch(function (error) {
			// handle error
			console.log("Error fetchDataConstantValues");
		});
	}
}

export function sendServiceReview(eventId, data, lang = DEFAULT_LANG) {
	var submitServiceReview = endPoints.submitServiceReview(data ,lang);

	return axios.post(staticVariables.MIDDLEWARE_URL + '?time=0&type=lite_weight_rating_update&id=' + eventId, encryptBody({
		"apis": [submitServiceReview]
	})).then(function (response) {
		// handle success
		return response;
	}).catch(function (error) {
		// handle error
		console.log("Error Fetching sendServiceReview");
		return {};
	});
}
export function isServiceReviewed(eventId, lang = DEFAULT_LANG) {
	var serviceReviewed = endPoints.isServiceReviewed(eventId ,lang);

	return axios.post(staticVariables.MIDDLEWARE_URL + '?time=0&type=lite_weight_rating&id=' + eventId, {
		"apis": [serviceReviewed]
	}).then(function (response) {
		// handle success
		return response;
	}).catch(function (error) {
		// handle error
		console.log("Error Fetching isServiceReviewed", error);
		return {};
	});
}
export function sendMaidReview(data, lang = DEFAULT_LANG) {
	var submitMaidReview = endPoints.submitMaidReview(data ,lang)
	return axios.post('', encryptBody({
		"apis": [submitMaidReview]
	})).then(function (response) {
		return response.data.submitMaidreview;
	}).catch(function (err) {
		// handle error
		console.log("Error sendMaidReview",err);
		//error(err);
	});
}
export function isEventReviewed(eventId, isMaidReviewedResponse = null, lang = DEFAULT_LANG) {
	var eventReviewed = endPoints.isEventReviewed(eventId, lang)
	return axios.post('', encryptBody({
		"apis": [eventReviewed]
	})).then(function (response) {
		return response.data.eventReview;
	}).catch(function (err) {
		// handle error
		console.log("Error isEventReviewed",err);
		//error(err);
	});
}
export function getWorkersDetailByEventId(EventId, lang = DEFAULT_LANG) {
	var workersDetails = endPoints.getWorkersDetails(EventId, lang);
	return axios.post('', encryptBody({
		"apis": [workersDetails]
	})).then(function (response) {
		return response.data.workersDetails;
	}).catch(function (err) {
		// handle error
		console.log("Error getWorkersDetailByEventId", err);
		//error(err);
	});
}
export function fetchTaxPlan(city_id, lang = DEFAULT_LANG) {
	var taxPlanPayload = endPoints.taxPlan(city_id, lang);
	var tax_plan_url = taxPlanPayload.url;
	var headers = taxPlanPayload.headers;
	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": [taxPlanPayload]
		})).then(function (response) {
			// handle success
			var taxPlan = response.data.taxPlan;
			dispatch(setTaxPlan(taxPlan.data));

		}).catch(function (error) {
			// handle error
			console.log("Error fetchTaxPlan",error, taxPlanPayload);
		});
	}
}
export function fetchZohoMigratedService(lang = DEFAULT_LANG){
	var zohoMigratedPayload = endPoints.zohoMigratedService(lang);
	return (dispatch)=>{
		return axios.post('', encryptBody({
			"apis" : [ zohoMigratedPayload ]
		})).then(function (response) {
			// handle success
			var zohoMigratedService = response.data.zohoMigratedService;
			dispatch(setZohoMigratedService(zohoMigratedService.data));

		}).catch(function (error) {
			// handle error
			console.log("Error fetchTaxPlan", );
		});
	}
}

export function fetchDateTime(countryId = UAE_ID) {
	return (dispatch) => {
		let version = new Date().getTime();
		return axios.get(staticVariables.MIDDLEWARE_URL + '?time=0&type=get-current-time&country_id=' + countryId+"&version="+version, {}).then(function (response) {
			var dateTimeResponse = response.data;
			if (typeof dateTimeResponse.success != "undefined" && dateTimeResponse.success) {
				dispatch(setDateTime(dateTimeResponse.current_time));
			}


		}).catch(function (error) {
			// handle error
			console.log("Error fetchDateTime");
		});
		//return axios.get(staticVariables.MIDDLEWARE_URL + '?time=0&type=get-current-time')
	}
}
export function fetchDateTimeAvailability(accessToken = '', lang = DEFAULT_LANG) {
	var dateTimeAvailabilityPayload = endPoints.dateTimeAvailability(accessToken, lang);
	var date_time_availability_url = dateTimeAvailabilityPayload.url;
	var headers = dateTimeAvailabilityPayload.headers;

	return (dispatch) => {
		return axios.post('', encryptBody({
			"apis": [dateTimeAvailabilityPayload]
		})).then(function (response) {
			// handle success
			var DateTimeAvailability = response.data.DateTimeAvailability;
			//console.log("DateTimeAvailability",DateTimeAvailability);
			if(typeof DateTimeAvailability.data != "undefined"){
				dispatch(setDateTimeAvailability(DateTimeAvailability.data));
			}

		}).catch(function (error) {
			// handle error
			console.log("Error fetchDateTimeAvailability_error", error);
		});
	}
}

export function couponValidate(data, isCustomerLoggedIn = false, user_token, signedInUserId = 0, signedInCustomerId = 0, lang = DEFAULT_LANG) {
	var couponValidationPayload = endPoints.couponValidate(data, isCustomerLoggedIn, user_token, signedInUserId, signedInCustomerId ,lang);
	var headers = couponValidationPayload.headers;

	return axios.post('', encryptBody({
		"apis": [couponValidationPayload]
	})).then(function (response) {
		// handle success
		return response.data;
	})
}
export function VoucherifyValidate(data, userToken = "", lang = DEFAULT_LANG) {
	data = JSON.stringify(data);
	var voucherValidationPayload = endPoints.voucherValidate(data, userToken ,lang);

	return axios.post('', encryptBody({
		"apis": [voucherValidationPayload]
	})).then(function (response) {
		// handle success
		return response.data;
	})
}
export function VoucherifyRedeem(data, userToken, lang = DEFAULT_LANG) {
	data = JSON.stringify(data);
	console.log("VoucherifyRedeem 123", data);
	var voucherValidationPayload = endPoints.voucherRedemption(data, userToken ,lang);

	return axios.post('', encryptBody({
		"apis": [voucherValidationPayload]
	})).then(function (response) {
		// handle success
		return response.data;
	})
}
export function getPricer(data, isCustomerLoggedIn = false, user_token, lang = DEFAULT_LANG) {
	//console.log(user_token);

	var getPricerPayload = endPoints.getPricer(JSON.stringify(data), isCustomerLoggedIn, user_token ,lang);

	return axios.post('', encryptBody({
		"apis": [getPricerPayload]
	})).then(function (response) {
		// handle success
		return response.data;
	})
}

// Build Services Groups
function categoriesOrder(){
	return [1, 3, 6, 4, 5, 7, 2];
}
function footerCategoriesOrder(){
	return [1, 2, 3, 4, 5, 6, 7];
}
function processServices(services) {
	let categoriesOrder = [1, 3, 6, 4, 5, 7, 2];
	if(services.length){
		const mainServices = services.filter(item => item.show_in_header == true);
		const category1 = mainServices.filter(item => Array.isArray(item.header_category) ? item.header_category.filter((cat) => cat.value == categoriesOrder[0]).length > 0 : false);
		const category2 = mainServices.filter(item => Array.isArray(item.header_category) ? item.header_category.filter((cat) => cat.value == categoriesOrder[1]).length > 0 : false);
		const category3 = mainServices.filter(item => Array.isArray(item.header_category) ? item.header_category.filter((cat) => cat.value == categoriesOrder[2]).length > 0 : false);
		const category4 = mainServices.filter(item => Array.isArray(item.header_category) ? item.header_category.filter((cat) => cat.value == categoriesOrder[3]).length > 0 : false);
		const category5 = mainServices.filter(item => Array.isArray(item.header_category) ? item.header_category.filter((cat) => cat.value == categoriesOrder[4]).length > 0 : false);
		const category6 = mainServices.filter(item => Array.isArray(item.header_category) ? item.header_category.filter((cat) => cat.value == categoriesOrder[5]).length > 0 : false);
		const category7 = mainServices.filter(item => Array.isArray(item.header_category) ? item.header_category.filter((cat) => cat.value == categoriesOrder[6]).length > 0 : false);
		return [category1, category2, category3, category4, category5, category6, category7];
	}else{
		return [];
	}
}

// Build Footer Services Groups
function footerServices(services) {
	let footerCategoriesOrder = [1, 2, 3, 4, 5, 6, 7];
	const mainServices = services.filter(item => item.show_in_footer == true);
	const category1 = mainServices.filter(item => Array.isArray(item.footer_category) ? item.footer_category.filter((cat) => cat.value == footerCategoriesOrder[0]).length > 0 : false);
	const category2 = mainServices.filter(item => Array.isArray(item.footer_category) ? item.footer_category.filter((cat) => cat.value == footerCategoriesOrder[1]).length > 0 : false);
	const category3 = mainServices.filter(item => Array.isArray(item.footer_category) ? item.footer_category.filter((cat) => cat.value == footerCategoriesOrder[2]).length > 0 : false);
	const category4 = mainServices.filter(item => Array.isArray(item.footer_category) ? item.footer_category.filter((cat) => cat.value == footerCategoriesOrder[3]).length > 0 : false);
	const category5 = mainServices.filter(item => Array.isArray(item.footer_category) ? item.footer_category.filter((cat) => cat.value == footerCategoriesOrder[4]).length > 0 : false);
	const category6 = mainServices.filter(item => Array.isArray(item.footer_category) ? item.footer_category.filter((cat) => cat.value == footerCategoriesOrder[5]).length > 0 : false);
	const category7 = mainServices.filter(item => Array.isArray(item.footer_category) ? item.footer_category.filter((cat) => cat.value == footerCategoriesOrder[6]).length > 0 : false);
	return [category1, category2, category3, category4, category5, category6, category7];
}

// Get Select City Options
export function getCityOptions(countryCitiesAreas, currentCity) {
	var citiesOptions = [];
	for (var i = 0; i < countryCitiesAreas.length; i++) {
		if (countryCitiesAreas[i].cities.filter(item => item.name.toLowerCase().split(' ').join('-') == currentCity).length) {
			citiesOptions = countryCitiesAreas[i].cities;
			break;
		}
	}
	citiesOptions.forEach(item => { item.value = item.name.toLowerCase().split(' ').join('-'); item.label = item.name });
	return citiesOptions;
}
// Get Select Area Options
export function getAreaOptions(cityOptions, selectedCity) {
	var areaOptions = [];
	var temp;
	if (typeof cityOptions != "undefined" && selectedCity) {
		for (var i = 0; i < cityOptions.length; i++) {
			if (cityOptions[i].name.toLowerCase().split(' ').join('-') == selectedCity.value) {
				areaOptions = cityOptions[i].areas;
				break;
			}
		}
	}
	return areaOptions;
}

// Form Validation
export function isValidSection(sectionID, lang = DEFAULT_LANG) {
	if((typeof window != "undefined" && typeof window.REDUX_DATA != "undefined") && lang != window.REDUX_DATA.lang ){
		lang = window.REDUX_DATA.lang;
	}
	var valid = true;
	var section = document.getElementById(sectionID);
	var checkPointsElements = section.querySelectorAll('.checkpoint');
	var selectElements = section.querySelectorAll('.dropdown button.invalid-value');
	if (checkPointsElements && checkPointsElements.length) {
		//Array.prototype.forEach.call(checkPointsElements, item => {
		for (var i = 0, len = checkPointsElements.length; i < len; i++) {
			let item = checkPointsElements[i];
			var temp = checkValidationTypes(item, lang);
			if (!temp.valid) {
				valid = false;
			}
			var msgElement = item.parentNode.querySelector('.error-msg');
			if (msgElement) {
				msgElement.innerText = temp.validationMessage;
			} else {
				var span = document.createElement("span");
				span.className = "error-msg";
				span.innerText = temp.validationMessage;
				item.parentNode.appendChild(span);
			}
		}
	}

	selectElements.forEach((item, index) => {
		if (item.className.indexOf('border-danger') == -1) {
			item.className += ' border-danger';
		}
		valid = false;
	});

	if (!valid) {
		window.setTimeout(function () {
			try {
				window.scroll({
					top: elementOffset(section.querySelector('.border-danger')).top - 120,
					behavior: "smooth"
				});
			}catch(err) {
				if (err instanceof TypeError) {
					window.scroll(0, 0)
				} else {
					console.log(err)
				}
			}
		}, 100);
	}

	return valid;
}

// Form Validation
export function updateSingleInputValid(element, lang = DEFAULT_LANG) {
	var temp = checkValidationTypes(element, lang);
	var msgElement = element.parentNode.querySelector('.error-msg');
	if (msgElement) {
		element.parentNode.querySelector('.error-msg').innerText = temp.validationMessage;
	} else {
		var span = document.createElement("span");
		span.className = "error-msg";
		span.innerText = temp.validationMessage;
		element.parentNode.appendChild(span);
	}

}

export function phoneNoValidation(value, lang){
	let numRegExp = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
	return numRegExp.test(value);
	
}
export function parseArabic(string) {
	if(string != ""){
		return string.replace(/[\u0660-\u0669]/g, function (c) {
			return c.charCodeAt(0) - 0x0660;
		}).replace(/[\u06f0-\u06f9]/g, function (c) {
		   return c.charCodeAt(0) - 0x06f0;
	   });
	}else{
		return "";
	}
}
export function isEmpty(value) {
	if (value != null && typeof (value) != undefined) {
		return value.toString().length ? false : true;
	}
	return true;
}
export function isNumbers(value, lang) {
	let numRegExp = /^\d+$/;
	if(lang == LANG_AR){
		numRegExp = /^[\u0660-\u0669]{10}$/
	}
	if ((value != null && typeof (value) != undefined) && value != 0) {
		return numRegExp.test(value);
	}
	return false;
}
export function isAlphaNumeric(value) {
	if (value != null && typeof (value) != undefined) {
		return /^[a-z0-9]+$/i.test(value);
	}
	return false;
}
export function isLessThan(limit, value) {
	if (value != null && typeof (value) != undefined) {
		return value.toString().length < limit;
	}
	return false;
}
export function isMoreThan(limit, value) {
	if (value != null && typeof (value) != undefined) {
		return value.toString().length > limit;
	}
	return false;
}
export function validEmail(email) {
	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{1,}))$/;
	return re.test(String(email).toLowerCase());
}
function checkValidationTypes(element, lang) {
	var validationMessage = '';
	var inputType = element.className.indexOf('numbers') != -1 || element.className.indexOf('phone') != -1 ? ' digits' : ' characters';
	element.className = element.className.replace(' border-danger', '');
	element.className = element.className.replace(' sm-valid', '');
	let elementVal = typeof element.value != "undefined" ? element.value : "";
	if((element.className.indexOf('ph-valid') != -1)){
		elementVal = parseArabic(elementVal);
	}
	var validationMsg = typeof element.dataset.validationmsg != "undefined" ? element.dataset.validationmsg : "";

	if ((element.className.indexOf('required') != -1) && isEmpty(element.value)) {
		element.className += ' border-danger';
		validationMessage = validationMsg != "" ? validationMsg : locationHelper.translate('THIS_FIELD_IS_REQUIRED', lang);
		return { valid: false, validationMessage: validationMessage };
	}
	if (element.className.indexOf('minDate') != -1) {
		element.className += ' border-danger';
		validationMessage = validationMsg != "" ? validationMsg : locationHelper.translate('TILL_DATE_CANNOT_BE_BEFORE_FROM_DATE', lang);
		return { valid: false, validationMessage: validationMessage };
	}
	if ((element.className.indexOf('fromlist-only') != -1)) {
		element.className += ' border-danger';
		validationMessage = validationMsg != "" ? validationMsg : locationHelper.translate('KINDLY_SELECT_YOUR_AREA_OR_THE_NEAREST_AVAILABLE_AREA_FROM_THE_LIST', lang);
		return { valid: false, validationMessage: validationMessage };
	}
	if ((element.className.indexOf('min-check-1') != -1)) {
		element.className += ' border-danger';
		validationMessage = validationMsg != "" ? validationMsg : locationHelper.translate('KINDLY_SELECT_ONE_BOX_AT_LEAST', lang);
		return { valid: false, validationMessage: validationMessage };
	}
	if ((element.className.indexOf('min-check-2') != -1)) {
		element.className += ' border-danger';
		validationMessage = validationMsg != "" ? validationMsg :locationHelper.translate('KINDLY_SELECT_TWO_DAYS_AT_LEAST', lang);// 'Kindly select two days at least';
		return { valid: false, validationMessage: validationMessage };
	}
	if ((element.className.indexOf('radio-required') != -1)) {
		element.className += ' border-danger';
		validationMessage = validationMsg != "" ? validationMsg : locationHelper.translate('KINDLY_SELECT_ONE_BOX', lang);
		return { valid: false, validationMessage: validationMessage };
	}
	if ((element.className.indexOf('email') != -1) && !validEmail(element.value)) {
		element.className += ' border-danger';
		validationMessage = validationMsg != "" ? validationMsg : locationHelper.translate('SORRY_THE_EMAIL_ADDRESS_YOU_ENTERED_IS_NOT_VALID', lang);
		return { valid: false, validationMessage: validationMessage };
	}
	if ((element.className.indexOf('numbers') != -1) && !isNumbers(element.value, lang)) {
		element.className += ' border-danger';
		validationMessage = validationMsg != "" ? validationMsg : locationHelper.translate('PLEASE_ENTER_NUMBERS_ONLY', lang);
		return { valid: false, validationMessage: validationMessage };
	}
	if ((element.className.indexOf('alphanumeric') != -1) && !isAlphaNumeric(element.value)) {
		element.className += ' border-danger';
		validationMessage = validationMsg != "" ? validationMsg : locationHelper.translate('PLEASE_ENTER_ALPHANUMERIC_ONLY', lang);
		return { valid: false, validationMessage: validationMessage };
	}
	if ((element.className.indexOf('min5') != -1) && isLessThan(5, element.value)) {
		element.className += ' border-danger';
		validationMessage = validationMsg != "" ? validationMsg : 'Minimum is 5' + inputType;
		return { valid: false, validationMessage: validationMessage };
	}
	if ((element.className.indexOf('min8') != -1) && isLessThan(8, element.value)) {
		element.className += ' border-danger';
		validationMessage = validationMsg != "" ? validationMsg : 'Minimum is 8' + inputType;
		return { valid: false, validationMessage: validationMessage };
	}
	if((element.className.indexOf('ph-valid') != -1) && !phoneNoValidation(elementVal)){
		element.className += ' border-danger';
		validationMessage = validationMsg != "" ? validationMsg : locationHelper.translate('PLEASE_ENTER_A_VALID_PHONE_NUMBER_WITH_AT_LEAST_10_DIGITS', lang);
		return { valid: false, validationMessage: validationMessage };
	}
	if ((element.className.indexOf('min10') != -1) && (element.className.indexOf('phone') != -1) && isLessThan(10, element.value)) {
		element.className += ' border-danger';
		validationMessage = validationMsg != "" ? validationMsg : locationHelper.translate('PLEASE_ENTER_A_VALID_PHONE_NUMBER_WITH_AT_LEAST_10_DIGITS', lang);
		return { valid: false, validationMessage: validationMessage };
	}
	if ((element.className.indexOf('min9') != -1) && (element.className.indexOf('phone') != -1) && (isLessThan(9, element.value) || isMoreThan(20, element.value))) {
		element.className += ' border-danger';
		validationMessage = validationMsg != "" ? validationMsg : locationHelper.translate('PLEASE_ENTER_A_VALID_PHONE_NUMBER', lang);
		return { valid: false, validationMessage: validationMessage };
	}
	if ((element.className.indexOf('min13') != -1) && (element.className.indexOf('phone') != -1) && isLessThan(13, element.value)) {
		element.className += ' border-danger';
		validationMessage = validationMsg != "" ? validationMsg : locationHelper.translate('PLEASE_ENTER_A_VALID_PHONE_NUMBER', lang);
		return { valid: false, validationMessage: validationMessage };
	}
	if ((element.className.indexOf('min10') != -1) && isLessThan(10, element.value)) {
		element.className += ' border-danger';
		validationMessage = validationMsg != "" ? validationMsg : 'Minimum is 10' + inputType;
		return { valid: false, validationMessage: validationMessage };
	}
	if ((element.className.indexOf('max10') != -1) && isMoreThan(10, element.value)) {
		element.className += ' border-danger';
		validationMessage = validationMsg != "" ? validationMsg : 'Maximum is 10' + inputType;
		return { valid: false, validationMessage: validationMessage };
	}
	if ((element.className.indexOf('max20') != -1) && isMoreThan(20, element.value)) {
		element.className += ' border-danger';
		validationMessage = validationMsg != "" ? validationMsg : 'Maximum is 20' + inputType;
		return { valid: false, validationMessage: validationMessage };
	}
	if ((element.className.indexOf('max80') != -1) && isMoreThan(80, element.value)) {
		element.className += ' border-danger';
		validationMessage = validationMsg != "" ? validationMsg : 'Maximum is 80' + inputType;
		return { valid: false, validationMessage: validationMessage };
	}
	if ((element.className.indexOf('max120') != -1) && isMoreThan(120, element.value)) {
		element.className += ' border-danger';
		validationMessage = validationMsg != "" ? validationMsg : 'Maximum is 120' + inputType;
		return { valid: false, validationMessage: validationMessage };
	}
	if ((element.className.indexOf('max200') != -1) && isMoreThan(200, element.value)) {
		element.className += ' border-danger';
		validationMessage = validationMsg != "" ? validationMsg : 'Maximum is 200' + inputType;
		return { valid: false, validationMessage: validationMessage };
	}

	element.className += ' sm-valid';
	return { valid: true, validationMessage: validationMessage };
}


export function scrollToTop() {
	try {
		window.scrollTo({
			top: 0
		});
	} catch(err) {
		if (err instanceof TypeError) {
			window.scroll(0, 0)
		} else {
			throw err
		}
	}
}
export function elementOffset(el) {
	var rect = el.getBoundingClientRect(),
		scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
		scrollTop = window.pageYOffset || document.documentElement.scrollTop;
	return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
}
export function isLiteWeightBooking(service) {

	var liteWeightServices = [
		URLCONSTANT.LOCAL_MOVER_PAGE_URI,
		URLCONSTANT.AC_MAINTENANCE_PAGE_URI,
		URLCONSTANT.PEST_CONTROL_PAGE_URI,
		URLCONSTANT.ELECTRICIAN_PAGE_URI,
		URLCONSTANT.MATTRESS_CLEANING_PAGE_URI,
		URLCONSTANT.PLUMBING_PAGE_URI,
		URLCONSTANT.CARPET_CLEANING_PAGE_URI,
		URLCONSTANT.SOFA_AND_UPHOLSTERY_CLEANING_PAGE_URI,
		URLCONSTANT.WATER_TANK_CLEANING_PAGE_URI,
		URLCONSTANT.DEEP_CLEANING_PAGE_URI,
		URLCONSTANT.MAINTENANCE_PAGE_URI,
		URLCONSTANT.NOON_TV_INSTALLATION_PAGE_URI,
		URLCONSTANT.LAUNDRY_DRY_CLEANING,
	];

	return liteWeightServices.includes(service);

}
export function isCoreBooking(service){
    var coreBookingServices = [
        URLCONSTANT.CLEANING_MAID_PAGE_URI,
        URLCONSTANT.WINDOW_CLEANING_PAGE_URI,
        URLCONSTANT.POOL_CLEANING_PAGE_URI,
		URLCONSTANT.PAINTERS_PAGE_URI,
		URLCONSTANT.PEST_CONTROL_PAGE_URI
    ];
	return coreBookingServices.includes(service);
}
/*export function  isMobile() {
	try{ document.createEvent("TouchEvent"); return true; }
	catch(e){ return false; }
};*/
export function isMobile() {
	var check = false;
	if (typeof navigator != "undefined" && typeof window != "undefined") {
		(function (a, b) { if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true; })(navigator.userAgent || navigator.vendor || window.opera);
	}
	return check;
}
export function swapArrayElements(a, x, y) {
	if (a.length === 1) return a;
	a.splice(y, 1, a.splice(x, 1, a[y])[0]);
	return a;
};
export function zendeskChatBox(lang = DEFAULT_LANG) {
	if(lang == DEFAULT_LANG){
		/*<![CDATA[*/
			
		window.zEmbed || function (e, t) {
			var n, o, d, i, s, a = [], r = document.createElement("iframe");
			window.zEmbed = function () {
				a.push(arguments)
			}, window.zE = window.zE || window.zEmbed, r.src = "javascript:false", r.title = "", r.role = "presentation", (r.frameElement || r).style.cssText = "display: none", d = document.getElementsByTagName("script"), d = d[d.length - 1], d.parentNode.insertBefore(r, d), i = r.contentWindow, s = i.document;
			try {
				o = s
			} catch (c) {
				n = document.domain, r.src = 'javascript:var d=document.open();d.domain="' + n + '";void(0);', o = s
			}
			o.open()._l = function () {
				var o = this.createElement("script");
				n && (this.domain = n), o.id = "js-iframe-async", o.src = e, this.t = +new Date, this.zendeskHost = t, this.zEQueue = a, this.body.appendChild(o)
			}, o.write('<body onload="document._l();">'), o.close()
		}("https://assets.zendesk.com/embeddable_framework/main.js", "servicemarket.zendesk.com");
		/*]]>*/
		if (window && window.zEmbed !== undefined) {
			zEmbed(function() {
				zEmbed.setLocale(lang);
				zEmbed.show();
			})
		}
	}
}
export function hideZendeskChatBox() {
	if (window && window.zEmbed !== undefined) {
		zEmbed(function() {
			zEmbed.hide();
		})
	}
}
export function formTitle(serviceUrl, lang = DEFAULT_LANG) {
	var notificationMessages = {};
	notificationMessages[URLCONSTANT.LOCAL_MOVER_PAGE_URI] = locationHelper.translate("FORM_TITLE_LOCAL_MOVE", lang);
	notificationMessages[URLCONSTANT.INTERNATIONAL_MOVER_PAGE_URI] = locationHelper.translate("FORM_TITLE_INTL_MOVER_PAGE", lang);
	notificationMessages[URLCONSTANT.STORAGE_COMPANIES_PAGE_URI] = locationHelper.translate("FORM_TITLE_STORAGE_COMPANIES_PAGE", lang);
	notificationMessages[URLCONSTANT.CAR_SHIPPING_PAGE_URI] = locationHelper.translate("FORM_TITLE_CAR_SHIPPING_PAGE", lang);
	notificationMessages[URLCONSTANT.CLEANING_MAID_PAGE_URI] = locationHelper.translate("FORM_TITLE_CLEANING_MAID_PAGE", lang);
	notificationMessages[URLCONSTANT.MAINTENANCE_PAGE_URI] = locationHelper.translate("FORM_TITLE_MAINTENANCE_PAGE", lang);
	notificationMessages[URLCONSTANT.PEST_CONTROL_PAGE_URI] = locationHelper.translate("FORM_TITLE_PEST_CONTROL_PAGE", lang);
	notificationMessages[URLCONSTANT.PAINTERS_PAGE_URI] = locationHelper.translate("FORM_TITLE_PAINTERS_PAGE", lang);
	notificationMessages[URLCONSTANT.GARDENING_PAGE_URI] = locationHelper.translate("FORM_TITLE_GARDENING_PAGE", lang);
	notificationMessages[URLCONSTANT.HOME_INSURANCE_PAGE_URI] = locationHelper.translate("FORM_TITLE_HOME_INSURANCE_PAGE", lang);
	notificationMessages[URLCONSTANT.CAR_INSURANCE_PAGE_URI] = locationHelper.translate("FORM_TITLE_CAR_INSURANCE_PAGE", lang);
	notificationMessages[URLCONSTANT.CLEANING_MAID_PAGE_URI] = locationHelper.translate("FORM_TITLE_CLEANING_MAID_PAGE", lang);
	return typeof notificationMessages[serviceUrl] != "undefined" ? notificationMessages[serviceUrl] : "";
}
export function updateLangParams(url, lang){
	if(DEFAULT_LANG != lang){
		url +="&lang="+lang
	}
	return url;
}
export function getLogo(lang = DEFAULT_LANG){
	var logoFolder = DEFAULT_LANG != lang ? lang+"/" : "";
	return "/dist/images/"+logoFolder+"servicemarket-main-logo.svg";
}
export const arabicAllowedCities = [
	DAMMAM,
	RIYADH,
	JEDDAH,
];
export const arabicAllowedPages = [
		URLCONSTANT.LOCAL_MOVER_PAGE_URI, 
		URLCONSTANT.INTERNATIONAL_MOVER_PAGE_URI,
		URLCONSTANT.PAINTERS_PAGE_URI,
		URLCONSTANT.CLEANING_MAID_PAGE_URI,
		URLCONSTANT.MAINTENANCE_PAGE_URI,
		URLCONSTANT.AC_MAINTENANCE_PAGE_URI,
		URLCONSTANT.PEST_CONTROL_PAGE_URI,
		URLCONSTANT.DEEP_CLEANING_PAGE_URI,
		URLCONSTANT.CARPET_CLEANING_PAGE_URI,
		URLCONSTANT.SOFA_AND_UPHOLSTERY_CLEANING_PAGE_URI,
		URLCONSTANT.MATTRESS_CLEANING_PAGE_URI,
		URLCONSTANT.WINDOW_CLEANING_PAGE_URI,
		URLCONSTANT.OFFICE_CLEANING_PAGE_URI,
		URLCONSTANT.FULL_TIME_MAIDS_PAGE_URI,
		'about-us',
		'become-a-partner', 
		'write-a-review', 
		'careers', 
		'faqs', 
		'contact-us', 
		'privacy-policy', 
		'disclaimer', 
		'terms-and-conditions', 
		'payment-and-refund-policy', 
		'profile',
		DAMMAM,
		JEDDAH,
		RIYADH
];
export function showLangNav(city, urlArugs){
	let arabicNotAllowedPages = ['reviews','service-provider'];
	let isarabicNotAllowedPages = false;
	if(typeof urlArugs != "undefined"){
		isarabicNotAllowedPages = arabicNotAllowedPages.filter(function (elem) {
			return (urlArugs.indexOf(elem) > -1);
		}).length;
	}
	//console.log("isarabicNotAllowedPages", isarabicNotAllowedPages);
	return ( !isarabicNotAllowedPages && arabicAllowedCities.includes(city) ) ? true : false;
}
function encryptBody(data) {
	return data;
}
export function processStaticData(action){
	if(typeof action.payload[0] != "undefined"){
		return {acf:action.payload[0].acf}
	}else{
		return action.payload
	}
}
export function removeAreas(action){
	let cities = action.payload;
	//console.log("cities", cities);
	if(cities.length){
		cities.map((item) => { delete item.cityDto["areas"]; return item;})
	}
	return cities;
}