import React from "react";
import Loader from "../components/Loader";
import CompanyItem from "../components/CompanyItem";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { fetchProviderByURL, fetchSimilarPartners, getCityCode, showLoader, hideLoader, setBodyClass, URLCONSTANT } from "../actions/index";
import ProviderBanner from "../components/ProviderBanner";
import SimilarPartners from '../components/SimilarPartners';
import SectionTitle from "../components/SectionTitle";
import LazyLoad from 'react-lazyload';
import PlaceholderComponent from '../components/Placeholder';
import StarRating from "../components/StarRating";
import { fetchProvider, isValidSection, scrollToTop } from "../actions";
import moment from "moment";
import commonHelper from "../helpers/commonHelper";
import { withCookies } from "react-cookie";
import { Link } from "react-router-dom";
import Lightbox from "react-images"
import GallerySlider from "../components/GallerySlider";
import DefaultBanner from "../components/DefaultBanner";


var parentsServices = [];
var nestedServices = [];
class CompanyPage extends React.Component {
    _isMounted = false;
    constructor(props) {
        super(props);

        this.state = {
            classifications: [],
            filter: null,
            provider: null,
            nestedServices: null,
            showPhoneNo: false,
            selectedURLSlug: '',
            companyData: {},
            showFullText: false,
            similarPartners: [],
        }

        this.togglePhoneNo = this.togglePhoneNo.bind(this);
        this.updateMetaData = this.updateMetaData.bind(this);
        this.showFullText = this.showFullText.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.saveCookies = this.saveCookies.bind(this);
        this.moveNext = this.moveNext.bind(this);
        this.getSimilarPartners = this.getSimilarPartners.bind(this);
    }

    componentWillUnmount() {
        this._isMounted = false;
    }
    componentDidMount() {
        this._isMounted = true;
        this.props.setBodyClass('companies');
        this.props.showLoader();
        parentsServices = [];
        let partnerId = '';


        this.props.fetchProviderByURL(this.props.match.params.provider).then((res) => {
            if (this._isMounted) {
                partnerId = res && res.id ? res.id : '';
                nestedServices = res.offeredServices.reduce((accumulator, pilot) => {
                    if (pilot.parentServiceId != null) {
                        if (!accumulator[pilot.parentServiceId]) {
                            accumulator[pilot.parentServiceId] = [];
                        }
                        if (pilot.serviceStatus == 1) {
                            accumulator[pilot.parentServiceId].push({
                                id: pilot.serviceId,
                                value: pilot,
                                label: pilot.serviceLabel
                            });
                        }
                    } else {
                        parentsServices.push({ id: pilot.serviceId, value: pilot, label: pilot.serviceLabel });
                    }
                    return accumulator;
                }, []);
                this.getSimilarPartners(partnerId);
                var companyData = { id: res.id, name: res.name };

                this.setState({
                    provider: res,
                    companyData: companyData,
                    selectedService: parentsServices[0],

                });

                this.props.hideLoader();
                this.updateMetaData();
            }
        });
    }
    changeCompany = (partner) => {

        this.props.setBodyClass('companies');
        this.props.showLoader();
        parentsServices = [];
        let partnerId = '';

        this.props.showLoader();
        this.props.fetchProviderByURL(partner).then((res) => {
            if (this._isMounted) {
                partnerId = res && res.id ? res.id : '';
                nestedServices = res.offeredServices.reduce((accumulator, pilot) => {
                    if (pilot.parentServiceId != null) {
                        if (!accumulator[pilot.parentServiceId]) {
                            accumulator[pilot.parentServiceId] = [];
                        }
                        if (pilot.serviceStatus == 1) {
                            accumulator[pilot.parentServiceId].push({
                                id: pilot.serviceId,
                                value: pilot,
                                label: pilot.serviceLabel
                            });
                        }
                    } else {
                        parentsServices.push({ id: pilot.serviceId, value: pilot, label: pilot.serviceLabel });
                    }
                    return accumulator;
                }, []);
                this.getSimilarPartners(partnerId);
                let companyData = { id: res.id, name: res.name };

                this.setState({
                    provider: res,
                    companyData: companyData,
                    selectedService: parentsServices[0],

                });

                this.props.hideLoader();
                this.updateMetaData();
            }
        });
    }

    getSimilarPartners = (Id) => {
        let similarCompanies = [];
        if (Id != '') {
            fetchSimilarPartners(Id, (response) => {

                if (response && response.data) {
                    this.setState({ similarPartners: response.data })
                }
            },
                (err) => { console.log(err) });
        }
    }
    updateMetaData() {

        var offeredServices = '';
        if (this.state.provider && this.state.provider.offeredServices.length) {
            this.state.provider.offeredServices.map((item) => {
                if (item != null) {
                    offeredServices = offeredServices + (item.serviceName) + ',';
                }
            })
        }
        document.title = this.state.companyData.name + ' - ' + this.state.provider.headOffice.addressResponse.city + " | ServiceMarket";
        var allMetaElements = document.getElementsByTagName('meta');
        for (var i = 0; i < allMetaElements.length; i++) {
            if (allMetaElements[i].getAttribute("name") == "description") {
                allMetaElements[i].setAttribute('content', this.state.provider.numberOfReviews + ' reviews for ' + this.state.companyData.name + ' - Read reviews and get quotes for ' + offeredServices + ' from ' + this.state.companyData.name + ' on ServiceMarket');
                break;
            }
        }
        this.props.history.push('/service-provider/' + this.state.provider.url);
    }
    showFullText() {
        this.setState({
            showFullText: !this.state.showFullText
        });
    }
    handleInputChange(name, value) {
        this.setState({
            [name]: value
        });
        if (name == 'selectedService') {
            this.setState({
                selectedURLSlug: (commonHelper.getServiceSlugByParentID(URLCONSTANT, this.props.service_constants, value.id))
            });
        }
    }
    reviewReplies(replies = []) {
        var replyData = []
        if (replies.length) {
            replies.map((item) => {
                replyData.push(
                    <div className="col ml-3 container-reply container-reply">
                        <div className="review-details">
                            <strong>{item.nameOfPerson}</strong>
                            <p className="m-0">{moment(item.dateOfReply).format("DD/MM/YYYY")}</p>
                            <div className="py-2 reply-text">{item.reply}</div>
                        </div>
                    </div>
                );
            })
        }
        return replyData;
    }
    moveNext() {
        if (isValidSection('company-form')) {
            this.saveCookies('quotes_data', { 'selected_companies': [this.state.companyData], 'selectedService': this.state.selectedService, 'selectedSubService': this.state.selectedSubService });
            // TODO the city might need to be updated here
            this.props.history.push("/en/" + this.props.currentCity + "/" + this.state.selectedURLSlug + '/journey3/step2');
        } else {
            scrollToTop();
        }
    }
    saveCookies(name, data) {
        var currentLanguage = "en";
        var cookieName = currentLanguage + "." + this.props.currentCity + "." + this.state.selectedURLSlug + "." + name;
        const { cookies } = this.props;
        cookies.set(cookieName, JSON.stringify(data), { path: '/' });
    }
    togglePhoneNo() {
        this.setState({
            showPhoneNo: true
        })
    }
    // getCookieSelectedCompany(){
    //
    //     var currentLanguage = "en";
    //     var cookieName = currentLanguage + "." + this.props.currentCity + "." + this.state.selectedURLSlug + ".quotes_data";
    //     const { cookies } = this.props;
    //     return cookies.get(cookieName) || {};
    // }
    render() {

        const { loader } = this.props;
        const { selectedService } = this.state;
        let provider = this.state.provider;
        //console.log("getHomeCarInsuranceLink", this.state.selectedService);

        if (provider && !loader) {
            const { provider, showPhoneNo } = this.state;
            const { canSelect } = this.props;
            var showPhoneClass = "text-black border btn mr-3 text-capitalize show-phone-number telephone mrm phone-number";
            var toggleClass = "text-black border btn mr-3 show-phone-number telephone d-none mrm";

            var portfolioImages = (typeof provider.portfolioImages != "undefined" && provider.portfolioImages.length) ? provider.portfolioImages : [];
            var newPortfolioImages = [];
            if (portfolioImages.length) {
                portfolioImages.map((item) => {
                    newPortfolioImages.push(
                        { src: item }
                    )
                })
            }
            if (showPhoneNo) {
                showPhoneClass += " d-none";
                toggleClass += " d-inline-block";
            } else {
                showPhoneClass += " d-inline-block";
                toggleClass += " d-none";
            }
            const rate = !isNaN(provider.averageRating) ? Number((provider.averageRating).toFixed(2)) : provider.averageRating;
            var offeredServices = '';
            if (provider.offeredServices.length) {
                provider.offeredServices.map((item) => {
                    offeredServices = offeredServices + (item.serviceName) + ', ';
                })
            }

            return (
                <main id="company-form" >
                    {/* TODO remove the static background in ProviderBanner */}
                    <ProviderBanner
                        nestedServices={nestedServices}
                        parentsServices={parentsServices}
                        title={"Get a quote from " + provider.name + ". Start by selecting the service you need."}
                        background={"https://blog.servicemarket.com/wp-content/uploads/2018/07/local-move.jpg"}
                        handleInputChange={this.handleInputChange}
                    />

                    <div className="container pt-3">
                        <section className={"clear mb-5 mt-3 d-none d-md-block"}>
                            <div className={"col75"}>
                                <div className="row mb-5">
                                    <div className="border">
                                        <LazyLoad offset={500} placeholder={<PlaceholderComponent />}>
                                            <div className="provider-img">
                                                <img src={provider.logo} alt={provider.name} className="service-logo" />
                                            </div>
                                        </LazyLoad>
                                    </div>
                                    <div className="col">
                                        <div className="row">
                                            <div className="col">
                                                <p className="h2">{provider.name}</p>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-auto text-left">
                                                <StarRating rating={rate} />
                                            </div>
                                            <div className="col-auto px-0 border-right"></div>
                                            <div className="col-auto text-left">
                                                <p className="h3 m-0 text-primary">{(rate && rate != 'NaN') ? (rate + '/5') : 'No rating available'}</p>
                                            </div>
                                            <p className="m-0">
                                                {provider.numberOfReviews} Reviews
                                        </p>
                                            <div className="col-auto text-left">
                                                {(typeof provider.providerClass != "undefined" && provider.providerClass != null) && (<span className="pts">
                                                    <i className="fa fa-shield"></i>
                                                    <strong className="text-capitalize d-inline-block ml-1">
                                                        {provider.providerClass}
                                                    </strong>
                                                </span>)
                                                }
                                            </div>

                                        </div>
                                        {provider.contactInformation != null && (<div
                                            className="row mt-3 contact-phone-website d-none d-md-block d-sm-block d-lg-block">
                                            <div className="col-lg-12 col-md-12 hidden-xs">
                                                <a href="javascript:void(0)" className={showPhoneClass} id="show-phone"
                                                    onClick={() => {
                                                        this.togglePhoneNo()
                                                    }}>show phone number</a>
                                                <a className={toggleClass} id="phone-number"
                                                    href={"tel:" + provider.contactInformation.phone}>
                                                    <span className="field-descp telephone-number"
                                                        itemProp="telephone">{provider.contactInformation.phone}</span>
                                                </a>
                                                <a target="_blank" href={"http://"+provider.website} className="text-black website-url border d-inline-block btn text-capitalize d-none">
                                                    visit website
                                                </a>
                                            </div>
                                        </div>)
                                        }

                                    </div>
                                </div>
                                {provider.contactInformation != null && (
                                    <div className="row my-3 contact-phone-website d-md-none d-sm-none d-lg-none">
                                        <div className="col-6 text-center">
                                            <a href="javascript:void(0)" className={showPhoneClass} id="show-phone"
                                                onClick={() => {
                                                    this.togglePhoneNo()
                                                }}>show phone number</a>
                                            <a className={toggleClass} id="phone-number"
                                                href={"tel:" + provider.contactInformation.phone}>
                                                <span className="field-descp telephone-number"
                                                    itemProp="telephone">{provider.contactInformation.phone}</span>
                                            </a>
                                        </div>
                                        <div className="col-6 text-center d-none">
                                            <a target="_blank" href={"http://"+provider.website}
                                                className="d-block text-black website-url border p-2 text-capitalize">
                                                visit website
                                        </a>
                                        </div>
                                    </div>)
                                }
                                <div className="row align-items-center mb-4">
                                    <div className="col pad-md-no">
                                        <div className="d-inline-block">
                                            <i className="fa fa-building"></i>
                                            <span className="d-inline-block font-weight-bold mx-2">Established </span>
                                            {provider.establishedIn}
                                        </div>
                                        {provider.contactInformation ? (
                                            <div className="d-block mt-3">
                                                <i className="fa fa-user"></i>
                                                <span className="d-inline-block font-weight-bold mx-2">Contact Person </span>
                                                {provider.contactInformation ? provider.contactInformation.name : ''}
                                            </div>
                                        ) : ''}
                                        <div className="d-block my-3">
                                            <i className="fa fa-map-marker"></i>
                                            <span className="d-inline-block font-weight-bold mx-2">Location </span>
                                            {provider.headOffice.addressResponse.line1 + " " + provider.headOffice.addressResponse.city + " " + provider.headOffice.addressResponse.country}
                                        </div>
                                        <div className="row align-items-center mb-3 d-none d-md-block">
                                            {
                                                offeredServices.length > 200 ? (
                                                    <div className={"col " + (this.state.showFullText ? "half-text" : "full-text")}><span
                                                        className={"font-weight-bold "}>Services Offered : </span>
                                                        <span className={'text-section full '}>
                                                            {offeredServices.substring(0, offeredServices.length / 2)}
                                                            <span className={"btn-span"} onClick={this.showFullText} >see more</span>
                                                        </span>
                                                        <span className={'text-section half '}>{offeredServices}</span>
                                                    </div>
                                                ) : (
                                                        <div className={"col "}>
                                                            <span className="text-section">
                                                                {offeredServices}
                                                            </span>
                                                        </div>
                                                    )
                                            }

                                        </div>
                                        {provider.accreditation.length ? (
                                            <div className="row align-items-center mb-1 d-none d-md-block">
                                                <div className="col"><span
                                                    className="font-weight-bold">Accreditations : </span>{provider.accreditation[0]["accreditation"]}
                                                </div>
                                            </div>) : ''}
                                    </div>
                                    {/* <div className='col pad-md-no'>
                                    <SimilarPartners partners={this.state.similarPartners} />
                                </div> */}
                                </div>
                                {(typeof newPortfolioImages != "undefined" && newPortfolioImages.length) ?
                                    (<div className="row align-items-center mb-4">
                                        <div className="col-12 mb-2 pad-md-no">
                                            <span className="d-inline-block font-weight-bold">Photo / Gallery </span>
                                        </div>
                                        <div className="col-12">
                                            <GallerySlider images={newPortfolioImages}
                                                showThumbnails={true}
                                            />
                                        </div>
                                    </div>) : ""
                                }
                                <div className="row">
                                    <div className="col pad-md-no">
                                        <span className="h3 text-primary">Reviews ({provider.numberOfReviews})</span>
                                        <Link to={'/en/write-a-review/' + provider.url}><span className="btn right btn-link-warning"><i className="fa fa-plus-circle pr-2"></i>Add a review</span></Link>
                                        <hr />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col mb-5">
                                        {provider.customerReviews.map((review, index) => {
                                            if (review.approvedReview) {
                                                return (
                                                    <div key={'review' + index}>
                                                        <div className="review">
                                                            <div className="pl-md-0 pl-3 row">
                                                                <div
                                                                    className="col-7 col-md pl-0 pl-md-3 flex-column justify-content-between">
                                                                    <p className="h4">{review.reviewerName}</p>
                                                                    <div className="row col">
                                                                        <StarRating rating={review.rating} />
                                                                        <span className={"lh-1"}>
                                                                            {(review.rating && review.rating != 'NaN') ? (review.rating + '/5') : 'No rating available'}
                                                                        </span>
                                                                    </div>

                                                                </div>
                                                                <div
                                                                    className="col-5 col-md d-flex flex-column justify-content-center">
                                                                    <p className="h4 m-0">{review.serviceName}</p>
                                                                    <p className="m-0">{moment(review.creationDate).format("DD/MM/YYYY")}</p>
                                                                </div>
                                                            </div>
                                                            <div className="row">
                                                                <div className="col">
                                                                    <p className="h4  mt-3 font-weight-bold">{'"' + review.approvedTitle + '"'}</p>
                                                                    {review.approvedReview ? (<div
                                                                        className="m-0">{review.approvedReview}</div>) : ''}
                                                                </div>
                                                            </div>
                                                        </div>
                                                        { review.reviewRepliesDtos.length !=0 && <div className="row reply-container">{ this.reviewReplies(review.reviewRepliesDtos) }</div> }
                                                        <hr />
                                                    </div>
                                                )
                                            }

                                        })}
                                    </div>
                                </div>
                            </div>
                            <div className={"col-similar-partner d-none d-md-block "}>
                                <div className='col pad-md-no'>
                                    <SimilarPartners partners={this.state.similarPartners} changeCompany={this.changeCompany} />
                                </div>
                            </div>
                        </section>
                        <section className={"clear mb-5 mt-3 d-md-none"}>
                            <div className="row mb-5">
                                <div className="border">
                                    <LazyLoad offset={500} placeholder={<PlaceholderComponent />}>
                                        <div className="provider-img">
                                            <img src={provider.logo} alt={provider.name} className="service-logo" />
                                        </div>
                                    </LazyLoad>
                                </div>
                                <div className="col">
                                    <div className="row">
                                        <div className="col">
                                            <p className="h2">{provider.name}</p>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-auto text-left">
                                            <StarRating rating={rate} />
                                        </div>
                                        <div className="col-auto px-0 border-right"></div>
                                        <div className="col-auto text-left">
                                            <p className="h3 m-0 text-primary">{(rate && rate != 'NaN') ? (rate + '/5') : 'No rating available'}</p>
                                        </div>
                                        <p className="m-0">
                                            {provider.numberOfReviews} Reviews
                                        </p>
                                        <div className="col-auto text-left">
                                            {(typeof provider.providerClass != "undefined" && provider.providerClass != null) && (<span className="pts">
                                                <i className="fa fa-shield"></i>
                                                <strong className="text-capitalize d-inline-block ml-1">
                                                    {provider.providerClass}
                                                </strong>
                                            </span>)
                                            }
                                        </div>

                                    </div>
                                    {provider.contactInformation != null && (<div
                                        className="row mt-3 contact-phone-website d-none d-md-block d-sm-block d-lg-block">
                                        <div className="col-lg-12 col-md-12 hidden-xs">
                                            <a href="javascript:void(0)" className={showPhoneClass} id="show-phone"
                                                onClick={() => {
                                                    this.togglePhoneNo()
                                                }}>show phone number</a>
                                            <a className={toggleClass} id="phone-number"
                                                href={"tel:" + provider.contactInformation.phone}>
                                                <span className="field-descp telephone-number"
                                                    itemProp="telephone">{provider.contactInformation.phone}</span>
                                            </a>
                                            <a target="_blank" href={"http://"+provider.website}
                                                className="text-black website-url border d-inline-block btn text-capitalize d-none">
                                                visit website
                                            </a>
                                        </div>
                                    </div>)
                                    }

                                </div>
                            </div>
                            {provider.contactInformation != null && (
                                <div className="row my-3 contact-phone-website d-md-none d-sm-none d-lg-none">
                                    <div className="col-6 text-center">
                                        <a href="javascript:void(0)" className={showPhoneClass} id="show-phone"
                                            onClick={() => {
                                                this.togglePhoneNo()
                                            }}>show phone number</a>
                                        <a className={toggleClass} id="phone-number"
                                            href={"tel:" + provider.contactInformation.phone}>
                                            <span className="field-descp telephone-number"
                                                itemProp="telephone">{provider.contactInformation.phone}</span>
                                        </a>
                                    </div>
                                    <div className="col-6 text-center d-none">
                                        <a target="_blank" href={"http://"+provider.website}
                                            className="d-block text-black website-url border p-2 text-capitalize">
                                            visit website
                                        </a>
                                    </div>
                                </div>)
                            }
                            <div className="row align-items-center mb-4">
                                <div className="col pad-md-no">
                                    <div className="d-inline-block">
                                        <i className="fa fa-building"></i>
                                        <span className="d-inline-block font-weight-bold mx-2">Established </span>
                                        {provider.establishedIn}
                                    </div>
                                    {provider.contactInformation ? (
                                        <div className="d-block mt-3">
                                            <i className="fa fa-user"></i>
                                            <span className="d-inline-block font-weight-bold mx-2">Contact Person </span>
                                            {provider.contactInformation ? provider.contactInformation.name : ''}
                                        </div>
                                    ) : ''}
                                    <div className="d-block my-3">
                                        <i className="fa fa-map-marker"></i>
                                        <span className="d-inline-block font-weight-bold mx-2">Location </span>
                                        {provider.headOffice.addressResponse.line1 + " " + provider.headOffice.addressResponse.city + " " + provider.headOffice.addressResponse.country}
                                    </div>
                                    <div className="row align-items-center mb-3 d-none d-md-block">
                                        {
                                            offeredServices.length > 200 ? (
                                                <div className={"col " + (this.state.showFullText ? "half-text" : "full-text")}><span
                                                    className={"font-weight-bold "}>Services Offered : </span>
                                                    <span className={'text-section full '}>
                                                        {offeredServices.substring(0, offeredServices.length / 2)}
                                                        <span className={"btn-span"} onClick={this.showFullText} >see more</span>
                                                    </span>
                                                    <span className={'text-section half '}>{offeredServices}</span>
                                                </div>
                                            ) : (
                                                    <div className={"col "}>
                                                        <span className="text-section">
                                                            {offeredServices}
                                                        </span>
                                                    </div>
                                                )
                                        }

                                    </div>
                                    {provider.accreditation.length ? (
                                        <div className="row align-items-center mb-1 d-none d-md-block">
                                            <div className="col"><span
                                                className="font-weight-bold">Accreditations : </span>{provider.accreditation[0]["accreditation"]}
                                            </div>
                                        </div>) : ''}
                                </div>
                                {/* <div className='col pad-md-no'>
                                    <SimilarPartners partners={this.state.similarPartners} />
                                </div> */}
                            </div>
                            {(typeof newPortfolioImages != "undefined" && newPortfolioImages.length) ?
                                (<div className="row align-items-center mb-4">
                                    <div className="col-12 mb-2 pad-md-no">
                                        <span className="d-inline-block font-weight-bold">Photo / Gallery </span>
                                    </div>
                                    <div className="col-12">
                                        <GallerySlider images={newPortfolioImages}
                                            showThumbnails={true}
                                        />
                                    </div>
                                </div>) : ""
                            }
                            <div className="row">
                                <div className="col pad-md-no">
                                    <span className="h3 text-primary">Reviews ({provider.numberOfReviews})</span>
                                    <Link to={'/en/write-a-review/' + provider.url}><span className="btn right btn-link-warning"><i className="fa fa-plus-circle pr-2"></i>Add a review</span></Link>
                                    <hr />
                                </div>
                            </div>
                            <div className="row">
                                <div className="col mb-5">
                                    {provider.customerReviews.map((review, index) => {
                                        if (review.approvedReview) {
                                            return (
                                                <div key={'review' + index}>
                                                    <div className="review">
                                                        <div className="pl-md-0 pl-3 row">
                                                            <div
                                                                className="col-7 col-md pl-0 pl-md-3 flex-column justify-content-between">
                                                                <p className="h4">{review.reviewerName}</p>
                                                                <div className="row col">
                                                                    <StarRating rating={review.rating} />
                                                                    <span className={"lh-1"}>
                                                                        {(review.rating && review.rating != 'NaN') ? (review.rating + '/5') : 'No rating available'}
                                                                    </span>
                                                                </div>

                                                            </div>
                                                            <div
                                                                className="col-5 col-md d-flex flex-column justify-content-center">
                                                                <p className="h4 m-0">{review.serviceName}</p>
                                                                <p className="m-0">{moment(review.creationDate).format("DD/MM/YYYY")}</p>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col">
                                                                <p className="h4  mt-3 font-weight-bold">{'"' + review.approvedTitle + '"'}</p>
                                                                {review.approvedReview ? (<div
                                                                    className="m-0">{review.approvedReview}</div>) : ''}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr />
                                                </div>
                                            )
                                        }

                                    })}
                                </div>
                            </div>

                        </section>
                        { typeof this.state.selectedService != "undefined" ? (
                        <section>
                            <footer className={"footer-buttons fixed-bottom container-fluid bg-white shadow-1"}>
                                <div className="row btn-fixed">
                                    <div className="col">
                                        <div className="row">
                                            <div className="col-12 px-0 d-xs-flex text-center">
                                                {
                                                    (selectedService && selectedService.id != this.props.service_constants.SERVICE_HOME_INSURANCE && selectedService.id != this.props.service_constants.SERVICE_CAR_INSURANCE) ? (
                                                        <button className="login-step-button orange-btn-content btn btn-inline-block btn-warning py-2 text-uppercase font-weight-bold" onClick={() => this.moveNext()}>
                                                            <span className="text-uppercase">Get your free quotes</span>
                                                        </button>
                                                    ) : typeof this.state.selectedService != "undefined" ? (
                                                            <a className="login-step-button orange-btn-content btn btn-inline-block btn-warning py-2 text-uppercase font-weight-bold" href={commonHelper.getHomeCarInsuranceLinks(this.state.selectedService.id, this.props.service_constants)}>
                                                                <span className="text-uppercase">Get your free quotes</span>
                                                            </a>
                                                        ) : (<a className="login-step-button orange-btn-content btn btn-inline-block btn-warning py-2 text-uppercase font-weight-bold" href="#">
                                                                <span className="text-uppercase">Get your free quotes</span>
                                                            </a>)
                                                }

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </footer>
                        </section>) : '' }
                    </div>
                </main >
            );
        } else {
            return (
                <main>
                    <DefaultBanner title={"Get a quote from " + this.props.currentProvider.name + ". Start by selecting the service you need."}
                        background={"https://blog.servicemarket.com/wp-content/uploads/2018/07/local-move.jpg"} />
                    <Loader />
                </main>);
        }
    }
}

function mapStateToProps(state) {
    return {
        loader: state.loader,
        cities: state.cities,
        service_constants: state.serviceConstants,
        currentCity: state.currentCity,
        currentProvider: state.currentProvider,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ showLoader, hideLoader, setBodyClass, fetchProviderByURL }, dispatch);
}

export default withCookies(connect(mapStateToProps, mapDispatchToProps)(CompanyPage));
