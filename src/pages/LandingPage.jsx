import React from "react";
import DefaultBanner from "../components/DefaultBanner";
import BookOrQuotes from "../components/BookOrQuotes";
import SectionTitle from "../components/SectionTitle";
import PopularServices from "../components/PopularServices";
import LandingMidContent from "../components/LandingMidContent";
import WhyServiceMarket from "../components/WhyServiceMarket";
import GoogleReviews from "../components/GoogleReviews";
import AllGoogleReviews from "../components/AllGoogleReviews";
import HowItWorks from "../components/HowItWorks";
import Logos from "../components/Logos";
import LatestBlogNews from "../components/LatestBlogNews";
import OurApps from "../components/OurApps";
import Locations from "../components/Locations";
import Accordion from "../components/Accordion";
import Loader from "../components/Loader";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {fetchLandingData, showLoader, hideLoader, whySMData, howIWData, setBodyClass, isSmartBannerActive, URLCONSTANT, fetchAllReviews, cityCodes} from "../actions/index";
//import SmartBanner from 'react-smartbanner';
import commonHelper from "../helpers/commonHelper";
import StickyCTA from "../components/StickyCTA";
import { Sticky, StickyContainer } from 'react-sticky';
import locationHelper from "../helpers/locationHelper";
import { withCookies } from "react-cookie";

class LandingPage extends React.Component{
    _isMounted = false;
    constructor(props) {
        super(props);
        let getParams = props.pageParams;
        //console.log("props.quotesParams", getParams);
        this.state = {
            h1Text : (typeof getParams.h1 != "undefined" && getParams.h1 != "") ? getParams.h1 : "",
            h2Text : (typeof getParams.h2 != "undefined" && getParams.h2 != "") ?  getParams.h2 : "",
            promo: (typeof getParams.promo != "undefined" && getParams.promo != "") ? getParams.promo : "",
        }
        this.updateMetaData = this.updateMetaData.bind(this);
        this.locationHelperInit = locationHelper.init.bind(this);
        this.locationHelperInit();
    }
    componentDidMount(){
        this._isMounted = true;
       //console.log("getParameterByName", commonHelper.getParameterByName("h1"), commonHelper.getParameterByName("h2"));
        if(commonHelper.getParameterByName("h2") != null || commonHelper.getParameterByName("h1") != null || commonHelper.getParameterByName("promo") != null){
            /*this.setState({
                h1Text : commonHelper.getParameterByName("h1") != null ? commonHelper.getParameterByName("h1") : "",
                h2Text : commonHelper.getParameterByName("h2") != null ? commonHelper.getParameterByName("h2") : "",
                promo: commonHelper.getParameterByName("promo") != null ? commonHelper.getParameterByName("promo") : "",
            })*/
            var url = window.location.href;
            var n = url.indexOf('?');
            url = url.substring(0, n != -1 ? n : url.length);
            history.pushState(null, null, url);
        }
        if ( window.REDUX_DATA.landingData.length <= 0 ) {
            this.props.showLoader();
            // Check if the Shared data between pages already requested, if exist we don't need to fetch them again
            const extraData = [];
            if(!this.props.why_sm.acf) extraData.push('whySMData');
            //if(!this.props.how_iw.acf) extraData.push('howIWData');
            if(!this.props.logos.acf) extraData.push('logosData');
            this.props.fetchLandingData(this.props.match.params.city, this.props.match.params.slug, extraData, this.props.lang)
            .then(()=> {
                if (this._isMounted) {
                    this.props.hideLoader();
                    if (this.props.landingData.length == 0) {
                        this.props.history.push('/' + this.props.match.params.lang + "/" + this.props.match.params.city);
                    } else {
                        this.updateMetaData();
                    }
                }
            });
        } else {
            this.updateMetaData();
        }
        this.props.setBodyClass('landing');

    }

    componentDidUpdate(prevProps){
        // console.log("component Did Update");
        const prevCity    = prevProps.match.params.city;
        const currentCity = this.props.match.params.city;
        const prevSlug    = prevProps.match.params.slug;
        const currentSlug = this.props.match.params.slug;
        if( (prevCity != currentCity) || (prevSlug != currentSlug) ){
            this.props.showLoader();
            // Check if the Shared data between pages already requested, if exist we don't need to fetch them again
            const extraData = [];
            if(!this.props.why_sm.acf) extraData.push('whySMData');
            if(!this.props.how_iw.acf) extraData.push('howIWData');
            if(!this.props.logos.acf) extraData.push('logosData');
            this.props.fetchLandingData(this.props.match.params.city, this.props.match.params.slug, extraData, this.props.lang)
            .then(()=> {
                console.log("component Did Update");
                let city = this.props.match.params.city;
                let  {landingData, service_constants, lang} =  this.props;
                let landingPageData = ( typeof landingData != "undefined" && landingData.length ) ? landingData : [];
                if(landingPageData.length){
                    landingPageData = landingPageData[0];
                    let serviceConstants = service_constants;
                    let serviceID = typeof serviceConstants[landingPageData.acf.id] != "undefined" ? serviceConstants[landingPageData.acf.id] : '';
                    this.props.fetchAllReviews(cityCodes[city],serviceID, lang).then(()=> {
                        this.props.hideLoader();
                        this.updateMetaData();
                    })
                }else{
                    this.props.history.push('/'+this.props.match.params.lang+"/"+currentCity);
                }
            });
        }

    }
    componentWillUnmount(){
        window.REDUX_DATA.landingData = [];
        this.props.setBodyClass('default');
        this._isMounted = false;
    }
    updateMetaData() {
        // console.log('updating meta data');
        document.title = this.props.landingData[0].acf.meta_title;
        var allMetaElements = document.getElementsByTagName('meta');
        //loop through and find the element you want
        for (var i=0; i<allMetaElements.length; i++) {
            if (allMetaElements[i].getAttribute("name") == "description") {
                //make necessary changes
                allMetaElements[i].setAttribute('content', this.props.landingData[0].acf.meta_description);
                //no need to continue loop after making changes.
                break;
            }
        }
    }
    render() {
        const {why_sm, how_iw, landingData, currentCity, logos, lang} = this.props;
        const imageBlurBlend = true;
        let isSmartBannerShow = isSmartBannerActive(this.props.match.params.slug);
        // App Install Popup values
        const daysHidden = 1;
        const daysReminder = 1;
        const author = 'Book or get quotes through the app!';
        const appTitle='ServiceMarket';
        const appViewLabel = 'Install';
        const appPrice = { ios: 'FREE', android: 'FREE' };
        const storeText = {ios: 'On the App Store', android: 'In Google Play'};
        const appURL = {ios: 'https://servicemarket.onelink.me/R9tZ/47e24049', android: 'https://servicemarket.onelink.me/R9tZ/2bf82707'};

    	if( landingData.length && !this.props.loader ){
            var data = landingData[0];
            
            let links = [];

            let serviceId = data.acf.id;

            let leftBoxTitle = data.acf.left_box_title;

            let leftBoxDesc = data.acf.left_box_description;
            let rightBoxTitle = data.acf.right_box_title;
            let rightBoxDesc = data.acf.right_box_description;
            
            let bookNowJourney = data.acf.book_now_journey;

            let getQuoteJourney = data.acf.get_quote_journey;

            var isExternalLink = (serviceId === 'SERVICE_HOME_INSURANCE' || serviceId === 'SERVICE_CAR_INSURANCE') ? true : false;

            if(leftBoxTitle.length && bookNowJourney != '-- Select if any --'){
                let {promo} = this.state;
                let boxLink = "/"+lang+"/"+currentCity+"/"+bookNowJourney+"/book-online";
                boxLink += promo != "" ? "?promo="+promo : "";
                links.push({
                    title:locationHelper.translate("BOOK_NOW"), 
                    link: boxLink, 
                    isQuote: false, 
                    isBooking: true, 
                    isExternalLink:isExternalLink
                })
            }
            if(rightBoxTitle.length && getQuoteJourney != '-- Select if any --'){
               let boxTitle = (leftBoxTitle.length && bookNowJourney != '-- Select if any --') ? "GET_QUOTES" : "GET_FREE_QUOTES"
                links.push({
                    title: locationHelper.translate(boxTitle), 
                    link: "/"+lang+"/"+currentCity+"/"+getQuoteJourney+"/journey1", 
                    isQuote: true, 
                    isBooking: true, 
                    serviceId : serviceId, 
                    isExternalLink:isExternalLink
                })
            }
            let city = currentCity.charAt(0).toUpperCase() + currentCity.slice(1);
        //return ("Hello"+locationHelper.translate("RELATED_SERVICES"));    
        return (
            <React.Fragment>
                <main loader={this.props.loader ? "show": "hide"}>
                    { /*isSmartBannerShow && (<SmartBanner daysHidden={daysHidden} daysReminder={daysReminder} title={appTitle} button={appViewLabel} price={appPrice} storeText={storeText} url={appURL} author={author} />) */}
                    <DefaultBanner showSecondHeader={!data.acf.left_box_title || !data.acf.left_box_title.length || !data.acf.right_box_title || !data.acf.right_box_title.length} isServiceLanding={true} h1Text = {this.state.h1Text} h2Text = {this.state.h2Text} title={data.acf.banner_title} description={data.acf.banner_description} background={data.acf.banner_image} imageBlurBlend={imageBlurBlend} >
                        <BookOrQuotes 
                            promo={this.state.promo} 
                            serviceId ={data.acf.id} 
                            leftBoxTitle={data.acf.left_box_title} 
                            leftBoxDesc={data.acf.left_box_description} 
                            rightBoxTitle={data.acf.right_box_title} 
                            currentCity={currentCity} 
                            rightBoxDesc={data.acf.right_box_description} 
                            bookNowJourney={data.acf.book_now_journey} 
                            getQuoteJourney={data.acf.get_quote_journey}
                            h1Text = {this.state.h1Text} 
                            h2Text = {this.state.h2Text} 
                            title={data.acf.banner_title} 
                            description={data.acf.banner_description}
                            match = {this.props.match} />
                    </DefaultBanner>
                    <StickyContainer>
                        <section className="py-5 bg-white">
                            <div className="container">
                                <SectionTitle title={locationHelper.translate("RELATED_SERVICES")} subTitle={data.acf.recommended_for_you_static.recommended_main_text}/>
                                {data.acf.recommended_for_you && <PopularServices items={data.acf.recommended_for_you} currentCity={currentCity} />}
                                <SectionTitle title={locationHelper.translate("HOW_IT_WORKS_TITLE")} subTitle={locationHelper.translate("HOW_IT_WORKS_DESC")} parentClass="row mb-4" childClass="col px-4 px-md-3" />
                                {data.acf && <HowItWorks parentClass="row mb-5" childClass="col px-4 px-md-3" items={(data.acf.how_it_works_items && data.acf.how_it_works_items.length) ? data.acf.how_it_works_items : how_iw.acf.how_it_works_items} />}
                                <SectionTitle title={locationHelper.translate("WHY_SERVICEMARKET")} subTitle={locationHelper.translate("WHY_SERVICEMARKET_DESC")} childClass="col px-4 px-md-3" />
                                {why_sm.acf && <WhyServiceMarket parentClass="row mb-5" childClass="col px-4 px-md-3" items={why_sm.acf.why_service_market_items} />}
                                <GoogleReviews  match={this.props.match} getQuoteJourney={data.acf.get_quote_journey} serviceID={data.acf.id} history={this.props.history} parentClass="row mb-3 mb-md-5" childClass="col px-4 px-md-3 reviews" />
                                <AllGoogleReviews product_name={commonHelper.getServiceReviewNameBySlug(URLCONSTANT, this.props.service_constants, this.props.match.params.slug)} parentClass="row mb-5" childClass="col px-4 px-md-3" />
                                <SectionTitle title={locationHelper.translate("AS_FEATURED_IN")} subTitle="" parentClass="row mb-4" childClass="col px-4 px-md-3"/>
                                {logos.acf && <Logos parentClass="row" childClass="col-4 col-md-2 text-center" items={logos.acf.partners} />}
                                <OurApps parentClass="row mb-5" childClass="col px-4 px-md-3 mt-md-5" />
                                <hr className="my-5" />
                                <SectionTitle title={data.acf.what_included_title} subTitle={data.acf.what_included_description} childClass="col px-4 px-md-3" />
                                {data.acf.whats_included_in_service && <LandingMidContent items={data.acf.whats_included_in_service} />}
                                <hr className="mb-5 mt-6" />
                                {data.acf.posts && data.acf.posts.length > 0 && <SectionTitle title={locationHelper.translate("SERVICEMARKET_BLOG")} subTitle={locationHelper.translate("EVERYTHING_YOU_NEED_TO_KNOW_ABOUT_ORGANISING_YOUR_HOME_LIFE_AND_MORE")} parentClass="row mb-4" childClass="col px-4 px-md-3" />}
                                {data.acf.posts && <LatestBlogNews items={data.acf.posts}/>}
                                {data.acf.frequently_asked_questions && <SectionTitle title={locationHelper.translate("FREQUENTLY_ASKED_QUESTIONS")} subTitle={locationHelper.translate("FIND_ANSWERS_QUICKLY_GET_BACK_TO_DOING_WHAT_YOU_LOVE")} parentClass="row mb-4" childClass="col px-4 px-md-3" />}
                                {data.acf.frequently_asked_questions && <Accordion panels={data.acf.frequently_asked_questions} parentClass="row mb-5" childClass="col px-4 px-md-3" />}
                                <SectionTitle title={locationHelper.translate("SERVICEMARKET_LOCATIONS")} subTitle={locationHelper.translate("WE_ARE_CURRENTLY_AVAILABLE_IN")} parentClass="row mb-4" childClass="col px-4 px-md-3" />
                                <Locations />
                            </div>
                        </section>
                        <StickyCTA title = {data.acf.banner_title} btnLinks={links} />
                    </StickyContainer>
                </main>
                
                </React.Fragment>
            ); 
    		}else{
				return (<main loader={this.props.loader ? "show": "hide"}><Loader/></main>);
        }
    }
}


function mapStateToProps(state){
	return {
        landingData: state.landingData,
        loader: state.loader,
        why_sm: state.why_sm,
        how_iw: state.how_iw,
        logos: state.logos,
        currentCity: state.currentCity,
        service_constants: state.serviceConstants,
        lang: state.lang,
        pageParams: state.pageParams
	}
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({fetchLandingData, showLoader, hideLoader, whySMData, howIWData, setBodyClass, fetchAllReviews}, dispatch);
}
export default withCookies(connect(mapStateToProps, mapDispatchToProps)(LandingPage));
