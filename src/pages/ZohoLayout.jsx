import React from "react";
import { Switch, Route, Link } from "react-router-dom";
import Routes from "../Routes";
import { withRouter } from 'react-router';

class ZohoLayout extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
			pathname: ''
        }
    }
	componentDidMount() {
        
		var pathname = window.location.pathname;
		this.setState({
			pathname: pathname
		});
	}
    render() {
        //console.log(Routes);
        return (
			<div>
				<Switch>
                    { Routes.map( route => <Route key={ route.path } { ...route } /> ) }
				</Switch>
			</div>
        );
    }
}

export default withRouter(ZohoLayout);