import React from "react";
import DefaultBanner from "../components/DefaultBanner";
import SectionTitle from "../components/SectionTitle";
import TitleTextBlock from "../components/TitleTextBlock";
import Loader from "../components/Loader";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {fetchTermsData, showLoader, hideLoader, fetchDataDictionaryValues} from "../actions/index";

class TermsPage extends React.Component{
    constructor(props) {
        super(props);
        this.updateMetaData = this.updateMetaData.bind(this);
    }
    createMarkup(htmlString) {
        return {__html: htmlString};
    }
    componentDidMount(){
        if ( !this.props.termsData.acf ) {
            this.props.showLoader();
            this.props.fetchTermsData(this.props.lang)
            .then(()=> {
                this.updateMetaData();
                this.props.hideLoader();
            });
        } else {
            this.updateMetaData();
        }
    }
    updateMetaData() {
        // console.log('updating meta data');
        document.title = this.props.termsData.acf.seo_meta_title ? this.props.termsData.acf.seo_meta_title : 'ServiceMarket';
        var allMetaElements = document.getElementsByTagName('meta');
        for (var i=0; i<allMetaElements.length; i++) {
            if (allMetaElements[i].getAttribute("name") == "description") {
                allMetaElements[i].setAttribute('content', this.props.termsData.acf.seo_meta_description ? this.props.termsData.acf.seo_meta_description : 'ServiceMarket');
                break;
            }
        }
    }
    render() {
        const {termsData, loader} = this.props;
        if(termsData.acf && !loader){
        return (
            <main>
                <DefaultBanner title={termsData.acf.banner_title} background={termsData.acf.banner_image}/>
                <section className="py-5 bg-white">
                    <div className="container">
                        {termsData.acf.banner_description.length ? ( <p dangerouslySetInnerHTML={this.createMarkup(termsData.acf.banner_description)}></p> ) : ''}
                        <SectionTitle title={termsData.title.rendered} />
                        <TitleTextBlock text={termsData.acf.general_content}/>
                    </div>
                </section>
            </main>
        );
        }else{
            return ( <main loader={loader ? "show": "hide"}><Loader/></main>);
        }
    }
}

function mapStateToProps(state){
	return {
        loader: state.loader,
        termsData: state.termsData,
        CurrentCity: state.CurrentCity,
        lang : state.lang
	}
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({fetchTermsData, showLoader, hideLoader}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(TermsPage);
