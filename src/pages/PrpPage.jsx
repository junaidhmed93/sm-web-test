import React from "react";
import DefaultBanner from "../components/DefaultBanner";
import SectionTitle from "../components/SectionTitle";
import TitleTextBlock from "../components/TitleTextBlock";
import Loader from "../components/Loader";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {fetchPRPData, showLoader, hideLoader} from "../actions/index";

class PrpPage extends React.Component{
    constructor(props) {
        super(props);
        this.updateMetaData = this.updateMetaData.bind(this);
    }
    createMarkup(htmlString) {
        return {__html: htmlString};
    }
    componentDidMount(){
        if ( !this.props.prpData.acf ) {
            this.props.showLoader();
            this.props.fetchPRPData(this.props.lang)
            .then(()=> {
                this.updateMetaData();
                this.props.hideLoader();
            });
        } else {
            this.updateMetaData();
        }
    }
    updateMetaData() {
        // console.log('updating meta data');
        document.title = this.props.prpData.acf.seo_meta_title ? this.props.prpData.acf.seo_meta_title : 'ServiceMarket';
        var allMetaElements = document.getElementsByTagName('meta');
        for (var i=0; i<allMetaElements.length; i++) {
            if (allMetaElements[i].getAttribute("name") == "description") {
                allMetaElements[i].setAttribute('content', this.props.prpData.acf.seo_meta_description ? this.props.prpData.acf.seo_meta_description : 'ServiceMarket');
                break;
            }
        }
    }
    render() {
        const {prpData, loader} = this.props;
        if(prpData.acf && !loader){
        return (
            <main>
                <DefaultBanner title={prpData.acf.banner_title} background={prpData.acf.banner_image}/>
                <section className="py-5 bg-white">
                    <div className="container">
                        {prpData.acf.banner_description.length ? ( <p dangerouslySetInnerHTML={this.createMarkup(prpData.acf.banner_description)}></p> ) : ''}
                        <SectionTitle title={prpData.title.rendered} />
                        <TitleTextBlock text={prpData.acf.general_content}/>
                    </div>
                </section>
            </main>
        );
        }else{
            return ( <main loader={loader ? "show": "hide"}><Loader/></main>);
        }
    }
}

function mapStateToProps(state){
	return {
        loader: state.loader,
        prpData: state.prpData,
        CurrentCity: state.CurrentCity,
        lang : state.lang
	}
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({fetchPRPData, showLoader, hideLoader}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(PrpPage);
