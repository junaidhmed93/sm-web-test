import React from "react";
import HomeBanner from "../components/HomeBanner";
import SectionTitle from "../components/SectionTitle";
import WhyServiceMarket from "../components/WhyServiceMarket";
import HowItWorks from "../components/HowItWorks";
import Logos from "../components/Logos";
import OurApps from "../components/OurApps";
import Locations from "../components/Locations";
import ContentSlider from "../components/ContentSlider";
import GoogleReviews from "../components/GoogleReviews";
import AllGoogleReviews from "../components/AllGoogleReviews";
import Loader from "../components/Loader";
import {bindActionCreators} from "redux";
import {connect} from 'react-redux';
import {fetchHomepageData, setBodyClass, toggleForgetPasswordModal} from "../actions/index";
import commonHelper from "../helpers/commonHelper";
import locationHelper from "../helpers/locationHelper";

class HomePage extends React.Component{
    constructor(props) {
        super(props);
        this.showResetPasswordModal = this.showResetPasswordModal.bind(this);
        this.updateMetaData = this.updateMetaData.bind(this);
        this.locationHelperInit = locationHelper.init.bind(this);
        this.locationHelperInit();
    }
    componentDidMount(){
        this.props.setBodyClass('home');
        // Check if the Shared data between pages already requested, if exist we don't need to fetch them again
        const extraData = [];
        if(!this.props.why_sm.acf) extraData.push('whySMData');
        if(!this.props.how_iw.acf) extraData.push('howIWData');
        if(!this.props.logos.acf) extraData.push('logosData');
        const {lang} = this.props;
        if(extraData.length){
            this.props.fetchHomepageData(this.props.match.params.city, extraData, lang);
        }
        if(this.props.match.params.resetKey) {
            this.showResetPasswordModal();
        }
    }
    updateMetaData() {
        let {lang} = this.props
        var seoText = commonHelper.getSeoHomePageTextArray(lang);

        document.title = commonHelper.replaceCodedCurrentCityName(seoText.seoTitle, this.props.currentCity);

        var allMetaElements = document.getElementsByTagName('meta');
        for (var i=0; i<allMetaElements.length; i++) {
            if (allMetaElements[i].getAttribute("name") == "description") {
                allMetaElements[i].setAttribute('content', commonHelper.replaceCodedCurrentCityName(seoText.seoDesc, this.props.currentCity));
                break;
            }
        }
    }
    componentDidUpdate(prevProps) {
        //console.log("component Did Update");
        const prevCity = prevProps.match.params.city;
        const currentCity = this.props.match.params.city;
        if ((prevCity != currentCity)) {
            this.updateMetaData();
        }
    }
    componentWillUnmount(){
        this.props.setBodyClass('default');
    }
    showResetPasswordModal() {
        this.props.toggleForgetPasswordModal({visibility: true, resetToken: this.props.match.params.resetKey});
    }
    renderSliders(){
        const {currentCity} = this.props;
        if( !this.props.loader ){
            return this.props.mainServices.map((links, index)=> {
                var label = '';
                if(links.length) {
                    for (var i = 0; i < links[0].header_category.length; i++) {
                        if (links[0].header_category[i].value == commonHelper.categoriesOrder()[index]) {
                            label = links[0].header_category[i].label;
                        }
                    }
                }
                if(links.length){
                    return (
                        <div key={"category"+index}>
                            <SectionTitle title={label} />
                            <ContentSlider items={links} currentCity={currentCity} />
                        </div>
                    )
                }
            });
        }else{
            return (
                <Loader/>
            )
        }
        
    }
    render() {
        const {why_sm, how_iw, logos, currentCity, mainServices} = this.props;
        let city = currentCity.charAt(0).toUpperCase() + currentCity.slice(1);
        /*console.log("mainServices", mainServices);
        if(mainServices.length){
            return (<section className="error-404 image-404 text-center text-align-centre margin-top60">
            <h1>Wrong address?</h1>
            <p>The page you are looking for was moved, renamed, or may have never existed. </p>
            <p>Go <a href="/" className="text-white">home</a> or try a search:</p>
        </section>);
        }*/
        return (
                <main>
                    <HomeBanner />
                    <section className="py-5 bg-white">
                        <div className="container">
                            {this.renderSliders()}
                        </div>
                    </section>
                    <section className="bg-white">
                        <div className="container">
                            <SectionTitle title={locationHelper.translate("HOW_IT_WORKS_TITLE")} subTitle={locationHelper.translate("HOW_IT_WORKS_DESC")} parentClass="row mb-4 md-md-5" />
                            {how_iw.acf && <HowItWorks items={how_iw.acf.how_it_works_items} parentClass="row pb-5 mb-4 md-md-5" childClass="col px-4 px-md-3" />}
                            <SectionTitle title={locationHelper.translate("WHY_SERVICEMARKET")} subTitle={locationHelper.translate("WHY_SERVICEMARKET_DESC")}/>
                            {why_sm.acf && <WhyServiceMarket items={why_sm.acf.why_service_market_items} parentClass="row pb-5 mb-4 md-md-5" childClass="col px-4 px-md-3" />}
                            <GoogleReviews match={this.props.match} history={this.props.history} parentClass="mb-3 mb-md-5" />
                            <AllGoogleReviews parentClass={"pb-5 mb-4"} childClass=""/>
                            <SectionTitle title={locationHelper.translate("AS_FEATURED_IN")} subTitle=""/>
                            {logos.acf && <Logos items={logos.acf.partners} parentClass="row mb-4 md-md-5" childClass="col-4 col-md-2 text-center my-3" />}
                            <OurApps parentClass="row mb-4 md-md-5" childClass="col px-4 px-md-3 pb-5 mt-md-5" />
                            <SectionTitle title={locationHelper.translate("SERVICEMARKET_LOCATIONS")} subTitle={locationHelper.translate("WE_ARE_CURRENTLY_AVAILABLE_IN")} parentClass="row mb-4 md-md-5" />
                            <Locations />
                        </div>
                    </section>
                </main>
        );
    }
}


function mapStateToProps(state){
    return {
        homepageData: state.homepageData,
        currentCity: state.currentCity,
        lang: state.lang,
        mainServices: state.mainServices,
        why_sm: state.why_sm,
        how_iw: state.how_iw,
        logos: state.logos,
        loader: state.loader
    }
}
function mapDispatchToProps(dispatch){
    return bindActionCreators({setBodyClass,  fetchHomepageData, toggleForgetPasswordModal}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
