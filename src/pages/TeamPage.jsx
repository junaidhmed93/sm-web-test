import React from "react";
import DefaultBanner from "../components/DefaultBanner";
import ImageTitleTextGrid from "../components/ImageTitleTextGrid";
import ApplyButton from "../components/ApplyButton";
import Loader from "../components/Loader";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { fetchTeamData, showLoader, hideLoader } from "../actions/index";
import commonHelper from "../helpers/commonHelper";
import locationHelper from '../helpers/locationHelper'

class TeamPage extends React.Component {
    constructor(props) {
        super(props);
        this.updateMetaData = this.updateMetaData.bind(this);
    }
    createMarkup(htmlString) {
        return { __html: htmlString };
    }
    updateMetaData() {
        // console.log('updating meta data');
        var seoText = commonHelper.getSeoCareersTextArray();
        document.title = seoText.seoTitle;
        var allMetaElements = document.getElementsByTagName('meta');
        for (var i = 0; i < allMetaElements.length; i++) {
            if (allMetaElements[i].getAttribute("name") == "description") {
                allMetaElements[i].setAttribute('content', seoText.seoDesc);
                break;
            }
        }
    }
    componentDidMount() {
        if (!this.props.teamData.acf) {
            this.props.showLoader();
            this.props.fetchTeamData(this.props.lang)
            .then(()=> {
                this.updateMetaData();
                this.props.hideLoader();
            });
        } else {
            this.updateMetaData();
        }
    }
    render() {
        const { teamData, loader } = this.props;
        if (teamData.acf && !loader) {
            return (
                <main>
                    <DefaultBanner title={teamData.acf.banner_title} background={teamData.acf.banner_image} />
                    <section className="py-5 bg-white">
                        <div className="container">
                            {teamData.acf.banner_description.length ? (<p dangerouslySetInnerHTML={this.createMarkup(teamData.acf.banner_description)}></p>) : ''}
                            <ImageTitleTextGrid items={teamData.acf.be_part_of_our_team_features} />
                            <div className="row">
                                <div className="col text-center">
                                    <p className="text-center">{locationHelper.translate('CAREERS_PAGE_TITlE_TXT')}</p>
                                    <div className="row">
                                        <div className="col col-sm-6 col-md-4 mx-auto">
                                            <a href="mailto:careers@servicemarket.com" className="btn font-weight-bold text-uppercase btn-warning btn-lg px-5 mb-3">
                                            {locationHelper.translate('APPLY')}
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </main>
            );
        } else {
            return (<main loader={loader ? "show" : "hide"}><Loader /></main>);
        }
    }
}

function mapStateToProps(state) {
    return {
        loader: state.loader,
        teamData: state.teamData,
        CurrentCity: state.CurrentCity,
        lang : state.lang
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ fetchTeamData, showLoader, hideLoader }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(TeamPage);
