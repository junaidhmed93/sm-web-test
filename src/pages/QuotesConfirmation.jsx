import React from "react";
import Loader from "../components/Loader";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {withCookies} from 'react-cookie';
import { setBodyClass, URLCONSTANT, DUBAI} from "../actions";
import SectionTitle from "../components/SectionTitle";
import ContentSlider from "../components/ContentSlider";
import LatestBlogNews from "../components/LatestBlogNews";
import commonHelper from "../helpers/commonHelper";
import locationHelper from "../helpers/locationHelper";
import DuPromo from "../components/DuPromo";

class QuotesConfirmation extends React.Component {
    _isMounted = false;

    constructor(props) {
        super(props);
        const { cookies } = props;
        var url_params = props.url_params;
        var serviceURL = url_params.params.slug;

        this.locationHelperInit = locationHelper.init.bind(this);
        this.locationHelperInit();

        var serviceId = commonHelper.getParameterByName("service_id",props.quotesParams);
        var serviceHeading = this.titleText(serviceId, serviceURL);

        this.state = {
            serviceHeading: serviceHeading
        };
        this.titleText = this.titleText.bind(this);
        this.getCookieName = this.getCookieName.bind(this);
        this.duPromo = this.duPromo.bind(this);

    }
    componentDidMount() {
        this._isMounted = true;
        this.props.setBodyClass('quotesconfirmation');
        const { cookies } = this.props;
        
        /*var serviceId = commonHelper.getParameterByName("service_id");

        var serviceHeading = this.titleText(serviceId) != "" ? this.titleText(serviceId) : "Thank you for submitting your request";
        
        this.setState({
            serviceHeading : serviceHeading
        })*/
        //var rid = cookies.get("rid") || false;
        cookies.remove("rid" , {path: '/'});
        var quotesDataName = this.getCookieName("quotes_data");
        cookies.remove("passedService" , {path: '/'});
        cookies.remove(quotesDataName, { path: '/' });
        var quotesMoveDetailsName = this.getCookieName("local_move_details");
        cookies.remove(quotesMoveDetailsName, { path: '/' });
        var url_params = this.props.url_params;
        var serviceURL = url_params.params.slug;
        if(serviceURL != URLCONSTANT.INTERNATIONAL_MOVER_PAGE_URI){
            let thisVar = this;
            setTimeout(function(){
                thisVar.setState({
                    showPromo : true
                })
            },7000)
        }
    }

    componentWillUnmount(){
        this._isMounted = false;
    }
    getCookieName(cookie_name) {
        var url_params = this.props.url_params;

        var serviceURL = url_params.params.slug;

        var currentLanguage = url_params.params.lang;

        var currentPath = serviceURL;

        var current_city = url_params.params.city;

        var cookieName = currentLanguage + "." + current_city + "." + currentPath + "." + cookie_name;

        return cookieName;
    }
    titleText(serviceId, serviceURL){
        const serviceConstants = this.props.serviceConstants;
        
        var headingText = {},
            heading = "";

        let movingText = locationHelper.translate("THANK_YOU_FOR_REQUESTING_MOVING_QUOTES");

        let shippingText = locationHelper.translate("THANK_YOU_FOR_REQUESTING_CAR_SHIPPING_QUOTES");

        let storageText = locationHelper.translate("THANK_YOU_FOR_REQUESTING_STORAGE_QUOTES");

        let gentralText = locationHelper.translate("THANK_YOU_FOR_REQUESTING_QUOTES");

        let deepCleaningText = locationHelper.translate("THANK_YOU_FOR_REQUESTING_DEEP_CLEANING_QUOTES");

        let cateringText = locationHelper.translate("THANK_YOU_FOR_REQUESTING_CATERING_QUOTES");

        let waterTankText = locationHelper.translate("THANK_YOU_FOR_REQUESTING_WATER_TANK_QUOTES");

        let carpetText = locationHelper.translate("THANK_YOU_FOR_REQUESTING_CARPET_CLEANING_QUOTES");

        let sofaText = locationHelper.translate("THANK_YOU_FOR_REQUESTING_SOFA_CLEANING_QUOTES");

        let mattressText = locationHelper.translate("THANK_YOU_FOR_REQUESTING_MATTRESS_CLEANING_QUOTES");

        let officeText = locationHelper.translate("THANK_YOU_FOR_REQUESTING_OFFICE_CLEANING_QUOTES");

        let paintingText = locationHelper.translate("THANK_YOU_FOR_REQUESTING_PAINTING_QUOTES");

        let acText = locationHelper.translate("THANK_YOU_FOR_REQUESTING_AC_MAINTENANCE_QUOTES");

        let landscapingText = locationHelper.translate("THANK_YOU_FOR_REQUESTING_LANDSCAPING_QUOTES");

        let carpentryText = locationHelper.translate("THANK_YOU_FOR_REQUESTING_CARPENTRY_QUOTES");

        let pestControlText = locationHelper.translate("THANK_YOU_FOR_REQUESTING_PEST_CONTROL_QUOTES");

        let photographyText = locationHelper.translate("THANK_YOU_FOR_REQUESTING_PHOTOGRAPHY_QUOTES");

        headingText[serviceConstants.SERVICE_LOCAL_MOVE] = movingText;
        
        headingText[URLCONSTANT.LOCAL_MOVER_PAGE_URI]= movingText,

        headingText[serviceConstants.SERVICE_INTERNATIONAL_MOVE] =  movingText;
        headingText[URLCONSTANT.INTERNATIONAL_MOVER_PAGE_URI]= movingText,
        headingText[serviceConstants.SERVICE_CAR_SHIPPING] =  shippingText,
        headingText[URLCONSTANT.CAR_SHIPPING_PAGE_URI] =  shippingText,
        headingText[serviceConstants.SERVICE_STORAGE] = storageText,
        headingText[URLCONSTANT.STORAGE_COMPANIES_PAGE_URI] = storageText,
        headingText[serviceConstants.SERVICE_PARTTIME_NANNIES__BABYSITTERS] = gentralText,
        headingText[URLCONSTANT.BABYSITTING_PAGE_URI] = gentralText,
        headingText[serviceConstants.SERVICE_CURTAINS__BLINDS] = gentralText,
        headingText[URLCONSTANT.CURTAINS_PAGE_URI] = gentralText,
        headingText[serviceConstants.SERVICE_CATERING] = cateringText,
        headingText[URLCONSTANT.CATERING_PAGE_URI] = cateringText,
        headingText[serviceConstants.SERVICE_PHOTOGRAPHY] = photographyText,
        headingText[URLCONSTANT.PHOTOGRAPHY_PAGE_URI] = photographyText,
        headingText[serviceConstants.SERVICE_DEEP_CLEANING_SERVICE] = deepCleaningText,
        headingText[serviceConstants.SERVICE_WATER_TANK_CLEANING] = waterTankText,
        headingText[serviceConstants.SERVICE_MATTRESS_CLEANING] = mattressText,
        headingText[serviceConstants.SERVICE_SOFA_CLEANING] = sofaText,
        headingText[serviceConstants.SERVICE_CARPET_CLEANING] = carpetText,
        headingText[serviceConstants.SERVICE_OFFICE_CLEANING] = officeText,
        headingText[serviceConstants.SERVICE_FULLTIME_MAIDS] =  gentralText,
        headingText[URLCONSTANT.CLEANING_MAID_PAGE_URI] = gentralText,
        headingText[serviceConstants.SERVICE_PAINTING] = paintingText,
        
        headingText[URLCONSTANT.PAINTERS_PAGE_URI] = paintingText,

        
        headingText[serviceConstants.SERVICE_AC_AND_HEATING] =  acText,
        headingText[serviceConstants.SERVICE_AC_CLEANING] =  acText,
        headingText[serviceConstants.SERVICE_AC_INSTALLATION] =  acText,
        headingText[serviceConstants.SERVICE_AC_REPAIR] =  acText,
        headingText[serviceConstants.SERVICE_AC_DUCT_CLEANING] = acText,
        headingText[URLCONSTANT.AC_MAINTENANCE_PAGE_URI]= acText,

        headingText[serviceConstants.SERVICE_MAINTENANCE] = gentralText,
        headingText[serviceConstants.SERVICE_ELECTRICIAN] = gentralText,
        headingText[serviceConstants.SERVICE_PLUMBING] = gentralText,
        headingText[serviceConstants.SERVICE_LOCKSMITH] = gentralText,
        headingText[serviceConstants.SERVICE_CARPENTRY] = carpentryText,
        headingText[URLCONSTANT.MAINTENANCE_PAGE_URI] = gentralText,
        headingText[serviceConstants.SERVICE_BUILDING__FLOORING] = gentralText,
        headingText[serviceConstants.SERVICE_ANNUAL_MAINTENANCE_CONTRACT] = gentralText,
        headingText[serviceConstants.SERVICE_GARDEN_MAINTENANCE] = gentralText,
        headingText[URLCONSTANT.GARDENING_PAGE_URI] = gentralText,
        headingText[serviceConstants.SERVICE_LANDSCAPING_AND_DESIGN] = landscapingText,
        headingText[serviceConstants.SERVICE_GAZEBOS_DECKS_AND_PORCHES] = gentralText,
        headingText[serviceConstants.SERVICE_GRASS_AND_ARTIFICIAL_LAWNS] = gentralText,
        headingText[serviceConstants.SERVICE_SWIMMING_POOLS_AND_WATER_FEATURES__MAINTENANCE] = gentralText,
        headingText[serviceConstants.SERVICE_ANNUAL_GARDENING_CONTRACT] = gentralText,
        headingText[serviceConstants.SERVICE_ANNUAL_PEST_CONTROL_CONTRACT] = gentralText,
        headingText[serviceConstants.SERVICE_PEST_CONTROL] = pestControlText;
        headingText[URLCONSTANT.PEST_CONTROL_PAGE_URI] = pestControlText;
        

        heading = typeof headingText[serviceId] != "undefined" ? headingText[serviceId] : ""; 

        heading = ( heading == "" && typeof headingText[serviceURL] != "undefined" ) ? headingText[serviceURL] : heading;
        
        //console.log("heading", headingText[serviceURL], headingText[serviceId], heading);

        return heading != "" ? heading : "Thank you for submitting your request";
        
    }
    duPromo(){
        const { cookies, url_params, currentCity} = this.props;
        let serviceURL = url_params.params.slug;
        let quotesMoveDetailsName = this.getCookieName("local_move_details");
        let quotesMoveDetails = cookies.get(quotesMoveDetailsName) || {};

        if(serviceURL != URLCONSTANT.INTERNATIONAL_MOVER_PAGE_URI && currentCity == DUBAI ){
            return (<DuPromo showPopUp={this.state.showPromo} quotesMoveDetails={quotesMoveDetails}/>);
        }else{
            return "";
        }
    }

    render() {
        const { rid, landingData, currentCity, currentCityData} = this.props;
        let cityObj = currentCityData[0];
        var data = {};
        /*var confirmationSummary = cookies.get('_confirmation_summary') || {};
        //console.log((confirmationSummary));*/
        if (landingData.length) {
            data = landingData[0];
        }
        return (
            <React.Fragment>
            <main className="confirmation quote-confirmation">
                <div className="container ">
                    <div className="journey-row row">
                        <div className="region region-content">
                            <div id="block-system-main" className="block block-system">
                                <div className="content">
                                    <section>
                                        <div className="request-submission">
                                            <div className="confirmation-container">
                                                <div className="confirmation-header mth mbh pbm">
                                                    <div className="row show-grid">
                                                        <div className="col-md-12">
                                                            <p className="text-center mvm">
                                                                <img src="/dist/images/sm-user-icon.png" width="154"/>
                                                            </p>
                                                            <h2 className="mvm text-center link-text uppercase">
                                                                <strong>{this.state.serviceHeading}</strong>
                                                            </h2>
                                                            <div className="reference-text">
                                                                <p className="text-center mtm">{locationHelper.translate("YOUR_REFERENCE_ID")} </p>
                                                                <p className="text-center mbm"><span
                                                                    className="request-id text-warning">{rid}</span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="row booking-confirmation-info pbm">
                                                    <div className="col-12 confirmation-info">
                                                        <div className="what-happens-next">
                                                            <h3>{locationHelper.translate("WHAT_HAPPENS_NEXT")}</h3>
                                                            <ul>
                                                                <li>{locationHelper.translate("WE_WILL_SHARE_YOUR_REQUEST_WITH_OUR_PARTNERS")}</li>
                                                                <li>{locationHelper.translate("WE_MIGHT_CONTACT_YOU")}</li>
                                                                <li>{locationHelper.translate("IN_THE_MEANTIME_FEEL_FREE")}</li>

                                                            </ul>
                                                        </div>
                                                        <div className="need-assistance">
                                                            <h3>{locationHelper.translate("NEED_ANY_ASSISTANCE")}</h3>
                                                            <p className="mtm mbm">{locationHelper.translate("YOU_CAN_REACH_US_ON")} &nbsp;<a href={'tel:' + cityObj.phoneNumber} className="ltr d-inline-block">{cityObj.phoneNumber.substr(0, 4) + ' ' + cityObj.phoneNumber.substr(4, 1) + ' ' + cityObj.phoneNumber.substr(5)}</a> 
                                                                <span className="uae-location phone-no-link"> &nbsp;{locationHelper.translate("ANY_DAY_OF_THE_WEEK_BETWEEN")} {cityObj.hoursInWeek}.</span>
                                                            </p>
                                                            <p className="mbm">{locationHelper.translate("OR_YOU_CAN_EMAIL_US_ON")} 
                                                                <a href="mailto:support@servicemarket.com">support@servicemarket.com</a>.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                    {
                        data.acf?(
                            <div className={"py-3"}>
                                <SectionTitle title={locationHelper.translate("RELATED_SERVICES")} subTitle={data.acf.recommended_for_you_static.recommended_main_text}/>
                                {data.acf.recommended_for_you && <ContentSlider items={data.acf.recommended_for_you} currentCity={currentCity} />}
                                {data.acf.posts && data.acf.posts.length > 0 && <SectionTitle title={locationHelper.translate("SERVICEMARKET_BLOG")} subTitle={locationHelper.translate("EVERYTHING_YOU_NEED_TO_KNOW_ABOUT_ORGANISING_YOUR_HOME_LIFE_AND_MORE")} parentClass="row mb-4" childClass="col px-4 px-md-3" />}
                                {data.acf.posts && <LatestBlogNews items={data.acf.posts}/>}
                            </div>
                        ):''
                    }
                </div>
            </main>
                { this.duPromo() }
            </React.Fragment>
        )

    }
}

function mapStateToProps(state) {
    return {
        signInDetails: state.signInDetails,
        myCreditCardsData: state.myCreditCardsData,
        landingData: state.landingConfirmationData,
        currentCity: state.currentCity,
        serviceConstants: state.serviceConstants,
        lang: state.lang,
        currentCityData: state.currentCityData
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ setBodyClass}, dispatch);
}

export default withCookies(connect(mapStateToProps, mapDispatchToProps)(QuotesConfirmation));

