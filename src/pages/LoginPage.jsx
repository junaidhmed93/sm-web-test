import React from "react";
import Loader from "../components/Loader";
import SocialButton from "../components/SocialButton";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {showLoader, hideLoader} from "../actions/index";
import staticVariables from "../staticVariables";

class LoginPage extends React.Component{
    constructor(props) {
        super(props);
    }
    componentDidMount(){

    }

    render() {
        return (
        <main>
            <div>
                <SocialButton
                    provider='facebook'
                    appId={staticVariables.FACEBOOK_APP_ID}
                    onLoginSuccess={handleSocialLogin}
                    onLoginFailure={handleSocialLoginFailure}
                    >
                    Login with Facebook
                </SocialButton>
                <SocialButton
                    provider='google'
                    appId={staticVariables.GOOGLE_APP_ID}
                    onLoginSuccess={handleSocialLogin}
                    onLoginFailure={handleSocialLoginFailure}
                    >
                    Login with Google
                </SocialButton>

            </div>
        </main>
        )

    }
}

const handleSocialLogin = (user) => {
    // console.log(user._profile);
    // console.log(user._profile)
}

const handleSocialLoginFailure = (err) => {
    console.error(err)
}

function mapStateToProps(state){
	return {
        loader: state.loader
	}
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({showLoader, hideLoader}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
