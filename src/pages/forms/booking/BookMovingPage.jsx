import React from "react";
import FormFieldsTitle from "../../../components/FormFieldsTitle";
import CheckRadioBoxInput from "../../../components/Form_Fields/CheckRadioBoxInput";
import DatePick from "../../../components/Form_Fields/DatePick";
import SelectInput from "../../../components/Form_Fields/SelectInput";
import TextFloatInput from "../../../components/Form_Fields/TextFloatInput";
import AreaSuggestionInput from "../../../components/Form_Fields/AreaSuggestionInput";
import TextareaInput from "../../../components/Form_Fields/TextareaInput";
import BookingSummary from "../../../components/Form_Fields/BookingSummary";
import PackageBox from "../../../components/Form_Fields/PackageBox";
import BookNextStep from "../../../components/Form_Fields/BookNextStep";
import FormTitleDescription from "../../../components/FormTitleDescription";
import ContactDetailsStep from "../../../components/ContactDetailsStep";
import locationHelper from "../../../helpers/locationHelper";
import commonHelper from "../../../helpers/commonHelper";
import TaxCalculator from "./TaxCalculator";
import BookingVoucherify from "./BookingVoucherify";
import moment from 'moment';
import {withCookies} from "react-cookie";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import Loader from "../../../components/Loader";
let allServiceConstant = {};
let URLConstant = {};
let currentCurrency = "";
let current_city = "dubai";
var discountData = "";
import {
    scrollToTop,
    zendeskChatBox
} from "../../../actions";
var status = null;
var bid = null;
var tt = null;
let movingPrice = {
    "local-moving-full":{
        "transportation_charges": [
            {
                "from": "dubai",
                "to": {
                    "dubai": 0,
                    "abu-dhabi": 300,
                    "sharjah": 100,
                    "ajman": 300,
                    "fujairah": 500,
                    "ras-al-khaimah": 300,
                    "umm-al-quwain": 300
                }
            },
            {
                "from": "abu-dhabi",
                "to": {
                    "dubai": 300,
                    "abu-dhabi": 0,
                    "sharjah": 300,
                    "ajman": 500,
                    "fujairah": 500,
                    "ras-al-khaimah": 500,
                    "umm-al-quwain": 500
                }
            },
            {
                "from": "sharjah",
                "to": {
                    "dubai": 100,
                    "abu-dhabi": 300,
                    "sharjah": 0,
                    "ajman": 150,
                    "fujairah": 500,
                    "Ras Al Khaimah": 300,
                    "umm-al-quwain": 300
                }
            }
        ],
        "packages": [
            {
                "id": 1,
                "name": "Standard mover",
                "tagline": "Most Popular",
                "card_type": "NOTICEABLE",
                "move_provider_category": "Standard companies",
                "btnClass":"btn-warning",
                "great_for":[
                    "Professionally trained team",
                    "Standard packing and unpacking materials",
                    "Mover with customer rating above 4 out of 5"
                ],
                "details": {
                    "pricing": [
                        {
                            "type": "Apartment",
                            "price_by_size": {
                                "1": 1300,
                                "2": 2000,
                                "3": 3200,
                                "4": 4000,
                                "Studio": 950
                            }
                        },
                        {
                            "type": "Villa",
                            "price_by_size": {
                                "2": 2500,
                                "3": 3500,
                                "4": 5000,
                                "5": 6000
                            }
                        }
                    ],
                    "features": [
                        {
                            "title": "Team",
                            "perks": {
                                "Trucks": "Closed",
                                "Uniform": "Yes",
                                "Communication Skills": "Very Good",
                                "Quality of Packing": "Excellent"
                            }
                        },
                        {
                            "title": "Packing Material used",
                            "perks": {
                                "Boxes": "Premium Quality",
                                "Hanging Boxes for Clothes": "Yes",
                                "Bubble Wrap": "No"
                            }
                        },
                        {
                            "title": "Services Included",
                            "perks": {
                                "Packing": "Yes",
                                "Unpacking Skills": "Yes",
                                "Disassembly/ Assembly": "Yes"
                            }
                        },
                        {
                            "title": "Others",
                            "perks": {
                                "Customer Rating": "4 or higher",
                                "International certifications": "None",
                                "Offers insurance?": "No"
                            }
                        }
                    ]
                }
            },
            {
                "id": 2,
                "name": "Premium mover",
                "tagline": "Peace of mind",
                "card_type": "NORMAL",
                "btnClass":"btn-primary",
                "move_provider_category": "Premium companies",
                "great_for":[
                    "Internationally certified mover",
                    "Premium packing and unpacking",
                    "Mover with customer rating above 4 out of 5"
                ],
                "details": {
                    "pricing": [
                        {
                            "type": "Apartment",
                            "price_by_size": {
                                "1": 1700,
                                "2": 2700,
                                "3": 4500,
                                "4": 5000
                            }
                        },
                        {
                            "type": "Villa",
                            "price_by_size": {
                                "2": 3200,
                                "3": 4500,
                                "4": 6000,
                                "5": 7000
                            }
                        }
                    ],
                    "features": [
                        {
                            "title": "Team",
                            "perks": {
                                "Trucks": "Closed",
                                "Uniform": "Yes",
                                "Communication Skills": "Very Good",
                                "Quality of Packing": "Expert:"
                            }
                        },
                        {
                            "title": "Packing Material used",
                            "perks": {
                                "Boxes": "International Quality",
                                "Hanging Boxes for Clothes": "Yes",
                                "Bubble Wrap": "Yes"
                            }
                        },
                        {
                            "title": "Services Included",
                            "perks": {
                                "Packing": "Yes",
                                "Unpacking Skills": "Yes",
                                "Disassembly/ Assembly": "Yes"
                            }
                        },
                        {
                            "title": "Others",
                            "perks": {
                                "Customer Rating": "4 or higher",
                                "International certifications": "FIDI certified",
                                "Offers insurance?": "Yes"
                            }
                        }
                    ]
                }
            },
            {
                "id": 3,
                "name": "Budget mover",
                "tagline": "Saver option",
                "card_type": "NORMAL",
                "move_provider_category": "Budget companies",
                "great_for":[
                    "Experienced labour",
                    "Recycled packing materials",
                    "Mover with customer rating above 3.5 out of 5"
                ],
                "details": {
                    "pricing": [
                        {
                            "type": "Apartment",
                            "price_by_size": {
                                "1": 1000,
                                "2": 1500,
                                "3": 2500,
                                "4": 3500,
                                "Studio": 750
                            }
                        },
                        {
                            "type": "Villa",
                            "price_by_size": {}
                        }
                    ],
                    "features": [
                        {
                            "title": "Team",
                            "perks": {
                                "Trucks": "Open",
                                "Uniform": "Yes",
                                "Communication Skills": "Fair",
                                "Quality of Packing": "Fair"
                            }
                        },
                        {
                            "title": "Packing Material used",
                            "perks": {
                                "Boxes": "Normal",
                                "Hanging Boxes for Clothes": "No",
                                "Bubble Wrap": "No"
                            }
                        },
                        {
                            "title": "Services Included",
                            "perks": {
                                "Packing": "Yes",
                                "Unpacking Skills": "Yes",
                                "Disassembly/Assembly": "No"
                            }
                        },
                        {
                            "title": "Others",
                            "perks": {
                                "Customer Rating": "3.5 or higher",
                                "International certifications": "None",
                                "Offers insurance?": "No"
                            }
                        }
                    ]
                }
            }
        ]
    },
    "local-moving":{
        "transportation_charges":{
            "dubai":{
                "dubai": 0,
                "abu-dhabi": 300,
                "sharjah": 100,
                "ajman": 300,
                "fujairah": 500,
                "ras-al-khaimah": 300,
                "umm-al-quwain": 300
            },
            "abu-dhabi":{
                "dubai": 0,
                "abu-dhab	i": 300,
                "sharjah": 100,
                "ajman": 300,
                "fujairah": 500,
                "ras-al-khaimah": 300,
                "umm-al-quwain": 300
            },
            "sharjah":{
                "dubai": 100,
                "abu-dhabi": 300,
                "sharjah": 0,
                "ajman": 150,
                "fujairah": 500,
                "Ras Al Khaimah": 300,
                "umm-al-quwain": 300
            }
        },
        "full": {
            "packages": [
                {
                    "id": 1,
                    "name": "Standard mover",
                    "tagline": "Most Popular",
                    "card_type": "NOTICEABLE",
                    "move_provider_category": "Standard companies",
                    "btnClass":"btn-warning",
                    "rating":{
                        "Mover rating": 2,
                        "Team": 2,
                        "Packing material": 2,
                        "Communication skills": 2,
                        "Quality of Packing": 2
                    },
                    "pricing":{
                        "Apartment":{
                            "1": 1300,
                            "2": 2000,
                            "3": 3200,
                            "4": 4000,
                            "Studio": 950
                        },
                        "Villa":{
                            "2": 2500,
                            "3": 3500,
                            "4": 5000,
                            "5": 6000
                        }
                    },
                    "features": {
                        "Closed Trucks": "Yes",
                        "Uniform": "Yes",
                        "Boxes": "Yes",
                        "Bubble Wrap": "No",
                        "Packing / Unpacking":"Yes",
                        "Dis-Assembly / Assembly": "No",
                        "Offers Insurance": "No",
                        "International Certification":"No"
                    }
                },
                {
                    "id": 2,
                    "name": "Premium mover",
                    "tagline": "Peace of mind",
                    "card_type": "NORMAL",
                    "btnClass":"btn-primary",
                    "move_provider_category": "Premium companies",
                    "rating":{
                        "Mover rating": 3,
                        "Team": 3,
                        "Packing material": 3,
                        "Communication skills": 3,
                        "Quality of Packing": 3
                    },
                    "pricing":{
                        "Apartment":{
                            "1": 1700,
                            "2": 2700,
                            "3": 4500,
                            "4": 5000
                        },
                        "Villa":{
                            "2": 3200,
                            "3": 4500,
                            "4": 6000,
                            "5": 7000
                        }
                    },
                    "features": {
                        "Closed Trucks": "Yes",
                        "Uniform": "Yes",
                        "Boxes": "Yes",
                        "Bubble Wrap": "Yes",
                        "Packing / Unpacking":"Yes",
                        "Dis-Assembly / Assembly": "Yes",
                        "Offers Insurance": "Yes",
                        "International Certification":"Yes"
                    }
                },
                {
                    "id": 3,
                    "name": "Budget mover",
                    "tagline": "Saver option",
                    "card_type": "NORMAL",
                    "btnClass":"btn-grey",
                    "move_provider_category": "Budget companies",
                    "rating":{
                        "Mover rating": 1,
                        "Team": 1,
                        "Packing material": 1,
                        "Communication skills": 1,
                        "Quality of Packing": 1
                    },
                    "pricing":{
                        "Apartment":{
                            "1": 1000,
                            "2": 1500,
                            "3": 2500,
                            "4": 3500,
                            "Studio": 750
                        },
                        "Villa":{
                        }
                    },
                    "features": {
                        "Closed Trucks": "No",
                        "Uniform": "Yes",
                        "Boxes": "Yes",
                        "Bubble Wrap": "No",
                        "Packing / Unpacking":"Yes",
                        "Dis-Assembly / Assembly": "No",
                        "Offers Insurance": "No",
                        "International Certification":"No"
                    }
                }
            ]
        },
        "small":{
            "packages": [
                {
                    "id": 1,
                    "name": "One Guy and Truck",
                    "tagline": "Basic Option",
                    "card_type": "NORMAL",
                    "move_provider_category": "Budget companies",
                    "move_provider_title": "Budget movers",
                    "btnClass":"btn-grey",
                    "great_for":[
                        "Pick up and delivery of a single item or a few boxes on a single trip with a driver. This doesn't include packing, heavy lifting, or assembly."
                    ],
                    "startingPrice": 249,
                    "descriptive_details": "We’ll book a delivery truck and driver to load your items, and deliver them to the location."
                },
                {
                    "id": 2,
                    "name": "2 Guys and Truck",
                    "tagline": "Most Popular",
                    "card_type": "NOTICEABLE",
                    "move_provider_category": "Standard companies",
                    "move_provider_title": "Standard movers",
                    "btnClass":"btn-warning",
                    "great_for":[
                        "Packing, pick up and delivery services for moving 2-3 pieces of furniture or 5-10 boxes"
                    ],
                    "startingPrice": 449,
                    "descriptive_details": "We'll help find you a delivery truck and a helping hand to help pick up the items, load them into a truck, and deliver them to their destination."
                },
                {
                    "id": 3,
                    "name": "A small crew",
                    "tagline": "Smart Move",
                    "card_type": "NORMAL",
                    "move_provider_category": "Premium companies",
                    "move_provider_title": "Premium movers",
                    "btnClass":"btn-primary",
                    "great_for":[
                        "Packing, pick up, delivery, and reassembly services for moving 4-5 pieces of furniture or more than 10 boxes."
                    ],
                    "startingPrice": 649,
                    "descriptive_details": "We'll help find you a delivery truck and a couple of helping hands to help pick up the items, load them into a truck, and deliver them to their destination."
                }
            ]
        }
    }
}
let transportationCharges = movingPrice["local-moving"]["transportation_charges"];
let full_move_package = movingPrice["local-moving"]["full"];
let small_move_package = movingPrice["local-moving"]["small"];
class BookMovingPage extends React.Component{

    constructor(props) {
        super();
        this.state = {
            typeOfMove: {id: 1, value: "full_move", label: "Full home"},
            homeType:"",
            rooms:"",
            moving_size: {value: "", label: ""},
            from_emirate:"",
            from_area:"",
            from_address:"",
            to_emirate:"",
            to_area:"",
            to_address:"",
            details:"",
            item_lists:"",
            booking_date:'',
            input_email: props.userProfile.email,
            input_phone: props.userProfile.address ? props.userProfile.address.phoneNumber : '',
            input_name: props.userProfile.customerFirstName,
            input_last_name: props.userProfile.customerLastName,
            input_address_city: {},
            input_address_area: props.setUserArea,
            input_address_area_building_name: props.userProfile.address ? props.userProfile.address.building : '',
            input_address_area_building_apartment: props.userProfile.address ? props.userProfile.address.apartment : '',
            payment: '',
            package_options:[],
            packageError: false,
            transportation_fees: 0,
            showMobileSummary: false,
            loginRequired: false,
            packages: {},
            need_handyman: {id:2,value:"No",label:"No"},
            price:0,
            subtotal:0,
            vat:0,
            total:0,
            totalAfterDiscount:0,
            discountData: {},
            voucherCode: "",
            softSafeBookingId:""
        }
        var cookieData = props.getCookieFormData();

        if(Object.keys(cookieData).length){
            props.removeCookie("booking_data");
            props.removeCookie("submitted_data");
            props.removeCookie("confirmation_summary");
        }

        this.movingRequest = this.movingRequest.bind(this);
        this.ContactDetails = this.ContactDetails.bind(this);
        this.getPackagePrices = this.getPackagePrices.bind(this);
        this.getTransportationCharges = this.getTransportationCharges.bind(this);
        this.Packages = this.Packages.bind(this);
        this.handleShowMoreOptionChange = this.handleShowMoreOptionChange.bind(this);
        this.handleCustomChange = this.handleCustomChange.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.currentStep = this.currentStep.bind(this);
        this.moveNext = this.moveNext.bind(this);
        this.hashChangeHandler = this.hashChangeHandler.bind(this);
        this.calculatePrice = this.calculatePrice.bind(this);
        this.mobileSummary = this.mobileSummary.bind(this);
        this.handleVoucherChange = this.handleVoucherChange.bind(this);
        this.updateTotal = this.updateTotal.bind(this);

        /*this.handleMovingFromEmirate = this.handleMovingFromEmirate.bind(this);
        this.handleMovingToEmirate = this.handleMovingToEmirate(this);*/

    }
    componentDidMount() {
        allServiceConstant = this.props.service_constants;
        URLConstant =  this.props.url_constants;
        current_city = this.props.current_city;
        var defaultCity = locationHelper.getLocationByName(current_city);
        var selectedCityVal = {id: defaultCity.id, value: commonHelper.slugify(defaultCity.name), label: defaultCity.name}
        var promoCode =  commonHelper.getParameterByName("promo") != null ? commonHelper.getParameterByName("promo") : "";
        this.setState({
            from_emirate: selectedCityVal,
            to_emirate: selectedCityVal,
            voucherCode: promoCode
        });
        currentCurrency = locationHelper.getCurrentCurrency();
        zendeskChatBox();
        window.addEventListener("hashchange", this.hashChangeHandler, false);
        
    }
    componentWillUnmount(){
        window.removeEventListener("hashchange", this.hashChangeHandler, false);
    }
    componentWillReceiveProps (newProps) {
        if(newProps.userProfile !== this.props.userProfile){
            this.setState({
                input_email: newProps.userProfile.email,
                input_name: newProps.userProfile.customerFirstName,
                input_last_name: newProps.userProfile.customerLastName,
            });
            if (newProps.userProfile.address && newProps.userProfile.address.phoneNumber.length) {
                this.setState({
                    input_phone: newProps.userProfile.address.phoneNumber,
                });
            }
        }

        if( this.props.signInDetails.customer !== newProps.signInDetails.customer ){

            if( newProps.signInDetails.customer ){
                this.setState({
                    loginRequired: false
                })
            }
        }
    }
    calculatePrice(item){
        var total_price = 0,
        taxRate = 0,
        taxChargesTotal = 0,
        total = 0;
        var {packages,booking_date} = this.state;

        if(typeof item != "undefined"){
            packages = item;
        }
        //console.log(packages);

        if(typeof packages.new_pricing != "undefined"){
            total_price = parseInt(packages.new_pricing);

            taxChargesTotal = TaxCalculator.calculateVAT(total_price,booking_date);

            total = total_price + taxChargesTotal;

        }
        this.setState({
            packages: packages,
            price:total_price,
            subtotal:total_price,
            vat:taxChargesTotal,
            total:total
        })
    }
    sumarryItems(){
        const {typeOfMove, homeType, moving_size, from_emirate, to_emirate, booking_date, from_area, to_area, rooms, subtotal, vat, total, need_handyman} = this.state;
        var handyman_charge = 150;
        var items = [
            {label: 'Type of moving', value: typeOfMove.label},
            {label: 'Type of residence', value: moving_size.summeryLabel},
            {label: 'Moving from', value: from_emirate.label + (from_area.length ? ' - '+ from_area : '')},
            {label: 'Move To', value: to_emirate.label + (to_area.length ? ' - '+ to_area : '')},
            {label: 'Service Schedule', value: booking_date}
        ];

        if(need_handyman.value == "Yes"){
            items.push({label: 'Handyman Charges', value: currentCurrency+" "+handyman_charge+" /hour"})
        }
        if(subtotal != 0) {
            items.push({label: 'Subtotal', value: currentCurrency + " " + subtotal},
                {label: 'VAT', value: currentCurrency + " " + vat});
        }
        return items
    }
    hashChangeHandler(){
        let hashVal = window.location.hash;
        let step;
        hashVal.length ? step=parseInt( window.location.hash.replace('#','') ) : step = 1;
        this.props.moveNext(step);
        //scrollToTop();
    }
    currentStep(){
        return this.props.formCurrentStep;
    }
    moveNext(step){
        const {moveNextStep, formCurrentStep} = this.props;
        var selectedPackage = this.state.packages;
        var packageError = false;

        if(formCurrentStep == 3){
            if(typeof selectedPackage == "object" && Object.keys(selectedPackage).length) {
                moveNextStep(step, this.state);
            }else{
                packageError = true;
                scrollToTop()
            }
            this.setState({
                packageError: packageError
            })
        }else {
            moveNextStep(step, this.state);
        }
    }
    handleShowMoreOptionChange(){
        this.setState({
            show_more_option: !this.state.show_more_option
        });
    }
    handleCustomChange(name, item){

        if(name == "typeOfMove"){
            if(item.value == "small_move"){
                var package_options = this.getPackagePrices();
                this.setState({
                    [name]: item,
                    "package_options":package_options
                });
            }else{
                this.setState({
                    [name]: item
                });
            }
        }
        if( name == "moving_size" ){
            let homeType = "";
            let rooms = "";
            if(item.value == "Studio Apartment"){
                homeType = "Apartment";
                rooms = "Studio";
            }else{
                let home = item.value.split(" ");
                homeType = home[2];
                rooms = home[0];
            }
            var package_options = this.getPackagePrices(homeType, rooms);

            this.setState({
                [name]: item,
                "homeType": homeType,
                "rooms":rooms,
                "package_options":package_options
            });
        }
        else if( name == "from_emirate" || name == "to_emirate"){
            let from_emirate = "";
            let to_emirate = "";
            let valueToUpdate = ""
            if( name == "from_emirate"){
                from_emirate = item.value;
                to_emirate = this.state.to_emirate.value || '';
                valueToUpdate = "from_area";
            }
            if( name == "to_emirate"){
                from_emirate = this.state.from_emirate.value || '';
                to_emirate = item.value;
                valueToUpdate = "to_area";
            }
            var transport_fee = 0;
            if(from_emirate!="" && to_emirate!="") {
                transport_fee = this.getTransportationCharges(from_emirate, to_emirate);
            }

            document.getElementsByName(valueToUpdate)[0].value = "";
            this.setState({
                [name]: item,
                [valueToUpdate]:'',
                transportation_fees: transport_fee
            });
        }
        else if( name == "packages"){
            this.setState({
                [name]: item
            });
            this.calculatePrice(item)
        }
        else{
            this.setState({
                [name]: item
            });
        }

    }
    handleInputChange(name, value){
        this.setState({
            [name]: value
        });
    }
    handleNoOfUnits(name, value){
        this.setState({
            number_of_units: value
        });
    }
    movingRequest(){
        var data_values = this.props.data_values;

        var service_constants = allServiceConstant;

        var move_type = [
            {id: 1, value: "full_move", label: "Moving my full home"},
            {id: 2, value: "small_move", label: "Moving a few items"}
        ];

        var moving_size = [
            {id: 1, rooms: 0, homeType:"Apartment", value: "Studio Apartment", label: "Studio Apartment", summeryLabel:"Apartment - STUDIO"},
            {id: 2, rooms: 1, homeType:"Apartment", value: "1 BR Apartment", label: "1 Bedroom Apartment", summeryLabel:"Apartment - 1 BR"},
            {id: 3, rooms: 2, homeType:"Apartment", value: "2 BR Apartment", label: "2 Bedroom Apartment", summeryLabel:"Apartment - 2 BR"},
            {id: 4, rooms: 3, homeType:"Apartment", value: "3 BR Apartment", label: "3 Bedroom Apartment", summeryLabel:"Apartment - 3 BR"},
            {id: 5, rooms: 4, homeType:"Apartment", value: "4 BR Apartment", label: "4 Bedroom Apartment", summeryLabel:"Apartment - 4 BR"},
            {id: 6, rooms: 2, homeType:"Villa", value: "2 BR Villa", label: "2 Bedroom Villa", summeryLabel:"Villa - 2 BR"},
            {id: 7, rooms: 3, homeType:"Villa", value: "3 BR Villa", label: "3 Bedroom Villa", summeryLabel:"Villa - 3 BR"},
            {id: 8, rooms: 4, homeType:"Villa", value: "4 BR Villa", label: "4 Bedroom Villa", summeryLabel:"Villa - 4 BR"},
            {id: 9, rooms: 5, homeType:"Villa", value: "5 BR Villa", label: "5 Bedroom Villa", summeryLabel:"Villa - 5 BR"}
        ];

        var move_type_value = this.state.typeOfMove;

        var moving_size_value = this.state.moving_size;

        var from_emirate_value = this.state.from_emirate;

        var from_area_value = this.state.from_area;

        var from_address_value = this.state.from_address;

        var to_emirate_value = this.state.to_emirate;

        var to_area_value = this.state.to_area;

        var to_address_value = this.state.to_address;

        var details_value = this.state.details;

        var items_list_value = this.state.item_lists;

        var booking_date_value = this.state.booking_date;


        //other_services
        var currentStepClass = this.currentStep() === 1 ? '' : "d-none";

        var allCities = locationHelper.fetchCities();

        var cityOptions = [];

        allCities.map(function(item){
            let value = commonHelper.slugify(item.label);
            cityOptions.push({id: item.id, value: value, label: item.label});
        })

        var from_city_id = from_emirate_value !="" && Object.keys(from_emirate_value).length ? from_emirate_value.id : locationHelper.getCurrentCityId();

        var to_city_id = to_emirate_value !="" && Object.keys(to_emirate_value).length ? to_emirate_value.id : locationHelper.getCurrentCityId();

        var from_areas =  locationHelper.getAreasByCity(from_city_id);

        var to_areas =  locationHelper.getAreasByCity(to_city_id);
        
        return (
            <div id="section-request-form" className={currentStepClass}>
                <section>
                    <FormFieldsTitle title="What is the type of your move?" />
                    <CheckRadioBoxInput 
                    InputType="radio" name="typeOfMove" 
                    inputValue={move_type_value} 
                    items={move_type} 
                    onInputChange={this.handleCustomChange} 
                     childClass="col-12 col-md-4 mb-3 d-flex" />
                </section>
                { move_type_value.value == "full_move" ? (<section>
                        <FormFieldsTitle title="Describe your home" />
                        <div className="row mb-4">
                            <div className="col-12 col-md-6 mb-3">
                                <SelectInput name="moving_size" inputValue={moving_size_value} label="Select Size" options={moving_size} onInputChange={this.handleCustomChange} />
                            </div>
                        </div>
                    </section>) : (<section>
                        <FormFieldsTitle title="Please list the items that you need to move" />
                        <div className="row mb-4">
                            <div className="col mb-3">
                                <TextareaInput name="item_lists" inputValue={items_list_value} placeholder="Please describe the job you need to have done." validationClasses="required" onInputChange={this.handleInputChange}  />
                            </div>
                        </div>
                    </section>)
                }
                <section>
                    <FormFieldsTitle title="Where are you moving from?"/>
                    <div className="row mb-4">
                        <div className="col-12 col-md-4 mb-3">
                            <SelectInput name="from_emirate" inputValue={from_emirate_value} label="City" options={cityOptions} onInputChange={this.handleCustomChange} />
                        </div>
                        <div className="col-12 col-md-4 mb-3">
                            <AreaSuggestionInput 
                            allowNonArea = {true} 
                            InputType="text" 
                            name="from_area" 
                            areas={from_areas} 
                            inputValue={from_area_value} 
                            label="Area" 
                            onInputChange={this.handleInputChange} 
                            validationClasses="required" />
                        </div>
                        <div className="col-12 col-md-4 mb-3">
                            <TextFloatInput InputType="text" name="from_address" inputValue={from_address_value} label="Building or Street no" onInputChange={this.handleInputChange} validationClasses="required" />
                        </div>
                    </div>
                </section>
                <section>
                    <FormFieldsTitle title="And where are you moving to?"/>
                    <div className="row mb-4">
                        <div className="col-12 col-md-4 mb-3">
                            <SelectInput name="to_emirate" inputValue={to_emirate_value} label="City" options={cityOptions} onInputChange={this.handleCustomChange} />
                        </div>
                        <div className="col-12 col-md-4 mb-3">
                            <AreaSuggestionInput allowNonArea = {true} InputType="text" name="to_area" areas={to_areas} inputValue={to_area_value} label="Area" onInputChange={this.handleInputChange} validationClasses="required" />
                        </div>
                        <div className="col-12 col-md-4 mb-3">
                            <TextFloatInput InputType="text" name="to_address" inputValue={to_address_value} label="Building or Street no" onInputChange={this.handleInputChange} validationClasses="required" />
                        </div>
                    </div>
                </section>

                <section>
                    <FormFieldsTitle title="When do you need to move?" />
                    <div className="row mb-4">
                        <DatePick 
                        inputValue = {this.state.booking_date}
                        name="booking_date" 
                        onInputChange={ this.handleCustomChange } 
                        disableFriday ={ false } 
                        validationClasses="required" />
                    </div>
                </section>

                <section>
                    <FormFieldsTitle title="Tell us about the job, and share any special instructions" />
                    <div className="row mb-4">
                        <div className="col mb-3">
                            <TextareaInput name="details" inputValue={this.state.details} placeholder="Please describe the job you need to have done." onInputChange={this.handleInputChange}  />
                        </div>
                    </div>
                </section>

                <section>
                    <BookNextStep total={this.state.total} title="Contact Details" moveNext={this.moveNext} toStep="2" setModal={this.mobileSummary}/>
                </section>
            </div>
        )
    }
    ContactDetails(){
        var cityOptions = locationHelper.getLocationByName(current_city);
        var currentStepClass = this.currentStep() === 2 ? '' : "d-none";
        //var currentStepClass =  ''
        var isLocationDetailShow = false;

        var contact_details = {
            input_email:this.state.input_email,
            input_phone:this.state.input_phone,
            input_name:this.state.input_name,
            input_last_name:this.state.input_last_name
        }

        if(isLocationDetailShow){
            contact_details.input_address_city = this.state.input_address_city;
            contact_details.input_address_area = this.state.input_address_area;
            contact_details.input_address_area_building_name = this.state.input_address_area_building_name;
            contact_details.input_address_area_building_apartment = this.state.input_address_area_building_apartment;
        }

        var userProfile =  this.props.userProfile;

        return (
            <div id="personal-information-form" className={currentStepClass}>
                <ContactDetailsStep
                    cityOptions={cityOptions}
                    userProfile = {userProfile}
                    signInDetails={this.props.signInDetails}
                    isLocationDetailShow={isLocationDetailShow}
                    contact_details={contact_details}
                    handleDropDownChange={this.handleInputChange}
                    handleInputChange={this.handleInputChange}
                    mobileSummary={this.mobileSummary}
                    moveNext={this.moveNext}
                    toStep={3}
                    isBooking={true}
                    buttonNextStepText = "Select Package"
                    total={this.state.total}
                    currentCity = {this.props.current_city}
                />
            </div>
        )
    }
    getTransportationCharges(from, to){
        let transportation_packages = transportationCharges;

        let transportation_charges = typeof transportation_packages[from][to] != "undefined" ? parseInt( transportation_packages[from][to] ) : 0;

        return transportation_charges;
    }
    getPackagePrices(homeType, rooms ){
        let newPackage = [];

        let transportation_fees = this.state.transportation_fees;

        let transportation_packages = this.state.transportation_fees;

        if(typeof homeType != "undefined" && typeof rooms != "undefined") {

            var packages = full_move_package.packages;

            Object.keys(packages).forEach(function (key) {
                var prices = typeof packages[key]["pricing"][homeType][rooms] != "undefined" ? packages[key]["pricing"][homeType][rooms] : 0;
                if(prices != 0){
                    //delete packages[key]["pricing"];
                    packages[key]["new_pricing"] = prices;
                    newPackage.push(packages[key]);
                }
            });
        }else{
            var packages = small_move_package.packages;
            Object.keys(packages).forEach(function (key) {
                var prices = typeof packages[key]["startingPrice"] != "undefined" ? packages[key]["startingPrice"] : 0;
                if(prices != 0){
                    packages[key]["new_pricing"] = prices;
                    newPackage.push(packages[key]);
                }
            });
        }

        // console.log("newPackage", newPackage);

        return newPackage;
    }
    Packages(packages, transport_fee){
        var selectedPackage = this.state.packages;
        var need_handyman = this.state.need_handyman;
        var packageError = this.state.packageError;
        var currentStepClass = this.currentStep() === 3 ? '' : "d-none";
        var need_handyman_opt = [
            {id:1,value:"Yes",label:"Yes"},
            {id:2,value:"No",label:"No"}
        ]

        return (<div id="section-moving-package" className={currentStepClass}>
            <FormTitleDescription title="Please choose the right moving package" desc="Choose the type of mover and budget that suits you. Book now and you only pay when the service is complete" />
            <PackageBox 
                packageError = {packageError} 
                items={packages} name="packages" 
                inputValue={selectedPackage} 
                onInputChange={this.handleCustomChange} 
                transport_fee={transport_fee} 
                validationClasses="radio-required"
                typeOfPackage = "movingBooking" />

            <section>
                <FormTitleDescription title="Do you also need a handyman?" desc={"Handyman is at an additional charge of "+currentCurrency+" 150/hour"} />
                <CheckRadioBoxInput InputType="radio" name="need_handyman" inputValue={need_handyman} items={need_handyman_opt}  childClass="col-6 col-sm-4 mb-3 d-flex" onInputChange={this.handleCustomChange}/>
            </section>
            <section>
                <BookNextStep total={this.state.total} title="Pay on delivery" noNextStep={false} moveNext={this.moveNext} toStep="4" setModal={this.mobileSummary}/>
            </section>
        </div>);
    }
    mobileSummary(boolVal){
        this.setState({
            showMobileSummary: boolVal
        })
    }
    componentWillReceiveProps (newProps) {

        if (status == null) {
            if (newProps.softSafeBookingId !== this.props.softSafeBookingId) {
                // console.log("callling Here");
                var payment = this.state.payment;
                if (payment == "") {
                    payment = "cash";
                }
                this.setState({
                    payment: payment,
                    softSafeBookingId: newProps.softSafeBookingId
                })
            }
        }
        
    }
    componentDidUpdate(prevProps, prevState) {
        var {voucherCode, booking_date, price} = this.state;
        
        if( (prevState.voucherCode !== voucherCode || prevState.booking_date !== booking_date || prevState.price !== price) && ( voucherCode != "" ) ){
            this.handleVoucherChange(voucherCode, price);
        }
    }
    updateTotal(res){
        var response = res;
        var discountData = response.data;
        var currentTotal = response.currentTotal;
        var taxChargesTotal = response.taxChargesTotal;
        var total = response.total;

        this.setState({
            price: currentTotal,
            total: total,
            subtotal : currentTotal,
            vat:taxChargesTotal,
            discountData: discountData
        });
    }
    handleVoucherChange(value, new_price = 0) {

        var couponValue = value.trim();

        this.setState({
            discountData: "load"
        })

        var price = this.state.price;

        if(new_price != 0 ){
            price = new_price;
        }

        if (typeof couponValue == 'undefined' || couponValue.length == 0) {
            var response = BookingVoucherify.error("Please provide coupon.");
            this.setState({
                discountData: response.data
            });
            //this.calculatePrice("reset_coupon", true);
        } else if (couponValue.trim().length < 3) { // minimum length of a promo code is more than 3 characters, so need to go to server if less than that
            var response = BookingVoucherify.error("Promotion code is invalid");
            this.setState({
                discountData: response.data
            })
            //this.calculatePrice("reset_coupon", true);
        }
        else if(price == 0){
            var response = BookingVoucherify.error("Please select the package");
            this.setState({
                discountData: response.data
            });
        }
        else {
            let {booking_date,discountData,subtotal, vat, payment, price} = this.state;

            var rules = BookingVoucherify.generateRules( couponValue, price, allServiceConstant.SERVICE_LOCAL_SMALL_MOVE, 0, booking_date, current_city);

            BookingVoucherify.validate(couponValue, rules).then((response)=> {
                var res = response;
                if(this.state.voucherCode != "" || (this.state.voucherCode == CREDITCARD_COUPON && this.state.payment == "credit")){
                    this.updateTotal(res);
                }
            });
        }
    }
    render(){
        var showPromo = true;
        var package_opt = this.state.package_options;
        var transport_fee = this.state.transportation_fees;
        var packages = this.Packages(package_opt, transport_fee);
        const items = this.sumarryItems();

        var selectedPackage = this.state.packages;

        /*if(typeof selectedPackage == "object" && Object.keys(selectedPackage).length) {
            showPromo = true
        }*/

        const showPrice = this.currentStep() == '3' && total != 0 ? true : false;

        var total = this.state.total;

        var discountData = this.state.discountData;

        var booking_date = this.state.booking_date;

        var isLoading =  this.state.loader || this.props.loader;

        var bookingSummary = <BookingSummary
            items={items}
            showPromo={showPromo}
            showPrice={showPrice}
            total={total}
            booking_date={booking_date}
            discountData={discountData}
            couponValue={this.state.voucherCode}
            handleCouponChange={this.handleInputChange}
            showMobileSummary={this.state.showMobileSummary}
            setModal={this.mobileSummary}
            formData={this.state}
            updateTotal={this.updateTotal}
            isLW={true}
        />;
        if(isLoading) {
            return (
                <React.Fragment>
                    <div className="col-lg-8">
                        <main loader={ isLoading ? "show" : "hide"}><Loader/></main>
                    </div>
                    <div className="col-md-3 ml-auto">
                        {bookingSummary}
                    </div>
                </React.Fragment>
            );

        }else {

            return (
                <React.Fragment>
                    <div className="col-lg-8">
                        {this.movingRequest()}
                        {this.ContactDetails("no")}
                        {packages}
                    </div>
                    <div className="col-md-3 ml-auto">
                        {bookingSummary}
                    </div>
                </React.Fragment>
            )
        }
    }
}

function mapStateToProps(state){
    return {
        pricePlan: state.PricePlan,
        myCreditCardsData: state.myCreditCardsData,
        currentCity: state.currentCity,
        lang: state.lang
    }
}


export default withCookies(connect(mapStateToProps)(BookMovingPage));