import React from "react";
import FormFieldsTitle from "../../../components/FormFieldsTitle";
import CheckRadioBoxInput from "../../../components/Form_Fields/CheckRadioBoxInput";
import CheckRadioBoxInputWithPrice from "../../../components/Form_Fields/CheckRadioBoxInputWithPrice";
import DatePick from "../../../components/Form_Fields/DatePick";
import TextareaInput from "../../../components/Form_Fields/TextareaInput";
import TextFloatInput from "../../../components/Form_Fields/TextFloatInput";
import NewBookingSummary from "../../../components/Form_Fields/NewBookingSummary";
import BookNextStep from "../../../components/Form_Fields/BookNextStep";
import FormTitleDescription from "../../../components/FormTitleDescription";
import ContactDetailsStep from "../../../components/ContactDetailsStep";
import stringConstants from '../../../constants/stringConstants';
import {
    scrollToTop,
    getPricer,
    zendeskChatBox
} from "../../../actions";
import locationHelper from "../../../helpers/locationHelper";
import commonHelper from "../../../helpers/commonHelper";
import TaxCalculator from "./TaxCalculator";
import { withCookies } from "react-cookie";
import { connect } from "react-redux";
import Loader from "../../../components/Loader";
import GeneralModal from "../../../components/GeneralModal";

let typeofHomeDataValues = {};
let dataValues = {};
let allServiceConstant = {};
let URLConstant = {};
let currentCurrency = "";
let current_city = "dubai";
var discountData = "";

var additionRoomCount = 0;

class BookPaintingPage extends React.Component {

    constructor(props) {
        super(props);
        //console.log("props.constructor.setUserArea", props.setUserArea);
        this.state = {
            typeOfPainting: 0,
            homeType: "",
            numberOfUnitsOptions: [],
            numberOfUnits: "",
            fieldFurnished: "",
            fieldCeilingsPainted: "",
            colorToBePainted: "",
            colorAreNowOnYourWalls: "",
            additionalRooms: [],
            booking_date: '',
            input_email: props.userProfile.email,
            input_phone: props.userProfile.address ? props.userProfile.address.phoneNumber : '',
            input_name: props.userProfile.customerFirstName,
            input_last_name: props.userProfile.customerLastName,
            input_address_city: '',
            input_address_area: props.setUserArea,
            input_address_area_building_name: props.userProfile.address ? props.userProfile.address.building : '',
            input_address_area_building_apartment: props.userProfile.address ? props.userProfile.address.apartment : '',
            details: '',
            showMobileSummary: false,
            discountData: {},
            couponCode: "",
            subtotal: 0,
            vat: 0,
            totalAfterDiscount: 0,
            total: 0,
            price: 0,
            softSafeBookingId: '',
            softSafeRequestId: '',
            loader: true,
            priceLoader: false,
            noOfCellings: 0,
            itemData: [],
            payment: 'cash',
            duration: "",
            iWantDifferentColorInspectionModal: false,
            walletUsed:''

        }
        var cookieData = props.getCookieFormData();

        if(Object.keys(cookieData).length){ 
            props.removeCookie("booking_data");
            props.removeCookie("submitted_data");
            props.removeCookie("confirmation_summary");
        }
        this.ContactDetails = this.ContactDetails.bind(this);
        this.PaintingRequest = this.PaintingRequest.bind(this);
        this.handleShowMoreOptionChange = this.handleShowMoreOptionChange.bind(this);
        this.handleCustomChange = this.handleCustomChange.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleDropDownChange = this.handleDropDownChange.bind(this);
        this.handleNoOfUnits = this.handleNoOfUnits.bind(this);
        this.currentStep = this.currentStep.bind(this);
        this.moveNext = this.moveNext.bind(this);
        this.hashChangeHandler = this.hashChangeHandler.bind(this);
        this.mobileSummary = this.mobileSummary.bind(this);
        this.numberOfUnitsOptions = this.numberOfUnitsOptions.bind(this);
        this.getIWantDifferentColorModalContent = this.getIWantDifferentColorModalContent.bind(this);
        this.showOrHideModalIWantDifferentColor = this.showOrHideModalIWantDifferentColor.bind(this);
        this.calculatePrice = this.calculatePrice.bind(this);
        this.updateDataValue = this.updateDataValue.bind(this);


    }
    componentDidMount() {

        /*var typeofHomeDataValues = commonHelper.processDataValues(this.props.dataConstants["TYPE_OF_HOME_TO_PAINT"]);

        var apartmentUnitsDataValues = commonHelper.processDataValues(this.props.dataConstants["SIZE_OF_APARTMENT"]);

        var villaUnitsDataValues = commonHelper.processDataValues(this.props.dataConstants["SIZE_OF_VILLA"]);

        var colorToBePaintedDataValues = commonHelper.processDataValues(this.props.dataConstants["WALL_COLORS"]);

        var fieldFurnishedDataValues = commonHelper.processDataValues(this.props.dataConstants["HOME_FURNISHED"]);

        var fieldCeilingsPaintedDataValues = commonHelper.processDataValues(this.props.dataConstants["NEED_CEILINGS_PAINTED"]);

        var additionalRoomsDataValues = commonHelper.processDataValues(this.props.dataConstants["ADDITIONAL_ROOMS"]);

        var typeOfPaintingDataValues = commonHelper.processDataValues(this.props.dataConstants["TYPE_OF_PAINTING"]);

        var allDataValues = {};

        currentCurrency = locationHelper.getCurrentCurrency();
        
        allDataValues = Object.assign(
            typeofHomeDataValues,
            typeOfPaintingDataValues,
            apartmentUnitsDataValues,
            villaUnitsDataValues,
            colorToBePaintedDataValues,
            fieldFurnishedDataValues,
            fieldCeilingsPaintedDataValues,
            additionalRoomsDataValues,
            allDataValues
        );

        dataValues = allDataValues;*/

        /*this.updateDataValue();

        this.numberOfUnitsOptions();*/
        

        var cityOptions = locationHelper.getLocationByName(current_city);
        var areaValue = this.props.setUserArea != "" ? this.props.setUserArea : "";
        this.setState({
            input_address_city: cityOptions,
            input_address_area: areaValue
        });
        URLConstant = this.props.url_constants;

        current_city = this.props.current_city;

        zendeskChatBox();

        window.addEventListener("hashchange", this.hashChangeHandler, false);
    }
    componentWillUnmount() {
        window.removeEventListener("hashchange", this.hashChangeHandler, false);
    }
    sumarryItems() {
        const { homeType, numberOfUnits, fieldFurnished, colorToBePainted, colorAreNowOnYourWalls, booking_date, subtotal, vat, itemData, duration } = this.state;

        /*var items = [
            { label: 'Home Type', value: homeType.label },
            { label: 'Units', value: numberOfUnits.label },
            { label: 'Color Needed', value: colorToBePainted.label },
            { label: 'Current Color', value: colorAreNowOnYourWalls.label },
            { label: 'Furnished', value: fieldFurnished.label },
            { label: 'Date', value: booking_date },
        ];*/
        
        let sumarryItems = []; 
        
        let jobDetails = [];

        if(typeof homeType.label != "undefined"){
            let homeLbl = homeType.label;
            
            homeLbl += typeof numberOfUnits.label != "undefined" ?  " - " + numberOfUnits.label : "";
            
            jobDetails.push(homeLbl);
        }

        /*if(typeof colorToBePainted.label != "undefined" && typeof colorAreNowOnYourWalls.label != "undefined"){
            let colorsLbl = <span className="text-warning">{colorAreNowOnYourWalls.label + " - "+ colorToBePainted.label}</span>;
            jobDetails.push(homeLbl);
        }

        if(typeof fieldFurnished.label != "undefined"){
            jobDetails.push(fieldFurnished.label);
        }*/

        if (duration != "") {
            jobDetails.push('Duration '+duration);
        }

        var items = [
            {label: 'Time', value: booking_date},
            {label: 'Details', value: jobDetails, conClass: 'col-12 mb-4'}
        ];
        
        if (this.props.userProfile && this.props.userProfile.userWallet) {
            const walletAmount = this.props.userProfile.userWallet.totalAmount;
            const walletCurrency = this.props.userProfile.userWallet.currency.code;
            
            if ( this.props.userProfile.userWallet.totalAmount > 0 && walletCurrency === currentCurrency && walletAmount > 0) {
            
                items.push({ label: stringConstants.WALLET, value: `-  ${walletCurrency}  ${walletAmount}` });
            }
        }
        
        return items;
    }
    hashChangeHandler() {
        let hashVal = window.location.hash;
        let step;
        hashVal.length ? step = parseInt(window.location.hash.replace('#', '')) : step = 1;
        this.props.moveNext(step);
        scrollToTop();
    }
    currentStep() {
        return this.props.formCurrentStep;
    }
    moveNext(step) {
        this.props.moveNextStep(step, this.state);
    }
    handleShowMoreOptionChange() {
        this.setState({
            show_more_option: !this.state.show_more_option
        });
    }
    handleCustomChange(name, value) {
        this.setState({
            booking_time: value
        });
    }
    handleInputChange(name, value) {
        if (name == "colorToBePainted" && value == "i_want_diff_color") {
            this.showOrHideModalIWantDifferentColor(true);
        } else {
            if (name == "fieldCeilingsPainted") {
                if (value.value == dataValues.DATA_VALUE_CEILINGS_PAINT_YES) {
                    this.setState({
                        noOfCellings: 1
                    });
                } else {
                    this.setState({
                        noOfCellings: 0
                    });
                }
            }
            if (name == "additionalRooms") {
                this.calculatePrice();
            }
            this.setState({
                [name]: value
            });
        }
    }
    handleNoOfUnits(name, value) {
        this.setState({
            numberOfUnits: value
        });

        //console.log(name, value);
    }
    handleDropDownChange(name, value) {
        console.log(name, value);
    }
    numberOfUnitsOptions() {

        var { homeType } = this.state;

        var numberOfUnitsApartment = [
            { id: 1, value: dataValues.DATA_VALUE_STUDIO_APARTMENT, label: "Studio", price: 650, currentCurrency: currentCurrency, constant: 'STUDIO_APARTMENT' },
            { id: 2, value: dataValues.DATA_VALUE_1_BR_APARTMENT, label: "1 BR", price: 900, currentCurrency: currentCurrency, constant: '1_BR_APARTMENT' },
            { id: 3, value: dataValues.DATA_VALUE_2_BR_APARTMENT, label: "2 BR", price: 1100, currentCurrency: currentCurrency, constant: '2_BR_APARTMENT' },
            { id: 4, value: dataValues.DATA_VALUE_3_BR_APARTMENT, label: "3 BR", price: 1450, currentCurrency: currentCurrency, constant: '3_BR_APARTMENT' },
            { id: 5, value: dataValues.DATA_VALUE_4_BR_APARTMENT, label: "4 BR", price: 2000, currentCurrency: currentCurrency, constant: '4_BR_APARTMENT' },
            { id: 6, value: dataValues.DATA_VALUE_5_BR_APARTMENT, label: "5 BR", price: 2650, currentCurrency: currentCurrency, constant: '5_BR_APARTMENT' }
        ];

        var numberOfUnitsVilla = [
            { id: 1, value: dataValues.DATA_VALUE_1_BR_VILLA, label: "1 BR", price: 1650, currentCurrency: currentCurrency, constant: '1_BR_VILLA' },
            { id: 2, value: dataValues.DATA_VALUE_2_BR_VILLA, label: "2 BR", price: 2250, currentCurrency: currentCurrency, constant: '2_BR_VILLA' },
            { id: 3, value: dataValues.DATA_VALUE_3_BR_VILLA, label: "3 BR", price: 2800, currentCurrency: currentCurrency, constant: '3_BR_VILLA' },
            { id: 4, value: dataValues.DATA_VALUE_4_BR_VILLA, label: "4 BR", price: 3350, currentCurrency: currentCurrency, constant: '4_BR_VILLA' },
            { id: 5, value: dataValues.DATA_VALUE_5_BR_VILLA, label: "5 BR", price: 4050, currentCurrency: currentCurrency, constant: '5_BR_VILLA' }
        ];

        var homeUnitOptions = [];

        if (typeof homeType.value != "undefined") {
            homeUnitOptions = homeType.value == dataValues.DATA_VALUE_HOME_TYPE_VILLA ? numberOfUnitsVilla : numberOfUnitsApartment;
        } else {
            homeUnitOptions = numberOfUnitsApartment;
        }

        this.setState({
            numberOfUnitsOptions: homeUnitOptions
        });
    }
    PaintingRequest() {
        var homeType = [
            { id: 1, value: dataValues.DATA_VALUE_HOME_TYPE_APARTMENT, label: "Apartment", constant: 'HOME_TYPE_APARTMENT' },
            { id: 2, value: dataValues.DATA_VALUE_HOME_TYPE_VILLA, label: "Villa", constant: 'HOME_TYPE_VILLA' }
        ];

        var colorToBePainted = [
            { id: 1, value: dataValues.DATA_VALUE_WALL_COLOR_WHITE, label: "White", constant: 'WALL_COLOR_WHITE' },
            { id: 2, value: dataValues.DATA_VALUE_WALL_COLOR_OFF_WHITE, label: "Off-white", constant: 'WALL_COLOR_OFF_WHITE' },
            /*{id: 3, value: "i_want_diff_color", label: "I want a different color" ,isQuotes:true, quotesLink: "../"+URLConstant.PAINTERS_PAGE_URI+"/journey1"}*/
        ];

        var colorAreNowOnYourWalls = [
            { id: 1, value: dataValues.DATA_VALUE_WALL_COLOR_WHITE, label: "White", constant: 'WALL_COLOR_WHITE' },
            { id: 2, value: dataValues.DATA_VALUE_WALL_COLOR_OFF_WHITE, label: "Off-white", constant: 'WALL_COLOR_OFF_WHITE' },
            { id: 3, value: dataValues.DATA_VALUE_WALL_COLOR_CUSTOM, label: "Color", constant: 'WALL_COLOR_CUSTOM' },
            { id: 4, value: dataValues.DATA_VALUE_WALL_COLOR_MIXED, label: "Mixed", constant: 'WALL_COLOR_MIXED' }
        ];

        var fieldFurnished = [
            { id: 1, value: dataValues.DATA_VALUE_HOME_FURNISHED_YES, label: "Furnished", constant: 'HOME_FURNISHED_YES' },
            { id: 2, value: dataValues.DATA_VALUE_HOME_FURNISHED_NO, label: "Unfurnished", constant: 'HOME_FURNISHED_NO' }
        ];

        var fieldCeilingsPainted = [
            { id: 1, value: dataValues.DATA_VALUE_CEILINGS_PAINT_YES, label: "I need ceilings painted", constant: 'CEILINGS_PAINT_YES' },
            { id: 2, value: dataValues.DATA_VALUE_CEILINGS_PAINT_NO, label: "I don't need ceilings painted", constant: 'CEILINGS_PAINT_NO' }
        ];

        var additionalRooms = [
            { id: 1, value: dataValues.DATA_VALUE_ADDITIONAL_ROOM_MAID, label: "Maid's room", constant: 'ADDITIONAL_ROOM_MAID' },
            { id: 2, value: dataValues.DATA_VALUE_ADDITIONAL_ROOM_STUDY, label: "Study room", constant: 'ADDITIONAL_ROOM_STUDY' },
            { id: 3, value: dataValues.DATA_VALUE_ADDITIONAL_ROOM_STORE, label: "Store room", constant: 'ADDITIONAL_ROOM_STORE' },
            { id: 4, value: dataValues.DATA_VALUE_ADDITIONAL_ROOM_BALCONY, label: "Balcony", constant: 'ADDITIONAL_ROOM_BALCONY' }
        ]

        var homeTypeValue = this.state.homeType;

        var numberOfUnitsValue = this.state.numberOfUnits;

        var colorToBePaintedValue = this.state.colorToBePainted;

        var colorAreNowOnYourWallsValue = this.state.colorAreNowOnYourWalls;

        var fieldFurnishedValue = this.state.fieldFurnished;

        var fieldCeilingsPaintedValue = this.state.fieldCeilingsPainted;

        var additionalRoomsValue = this.state.additionalRooms;

        var numberOfUnitsItem = typeof homeTypeValue.value != ""

        //other_services
        var currentStepClass = this.currentStep() === 1 ? '' : "d-none";


        return (
            <div id="section-request-form" className={currentStepClass}>
                <section>
                    <FormFieldsTitle title="Describe your home" />
                    <CheckRadioBoxInput
                        InputType="radio"
                        name="homeType"
                        inputValue={homeTypeValue}
                        items={homeType}
                        onInputChange={this.handleInputChange}
                        parentClass="row mb-2"
                        childClass="col-12 col-md-4 mb-2 d-flex"
                        validationClasses="radio-required"
                        validationMessage="Please describe your home type."
                    />
                </section>
                <section>
                    <CheckRadioBoxInputWithPrice
                        InputType="radio"
                        name="numberOfUnitsApartment"
                        inputValue={numberOfUnitsValue}
                        items={this.state.numberOfUnitsOptions}
                        onInputChange={this.handleNoOfUnits}
                        childClass="col-6 col-sm-2 mb-3 d-flex"
                        parentClass="row mb-1"
                        validationClasses="radio-required"
                        validationMessage="Please select your home size."
                    />
                </section>
                <section>
                    <FormFieldsTitle title="What color do you want your home painted in?" />
                    <CheckRadioBoxInput
                        InputType="radio"
                        name="colorToBePainted"
                        inputValue={colorToBePaintedValue}
                        items={colorToBePainted}
                        onInputChange={this.handleInputChange}
                        parentClass="row "
                        childClass="col-12 col-md-3 mb-2 d-flex"
                        validationClasses="radio-required"
                        validationMessage="Please describe the paint color you would like."
                    />
                    <div className="want-different-color mb-4 ml-2">
                        <a href="#" onClick={() => this.showOrHideModalIWantDifferentColor(true)} id="select-different-color">
                            I want a different color
                     </a>
                    </div>
                </section>
                <section>
                    <FormFieldsTitle title="What color are your walls now?" />
                    <CheckRadioBoxInput InputType="radio"
                        name="colorAreNowOnYourWalls"
                        inputValue={colorAreNowOnYourWallsValue}
                        items={colorAreNowOnYourWalls}
                        onInputChange={this.handleInputChange}
                        parentClass="row mb-2"
                        childClass="col-12 col-md-3 mb-2 d-flex"
                        validationClasses="radio-required"
                        validationMessage="Please describe the current color of your walls"
                        parentClass="row mb-4"
                    />
                </section>
                <section>
                    <FormTitleDescription title="Do you need to paint these rooms?" desc="These rooms are not included in the base price. A fee of AED 150 per room applies" />
                    <CheckRadioBoxInput
                        InputType="checkbox"
                        name="additionalRooms"
                        inputValue={additionalRoomsValue}
                        items={additionalRooms}
                        onInputChange={this.handleInputChange}
                        parentClass="row mb-4"
                        childClass="col-12 col-md-3 mb-2 d-flex" />
                </section>
                <section>
                    <FormTitleDescription title="Will your home be furnished at the time of painting?" desc="The painters will cover your furniture in plastic sheets. Additional charges apply" />
                    <CheckRadioBoxInput
                        InputType="radio"
                        name="fieldFurnished"
                        inputValue={fieldFurnishedValue}
                        items={fieldFurnished}
                        onInputChange={this.handleInputChange}
                        parentClass="row"
                        childClass="col-6 col-sm-4 mb-3 d-flex"
                        validationClasses="radio-required"
                        parentClass="row mb-2"
                        validationMessage="Please let us know if your home is furnished or not."
                    />
                </section>
                <section className="ceilings-painted">
                    <FormTitleDescription title="Do you need ceilings painted" desc="Ceiling painting costs AED 150 per ceiling" />
                    {
                        fieldCeilingsPaintedValue.value == dataValues.DATA_VALUE_CEILINGS_PAINT_YES &&
                        <div className="row mb-2"><div className="col-6 pr-2 pr-sm-3 mb-3">
                            <TextFloatInput
                                InputType="text"
                                name="noOfCellings"
                                label="No.Of Cellings"
                                inputValue={this.state.noOfCellings}
                                onInputChange={this.handleInputChange}
                                validationClasses="required"
                                validationMessage="Please enter number of ceilings"
                            /></div>
                        </div>
                    }
                    <CheckRadioBoxInput
                        InputType="radio"
                        name="fieldCeilingsPainted"
                        inputValue={fieldCeilingsPaintedValue}
                        items={fieldCeilingsPainted}
                        onInputChange={this.handleInputChange}
                        childClass="col-6 col-sm-4 mb-3 d-flex"
                        validationClasses="radio-required"
                        validationMessage="Please verify if you need ceilings painted."
                    />
                </section>
                <section>
                    <FormTitleDescription title="When do you need your home painted?" desc="Please select the date you need the service on. If there are any changes, you can inform us later!" />
                    <div className="row mb-4">
                        <DatePick name="booking_date"
                            inputValue={this.state.booking_date}
                            selectedDate={this.state.booking_date}
                            onInputChange={this.handleInputChange}
                            validationClasses="required" />
                    </div>
                </section>
                <section className="d-none">
                    <FormFieldsTitle title="Any special instructions or additional details" />
                    <div className="row mb-4">
                        <div className="col mb-3">
                            <TextareaInput name="details" placeholder="Example: You will need to bring a gate pass, I will not be home and am leaving the key under the mat, et cetera" />
                        </div>
                    </div>
                </section>
                <section>
                    <BookNextStep total={this.state.total} title="Contact Details" moveNext={this.moveNext} toStep="2" setModal={this.mobileSummary} />
                </section>
            </div>
        )
    }
    ContactDetails() {
        var cityOptions = locationHelper.getLocationByName(current_city);

        var currentStepClass = this.currentStep() === 2 ? '' : "d-none";
        //var currentStepClass =  ''
        var isLocationDetailShow = "yes";

        var contact_details = {
            input_email: this.state.input_email,
            input_phone: this.state.input_phone,
            input_name: this.state.input_name,
            input_last_name: this.state.input_last_name
        }

        if (isLocationDetailShow) {
            contact_details.input_address_city = this.state.input_address_city != "" ? this.state.input_address_city : cityOptions;
            contact_details.input_address_area = this.state.input_address_area;
            contact_details.input_address_area_building_name = this.state.input_address_area_building_name;
            contact_details.input_address_area_building_apartment = this.state.input_address_area_building_apartment;
        }

        var userProfile = this.props.userProfile;

        //console.log("cityOptions", contact_details.input_address_city, cityOptions);

        return (
            <div id="personal-information-form" className={currentStepClass}>
                <ContactDetailsStep
                    cityOptions={cityOptions}
                    userProfile={userProfile}
                    signInDetails={this.props.signInDetails}
                    isLocationDetailShow={isLocationDetailShow}
                    contact_details={contact_details}
                    handleDropDownChange={this.handleInputChange}
                    handleInputChange={this.handleInputChange}
                    mobileSummary={this.mobileSummary}
                    moveNext={this.moveNext}
                    toStep={3}
                    isBooking={true}
                    noNextStep={false} 
                    total={this.state.total}
                    buttonNextStepText = "Looking forward to serving you"
                    currentCity = {this.props.current_city}
                />
            </div>
        )
    }
    calculatePrice() {

        var {
            homeType,
            numberOfUnits,
            fieldFurnished,
            fieldCeilingsPainted,
            colorToBePainted,
            colorAreNowOnYourWalls,
            additionalRooms,
            booking_date,
            noOfCellings
        } = this.state;

        var user_token = (typeof this.props.signInDetails != "undefined" && typeof this.props.signInDetails.access_token  != "undefined")
         ? this.props.signInDetails.access_token : "";

        var homeTypeValue = typeof homeType == "object" ? homeType.constant : '';

        var numberOfUnitsValue = typeof numberOfUnits == "object" ? numberOfUnits.constant : '';

        var fieldFurnishedValue = typeof fieldFurnished == "object" ? fieldFurnished.constant : "HOME_FURNISHED_NO";

        var fieldCeilingsPaintedValue = typeof fieldCeilingsPainted == "object" ? fieldCeilingsPainted.constant : "CEILINGS_PAINT_NO";

        var colorToBePaintedValue = typeof colorToBePainted == "object" ? colorToBePainted.constant : '';

        var colorAreNowOnYourWallsValue = typeof colorAreNowOnYourWalls == "object" ? colorAreNowOnYourWalls.constant : '';

        var additionalRoomsValue = additionalRooms;

        var packagePrice = typeof numberOfUnits == "object" && typeof numberOfUnits.price != "undefined" ? numberOfUnits.price : 0;

        var total_price = 0;

        var taxRate = 0;

        if (homeTypeValue != "" && numberOfUnitsValue != "") {

            this.setState({
                priceLoader: true
            })

            var payloadParams = {
                "type": "TYPE_OF_PAINT_ALL_ROOMS",
                "paintingBookingPriceModel": {
                    "color": {
                        "now": colorAreNowOnYourWallsValue,
                        "after": colorToBePaintedValue,
                        "needPaintMaterial": "PAINT_NEEDED_NO"
                    },
                    "isFurnishedRequired": fieldFurnishedValue,
                    "ceilings": {
                        "isPaintRequired": fieldCeilingsPaintedValue,
                        "count": noOfCellings,
                        "isHigh": "HIGH_CEILINGS_NO"
                    },
                    "waterDamage": {
                        "isRepairRequired": "WATER_DAMAGE_NO",
                        "count": 0
                    },
                    "sizeOfHome": numberOfUnitsValue,
                    "numberOfWalls": 0
                }
            };
            var moreRooms = [];


            if (additionalRoomsValue.length) {

                additionalRoomsValue.map((item) => {
                    moreRooms.push(item.constant);
                })
                payloadParams["paintingBookingPriceModel"]["additionalRooms"] = moreRooms;
            }

            //payloadParams = {"type":"TYPE_OF_PAINT_ALL_ROOMS","paintingBookingPriceModel":{"color":{"now":"","after":"WALL_COLOR_WHITE","needPaintMaterial":"PAINT_NEEDED_NO"},"isFurnishedRequired":"HOME_FURNISHED_NO","ceilings":{"isPaintRequired":"CEILINGS_PAINT_NO","count":0,"isHigh":"HIGH_CEILINGS_NO"},"waterDamage":{"isRepairRequired":"WATER_DAMAGE_NO","count":0},"sizeOfHome":"STUDIO_APARTMENT","numberOfWalls":0}}

            var isCustomerLoggedIn = user_token == "" ? false : true;

            var itemData = [];

            var additionalRoomsMap = {
                'ADDITIONAL_ROOM_MAID': "Maid's room",
                'ADDITIONAL_ROOM_STUDY': "Study room",
                'ADDITIONAL_ROOM_STORE': "Store room",
                'ADDITIONAL_ROOM_BALCONY': "Balcony"
            };

            getPricer(payloadParams, isCustomerLoggedIn, user_token).then((response) => {

                var priceData = JSON.parse(response.price);

                if (priceData.success) {
                    total_price = parseInt(priceData.total);

                    var roomsCount = Object.keys(priceData.rooms).length;


                    if (priceData.sizeOfHome != null) {
                        var sizeOfHome = priceData.sizeOfHome;

                        var typeOfHomeLabel = numberOfUnits.label + " " + homeType.label;

                        itemData.push({ label: typeOfHomeLabel, value: currentCurrency + " " + sizeOfHome, classNme:'text-warning'});

                    }

                    if (priceData.colorPrice != null) {
                        itemData.push({
                            label: colorAreNowOnYourWalls.label + " to " + colorToBePainted.label,
                            value: currentCurrency + " " + priceData.colorPrice,
                            classNme:'text-warning'
                        });
                    }
                    if (roomsCount != 0) {
                        var rooomData = priceData.rooms;
                        Object.keys(rooomData).map((item) => {
                            itemData.push({ label: additionalRoomsMap[item], value: currentCurrency + " " + rooomData[item],classNme:'text-warning'});
                        })

                    }
                    if (priceData.furnished != null && priceData.furnished != "0") {
                        itemData.push({ label: "Furnished", value: currentCurrency + " " + priceData.furnished,classNme:'text-warning'});
                    }

                    if (priceData.ceilings != null && priceData.ceilings != "0") {
                        itemData.push({ label: 'Ceilings', value: currentCurrency + " " + priceData.ceilings,classNme:'text-warning'});
                    }
                    let walletUsed = 0;

                    if (this.props.userProfile && this.props.userProfile.userWallet) {
                        const walletAmount = this.props.userProfile.userWallet.totalAmount;
                        const walletCurrency = this.props.userProfile.userWallet.currency.code;

                        if (walletCurrency === currentCurrency && walletAmount > 0) {
                            if (walletAmount >= total_price) {
                                walletUsed = total_price;
                                total_price = 0;
                            }
                            else if (walletAmount < total_price) {
                                walletUsed = walletAmount;
                                total_price = total_price - walletAmount;
                            }
                        }
                    }

                    var taxChargesTotal = TaxCalculator.calculateVAT(total_price, booking_date);

                    var total = total_price + taxChargesTotal;
                    
                    this.setState({
                        total: total,
                        subtotal: total_price,
                        vat: taxChargesTotal,
                        itemData: itemData,
                        duration: priceData.duration,
                        walletUsed: walletUsed

                    });

                }
                this.setState({
                    priceLoader: false
                })
            });
        } else {

            var total_price = 0;

            var taxRate = 0;

            if (packagePrice != 0) {
                total_price = parseInt(numberOfUnits.price);
            }

            var prefferedDate = booking_date;

            var taxChargesTotal = TaxCalculator.calculateVAT(total_price, prefferedDate);

            var total = total_price + taxChargesTotal;

            this.setState({
                total: total,
                subtotal: total_price,
                vat: taxChargesTotal,
                duration: ""
            });
        }

        //return returnData;


    }
    componentDidUpdate(prevProps, prevState) {

        var { homeType,
            numberOfUnitsOptions,
            numberOfUnits,
            fieldFurnished,
            fieldCeilingsPainted,
            colorToBePainted,
            colorAreNowOnYourWalls,
            additionalRooms,
            noOfCellings,
            total
        } = this.state;


        if (prevState.homeType != homeType) {
            this.numberOfUnitsOptions();
        }

        if (prevState.homeType != homeType ||
            prevState.numberOfUnits !== numberOfUnits ||
            prevState.fieldFurnished !== fieldFurnished ||
            prevState.fieldCeilingsPainted !== fieldCeilingsPainted ||
            prevState.colorToBePainted !== colorToBePainted ||
            prevState.colorAreNowOnYourWalls !== colorAreNowOnYourWalls ||
            prevState.additionalRooms !== additionalRooms ||
            prevState.noOfCellings !== noOfCellings) {
            this.calculatePrice();
        }
    }
    mobileSummary(boolVal) {
        this.setState({
            showMobileSummary: boolVal
        })
    }
    updateDataValue(dataConstants){
        var typeofHomeDataValues = commonHelper.processDataValues(dataConstants["TYPE_OF_HOME_TO_PAINT"]);

        var apartmentUnitsDataValues = commonHelper.processDataValues(dataConstants["SIZE_OF_APARTMENT"]);

        var villaUnitsDataValues = commonHelper.processDataValues(dataConstants["SIZE_OF_VILLA"]);

        var colorToBePaintedDataValues = commonHelper.processDataValues(dataConstants["WALL_COLORS"]);

        var fieldFurnishedDataValues = commonHelper.processDataValues(dataConstants["HOME_FURNISHED"]);

        var fieldCeilingsPaintedDataValues = commonHelper.processDataValues(dataConstants["NEED_CEILINGS_PAINTED"]);

        var additionalRoomsDataValues = commonHelper.processDataValues(dataConstants["ADDITIONAL_ROOMS"]);

        var typeOfPaintingDataValues = commonHelper.processDataValues(dataConstants["TYPE_OF_PAINTING"]);

        var allDataValues = {};

        currentCurrency = locationHelper.getCurrentCurrency();
        
        allDataValues = Object.assign(
            typeofHomeDataValues,
            typeOfPaintingDataValues,
            apartmentUnitsDataValues,
            villaUnitsDataValues,
            colorToBePaintedDataValues,
            fieldFurnishedDataValues,
            fieldCeilingsPaintedDataValues,
            additionalRoomsDataValues,
            allDataValues
        );

        dataValues = allDataValues;
    }
    componentWillReceiveProps(newProps) {
        if (newProps.dataConstants !== this.props.dataConstants) {
            this.updateDataValue(newProps.dataConstants);
            this.numberOfUnitsOptions();
            this.setState({
                typeOfPainting: dataValues.DATA_VALUE_TYPE_OF_PAINT_ALL_ROOMS,
                loader: false
            });
        }
        if ((newProps.formCurrentStep == 2) && (newProps.formCurrentStep !== this.props.formCurrentStep)) {
            var payment = this.state.payment;
            if (payment == "") {
                payment = "credit";
                this.setState({
                    payment: payment
                });
            }
        }
        if (newProps.setUserArea !== this.props.setUserArea) {
            //console.log("setUserArea", newProps.setUserArea)
            this.setState({
                input_address_area: newProps.setUserArea
            })
        }
    }
    showOrHideModalIWantDifferentColor(boolVal) {
        //e.preventDefault();
        this.setState({
            iWantDifferentColorInspectionModal: boolVal
        })
    }
    getIWantDifferentColorModalContent() {
        return (<div className="modal-different">
            <p className="btn-desc-text my-2">Looking to paint your home in anything but white or off-white?</p>
            <p className="btn-desc-text my-2">We can’t book you online, but we can get you FREE quotes for your custom painting job!</p>
            <div className="text-center">
                <a className="btn btn-inline-block btn-warning py-2 font-weight-bold" href={"../" + URLConstant.PAINTERS_PAGE_URI + "/journey1"}>GET A FREE QUOTE </a>
            </div>
            <div className="text-center my-2">
                <a href="#" onClick={() => this.showOrHideModalIWantDifferentColor(false)}> No thanks, take me back to booking </a>
            </div>
        </div>);
    }
    render() {

        var { itemData, total, subtotal, vat } = this.state;

        var items = this.sumarryItems(),
            sumarryItems = [];

        if(itemData.length) {
            sumarryItems = items.concat(itemData);
        } else { 
            sumarryItems = items;
        }

        if (subtotal != 0) {
            sumarryItems.push(
                {label: 'Subtotal', value: currentCurrency+" "+subtotal, classNme:'text-warning'}
            );
        }

        var {total,vat} = this.state;

        const showPromo = false;

        const showPrice = total != 0 ? true : false;

        var isLoading = this.state.loader || this.props.loader;

        var discountData = this.state.discountData;

        var booking_date = this.state.booking_date;

        var priceLoader = this.state.priceLoader;

        let {isWalletAvailable} = this.props;


        var bookingSummary = <NewBookingSummary
            items = {sumarryItems}
            showPromo = {showPromo}
            showPrice = {showPrice}
            total = {total}
            vat = {vat}
            booking_date = {booking_date}
            showMobileSummary = {this.state.showMobileSummary}
            handleCouponChange = {this.handleInputChange}
            setModal = {this.mobileSummary}
            discountData = {discountData}
            couponValue = {this.state.couponCode}
            isCoreBooking = {true}
            priceLoader = {priceLoader}
            walletAvailable={isWalletAvailable}
        />;

        if (isLoading) {
            return (
                <React.Fragment>
                    <div className="col-lg-8">
                        <main loader={isLoading ? "show" : "hide"}><Loader /></main>
                    </div>
                    <div className="col-md-3 ml-auto">
                        {bookingSummary}
                    </div>
                </React.Fragment>
            );

        } else {

            return (
                <React.Fragment>
                    <div className="col-lg-8">
                    {this.props.userProfile && this.props.userProfile.userWallet && this.props.userProfile.userWallet.currency.code === currentCurrency && this.props.userProfile.userWallet.totalAmount > 0?
                            <div className='wallet-div'> 
                                <img src={'../../../../dist/images/wallet-filled-money-tool-light.png'} className="mh-100" height="30" alt="" />
                                <span className='margin-left30' style={{verticalAlign: 'middle',fontSize: '15px'}}>{stringConstants.YOU_HAVE_TXT}
                                    <span className='wallet-amount-span'>{this.props.userProfile.userWallet.currency.code + ' ' + this.props.userProfile.userWallet.totalAmount} </span>{stringConstants.WALLET_BALANCE_TXT}
                                </span>
                            </div>
                            : null
                        }
                        {this.PaintingRequest()}
                        {this.ContactDetails()}
                    </div>
                    <div className="col-md-3 ml-auto">
                        {bookingSummary}
                    </div>
                    {this.state.iWantDifferentColorInspectionModal &&
                        <GeneralModal title="I want a different color" modalBody={this.getIWantDifferentColorModalContent()}
                            setModal={this.showOrHideModalIWantDifferentColor} />}
                </React.Fragment>

            )

        }

    }
}
function mapStateToProps(state) {
    return {
        myCreditCardsData: state.myCreditCardsData,
        userProfile: state.userProfile,
        currentCity: state.currentCity,
        lang: state.lang,

    }
}


export default withCookies(connect(mapStateToProps)(BookPaintingPage));