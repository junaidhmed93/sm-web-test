import React from "react";
import FormTitleDescription from "../../../components/FormTitleDescription";
import {withCookies} from "react-cookie";
import {connect} from "react-redux";
import { isMobile, locationConstants } from "../../../actions";
let currentCurrency = ""
class DryCleaningPrice extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isMobileView: false
        }
        const { cookies } = props;
        let {currentCityData} = props;
        currentCurrency = (typeof currentCityData != "undefined" && currentCityData.length) ? currentCityData[0].cityDto.countryDto.currencyDto.code : locationConstants.DEFAULT_CURRENCY;
    }
    componentDidMount(){
        this.setState({
            isMobileView:isMobile()
        })
    }
    render() {
        let isMobileView = this.state.isMobileView;
        /*parentClass={ isMobileView ? "row text-center" : "row" }*/
        return (<div className={isMobileView ? "learn-more-body p-2" : "learn-more-body p-5"}>
            <FormTitleDescription childClass="text-secondary mt-1 mb-0"  title="All you can wash bags"  desc="Price per bag hassle less &amp; affordable laundry services at your doorstep." />
            <div className="row mt-5 wash-bag-prices">
                <div className="col-12 col-sm-4 text-center mb-5">
                    <h3 className="text-dark text-center mb-5">Wash &amp; Fold</h3>
                    <img src="/../dist/images/laundry_bag.png" className="d-block m-auto" style={{width:'130px'}}/>
                    <div className="border d-inline-block text-primary rounded mt-5 price">
                        {currentCurrency} 49
                    </div>
                </div>
                <div className="col-12 col-sm-4 text-center mb-5">
                    <h3 className="text-dark text-center mb-5">Iron Only</h3>
                    <img src="/../dist/images/laundry_bag.png" className="d-block m-auto" style={{width:'130px'}}/>
                    <div className="border d-inline-block text-primary rounded mt-5 price">
                        {currentCurrency} 69
                    </div>
                </div>
                <div className="col-12 col-sm-4 text-center mb-5">
                    <h3 className="text-dark text-center mb-5">Wash &amp; Iron</h3>
                    <img src="/../dist/images/laundry_bag.png" className="d-block m-auto" style={{width:'130px'}}/>
                    <div className="border d-inline-block text-primary rounded mt-5 price">
                        {currentCurrency} 99
                    </div>
                </div>
            </div>
            <div className="my-4 text-secondary">
                <p className="mb-1">We will provide laundry bags. The bags measure 70 x 50 cm- equivalent to 2 laundry loads</p>
                <p className="text-primary text-left mt-0"><sup>*</sup>All prices excluding VAT.</p>
            </div>
            {/* In for when your feeling extra special!*/}
            <FormTitleDescription  parentClass={"row mt-5"} title="Individual Dry Cleaning Pricing" desc="" />
            <div className="table-responsive mt-3">
            <table className="table ">
                <tbody>
                    <tr>
                        <th className="text-primary h3 first-th">Tops</th>
                        <th className="text-secondary h3"><strong>Clean Only</strong></th>
                        <th className="text-secondary h3"><strong>Clean + Press</strong></th>
                    </tr>
                    <tr>
                        <td>Blouse <hr/></td>
                        <td>{currentCurrency} 7</td>
                        <td>{currentCurrency} 12</td>
                    </tr>
                    <tr>
                        <td>Shirt <hr/></td>
                        <td>{currentCurrency} 5</td>
                        <td>{currentCurrency} 9</td>
                    </tr>
                    <tr>
                        <td>Sweater/Pullover <hr/></td>
                        <td>{currentCurrency} 10</td>
                        <td>{currentCurrency} 15</td>
                    </tr>
                    <tr>
                        <td>T-shirt <hr/></td>
                        <td>{currentCurrency} 5</td>
                        <td>{currentCurrency} 9</td>
                    </tr>
                    <tr>
                        <td>Undershirt <hr/></td>
                        <td>{currentCurrency} 2</td>
                        <td>{currentCurrency} 4</td>
                    </tr>
                    </tbody>
                    </table>
                    <table className="table mt-5">
                    <tbody>
                    <tr>
                        <th className="text-primary h3 first-th">Bottoms</th>
                        <th className="text-secondary h3"><strong>Clean Only</strong></th>
                        <th className="text-secondary h3"><strong>Clean + Press</strong></th>
                    </tr>
                    <tr>
                        <td>Lungi <hr/></td>
                        <td>{currentCurrency} 6</td>
                        <td>{currentCurrency} 8</td>
                    </tr>
                    <tr>
                        <td>Pants <hr/></td>
                        <td>{currentCurrency} 7</td>
                        <td>{currentCurrency} 12</td>
                    </tr>
                    <tr>
                        <td>Shorts <hr/></td>
                        <td>{currentCurrency} 5</td>
                        <td>{currentCurrency} 8</td>
                    </tr>
                    <tr>
                        <td>Skirt <hr/></td>
                        <td>{currentCurrency} 8</td>
                        <td>{currentCurrency} 12</td>
                    </tr>
                    </tbody>
                    </table>
                    <table className="table mt-5">
                    <tbody>
                    <tr>
                        <th className="text-primary h3 first-th">Undergarment</th>
                        <th className="text-secondary h3"><strong>Clean Only</strong></th>
                        <th className="text-secondary h3"><strong>Clean + Press</strong></th>
                    </tr>
                    <tr>
                        <td>Bra <hr/></td>
                        <td>{currentCurrency} 3</td>
                        <td>{currentCurrency} 5</td>
                    </tr>
                    <tr>
                        <td>Socks <hr/></td>
                        <td>{currentCurrency} 2</td>
                        <td>{currentCurrency} 3</td>
                    </tr>
                    <tr>
                        <td>Underwear <hr/></td>
                        <td>{currentCurrency} 2</td>
                        <td>{currentCurrency} 4</td>
                    </tr>
                    </tbody>
                    </table>
                    <table className="table mt-5">
                    <tbody>
                    <tr>
                        <th className="text-primary h3 first-th">Bedding/Linen</th>
                        <th className="text-secondary h3"><strong>Clean Only</strong></th>
                        <th className="text-secondary h3"><strong>Clean + Press</strong></th>
                    </tr>
                    <tr>
                        <td>Bedsheet <hr/></td>
                        <td>{currentCurrency} 7</td>
                        <td>{currentCurrency} 10</td>
                    </tr>
                    <tr>
                        <td>Blanket <hr/></td>
                        <td>-</td>
                        <td>{currentCurrency} 30</td>
                    </tr>
                    <tr>
                        <td>Bathrobe <hr/></td>
                        <td>-</td>
                        <td>{currentCurrency} 12</td>
                    </tr>
                    <tr>
                        <td>Duve <hr/></td>
                        <td>-</td>
                        <td>{currentCurrency} 30</td>
                    </tr>
                    <tr>
                        <td>Duvet Cover <hr/></td>
                        <td>{currentCurrency} 12</td>
                        <td>{currentCurrency} 18</td>
                    </tr>
                    <tr>
                        <td>Pillow <hr/></td>
                        <td>-</td>
                        <td>{currentCurrency} 12</td>
                    </tr>
                    <tr>
                        <td>Pillow Case <hr/></td>
                        <td>{currentCurrency} 3</td>
                        <td>{currentCurrency} 5</td>
                    </tr>
                    <tr>
                        <td>Table Cloth <hr/></td>
                        <td>{currentCurrency} 6</td>
                        <td>{currentCurrency} 10</td>
                    </tr>
                    <tr>
                        <td>Towel (S/L) <hr/></td>
                        <td>{currentCurrency} 4/2</td>
                        <td>{currentCurrency} 6/3</td>
                    </tr>
                    </tbody>
                    </table>
                    <table className="table mt-5">
                    <tbody>
                    <tr>
                        <th className="text-primary h3 first-th">Formal Wear/Others</th>
                        <th className="text-secondary h3"><strong>Clean Only</strong></th>
                        <th className="text-secondary h3"><strong>Clean + Press</strong></th>
                    </tr>
                    <tr>
                        <td>Abaya <hr/></td>
                        <td>{currentCurrency} 14</td>
                        <td>{currentCurrency} 20</td>
                    </tr>
                    <tr>
                        <td>Cap <hr/></td>
                        <td>-</td>
                        <td>{currentCurrency} 5</td>
                    </tr>
                    <tr>
                        <td>Coat <hr/></td>
                        <td>{currentCurrency} 15</td>
                        <td>{currentCurrency} 10</td>
                    </tr>
                    <tr>
                        <td>Dress (Casual) <hr/></td>
                        <td>{currentCurrency} 12</td>
                        <td>{currentCurrency} 20</td>
                    </tr>
                    <tr>
                        <td>Dress (Special) <hr/></td>
                        <td>{currentCurrency} 17</td>
                        <td>{currentCurrency} 30</td>
                    </tr>
                    <tr>
                        <td>Ghatra <hr/></td>
                        <td>{currentCurrency} 6</td>
                        <td>{currentCurrency} 8</td>
                    </tr>
                    <tr>
                        <td>Jacket <hr/></td>
                        <td>{currentCurrency} 18</td>
                        <td>{currentCurrency} 25</td>
                    </tr>
                    <tr>
                        <td>Kandura <hr/></td>
                        <td>{currentCurrency} 8</td>
                        <td>{currentCurrency} 15</td>
                    </tr>
                    <tr>
                        <td>Napkin <hr/></td>
                        <td>{currentCurrency} 2</td>
                        <td>{currentCurrency} 3</td>
                    </tr>
                    <tr>
                        <td>Overcoat <hr/></td>
                        <td>{currentCurrency} 25</td>
                        <td>{currentCurrency} 40</td>
                    </tr>
                    <tr>
                        <td>Pyjamas (2 pc) <hr/></td>
                        <td>{currentCurrency} 9</td>
                        <td>{currentCurrency} 14</td>
                    </tr>
                    <tr>
                        <td>Sari <hr/></td>
                        <td>{currentCurrency} 21</td>
                        <td>{currentCurrency} 30</td>
                    </tr>
                    <tr>
                        <td>Suit (2 pc) <hr/></td>
                        <td>{currentCurrency} 22</td>
                        <td>{currentCurrency} 35</td>
                    </tr>
                    <tr>
                        <td>Suit (3 pc) <hr/></td>
                        <td>{currentCurrency} 25</td>
                        <td>{currentCurrency} 40</td>
                    </tr>
                    <tr>
                        <td>Tie <hr/></td>
                        <td>{currentCurrency} 5</td>
                        <td>{currentCurrency} 7</td>
                    </tr>
                    <tr>
                        <td>Scarf <hr/></td>
                        <td>{currentCurrency} 4</td>
                        <td>{currentCurrency} 6</td>
                    </tr>
            </tbody>
        </table>
        
        </div>
    </div>);
    }
}
function mapStateToProps(state) {
    return {
        myCreditCardsData: state.myCreditCardsData,
        showLoginMenu: state.showLoginMenu,
        currentCity: state.currentCity,
        currentCityData: state.currentCityData,
        lang: state.lang
    }
}
export default withCookies(connect(mapStateToProps)(DryCleaningPrice));