import {backend_url} from "../../../api";
import axios from 'axios';
import {VoucherifyValidate,OXYCLEAN_PRICE,VoucherifyRedeem, CREDITCARD_COUPON} from "../../../actions/"
import TaxCalculator from "./TaxCalculator";
import locationHelper from "../../../helpers/locationHelper";
import commonHelper from "../../../helpers/commonHelper";
import moment from 'moment';
//const OXYCLEAN_PRICE = 250

const BookingVoucherify = {
    validate:function (coupon, rules) {
        var REDUX_DATA =  window.REDUX_DATA;
        var serviceConstants =  REDUX_DATA.serviceConstants;
        var current_currency =  locationHelper.getCurrentCurrency(); // Todo should be from global constant;
        var bookingDate = rules["bookingDate"];
        var coupon = coupon.trim();
        var price = rules["amount"];
        /*if(price == 0){
            return this.error("Please select the package.");
        }*/
        if (typeof coupon == 'undefined' || coupon.length == 0) {
            return this.error("Please provide coupon.");
        }else if (coupon.trim().length < 3) { // minimum length of a promo code is more than 3 characters, so need to go to server if less than that
            //this.couponMessage.error()
            return this.error("Please provide coupon.")
        } else {
            var userToken = commonHelper.getUserToken();

            var isCreditCardCouponApplied = false;

            return VoucherifyValidate(rules, userToken).then((res)=> {
                var data = JSON.parse(res.voucher)
                if(data.status == "BAD_REQUEST"){
                    var returnData = {};

                    var taxChargesTotal = TaxCalculator.calculateVAT(price, bookingDate);

                    var total = price + taxChargesTotal;

                     returnData["error"] = true;

                     returnData["data"] = this.error(data.errors);

                     var errors = data.errors;

                    if(errors.length == 1){
                        errors = errors[0];
                        if( coupon == CREDITCARD_COUPON && (errors["errorCode"] == "redemption validation rules are violated" && errors["errorMessage"] == "This code can only be used once")){
                            isCreditCardCouponApplied = true;
                        }
                    }

                     returnData["taxChargesTotal"] = taxChargesTotal;

                     returnData["currentTotal"] = price;

                     returnData["subtotal"] = price;

                     returnData["total"] = parseFloat(total);

                     returnData["isCreditCardCouponApplied"] = isCreditCardCouponApplied

                    return returnData;

                }else{
                    return this.success(data, bookingDate);
                }
            });
        }
    },
    voucherRedeem:function (rules, userToken) {
        var REDUX_DATA =  window.REDUX_DATA;
        var serviceConstants =  REDUX_DATA.serviceConstants;
        var current_currency =  locationHelper.getCurrentCurrency(); // Todo should be from global constant;
        
        return VoucherifyRedeem(rules, userToken).then((res)=> { 
            var data = JSON.parse(res.voucherRedemption)
            return data;
        });
    },
    generateRules(coupon, price, serviceId, subServiceId = 0, startDate, city,  packageDetail, paymentMethod = "", isRedeem = false){
        var serviceConstants =  REDUX_DATA.serviceConstants;
        serviceId = typeof serviceId == "object" ?  serviceId.value : serviceId;

        if( serviceId == serviceConstants.SERVICE_AC_CLEANING || subServiceId == serviceConstants.SERVICE_AC_CLEANING ){ // for oxyclean
            if( packageDetail['oxyclean'] == "yes"){
                price += packageDetail['number_of_units'] * OXYCLEAN_PRICE;
            }
        }
        var bookingDate = startDate;

        startDate = startDate != "" ? startDate.split('-').reverse().join('-') : "";

        startDate = startDate != "" ? moment(startDate) : moment(window.REDUX_DATA.dateAndTime);

        var metaData = {},
            serviceDateTimeStamp = startDate.unix();


        metaData["bookingDate"] = bookingDate;

        metaData["service_start_date"] = serviceDateTimeStamp;

        metaData["service_end_date"] = serviceDateTimeStamp;

        metaData["start_date"] = startDate.format("YYYY-MM-DD");

        metaData["city"] = city;
        if(paymentMethod != ""){
            paymentMethod = paymentMethod == "credit" ? "card" : paymentMethod;
            metaData["payment_method"] = paymentMethod;
         }// == "" ? "not_selected" : paymentMethod;

        var data = {};

        var isLWKey = isRedeem ? "is_lw" : "isLW";

        data[isLWKey] = true;

        data["code"] = coupon.toUpperCase();

        serviceId = subServiceId != 0 ? subServiceId :serviceId;

        var metaData_rules = this.voucherify_meta_data_mapping(serviceId);

        if(typeof metaData_rules.service != "undefined"){
            metaData["service"] = metaData_rules.service;
            metaData["sub_service"] = metaData_rules.sub_service;
        }

        data["rules"] = metaData;

        data["amount"] = parseInt(price);

        return data;
    },
    zohoPriceData(price, tax = 0, serviceDate, packageDetails = [], isHourlyPrice = false, redemptionData = {} ){
        var returnData = {};
        if(Object.keys(redemptionData).length){
            // console.log("calling Here 123");
            returnData = this.voucherifyZohoData( redemptionData, serviceDate,  packageDetails, isHourlyPrice);
        }else{
            returnData = this.vatZohoData( price, tax , serviceDate , isHourlyPrice, packageDetails );
        }

        return returnData;

    },
    voucherifyZohoData(redemptionData, serviceDate, packageDetails, isHourlyPrice = false){
        var returnData = {};
        var orginalPrice = redemptionData["amount"];
        var price = redemptionData["finalAmount"];
        var vat_charge = 0;
        var voucherData = {
             "Redemption_Id": redemptionData["id"],
             "Voucher_Code" : redemptionData["voucher"],
             "isCouponApplied" : "true",
             "Orginal_Price1" : orginalPrice
        };
       
        var vatZohoData = this.vatZohoData(price, vat_charge, serviceDate, isHourlyPrice, packageDetails, true );

        returnData = Object.assign(voucherData, vatZohoData);

        return returnData;
    },
    vatZohoData(price, tax = 0, bookingDate , isHourlyPrice = false, packageDetails , isredemptionData = false){

        tax =  tax == 0 ? TaxCalculator.calculateVAT(price, bookingDate) : tax;

        var totalPrice = price + tax;

        var vatZohoData = {
            "Tax" : tax,
            "Subtotal" : price,
            "Payment_Status": "Pending"
        };
        if(!isredemptionData){
            vatZohoData["Orginal_Price1"] = price;
        }
        if( Object.keys(packageDetails).length && isHourlyPrice ){
            vatZohoData["No_of_Hours"] = packageDetails["No_of_Hours"];
            vatZohoData["Price"] = totalPrice;
            vatZohoData["Hourly_rate"] = packageDetails["hourly_price"];
        }else {
            if ( isHourlyPrice ) {
                vatZohoData["Hourly_rate"] = totalPrice;
            } else {
                vatZohoData["Price"] = totalPrice;
            }
        }
        //console.log("vatZohoData", vatZohoData);

        return vatZohoData;
    },
    voucherify_meta_data_mapping(serviceId){
        var serviceConstants =  window.REDUX_DATA.serviceConstants;
        var mappedData = {};

        var liteWeightServiceArray = new Array();

        liteWeightServiceArray[serviceConstants.SERVICE_AC_CLEANING] =  {"service":"ac_maintenance", "sub_service":"ac_cleaning"};

        liteWeightServiceArray[serviceConstants.SERVICE_AC_INSTALLATION] =  {"service":"ac_maintenance", "sub_service":"ac_installation"};

        liteWeightServiceArray[serviceConstants.SERVICE_AC_REPAIR] =  {"service":"ac_maintenance", "sub_service":"ac_repair"};

        liteWeightServiceArray[serviceConstants.SERVICE_COCKROACHES_PEST_CONTROL] =  {"service":"pest_control", "sub_service":"cockroaches"};

        liteWeightServiceArray[serviceConstants.SERVICE_PEST_CONTROL] =  {"service":"pest_control", "sub_service":"cockroaches"};

        liteWeightServiceArray[serviceConstants.SERVICE_BED_BUGS_PEST_CONTROL] =  {"service":"pest_control", "sub_service":"bed_bugs"};

        liteWeightServiceArray[serviceConstants.SERVICE_ANTS_PEST_CONTROL] =  {"service":"pest_control", "sub_service":"ants"};

        liteWeightServiceArray[serviceConstants.SERVICE_MOVE_IN_PEST_CONTROL] =  {"service":"pest_control", "sub_service":"move_pest_control"};

        liteWeightServiceArray[serviceConstants.SERVICE_HANDYMAN_SERVICE] =  {"service":"handyman_services", "sub_service":"handyman"};

        liteWeightServiceArray[serviceConstants.SERVICE_FURNITURE_ASSEMBLY] =  {"service":"handyman_services", "sub_service":"furniture_assembly"};

        liteWeightServiceArray[serviceConstants.SERVICE_TV_MOUNTING] =  {"service":"handyman_services", "sub_service":"tv_mounting"};

        liteWeightServiceArray[serviceConstants.SERVICE_CURTAIN_HANGING] =  {"service":"handyman_services", "sub_service":"curtain_hanging"};

        liteWeightServiceArray[serviceConstants.SERVICE_LIGHTBULBS_AND_LIGHTING] =  {"service":"handyman_services", "sub_service":"lightbulbs"};

        liteWeightServiceArray[serviceConstants.SERVICE_ELECTRICIAN] =  {"service":"electrician_services", "sub_service":"electrician"};

        liteWeightServiceArray[serviceConstants.SERVICE_LIGHT_FIXTURES] =  {"service":"electrician_services", "sub_service":"electrician"};

        liteWeightServiceArray[serviceConstants.SERVICE_INDOOR_AND_OUTDOOR_WIRING] =  {"service":"electrician_services", "sub_service":"electrician"};

        liteWeightServiceArray[serviceConstants.SERVICE_OUTLETS_AND_SWITCHES] =  {"service":"electrician_services", "sub_service":"electrician"};

        liteWeightServiceArray[serviceConstants.SERVICE_ELECTRICAL_INSTALLATIONS_AND_REPAIRS] =  {"service":"electrician_services", "sub_service":"electrician"};

        liteWeightServiceArray[serviceConstants.SERVICE_PLUMBING] =  {"service":"plumbing_services", "sub_service":"plumbing"};

        liteWeightServiceArray[serviceConstants.SERVICE_FIXING_LEAKS] =  {"service":"plumbing_services", "sub_service":"plumbing"};

        liteWeightServiceArray[serviceConstants.SERVICE_KITCHEN_PLUMBING] =  {"service":"plumbing_services", "sub_service":"plumbing"};

        liteWeightServiceArray[serviceConstants.SERVICE_BATHROOM_PLUMBING] =  {"service":"plumbing_services", "sub_service":"plumbing"};

        liteWeightServiceArray[serviceConstants.SERVICE_WATER_HEATERS] =  {"service":"plumbing_services", "sub_service":"plumbing"};

        liteWeightServiceArray[serviceConstants.SERVICE_DEEP_CLEANING_SERVICE] =  {"service":"cleaning_services", "sub_service":"deep_cleaning"};

        liteWeightServiceArray[serviceConstants.SERVICE_WATER_TANK_CLEANING] =  {"service":"cleaning_services", "sub_service":"water_tank_cleaning"};

        liteWeightServiceArray[serviceConstants.SERVICE_MATTRESS_CLEANING] =  {"service":"cleaning_services", "sub_service":"mattress_cleaning"};

        liteWeightServiceArray[serviceConstants.SERVICE_SOFA_CLEANING] =  {"service":"cleaning_services", "sub_service":"sofa_cleaning"};

        liteWeightServiceArray[serviceConstants.SERVICE_CARPET_CLEANING] =  {"service":"cleaning_services", "sub_service":"carpet_cleaning"};
        
        liteWeightServiceArray[serviceConstants.SERVICE_LAUNDRY_AND_DRY_CLEANING] =  {"service":"cleaning_services", "sub_service":"dry_cleaning"};

        liteWeightServiceArray[serviceConstants.SERVICE_LOCAL_MOVE] =  {"service":"moving_services", "sub_service":"full_moving"};

        liteWeightServiceArray[serviceConstants.SERVICE_LOCAL_SMALL_MOVE] =  {"service":"moving_services", "sub_service":"small_moving"};

        //console.log(liteWeightServiceArray);

        if( typeof liteWeightServiceArray[serviceId] != undefined){
            mappedData = liteWeightServiceArray[serviceId];
        }

        return mappedData;
    },
    error:function (error) {
        
        //console.log("error", error);

        var errors = {"data":{"error":true}};

        if(typeof error == "string"){
            errors["data"]["error_message"] =  error;
        }else{
            var error_message = "";
            if(error){
                error.map((item,index) =>{
                    if(item.errorCode == "resource_not_found"){
                        error_message = "Promotion code is invalid";
                    }else{
                        error_message = item.errorMessage;
                    }
                })
            }
            errors["data"]["error_message"] = error_message;
        }
        return errors;
    },
    success:function (res, startDate) {
        var returnData = {};
        var response = res;
        var discountData = response.data;
        var current_currency =  locationHelper.getCurrentCurrency();
        //console.log(discountData);

        var currentTotal = discountData.amount;
        var promoCodeDiscountText = "";
        var totalAfterDiscount = 0;
        var prefferedDate = startDate;

        if(discountData.discountType == 'PERCENT') {
            promoCodeDiscountText = discountData.discountPercentOff + '% OFF';
            totalAfterDiscount =  discountData.finalAmount;//currentTotal - discountData.discountAmountOff;
        }
        else if (discountData.discountType == 'AMOUNT') {
            promoCodeDiscountText = current_currency+" "+discountData.discountAmountOff+' OFF';
            totalAfterDiscount = currentTotal - discountData.discountAmountOff;
        }

        var taxChargesTotal = TaxCalculator.calculateVAT(totalAfterDiscount,prefferedDate);

        var total = this.financial(totalAfterDiscount + taxChargesTotal);

        returnData["taxChargesTotal"] = taxChargesTotal;

        returnData["currentTotal"] = currentTotal;

        returnData["totalAfterDiscount"] = totalAfterDiscount;

        returnData["total"] = parseFloat(total);

        returnData["data"] = discountData;

        returnData["data"]["promoCodeDiscountText"] = promoCodeDiscountText;

        returnData["data"]["success"] = true;

        returnData["data"]["description"] = discountData.metadata != null ?  discountData.metadata.success : "Congratulations! Your discount code has been applied";

        return returnData;
    },
    financial(x) {
        return Number.parseFloat(x).toFixed(2);
    }

}
export default BookingVoucherify;