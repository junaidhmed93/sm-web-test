import React from "react";
import FormFieldsTitle from "../../../components/FormFieldsTitle";
import CheckRadioBoxInput from "../../../components/Form_Fields/CheckRadioBoxInput";
import DatePick from "../../../components/Form_Fields/DatePick";
import TimePicker from "../../../components/Form_Fields/TimePicker";
import TextareaInput from "../../../components/Form_Fields/TextareaInput";
import PaymentMethods from "../../../components/Form_Fields/PaymentMethods";
import NewBookingSummary from "../../../components/Form_Fields/NewBookingSummary";
import BookNextStep from "../../../components/Form_Fields/BookNextStep";
import FormTitleDescription from "../../../components/FormTitleDescription";
import ContactDetailsStep from "../../../components/ContactDetailsStep";
import { isValidSection, scrollToTop, locationConstants } from "../../../actions/index";
import locationHelper from "../../../helpers/locationHelper";
import commonHelper from "../../../helpers/commonHelper";
import TaxCalculator from "./TaxCalculator";
import BookingVoucherify from "./BookingVoucherify";
import {withCookies} from "react-cookie";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import Loader from "../../../components/Loader";
import stringConstants from '../../../constants/stringConstants';
import GeneralModal from "../../../components/GeneralModal";
import moment from 'moment';
let allServiceConstant = {};
let URLConstant = {};
let currentCurrency = "";
let current_city = "dubai";
var discountData = "";
import {
    CREDITCARD_COUPON,
    zendeskChatBox
} from "../../../actions";
import DryCleaningPrice from "./DryCleaningPrice";
var status = null;
var bid = null;
var tt = null;
let WATER_TANK_CLEANING_PRICE = 0;
class BookDryCleaningPage extends React.Component {

    constructor(props) {
        super(props);

        const { cookies } = props;

        var bookingData = {
            booking_date: '',
            booking_time: '',
            dropOff: { id: 'free', value: 48, label: "Standard (48 hours)", desc: 'Free Delivery', descClass:'desc mt-1 text-warning text-center border-top width-50' },
            input_email: props.userProfile.email,
            input_phone: props.userProfile.address ? props.userProfile.address.phoneNumber : '',
            input_name: props.userProfile.customerFirstName,
            input_last_name: props.userProfile.customerLastName,
            input_address_city: {},
            input_address_area: props.setUserArea,
            input_address_area_building_name: props.userProfile.address ? props.userProfile.address.building : '',
            input_address_area_building_apartment: props.userProfile.address ? props.userProfile.address.apartment : '',
            details: '',
            showMobileSummary: false,
            discountData: {},
            voucherCode: "",
            hourlyRate: 0,
            subtotal: 0,
            vat: 0,
            totalAfterDiscount: 0,
            isNewBookingEngine: true,
            total: WATER_TANK_CLEANING_PRICE,
            price: WATER_TANK_CLEANING_PRICE,
            payment: '',
            loader: false,
            discountPrices: {},
            walletUsed: '',
            isWalletAvailable: false,
            showPriceList: false
        }
        let {currentCityData} = props;

        currentCurrency = (typeof currentCityData != "undefined" && currentCityData.length) ? currentCityData[0].cityDto.countryDto.currencyDto.code : locationConstants.DEFAULT_CURRENCY;
        
        var cookieData = props.getCookieFormData();

        if (Object.keys(cookieData).length) { // restore from cookies
            bookingData = cookieData;
            bookingData["softSafeBookingId"] = "";
            bookingData["softSafeRequestId"] = "";
            props.removeCookie("booking_data");
            props.removeCookie("submitted_data");
            props.removeCookie("confirmation_summary");
        }
        this.state = bookingData;

        this.ContactDetails = this.ContactDetails.bind(this);
        this.cleaningRequest = this.cleaningRequest.bind(this);
        this.handleCustomChange = this.handleCustomChange.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleDropDownChange = this.handleDropDownChange.bind(this);
        this.currentStep = this.currentStep.bind(this);
        this.getPrices = this.getPrices.bind(this);
        this.moveNext = this.moveNext.bind(this);
        this.hashChangeHandler = this.hashChangeHandler.bind(this);
        this.calculatePrice = this.calculatePrice.bind(this);
        this.handleVoucherChange = this.handleVoucherChange.bind(this);
        this.updateTotal = this.updateTotal.bind(this);
        this.mobileSummary = this.mobileSummary.bind(this);
        this.handleBookingTimeChange = this.handleBookingTimeChange.bind(this);
        this.applyCreditCardCoupon = this.applyCreditCardCoupon.bind(this);
        this.togglePriceListPopUp = this.togglePriceListPopUp.bind(this);
        this.showPriceList = this.showPriceList.bind(this);

        //.PricePlan.planDetail.packages[0].price

    }
    componentDidMount() {
        allServiceConstant = this.props.service_constants;
        URLConstant = this.props.url_constants;
        current_city = this.props.current_city;
        //console.log("props.pricingPlan", this.props.pricePlan);
        //WATER_TANK_CLEANING_PRICE = 0;
        if (status == null) {
            var city = locationHelper.getLocationByName(current_city);
            let priceList = commonHelper.getParameterByName("price_list");
            let showPriceList = priceList == "yes" ? true : false;
            //console.log("showPriceList", showPriceList, commonHelper.getParameterByName("price_list"), priceList);
            var promoCode = commonHelper.getParameterByName("promo") != null ? commonHelper.getParameterByName("promo") : "";
            this.setState({
                input_address_city: city,
                loader: false,
                voucherCode: promoCode,
                showPriceList: showPriceList
            });
        }
        
        const walletAvailable = this.props.userProfile && this.props.userProfile.userWallet && this.props.userProfile.userWallet.currency.code === currentCurrency && this.props.userProfile.userWallet.totalAmount > 0 ? true : false;
        this.setState({ isWalletAvailable: walletAvailable });

        zendeskChatBox();
        window.addEventListener("hashchange", this.hashChangeHandler, false);
    }
    componentWillUnmount() {
        window.removeEventListener("hashchange", this.hashChangeHandler, false);
    }
    handleBookingTimeChange(name, value) {
        this.setState({
            booking_time: value
        });
    }
    hashChangeHandler() {
        let hashVal = window.location.hash;
        let step;
        hashVal.length ? step = parseInt(window.location.hash.replace('#', '')) : step = 1;
        this.props.moveNext(step);
        scrollToTop();
    }
    sumarryItems(){
        const {booking_date, dropOff, booking_time,discountData,subtotal, vat, payment,walletUsed} = this.state;
        let timeValue ="";
        if(booking_date != "" ){
            let dateOfBooking = booking_date.split('-').reverse().join('-');

            dateOfBooking = typeof booking_time.value != "undefined" ? dateOfBooking+' '+booking_time.value : dateOfBooking;

            dateOfBooking = moment( dateOfBooking );
            
            let dateStr = dateOfBooking.format('DD-MM-YYYY');

            timeValue = dateStr;

            if(typeof booking_time.value != "undefined"){
                //timeValue += " Between "+booking_time.label +" - "+ dateOfBooking.add(2, 'hours').format('h A')+"\n";
                timeValue += ", "+booking_time.label;
            }
            
        }
        let deliveryDetail = dropOff.value == 24 ? dropOff.label+" "+dropOff.desc : dropOff.label+" - "+dropOff.desc;

        var items = [
            {label: 'Pickup time', value: timeValue},
            {label: 'Delivery', value: deliveryDetail}
       ];
       const {isWalletAvailable} = this.props;

       if (isWalletAvailable) {
            const walletAmount = walletUsed;//this.props.userProfile.userWallet.totalAmount;
            const walletCurrency = this.props.userProfile.userWallet.currency.code;
            //if (walletCurrency === currentCurrency && walletAmount > 0) {
            if (walletCurrency === currentCurrency) {
               items.push({ label: stringConstants.WALLET, value: this.props.userProfile.userWallet.currency.code + ' ' + this.props.userProfile.userWallet.totalAmount});
            }
        }
    
        
        if(typeof discountData.promoCodeDiscountText != "undefined"){
            items.push({label: 'Promo code', value: discountData.promoCodeDiscountText});
        }

        items.push(
            { label: 'Payment', value: payment }
        );
        return items;
    }
    calculatePrice(getTotal = false) {

        var changed_element = null, changed_value = null;

        var {
            booking_date,
            voucherCode,
            discountPrices,
            discountData,
            isWalletAvailable
        } = this.state;

        var total_price = 0;
        var taxRate = 0;
        var updatedPrice = false;

        total_price = parseInt(WATER_TANK_CLEANING_PRICE);

        let walletUsed = 0;
        if (!getTotal && isWalletAvailable) {
            const walletAmount = this.props.userProfile.userWallet.totalAmount;
            const walletCurrency = this.props.userProfile.userWallet.currency.code;

            if (walletCurrency === currentCurrency && walletAmount > 0) {
                if (walletAmount >= total_price) {
                    walletUsed = total_price;
                    total_price = 0;
                }
                else if (walletAmount < total_price) {
                    walletUsed = walletAmount;
                    total_price = total_price - walletAmount;
                }

            }

        }

        var prefferedDate = booking_date;

        var taxChargesTotal = TaxCalculator.calculateVAT(total_price, prefferedDate);

        var total = total_price + taxChargesTotal;

        if (this.props.userProfile && this.props.userProfile.userWallet) {
            const walletAmount = this.props.userProfile.userWallet.totalAmount;
            const walletCurrency = this.props.userProfile.userWallet.currency.code;

            if (walletCurrency === currentCurrency && walletAmount > 0) {

                total = total > 0 ? total : 0.00;
                //this.setState({walletUsed: walletUsed});
            }
        }

        var returnData = {
            total: total,
            subtotal: total_price,
            vat: taxChargesTotal,
            walletUsed: walletUsed
        };

        if (!getTotal && (typeof discountData != "undefined" && (typeof discountData.success != "undefined" && discountData.success))) {
            returnData = this.updateTotal(discountPrices, true);
        }

        return returnData;
    }
    handleVoucherChange(value, new_price = 0) {

        var couponValue = value.trim()
            , changed_element = null, changed_value = null;

        this.setState({
            discountData: "load"
        })

        let { booking_date, booking_time, discountData, subtotal, vat, payment, price } = this.state;

        if (typeof couponValue == 'undefined' || couponValue.length == 0) {
            var response = BookingVoucherify.error("Please provide coupon.");
            this.setState({
                discountData: response.data
            });
            //this.calculatePrice("reset_coupon", true);
        } else if (couponValue.trim().length < 3) { // minimum length of a promo code is more than 3 characters, so need to go to server if less than that
            var response = BookingVoucherify.error("Promotion code is invalid");
            this.setState({
                discountData: response.data
            })
            //this.calculatePrice("reset_coupon", true);
        }
        else {


            if (new_price != 0) {
                price = new_price;
            }

            if (new_price != 0) {
                price = new_price;
            }
            //console.log("handleVoucherChange ServiceID", allServiceConstant.SERVICE_DEEP_CLEANING_SERVICE)

            var rules = BookingVoucherify.generateRules(couponValue, price, allServiceConstant.SERVICE_WATER_TANK_CLEANING, 0, booking_date, current_city, "", payment);

            BookingVoucherify.validate(couponValue, rules).then((response) => {
                var res = response;
                //console.log(res);
                if (this.state.voucherCode != "" || (this.state.voucherCode == CREDITCARD_COUPON && this.state.payment == "credit")) {
                    this.updateTotal(res);
                }
            });
        }
    }
    updateTotal(res, isReturnData = false) {
        let { walletUsed, booking_date} = this.state;
        let response = res;
        let discountData = response.data;
        let currentTotal = response.currentTotal;
        let taxChargesTotal = response.taxChargesTotal;
        let total = response.total;

        if (walletUsed > 0) {
            currentTotal = parseFloat((response.totalAfterDiscount - walletUsed).toFixed(2));
            taxChargesTotal = TaxCalculator.calculateVAT(currentTotal, booking_date);
            total = parseFloat((currentTotal + taxChargesTotal).toFixed(2));

        }

        var returnData = {
            price: currentTotal,
            total: total,
            subtotal: currentTotal,
            vat: taxChargesTotal,
            walletUsed: walletUsed
        };

        if (isReturnData) {
            return returnData;
        } else {
            returnData["discountData"] = discountData;
            returnData["discountPrices"] = res
        }

        this.setState(returnData);
    }
    currentStep() {
        return this.props.formCurrentStep;
    }
    moveNext(step) {
        const { formCurrentStep, signInDetails, showLoginModal, showLoginMenu, moveNextStep } = this.props;
        moveNextStep(step, this.state, true);
    }
    handleCustomChange(name, value) {
        this.setState({
            booking_time: value
        });
    }
    handleInputChange(name, value) {
        if (name == "payment") {
            this.applyCreditCardCoupon(value);
        }
        this.setState({
            [name]: value
        });

        //console.log(name, value);
    }
    handleDropDownChange(name, value) {
        console.log(name, value);
    }
    getPrices() {
        var lite_weight_prices = WATER_TANK_CLEANING_PRICE;//this.props.lite_weight_prices;

        return lite_weight_prices
    }
    cleaningRequest() {
        let {PricePlan} = this.props;

        var wateTankPrice = this.getPrices();

        var booking_date = this.state.booking_date;

        //other_services
        var currentStepClass = this.currentStep() === 1 ? '' : "d-none";
        //console.log("PricePlan", PricePlan)
        let deliveryCharges = (Object.keys(PricePlan).length && typeof PricePlan.planDetail != "undefined") ? PricePlan.planDetail.additional_charges.delivery_charges[0].price : 50;
        //PricePlan.planDetail.additional_charges.delivery_charges

        let dropOffOptions = [
            { id: 'free', value: 48, label: "Standard (48 hours)", desc: 'Free Delivery', descClass:'desc mt-1 text-warning text-center border-top width-50' },
            { id: 'express', value: 24, label: "Express (24 hours)",desc: "+ "+currentCurrency+" "+deliveryCharges, descClass:'desc mt-1 text-warning text-center border-top width-50' }
        ];

        return (
            <div id="section-request-form" className={currentStepClass}>
                {this.props.showDiscountNotification(this.state.discountData)}
                <section>
                    <div className="row mb-5 dry-cleaning-detail">
                        <div className="col">
                            <div className="row m-0 bg-white border rounded is-r">
                                <div className="detailed-pricing is-a right mt-3 mr-3 hidden d-none"><a href="#" onClick={(e) => {this.togglePriceListPopUp(true); e.preventDefault();}}>View detailed pricing</a></div>
                                <div className="col-1 bg-primary py-3">
                                </div>
                                <div className="py-3 col laundry-desc">
                                    <div className="laundry-option mb-4">
                                        <i className="fa fa-tag fa-2x text-white is-a text-white"></i>
                                        <div className="row">
                                            <div className="col-5 col-sm-3 pr-0">
                                                <span className="font-weight-bold">Wash &amp; fold bag:</span>
                                            </div>
                                            <div className="col-auto pl-0">
                                                {currentCurrency} 49 + VAT
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-5 col-sm-3 pr-0">
                                                <span className="font-weight-bold">Iron only bag:</span>
                                            </div>
                                            <div className="col-auto pl-0">
                                                {currentCurrency} 69 + VAT
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-5 col-sm-3 pr-0">
                                                <span className="font-weight-bold">Wash &amp; iron bag:</span>
                                            </div>
                                            <div className="col-auto pl-0">
                                                {currentCurrency} 99 + VAT
                                            </div>
                                        </div>
                                        { /*<p className="m-0"><span className="font-weight-bold">Wash &amp; fold bag:</span> {currentCurrency} 49 + VAT</p>
                                        <p className="m-0"><span className="font-weight-bold">Iron only bag:</span> {currentCurrency} 69 + VAT</p>
                                        <p className="m-0"><span className="font-weight-bold">Wash &amp; iron bag:</span> {currentCurrency} 99 + VAT</p>*/}
                                        { /* <p className="m-0 d-none">Dry cleaning services available. View detailed <a href="#" onClick={(e) => {this.togglePriceListPopUp(true); e.preventDefault();}}>price list</a></p> */ }
                                        <p className="m-0 mt-1">
                                            Dry cleaning services available - 
                                            &nbsp; <a href="#" className="text-decoration-underline" onClick={(e) => {this.togglePriceListPopUp(true); e.preventDefault();}}>
                                                see price list
                                            </a>
                                        </p>
                                    </div>
                                    <div className="laundry-delivery mb-4">
                                        <i className="fa fa-truck is-a fa-2x text-white"></i>
                                        Free delivery in 48 hours
                                    </div>
                                    <div className="laundry-bag-desc mb-2">
                                        <i className="fa fa-shopping-bag is-a fa-2x text-white"></i>
                                            We will provide laundry bags. The bags measure 70 x 50 cm- equivalent to 2 laundry loads
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                </section>
                <section>
                    <FormFieldsTitle title="Select your pickup time" />
                    <div className="row mb-4">
                        <DatePick
                            name="booking_date"
                            inputValue={this.state.booking_date}
                            selectedDate={this.state.booking_date}
                            disable_friday={true}
                            onInputChange={this.handleInputChange}
                            validationClasses="required"
                            disableFriday={false} />

                        <TimePicker inputValue={this.state.booking_time}
                            name="booking_time"
                            disableFirstSlot={true}
                            onInputChange={this.handleBookingTimeChange}
                            validationClasses="required" 
                            booking_date={this.state.booking_date}
                            timeSlotDesc="Please allow up to 60 min for the driver to reach your location"/>
                        
                    </div>
                </section>
                <section>
                    <FormFieldsTitle title="Select your delivery preference" />
                    <CheckRadioBoxInput
                        InputType="radio"
                        name="dropOff"
                        inputValue={this.state.dropOff}
                        items={dropOffOptions}
                        onInputChange={this.handleInputChange}
                        childClass="col-6 mb-3 d-flex"
                        parentClass="row mb-2" />
                </section>
                <section>
                    <FormTitleDescription title="Any special instructions or additional details" />
                    <div className="row mb-4">
                        <div className="col mb-3">
                            <TextareaInput
                                name="details" inputValue={this.state.details}
                                placeholder="Example: Please ask driver to pick up my laundry at the building reception this week"
                                onInputChange={this.handleInputChange} />
                        </div>
                    </div>
                </section>
                <section>
                    <BookNextStep total={this.state.total} title="Contact Details" moveNext={this.moveNext} toStep="2" setModal={this.mobileSummary} />
                </section>
            </div>
        )
    }
    ContactDetails(isLocationDetailShow) {
        var cityOptions = locationHelper.getLocationByName(current_city);

        var currentStepClass = this.currentStep() === 2 ? '' : "d-none";
        //var currentStepClass =  ''
        var isLocationDetailShow = "yes";

        var contact_details = {
            input_email: this.state.input_email,
            input_phone: this.state.input_phone,
            input_name: this.state.input_name,
            input_last_name: this.state.input_last_name
        }

        if (isLocationDetailShow) {
            contact_details.input_address_city = this.state.input_address_city;
            contact_details.input_address_area = this.state.input_address_area;
            contact_details.input_address_area_building_name = this.state.input_address_area_building_name;
            contact_details.input_address_area_building_apartment = this.state.input_address_area_building_apartment;
        }

        var userProfile = this.props.userProfile;

        return (
            <div id="personal-information-form" className={currentStepClass}>
                <ContactDetailsStep
                    cityOptions={cityOptions}
                    userProfile={userProfile}
                    signInDetails={this.props.signInDetails}
                    isLocationDetailShow={isLocationDetailShow}
                    contact_details={contact_details}
                    handleDropDownChange={this.handleInputChange}
                    handleInputChange={this.handleInputChange}
                    mobileSummary={this.mobileSummary}
                    moveNext={this.moveNext}
                    toStep={3}
                    isBooking={true}
                    total={this.state.total}
                    currentCity={this.props.current_city}
                />
            </div>
        )
    }
    PaymentSection() {
        var payment = this.state.payment;
        var currentStepClass = this.currentStep() === 3 ? '' : "d-none";
        //var currentStepClass ='';
        if (!this.state.paymentLoader) {

            return (
                <div id="payment-method-form" className={currentStepClass}>
                    <section className="payment">
                        <PaymentMethods name="payment" inputValue={payment} status={status} tt={tt} onInputChange={this.handleInputChange} />
                    </section>
                    <section>
                        <BookNextStep total={this.state.total} title="Looking forward to serving you" noNextStep={false} moveNext={this.moveNext} setModal={this.mobileSummary} />
                    </section>
                </div>
            );
        } else {
            return (
                <main loader={this.state.paymentLoader ? "show" : "hide"}><Loader /></main>
            );
        }
    }
    mobileSummary(boolVal) {
        this.setState({
            showMobileSummary: boolVal
        })
    }
    componentWillReceiveProps(newProps) {
        if (status == null) {
            if ((newProps.formCurrentStep == 3) && (newProps.formCurrentStep !== this.props.formCurrentStep)) {
                var payment = this.state.payment;
                if (payment == "") {
                    payment = "credit";
                    this.setState({
                        payment: payment
                    });
                    this.applyCreditCardCoupon(payment);
                }
            }
            if (newProps.setUserArea !== this.props.setUserArea) {
                this.setState({
                    input_address_area: newProps.setUserArea
                })
            }
            /*if (newProps.pricePlan !== this.props.pricePlan) {
                WATER_TANK_CLEANING_PRICE = newProps.pricePlan.planDetail.packages[0].price
                this.setState({
                    total: WATER_TANK_CLEANING_PRICE,
                    price: WATER_TANK_CLEANING_PRICE,
                })
            }*/
        }
    }
    componentDidUpdate(prevProps, prevState) {
        var {numberOfUnits, booking_date,voucherCode, payment} = this.state;
        var city = current_city;
        var prices = this.calculatePrice();
        var new_price = this.state.price;

        if (prevState.total != prices.total) {
            this.setState({
                price: prices.subtotal,
                total: prices.total,
                subtotal: prices.subtotal,
                vat: prices.vat,
                walletUsed: prices.walletUsed
            })
            new_price = prices.subtotal;
        }

        //console.log(prevState.booking_date , booking_date);

        if((prevState.voucherCode !== voucherCode || prevState.booking_date !== booking_date || prevState.payment !== payment) && ( voucherCode != "" ) ){
            var prices = this.calculatePrice(true);
            new_price = prices.subtotal;
            this.handleVoucherChange(voucherCode, new_price);
        }
    }
    applyCreditCardCoupon(value) {
        var returnData = this.props.applyCreditCardCoupon(value, this.state.voucherCode);
        var {voucherCode} = this.state;
        var applyCreditCardCoupon = false;
        var payment = typeof value != "undefined" ? value : this.state.payment;
        if (this.state.payment == "") {
            this.setState({
                payment: 'credit',
            });
            applyCreditCardCoupon = true
        } else {
            applyCreditCardCoupon = (payment != "" && payment == "credit") ? true : false;
        }
        if (this.props.isCreditCardCouponApplied && (voucherCode != "" && voucherCode == CREDITCARD_COUPON)) {
            this.setState({
                voucherCode: ""
            });
        } else {
            if (!this.props.isCreditCardCouponApplied && ((applyCreditCardCoupon && payment == "credit") && (voucherCode == "" || voucherCode == CREDITCARD_COUPON))) {
                if (voucherCode == "") {
                    this.setState({
                        voucherCode: CREDITCARD_COUPON
                    });
                }
            } else {
                if (payment == "cash" && voucherCode == CREDITCARD_COUPON) {
                    this.setState({
                        voucherCode: ""
                    });
                }
            }
        }
    }
    togglePriceListPopUp(state) {
        this.setState({
            showPriceList: state
        })
    }
    showPriceList(){
        return (<DryCleaningPrice />);
    }
    render() {
        const items = this.sumarryItems();

        const showPromo = true;

        const showPrice = false;

        let {total,vat} = this.state;

        var discountData = this.state.discountData;

        var booking_date = this.state.booking_date;

        var isLoading = this.state.loader || this.props.loader;

        const {userProfile, isWalletAvailable} = this.props;

        let userWalletAmount = isWalletAvailable ? userProfile.userWallet.totalAmount : 0;
        
        var bookingSummary = <NewBookingSummary
            items = {items}
            showPromo = {showPromo}
            showPrice = {showPrice}
            total = {total}
            vat = {vat}
            booking_date = {booking_date}
            discountData = {discountData}
            couponValue = {this.state.voucherCode}
            handleCouponChange = {this.handleInputChange}
            showMobileSummary = {this.state.showMobileSummary}
            setModal = {this.mobileSummary}
            formData = {this.state}
            updateTotal = {this.updateTotal}
            isLW = {true}
            walletAvailable={isWalletAvailable}
            userWalletAmount = {userWalletAmount}
            isDryCleaning = {true}
        />;

        if (isLoading) {
            return (
                <React.Fragment>
                    <div className="col-lg-8">
                        <main loader={isLoading ? "show" : "hide"}><Loader /></main>
                    </div>
                    <div className="col-md-3 ml-auto">
                        {bookingSummary}
                    </div>
                </React.Fragment>
            );

        } else {

            return (
                <React.Fragment>
                    <div className="col-lg-8">
                        {isWalletAvailable ?
                            <div className='wallet-div'>
                                <img src={"../../../../dist/images/wallet-filled-money-tool-light.png"} className="mh-100" height="28" alt="" />
                                <span className='margin-left30' style={{ verticalAlign: 'middle', fontSize: '15px' }}>{stringConstants.YOU_HAVE_TXT}
                                    <span className='wallet-amount-span'>{this.props.userProfile.userWallet.currency.code + ' ' + this.props.userProfile.userWallet.totalAmount} </span>{stringConstants.WALLET_BALANCE_TXT}
                                </span>
                            </div>
                            : null
                        }
                        {this.cleaningRequest()}
                        {this.ContactDetails()}
                        {this.PaymentSection()}
                    </div>
                    <div className="col-md-3 ml-auto">
                        {bookingSummary}
                    </div>
                    {this.state.showPriceList && <GeneralModal 
                        title="Laundry &amp; Dry Cleaning Pricing"
                        headerClass="bg-primary"
                        titleClass="text-white h3 m-0"
                        modalClass="price-list"
                        showHeader={true} 
                        modalSize="modal-lg" 
                        modalBody={this.showPriceList()}
                        setModal={this.togglePriceListPopUp} />}
                </React.Fragment>
            )
        }
    }
}
function mapStateToProps(state) {
    return {
        myCreditCardsData: state.myCreditCardsData,
        showLoginMenu: state.showLoginMenu,
        currentCity: state.currentCity,
        currentCityData: state.currentCityData,
        lang: state.lang
    }
}
export default withCookies(connect(mapStateToProps)(BookDryCleaningPage));