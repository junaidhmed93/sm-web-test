import React from "react";
import FormFieldsTitle from "../../../components/FormFieldsTitle";
import CheckRadioBoxInput from "../../../components/Form_Fields/CheckRadioBoxInput";
import CheckRadioBoxInputWithPrice from "../../../components/Form_Fields/CheckRadioBoxInputWithPrice";
import CleaningDatePick from "../../../components/Form_Fields/CleaningDatePick";
import TextareaInput from "../../../components/Form_Fields/TextareaInput";
import PaymentMethods from "../../../components/Form_Fields/PaymentMethods";
import NewBookingSummary from "../../../components/Form_Fields/NewBookingSummary";
import BookNextStep from "../../../components/Form_Fields/BookNextStep";
import ContactDetailsStep from "../../../components/ContactDetailsStep";
import {connect} from "react-redux";
import TaxCalculator from "./TaxCalculator"
import locationHelper from "../../../helpers/locationHelper";
import commonHelper from "../../../helpers/commonHelper";
import GeneralModal from "../../../components/GeneralModal";
import Loader from "../../../components/Loader";
import {
    scrollToTop,
    zendeskChatBox
} from "../../../actions";
import {withCookies} from "react-cookie";

let allServiceConstant = {};
let URLConstant = {};
var currentCurrency = "";
let current_city = "dubai";
let bookingFrequencyDataConstants = {};
let dataValues = {}
var status = null;
var bid = null;
var tt = null;

class BookWindowCleaningPage extends React.Component{

    constructor(props) {
        super(props);
        var pricingPlan = props.pricingPlan;
        var bookingData = {
            showAlert : true,
            windowSize: {},
            field_service_needed: "",
            booking_date:'',
            input_email: props.userProfile.email,
            input_phone: props.userProfile.address ? props.userProfile.address.phoneNumber : '',
            input_name: props.userProfile.customerFirstName,
            input_last_name: props.userProfile.customerLastName,
            input_address_city: '',
            input_address_area: props.setUserArea,
            input_address_area_building_name: props.userProfile.address ? props.userProfile.address.building : '',
            input_address_area_building_apartment: props.userProfile.address ? props.userProfile.address.apartment : '',
            details:'',
            payment: '',
            showMobileSummary: false,
            pricingPlan : pricingPlan,
            discountData:{},
            couponCode:"",
            subtotal:0,
            vat:0,
            totalAfterDiscount:0,
            total:0,
            price: 0,
            softSafeBookingId:'',
            softSafeRequestId:'',
            isCreditCouponRedeemed: false,
            planId: pricingPlan.id,
            loader : false
        };

        var cookieData = props.getCookieFormData();

        if(Object.keys(cookieData).length){ // restore from cookies
            bookingData = cookieData;
            bookingData["softSafeBookingId"] = "";
            bookingData["softSafeRequestId"] = "";
            props.removeCookie("booking_data");
            props.removeCookie("submitted_data");
            props.removeCookie("confirmation_summary");
        }

        this.state = bookingData;

        this.ContactDetails = this.ContactDetails.bind(this);
        this.WindowCleaningRequest = this.WindowCleaningRequest.bind(this);
        this.handleShowMoreOptionChange = this.handleShowMoreOptionChange.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleDropDownChange = this.handleDropDownChange.bind(this);
        this.currentStep = this.currentStep.bind(this);
        this.moveNext = this.moveNext.bind(this);
        this.hashChangeHandler = this.hashChangeHandler.bind(this);
        this.calculatePrice = this.calculatePrice.bind(this);
        this.mobileSummary = this.mobileSummary.bind(this);
        this.handleCouponChange = this.handleCouponChange.bind(this);
        this.handleHomeSizeSelectionChange = this.handleHomeSizeSelectionChange.bind(this);
        this.showOrHideModalForBookingInspection = this.showOrHideModalForBookingInspection.bind(this);
        this.applyCreditCardCoupon = this.applyCreditCardCoupon.bind(this);

    }

    componentDidMount() {
        allServiceConstant = this.props.service_constants;
        URLConstant =  this.props.url_constants;
        current_city = this.props.current_city;
        bookingFrequencyDataConstants = this.props.dataConstants["BOOKING_FREQUENCY"];
        dataValues = commonHelper.processDataValues(bookingFrequencyDataConstants);
        var cityOptions = locationHelper.getLocationByName(current_city);

        const url_params = this.props.url_params;

        status = commonHelper.getParameterByName("status");
        bid = commonHelper.getParameterByName("bid");
        tt = commonHelper.getParameterByName("tt");

        if(status == null ) {
            this.setState({
                input_address_city: cityOptions
            })
        }
        currentCurrency = locationHelper.getCurrentCurrency();
        zendeskChatBox();
        window.addEventListener("hashchange", this.hashChangeHandler, false);

    }
    componentWillUnmount(){
        allServiceConstant = this.props.service_constants;
        URLConstant =  this.props.url_constants;
        current_city = this.props.current_city;
        window.removeEventListener("hashchange", this.hashChangeHandler, false);
    }
    calculatePrice(){

        var {windowSize, booking_date,couponCode} = this.state,
            changed_element = null , changed_value = null;


        var total_price = windowSize && typeof windowSize.price != "undefined" ? windowSize.price : 0,
        taxRate = 0,
        updatedPrice = false,
        total = 0,
        taxChargesTotal = 0;

        if( total_price != 0 ) {

            taxChargesTotal = TaxCalculator.calculateVAT(total_price, booking_date);

            total = total_price + taxChargesTotal;
        }

       return {
            price: total_price,
            total: total,
            subtotal : total_price,
            vat:taxChargesTotal
        };
    }
    handleCouponChange(value, newPrice = 0){
        this.props.handleCouponChange(value, this.state, newPrice);
    }

    sumarryItems(){
        const {windowSize, field_service_needed, booking_date,discountData,subtotal, vat, payment, total} = this.state;

        let timeValue = typeof field_service_needed.label !="undefined" ? field_service_needed.label+"\n" : "";

        if( booking_date != "" ){
            timeValue += field_service_needed.value == dataValues.DATA_VALUE_ONCE ? " On "+booking_date : " Starting "+booking_date;
        }
        
        let jobDetails = [];

        if(typeof windowSize.label != "undefined"){
            jobDetails.push("Villa - "+windowSize.label);
            jobDetails.push(<span className="text-warning">{currentCurrency + " "+windowSize.price}</span>);
        }

       //jobDetails.push(<span className="text-warning">{ currentCurrency + " " + hourlyRate+" /hour" } { locationHelper.getCurrentCountryId() != QATAR_ID ? " + VAT" : ""}</span>);
 
       var items = [
            {label: 'Time', value: timeValue},
            {label: 'Details', value: jobDetails, conClass: 'col-12 mb-4'}
       ];

       
        if( total != 0 ) {
            if (typeof discountData.promoCodeDiscountText != "undefined") {
                items.push({label: 'Promo code', value: discountData.promoCodeDiscountText});
                items.push({label: 'Subtotal', value: currentCurrency + " " + subtotal, text_strike: "yes"});
            } else {
                items.push({label: 'Subtotal', value: currentCurrency + " " + subtotal, classNme:'text-warning'});
            }
        }

        items.push({label: 'Payment', value: payment});

        return items;
    }
    hashChangeHandler(){
        let hashVal = window.location.hash;
        let step;
        hashVal.length ? step=parseInt( window.location.hash.replace('#','') ) : step = 1;
        this.props.moveNext(step);
        scrollToTop();
    }
    currentStep(){
        return this.props.formCurrentStep;
    }
    moveNext(step){
        const {formCurrentStep, signInDetails, showLoginModal, showLoginMenu, moveNextStep} = this.props;
        moveNextStep(step, this.state, true);

        /*var valid = true;
        // Step 1 validation
        if( formCurrentStep == '1' ){
            valid = isValidSection('section-request-form');
            if( valid ){
                window.location.hash = step;
                scrollToTop();
                // Show Login Modal
                if( !signInDetails.customer ){
                    showLoginModal();
                }
            }
        }
        // Step 2
        if( formCurrentStep == '2' ){
            valid = isValidSection('personal-information-form');
            if( valid ){
                window.location.hash = step;
                scrollToTop();
                // Show Login Modal
                if( !signInDetails.customer ){
                    showLoginModal();
                }
            }
        }*/
    }
    handleShowMoreOptionChange(){
        this.setState({
            show_more_option: !this.state.show_more_option
        });
    }
   handleHomeSizeSelectionChange(name,value){
        if(value.showDialogForInspectionBooking){
            this.showOrHideModalForBookingInspection(true);
        }else{
            this.handleInputChange(name,value);
        }
    }
    handleInputChange(name, value){
        if(name == "payment"){
            this.applyCreditCardCoupon(value)
        }
        this.setState({
            [name]: value
        });
    }
    handleDropDownChange(name, value){
        // console.log(name, value);
    }
    WindowCleaningRequest(){
        const pricingPlan = this.state.pricingPlan;

        var windowSizeOptions = [];

        if(pricingPlan.planChargesDtoList){
            pricingPlan.planChargesDtoList.map(function(item,index){
                const sizeObject = {};
                sizeObject.id = index;
                sizeObject.value = item.constant;
                sizeObject.label = item.constant.replace(/_/g,' ');;
                sizeObject.price = item.rate ;
                sizeObject.currentCurrency = currentCurrency;
                sizeObject.isPerVisit = true;
                windowSizeOptions.push(sizeObject);
            });
        }
                
        var option6BRandAbove= {};
        option6BRandAbove.id = 10;
        option6BRandAbove.value = "6_BR";
        option6BRandAbove.showDialogForInspectionBooking = true;
        option6BRandAbove.label = "6 BR+"
        option6BRandAbove.price = "Price confirmed after inspection" ;
        option6BRandAbove.currentCurrency = "";
        windowSizeOptions.push(option6BRandAbove);

        var cleaning_frequency = [
            {id: "1", value: dataValues.DATA_VALUE_EVERY_MONTH, label: "Every month"},
            {id: "2", value: dataValues.DATA_VALUE_EVERY_3_MONTH, label: "Every 3 months"},
            {id: "3", value: dataValues.DATA_VALUE_EVERY_6_MONTH, label: "Every 6 months"},
            {id: "4", value: dataValues.DATA_VALUE_ONCE, label: "One time only"}
        ];

        var windowSize_value = this.state.windowSize;

        var service_needed_value = this.state.field_service_needed;

        //other_services
        var currentStepClass = this.currentStep() === 1 ? '' : "d-none";

        return (
            <div id="section-request-form" className={currentStepClass}>
               <section>
                    {this.props.showDiscountNotification()} 
                    {this.state.showAlert && <div className="alert alert-primary mb-5" role="alert">
                        <div className="row">
                            <div className="col-1">
                                <i className="fa fa-info-circle mt-3 ml-3"></i>
                            </div>
                            <div className="col">
                                <p className="mb-0">This service is for window cleaning of villas and townhouses</p>
                                <p className="mb-0">It includes both external cleaning using high-pressure brushes and interior cleaning</p>
                            </div>
                            <div className="col-1">
                                <button type="button" className="close" data-dismiss="alert" aria-label="Close" onClick={()=>{this.setState({showAlert:false})}}>
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>}
                    <FormFieldsTitle title="What is the size of your home?" />
                    <CheckRadioBoxInputWithPrice
                        InputType="radio"
                        name="windowSize"
                        inputValue={windowSize_value}
                        items={windowSizeOptions}
                        onInputChange={this.handleHomeSizeSelectionChange}
                        validationClasses="radio-required"
                        validationMessage = "Please select size of your home"
                        parentClass="row mb-2"
                        childClass="col-12 col-md-3 mb-3 d-flex min-height-100"
                        moreTaxText="Per visit" />
                </section>

                <section>
                    <FormFieldsTitle title="How often do you require window cleaning?" />
                    <CheckRadioBoxInput
                        InputType="radio"
                        name="field_service_needed"
                        inputValue={service_needed_value}
                        items={cleaning_frequency}
                        onInputChange={this.handleInputChange}
                        validationClasses="radio-required"
                        parentClass="row mb-2"
                        childClass="col-6 col-sm-3 mb-3 d-flex"
                        validationMessage = "Please select how often do you require service"
                         />
                </section>
                <section>
                    <FormFieldsTitle title="Pick your start date" />
                    <div className="row mb-4">

                    <CleaningDatePick
                        inputValue={this.state.booking_date}
                        name="booking_date"
                        show_error= ""
                        service_needed={this.state.windowSize}
                        dataValues={dataValues}
                        startDateThresholdIfTodayIsDisabled={2}
                        disableFriday={true}
                        onInputChange={this.handleInputChange}
                        childClass="col-12 col-sm-6 date-container"
                        validationClasses="required"
                        validationMessage = "Please select date for your service"
                    />

                    </div>
                </section>
                <section>
                    <FormFieldsTitle title="Special instructions or requests? (Optional)" />
                    <div className="row mb-4">
                        <div className="col mb-3">
                            <TextareaInput name="details" inputValue={this.state.details} placeholder="Example: The keys are under the door mat, and the money is on the kitchen counter." onInputChange={this.handleInputChange}  />
                        </div>
                    </div>
                </section>
                <section>
                    <BookNextStep total={this.state.total} title="Contact Details" moveNext={this.moveNext} toStep="2" setModal={this.mobileSummary}/>
                </section>
            </div>
        )
    }
    ContactDetails(isLocationDetailShow){
        var cityOptions = locationHelper.getLocationByName(current_city);

        var currentStepClass = this.currentStep() === 2 ? '' : "d-none";
        //var currentStepClass =  ''
        var isLocationDetailShow = "yes";

        var contact_details = {
            input_email:this.state.input_email,
            input_phone:this.state.input_phone,
            input_name:this.state.input_name,
            input_last_name:this.state.input_last_name
        }

        if(isLocationDetailShow){
            contact_details.input_address_city = this.state.input_address_city != "" ? this.state.input_address_city : cityOptions;
            contact_details.input_address_area = this.state.input_address_area;
            contact_details.input_address_area_building_name = this.state.input_address_area_building_name;
            contact_details.input_address_area_building_apartment = this.state.input_address_area_building_apartment;
        }

        var userProfile =  this.props.userProfile;

        return (
            <div id="personal-information-form" className={currentStepClass}>
                <ContactDetailsStep
                    cityOptions={cityOptions}
                    userProfile = {userProfile}
                    signInDetails={this.props.signInDetails}
                    isLocationDetailShow={isLocationDetailShow}
                    contact_details={contact_details}
                    handleDropDownChange={this.handleInputChange}
                    handleInputChange={this.handleInputChange}
                    mobileSummary={this.mobileSummary}
                    moveNext={this.moveNext}
                    toStep={3}
                    isBooking={true}
                    currentCity = {this.props.current_city}
                />
            </div>
        )
    }
    PaymentSection(){
        var payment = this.state.payment;
        var currentStepClass = this.currentStep() === 3 ? '' : "d-none";
        //var currentStepClass ='';
        if(!this.state.paymentLoader) {

            return (
                <div id="payment-method-form" className={currentStepClass}>
                    <section className="payment">
                        <PaymentMethods 
                        isCreditCardCouponApplied = {true}
                        name="payment" inputValue={payment} status={status} tt={tt} onInputChange={this.handleInputChange} />
                    </section>
                    <section>
                        <BookNextStep total={this.state.total} title="Looking forward to serving you" noNextStep={false} moveNext={this.moveNext} setModal={this.mobileSummary} />
                    </section>
                </div>
            );
        } else {
            return (
                <main loader={this.state.paymentLoader ? "show" : "hide"}><Loader/></main>
            );
        }
    }
    mobileSummary(boolVal){
        this.setState({
            showMobileSummary: boolVal
        })
    }    
    showOrHideModalForBookingInspection(boolVal){
        this.setState({
            showBookingInspectionModal: boolVal
        })
    }  
    getBookingInspectionModalContent(){
        return (<p>We can still help you! For bigger jobs our service partner will 
            perform a quick inspection and verify the price with you before the job. 
            Call us on <a href="tel://0097144229639" className="ltr">+971 4 4229639</a> to book an inspection.</p>);
    }
    componentDidUpdate(prevProps, prevState) {
        var {windowSize, field_service_needed, booking_date,couponCode} = this.state;
        var city = current_city;
        var prices = this.calculatePrice();
        var new_price = this.state.price;

        if(prevState.total != prices.total) {
            this.setState({
                price: prices.subtotal,
                total: prices.total,
                subtotal: prices.subtotal,
                vat: prices.vat
            })
           // new_price = prices.subtotal;
        }
        if((prevState.windowSize !== windowSize ||
            prevState.field_service_needed !== field_service_needed ||
            prevState.couponCode !== couponCode ||
            prevState.booking_date !== booking_date) &&
            ( couponCode != "" && new_price != 0) ){
            // console.log("componentDidUpdate", couponCode);

            this.handleCouponChange(couponCode, prices.price);
        }

    }
    applyCreditCardCoupon(value){
        if( status == null) {
            var applyCreditCardCoupon = false;
            var couponCode = this.state.couponCode;

            var payment = typeof value != "undefined" ? value : this.state.payment;

            if (this.state.payment == "") {
                this.setState({
                    payment: 'credit',
                });
            }
        }
        /*else{
            applyCreditCardCoupon = ( payment != "" && payment == "credit" ) ? true : false;
        }

        if( (applyCreditCardCoupon && payment == "credit" ) && (couponCode == "" || couponCode == CREDITCARD_COUPON)){
            if(couponCode == "") {
                this.setState({
                    couponCode: CREDITCARD_COUPON
                });
            }
        }else{
            //console.log("applyCreditCardCoupon else");
            if(payment == "cash" && couponCode == CREDITCARD_COUPON){
                this.setState({
                    couponCode: ""
                });
            }
        }*/
    }
    componentWillReceiveProps (newProps) {
        if( status == null) {
            if((newProps.isCreditCardCouponApplied !== this.props.isCreditCardCouponApplied) && newProps.isCreditCardCouponApplied){
                this.setState({
                    couponCode: ""
                });
            }
            if (newProps.softSafeBookingId !== this.props.softSafeBookingId) {
                var payment = this.state.payment;
                if (payment == "") {
                    payment = "credit";
                }
                this.setState({
                    softSafeBookingId: newProps.softSafeBookingId,
                    softSafeRequestId: newProps.softSafeRequestId,
                })
                //Credit card coupon
                this.applyCreditCardCoupon(payment);

            }
            if (newProps.discountData !== this.props.discountData) {
                this.setState({
                    discountData: newProps.discountData,
                })
            }
            if (newProps.discountPrices !== this.props.discountPrices) {
                this.setState(
                    newProps.discountPrices
                );
            }
            if (newProps.setUserArea !== this.props.setUserArea) {
                this.setState({
                    input_address_area: newProps.setUserArea
                })
            }
        }

    }
    render(){
        const items = this.sumarryItems();
        const showPromo = true;
        const showPrice = true;

        var isLoading =  this.state.loader || this.props.loader;

        var discountData = this.state.discountData;

        var booking_date = this.state.booking_date;

        let {total, vat} = this.state

        var bookingSummary = <NewBookingSummary
            items={items}
            showPromo={showPromo}
            showPrice={showPrice}
            total = {total}
            vat = {vat}
            booking_date={booking_date}
            showMobileSummary={this.state.showMobileSummary}
            handleCouponChange = {this.handleInputChange}
            setModal={this.mobileSummary}
            discountData={discountData}
            couponValue = {this.state.couponCode}
            isCoreBooking = {true}
        />;

        if(isLoading) {
            return (
                <React.Fragment>
                    <div className="col-lg-8">
                        <main loader={ isLoading ? "show" : "hide"}><Loader/></main>
                    </div>
                    <div className="col-md-3 ml-auto">
                        {bookingSummary}
                    </div>
                </React.Fragment>
            );

        }else{
            return (
                <React.Fragment>
                    <div className="col-lg-8">
                        {this.WindowCleaningRequest()}
                        {this.ContactDetails()}
                        {this.PaymentSection()}
                    </div>
                    <div className="col-md-3 ml-auto">
                        {bookingSummary}
                    </div>
                    {this.state.showBookingInspectionModal &&
                    <GeneralModal title="Book Inspection" modalBody={this.getBookingInspectionModalContent()}
                                  setModal={this.showOrHideModalForBookingInspection}/>}
                </React.Fragment>
            )
        }
    }
}

function mapStateToProps(state){
    return {
        myCreditCardsData: state.myCreditCardsData,
        userProfile: state.userProfile,
        currentCity: state.currentCity,
        lang: state.lang
    }
}
export default withCookies(connect(mapStateToProps)(BookWindowCleaningPage));