import React from "react";
import FormFieldsTitle from "../../../components/FormFieldsTitle";
import CheckRadioBoxInputWithPrice from "../../../components/Form_Fields/CheckRadioBoxInputWithPrice";
import CleaningDatePick from "../../../components/Form_Fields/CleaningDatePick";
import DaysSelectionComponent from "../../../components/Form_Fields/DaysSelectionComponent";
import TextareaInput from "../../../components/Form_Fields/TextareaInput";
import PaymentMethods from "../../../components/Form_Fields/PaymentMethods";
import NewBookingSummary from "../../../components/Form_Fields/NewBookingSummary";
import BookNextStep from "../../../components/Form_Fields/BookNextStep";
import ContactDetailsStep from "../../../components/ContactDetailsStep";
import {connect} from "react-redux";
import TaxCalculator from "./TaxCalculator"
import locationHelper from "../../../helpers/locationHelper";
import commonHelper from "../../../helpers/commonHelper";
import Loader from "../../../components/Loader";
import {
    scrollToTop,
    zendeskChatBox
} from "../../../actions";
import stringConstants from '../../../constants/stringConstants';
import { withCookies } from "react-cookie";

let allServiceConstant = {};
let URLConstant = {};
var currentCurrency = "";
let current_city = "dubai";
let bookingFrequencyDataConstants = {};
let dataValues = {}
var status = null;
var bid = null;
var tt = null;

class BookPoolCleaningPage extends React.Component {

    constructor(props) {
        super(props);
        var pricingPlan = props.pricingPlan;
        var bookingData = {
            poolSize: "",
            field_service_needed: "",
            field_multiple_times_week: [],
            booking_date: '',
            details: '',
            input_email: props.userProfile.email,
            input_phone: props.userProfile.address ? props.userProfile.address.phoneNumber : '',
            input_name: props.userProfile.customerFirstName,
            input_last_name: props.userProfile.customerLastName,
            input_address_city: '',
            input_address_area: props.setUserArea,
            input_address_area_building_name: props.userProfile.address ? props.userProfile.address.building : '',
            input_address_area_building_apartment: props.userProfile.address ? props.userProfile.address.apartment : '',
            payment: '',
            showMobileSummary: false,
            pricingPlan: pricingPlan,
            discountData: {},
            couponCode: "",
            subtotal: 0,
            vat: 0,
            totalAfterDiscount: 0,
            total: 0,
            price: 0,
            softSafeBookingId: '',
            softSafeRequestId: '',
            isCreditCouponRedeemed: false,
            planId: pricingPlan.id,
            loader: false,
            walletUsed: '',
            isWalletAvailable: false,
        }
        var cookieData = props.getCookieFormData();

        if (Object.keys(cookieData).length) { // restore from cookies
            bookingData = cookieData;
            bookingData["softSafeBookingId"] = "";
            bookingData["softSafeRequestId"] = "";
            props.removeCookie("booking_data");
            props.removeCookie("submitted_data");
            props.removeCookie("confirmation_summary");
        }

        this.state = bookingData;

        this.ContactDetails = this.ContactDetails.bind(this);
        this.PoolCleaningRequest = this.PoolCleaningRequest.bind(this);
        this.handleCustomChange = this.handleCustomChange.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleDropDownChange = this.handleDropDownChange.bind(this);
        this.currentStep = this.currentStep.bind(this);
        this.moveNext = this.moveNext.bind(this);
        this.hashChangeHandler = this.hashChangeHandler.bind(this);
        this.calculatePrice = this.calculatePrice.bind(this);
        this.handleCouponChange = this.handleCouponChange.bind(this);
        this.enableSelectedDaysOnly = this.enableSelectedDaysOnly.bind(this);
        this.mobileSummary = this.mobileSummary.bind(this);
        this.applyCreditCardCoupon = this.applyCreditCardCoupon.bind(this);
        this.updateTotal = this.updateTotal.bind(this);


    }

    componentDidMount() {
        allServiceConstant = this.props.service_constants;
        URLConstant = this.props.url_constants;
        current_city = this.props.current_city;

        bookingFrequencyDataConstants = this.props.dataConstants["BOOKING_FREQUENCY"];

        dataValues = commonHelper.processDataValues(bookingFrequencyDataConstants);

        var cityOptions = locationHelper.getLocationByName(current_city);

        const url_params = this.props.url_params;

        status = commonHelper.getParameterByName("status");
        bid = commonHelper.getParameterByName("bid");
        tt = commonHelper.getParameterByName("tt");

        if (status == null) {

            this.setState({
                field_service_needed: {
                    id: "3",
                    value: dataValues.DATA_VALUE_MULTIPLE_TIMES_WEEK,
                    label: "Several times a week"
                },
                input_address_city: cityOptions
            })
        }
        currentCurrency = locationHelper.getCurrentCurrency();
        const walletAvailable = this.props.userProfile && this.props.userProfile.userWallet && this.props.userProfile.userWallet.currency.code === currentCurrency && this.props.userProfile.userWallet.totalAmount > 0 ? true : false;
        this.setState({ isWalletAvailable: walletAvailable });

        zendeskChatBox();
        window.addEventListener("hashchange", this.hashChangeHandler, false);
    }
    componentWillUnmount() {
        window.removeEventListener("hashchange", this.hashChangeHandler, false);
    }
    sumarryItems(){
        const {poolSize, field_multiple_times_week, booking_date,discountData,subtotal, vat, payment, total, walletUsed } = this.state;
        
        let timeValue = "Every month \n";

        if(field_multiple_times_week.length){
            let field_multiple_times_week_text = field_multiple_times_week.map( item => item.label).join(", ");
            timeValue += field_multiple_times_week_text+"\n";
        }
        if( booking_date !="" ){
            timeValue += " Starting "+booking_date;
        }
        let jobDetails = [];
        if(typeof poolSize.label !="undefined"){
            jobDetails.push(poolSize.label);
            jobDetails.push(<span className="text-warning">{currentCurrency+" "+poolSize.price}</span>);
        }

        var items =  [
            {label: 'Time', value: timeValue}
        ]; 

        if(jobDetails.length){
            items.push({label: 'Details', value: jobDetails, conClass: 'col-12 mb-4'});
        }
        if( total != 0 ){
            if(typeof discountData.promoCodeDiscountText != "undefined"){
                items.push({label: 'Promo code', value: discountData.promoCodeDiscountText});
                items.push({label: 'Subtotal', value: currentCurrency+" "+subtotal, text_strike:"yes"});
            }else{
                items.push({ label: 'Subtotal', value: currentCurrency + " " + subtotal });
            }
            /*if (vat) {
                items.push(
                    { label: 'VAT', value: currentCurrency + " " + vat },
                );
            }*/
        }

        items.push({ label: 'Payment', value: payment });

        return items;

    }
    hashChangeHandler() {
        let hashVal = window.location.hash;
        let step;
        hashVal.length ? step = parseInt(window.location.hash.replace('#', '')) : step = 1;
        this.props.moveNext(step);
        scrollToTop();
    }
    currentStep() {
        return this.props.formCurrentStep;
    }
    calculatePrice() {

        var {
            poolSize,
            field_multiple_times_week,
            booking_date,
            couponCode,
            isWalletAvailable
        } = this.state;
            //changed_element = null, changed_value = null;


        var total_price = poolSize ? poolSize.price : 0;
        var taxRate = 0;
        var updatedPrice = false;

        let walletUsed = 0;
        if ( isWalletAvailable) {
            const walletAmount = this.props.userProfile.userWallet.totalAmount;
            const walletCurrency = this.props.userProfile.userWallet.currency.code;

            if (walletCurrency === currentCurrency && walletAmount > 0) {
                if (walletAmount >= total_price) {
                    walletUsed = total_price;
                    total_price = 0;
                }
                else if (walletAmount < total_price) {
                    walletUsed = walletAmount;
                    total_price = total_price - walletAmount;
                }

            }

        }

        var taxChargesTotal = TaxCalculator.calculateVAT(total_price, booking_date);

        var total = total_price + taxChargesTotal;

        if (this.props.userProfile && this.props.userProfile.userWallet) {
            const walletAmount = this.props.userProfile.userWallet.totalAmount;
            const walletCurrency = this.props.userProfile.userWallet.currency.code;

            if (walletCurrency === currentCurrency && walletAmount > 0) {

                total = total > 0 ? total : 0.00;
                //this.setState({walletUsed: walletUsed});
            }
        }

        return {
            couponCode: couponCode,
            price: total_price,
            total: total,
            subtotal: total_price,
            vat: taxChargesTotal,
            walletUsed: walletUsed
        };
    }
    handleCouponChange(value, changed_element = null, changed_value = null, new_price = 0) {
        this.props.handleCouponChange(value, this.state);
    }


    updateTotal(res) {
        let { walletUsed, booking_date } = this.state;
        let response = res;
        let discountData = response.data;
        let currentTotal = response.currentTotal;
        let taxChargesTotal = response.taxChargesTotal;
        let total = response.total;

        if (walletUsed > 0) {
            currentTotal = parseFloat((response.totalAfterDiscount - walletUsed).toFixed(2));
            taxChargesTotal = TaxCalculator.calculateVAT(currentTotal, booking_date);
            total = parseFloat((currentTotal + taxChargesTotal).toFixed(2));

        }


        this.setState({
            price: currentTotal,
            total: total,
            subtotal: currentTotal,
            vat: taxChargesTotal,
            discountData: discountData,
            walletUsed: walletUsed
        });
    }

    moveNext(step) {
        const { moveNextStep } = this.props;
        moveNextStep(step, this.state, true);
    }

    handleCustomChange(name, value) {
        if (name == "payment") {
            this.applyCreditCardCoupon(value)
        }
        this.setState({
            booking_time: value
        });
    }
    handleInputChange(name, value) {
        this.setState({
            [name]: value
        });

        this.calculatePrice(name, value);
    }
    handleDropDownChange(name, value) {
        // console.log(name, value);
    }
    PoolCleaningRequest() {

        const pricingPlan = this.state.pricingPlan;
        var poolSizeOptions = [];
        if (pricingPlan.planChargesDtoList) {
            pricingPlan.planChargesDtoList.map(function (item, index) {
                const poolSizeObject = {};
                poolSizeObject.id = index;
                poolSizeObject.value = item.constant;
                poolSizeObject.label = item.constant.replace(/_/g, ' ');;
                poolSizeObject.price = item.rate;
                poolSizeObject.currentCurrency = currentCurrency;
                poolSizeOptions.push(poolSizeObject);
            });


        }

        var field_multiple_times_week = [
            { id: "1", value: "7", label: "Sunday" },
            { id: "2", value: "1", label: "Monday" },
            { id: "3", value: "2", label: "Tuesday" },
            { id: "4", value: "3", label: "Wednesday" },
            { id: "5", value: "4", label: "Thursday" },
            { id: "6", value: "6", label: "Saturday" }
        ];

        var poolSizeValue = this.state.poolSize;

        //other_services
        var currentStepClass = this.currentStep() === 1 ? '' : "d-none";

        var cleaningDays = this.state.field_multiple_times_week;

        var showError = cleaningDays.length == 2 ? false : true;

        return (
            <div id="section-request-form" className={currentStepClass}>
                {this.props.showDiscountNotification()}
                <section>
                    <FormFieldsTitle title="What size is your pool?" />
                    <CheckRadioBoxInputWithPrice
                        InputType="radio"
                        name="poolSize"
                        inputValue={poolSizeValue}
                        items={poolSizeOptions}
                        onInputChange={this.handleInputChange}
                        validationClasses="radio-required"
                        validationMessage="Please Select size of the pool"
                        parentClass="row mb-2"
                        childClass="col-12 col-md-4 mb-3 d-flex min-height-100" />

                </section>
                <section>
                    <FormFieldsTitle title="Select the 2 days of the week for cleaning" />
                    <DaysSelectionComponent InputType="checkbox"
                        inputValue={this.state.field_multiple_times_week} name="field_multiple_times_week"
                        items={field_multiple_times_week} onInputChange={this.handleInputChange}
                        maxSelectionCount={2}
                        parentClass="row mb-2"
                        childClass="col-6 col-md-4 col-sm-3 mb-3 d-flex"
                        validationClasses="min-check-2"
                        validationMessage="Please select the weekdays would you like cleaning"
                    />
                </section>
                <section>
                    <FormFieldsTitle title="Pick your start date" />
                    <div className="row mb-4">
                        {
                            <CleaningDatePick name="booking_date"
                                show_error={showError}
                                inputValue={this.state.booking_date}
                                dataValues={dataValues}
                                service_needed={poolSizeValue}
                                startDateThresholdIfTodayIsDisabled={2}
                                filterDate={this.enableSelectedDaysOnly}
                                onInputChange={this.handleInputChange}
                                childClass="col-12 col-sm-6 date-container"
                                validationClasses="required"
                                validationMessage="Please select date for your service"
                            />

                        }
                    </div>
                </section>
                <section>
                    <FormFieldsTitle title="Special instructions or requests? (Optional)" />
                    <div className="row mb-4">
                        <div className="col mb-3">
                            <TextareaInput name="details" inputValue={this.state.details} placeholder="Example: The keys are under the door mat, and the money is on the kitchen counter." onInputChange={this.handleInputChange} />
                        </div>
                    </div>
                </section>
                <section>
                    <BookNextStep total={this.state.total} title="Contact Details" moveNext={this.moveNext} toStep="2" setModal={this.mobileSummary} />
                </section>
            </div>
        )
    }
    enableSelectedDaysOnly(date) {
        const day = date.day();
        var fieldMultipleTimesWeekValues = this.state.field_multiple_times_week;

        if (fieldMultipleTimesWeekValues && fieldMultipleTimesWeekValues.length > 0) {
            for (const selectedDay of fieldMultipleTimesWeekValues) {
                if (selectedDay.value == day || (selectedDay.value == 7 && day == 0)) {
                    return true;
                }
            }
        }
        return false;
    }
    ContactDetails(isLocationDetailShow) {
        var cityOptions = locationHelper.getLocationByName(current_city);

        var currentStepClass = this.currentStep() === 2 ? '' : "d-none";
        //var currentStepClass =  ''
        var isLocationDetailShow = "yes";

        var contact_details = {
            input_email: this.state.input_email,
            input_phone: this.state.input_phone,
            input_name: this.state.input_name,
            input_last_name: this.state.input_last_name
        }

        if (isLocationDetailShow) {
            contact_details.input_address_city = this.state.input_address_city != "" ? this.state.input_address_city : cityOptions;
            contact_details.input_address_area = this.state.input_address_area;
            contact_details.input_address_area_building_name = this.state.input_address_area_building_name;
            contact_details.input_address_area_building_apartment = this.state.input_address_area_building_apartment;
        }

        var userProfile = this.props.userProfile;

        return (
            <div id="personal-information-form" className={currentStepClass}>
                <ContactDetailsStep
                    cityOptions={cityOptions}
                    userProfile={userProfile}
                    signInDetails={this.props.signInDetails}
                    isLocationDetailShow={isLocationDetailShow}
                    contact_details={contact_details}
                    handleDropDownChange={this.handleInputChange}
                    handleInputChange={this.handleInputChange}
                    mobileSummary={this.mobileSummary}
                    moveNext={this.moveNext}
                    toStep={3}
                    isBooking={true}
                    currentCity={this.props.current_city}
                />
            </div>
        )
    }
    PaymentSection() {
        var payment = this.state.payment;
        var currentStepClass = this.currentStep() === 3 ? '' : "d-none";
        //var currentStepClass ='';
        if (!this.state.paymentLoader) {

            return (
                <div id="payment-method-form" className={currentStepClass}>
                    <section className="payment">
                        <PaymentMethods
                            isCreditCardCouponApplied={true}
                            name="payment" inputValue={payment} status={status} tt={tt} onInputChange={this.handleInputChange} />
                    </section>
                    <section>
                        <BookNextStep total={this.state.total} title="Looking forward to serving you" noNextStep={false} moveNext={this.moveNext} setModal={this.mobileSummary} />
                    </section>
                </div>
            );
        } else {
            return (
                <main loader={this.state.paymentLoader ? "show" : "hide"}><Loader /></main>
            );
        }
    }
    mobileSummary(boolVal) {
        this.setState({
            showMobileSummary: boolVal
        })
    }
    componentDidUpdate(prevProps, prevState) {
        if (status == null) {
            var { poolSize, field_service_needed, booking_date, couponCode } = this.state;
            var city = current_city;
            var prices = this.calculatePrice();
            var new_price = this.state.price;

            if (prevState.total != prices.total) {
                this.setState({
                    price: prices.subtotal,
                    total: prices.total,
                    subtotal: prices.subtotal,
                    vat: prices.vat,
                    walletUsed: prices.walletUsed
                })
                // new_price = prices.subtotal;
            }
            if ((prevState.poolSize !== poolSize ||
                prevState.field_service_needed !== field_service_needed ||
                prevState.couponCode !== couponCode ||
                prevState.booking_date !== booking_date) &&
                (couponCode != "" && new_price != 0)) {
                // console.log("componentDidUpdate", couponCode);

                this.handleCouponChange(couponCode, new_price);
            }
        }

    }
    componentWillReceiveProps(newProps) {
        if (status == null) {
            if ((newProps.isCreditCardCouponApplied !== this.props.isCreditCardCouponApplied) && newProps.isCreditCardCouponApplied) {
                this.setState({
                    couponCode: ""
                });
            }
            if (newProps.softSafeBookingId !== this.props.softSafeBookingId) {
                var payment = this.state.payment;
                if (payment == "") {
                    payment = "credit";
                }
                this.setState({
                    softSafeBookingId: newProps.softSafeBookingId,
                    softSafeRequestId: newProps.softSafeRequestId,
                })
                //Credit card coupon
                this.applyCreditCardCoupon(payment);

            }
            if (newProps.discountData !== this.props.discountData) {
                this.setState({
                    discountData: newProps.discountData,
                })
            }
            if (newProps.discountPrices !== this.props.discountPrices) {
                this.setState(
                    newProps.discountPrices
                );
            }
            if (newProps.setUserArea !== this.props.setUserArea) {
                this.setState({
                    input_address_area: newProps.setUserArea
                })
            }
        }
    }
    applyCreditCardCoupon(value) {
        var applyCreditCardCoupon = false;
        var couponCode = this.state.couponCode;

        var payment = typeof value != "undefined" ? value : this.state.payment;

        if (this.state.payment == "") {
            this.setState({
                payment: 'credit',
            });
            //applyCreditCardCoupon = true
        }/*else{
         applyCreditCardCoupon = ( payment != "" && payment == "credit" ) ? true : false;
         }

         if( (applyCreditCardCoupon && payment == "credit" ) && (couponCode == "" || couponCode == CREDITCARD_COUPON)){
         if(couponCode == "") {
         this.setState({
         couponCode: CREDITCARD_COUPON
         });
         }
         }else{
         //console.log("applyCreditCardCoupon else");
         if(payment == "cash" && couponCode == CREDITCARD_COUPON){
         this.setState({
         couponCode: ""
         });
         }
         }*/
    }
    render() {
        const items = this.sumarryItems();
        const showPromo = true;
        const showPrice = true;

        var isLoading = this.state.loader || this.props.loader;

        var discountData = this.state.discountData;

        var booking_date = this.state.booking_date;

        const {isWalletAvailable} = this.props;

        let {total, vat} = this.state;

        var bookingSummary = <NewBookingSummary
            items = {items}
            showPromo = {showPromo}
            showPrice = {showPrice}
            total = {total}
            vat = {vat}
            items={items}
            showPromo={showPromo}
            showPrice={showPrice}
            booking_date={booking_date}
            showMobileSummary={this.state.showMobileSummary}
            handleCouponChange={this.handleInputChange}
            setModal={this.mobileSummary}
            discountData={discountData}
            couponValue={this.state.couponCode}
            isCoreBooking={true}
            walletAvailable={isWalletAvailable}
        />;

        var isLoading = this.state.loader || this.props.loader;

        if (isLoading) {
            return (
                <React.Fragment>
                    <div className="col-lg-8">
                        <main loader={isLoading ? "show" : "hide"}><Loader /></main>
                    </div>
                    <div className="col-md-3 ml-auto">
                        {bookingSummary}
                    </div>
                </React.Fragment>
            );

        } else {
            return (
                <React.Fragment>
                    <div className="col-lg-8">
                        {isWalletAvailable ?
                            <div className='wallet-div'>
                                <img src={"../../../../dist/images/wallet-filled-money-tool-light.png"} className="mh-100" height="28" alt="" />
                                <span className='margin-left30' style={{ verticalAlign: 'middle', fontSize: '15px' }}>{stringConstants.YOU_HAVE_TXT}
                                    <span className='wallet-amount-span'>{this.props.userProfile.userWallet.currency.code + ' ' + this.props.userProfile.userWallet.totalAmount} </span>{stringConstants.WALLET_BALANCE_TXT}
                                </span>
                            </div>
                            : null
                        }
                        {this.PoolCleaningRequest()}
                        {this.ContactDetails()}
                        {this.PaymentSection()}
                    </div>
                    <div className="col-md-3 ml-auto">
                        {bookingSummary}
                    </div>
                </React.Fragment>
            )
        }
    }
}
function mapStateToProps(state) {
    return {
        myCreditCardsData: state.myCreditCardsData,
        userProfile: state.userProfile,
        currentCity: state.currentCity,
        lang: state.lang
    }
}
export default withCookies(connect(mapStateToProps)(BookPoolCleaningPage));