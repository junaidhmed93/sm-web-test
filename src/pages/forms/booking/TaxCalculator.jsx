
const TaxCalculator = {
    calculateVAT:function( amount , prefferedDate = ""){

        if(prefferedDate == "" ){
            prefferedDate = new Date();
        }else{
           // console.log("TaxCalculator", prefferedDate);

            prefferedDate = prefferedDate.split('-').reverse().join('-');
        }

        var applicableTaxObj = this.getApplicableTax(prefferedDate);

        var taxRate = 0 ;

        var taxChargesTotal = 0;

        var calculatedVatCharges =  0;

        var subjectAmountForTax = amount;

        if(Object.keys(applicableTaxObj).length) {
            applicableTaxObj = Object.values( applicableTaxObj );

            applicableTaxObj.map((item, index) => {
                taxRate = item.value;
                calculatedVatCharges = (taxRate / 100) * subjectAmountForTax;
                calculatedVatCharges = parseFloat(calculatedVatCharges.toFixed(2));
                taxChargesTotal += calculatedVatCharges;
            })
        }
        return taxChargesTotal;
    },
    getApplicableTax:function(prefferedDate){
        //bookingTaxPlan
        var bookingTaxPlan = (typeof window != "undefined" &&  typeof window.REDUX_DATA.TaxPlanReducer != "undefined") ? window.REDUX_DATA.TaxPlanReducer : {};

        var taxPlan = typeof bookingTaxPlan.taxRateDtoList != "undefined" ? bookingTaxPlan.taxRateDtoList : [];

        var applicableTaxObj = {};

        if(taxPlan.length) {
            taxPlan.map((item, index) => {
                if (new Date(prefferedDate) >= new Date(item.validFrom) && new Date(prefferedDate) <= new Date(item.validTo)) {
                    applicableTaxObj[item.key] = item;
                }
            });
        }
        return applicableTaxObj;
    }
}

export default TaxCalculator;
