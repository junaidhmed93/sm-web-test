import React from "react";
import FormFieldsTitle from "../../../components/FormFieldsTitle";
import DatePick from "../../../components/Form_Fields/DatePick";
import TimePicker from "../../../components/Form_Fields/TimePicker";
import TextareaInput from "../../../components/Form_Fields/TextareaInput";
import PaymentMethods from "../../../components/Form_Fields/PaymentMethods";
import NewBookingSummary from "../../../components/Form_Fields/NewBookingSummary";
import BookNextStep from "../../../components/Form_Fields/BookNextStep";
import FormTitleDescription from "../../../components/FormTitleDescription";
import ContactDetailsStep from "../../../components/ContactDetailsStep";
import SelectInput from "../../../components/Form_Fields/SelectInput";
import { isValidSection, scrollToTop, isMobile } from "../../../actions/index";
import locationHelper from "../../../helpers/locationHelper";
import commonHelper from "../../../helpers/commonHelper";
import TaxCalculator from "./TaxCalculator";
import BookingVoucherify from "./BookingVoucherify";
import { withCookies } from "react-cookie";
import { connect } from "react-redux";
import Loader from "../../../components/Loader";
import GeneralModal from "../../../components/GeneralModal";
import stringConstants from '../../../constants/stringConstants';
let allServiceConstant = {};
let URLConstant = {};
let currentCurrency = "";
let current_city = "dubai";
import {
    CREDITCARD_COUPON,
    zendeskChatBox
} from "../../../actions";
var status = null;
var bid = null;
var tt = null;
class BookSofaCleaningPage extends React.Component {

    constructor(props) {
        super(props);
        const { cookies } = props;
        var bookingData = {
            sofas: [{ sofa_size: { label: "", value: "" }, quantity: { label: "", value: "" }, cleaning_method: { label: "", value: "" } }],
            booking_date: '',
            booking_time: '',
            input_email: props.userProfile.email,
            input_phone: props.userProfile.address ? props.userProfile.address.phoneNumber : '',
            input_name: props.userProfile.customerFirstName,
            input_last_name: props.userProfile.customerLastName,
            input_address_city: {},
            input_address_area: props.setUserArea,
            input_address_area_building_name: props.userProfile.address ? props.userProfile.address.building : '',
            input_address_area_building_apartment: props.userProfile.address ? props.userProfile.address.apartment : '',
            showMobileSummary: false,
            discountData: {},
            voucherCode: "",
            hourlyRate: 0,
            subtotal: 0,
            vat: 0,
            totalAfterDiscount: 0,
            total: 0,
            price: 0,
            payment: '',
            loader: false,
            steamCleaningItems: {},
            normalCleaningItems: {},
            showMinMessage: false,
            details: "",
            learnMore: false,
            discountPrices: {},
            isNewBookingEngine: true,
            softSafeBookingId: '',
            softSafeRequestId: '',
            no_of_seat_for_steam_cleaning: 0,
            no_of_seat_for_shampoo_cleaning: 0,
            walletUsed: '',
            isWalletAvailable: false,
        }

        var cookieData = props.getCookieFormData();

        if (Object.keys(cookieData).length) { // restore from cookies
            bookingData = cookieData;
            bookingData["softSafeBookingId"] = "";
            bookingData["softSafeRequestId"] = "";
            props.removeCookie("booking_data");
            props.removeCookie("submitted_data");
            props.removeCookie("confirmation_summary");
        }

        this.state = bookingData;

        this.ContactDetails = this.ContactDetails.bind(this);
        this.sofaCleaningRequest = this.sofaCleaningRequest.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleDropDownChange = this.handleDropDownChange.bind(this);
        this.currentStep = this.currentStep.bind(this);
        this.getPrices = this.getPrices.bind(this);
        this.moveNext = this.moveNext.bind(this);
        this.handleCustomChange = this.handleCustomChange.bind(this);
        this.handleAddSofa = this.handleAddSofa.bind(this);
        //this.handleRemoveSofa = this.handleRemoveSofa.bind(this);
        this.hashChangeHandler = this.hashChangeHandler.bind(this);
        this.calculatePrice = this.calculatePrice.bind(this);
        this.handleVoucherChange = this.handleVoucherChange.bind(this);
        this.mobileSummary = this.mobileSummary.bind(this);
        this.updateTotal = this.updateTotal.bind(this);
        this.handleBookingTimeChange = this.handleBookingTimeChange.bind(this);
        this.toggleLearnMorePopUp = this.toggleLearnMorePopUp.bind(this);
        this.applyCreditCardCoupon = this.applyCreditCardCoupon.bind(this);

    }
    componentDidMount() {
        allServiceConstant = this.props.service_constants;
        URLConstant = this.props.url_constants;
        current_city = this.props.current_city;

        status = commonHelper.getParameterByName("status");
        bid = commonHelper.getParameterByName("bid");
        tt = commonHelper.getParameterByName("tt");

        if (status == null) {
            var city = locationHelper.getLocationByName(current_city);
            var promoCode = commonHelper.getParameterByName("promo") != null ? commonHelper.getParameterByName("promo") : "";
            this.setState({
                input_address_city: city,
                loader: false,
                voucherCode: promoCode
            });
        }
        currentCurrency = locationHelper.getCurrentCurrency();
        const walletAvailable = this.props.userProfile && this.props.userProfile.userWallet && this.props.userProfile.userWallet.currency.code === currentCurrency && this.props.userProfile.userWallet.totalAmount > 0 ? true : false;
        this.setState({ isWalletAvailable: walletAvailable });

        zendeskChatBox();
        window.addEventListener("hashchange", this.hashChangeHandler, false);
    }
    componentWillUnmount() {
        window.removeEventListener("hashchange", this.hashChangeHandler, false);
    }
    calculatePrice(getTotal = false) {
        let { sofas, booking_date, voucherCode, discountData, discountPrices, isWalletAvailable } = this.state,
            changed_element = null,
            changed_value = null;

        if (changed_element == "sofas") {
            sofas = changed_value;
        }

        var item_label = "",
            no_of_seat = 0,
            key = "",
            item_price = 0,
            total_price = 0,
            selectedSofa = {
                'steam': {
                    'armchair': 0,
                    '2_seats': 0,
                    '3_seats': 0,
                    '4_seats': 0,
                    '5_seats': 0,
                    '6_seats': 0,
                    '7_seats': 0,
                    '8_seats': 0,
                    '9_seats': 0,
                    '10_seats': 0
                },
                'normal': {
                    'armchair': 0,
                    '2_seats': 0,
                    '3_seats': 0,
                    '4_seats': 0,
                    '5_seats': 0,
                    '6_seats': 0,
                    '7_seats': 0,
                    '8_seats': 0,
                    '9_seats': 0,
                    '10_seats': 0
                }
            };

        //sofaPrices = sofaPrices["per-seat"];

        var steam_prince = this.getPrices("steam");

        var normal_prince = this.getPrices("normal");
        var steamCleaningItems = [],
            normalCleaningItems = [];

        sofas.map((sofa, idx) => {
            var quantity = (typeof sofa.quantity.value != "undefined" && sofa.quantity.value != "") ? parseInt(sofa.quantity.value) : 0;
            console.log("sofa.quantity.value", sofa.quantity.value, quantity)
            var sofa_size = typeof sofa.sofa_size.value != "undefined" ? sofa.sofa_size.value : "";
            var cleaning_type = typeof sofa.cleaning_method.value != "undefined" ? sofa.cleaning_method.value : "";

            if ((sofa_size != "" && quantity != 0 && cleaning_type != "")) {
                selectedSofa[cleaning_type][sofa_size] = selectedSofa[cleaning_type][sofa_size] + quantity;
            }
        });

        //console.log("selectedSofa", selectedSofa);

        var no_of_normal_seats_count = 0;

        Object.keys(selectedSofa["normal"]).map((key) => {

            if (selectedSofa["normal"][key] != 0) {
                var val = selectedSofa["normal"][key];
                if (key == "armchair") {
                    item_label = "Armchair";
                    no_of_seat = 1;
                }else{
                    item_label = "Sofa ( " +key.replace("_"," ") + " )";
                    no_of_seat = parseInt( key.replace("_seats","") );
                }
                no_of_normal_seats_count += (no_of_seat * parseInt(selectedSofa["normal"][key]));
                item_price = (no_of_seat * parseInt(selectedSofa["normal"][key])) * normal_prince;
                total_price += item_price;

                normalCleaningItems.push(val+" x "+item_label);
                normalCleaningItems.push("Shampoo cleaning");
                normalCleaningItems.push(<span className="text-warning">{currentCurrency + " "+item_price.toFixed(2)}</span>);
                //normalCleaningItems.push({ label: item_label + ", " + val, value: currentCurrency + " " + item_price.toFixed(2), titleCase: true });
            }
        });
        var new_summery_item = [];
        if (no_of_normal_seats_count != 0) {
            new_summery_item.push({ label: 'Shampoo Cleaning No.Of Seats', value: no_of_normal_seats_count });
        }
        var no_of_steam_seats_count = 0;
        Object.keys(selectedSofa["steam"]).map((key) => {
            if (selectedSofa["steam"][key] != 0) {
                var val = selectedSofa["steam"][key];
                if (key == "armchair") {
                    item_label = "Armchair";
                    no_of_seat = 1;
                }else{
                    //item_label = (key.replace("_"," ") + " sofa").capitalize();
                    item_label = "Sofa ( " +key.replace("_"," ") + " )";
                    no_of_seat = parseInt( key.replace("_seats","") );
                }
                no_of_steam_seats_count += (no_of_seat * parseInt(selectedSofa["steam"][key]));
                item_price = (no_of_seat * parseInt(selectedSofa["steam"][key])) * steam_prince;
                total_price += item_price;

                steamCleaningItems.push(val+" x "+item_label);
                steamCleaningItems.push("Steam cleaning");
                steamCleaningItems.push(<span className="text-warning">{currentCurrency + " "+item_price.toFixed(2)}</span>);

                //steamCleaningItems.push({ label: item_label + ", " + val, value: currentCurrency + " " + item_price.toFixed(2), titleCase: true });
            }
        });

        if (no_of_steam_seats_count != 0) {
            new_summery_item.push({ label: 'Shampoo Cleaning No.Of Seats', value: no_of_normal_seats_count });
        }
        var showMinMessage = false;
        if (total_price != 0) {
            total_price = parseInt(total_price);
            if (total_price <= 149) {
                total_price = 149;
                showMinMessage = true;
            }
        }

        let walletUsed = 0;
        if (!getTotal && isWalletAvailable) {
            const walletAmount = this.props.userProfile.userWallet.totalAmount;
            const walletCurrency = this.props.userProfile.userWallet.currency.code;

            if (walletCurrency === currentCurrency && walletAmount > 0) {
                if (walletAmount >= total_price) {
                    walletUsed = total_price;
                    total_price = 0;
                }
                else if (walletAmount < total_price) {
                    walletUsed = walletAmount;
                    total_price = total_price - walletAmount;
                }

            }

        }

        var prefferedDate = booking_date;

        var taxChargesTotal = TaxCalculator.calculateVAT(total_price, prefferedDate);

        var total = total_price + taxChargesTotal;

        var total = total_price + taxChargesTotal;
        if (this.props.userProfile && this.props.userProfile.userWallet) {
            const walletAmount = this.props.userProfile.userWallet.totalAmount;
            const walletCurrency = this.props.userProfile.userWallet.currency.code;

            if (walletCurrency === currentCurrency && walletAmount > 0) {

                total = total > 0 ? total : 0.00;
                //this.setState({walletUsed: walletUsed});
            }
        }

        var returnData = {
            total: total,
            subtotal: total_price,
            vat: taxChargesTotal,
            showMinMessage: showMinMessage,
            normalCleaningItems: normalCleaningItems,
            steamCleaningItems: steamCleaningItems,
            no_of_seat_for_steam_cleaning: no_of_steam_seats_count,
            no_of_seat_for_shampoo_cleaning: no_of_normal_seats_count,
            walletUsed: walletUsed
        }

        if (!getTotal && (typeof discountData != "undefined" && (typeof discountData.success != "undefined" && discountData.success))) {
            returnData = this.updateTotal(discountPrices, true);
            returnData["no_of_seat_for_steam_cleaning"] = no_of_steam_seats_count;
            returnData["no_of_seat_for_shampoo_cleaning"] = no_of_normal_seats_count;
            returnData["normalCleaningItems"] = normalCleaningItems;
            returnData["steamCleaningItems"] = steamCleaningItems;
        }

        return returnData
    }
    handleVoucherChange(value, new_price = 0) {
        let { booking_date, voucherCode, booking_time, discountData, subtotal, vat, payment, price } = this.state;
        var couponValue = value.trim(),
            changed_element = null, changed_value = null;
        this.setState({
            discountData: "load"
        })
        if (price == 0 && new_price == 0) {
            var response = BookingVoucherify.error("Please select the mattress");
            this.setState({
                discountData: response.data
            });
        } else {

            if (typeof couponValue == 'undefined' || couponValue.length == 0) {
                var response = BookingVoucherify.error("Please provide coupon.");
                this.setState({
                    discountData: response.data
                });
                //this.calculatePrice("reset_coupon", true);
            } else if (couponValue.trim().length < 3) { // minimum length of a promo code is more than 3 characters, so need to go to server if less than that
                var response = BookingVoucherify.error("Promotion code is invalid");
                this.setState({
                    discountData: response.data
                })
                //this.calculatePrice("reset_coupon", true);
            }
            else {
                var service_constants = allServiceConstant;


                var updatedPrice = false;

                if (new_price != 0) {
                    price = new_price;
                }

                var $package_detail = {};

                var rules = BookingVoucherify.generateRules(couponValue, price, allServiceConstant.SERVICE_SOFA_CLEANING, 0, booking_date, current_city, "", payment);

                BookingVoucherify.validate(couponValue, rules).then((response) => {
                    var res = response;
                    if (this.state.voucherCode != "" || (this.state.voucherCode == CREDITCARD_COUPON && this.state.payment == "credit")) {
                        this.updateTotal(res);
                    }
                });
            }
        }
    }
    updateTotal(res, isReturnData = false) {
        let { walletUsed, booking_date } = this.state;
        let response = res;
        let discountData = response.data;
        let currentTotal = response.currentTotal;
        let taxChargesTotal = response.taxChargesTotal;
        let total = response.total;

        if (walletUsed > 0) {
            currentTotal = parseFloat((response.totalAfterDiscount - walletUsed).toFixed(2));
            taxChargesTotal = TaxCalculator.calculateVAT(currentTotal, booking_date);
            total = parseFloat((currentTotal + taxChargesTotal).toFixed(2));

        }

        var returnData = {
            price: currentTotal,
            total: total,
            subtotal: currentTotal,
            vat: taxChargesTotal,
            discountData: discountData,
            walletUsed: walletUsed
        };

        if (typeof res.isCreditCardCouponApplied != "undefined" && res.isCreditCardCouponApplied) {
            returnData["voucherCode"] = "";
            this.props.updateCreditCardCouponApplied();
        }

        if (isReturnData) {
            return returnData;
        } else {
            returnData["discountData"] = discountData;
            returnData["discountPrices"] = res
        }

        this.setState(returnData);
    }
    hashChangeHandler() {
        let hashVal = window.location.hash;
        let step;
        hashVal.length ? step = parseInt(window.location.hash.replace('#', '')) : step = 1;
        this.props.moveNext(step);
        scrollToTop();
    }
    handleBookingTimeChange(name, value) {
        this.setState({
            booking_time: value
        });
    }
    sumarryItems(){
        const {
            sofas, 
            booking_date, 
            booking_time,
            discountData,
            subtotal,
            vat, 
            payment, 
            steamCleaningItems, 
            normalCleaningItems, 
            total, 
            walletUsed } = this.state;
        
        let timeValue = "";

        if( booking_date != "" ){
            timeValue += booking_date;
            if( typeof booking_time.label != "undefined" ){
                timeValue += " "+booking_time.label;
            }
        }
        let items = [
            {label: 'Time', value: timeValue}
        ];
       let detailLblAdded = false;

       

        if(typeof steamCleaningItems == "object" && Object.keys(steamCleaningItems).length){
            detailLblAdded = true;
            items.push({label: 'Details', value: steamCleaningItems, conClass: 'col-12 mb-4'});
        }
        if(typeof normalCleaningItems == "object" && Object.keys(normalCleaningItems).length){
            items.push({label: !detailLblAdded ? 'Details' : '', value: normalCleaningItems, conClass: 'col-12 mb-4'});
        }

        if(total != 0 ){
            if (this.props.userProfile && this.props.userProfile.userWallet) {
                const walletAmount = walletUsed; //this.props.userProfile.userWallet.totalAmount;
                const walletCurrency = this.props.userProfile.userWallet.currency.code;
                if (walletCurrency === currentCurrency && walletAmount > 0) {
                    items.push({ label: stringConstants.WALLET, value: `-  ${walletCurrency}  ${walletAmount}` });
                }
            }
            if(typeof discountData.promoCodeDiscountText != "undefined"){
                items.push({ label: 'Promo code', value: discountData.promoCodeDiscountText });
                items.push({ label: 'Subtotal', value: currentCurrency + " " + subtotal.toFixed(2), text_strike: "yes" });
            } else {
                items.push({ label: 'Subtotal', value: currentCurrency + " " + subtotal.toFixed(2) });
            }
            items.push(
                 {label: 'Payment', value: payment}
            );
        }

        return items;
    }
    currentStep() {
        return this.props.formCurrentStep;
    }
    moveNext(step) {
        const { formCurrentStep, signInDetails, showLoginModal, showLoginMenu, moveNextStep } = this.props;
        moveNextStep(step, this.state, true);
    }
    handleCustomChange = (idx) => (name, value) => {
        var new_name = name.replace("_" + idx, "")

        const newSofa = this.state.sofas.map((sofa, sidx) => {
            if (idx !== sidx) return sofa;
            return { ...sofa, [new_name]: value };
        });

        this.setState({ sofas: newSofa });
    }
    handleInputChange(name, value) {
        if (name == "payment") {
            this.applyCreditCardCoupon(value);
        }
        this.setState({
            [name]: value
        });
    }
    handleDropDownChange(name, value) {
        // console.log(name, value);
    }
    getPrices(type = "") {
        var lite_weight_prices = (typeof this.props.pricePlan != "undefined" && typeof this.props.pricePlan.planDetail != "undefined") ? this.props.pricePlan.planDetail.packages : 0;//this.props.lite_weight_prices;
        //console.log("getPrices", lite_weight_prices);
        if (type != "" && lite_weight_prices.length) {
            return lite_weight_prices.filter((item) => item.type == type).length ? lite_weight_prices.filter((item) => item.type == type)[0].price : 0;
        }
        return lite_weight_prices;
    }
    handleAddSofa = () => {
        this.setState({ sofas: this.state.sofas.concat([{ sofa_size: { label: "", value: "" }, quantity: { label: "", value: "" }, cleaning_method: { label: "", value: "" } }]) });
    }
    handleRemoveSofa = (idx) => () => {
        const newSofa = this.state.sofas.filter((s, sidx) => idx !== sidx);

        this.setState({ sofas: newSofa });
    }
    sofaCleaningRequest() {
        var currentStepClass = this.currentStep() === 1 ? '' : "d-none";

        var sofaCleaningPrices = this.getPrices();

        var sofa_size_option = [
            { id: 1, value: "armchair", label: "Armchair ( 1 Seat)" },
            { id: 2, value: "2_seats", label: "Sofa (2 seats)" },
            { id: 3, value: "3_seats", label: "Sofa (3 seats)" },
            { id: 4, value: "4_seats", label: "Sofa (4 seats)" },
            { id: 5, value: "5_seats", label: "Sofa (5 seats)" },
            { id: 6, value: "6_seats", label: "Sofa (6 seats)" },
            { id: 7, value: "7_seats", label: "Sofa (7 seats)" },
            { id: 8, value: "8_seats", label: "Sofa (8 seats)" },
            { id: 9, value: "9_seats", label: "Sofa (9 seats)" },
            { id: 10, value: "10_seats", label: "Sofa (10 seats)" },
        ];

        var sofa_count_option = [
            { id: 1, value: "1", label: "1" },
            { id: 2, value: "2", label: "2" },
            { id: 3, value: "3", label: "3" },
            { id: 4, value: "4", label: "4" },
            { id: 5, value: "5", label: "5" }
        ];

        //console.log(sofaCleaningPrices);

        var steam_prince = this.getPrices("steam");
        //( sofaCleaningPrices.length && sofaCleaningPrices.filter((item) => item.type == "steam").length ) ? sofaCleaningPrices.filter((item) => item.type == "normal")[0].price : 0; //sofaCleaningPrices["per-seat"]["steam"];

        var normal_prince = this.getPrices("normal");

        var sofa_cleaning_option = [
            { id: 1, value: "steam", price: steam_prince, label: "Steam Cleaning ( " + currentCurrency + " " + steam_prince + " )" },
            { id: 2, value: "normal", price: normal_prince, label: "Shampoo Cleaning ( " + currentCurrency + " " + normal_prince + " )" }
        ];

        var i = 1;
        //var classNameTxt = ;
        return (
            <div id="section-request-form" className={currentStepClass}>
                <section>
                    {this.props.showDiscountNotification(this.state.discountData)}
                    <FormTitleDescription title="Please describe the sofa you want cleaned" desc="You can select multiple sofa sizes and cleaning methods" />
                    {
                        this.state.sofas.map((sofa, idx) => (
                            <div className={isMobile() ? "row mb-4 multiple-row" : "row mb-2 multiple-row"}>
                                <div className="col-12 col-md-4 mb-3">
                                    <SelectInput name={"sofa_size_" + idx} inputValue={sofa.sofa_size} label="Select Sofa" options={sofa_size_option} onInputChange={this.handleCustomChange(idx)} />
                                </div>
                                <div className="col-12 col-md-4 mb-3">
                                    <SelectInput name={"quantity_" + idx} inputValue={sofa.quantity} label="Select Quantity" options={sofa_count_option} onInputChange={this.handleCustomChange(idx)} />
                                </div>
                                <div className="col-12 col-md-4 mb-3">
                                    {(idx == 0) && <a href="#" className="cleaning-method-more" onClick={() => this.toggleLearnMorePopUp(true)}>Learn More</a>}
                                    {(idx != 0) && <a href="javascript:void(0)" onClick={this.handleRemoveSofa(idx)}> <i className="fa fa-trash text-danger"></i> </a>}
                                    <SelectInput name={"cleaning_method_" + idx} inputValue={sofa.cleaning_method} label="Select Cleaning method" options={sofa_cleaning_option} onInputChange={this.handleCustomChange(idx)} />
                                </div>
                            </div>

                        ))}
                </section>
                <div className="mb-4">
                    <button className="btn border rounded bg-white text-warning px-3" onClick={this.handleAddSofa}><i className="fa fa-plus-circle"></i> Add</button>
                </div>
                <section>
                    <FormFieldsTitle title="Pick your start date and time" />
                    <div className="row mb-2">
                        <DatePick
                            name="booking_date"
                            inputValue={this.state.booking_date}
                            selectedDate={this.state.booking_date}
                            disable_friday={true}
                            onInputChange={this.handleInputChange}
                            validationClasses="required" />

                        <TimePicker inputValue={this.state.booking_time}
                            name="booking_time"
                            disableFirstSlot={true}
                            onInputChange={this.handleBookingTimeChange}
                            validationClasses="required" 
                            booking_date={this.state.booking_date}/>
                    </div>
                </section>
                <section>
                    <FormTitleDescription title="What else should we know? (Optional)" desc="Please describe the job you need to have done." />
                    <div className="row mb-4">
                        <div className="col mb-3">
                            <TextareaInput
                                name="details"
                                inputValue={this.state.details}
                                placeholder="Please describe the job you need to have done."
                                onInputChange={this.handleInputChange} />
                        </div>
                    </div>
                </section>
                <section>
                    <BookNextStep total={this.state.total} title="Contact Details" moveNext={this.moveNext} toStep="2" setModal={this.mobileSummary} />
                </section>
            </div>
        )
    }
    ContactDetails(isLocationDetailShow) {
        var cityOptions = locationHelper.getLocationByName(current_city);

        var currentStepClass = this.currentStep() === 2 ? '' : "d-none";
        //var currentStepClass =  ''
        var isLocationDetailShow = "yes";

        var contact_details = {
            input_email: this.state.input_email,
            input_phone: this.state.input_phone,
            input_name: this.state.input_name,
            input_last_name: this.state.input_last_name
        }

        if (isLocationDetailShow) {
            contact_details.input_address_city = this.state.input_address_city;
            contact_details.input_address_area = this.state.input_address_area;
            contact_details.input_address_area_building_name = this.state.input_address_area_building_name;
            contact_details.input_address_area_building_apartment = this.state.input_address_area_building_apartment;
        }

        var userProfile = this.props.userProfile;

        return (
            <div id="personal-information-form" className={currentStepClass}>
                <ContactDetailsStep
                    cityOptions={cityOptions}
                    userProfile={userProfile}
                    signInDetails={this.props.signInDetails}
                    isLocationDetailShow={isLocationDetailShow}
                    contact_details={contact_details}
                    handleDropDownChange={this.handleInputChange}
                    handleInputChange={this.handleInputChange}
                    mobileSummary={this.mobileSummary}
                    moveNext={this.moveNext}
                    toStep={3}
                    isBooking={true}
                    total={this.state.total}
                    currentCity={this.props.current_city}
                />
            </div>
        )
    }
    PaymentSection() {
        var payment = this.state.payment;
        var currentStepClass = this.currentStep() === 3 ? '' : "d-none";
        //var currentStepClass ='';
        if (!this.state.paymentLoader) {

            return (
                <div id="payment-method-form" className={currentStepClass}>
                    <section className="payment">
                        <PaymentMethods
                            isCreditCardCouponApplied={this.props.isCreditCardCouponApplied}
                            name="payment"
                            inputValue={payment}
                            status={status} tt={tt}
                            onInputChange={this.handleInputChange} />
                    </section>
                    <section>
                        <BookNextStep total={this.state.total} title="Looking forward to serving you" noNextStep={false} moveNext={this.moveNext} setModal={this.mobileSummary} />
                    </section>
                </div>
            );
        } else {
            return (
                <main loader={this.state.paymentLoader ? "show" : "hide"}><Loader /></main>
            );
        }
    }
    mobileSummary(boolVal) {
        this.setState({
            showMobileSummary: boolVal
        })
    }
    componentDidUpdate(prevProps, prevState) {
        var {sofas,booking_date,voucherCode, normalCleaningItems, steamCleaningItems, payment} = this.state;
        var city = current_city;
        var prices = this.calculatePrice();
        var new_price = this.state.price;

        if (prevState.total != prices.total || prevState.sofas !== sofas) {
            this.setState({
                price: prices.subtotal,
                total: prices.total,
                subtotal: prices.subtotal,
                vat: prices.vat,
                normalCleaningItems: prices.normalCleaningItems,
                steamCleaningItems: prices.steamCleaningItems,
                showMinMessage: prices.showMinMessage,
                no_of_seat_for_steam_cleaning: prices.no_of_seat_for_steam_cleaning,
                no_of_seat_for_shampoo_cleaning: prices.no_of_seat_for_shampoo_cleaning,
                walletUsed: prices.walletUsed
            })
            new_price = prices.subtotal;
        }

        //console.log(prevState.booking_date , booking_date);

        if((prevState.sofas !== sofas 
            || prevState.voucherCode !== voucherCode 
            || prevState.booking_date !== booking_date
            || prevState.payment !== payment) 
            && ( voucherCode != "" ) ){
                var prices = this.calculatePrice(true);
                new_price = prices.subtotal;
            this.handleVoucherChange(voucherCode, new_price);
        }
    }
    componentWillReceiveProps(newProps) {

        if (status == null) {
            if ((newProps.isCreditCardCouponApplied !== this.props.isCreditCardCouponApplied) && newProps.isCreditCardCouponApplied) {
                this.setState({
                    couponCode: ""
                });
                //this.applyCreditCardCoupon();
            }
            if ((newProps.formCurrentStep == 3) && (newProps.formCurrentStep !== this.props.formCurrentStep)) {
                var payment = this.state.payment;
                if (payment == "") {
                    payment = "credit";
                    this.setState({
                        payment: payment
                    });
                    this.applyCreditCardCoupon(payment);
                }
            }
            if (newProps.setUserArea !== this.props.setUserArea) {
                this.setState({
                    input_address_area: newProps.setUserArea
                })
            }
        }
    }
    learnMoreCleaningMethod() {
        return (<div className="learn-more-body">
            <h4 className="text-primary">Steam cleaning</h4>
            <ul className="steam-cleaning">
                <li>Steam cleaning is great for killing bacteria, fungus, dust mites and germs. The heat of the steam is sanitizing and kills 99.99% of bacteria.</li>
                <li>Since only water is used for the cleaning, it is especially suitable for people who suffer allergies.</li>
                <li>Easily removes stains. Hard to remove substances such as wax, glue and chewing gum is dissolved by the steam.</li>
                <li>Suitable for delicate materials (not leather or silk).</li>
                <li>The drying process is quick. Most of the steam evaporates instantly.</li>
            </ul>
            <h4 className="text-primary">Shampoo cleaning</h4>
            <ul className="shampoo-cleaning">
                <li>Shampoo cleaning starts with vacuuming the sofa with a high-powered vacuum.</li>
                <li>Stains are removed using either machines or manually using professional fabric shampoo.</li>
                <li>Excess water is extracted from the sofa.</li>
                <li>A second round of vacuum deep cleaning ensures that remainder shampoo and dirt is removed.</li>
                <li>The approximate drying time in an air conditioned room is 6-8 hours depending on the fiber type used in the sofa.</li>
            </ul>
        </div>)
    }
    toggleLearnMorePopUp(state) {
        this.setState({
            learnMore: state
        })
    }
    applyCreditCardCoupon(value) {
        var returnData = this.props.applyCreditCardCoupon(value, this.state.voucherCode);
        var {voucherCode} = this.state;
        var applyCreditCardCoupon = false;
        var payment = typeof value != "undefined" ? value : this.state.payment;
        if (this.state.payment == "") {
            this.setState({
                payment: 'credit',
            });
            applyCreditCardCoupon = true
        } else {
            applyCreditCardCoupon = (payment != "" && payment == "credit") ? true : false;
        }
        if (this.props.isCreditCardCouponApplied && (voucherCode != "" && voucherCode == CREDITCARD_COUPON)) {
            this.setState({
                voucherCode: ""
            });
        } else {
            if (!this.props.isCreditCardCouponApplied && ((applyCreditCardCoupon && payment == "credit") && (voucherCode == "" || voucherCode == CREDITCARD_COUPON))) {
                if (voucherCode == "") {
                    this.setState({
                        voucherCode: CREDITCARD_COUPON
                    });
                }
            } else {
                if (payment == "cash" && voucherCode == CREDITCARD_COUPON) {
                    this.setState({
                        voucherCode: ""
                    });
                }
            }
        }
    }
    render() {
        const items = this.sumarryItems();
        const showPromo = true;
        const showPrice = true;
        let {vat,total} = this.state;

        var discountData = this.state.discountData;

        var booking_date = this.state.booking_date;

        var showMinMessage = this.state.showMinMessage;

        const {isWalletAvailable} = this.props;

        var bookingSummary = <NewBookingSummary
            items={items}
            showPromo={showPromo}
            showPrice={showPrice}
            total={total}
            vat={vat}
            booking_date={booking_date}
            discountData={discountData}
            couponValue={this.state.voucherCode}
            handleCouponChange={this.handleInputChange}
            showMobileSummary={this.state.showMobileSummary}
            setModal={this.mobileSummary}
            formData={this.state}
            updateTotal={this.updateTotal}
            isLW={true}
            isSpecializedCleaning={true}
            showMinMessage={showMinMessage}
            walletAvailable={isWalletAvailable}
        />;
        var isLoading = this.state.loader || this.props.loader;

        if (isLoading) {
            return (
                <React.Fragment>
                    <div className="col-lg-8">
                        <main loader={isLoading ? "show" : "hide"}><Loader /></main>
                    </div>
                    <div className="col-md-3 ml-auto">
                        {bookingSummary}
                    </div>
                </React.Fragment>
            )
        } else {

            return (
                <React.Fragment>
                    <div className="col-lg-8">
                        {isWalletAvailable ?
                            <div className='wallet-div'>
                                <img src={"../../../../dist/images/wallet-filled-money-tool-light.png"} className="mh-100" height="28" alt="" />
                                <span className='margin-left30' style={{ verticalAlign: 'middle', fontSize: '15px' }}>{stringConstants.YOU_HAVE_TXT}
                                    <span className='wallet-amount-span'>{this.props.userProfile.userWallet.currency.code + ' ' + this.props.userProfile.userWallet.totalAmount} </span>{stringConstants.WALLET_BALANCE_TXT}
                                </span>
                            </div>
                            : null
                        }
                        {this.sofaCleaningRequest()}
                        {this.ContactDetails()}
                        {this.PaymentSection()}
                    </div>
                    <div className="col-md-3 ml-auto">
                        {bookingSummary}
                    </div>
                    {this.state.learnMore && <GeneralModal modalSize="modal-lg" title="About our cleaning methods" modalBody={this.learnMoreCleaningMethod()}
                        setModal={this.toggleLearnMorePopUp} />}
                </React.Fragment>
            )
        }
    }
}
function mapStateToProps(state) {
    return {
        myCreditCardsData: state.myCreditCardsData,
        showLoginMenu: state.showLoginMenu,
        currentCity: state.currentCity,
        lang: state.lang
    }
}
export default withCookies(connect(mapStateToProps)(BookSofaCleaningPage));