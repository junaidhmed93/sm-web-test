import React from "react";
import FormFieldsTitle from "../../../components/FormFieldsTitle";
import DatePick from "../../../components/Form_Fields/DatePick";
import TimePicker from "../../../components/Form_Fields/TimePicker";
import TextareaInput from "../../../components/Form_Fields/TextareaInput";
import PaymentMethods from "../../../components/Form_Fields/PaymentMethods";
import NewBookingSummary from "../../../components/Form_Fields/NewBookingSummary";
//import BookingSummary from "../../../components/Form_Fields/BookingSummary";
import BookNextStep from "../../../components/Form_Fields/BookNextStep";
import FormTitleDescription from "../../../components/FormTitleDescription";
import ContactDetailsStep from "../../../components/ContactDetailsStep";
import SelectInput from "../../../components/Form_Fields/SelectInput";
import TextFloatInput from "../../../components/Form_Fields/TextFloatInput";
import { isValidSection, scrollToTop } from "../../../actions/index";
import locationHelper from "../../../helpers/locationHelper";
import commonHelper from "../../../helpers/commonHelper";
import TaxCalculator from "./TaxCalculator";
import BookingVoucherify from "./BookingVoucherify";
import {withCookies} from "react-cookie";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import Loader from "../../../components/Loader";
import GeneralModal from "../../../components/GeneralModal";
import stringConstants from '../../../constants/stringConstants';
let allServiceConstant = {};
let URLConstant = {};
let currentCurrency = "";
let current_city = "dubai";
import {
    CREDITCARD_COUPON,
    zendeskChatBox,
    isMobile
} from "../../../actions";
var status = null;
var bid = null;
var tt = null;
class BookCarpetCleaningPage extends React.Component {

    constructor(props) {
        super(props);
        const { cookies } = props;
        var bookingData = {
            carpet: [
                {
                    carpet_width: '',
                    carpet_height: '',
                    carpet_units: { id: 1, value: "cms", label: "cm" },
                    squareFeet: 0,
                    quantity: 1,
                    cleaning_method: { label: "", value: "" }
                }
            ],
            booking_date: '',
            booking_time: '',
            input_email: props.userProfile.email,
            input_phone: props.userProfile.address ? props.userProfile.address.phoneNumber : '',
            input_name: props.userProfile.customerFirstName,
            input_last_name: props.userProfile.customerLastName,
            input_address_city: {},
            input_address_area: props.setUserArea,
            input_address_area_building_name: props.userProfile.address ? props.userProfile.address.building : '',
            input_address_area_building_apartment: props.userProfile.address ? props.userProfile.address.apartment : '',
            showMobileSummary: false,
            discountData: {},
            voucherCode: "",
            hourlyRate: 0,
            subtotal: 0,
            vat: 0,
            totalAfterDiscount: 0,
            total: 0,
            price: 0,
            payment: '',
            loader: false,
            steamCleaningItems: {},
            normalCleaningItems: {},
            showMinMessage: false,
            learnMore: false,
            discountPrices: {},
            isNewBookingEngine: true,
            softSafeBookingId: '',
            softSafeRequestId: '',
            carpet_area_for_steam_cleaning: 0,
            carpet_area_for_shampoo_cleaning: 0,
            walletUsed: '',
            isWalletAvailable: false,
        }
        var cookieData = props.getCookieFormData();

        if (Object.keys(cookieData).length) { // restore from cookies
            bookingData = cookieData;
            bookingData["softSafeBookingId"] = "";
            bookingData["softSafeRequestId"] = "";
            props.removeCookie("booking_data");
            props.removeCookie("submitted_data");
            props.removeCookie("confirmation_summary");
        }

        this.state = bookingData;

        this.ContactDetails = this.ContactDetails.bind(this);
        this.carpetCleaningRequest = this.carpetCleaningRequest.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleDropDownChange = this.handleDropDownChange.bind(this);
        this.currentStep = this.currentStep.bind(this);
        this.getPrices = this.getPrices.bind(this);
        this.moveNext = this.moveNext.bind(this);
        this.handleCustomChange = this.handleCustomChange.bind(this);
        this.handleAddCarpet = this.handleAddCarpet.bind(this);
        //this.handleRemoveCarpet = this.handleRemoveCarpet.bind(this);
        this.hashChangeHandler = this.hashChangeHandler.bind(this);
        this.calculatePrice = this.calculatePrice.bind(this);
        this.handleVoucherChange = this.handleVoucherChange.bind(this);
        this.mobileSummary = this.mobileSummary.bind(this);
        this.updateTotal = this.updateTotal.bind(this);
        this.carpetCleaningOptions = this.carpetCleaningOptions.bind(this);
        this.handleBookingTimeChange = this.handleBookingTimeChange.bind(this);
        this.updateSquareMeter = this.updateSquareMeter.bind(this);
        this.calculateSquareMeter = this.calculateSquareMeter.bind(this);
        this.toggleLearnMorePopUp = this.toggleLearnMorePopUp.bind(this);
        this.applyCreditCardCoupon = this.applyCreditCardCoupon.bind(this);

    }
    componentDidMount() {
        allServiceConstant = this.props.service_constants;
        URLConstant = this.props.url_constants;
        current_city = this.props.current_city;

        status = commonHelper.getParameterByName("status");
        bid = commonHelper.getParameterByName("bid");
        tt = commonHelper.getParameterByName("tt");

        if (status == null) {
            var city = locationHelper.getLocationByName(current_city);
            var promoCode = commonHelper.getParameterByName("promo") != null ? commonHelper.getParameterByName("promo") : "";
            this.setState({
                input_address_city: city,
                loader: false,
                voucherCode: promoCode
            });
        }
        currentCurrency = locationHelper.getCurrentCurrency();
        const walletAvailable = this.props.userProfile && this.props.userProfile.userWallet && this.props.userProfile.userWallet.currency.code === currentCurrency && this.props.userProfile.userWallet.totalAmount > 0 ? true : false;
        this.setState({ isWalletAvailable: walletAvailable });

        zendeskChatBox();
        window.addEventListener("hashchange", this.hashChangeHandler, false);
    }
    componentWillUnmount() {
        window.removeEventListener("hashchange", this.hashChangeHandler, false);
    }
    calculateSquareMeter(width, length, unit_size) {
        var square_meter = 0
        if (unit_size == "cms") {
            square_meter = ((width / 100) * (length / 100));
        } else if (unit_size == "inchs") {
            square_meter = ((width * 0.0254) * (length * 0.0254));
        } else if (unit_size == "feets") {
            square_meter = ((width * 0.3048) * (length * 0.3048));
        }
        if (square_meter < 1) {
            square_meter = Math.ceil(square_meter);
        } else {
            square_meter = parseFloat(Math.round(square_meter * 100) / 100);
        }

        return square_meter;
    }
    calculatePrice(getTotal = false) {
        let { carpet, booking_date, voucherCode, discountData, discountPrices, isWalletAvailable } = this.state;

        /*if(changed_element == "carpet"){
            carpet = changed_value;
        }*/

        var item_label = "",
            no_of_seat = 0,
            key = "",
            item_price = 0,
            total_price = 0,
            selectedCarpet = [],
            count = 1;

        var steamCount = 0;
        var normalCount = 0;

        var sqFeet = 0;

        carpet.map((carpetItem, idx) => {
            var quantity = 1;
            var cleaning_type = typeof carpetItem.cleaning_method.value != "undefined" ? carpetItem.cleaning_method.value : "";
            if ((quantity != 0 && (cleaning_type != ""))) {
                var sqFeet = this.updateSquareMeter(carpetItem);
                carpetItem["squareFeet"] = sqFeet;
                selectedCarpet.push(carpetItem);
            }
        });

        var single_price = 0;

        var steamCleaningItems = [],
            normalCleaningItems = [];

        //console.log("calculatePrice", selectedCarpet);

        var normalCarpets = selectedCarpet.filter((item) => (
            item.cleaning_method.value == "normal"
        ));

        var steamCarpets = selectedCarpet.filter((item) => (
            item.cleaning_method.value == "steam"
        ));

        var noOfSquareFeet = 0,
            quantity = 0,
            normalCleaningPrice = this.getPrices("normal"),
            steamCleaningPrice = this.getPrices("steam");
        let noOfSquareFeetNormal = 0;
        normalCarpets.map((normalCarpetItem) => {
            if (normalCarpetItem.quantity != 0 && normalCarpetItem.squareFeet != 0) {
                noOfSquareFeet = normalCarpetItem.squareFeet;
                quantity = normalCarpetItem.quantity;
                noOfSquareFeetNormal += noOfSquareFeet;
                
                item_label = "Carpet "+count+ ' ( ' + noOfSquareFeet+ " sqm )";

                item_price = noOfSquareFeet * ( quantity * normalCleaningPrice );
                item_price = parseFloat( Math.round(item_price * 100) / 100 );
                total_price += item_price;
                
                normalCleaningItems.push(item_label);
                normalCleaningItems.push("Shampoo cleaning");
                normalCleaningItems.push(<span className="text-warning">{currentCurrency + " "+item_price.toFixed(2)}</span>);

                //normalCleaningItems.push({ label: item_label, value: currentCurrency + " " + item_price.toFixed(2), titleCase: true });
                count++;
            }
        });

        item_price = 0,
            count = 1;
        let noOfSquareFeetSteam = 0;

        steamCarpets.map((steamCarpetItem) => {
            if (steamCarpetItem.quantity != 0 && steamCarpetItem.squareFeet != 0) {
                noOfSquareFeet = steamCarpetItem.squareFeet;
                quantity = steamCarpetItem.quantity;
                noOfSquareFeetSteam += noOfSquareFeet;
                
                item_label = "Carpet "+count+ ' ( ' + noOfSquareFeet+ " sqm )";

                item_price = noOfSquareFeet * ( quantity * steamCleaningPrice );
                item_price = parseFloat( Math.round(item_price * 100) / 100 );

                total_price += item_price;

                steamCleaningItems.push(item_label);
                steamCleaningItems.push("Steam cleaning");
                steamCleaningItems.push(<span className="text-warning">{currentCurrency + " "+item_price.toFixed(2)}</span>);

                count++;
            }
        });

        // total_price = 149;
        var showMinMessage = false;
        if (total_price != 0) {
            total_price = parseFloat(total_price);
            if (total_price <= 149) {
                total_price = 149;
                showMinMessage = true;
            }
        }

        let walletUsed = 0;
        if (!getTotal && isWalletAvailable) {
            const walletAmount = this.props.userProfile.userWallet.totalAmount;
            const walletCurrency = this.props.userProfile.userWallet.currency.code;

            if (walletCurrency === currentCurrency && walletAmount > 0) {
                if (walletAmount >= total_price) {
                    walletUsed = total_price;
                    total_price = 0;
                }
                else if (walletAmount < total_price) {
                    walletUsed = walletAmount;
                    total_price = total_price - walletAmount;
                }

            }

        }

        var prefferedDate = booking_date;

        var taxChargesTotal = TaxCalculator.calculateVAT(total_price, prefferedDate);


        var total = total_price + taxChargesTotal;
        if (this.props.userProfile && this.props.userProfile.userWallet) {
            const walletAmount = this.props.userProfile.userWallet.totalAmount;
            const walletCurrency = this.props.userProfile.userWallet.currency.code;

            if (walletCurrency === currentCurrency && walletAmount > 0) {

                total = total > 0 ? total : 0.00;

            }
        }
        var returnData = {
            total: total,
            subtotal: total_price,
            vat: taxChargesTotal,
            steamCleaningItems: steamCleaningItems,
            normalCleaningItems: normalCleaningItems,
            showMinMessage: showMinMessage,
            carpet_area_for_steam_cleaning: noOfSquareFeetSteam,
            carpet_area_for_shampoo_cleaning: noOfSquareFeetNormal,
            walletUsed: walletUsed
        };


        if (!getTotal && (typeof discountData != "undefined" && (typeof discountData.success != "undefined" && discountData.success))) {
            returnData = this.updateTotal(discountPrices, true);
            returnData["carpet_area_for_steam_cleaning"] = noOfSquareFeetSteam;
            returnData["carpet_area_for_shampoo_cleaning"] = noOfSquareFeetNormal;
            returnData["steamCleaningItems"] = steamCleaningItems;
            returnData["normalCleaningItems"] = normalCleaningItems;
        }

        return returnData;
    }
    handleVoucherChange(value, new_price = 0) {
        var changed_element = null, changed_value = null;
        let { booking_date, voucherCode, booking_time, discountData, subtotal, vat, payment, price } = this.state;

        var couponValue = value.trim();
        this.setState({
            discountData: "load"
        })
        if (price == 0 && new_price == 0) {
            var response = BookingVoucherify.error("Please select the package");
            this.setState({
                discountData: response.data
            });
        } else {

            if (typeof couponValue == 'undefined' || couponValue.length == 0) {
                var response = BookingVoucherify.error("Please provide coupon.");
                this.setState({
                    discountData: response.data
                });
                //this.calculatePrice("reset_coupon", true);
            } else if (couponValue.trim().length < 3) { // minimum length of a promo code is more than 3 characters, so need to go to server if less than that
                var response = BookingVoucherify.error("Promotion code is invalid");
                this.setState({
                    discountData: response.data
                })
                //this.calculatePrice("reset_coupon", true);
            }
            else {

                var updatedPrice = false;

                if (new_price != 0) {
                    price = new_price;
                }

                var rules = BookingVoucherify.generateRules(couponValue, price, allServiceConstant.SERVICE_CARPET_CLEANING, 0, booking_date, current_city, "", payment);

                BookingVoucherify.validate(couponValue, rules).then((response) => {
                    var res = response;
                    if (this.state.voucherCode != "" || (this.state.voucherCode == CREDITCARD_COUPON && this.state.payment == "credit")) {
                        this.updateTotal(res);
                    }
                });
            }
        }
    }
    updateTotal(res, isReturnData = false) {
        let { walletUsed, booking_date } = this.state;
        let response = res;
        let discountData = response.data;
        let currentTotal = response.currentTotal;
        let taxChargesTotal = response.taxChargesTotal;
        let total = response.total;

        if (walletUsed > 0) {
            currentTotal = parseFloat((response.totalAfterDiscount - walletUsed).toFixed(2));
            taxChargesTotal = TaxCalculator.calculateVAT(currentTotal, booking_date);
            total = parseFloat((currentTotal + taxChargesTotal).toFixed(2));
        }

        var returnData = {
            price: currentTotal,
            total: total,
            subtotal: currentTotal,
            vat: taxChargesTotal,
            walletUsed: walletUsed
        };

        if (typeof res.isCreditCardCouponApplied != "undefined" && res.isCreditCardCouponApplied) {
            returnData["voucherCode"] = "";
            this.props.updateCreditCardCouponApplied();
        }

        if (isReturnData) {
            return returnData;
        } else {
            returnData["discountData"] = discountData;
            returnData["discountPrices"] = res
        }

        this.setState(returnData);
    }
    hashChangeHandler() {
        let hashVal = window.location.hash;
        let step;
        hashVal.length ? step = parseInt(window.location.hash.replace('#', '')) : step = 1;
        this.props.moveNext(step);
        scrollToTop();
    }
    sumarryItems() {
        const { carpet, booking_date, booking_time, discountData, subtotal, vat, payment, steamCleaningItems, normalCleaningItems, total, walletUsed } = this.state;

        let timeValue = "";

        if( booking_date != "" ){
            timeValue += booking_date;
            if( typeof booking_time.label != "undefined" ){
                timeValue += " "+booking_time.label;
            }
        }
        let items = [
            {label: 'Time', value: timeValue}
        ];
        let detailLblAdded = false;
        if(typeof steamCleaningItems == "object" && Object.keys(steamCleaningItems).length){
            detailLblAdded = true
            items.push({label: 'Details', value: steamCleaningItems, conClass: 'col-12 mb-4'});
        }
        if(typeof normalCleaningItems == "object" && Object.keys(normalCleaningItems).length){
            items.push({label: !detailLblAdded ? 'Details' : '', value: normalCleaningItems, conClass: 'col-12 mb-4'});
        }

        /*items.push({label: 'Service', value: "Carpet cleaning"});
        items.push({ label: 'Service', value: "Carpet cleaning" });

        if (typeof steamCleaningItems == "object" && Object.keys(steamCleaningItems).length) {
            items.push({ label: 'Steam Cleaning', value: ' ' });
            items = items.concat(steamCleaningItems);
        }

        if (typeof normalCleaningItems == "object" && Object.keys(normalCleaningItems).length) {
            //carpet.
            items.push({ label: 'Shampoo Cleaning', value: ' ' });
            items = items.concat(normalCleaningItems);
        }

        items.push({label: 'Date', value: booking_date});*/

        if (this.props.userProfile && this.props.userProfile.userWallet) {
            const walletAmount = walletUsed; //this.props.userProfile.userWallet.totalAmount;
            const walletCurrency = this.props.userProfile.userWallet.currency.code;
            //console.log('payment', payment);
            if (walletCurrency === currentCurrency && walletAmount > 0) {
                items.push({ label: stringConstants.WALLET, value: `-  ${walletCurrency}  ${walletAmount}` });
            }
        }


        if (total != 0) {
            if (typeof discountData.promoCodeDiscountText != "undefined") {
                var index_to_insert = items.length - 3;
                items.push({ label: 'Promo code', value: discountData.promoCodeDiscountText });
                items.push({ label: 'Subtotal', value: currentCurrency + " " + subtotal.toFixed(2), text_strike: "yes" });
            } else {
                items.push({ label: 'Subtotal', value: currentCurrency + " " + subtotal.toFixed(2) });
            }
            items.push(
                {label: 'Payment', value: payment}
            );
        }

        return items;
    }

    currentStep() {
        return this.props.formCurrentStep;
    }
    moveNext(step) {
        const { formCurrentStep, signInDetails, showLoginModal, showLoginMenu, moveNextStep } = this.props;
        moveNextStep(step, this.state, true);
    }
    updateSquareMeter(carpet) {
        var width = parseInt(carpet.carpet_width);
        var length = parseInt(carpet.carpet_height);
        var unit_size = (carpet.carpet_units != "" && typeof carpet.carpet_units.value != "undefined") ? carpet.carpet_units.value : "";
        var squremeter = 0;

        if ((width != 0 && length != 0) && unit_size != "") {
            squremeter = this.calculateSquareMeter(width, length, unit_size);
            if (squremeter != 0 && !isNaN(squremeter)) {
                return squremeter;
            }
        }
        return 0;
    }
    handleCustomChange = (idx) => (name, value) => {
        var new_name = name.replace("_" + idx, "")

        let newCarpet = this.state.carpet.map((carpetItem, sidx) => {
            if (idx !== sidx) { return carpetItem; }
            return { ...carpetItem, [new_name]: value };
        });

        this.setState({ carpet: newCarpet });
    }
    handleBookingTimeChange(name, value) {
        this.setState({
            booking_time: value
        });
    }
    handleInputChange(name, value) {
        if (name == "payment") {
            this.applyCreditCardCoupon(value);
        }
        this.setState({
            [name]: value
        });
    }
    handleDropDownChange(name, value) {
        console.log(name, value);
    }
    getPrices(type = "") {
        var lite_weight_prices = (typeof this.props.pricePlan != "undefined" && typeof this.props.pricePlan.planDetail != "undefined") ? this.props.pricePlan.planDetail.packages : 0;//this.props.lite_weight_prices;
        if (type != "" && lite_weight_prices.length) {
            return lite_weight_prices.filter((item) => item.type == type).length ? lite_weight_prices.filter((item) => item.type == type)[0].price : 0;
        }
        return lite_weight_prices;
    }
    handleAddCarpet = () => {
        var newCarpet = this.state.carpet.concat([{
            carpet_width: '',
            carpet_height: '',
            carpet_units: { id: 1, value: "cms", label: "cm" },
            squareFeet: 0,
            quantity: 1,
            cleaning_method: { label: "", value: "" }
        }])
        this.setState({ carpet: newCarpet });
        //this.calculatePrice("carpet", newCarpet);
    }
    handleRemoveCarpet = (idx) => () => {
        var newCarpet = this.state.carpet.filter((s, sidx) => idx !== sidx);

        this.setState({ carpet: newCarpet });

        //this.calculatePrice("carpet", newCarpet);
    }
    carpetCleaningOptions(carpetItem) {
        var steam_price = 0;
        var normal_price = 0;
        var steam_price_label = "";
        var normal_price_label = "";
        if (typeof carpetItem != "undefined") {

            //var carpet_sizes_val = carpet_sizes.value;
            steam_price = this.getPrices("steam");
            normal_price = this.getPrices("normal");

            if (steam_price != 0 && normal_price != 0) {
                steam_price_label = " ( " + currentCurrency + " " + steam_price + " /sqm ) ";

                normal_price_label = " ( " + currentCurrency + " " + normal_price + " /sqm ) ";
            }
        }

        var carpet_cleaning_option = [
            { id: 1, value: "steam", label: "Steam Cleaning" + steam_price_label },
            { id: 2, value: "normal", label: "Shampoo Cleaning" + normal_price_label }
        ];

        return carpet_cleaning_option;

    }
    carpetCleaningRequest() {
        var currentStepClass = this.currentStep() === 1 ? '' : "d-none";

        var carpetUnits = [
            { id: 1, value: "cms", label: "cm" },
            { id: 2, value: "inchs", label: "inchs" },
            { id: 3, value: "feets", label: "feet" }
        ];

        var i = 1;
        return (
            <div id="section-request-form" className={currentStepClass}>
                <section>
                    {this.props.showDiscountNotification(this.state.discountData)}
                    <FormTitleDescription title="Please describe the carpets you want cleaned" desc="You can select multiple carpet sizes and cleaning methods" />
                    {
                        this.state.carpet.map((carpetItem, idx) => (
                            <div className={isMobile() ? "row mb-4 multiple-row" : "row mb-2 multiple-row"}>
                                <div className="col-12 col-md-7 mb-3">
                                    <div className="row">
                                        <div className="col-4">
                                            <TextFloatInput InputType="text" name={"carpet_width_" + idx} label="Width" inputValue={carpetItem.carpet_width} onInputChange={this.handleCustomChange(idx)} validationClasses="required numbers" />
                                        </div>
                                        <div className="col-4">
                                            <TextFloatInput InputType="text" name={"carpet_height_" + idx} label="Height" inputValue={carpetItem.carpet_height} onInputChange={this.handleCustomChange(idx)} validationClasses="required numbers" />
                                        </div>
                                        <div className="col-4">
                                            <SelectInput name={"carpet_units_" + idx} inputValue={carpetItem.carpet_units} label="Select" options={carpetUnits} onInputChange={this.handleCustomChange(idx)} validationClasses="required" />
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-md-5 mb-3">
                                    {(idx == 0) && <a href="#" className="cleaning-method-more" onClick={() => this.toggleLearnMorePopUp(true)}>Learn More</a>}
                                    {(idx != 0) && <a href="javascript:void(0)" onClick={this.handleRemoveCarpet(idx)}> <i className="fa fa-trash text-danger"></i> </a>}
                                    <SelectInput name={"cleaning_method_" + idx} inputValue={carpetItem.cleaning_method} label="Select Cleaning method" options={this.carpetCleaningOptions(carpetItem)} onInputChange={this.handleCustomChange(idx)} />
                                </div>
                            </div>

                        ))}
                </section>
                <div className="mb-4">
                    <button className="btn border rounded bg-white text-warning px-3" onClick={this.handleAddCarpet}><i className="fa fa-plus-circle"></i> Add</button>
                </div>
                <section>
                    <FormFieldsTitle title="Pick your start date and time" />
                    <div className="row mb-2">
                        <DatePick
                            name="booking_date"
                            inputValue={this.state.booking_date}
                            selectedDate={this.state.booking_date}
                            disable_friday={true}
                            onInputChange={this.handleInputChange}
                            validationClasses="required" />

                        <TimePicker inputValue={this.state.booking_time}
                            name="booking_time"
                            disableFirstSlot={true}
                            onInputChange={this.handleBookingTimeChange}
                            validationClasses="required"
                            booking_date={this.state.booking_date} />
                    </div>
                </section>
                <section>
                    <FormFieldsTitle title="What else should we know? (Optional)" />
                    <div className="row mb-4">
                        <div className="col mb-3">
                            <TextareaInput name="details" inputValue={this.state.details} placeholder="Please describe the job you need to have done." onInputChange={this.handleInputChange} />
                        </div>
                    </div>
                </section>
                <section>
                    <BookNextStep total={this.state.total} title="Contact Details" moveNext={this.moveNext} toStep="2" setModal={this.mobileSummary} />
                </section>
            </div>
        )
    }
    ContactDetails(isLocationDetailShow) {
        var cityOptions = locationHelper.getLocationByName(current_city);

        var currentStepClass = this.currentStep() === 2 ? '' : "d-none";
        //var currentStepClass =  ''
        var isLocationDetailShow = "yes";

        var contact_details = {
            input_email: this.state.input_email,
            input_phone: this.state.input_phone,
            input_name: this.state.input_name,
            input_last_name: this.state.input_last_name
        }

        if (isLocationDetailShow) {
            contact_details.input_address_city = this.state.input_address_city;
            contact_details.input_address_area = this.state.input_address_area;
            contact_details.input_address_area_building_name = this.state.input_address_area_building_name;
            contact_details.input_address_area_building_apartment = this.state.input_address_area_building_apartment;
        }

        var userProfile = this.props.userProfile;

        return (
            <div id="personal-information-form" className={currentStepClass}>
                <ContactDetailsStep
                    cityOptions={cityOptions}
                    userProfile={userProfile}
                    signInDetails={this.props.signInDetails}
                    isLocationDetailShow={isLocationDetailShow}
                    contact_details={contact_details}
                    handleDropDownChange={this.handleInputChange}
                    handleInputChange={this.handleInputChange}
                    mobileSummary={this.mobileSummary}
                    moveNext={this.moveNext}
                    toStep={3}
                    isBooking={true}
                    total={this.state.total}
                    currentCity={this.props.current_city}
                />
            </div>
        )
    }
    PaymentSection() {
        var payment = this.state.payment;
        var currentStepClass = this.currentStep() === 3 ? '' : "d-none";
        //var currentStepClass ='';
        if (!this.state.paymentLoader) {

            return (
                <div id="payment-method-form" className={currentStepClass}>
                    <section className="payment">
                        <PaymentMethods
                            isCreditCardCouponApplied={this.props.isCreditCardCouponApplied}
                            name="payment"
                            inputValue={payment}
                            status={status} tt={tt}
                            onInputChange={this.handleInputChange} />
                    </section>
                    <section>
                        <BookNextStep total={this.state.total} title="Looking forward to serving you" noNextStep={false} moveNext={this.moveNext} setModal={this.mobileSummary} />
                    </section>
                </div>
            );
        } else {
            return (
                <main loader={this.state.paymentLoader ? "show" : "hide"}><Loader /></main>
            );
        }
    }
    mobileSummary(boolVal) {
        this.setState({
            showMobileSummary: boolVal
        })
    }
    componentDidUpdate(prevProps, prevState) {
        var {carpet,booking_date,voucherCode, normalCleaningItems, steamCleaningItems,payment} = this.state;
        var city = current_city;
        var prices = this.calculatePrice();
        var new_price = this.state.price;

        if (prevState.total != prices.total || prevState.carpet !== carpet) {
            this.setState({
                price: prices.subtotal,
                total: prices.total,
                subtotal: prices.subtotal,
                vat: prices.vat,
                normalCleaningItems: prices.normalCleaningItems,
                steamCleaningItems: prices.steamCleaningItems,
                showMinMessage: prices.showMinMessage,
                carpet_area_for_steam_cleaning: prices.carpet_area_for_steam_cleaning,
                carpet_area_for_shampoo_cleaning: prices.carpet_area_for_shampoo_cleaning,
                walletUsed: prices.walletUsed
            })
            new_price = prices.subtotal;
        }

        if((prevState.carpet !== carpet 
            || prevState.voucherCode !== voucherCode 
            || prevState.booking_date !== booking_date 
            || prevState.payment !== payment) 
            && ( voucherCode != "" ) ){
                var prices = this.calculatePrice(true);
                new_price = prices.subtotal;
            this.handleVoucherChange(voucherCode, new_price);
        }

    }
    componentWillReceiveProps(newProps) {

        if (status == null) {
            if ((newProps.isCreditCardCouponApplied !== this.props.isCreditCardCouponApplied) && newProps.isCreditCardCouponApplied) {
                this.setState({
                    couponCode: ""
                });
                //this.applyCreditCardCoupon();
            }
            if ((newProps.formCurrentStep == 3) && (newProps.formCurrentStep !== this.props.formCurrentStep)) {
                var payment = this.state.payment;
                if (payment == "") {
                    payment = "credit";
                    this.setState({
                        payment: payment
                    });
                    this.applyCreditCardCoupon(payment);
                }
            }
            if (newProps.setUserArea !== this.props.setUserArea) {
                this.setState({
                    input_address_area: newProps.setUserArea
                })
            }
        }
    }
    learnMoreCleaningMethod() {
        return (<div className="learn-more-body">
            <h4 className="text-primary">Steam cleaning</h4>
            <ul className="steam-cleaning">
                <li>Steam cleaning is great for killing bacteria, fungus, dust mites and germs. The heat of the steam is sanitizing and kills 99.99% of bacteria.</li>
                <li>Since only water is used for the cleaning, it is especially suitable for people who suffer allergies.</li>
                <li>Easily removes stains. Hard to remove substances such as wax, glue and chewing gum is dissolved by the steam.</li>
                <li>Suitable for delicate materials (not leather or silk).</li>
            </ul>
            <h4 className="text-primary">Shampoo cleaning</h4>
            <ul className="shampoo-cleaning">
                <li>Shampoo cleaning starts with vacuuming the carpet with a high-powered vacuum.</li>
                <li>Stains are removed using either machines or manually using professional fabric shampoo.</li>
                <li>Excess water is extracted from the carpet.</li>
                <li>A second round of vacuum deep cleaning ensures that remainder shampoo and dirt is removed.</li>
            </ul>
        </div>)
    }
    toggleLearnMorePopUp(state) {
        this.setState({
            learnMore: state
        })
    }
    applyCreditCardCoupon(value) {
        var returnData = this.props.applyCreditCardCoupon(value, this.state.voucherCode);
        var {voucherCode} = this.state;
        var applyCreditCardCoupon = false;
        var payment = typeof value != "undefined" ? value : this.state.payment;
        if (this.state.payment == "") {
            this.setState({
                payment: 'credit',
            });
            applyCreditCardCoupon = true
        } else {
            applyCreditCardCoupon = (payment != "" && payment == "credit") ? true : false;
        }
        if (this.props.isCreditCardCouponApplied && (voucherCode != "" && voucherCode == CREDITCARD_COUPON)) {
            this.setState({
                voucherCode: ""
            });
        } else {
            if (!this.props.isCreditCardCouponApplied && ((applyCreditCardCoupon && payment == "credit") && (voucherCode == "" || voucherCode == CREDITCARD_COUPON))) {
                if (voucherCode == "") {
                    this.setState({
                        voucherCode: CREDITCARD_COUPON
                    });
                }
            } else {
                if (payment == "cash" && voucherCode == CREDITCARD_COUPON) {
                    this.setState({
                        voucherCode: ""
                    });
                }
            }
        }
    }
    render() {
        const items = this.sumarryItems();
        const showPromo = true;
        const showPrice = true;
        let {vat, total} = this.state

        var discountData = this.state.discountData;

        var booking_date = this.state.booking_date;

        var showMinMessage = this.state.showMinMessage;
        const {isWalletAvailable} = this.props;

        var bookingSummary = <NewBookingSummary
            items={items}
            showPromo={showPromo}
            showPrice={showPrice}
            total={total}
            vat={vat}
            booking_date={booking_date}
            discountData={discountData}
            couponValue={this.state.voucherCode}
            handleCouponChange={this.handleInputChange}
            showMobileSummary={this.state.showMobileSummary}
            setModal={this.mobileSummary}
            formData={this.state}
            updateTotal={this.updateTotal}
            isLW={true}
            isSpecializedCleaning={true}
            showMinMessage={showMinMessage}
            walletAvailable={isWalletAvailable}
        />;

        var isLoading = this.state.loader || this.props.loader;
        if (isLoading) {
            return (
                <React.Fragment>
                    <div className="col-lg-8">
                        <main loader={isLoading ? "show" : "hide"}><Loader /></main>
                    </div>
                    <div className="col-md-3 ml-auto">
                        {bookingSummary}
                    </div>
                </React.Fragment>
            )
        } else {
            return (<React.Fragment>
                <div className="col-lg-8">
                    {isWalletAvailable ?
                        <div className='wallet-div'>
                            <img src={"../../../../dist/images/wallet-filled-money-tool-light.png"} className="mh-100" height="28" alt="" />
                            <span className='margin-left30' style={{ verticalAlign: 'middle', fontSize: '15px' }}>{stringConstants.YOU_HAVE_TXT}
                                <span className='wallet-amount-span'>{this.props.userProfile.userWallet.currency.code + ' ' + this.props.userProfile.userWallet.totalAmount} </span>{stringConstants.WALLET_BALANCE_TXT}
                            </span>
                        </div>
                        : null
                    }
                    {this.carpetCleaningRequest()}
                    {this.ContactDetails()}
                    {this.PaymentSection()}
                </div>
                <div className="col-md-3 ml-auto">
                    {bookingSummary}
                </div>
                {this.state.learnMore && <GeneralModal modalSize="modal-lg" title="About our cleaning methods" modalBody={this.learnMoreCleaningMethod()}
                    setModal={this.toggleLearnMorePopUp} />}
            </React.Fragment>)
        }
    }
}
function mapStateToProps(state) {
    return {
        myCreditCardsData: state.myCreditCardsData,
        showLoginMenu: state.showLoginMenu,
        lang:state.lang,
        currentCity: state.currentCity
    }
}
export default withCookies(connect(mapStateToProps)(BookCarpetCleaningPage));
