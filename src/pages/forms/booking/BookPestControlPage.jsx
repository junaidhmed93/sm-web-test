import React from "react";
import FormFieldsTitle from "../../../components/FormFieldsTitle";
import CheckRadioBoxInput from "../../../components/Form_Fields/CheckRadioBoxInput";
import CheckRadioBoxInputWithPrice from "../../../components/Form_Fields/CheckRadioBoxInputWithPrice";
import DatePick from "../../../components/Form_Fields/DatePick";
import TimePicker from "../../../components/Form_Fields/TimePicker";
import TextareaInput from "../../../components/Form_Fields/TextareaInput";
import PaymentMethods from "../../../components/Form_Fields/PaymentMethods";
import NewBookingSummary from "../../../components/Form_Fields/NewBookingSummary";
import BookNextStep from "../../../components/Form_Fields/BookNextStep";
import ContactDetailsStep from "../../../components/ContactDetailsStep";
import GreatForPopUp from "../../../components/GreatForPopUp";
import {isValidSection, scrollToTop, CREDITCARD_COUPON} from "../../../actions/index";
import locationHelper from "../../../helpers/locationHelper";
import commonHelper from "../../../helpers/commonHelper";
import TaxCalculator from "./TaxCalculator";
import BookingVoucherify from "./BookingVoucherify";
import {withCookies} from "react-cookie";
import {connect} from "react-redux";
import Loader from "../../../components/Loader";
import GeneralModal from "../../../components/GeneralModal";
import stringConstants from '../../../constants/stringConstants';
let allServiceConstant = {};
let URLConstant = {};
let currentCurrency = "";
let current_city = "dubai";
let discountData = "";
import {
    zendeskChatBox,
    UAE_ID
} from "../../../actions";
let status = null;
let bid = null;
let tt = null;
class BookPestControlPage extends React.Component{

    constructor(props) {
        super(props);
        const { cookies } = props;

        let defaultServiceOption = props.defaultServiceOption != null ? props.defaultServiceOption : "";

        let bookingData = {
            typeOfPest: defaultServiceOption,
            homeType: "",
            numberOfUnits: "",
            pestOptions:[],
            booking_date:"",
            booking_time:'',
            input_email: props.userProfile.email,
            input_phone: props.userProfile.address ? props.userProfile.address.phoneNumber : '',
            input_name: props.userProfile.customerFirstName,
            input_last_name: props.userProfile.customerLastName,
            input_address_city: {},
            input_address_area: props.setUserArea,
            input_address_area_building_name: props.userProfile.address ? props.userProfile.address.building : '',
            input_address_area_building_apartment: props.userProfile.address ? props.userProfile.address.apartment : '',
            discountData:{},
            voucherCode:"",
            details:"",
            subtotal:0,
            vat:0,
            totalAfterDiscount:0,
            total:0,
            price: 0,
            payment: '',
            showMobileSummary: false,
            loader: false,
            showDialogToGreatFor: false,
            discountPrices:{},
            pestPricePlan: {},
            isNewBookingEngine: true,
            softSafeBookingId: '',
            softSafeRequestId: '',
            walletUsed:''
        };
        let cookieData = props.getCookieFormData();

        if(Object.keys(cookieData).length){ // restore from cookies
            bookingData = cookieData;
            bookingData["softSafeBookingId"] = "";
            bookingData["softSafeRequestId"] = "";
            props.removeCookie("booking_data");
            props.removeCookie("submitted_data");
            props.removeCookie("confirmation_summary");
        }
        this.state = bookingData;

        this.ContactDetails = this.ContactDetails.bind(this);
        this.PestControlRequest = this.PestControlRequest.bind(this);
        this.handleShowMoreOptionChange = this.handleShowMoreOptionChange.bind(this);
        this.handleCustomChange = this.handleCustomChange.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.pestPriceOptions = this.pestPriceOptions.bind(this);
        this.getPrices = this.getPrices.bind(this);
        this.handleDropDownChange = this.handleDropDownChange.bind(this);
        this.currentStep = this.currentStep.bind(this);
        this.moveNext = this.moveNext.bind(this);
        this.handleTypeOfHome = this.handleTypeOfHome.bind(this);
        this.handleNoOfUnits = this.handleNoOfUnits.bind(this);
        this.hashChangeHandler = this.hashChangeHandler.bind(this);
        this.getSubPestControlPriceKey = this.getSubPestControlPriceKey.bind(this);
        this.getPriceList = this.getPriceList.bind(this);
        this.calculatePrice = this.calculatePrice.bind(this);
        this.handleVoucherChange = this.handleVoucherChange.bind(this);
        this.mobileSummary = this.mobileSummary.bind(this);
        this.updateTotal = this.updateTotal.bind(this);
        this.greatForPopUp = this.greatForPopUp.bind(this);
        this.toggleGreatForPopUp = this.toggleGreatForPopUp.bind(this);
        this.fetchPestPrice = this.fetchPestPrice.bind(this);
        
        //console.log("defaultServiceOption", defaultServiceOption);

        this.fetchPestPrice(defaultServiceOption.value);
        
    }
    componentDidMount() {
        allServiceConstant = this.props.service_constants;
        URLConstant =  this.props.url_constants;
       // this.pestPriceOptions();
        current_city = this.props.current_city;

        status = commonHelper.getParameterByName("status");
        bid = commonHelper.getParameterByName("bid");
        tt = commonHelper.getParameterByName("tt");

        if(status == null ){
            let city = locationHelper.getLocationByName( current_city );
            var promoCode =  commonHelper.getParameterByName("promo") != null ? commonHelper.getParameterByName("promo") : "";
            this.setState({
                input_address_city: city,
                loader: false,
                input_address_area: this.props.setUserArea,
                voucherCode: promoCode
            });
        }
        currentCurrency = locationHelper.getCurrentCurrency();
        const walletAvailable = this.props.userProfile && this.props.userProfile.userWallet && this.props.userProfile.userWallet.currency.code === currentCurrency && this.props.userProfile.userWallet.totalAmount > 0 ? true : false;
        this.setState({ isWalletAvailable: walletAvailable });
        
        zendeskChatBox();
        window.addEventListener("hashchange", this.hashChangeHandler, false);
    }
    componentWillUnmount(){
        window.removeEventListener("hashchange", this.hashChangeHandler, false);
    }
    calculatePrice(getTotal = false){

        let {typeOfPest, homeType, numberOfUnits, booking_date,voucherCode, discountData, discountPrices, isWalletAvailable} = this.state,
        changed_element = null , changedValue = null;

        let total_price = 0;
        let taxRate = 0 ;

        if(typeof numberOfUnits.value != "undefined"){
            //console.log(numberOfUnits.price);
            total_price = parseInt(numberOfUnits.price);
        }

        let walletUsed = 0;
        if (this.props.userProfile && this.props.userProfile.userWallet) {
            const walletAmount = this.props.userProfile.userWallet.totalAmount;
            const walletCurrency = this.props.userProfile.userWallet.currency.code;
            
            if (walletCurrency === currentCurrency && walletAmount > 0) {
                if (walletAmount >= total_price) {
                    walletUsed = total_price;
                    total_price = 0;
                }
                else if (walletAmount < total_price) {
                    walletUsed = walletAmount;
                    total_price = total_price - walletAmount;
                }
            }
        }

        let prefferedDate = booking_date;

        let taxChargesTotal = TaxCalculator.calculateVAT(total_price,prefferedDate);

        let total = total_price + taxChargesTotal;
        
        let returnData = {
            total: total,
            subtotal : total_price,
            vat:taxChargesTotal,
            walletUsed: walletUsed 
        };

        if( !getTotal && ( typeof discountData != "undefined" &&  ( typeof discountData.success != "undefined" && discountData.success ) )){ 
            returnData = this.updateTotal(discountPrices, true );
           // console.log("returnData adsa", returnData);
        }

        return returnData;
    }
    sumarryItems(){
        const {typeOfPest, homeType, numberOfUnits, booking_date, booking_time,discountData,subtotal, vat, payment, walletUsed, total} = this.state;

        let timeDetails = [];

        if(typeof booking_time.value != "undefined"){
            timeDetails.push(booking_time.label);
        }
        if( booking_date != ""){
            timeDetails.push(booking_date);
        }

        let jobDetails = [];

        let typeOfPestLbl = typeof typeOfPest == "object" ? "Pest control - "+typeOfPest.label : "Pest control";

        jobDetails.push(typeOfPestLbl);

        if(typeof homeType.label != "undefined"){
            let homeLbl = homeType.label;
            homeLbl += typeof numberOfUnits.label != "undefined" ?  " - " + numberOfUnits.label : "";
            jobDetails.push(homeLbl);
        }

        var items = [
            {label: 'Time', value: timeDetails},
            {label: 'Details', value: jobDetails, conClass: 'col-12 mb-4'}
        ];

        if(total != 0){
            if(typeof discountData.promoCodeDiscountText != "undefined"){
                items.push({label: 'Promo code', value: discountData.promoCodeDiscountText});
                items.push({label: 'Subtotal', value: currentCurrency+" "+subtotal, text_strike:"yes"});
            }else{
                items.push({label: 'Subtotal', value: currentCurrency+" "+subtotal, classNme:'text-warning'});
            }
            items.push(
                {label: 'Payment', value: payment}
            );
        }
        /*items =  [
            {label: 'Type', value: typeOfPestLbl},
            {label: 'Home Type', value: homeType.label},
            {label: 'Units', value: numberOfUnits.label},
            {label: 'Date', value: booking_date},
            {label: 'Time', value: booking_time.label}
        ];*/
        if (this.props.userProfile && this.props.userProfile.userWallet) {
            const walletAmount = walletUsed; //this.props.userProfile.userWallet.totalAmount;
            const walletCurrency = this.props.userProfile.userWallet.currency.code;
            
            if (walletCurrency === currentCurrency && walletAmount > 0) {
                items.push({ label: stringConstants.WALLET, value: `-  ${walletCurrency}  ${walletAmount}` });
            }
        }
        return items;
    }
    hashChangeHandler(){
        let hashVal = window.location.hash;
        let step;
        hashVal.length ? step = parseInt( window.location.hash.replace('#','') ) : step = 1;
        this.props.moveNext(step);
        scrollToTop();
    }
    currentStep(){
        return this.props.formCurrentStep;
    }
    moveNext(step){
        const {formCurrentStep, signInDetails, showLoginModal, showLoginMenu, moveNextStep} = this.props;

        let continueAsGuest = typeof showLoginMenu.guest != "undefined" && showLoginMenu.guest == false ? true : false

        let valid = true;
        // Step 1 validation
        if( formCurrentStep == '1' || formCurrentStep == '2' || formCurrentStep == '3'){
            moveNextStep(step, this.state, true);

        }
    }
    handleShowMoreOptionChange(){
        this.setState({
            show_more_option: !this.state.show_more_option
        });
    }
    handleCustomChange(name, value){
        this.setState({
            booking_time: value
        });
    }
    handleInputChange(name, value){

        //console.log(name, value);
        if(name == "payment"){
            this.applyCreditCardCoupon(value);
            this.setState({
                [name]: value,
            });
        }else{
            this.setState({
                [name]: value
            });
        }
        if(name == "typeOfPest"){
            this.fetchPestPrice(value.value);
            
        }
        
    }
    handleDropDownChange(name, value){
        console.log(name, value);
    }
    handleNoOfUnits(name, value){
        this.setState({
            numberOfUnits: value
        });

    }
    fetchPestPrice(service_id){
        this.props.fetchPrices(service_id);
    }
    getPrices(){
        let lite_weight_prices = this.props.lite_weight_prices;

        return lite_weight_prices
    }
    handleTypeOfHome(name, value){
        this.setState({
            homeType: value
        });
        this.pestPriceOptions(this.state.pestPricePlan, this.state.typeOfPest, value)
    }
    getSubPestControlPriceKey(typeOfPestValue){
        let  sub_pest_key = "";
        if (allServiceConstant.SERVICE_COCKROACHES_PEST_CONTROL == typeOfPestValue) {
            sub_pest_key = "SERVICE_COCKROACHES";
        } else if (allServiceConstant.SERVICE_ANTS_PEST_CONTROL == typeOfPestValue) {
            sub_pest_key = "SERVICE_ANTS";
        } else if (allServiceConstant.SERVICE_BED_BUGS_PEST_CONTROL == typeOfPestValue) {
            sub_pest_key = "SERVICE_BED_BUGS";
        } else if (allServiceConstant.SERVICE_MOVE_IN_PEST_CONTROL == typeOfPestValue) {
            sub_pest_key = "SERVICE_MOVE_IN_PEST_CONTROL";
        }
        return sub_pest_key;
    }
    getPriceList( packages = {} , type_of_home){

        let pestPricePlan = packages;

        let numberOfUnits = [];
        
        let pestPricePackages = typeof pestPricePlan.planDetail != "undefined" ?  pestPricePlan.planDetail.packages : [];
        
        if(pestPricePackages.length){
            let homeTypeValue = typeof type_of_home ==  "undefined" ? this.state.homeType.value : type_of_home.value;
            let pest_prices = [];
            if(typeof homeTypeValue != "undefined" ) {
                let price_key = homeTypeValue.toLowerCase();
                let i = 1;
                let key_label = "";
                pestPricePackages.find((item) => {
                    let key = item.rooms;
                    if(item.type == price_key ) {
                        if (key == 0) {
                            key_label = "Studio";
                        } else {
                            key_label = key + " BR";
                        }
                        numberOfUnits.push({
                            id: i,
                            value: key,
                            label: key_label,
                            price: item.price,
                            currentCurrency: currentCurrency,
                            planId : pestPricePlan.id
                        })
                        i++;
                    }
                 });
            }
        }
        /*if (sub_pest_key != "") {

            let homeTypeValue = typeof type_of_home ==  "undefined" ? this.state.homeType.value : type_of_home.value;

            if(typeof  homeTypeValue != "undefined" ) {

                let numberOfUnitsValue = this.state.numberOfUnits;

                let price_key = homeTypeValue.toLowerCase();

                let pest_prices = prices[sub_pest_key][price_key];

                let i = 1;
                let key_label = "";
                Object.keys(pest_prices).forEach(function (key) {
                    if (key == 0) {
                        key_label = "Studio";
                    } else {
                        key_label = key + " BR";
                    }
                    numberOfUnits.push({
                        id: i,
                        value: key,
                        label: key_label,
                        price: pest_prices[key],
                        currentCurrency: currentCurrency
                    });
                    i++;
                });
            }*/
        return numberOfUnits;
    }
    pestPriceOptions( packages, typeOfPest, typeOfHome){
        
        let {numberOfUnits} = this.state;

        let typeOfPestValue = typeof typeOfPest ==  "undefined" ? allServiceConstant.SERVICE_COCKROACHES_PEST_CONTROL : typeOfPest.value;

        let subPestKey = this.getSubPestControlPriceKey(typeOfPestValue);

        let pestOptions = this.getPriceList( packages, typeOfHome);

        let newNumberOfUnits = [];

        if(Object.keys(numberOfUnits).length){
            newNumberOfUnits = pestOptions.filter((item) => (
                numberOfUnits.value == item.value
            ))
        }

        if(newNumberOfUnits.length){
            newNumberOfUnits = newNumberOfUnits[0];
            this.setState({
                pestOptions: pestOptions,
                numberOfUnits: newNumberOfUnits
            });
        }else{
            this.setState({
                pestOptions: pestOptions,
                numberOfUnits: ""
            });
        }
    }
    toggleGreatForPopUp(boolVal){
        this.setState({showDialogToGreatFor:boolVal});
    }
    greatForPopUp(serviceUrl = ""){
        return (<GreatForPopUp service={serviceUrl} />);
    }
    PestControlRequest(){
        let dataValues = this.props.dataValues;

        let service_constants = allServiceConstant;

        let homeType = [
            {id: 1, value: "Apartment", label: "Apartment"},
            {id: 2, value: "Villa", label: "Villa"}
        ];
        var getServiceCode = this.props.getServiceCode;
        
        let typeOfPest = [
            {id: 1, value: service_constants.SERVICE_COCKROACHES_PEST_CONTROL, serviceCode: getServiceCode(service_constants.SERVICE_COCKROACHES_PEST_CONTROL),label: "Cockroaches"},
            {id: 2, value: service_constants.SERVICE_BED_BUGS_PEST_CONTROL, serviceCode: getServiceCode(service_constants.SERVICE_BED_BUGS_PEST_CONTROL), label: "Bed bugs"},
            {id: 3, value: service_constants.SERVICE_ANTS_PEST_CONTROL, serviceCode: getServiceCode(service_constants.SERVICE_ANTS_PEST_CONTROL), label: "Ants"},
            {id: 4, value: service_constants.SERVICE_MOVE_IN_PEST_CONTROL, serviceCode: getServiceCode(service_constants.SERVICE_MOVE_IN_PEST_CONTROL), label: "General"}
        ];

        let homeTypeValue = this.state.homeType;

        let numberOfUnitsValue = this.state.numberOfUnits;

        let typeOfPestValue = this.state.typeOfPest;

        let booking_date = this.state.booking_date;

       // console.log(typeOfPestValue);

        //other_services
        let currentStepClass = this.currentStep() === 1 ? '' : "d-none";

        let pestOptions = this.state.pestOptions;

        //console.log( "service", URLConstant.PEST_CONTROL_PAGE_URI );
        let isTaxApplicable = locationHelper.getCurrentCountryId() == UAE_ID ? true : false;
        return (
            <div id="section-request-form" className={currentStepClass}>
                <section>
                    {this.props.showDiscountNotification(this.state.discountData)} 
                    <FormFieldsTitle title="What type of pest control are you looking for?" titleClass="h3 mb-2"/>
                    <div className="learn-more mb-3"><a href="#" className="text-primary" onClick={ () => this.toggleGreatForPopUp(true) }> (Which option is right for me?) </a></div>
                    <CheckRadioBoxInput
                        InputType="radio"
                        name="typeOfPest"
                        inputValue={typeOfPestValue}
                        items={typeOfPest}
                        onInputChange={this.handleInputChange}
                        parentClass="row mb-1"
                        childClass="col-12 col-md-3 mb-3 d-flex"
                        validationClasses="radio-required"
                    />
                </section>
                
                <section>
                    <FormFieldsTitle title="Describe your home"/>
                    <CheckRadioBoxInput
                        InputType="radio" name="homeType"
                        inputValue={homeTypeValue}
                        items={homeType}
                        onInputChange={this.handleTypeOfHome}
                        parentClass="row mb-2"
                        validationClasses="radio-required"
                        validationMessage="Please describe your home type."
                        childClass="col-12 col-md-4 mb-2 d-flex"/>
                </section>
                <section className={pestOptions.length == 0 ? "mb4" : ""}>
                { pestOptions.length != 0 &&
                    <CheckRadioBoxInputWithPrice
                        isTaxApplicable = {isTaxApplicable}
                        InputType="radio"
                        name="numberOfUnits"
                        inputValue={numberOfUnitsValue}
                        items={pestOptions}
                        onInputChange={this.handleInputChange}
                        childClass="col-6 col-sm-3 mb-3 d-flex"
                        parentClass="row"
                        validationClasses="radio-required"
                        validationMessage="Please select your home size."/>  }
                </section>

                <section>
                    <FormFieldsTitle title="Pick your start date and time" />
                    <div className="row mb-4">
                        <DatePick
                            name="booking_date"
                            inputValue={booking_date}
                            selectedDate={booking_date}
                            disable_friday={true}
                            onInputChange={this.handleInputChange}
                            validationClasses="required"/>
                        <TimePicker inputValue={this.state.booking_time}
                                    name="booking_time"
                                    disableFirstSlot={true}
                                    onInputChange={ this.handleCustomChange }
                                    validationClasses="required"
                                    booking_date={this.state.booking_date}/>
                    </div>
                </section>
                <section>
                    <FormFieldsTitle title="Tell us about the job, and share any special instructions" />
                    <div className="row mb-3">
                        <div className="col">
                            <TextareaInput name="details" placeholder="Please describe the job you need to have done."  onInputChange={this.handleInputChange} />
                        </div>
                    </div>
                </section>
                <section>
                    <BookNextStep total={this.state.total} title="Contact Details" moveNext={this.moveNext} toStep="2" setModal={this.mobileSummary}/>
                </section>
            </div>
        )
    }
    ContactDetails(){
        let cityOptions = locationHelper.getLocationByName(current_city);
        let currentStepClass = this.currentStep() === 2 ? '' : "d-none";
        //let currentStepClass =  ''
        let isLocationDetailShow = "yes";

        let contact_details = {
            input_email:this.state.input_email,
            input_phone:this.state.input_phone,
            input_name:this.state.input_name,
            input_last_name:this.state.input_last_name
        }

        if(isLocationDetailShow){
            contact_details.input_address_city = this.state.input_address_city;
            contact_details.input_address_area = this.state.input_address_area;
            contact_details.input_address_area_building_name = this.state.input_address_area_building_name;
            contact_details.input_address_area_building_apartment = this.state.input_address_area_building_apartment;
        }

        let userProfile =  this.props.userProfile;

        return (
            <div id="personal-information-form" className={currentStepClass}>
                <ContactDetailsStep
                    cityOptions={cityOptions}
                    userProfile = {userProfile}
                    signInDetails={this.props.signInDetails}
                    isLocationDetailShow={isLocationDetailShow}
                    contact_details={contact_details}
                    handleDropDownChange={this.handleInputChange}
                    handleInputChange={this.handleInputChange}
                    mobileSummary={this.mobileSummary}
                    moveNext={this.moveNext}
                    toStep={3}
                    isBooking={true}
                    total={this.state.total}
                    currentCity = {this.props.current_city}
                />
            </div>
        )
    }
    PaymentSection(){
        let payment = this.state.payment;
        let currentStepClass = this.currentStep() === 3 ? '' : "d-none";
        //let currentStepClass ='';
        if(!this.state.paymentLoader) {

            return (
                <div id="payment-method-form" className={currentStepClass}>
                    <section className="payment">
                        <PaymentMethods 
                            isCreditCardCouponApplied = {this.props.isCreditCardCouponApplied}
                            name="payment" 
                            inputValue={payment} 
                            status={status} tt={tt} 
                            onInputChange={this.handleInputChange} />
                    </section>
                    <section>
                        <BookNextStep total={this.state.total} title="Looking forward to serving you" noNextStep={false} moveNext={this.moveNext} setModal={this.mobileSummary} />
                    </section>
                </div>
            );
        } else {
            return (
                <main loader={this.state.paymentLoader ? "show" : "hide"}><Loader/></main>
            );
        }
    }
    //handleVoucherChange(value, changed_element = null , changedValue = null, new_price = 0) {
    handleVoucherChange(value, new_price = 0) {

        let couponValue = value.trim(),
            changed_element = null , changedValue = null;
        let {typeOfPest, homeType, numberOfUnits, booking_date, booking_time,discountData,subtotal, vat, payment, price,walletUsed} = this.state;
        this.setState({
            discountData: "load"
        })
        if(price == 0 && new_price == 0 ){
            var response = BookingVoucherify.error("Please select the package");
            this.setState({
                discountData: response.data
            });
        }else{

            if (typeof couponValue == 'undefined' || couponValue.length == 0) {
                let response = BookingVoucherify.error("Please provide coupon.");
                this.setState({
                    discountData: response.data
                });
                //this.calculatePrice("reset_coupon", true);
            } else if (couponValue.trim().length < 3) { // minimum length of a promo code is more than 3 characters, so need to go to server if less than that
                let response = BookingVoucherify.error("Promotion code is invalid");
                this.setState({
                    discountData: response.data
                })
                //this.calculatePrice("reset_coupon", true);
            }
            else {
                if(new_price != 0 ){
                    price = new_price;
                }

                if(changed_element == "typeOfPest"){
                    typeOfPest = changedValue;
                }else if(changed_element == "homeType"){
                    homeType = changedValue;
                }
                else if( changed_element == "numberOfUnits"){
                    numberOfUnits = changedValue;
                }
                else if(changed_element == "booking_date"){
                    booking_date = changedValue;
                }

                let city = current_city;
                price = walletUsed > 0 ? walletUsed + price : price; // for wallet case
                let rules = BookingVoucherify.generateRules(couponValue, price, typeOfPest, 0, booking_date, city, "", payment);

                BookingVoucherify.validate(couponValue, rules).then((response)=> {
                    let res = response;
                    if(this.state.voucherCode != "" || (this.state.voucherCode == CREDITCARD_COUPON && this.state.payment == "credit")){
                        this.updateTotal(res);
                    }
                });
            }
        }
    }
    updateTotal(res, isReturnData = false){
        //console.log('voucher res', res);
        let { walletUsed, booking_date } = this.state;
        let response = res;
        let discountData = response.data;
        let currentTotal = response.currentTotal;
        let taxChargesTotal = response.taxChargesTotal;
        let total = response.total;

        if (walletUsed > 0) {
            
            currentTotal = parseFloat((response.totalAfterDiscount - walletUsed).toFixed(2));
            taxChargesTotal = TaxCalculator.calculateVAT(currentTotal, booking_date);
            total = parseFloat((currentTotal + taxChargesTotal).toFixed(2));
            //console.log('here',currentTotal);
        }

        let returnData = {
            price: currentTotal,
            total: total,
            subtotal : currentTotal,
            vat:taxChargesTotal,
            walletUsed: walletUsed
        };
        if(typeof res.isCreditCardCouponApplied != "undefined" && res.isCreditCardCouponApplied){
            returnData["voucherCode"] = "";
            this.props.updateCreditCardCouponApplied();
        }
        if( isReturnData ){
            return returnData;
        }else{
            returnData["discountData"] = discountData;
            returnData["discountPrices"] =  res
        }

        this.setState( returnData );
    }
    mobileSummary(boolVal){
        this.setState({
            showMobileSummary: boolVal
        })
    }
    componentDidUpdate(prevProps, prevState) {
        let {voucherCode, price, typeOfPest,numberOfUnits,homeType, booking_date, discountData, discountPrices, payment} = this.state;
        let city = current_city;
        let prices = this.calculatePrice();
        let new_price = this.state.price;
        
        /*if( prevState.discountData != discountData && 
            (typeof discountData != "undefined" && 
            ( typeof discountData.success != "undefined" && discountData.success ) )
          ){
              this.updateTotal()
            //console.log( "discountPrices 123", discountPrices );
        }*/

        if(prevState.total != prices.total) {
            this.setState({
                price: prices.subtotal,
                total: prices.total,
                subtotal: prices.subtotal,
                vat: prices.vat,
                walletUsed : prices.walletUsed
            })
            new_price = prices.subtotal;
        }
        if((prevState.typeOfPest !== typeOfPest || 
            prevState.numberOfUnits !== numberOfUnits || 
            prevState.homeType !== homeType || 
            prevState.voucherCode !== voucherCode || 
            prevState.payment !== payment ||
            prevState.booking_date !== booking_date) 
            && ( voucherCode != "" ) ){
                let prices = this.calculatePrice(true);
                new_price = prices.subtotal;
            this.handleVoucherChange(voucherCode, new_price);
        }
    }
    componentWillReceiveProps (newProps) {
        if (status == null) {
            if((newProps.isCreditCardCouponApplied !== this.props.isCreditCardCouponApplied) && newProps.isCreditCardCouponApplied){
                this.setState({
                    couponCode: ""
                });
                //this.applyCreditCardCoupon();
            }
            if ( (newProps.formCurrentStep == 3 ) && ( newProps.formCurrentStep !== this.props.formCurrentStep )) {
                let payment = this.state.payment;
                if (payment == "") {
                    payment = "credit";
                    this.setState({
                        payment: payment
                    });
                    this.applyCreditCardCoupon(payment);
                }
            }
            if((newProps.pricePlan !== this.props.pricePlan) ){
                this.setState({
                    pestPricePlan: newProps.pricePlan
                });
                this.pestPriceOptions( newProps.pricePlan,this.state.typeOfPest, this.state.homeType );
                //this.applyCreditCardCoupon();
            }
            if (newProps.softSafeBookingId !== this.props.softSafeBookingId) {
                var payment = this.state.payment;
                if (payment == "") {
                    payment = "credit";
                }
                this.setState({
                    softSafeBookingId: newProps.softSafeBookingId,
                    softSafeRequestId: newProps.softSafeRequestId
                })
                //Credit card coupon
                this.applyCreditCardCoupon(payment);

            }
        }
        if (newProps.setUserArea !== this.props.setUserArea) {
            this.setState({
                input_address_area: newProps.setUserArea
            })
        }
    }
    applyCreditCardCoupon(value){
        var returnData = this.props.applyCreditCardCoupon(value, this.state.voucherCode);
        var {voucherCode} = this.state;
        var applyCreditCardCoupon = false;
        var payment = typeof value != "undefined" ? value : this.state.payment;
        if (this.state.payment == "") {
            this.setState({
                payment: 'credit',
            });
            applyCreditCardCoupon = true
        } else {
            applyCreditCardCoupon = (payment != "" && payment == "credit") ? true : false;
        }
        if (this.props.isCreditCardCouponApplied && (voucherCode != "" && voucherCode == CREDITCARD_COUPON)) {
            this.setState({
                voucherCode: ""
            });
        } else {
            if (!this.props.isCreditCardCouponApplied && ((applyCreditCardCoupon && payment == "credit") && (voucherCode == "" || voucherCode == CREDITCARD_COUPON))) {
                if (voucherCode == "") {
                    this.setState({
                        voucherCode: CREDITCARD_COUPON
                    });
                }
            } else {
                if (payment == "cash" && voucherCode == CREDITCARD_COUPON) {
                    this.setState({
                        voucherCode: ""
                    });
                }
            }
        }
    }
    render(){
        
        const {
            discountData,
            payment,
            voucherCode,
            subtotal,
            vat,
            totalAfterDiscount,
            total,
            price } = this.state;

        let items = this.sumarryItems();
        
        const showPromo = true;
        const showPrice = true;

        let booking_date = this.state.booking_date;

        let isLoading =  this.state.loader || this.props.loader;

        const {isWalletAvailable} = this.props;
        let bookingSummary = <NewBookingSummary
            items={items}
            showPromo={showPromo}
            showPrice={showPrice}
            total={total}
            vat={vat}
            booking_date={booking_date}
            discountData={discountData}
            couponValue={this.state.voucherCode}
            handleCouponChange={this.handleInputChange}
            showMobileSummary={this.state.showMobileSummary}
            setModal={this.mobileSummary}
            formData={this.state}
            updateTotal={this.updateTotal}
            isLW={true}
            walletAvailable={isWalletAvailable}
        />;

        if(isLoading) {
            return (
                <React.Fragment>
                    <div className="col-lg-8">
                        <main loader={ isLoading ? "show" : "hide"}><Loader/></main>
                    </div>
                    <div className="col-md-3 ml-auto">
                        {bookingSummary}
                    </div>
                </React.Fragment>
            );

        }else {
            return (
                <React.Fragment>
                    <div className="col-lg-8">
                    {this.props.userProfile && this.props.userProfile.userWallet && this.props.userProfile.userWallet.currency.code === currentCurrency && this.props.userProfile.userWallet.totalAmount > 0?
                            <div className='wallet-div'> {/*< i src={staticVariables.IMGIX_REACT_BASE_URL + "/dist/images/why-2.png"}></i> */}
                                <img src={ "../../../../dist/images/wallet-filled-money-tool-light.png"} className="mh-100" height="28" alt="" />
                                <span className='margin-left30' style={{verticalAlign: 'middle',fontSize: '15px'}}>{stringConstants.YOU_HAVE_TXT}
                                    <span className='wallet-amount-span'>{this.props.userProfile.userWallet.currency.code + ' ' + this.props.userProfile.userWallet.totalAmount} </span>{stringConstants.WALLET_BALANCE_TXT}
                                </span>
                            </div>
                            : null
                        }
                        {this.PestControlRequest()}
                        {this.ContactDetails()}
                        {this.PaymentSection()}
                    </div>
                    <div className="col-md-3 ml-auto">
                        {bookingSummary}
                    </div>
                    {this.state.showDialogToGreatFor &&
                    <GeneralModal modalSize="modal-lg" title="Which option is right for me?" modalBody={this.greatForPopUp(URLConstant.PEST_CONTROL_PAGE_URI)}
                                  setModal={this.toggleGreatForPopUp}/>}
                </React.Fragment>
            )
        }
    }
}
function mapStateToProps(state){
    return {
        myCreditCardsData: state.myCreditCardsData,
        showLoginMenu: state.showLoginMenu,
        currentCityID: state.currentCityID,
        pricePlan:state.PricePlan,
        currentCity: state.currentCity,
        lang: state.lang
    }
}
export default withCookies(connect(mapStateToProps)(BookPestControlPage));