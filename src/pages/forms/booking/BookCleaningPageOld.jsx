import React from "react";
import FormFieldsTitle from "../../../components/FormFieldsTitle";
import CheckRadioBoxInput from "../../../components/Form_Fields/CheckRadioBoxInput";
import CheckRadioBoxInputWithPrice from "../../../components/Form_Fields/CheckRadioBoxInputWithPrice";
import CleaningDatePick from "../../../components/Form_Fields/CleaningDatePick";
import CleaningTimePicker from "../../../components/Form_Fields/CleaningTimePicker";
import TextareaInput from "../../../components/Form_Fields/TextareaInput";
import PaymentMethods from "../../../components/Form_Fields/PaymentMethods";
import SubmitBooking from "../../../components/Form_Fields/SubmitBooking";
import BookingSummary from "../../../components/Form_Fields/BookingSummary";
import BookNextStep from "../../../components/Form_Fields/BookNextStep";
import FormTitleDescription from "../../../components/FormTitleDescription";
import ContactDetailsStep from "../../../components/ContactDetailsStep";
import {isValidSection, scrollToTop, getAreaOptions, isEmailExist, validEmail} from "../../../actions/index";
import {backend_url} from "../../../api";
import {connect} from "react-redux";
import TaxCalculator from "./TaxCalculator"
import BookingCoupons from "./BookingCoupons"
import locationHelper from "../../../helpers/locationHelper";
import commonHelper from "../../../helpers/commonHelper";
import axios from 'axios';
import moment from 'moment';
import GeneralModal from "../../../components/GeneralModal";
import Loader from "../../../components/Loader";
import {
    registerCreditCard,
    postRequest,
    fetchSignUp,
    fetchSignIn,
    fetchUserProfile,
    setSignIn,
    putUpdateCustomerProfile,
    getAllCreditCard,
    fetchOffersData
} from "../../../actions";
import {bindActionCreators} from "redux";
import {withCookies} from "react-cookie";
import PackageBox from "../../../components/Form_Fields/PackageBox";
let allServiceConstant = {};
let URLConstant = {};
let current_currency = "AED";
let current_city = "dubai";
let current_city_id = "1";
let data_values = {}
let bookingFrequencyDataConstants = {};
let bookingTaxPlan = {};
let bookingPricingPlan = {};
var field_multiple_times_week_options = [
    {id: 1, value: 7, label: "Sunday"},
    {id: 2, value: 1, label: "Monday"},
    {id: 3, value: 2, label: "Tuesday"},
    {id: 4, value: 3, label: "Wednesday"},
    {id: 5, value: 4, label: "Thursday"},
    {id: 6, value: 6, label: "Saturday"},
];

class BookCleaningPage extends React.Component{

    constructor(props) {
        super();

        this.state = {
            show_more_option : false,
            booking_date: '',
            booking_time: '',
            field_service_needed:{},
            hours_required: {id: 4, value: "4", label: "4 hours",price: 35, current_currency: current_currency},
            number_of_cleaners: {id: 1, value: "1", label: "1 Cleaner"},
            field_own_equipments: {id: 'no', value: "No", label: "No"},
            other_services:[],
            field_multiple_times_week:[],
            input_email: props.userProfile.email,
            input_phone: props.userProfile.address ? props.userProfile.address.phoneNumber : '',
            input_name: props.userProfile.customerFirstName,
            input_last_name: props.userProfile.customerLastName,
            input_address_city: '',
            input_address_area: props.setUserArea,
            input_address_area_building_name: props.userProfile.address ? props.userProfile.address.building : '',
            input_address_area_building_apartment: props.userProfile.address ? props.userProfile.address.apartment : '',
            discountData:{},
            packages:{},
            isSubscription: false,
            typeOfSubscription: "",
            planId:0,
            coupon_code:"",
            hourlyRate: 0,
            subtotal:0,
            vat:0,
            totalAfterDiscount:0,
            total:0,
            price: 0,
            payment: '',
            bookingPricingPlan:{},
            bookingTaxPlan:{},
            showMobileSummary: false,
            loginRequired: false,
            paymentLoader: false
        }
        this.ContactDetails = this.ContactDetails.bind(this);
        this.CleaningRequest = this.CleaningRequest.bind(this);
        this.handleShowMoreOptionChange = this.handleShowMoreOptionChange.bind(this);
        this.handleCustomChange = this.handleCustomChange.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleDropDownChange = this.handleDropDownChange.bind(this);
        this.currentStep = this.currentStep.bind(this);
        this.moveNext = this.moveNext.bind(this);
        this.hashChangeHandler = this.hashChangeHandler.bind(this);
        this.calculatePrice = this.calculatePrice.bind(this);
        //this.fetchBookingPricingPlan = this.fetchBookingPricingPlan.bind(this);
        this.hoursRequiredOptions = this.hoursRequiredOptions.bind(this);
        this.handleCouponChange = this.handleCouponChange.bind(this);
        this.mobileSummary = this.mobileSummary.bind(this);
        this.toggleServiceDateChangeDialog = this.toggleServiceDateChangeDialog.bind(this);
        this.getChangeServiceDateModalContent = this.getChangeServiceDateModalContent.bind(this);
        this.paymentHandler = this.paymentHandler.bind(this);
        this.updateUserProfileAndGoToPayment = this.updateUserProfileAndGoToPayment.bind(this);
        this.netflixCleaningPackages = this.netflixCleaningPackages.bind(this);

    }

    componentDidMount() {
        allServiceConstant = this.props.service_constants;
        URLConstant =  this.props.url_constants;
        current_city = this.props.current_city;
        bookingFrequencyDataConstants = this.props.dataConstants["BOOKING_FREQUENCY"];
        data_values = commonHelper.processDataValues(bookingFrequencyDataConstants);
        bookingTaxPlan = this.props.taxPlan;
        bookingPricingPlan =  this.props.pricePlan;
        //console.log(bookingPricingPlan);
        const url_params = this.props.url_params;
        //console.log("BookCleaningPage",url_params);

        var isSubscription = typeof url_params.subscription != "undefined" ? true : false;
        var typeOfSubscription = "";
        var field_service_needed = {id: data_values.DATA_VALUE_EVERY_WEEK, value: data_values.DATA_VALUE_EVERY_WEEK, label: "Every week",desc:"Same cleaner every time"};
        var hours_required = {id: 4, value: "4", label: "4 hours",price: 35, current_currency: current_currency};
        if(isSubscription){
            typeOfSubscription = url_params.subscription == "subscription" ? "weekly" : "daily";
            var noOfHours =  typeOfSubscription == "daily" ? hours_required.value : null;
            //console.log(typeOfSubscription);
            var number_of_cleaners_val = 1;
            if(typeOfSubscription == "daily"){
                field_service_needed = {id: "3", value: data_values.DATA_VALUE_MULTIPLE_TIMES_WEEK, label: "Several times a week",desc:"Same cleaner every time"};
            }

            var packages = this.netflixCleaningPackages(typeOfSubscription , number_of_cleaners_val, noOfHours);
            var selectedPackage = {};
            var field_multiple_times_week = [];
            if(packages.length){
                selectedPackage = packages[0];
                if(typeof selectedPackage.data_weekdays != "undefined") {
                    field_multiple_times_week = selectedPackage.weekdays;
                }

            }
        }

        this.props.getAllCreditCard(this.props.signInDetails.access_token)
            .then(() => {
                this.setState({
                    loader: false
                })
            });

        var city = locationHelper.getLocationByName( current_city );

        this.setState({
            field_service_needed: field_service_needed,
            loader: true,
            input_address_city: city,
            isSubscription: isSubscription,
            typeOfSubscription: typeOfSubscription,
            hours_required: hours_required,
            packages: selectedPackage
        });
        var changed_element = null , changed_value = null;
        if(isSubscription){
            changed_element = "packages";
            changed_value = selectedPackage;
        }
       this.calculatePrice(changed_element, changed_value);

       window.addEventListener("hashchange", this.hashChangeHandler, false);
    }
    componentWillUnmount(){
        window.removeEventListener("hashchange", this.hashChangeHandler, false);
    }
    componentWillReceiveProps (newProps) {
        if( newProps.userProfile !== this.props.userProfile ) {
            this.props.getAllCreditCard(this.props.signInDetails.access_token)
                .then(() => {
                    this.setState({
                        loader: false
                    })
                });
            this.setState({
                loader: true
            });
            var coupon_code = this.state.coupon_code;
            this.handleCouponChange(coupon_code);
        }
    }

    currentStep(){
        return this.props.formCurrentStep;
    }

    calculatePrice(changed_element = null , changed_value = null ){

        var total_price = 0;
        var hourlyRate = 0;

        // Here is the cost calculations
        var {hours_required,field_service_needed,field_own_equipments,number_of_cleaners, booking_date,coupon_code, field_multiple_times_week, isSubscription, typeOfSubscription, packages} = this.state;

        if(changed_element == "hours_required"){
            hours_required = changed_value;
        }else if(changed_element == "field_own_equipments"){
            field_own_equipments = changed_value;
        }else if(changed_element == "field_service_needed"){
            field_service_needed = changed_value;
        }else if(changed_element == "number_of_cleaners"){
            number_of_cleaners = changed_value;
        }
        else if(changed_element == "booking_date"){
            booking_date = changed_value;
        }
        else if(changed_element == "field_multiple_times_week"){
            field_multiple_times_week = changed_value;
        }
        else if(changed_element == "reset_coupon"){
            coupon_code = "";
        }
        else if(changed_element == "packages"){
            packages = changed_value;
            isSubscription =  true;
        }

        booking_date = booking_date != "" ? booking_date.split('-').reverse().join('-') : "";

        var numberOfHours = hours_required.value ==="dummy" ? 5 : parseInt(hours_required.value, 10);

        var numberOfCleaners = parseInt(number_of_cleaners.value, 10);

        var planId = 0;

        if(isSubscription){
            planId = packages.id;
            total_price = packages.rate;
            if (field_own_equipments.value == "Yes") {
                total_price += packages.data_cleaning_materials;
            }
            if(typeof packages.data_weekdays != "undefined") {
                field_multiple_times_week = packages.weekdays;
            }
            //console.log(packages);
        }

        if (typeof bookingPricingPlan != "undefined" && typeof bookingPricingPlan.planChargesDtoList != "undefined") {

            var planChargesDtoList = Object.values(bookingPricingPlan.planChargesDtoList);

            var hoursPlan = planChargesDtoList.find((element) => {
                return element.value === numberOfHours;
            })

            var planId = bookingPricingPlan.id;

            if (typeof hoursPlan.rate != "undefined") {
                var hours_rate = parseInt(hoursPlan.rate)
                hourlyRate += hours_rate
            }

            if (field_own_equipments.value == "Yes") {
                hourlyRate += bookingPricingPlan.planAdditionalChargesAsMap.CLEANING_MATERIAL;
            }
            var friday_charge = 0;

            if(booking_date != ""){
                var selected_booking_date = new Date(booking_date);
                if(selected_booking_date.getDay() == "5"){
                    friday_charge = (numberOfCleaners * numberOfHours) * bookingPricingPlan.planAdditionalChargesAsMap.FRIDAY_SURCHARGE;
                }
            }

            total_price = (numberOfCleaners * numberOfHours * hourlyRate ) + friday_charge;
        }

        var taxRate = 0 ;

        var prefferedDate = this.state.booking_date;

        var taxChargesTotal = TaxCalculator.calculateVAT(total_price,prefferedDate);

        var total = total_price + taxChargesTotal;

        coupon_code = coupon_code.trim();

        if(coupon_code != ""){
            this.handleCouponChange(coupon_code, changed_element  , changed_value , total_price);
        }

        this.setState({
            coupon_code: coupon_code,
            price: total_price,
            total: total,
            subtotal : total_price,
            vat:taxChargesTotal,
            hourlyRate: hourlyRate,
            planId: planId,
            field_multiple_times_week: field_multiple_times_week
        });
        return {
            hourlyRate: hourlyRate,
            total: total,
            subtotal : total_price,
            vat:taxChargesTotal
        }
    }
    handleCouponChange(value, changed_element = null , changed_value = null, new_price = 0){
        var coupon_value = value.trim();
        this.setState({
            discountData : "load"
        })

        if (typeof coupon_value == 'undefined' || coupon_value.length == 0) {
            var response = BookingCoupons.error("Please provide coupon.");
            this.setState({
                discountData: response.data
            });
            this.calculatePrice("reset_coupon",true);
        }else if (coupon_value.trim().length <= 3) { // minimum length of a promo code is more than 3 characters, so need to go to server if less than that
            var response = BookingCoupons.error("Promotion code is invalid");
            this.setState({
                discountData: response.data
            })
            this.calculatePrice("reset_coupon",true);
        }
        else {

            let {hours_required,field_own_equipments,number_of_cleaners,field_service_needed, booking_date,price,payment, field_multiple_times_week} = this.state;

            if(new_price != 0 ){
                price = new_price;
            }

            if(changed_element == "hours_required"){
                hours_required = changed_value;
            }else if(changed_element == "field_service_needed"){
                field_service_needed = changed_value;
            }
            else if(changed_element == "field_own_equipments"){
                field_own_equipments = changed_value;
            }
            else if(changed_element == "number_of_cleaners"){
                number_of_cleaners = changed_value;
            }
            else if(changed_element == "booking_date"){
                booking_date = changed_value;
            }else if(changed_element == "field_multiple_times_week"){
                field_multiple_times_week = changed_value;
                //dateChanged
            }

            //console.log(price, hours_required);

            var week_days = new Array();

            if(payment == ""){
                payment = "BOOKING_PAYMENT_METHOD_EMPTY"
            }

            var serviceId = allServiceConstant.SERVICE_HOME_CLEANING;

            var serviceLocationId = 1; // todo: dynamic value

            var isRecurring = field_service_needed.value != data_values.DATA_VALUE_ONCE ? true : false;

            var validDate = "";

            var customerId = !this.props.signInDetails.customer ? 0 : parseInt(this.props.signInDetails.customer.id);

            if(booking_date != ""){
                booking_date = booking_date.split('-').reverse().join('-');
                validDate = booking_date;
                booking_date = new Date(booking_date);
                var week_day = ""+booking_date.getDay();
                week_days = [week_day];
            }
            if(field_service_needed.value == data_values.DATA_VALUE_MULTIPLE_TIMES_WEEK && field_multiple_times_week.length !=0 ){
                field_multiple_times_week.map( (item, index) =>{
                    week_days.push(item.value);
                })
            }

            var hours_of_job =  parseInt(hours_required.value);

            var rules = {
                HOURS_OF_JOB: hours_of_job,
                PAYMENT_METHOD:payment,
                DAYS_OF_WEEK: week_days,
            };

            /*this.setState({
                coupon_code:value
            });*/

            var this_var = this;

            var data = BookingCoupons.generateRules(value, rules, isRecurring, price, serviceId, serviceLocationId, validDate, customerId);

            if(changed_element == "return_rules"){
                return data;
            }



            BookingCoupons.validate(value, data).then((response)=> {
                var response = response.data;
                if(response.error){
                    console.log("coupon error");
                }else{
                    var discountData = response;

                    var currentTotal = discountData.actualPrice;
                    var promoCodeDiscountText = "";
                    var totalAfterDiscount = 0;

                    if(discountData.discountType == 'PERCENT_OFF') {
                        promoCodeDiscountText = discountData.discount + '% OFF';
                        totalAfterDiscount = (currentTotal * ((100 - discountData.discount)/100));
                    }
                    else if (discountData.discountType == 'AMOUNT_OFF') {
                        promoCodeDiscountText = current_currency+" "+discountData.discount+' OFF';
                        totalAfterDiscount = currentTotal - discountData.discount;
                    }

                    var prefferedDate = validDate;

                    var taxChargesTotal = TaxCalculator.calculateVAT(totalAfterDiscount,prefferedDate);

                    var total = totalAfterDiscount + taxChargesTotal;

                    total = Number.parseFloat(total).toFixed(2);

                    this.setState({
                        price: currentTotal,
                        total: total,
                        subtotal : currentTotal,
                        vat:taxChargesTotal
                    });
                    response["promoCodeDiscountText"] = promoCodeDiscountText;
                    //response["promoCodeDiscountText"] = promoCodeDiscountText;

                }
                this.setState({
                    discountData: response
                })
            });
        }
    }
    sumarryItems(){
        const {field_service_needed, number_of_cleaners, hours_required, field_own_equipments, booking_date, booking_time, payment, subtotal,vat,hourlyRate, discountData, field_multiple_times_week,isSubscription,packages} = this.state;

        var items = [
            {label: 'Frequency', value: field_service_needed.label},
            {label: 'Cleaners', value: number_of_cleaners.label},
            {label: 'Duration', value: hours_required.label},
            {label: 'Materials', value: field_own_equipments.label},
            {label: field_service_needed.value == '240' ? 'Weekdays' : 'Date', value: field_service_needed.value == '240' ? field_multiple_times_week.map(item=> item.label).join(", ") : booking_date},
            {label: 'Time', value: booking_time.label}
        ];

        if(isSubscription){
            var rate = 0;
            //console.log(packages);
            if( typeof packages.rate != "undefined"){
                rate = packages.rate;
            }
            items.push({label: 'Monthly Rate', value: current_currency+" "+rate});
            if (field_own_equipments.value == "Yes") {
                items.push({label: 'Cleaning materials', value: current_currency+" "+packages.data_cleaning_materials+" / Month"});
            }
        }else{
            items.push({label: 'Hourly rate per cleaner', value:  current_currency+" "+hourlyRate});
        }

        if(typeof discountData.promoCodeDiscountText != "undefined"){
            var index_to_insert = items.length - 3;
            items.push({label: 'Promo code', value: discountData.promoCodeDiscountText});
            items.push({label: 'Subtotal', value: current_currency+" "+subtotal, text_strike:"yes"});
        }else{
            items.push({label: 'Subtotal', value: current_currency+" "+subtotal});
        }
        items.push(
            {label: 'VAT', value: current_currency+" "+vat},
            {label: 'Payment', value: payment}
        );

        return items;
        //subtotal
    }
    hashChangeHandler(){
        let hashVal = window.location.hash;
        let step;
        hashVal.length ? step=parseInt( window.location.hash.replace('#','') ) : step = 1;
        this.props.moveNext(step);
        scrollToTop();
    }
    moveNext(step){
        const {formCurrentStep, signInDetails, showLoginModal} = this.props;
        var valid = true;
        // Step 1 validation
        if( formCurrentStep == '1' ){
            valid = isValidSection('section-request-form');
            if( valid ){
                window.location.hash = step;
                scrollToTop();
                // Show Login Modal
                if( !signInDetails.customer ){
                    showLoginModal(true);
                }
            }
        }
        // Step 2
        if( formCurrentStep == '2' ){
            if( !this.state.loginRequired ){
                valid = isValidSection('personal-information-form');
                if( valid ){
                    if( !signInDetails.customer ) {
                        this.setState({
                            loader:true
                        });
                        this.props.signUpThenSignIn(this.state.input_email, this.state.input_name, this.state.input_last_name, '').then(()=> {
                            window.location.hash = 3;
                            scrollToTop();
                            this.setState({
                                loader: false
                            });
                        });
                    }
                    else{
                        window.location.hash = 3;
                        scrollToTop();
                    }
                }
            }else{
                showLoginModal(false);
            }
        }

        // Step 2
        if( formCurrentStep == '3' ){
            this.setState({
                paymentLoader: true
            });
            this.paymentHandler();
        }
    }
    handleShowMoreOptionChange(){
        this.setState({
            show_more_option: !this.state.show_more_option
        });
    }
    handleCustomChange(name, value){
        this.setState({
            booking_time: value
        });
    }
    toggleServiceDateChangeDialog(boolVal){
        this.setState({showDialogToChangeServiceDate:boolVal});
    }
    handleInputChange(name, value){
    	//console.log(name, value);
        if(name == "hours_required" && value.value == "dummy"){
            value = {id: 3, value: 5, label: "5 hours"};
        }
        if(name == "input_address_city"){
            var calender_date_of_service = this.state.booking_date;
            calender_date_of_service = calender_date_of_service != "" ? calender_date_of_service.split('-').reverse().join('-') : "";
            var prefferedDate = calender_date_of_service == "" ? new Date() : new Date(calender_date_of_service);
            const toggleServiceDateChangeDialog = prefferedDate.getDay()==5 && value.value !='dubai';
            this.setState({
                [name]: value,
                input_address_area: '',
                showDialogToChangeServiceDate : toggleServiceDateChangeDialog 
            });
            document.getElementsByName("input_address_area")[0].value = "";
        }else if(name == "packages"){
            //console.log(value);
            var data_no_of_cleaners = value.data_no_of_cleaners;
            var data_no_of_hours = value.data_no_of_hours;
            this.setState({
                [name]: value,
                hours_required:data_no_of_hours
            });
            //hours_required
            /*this.setState({
             [name]: value,
             });*/
        }else{
            this.setState({
                [name]: value,
            });
        }

        var calc_elements = ['hours_required','field_service_needed','number_of_cleaners','field_own_equipments','booking_date', 'packages'];

        if(calc_elements.includes(name)){
            var prices = this.calculatePrice(name, value);
        }
    }
    handleDropDownChange(name, value){
        console.log(name, value);
    }
    hoursRequiredOptions(return_more_hours = false){
        var hours_required = [];
        var more_hours = [];
        var bookingPricingPlan = typeof bookingPricingPlan == "undefined" ? this.props.pricePlan : bookingPricingPlan;
        
        if( typeof bookingPricingPlan != "undefined" && typeof bookingPricingPlan.planChargesDtoList != "undefined" ) {
            var planChargesDtoList = bookingPricingPlan.planChargesDtoList;
            var hour = 0 ;

            planChargesDtoList.map((item, index) => {

                hour = item.value;
                if(hour <= 8 ) {
                    if (hour <= 4) {
                        hours_required.push({
                            id: index,
                            value: hour,
                            label: hour + " hours",
                            price: item.rate,
                            current_currency: current_currency
                        })
                        if (hour == 4) {
                            hours_required.push({
                                id: 5,
                                value: 'dummy',
                                label: "5+ hours",
                                price: "35",
                                current_currency: current_currency
                            })
                        }
                    } else {
                        more_hours.push({
                            id: index,
                            value: hour,
                            label: hour + " hours"
                        })
                    }
                }
            })
        }
        if(return_more_hours){
            return more_hours;
        }
        return hours_required;
    }
    netflixCleaningPackages(packageType = "weekly", noOfCleaners = 1 , noOfHours = null){
        var selectedPackage = this.state.packages;
        var netflixPackages = [];
        var packages = bookingPricingPlan.filter(
            (item) => ( item.planChargesDtoList[0].constantJson.PACKAGE_TYPE == packageType && item.planChargesDtoList[0].constantJson.NO_OF_CLEANERS == noOfCleaners ) && ( (noOfHours != null && noOfHours == item.planChargesDtoList[0].constantJson.HOURS) || noOfHours == null )
        );
        var newPackage = [];
        if(packages.length){
            packages.sort(compare);
            var planChargesDtoList = "";
            var features = {};
            var singlePackage = [];
            var noOfCleaners = 0,
                noOfHours = 0,
                packageName = "",
                weekdays_nos = [],
                weekdays = [];
            packages.map((item,i) => {
                features = {};
                singlePackage = [];
                planChargesDtoList = item.planChargesDtoList[0];
                noOfCleaners = planChargesDtoList.constantJson.NO_OF_CLEANERS;
                noOfHours =  planChargesDtoList.constantJson.HOURS;
                singlePackage["id"] = item.id;
                packageName = noOfHours+" Hrs Every Week";
                if(packageType == "daily"){
                    packageName = noOfHours+" Hrs " + (planChargesDtoList.constantJson.WEEKDAYS).length+" days a week";
                }
                singlePackage["name"] = packageName;
                if(packageType == "weekly") {
                    features[noOfCleaners + " Cleaner"] = "Yes";
                    features[noOfHours + " Hours every week"] = "Yes";
                    features["Monthly flat fee for a weekly cleaning service"] = "Yes";
                }
                if(packageType == "daily") {
                    features["Same cleaner everyday"] = "Yes";
                    features["Pay monthly, enjoy daily"] = "Yes";
                    var hourlyRate = (planChargesDtoList.rate /
                    ((planChargesDtoList.constantJson.HOURS * (planChargesDtoList.constantJson.WEEKDAYS).length * 4.33))).toFixed(2);
                    features["Hourly rate of "+current_currency+" "+ hourlyRate + " per hour"] = "Yes";
                    weekdays_nos = planChargesDtoList.constantJson.WEEKDAYS;
                    singlePackage["data_weekdays"] = weekdays_nos;
                    weekdays = field_multiple_times_week_options.filter( (item) => ( weekdays_nos.includes(item.value) ));
                    //console.log(weekdays);
                    singlePackage["weekdays"] = weekdays;
                    singlePackage["data_first_weekday"] = planChargesDtoList.constantJson.WEEKDAYS[0];
                }
                singlePackage["features"] = features;
                singlePackage["rate"] = planChargesDtoList.rate;
                singlePackage["per_month"] = "Yes";
                singlePackage["data_no_of_hours"] = {
                    id: noOfHours,
                    value: noOfHours,
                    label: noOfHours + " hours"
                };
                singlePackage["data_no_of_cleaners"] = {id: noOfCleaners, value: noOfCleaners, label: noOfCleaners+" Cleaner"};
                /*singlePackage["data_weekdays"] = planChargesDtoList.constantJson.WEEKDAYS.join(",")
                singlePackage["data_first_weekday"] = planChargesDtoList.constantJson.WEEKDAYS[0];*/
                singlePackage["data_cleaning_materials"] = item.planAdditionalChargesAsMap.CLEANING_MATERIAL;
                singlePackage["data_frequency"] = {id: "3", value: data_values.DATA_VALUE_MULTIPLE_TIMES_WEEK, label: "Several times a week",desc:"Same cleaner every time"};

                //data_no_of_hours data_no_of_cleaners , data_weekdays, data_first_weekday, data_cleaning_materials , data_frequency
                newPackage.push(singlePackage);
               // netflixPackages.push(this.package(item));
            })
        }

        return newPackage;
        //<PackageBox />
        //return netflixPackages;
    }
    CleaningRequest(){

        var {isSubscription, typeOfSubscription} = this.state;
        var selectedPackage = this.state.packages;
        var cleaning_frequency = [
            {id: "1", value: data_values.DATA_VALUE_EVERY_WEEK, label: "Every week",desc:"Same cleaner every time"},
            {id: "2", value: data_values.DATA_VALUE_EVERY_TWO_WEEKS, label: "Every 2 weeks",desc:"Same cleaner every time"},
            {id: "3", value: data_values.DATA_VALUE_MULTIPLE_TIMES_WEEK, label: "Several times a week",desc:"Same cleaner every time"},
            {id: "4", value: data_values.DATA_VALUE_ONCE, label: "One time only"}
        ];

        var number_of_cleaners = [
            {id: 1, value: "1", label: "1 Cleaner"},
            {id: 2, value: "2", label: "2 Cleaners"},
            {id: 3, value: "3", label: "3 Cleaners"},
            {id: 4, value: "4", label: "4 Cleaners"}
        ];

        if( isSubscription && typeOfSubscription == "weekly" ){
            number_of_cleaners = [
                {id: 1, value: "1", label: "1 Cleaner"},
                {id: 2, value: "2", label: "2 Cleaners"}
            ];
        }

        var hours_required = [];

        var more_hours_required = [];

        if((isSubscription && typeOfSubscription == "daily" )){
            hours_required = [
                {id: 4, value: "4", label: "4 Hours"},
                {id: 6, value: "6", label: "6 Hours"},
                {id: 8, value: "8", label: "8 Hours"}
            ];
        }else{
            hours_required = this.hoursRequiredOptions();

            more_hours_required = this.hoursRequiredOptions(true);
        }

        var field_own_equipments = [
            {id: 'yes', value: "Yes", label: "Yes"},
            {id: 'no', value: "No", label: "No"}
        ];

        var other_services = [
            {id: 'ironing', value: "ironing", label: "Ironing"},
            {id: 'inside-window-cleaning', value: "inside-window-cleaning", label: "Interior window cleaning "},
        ];

        var show_more_option = [
            {id: 'show-more', value: "no", label: "Show more options"}
        ];

        var isShowMore = this.state.show_more_option;

        var service_needed_value = this.state.field_service_needed;

        var hours_required_value = this.state.hours_required;

        var number_of_cleaners_value = this.state.number_of_cleaners;


        var field_own_equipments_value = this.state.field_own_equipments;

        var other_services_values = this.state.other_services;

        //other_services
        var currentStepClass = this.currentStep() === 1 ? '' : "d-none";

        var show_subscription = false;

        //console.log(number_of_cleaners_value.value, hours_required_value.value, service_needed_value.value, current_city)

        if( number_of_cleaners_value.value <= 2 && hours_required_value.value <= 4 && service_needed_value.value == data_values.DATA_VALUE_EVERY_WEEK && current_city != 'qatar-doha'){
            show_subscription = true;
        }


        var field_multiple_times_week_values = this.state.field_multiple_times_week;

        var booking_date = this.state.booking_date;

        var time_picker = this.timePicker(booking_date);

        var selected_days = [];

        var show_error = "";

        if( service_needed_value.value == data_values.DATA_VALUE_MULTIPLE_TIMES_WEEK ){
            selected_days = field_multiple_times_week_values;
            if(selected_days.length == 0){
                show_error = "Please select days";
            }
        }
        var netflixCleaningPackages = [];
        if(isSubscription){
           var noOfHours =  typeOfSubscription == "daily" ? hours_required_value.value : null;
            netflixCleaningPackages = this.netflixCleaningPackages(typeOfSubscription, number_of_cleaners_value.value, noOfHours);
            //console.log(netflixCleaningPackages);
        }

        var date_picker = this.datePicker(show_error );
        //{show_error!="" &&  <p className="error">{show_error}</p>}
        //console.log( isSubscription && typeOfSubscription == "weekly" );

        return (
            <div id="section-request-form" className={currentStepClass}>
                <section className={ isSubscription ? "d-none" : ""}>
                    <FormFieldsTitle title="How often do you need cleaning?" />
                    <CheckRadioBoxInput InputType="radio" name="field_service_needed" inputValue={service_needed_value} items={cleaning_frequency} onInputChange={this.handleInputChange} parentClass="row mb-4"  childClass="col-6 col-sm-3 mb-3 d-flex" />
                </section>
                {
                    service_needed_value.value == data_values.DATA_VALUE_MULTIPLE_TIMES_WEEK && (
                        <section className={ (isSubscription && typeOfSubscription == "daily" )  ? "d-none" : ""}>
                            <FormFieldsTitle title="On which weekdays would you like cleaning?" />
                            <CheckRadioBoxInput InputType="checkbox" inputValue={field_multiple_times_week_values} onInputChange={this.handleInputChange} name="field_multiple_times_week" items={field_multiple_times_week_options} parentClass="row" childClass="col-6 col-sm-4 mb-3 d-flex" validationClasses="min-check-2" />

                            <div className="netflix-cleaning-message row mb-3">
                                <div className="col">
                                   Save money when you sign up for a daily subscription. <a href="book-online/daily-subscription">Click Here</a>
                                </div>
                            </div>
                        </section>)
                }

                <section className={ (isSubscription && typeOfSubscription != "weekly" )  ? "d-none" : ""}>
                    <FormFieldsTitle title="How many cleaners do you need?" />
                    <CheckRadioBoxInput InputType="radio" name="number_of_cleaners" inputValue={number_of_cleaners_value} items={number_of_cleaners} onInputChange={this.handleInputChange} childClass="col-6 col-sm-3 mb-3 d-flex" />
                </section>
                <section className={ (isSubscription && typeOfSubscription != "daily" ) ? "d-none" : "mb-4"}>
                    <FormFieldsTitle title={ typeOfSubscription == "daily" ? "How many hours do you need the cleaner for?" : "How long should they stay?" } />
                    <CheckRadioBoxInputWithPrice InputType="radio" name="hours_required" inputValue={hours_required_value} items={hours_required} onInputChange={this.handleInputChange} parentClass="row" childClass="col-6 col-sm-3 mb-3 d-flex" />
                    {( hours_required_value.value == "dummy" || hours_required_value.value >=5 ) && <CheckRadioBoxInput InputType="radio" name="hours_required" inputValue={hours_required_value} items={more_hours_required} onInputChange={this.handleInputChange} childClass="col-6 col-sm-3 mb-3 d-flex" /> }
                    {
                        (!isSubscription && show_subscription ) && (<div className="netflix-cleaning-message row">
                            <div className="col">
                                Save money when you sign up for a monthly subscription. <a href="book-online/subscription">Click Here</a>
                            </div>
                        </div>)
                    }
                </section>
                { netflixCleaningPackages.length != 0 && (<section className="netflix-cleaning-package mb-4"><FormFieldsTitle title="Select a subscription package" /><PackageBox items={netflixCleaningPackages} name="packages" inputValue={selectedPackage} onInputChange={this.handleInputChange}  /></section>) }
                <section className={ ( isSubscription && typeOfSubscription == "daily" ) ? "d-none" : ""}>
                    <FormFieldsTitle title="Do you need cleaning materials? " />
                    <CheckRadioBoxInput InputType="radio" name="field_own_equipments" inputValue={field_own_equipments_value} items={field_own_equipments} onInputChange={this.handleInputChange} childClass="col-6 col-sm-3 mb-3 d-flex" />
                </section>
                <section>
                    <FormFieldsTitle title="Pick your start date and time" />
                    <div className="row mb-4">
                        {date_picker}
                        {time_picker}
                    </div>
                </section>
                <section>
                    <FormFieldsTitle title="Any special requirements or comments? (Optional)" />
                    <div className="row mb-4">
                        <div className="col-6 col-sm-4 mb-3 d-flex">
                            <label className="border rounded btn-block pointer">
                                <input name="show_more_option" className="d-none" value={this.state.show_more_option} onChange={(e) => this.handleShowMoreOptionChange()} type="checkbox"  />
                                <span className="btn-radio">Show more options</span>
                            </label>
                        </div>
                    </div>
                </section>
                <section className={!isShowMore && "d-none"} >
                    <FormFieldsTitle title="Do you need ironing or window cleaning?" />
                    <CheckRadioBoxInput InputType="checkbox" inputValue={other_services_values} onInputChange={this.handleInputChange} name="other_services" items={other_services}  childClass="col-6 col-sm-4 mb-3 d-flex" />
                    <FormFieldsTitle title="Any special instructions" />
                    <div className="row mb-4">
                        <div className="col mb-3">
                            <TextareaInput name="details" placeholder="Example: The keys are under the door mat, and the money is on the kitchen counter."  />
                        </div>
                    </div>
                </section>
                <section>
                    <BookNextStep title="Contact Details" moveNext={this.moveNext} toStep="2" setModal={this.mobileSummary} />
                </section>
            </div>
        )
    }
    timePicker(booking_date){
        //console.log("timePicker"+booking_date)
        return (<CleaningTimePicker name="booking_time" booking_date={booking_date} onInputChange={ this.handleCustomChange } validationClasses="required" />);
    }
    datePicker(show_error){
        var {hours_required,field_service_needed,number_of_cleaners, field_multiple_times_week, isSubscription} = this.state;

        return (<CleaningDatePick
            name="booking_date"
            hours_required={hours_required}
            number_of_cleaners={number_of_cleaners}
            service_needed={field_service_needed}
            selected_days={field_multiple_times_week}
            show_error={show_error}
            data_values={data_values}
            onInputChange={this.handleInputChange}
            isSubscription={isSubscription}
            validationClasses="required"/>);
    }
    ContactDetails(isLocationDetailShow){

        var currentStepClass = this.currentStep() === 2 ? '' : "d-none";
        //var currentStepClass =  ''
        var isLocationDetailShow = "yes";

        var contact_details = {
            input_email:this.state.input_email,
            input_phone:this.state.input_phone,
            input_name:this.state.input_name,
            input_last_name:this.state.input_last_name
        }

        if(isLocationDetailShow){
            contact_details.input_address_city = this.state.input_address_city;
            contact_details.input_address_area = this.state.input_address_area;
            contact_details.input_address_area_building_name = this.state.input_address_area_building_name;
            contact_details.input_address_area_building_apartment = this.state.input_address_area_building_apartment;
        }

        var autocompleteShow = true;
        var cityOptions = this.props.cityOptions;
        var areasOptions = getAreaOptions(cityOptions, this.state.input_address_city);

        var userProfile =  this.props.userProfile;

        if(!this.state.loader){

            return (
                <div id="personal-information-form" className={currentStepClass}>
                    <ContactDetailsStep
                        cityOptions={cityOptions}
                        areasOptions={areasOptions}
                        userProfile = {userProfile}
                        signInDetails={this.props.signInDetails}
                        autocompleteShow={autocompleteShow}
                        isLocationDetailShow={isLocationDetailShow}
                        contact_details={contact_details}
                        handleDropDownChange={this.handleInputChange}
                        handleInputChange={this.handleInputChange}
                        mobileSummary={this.mobileSummary}
                        moveNext={this.moveNext}
                        isBooking={true}
                    />
                </div>
            )
        } else {
            return (
                <main loader={this.state.loader ? "show" : "hide"}><Loader/></main>
            );
        }
    }
    getUserProfile(token) {
        this.props.fetchUserProfile(token).then(res => {

        });
    }
    updateUserProfileAndGoToPayment(withProfile){
        console.log('zzzzzzzzzzzzzzz');
        console.log(this.state);
        var url = window.location.protocol + '//' + window.location.host + window.location.pathname;
        if (url.substr(-1) != '/') url += '/';
        putUpdateCustomerProfile(this.props.signInDetails.access_token,
            this.props.signInDetails.customer.id,
            this.state.input_email,
            this.state.input_name,
            this.state.input_last_name, {
                "apartment": this.state.input_address_area_building_apartment,
                "area": 1,
                "building": this.state.input_address_area_building_name,
                "cityId": 1,
                "phoneNumber": this.state.input_phone
            }).then(() => {
            if(withProfile){
                this.getUserProfile(this.props.signInDetails.access_token);
            }
            var booking_date = this.state.booking_date;

            booking_date = booking_date.split('-').reverse().join('-');

            var numberOfWeekDays = [];
            if(this.state.field_service_needed.value == data_values.DATA_VALUE_MULTIPLE_TIMES_WEEK){
                var field_multiple_times_week = this.state.field_multiple_times_week;
                field_multiple_times_week.map((item) => {
                    numberOfWeekDays.push(item.value);
                });

            }else{
                var booking_day = parseInt(moment(booking_date).format('d'));
                numberOfWeekDays.push(booking_day);
            }
            var service_id = parseInt(allServiceConstant.SERVICE_HOME_CLEANING);
            var serviceIds =  [service_id];
            var other_services = this.state.other_services;
            /*
             {id: 'ironing', value: "ironing", label: "Ironing"},
             {id: 'inside-window-cleaning', value: "inside-window-cleaning", label: "Interior window cleaning "},
             */
            var extraServices = [];
            /*todo: API is not returning service like Ironing and Intetior window cleaning, Once API fix done we can un comment following line
                We should call city state update function once the user logged in.
             */

            /*if(other_services.length){
                other_services.map((item, index) => {
                    if(item.value == "ironing"){
                        serviceIds.push(parseInt(allServiceConstant.SERVICE_IRONING))
                    }
                 if(item.value == "inside-window-cleaning"){
                     serviceIds.push(parseInt(allServiceConstant.SERVICE_INTERIOR_WINDOW_CLEANING))
                 }
                })
            }*/

            var createCustomerProfile = false; //

            var city_object = this.state.input_address_city;

            var area_object = this.state.input_address_area;

            var serviceLocationIdsToAdd = parseInt(city_object.id);

            var couponValidationModel = {};

            var discountData = this.state.discountData;



            var data = {
                "householdRequestModel": {
                    "isBooking": 1,
                    "serviceLocationIdsToAdd": [
                        serviceLocationIdsToAdd
                    ],
                    "planId": this.state.planId,
                    "bookingDetails": {
                        "bookingPaymentDetail": {
                            "paymentMethod": this.state.payment === 'credit' ? "CREDIT_CARD" : 'CASH_ON_DELIVERY',
                            "creditCardId": (this.props.myCreditCardsData.data && this.props.myCreditCardsData.data.length) ? this.props.myCreditCardsData.data[0].id : 0,
                            "isPaymentMethodVerified": (this.props.myCreditCardsData.data && this.props.myCreditCardsData.data.length > 0) || (this.state.payment !== 'credit')
                        },
                        "partTimeCleaningBookingModel": {
                            "bookingStartDate": booking_date,
                            "bookingTime": this.state.booking_time.value,
                            "hoursRequired": this.state.hours_required.value,
                            "numberOfWorkers": this.state.number_of_cleaners.value,
                            "requestFrequency": this.state.field_service_needed.value,
                            "equipmentNeeded": this.state.field_own_equipments.value,
                            "numberOfWeekDays": numberOfWeekDays,
                        },
                        "createCustomerProfile": createCustomerProfile
                    },
                    "contactInformationModel": {
                        "personName": this.state.input_name + " " + this.state.input_last_name,
                        "personPhone": this.state.input_phone,
                        "personEmail": this.state.input_email
                    },
                    "requestFrequency": this.state.field_service_needed.value,
                    "numberOfWeekDays": numberOfWeekDays,
                    "address": {
                        "addressLine1": "",
                        "addressLine2": "",
                        "city": city_object.id,
                        "area": this.state.input_address_area.id,
                        "building": this.state.input_address_area_building_name,
                        "apartment": this.state.input_address_area_building_apartment
                    },
                    "hoursRequired": this.state.hours_required.value,
                    "numberOfWorkers": this.state.number_of_cleaners.value,
                    "equipmentNeeded": this.state.field_own_equipments.value,
                    "serviceIds": serviceIds,
                    "description": "",
                    "serviceRequestTime": (new Date()),
                    "customerId" : this.props.signInDetails.customer.id,
                }
            };

            if (discountData.success) {
                //":{"couponCode":"SUMMER2018","couponRules":{"HOURS_OF_JOB":"4","PAYMENT_METHOD":"CASH_ON_DELIVERY"}

                couponValidationModel = this.handleCouponChange(this.state.coupon_code, 'return_rules');

                data["householdRequestModel"]["couponValidationModel"] = couponValidationModel;

                data["householdRequestModel"]["isCouponAttached"] = true;
            }

            //return false;
            var confirmationSummary = {
                service: 'Home Cleaning',
                date: data.householdRequestModel.bookingDetails.partTimeCleaningBookingModel.bookingStartDate,
                time: data.householdRequestModel.bookingDetails.partTimeCleaningBookingModel.bookingTime,
                name: data.householdRequestModel.contactInformationModel.personName,
                email: data.householdRequestModel.contactInformationModel.personEmail,
                subtotal: this.state.subtotal,
                total: this.state.total,
                payment: this.state.payment

            };
            const { cookies } = this.props;
            cookies.set('_confirmation_summary', JSON.stringify(confirmationSummary), {path: '/'});
            postRequest(this.props.signInDetails.access_token || '', JSON.stringify(data)).then((response) => {
                var rows = JSON.parse(response.data.addRequest).rows;
                confirmationSummary.bookingNumber = rows[0].requestUuid;
                confirmationSummary.bookingId = rows[0].bookingId;
                cookies.set('_confirmation_summary', JSON.stringify(confirmationSummary), {path: '/'});
                var bookingId = (rows[0].bookingId);
                if (this.state.payment !== 'cash' && !(this.props.myCreditCardsData.data && this.props.myCreditCardsData.data.length)) {
                    this.props.registerCreditCard(
                        this.props.signInDetails.access_token,
                        this.props.signInDetails.customer.id,
                        url + 'cancel',
                        url + 'decline',
                        url + bookingId +'/success'
                    ).then(() => {
                        window.location = this.props.myCreditCardsData.redirect_url;
                    });
                } else {
                    window.location = url + bookingId + '/success/211221712711287218';
                }
            });
        });
    }
    paymentHandler() {
        if (this.props.signInDetails.access_token) {
            this.updateUserProfileAndGoToPayment(false);
        }
        else{
            this.updateUserProfileAndGoToPayment(false);
        }
    }
    PaymentSection(){
        var payment = this.state.payment;
        var currentStepClass = this.currentStep() === 3 ? '' : "d-none";
        //var currentStepClass ='';
        

        if(!this.state.paymentLoader) {

            return (
                <div id="payment-method-form" className={currentStepClass}>
                    <section className="payment">
                        <PaymentMethods name="payment" inputValue={payment} onInputChange={this.handleInputChange} />
                    </section>
                    <section>
                        <BookNextStep title="Continue Booking" moveNext={this.moveNext} toStep="4" setModal={this.mobileSummary} />
                    </section>
                </div>
            );
        } else {
            return (
                <main loader={this.state.paymentLoader ? "show" : "hide"}><Loader/></main>
            );
        }
    }
    mobileSummary(boolVal){
        this.setState({
            showMobileSummary: boolVal
        })
    }
    getChangeServiceDateModalContent(){
        const selectedCity = this.state.input_address_city;
        const citySlug = selectedCity ? "in " + selectedCity.label : "";
        return (
            
            <div className="modal-body">
             <p className="info-text"><strong>Oops! Unfortunately we cannot book Fridays {citySlug} just yet. Would you like to pick a different day instead?</strong></p>
             <div className="popup city-change-pop-up-buttons text-center mth">
             <button className="btn btn-inline-block btn-warning py-2 text-uppercase font-weight-bold" onClick={()=>{this.toggleServiceDateChangeDialog(false)}}  >
                    <span className="text-uppercase">Change my service date</span>
             </button>

             <a className="text-uppercase " id="change-flow-button-modal" aria-d-none="true" data-dismiss="modal" data-switch="switchToQuote"> </a>
             </div>
            </div>
        );
    }
    render(){
        var items = this.sumarryItems();
        let showPromo = true;
        const showPrice = true;

        //var calculate_price = this.calculatePrice();
        let total = this.state.total;

        var discountData = this.state.discountData;

        var booking_date = this.state.booking_date;

        if(booking_date != ""){
            booking_date = booking_date.split('-').reverse().join('-');
            var selected_booking_date = new Date(booking_date);
            if(selected_booking_date.getDay() == "5"){
                var friday_surage = bookingPricingPlan.planAdditionalChargesAsMap.FRIDAY_SURCHARGE;
                var index_to_insert = items.length - 3;
                items.splice(index_to_insert,0,{label: 'Friday service charge', value: current_currency+" "+friday_surage});
            }
        }
        var isSubscription = this.state.isSubscription;

        if(isSubscription){
            showPromo = false;
        }

        return(
            <React.Fragment>
            <div className="col-lg-8">
                {this.CleaningRequest()}
                {this.ContactDetails()}
                {this.PaymentSection()}               
            </div>
            <div className="col-md-3 ml-auto">
                <BookingSummary items={items} showPromo={showPromo} showPrice={showPrice} total={total} discountData={discountData} handleCouponChange={this.handleCouponChange} showMobileSummary={this.state.showMobileSummary} setModal={this.mobileSummary} />
            </div>
            {this.state.showDialogToChangeServiceDate && <GeneralModal  modalBody={this.getChangeServiceDateModalContent()} setModal={this.toggleServiceDateChangeDialog} />}
            </React.Fragment>
        )
    }
}
function compare(a,b) {
    if (a.planChargesDtoList[0].rate < b.planChargesDtoList[0].rate)
        return -1;
    if (a.planChargesDtoList[0].rate > b.planChargesDtoList[0].rate)
        return 1;
    return 0;
}

function mapStateToProps(state){
    return {
        myCreditCardsData: state.myCreditCardsData
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({registerCreditCard, fetchUserProfile, setSignIn, getAllCreditCard}, dispatch);
}

export default withCookies(connect(mapStateToProps, mapDispatchToProps)(BookCleaningPage));