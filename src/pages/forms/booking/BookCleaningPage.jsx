import React from "react";
import FormFieldsTitle from "../../../components/FormFieldsTitle";
import CheckRadioBoxInput from "../../../components/Form_Fields/CheckRadioBoxInput";
import CheckRadioBoxInputWithPrice from "../../../components/Form_Fields/CheckRadioBoxInputWithPrice";
import CleaningDatePick from "../../../components/Form_Fields/CleaningDatePick";
import CleaningTimePicker from "../../../components/Form_Fields/CleaningTimePicker";
import TextareaInput from "../../../components/Form_Fields/TextareaInput";
import PaymentMethods from "../../../components/Form_Fields/PaymentMethods";
import SubmitBooking from "../../../components/Form_Fields/SubmitBooking";
import NewBookingSummary from "../../../components/Form_Fields/NewBookingSummary";
import BookNextStep from "../../../components/Form_Fields/BookNextStep";
import ContactDetailsStep from "../../../components/ContactDetailsStep";
import { connect } from "react-redux";
import TaxCalculator from "./TaxCalculator"
import locationHelper from "../../../helpers/locationHelper";
import commonHelper from "../../../helpers/commonHelper";
import GeneralModal from "../../../components/GeneralModal";
import Loader from "../../../components/Loader";
import stringConstants from '../../../constants/stringConstants';
import moment from 'moment';
import {
    isValidSection, 
    getAllCreditCard,
    scrollToTop, getAreaOptions,
    CREDITCARD_COUPON,
    zendeskChatBox,
    QATAR_ID,
    DOHA,
    SHARJAH_MAPPED_ID,
    SHARJAH_ID,
    DOHA_CITY_ID,
    DOHA_ID
} from "../../../actions";
import { bindActionCreators } from "redux";
import { withCookies } from "react-cookie";
import PackageBox from "../../../components/Form_Fields/PackageBox";
let allServiceConstant = {};
let URLConstant = {};
let currentCurrency = "";
let current_city = "dubai";
let current_city_id = "1";
let dataValues = {}
let bookingFrequencyDataConstants = {};
let bookingTaxPlan = {};
let bookingPricingPlan = {};
var cleaning_frequency = [];
var field_multiple_times_week_options = [
    { id: 1, value: 7, label: "Sunday" },
    { id: 2, value: 1, label: "Monday" },
    { id: 3, value: 2, label: "Tuesday" },
    { id: 4, value: 3, label: "Wednesday" },
    { id: 5, value: 4, label: "Thursday" },
    { id: 6, value: 6, label: "Saturday" },
];
var status = null;
var bid = null;
var tt = null;
var isCouponApplied = false;

class BookCleaningPage extends React.Component {
    _isMounted = false;

    constructor(props) {
        super(props);

        var bookingData = {
            show_more_option: false,
            booking_date: '',
            booking_time: '',
            field_service_needed: {},
            hours_required: { id: 4, value: "4", label: "4 hours", price: 35, currentCurrency: currentCurrency },
            number_of_cleaners: { id: 1, value: "1", label: "1 Cleaner" },
            field_own_equipments: { id: 'no', value: "No", label: "No" },
            other_services: [],
            field_multiple_times_week: [],
            input_email: props.userProfile.email,
            input_phone: props.userProfile.address ? props.userProfile.address.phoneNumber : '',
            input_name: props.userProfile.customerFirstName,
            input_last_name: props.userProfile.customerLastName,
            input_address_city: '',
            input_address_area: props.setUserArea,
            input_address_area_building_name: props.userProfile.address ? props.userProfile.address.building : '',
            input_address_area_building_apartment: props.userProfile.address ? props.userProfile.address.apartment : '',
            discountData: {},
            discountPrices: {},
            packages: {},
            isSubscription: false,
            typeOfSubscription: "",
            planId: 0,
            couponCode: "",
            hourlyRate: 0,
            subtotal: 0,
            vat: 0,
            totalAfterDiscount: 0,
            total: 0,
            price: 0,
            payment: '',
            bookingPricingPlan: {},
            bookingTaxPlan: {},
            showMobileSummary: false,
            loginRequired: false,
            paymentLoader: false,
            softSafeBookingId: '',
            softSafeRequestId: '',
            isCreditCouponRedeemed: false,
            isSlotNotAvailable: false,
            walletUsed: 0
        };

        var cookieData = props.getCookieFormData();

        if (Object.keys(cookieData).length) { // restore from cookies
            bookingData = cookieData;
            // console.log("cookieData", cookieData);
            bookingData["softSafeBookingId"] = "";
            bookingData["softSafeRequestId"] = "";
            props.removeCookie("booking_data");
            props.removeCookie("submitted_data");
            props.removeCookie("confirmation_summary");
            props.removeCookie("coupon_data");
        }

        this.state = bookingData;

        this.ContactDetails = this.ContactDetails.bind(this);
        this.CleaningRequest = this.CleaningRequest.bind(this);
        this.handleShowMoreOptionChange = this.handleShowMoreOptionChange.bind(this);
        this.handleCustomChange = this.handleCustomChange.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleDropDownChange = this.handleDropDownChange.bind(this);
        this.currentStep = this.currentStep.bind(this);
        this.moveNext = this.moveNext.bind(this);
        this.hashChangeHandler = this.hashChangeHandler.bind(this);
        this.calculatePrice = this.calculatePrice.bind(this);
        //this.fetchBookingPricingPlan = this.fetchBookingPricingPlan.bind(this);
        this.hoursRequiredOptions = this.hoursRequiredOptions.bind(this);
        this.handleCouponChange = this.handleCouponChange.bind(this);
        this.mobileSummary = this.mobileSummary.bind(this);
        this.toggleServiceDateChangeDialog = this.toggleServiceDateChangeDialog.bind(this);
        this.getChangeServiceDateModalContent = this.getChangeServiceDateModalContent.bind(this);
        this.netflixCleaningPackages = this.netflixCleaningPackages.bind(this);
        this.setFridayPopup = this.setFridayPopup.bind(this);
        this.toggleSlotNotAvilable = this.toggleSlotNotAvilable.bind(this);
        this.getChangeServiceTimeModalContent = this.getChangeServiceTimeModalContent.bind(this);

    }

    componentDidMount() {
        this._isMounted = true;
        allServiceConstant = this.props.service_constants;
        URLConstant = this.props.url_constants;
        current_city = this.props.current_city;
        bookingFrequencyDataConstants = this.props.dataConstants["BOOKING_FREQUENCY"];
        dataValues = commonHelper.processDataValues(bookingFrequencyDataConstants);
        bookingTaxPlan = this.props.taxPlan;
        bookingPricingPlan = this.props.pricePlan;

        const url_params = this.props.url_params;

        status = commonHelper.getParameterByName("status");
        bid = commonHelper.getParameterByName("bid");
        tt = commonHelper.getParameterByName("tt");

        currentCurrency = locationHelper.getCurrentCurrency();
        /*const walletAvailable = this.props.userProfile && this.props.userProfile.userWallet && this.props.userProfile.userWallet.currency.code === currentCurrency && this.props.userProfile.userWallet.totalAmount > 0 ? true : false;
        this.setState({ isWalletAvailable: walletAvailable });*/
        cleaning_frequency = [
            { id: "1", value: dataValues.DATA_VALUE_EVERY_WEEK, label: "Every week", desc: "Same cleaner every time", tag: "Most popular" },
            { id: "2", value: dataValues.DATA_VALUE_EVERY_TWO_WEEKS, label: "Every 2 weeks", desc: "Same cleaner every time" },
            { id: "3", value: dataValues.DATA_VALUE_MULTIPLE_TIMES_WEEK, label: "Several times a week", desc: "Same cleaner every time" },
            { id: "4", value: dataValues.DATA_VALUE_ONCE, label: "One time only" }
        ];

        var isSubscription = typeof url_params.subscription != "undefined" ? true : false;

        var typeOfSubscription = "";

        var field_service_needed = cleaning_frequency.filter(item => item.value == dataValues.DATA_VALUE_EVERY_WEEK);;

        var hours_required = { id: 4, value: "4", label: "4 hours", price: 35, currentCurrency: currentCurrency };

        var selectedItem = {}

        if (isSubscription) {
            typeOfSubscription = url_params.subscription == "subscription" ? "weekly" : "daily";
            var noOfHours = typeOfSubscription == "daily" ? hours_required.value : null;
            //console.log(typeOfSubscription);
            var number_of_cleaners_val = 1;
            if (typeOfSubscription == "daily") {
                field_service_needed = cleaning_frequency.filter(item => item.value == dataValues.DATA_VALUE_MULTIPLE_TIMES_WEEK);
            }

            var packages = this.netflixCleaningPackages(typeOfSubscription, number_of_cleaners_val, noOfHours);

            var selectedPackage = {};

            var field_multiple_times_week = [];

            if (packages.length) {
                selectedItem = {
                    id: packages[0].id,
                    name: packages[0].name,
                    rate: packages[0].rate,
                    data_no_of_cleaners: packages[0].data_no_of_cleaners,
                    data_no_of_hours: packages[0].data_no_of_hours,
                    data_cleaning_materials: packages[0].data_cleaning_materials
                };

                if (typeof packages[0].weekdays != "undefined") {
                    field_multiple_times_week = packages[0].weekdays;
                    selectedItem["weekdays"] = field_multiple_times_week
                }

                selectedPackage = selectedItem;

            }
        }
        var customerId = !this.props.signInDetails.customer ? 0 : parseInt(this.props.signInDetails.customer.id);
        if (status == null && customerId != 0) {
            this.props.getAllCreditCard(this.props.signInDetails.access_token)
                .then(() => {
                    if (this._isMounted) {
                        this.setState({
                            loader: false
                        })
                    }
                });
        } else {
            this.setState({
                loader: false
            })
        }

        var city = locationHelper.getLocationByName(current_city);

        if (status == null) {
            var promoCode = commonHelper.getParameterByName("promo") != null ? commonHelper.getParameterByName("promo") : "";

            var frequency = commonHelper.getParameterByName("frequency") != null ? commonHelper.getParameterByName("frequency") : "";

            var selectFrequency = "";

            if (frequency != "") {
                if (frequency == "weekly") {
                    selectFrequency = dataValues.DATA_VALUE_EVERY_WEEK;
                } else if (frequency == "one-time") {
                    selectFrequency = dataValues.DATA_VALUE_ONCE;

                } else if (frequency == "bi-weekly") {
                    selectFrequency = dataValues.DATA_VALUE_EVERY_TWO_WEEKS;
                }
                if (selectFrequency != "") {
                    field_service_needed = cleaning_frequency.filter(item => item.value == selectFrequency);
                }
            }

            if (typeof field_service_needed == "object" && field_service_needed.length) {
                field_service_needed = field_service_needed[0];
            }

            var userArea = "";
            let fetchedCities = locationHelper.fetchCities();
            let cityOptions = []
            fetchedCities.map(function(item){
                cityOptions.push({id: item.id, value: item.value, label: item.name, code: item.code});
            });
            if (commonHelper.isUserLoggedIn() && (this.props.userProfile != false && this.props.userProfile.address != null)) {
                if (this.props.userProfile.customerCityId != "" && this.props.userProfile.address.area != null) {
                    var customerCityId = this.props.userProfile.customerCityId == SHARJAH_MAPPED_ID ? SHARJAH_ID : this.props.userProfile.customerCityId;
                    customerCityId = customerCityId == DOHA_CITY_ID ? DOHA_ID : customerCityId;
                    city = locationHelper.getLocationById(customerCityId);
                    var cityAreas = locationHelper.getAreasByCity(city.id);
                    userArea = locationHelper.getAreaByName(cityAreas, this.props.userProfile.address.area);
                }
            }

            if(Object.keys(city).length){
                let cityFound = cityOptions.filter(item => item.code == city.code);
                let currentCity = this.props.current_city
                if(!cityFound.length){
                    city = locationHelper.getLocationByName( currentCity ) ;
                }
            }

           // console.log("componentDidMount",city, this.props.userProfile, this.state.input_address_city, this.state.input_address_area, userArea);

            this.setState({
                field_service_needed: field_service_needed,
                input_address_city: city,
                isSubscription: isSubscription,
                typeOfSubscription: typeOfSubscription,
                hours_required: hours_required,
                packages: selectedPackage,
                couponCode: promoCode
            });

        }
        var changed_element = null, changed_value = null;
        if (isSubscription) {
            changed_element = "packages";
            changed_value = selectedPackage;
        }
        //this.calculatePrice(changed_element, changed_value);


        zendeskChatBox();

        window.addEventListener("hashchange", this.hashChangeHandler, false);
    }
    componentWillUnmount() {
        this._isMounted = false;
        window.removeEventListener("hashchange", this.hashChangeHandler, false);
    }
    currentStep() {
        return this.props.formCurrentStep;
    }

    calculatePrice(getTotal = false) {

        let total_price = 0;
        let hourlyRate = 0;

        /* Here is the cost calculations based on promotion code used &
         wallet amount deductions.*/
        let {
            hours_required,
            field_own_equipments,
            number_of_cleaners,
            booking_date,
            field_multiple_times_week,
            isSubscription,
            packages,
            discountPrices } = this.state,
            {isWalletAvailable} = this.props,
            changed_element = null, changed_value = null;

        booking_date = booking_date != "" ? booking_date.split('-').reverse().join('-') : "";

        const numberOfHours = hours_required.value === "dummy" ? 5 : parseInt(hours_required.value, 10);

        const numberOfCleaners = parseInt(number_of_cleaners.value, 10);

        var planId = 0;

        if (isSubscription) {
            planId = packages.id;
            total_price = packages.rate;
            if (field_own_equipments.value == "Yes") {
                total_price += packages.data_cleaning_materials;
            }
            if (typeof packages.weekdays != "undefined") {
                field_multiple_times_week = packages.weekdays;
            }
        }

        if (typeof bookingPricingPlan != "undefined" && typeof bookingPricingPlan.planChargesDtoList != "undefined") {

            const planChargesDtoList = Object.values(bookingPricingPlan.planChargesDtoList);

            let hoursPlan = planChargesDtoList.find((element) => {
                return element.value === numberOfHours;
            })

            planId = bookingPricingPlan.id;

            if (typeof hoursPlan.rate != "undefined") {
                var hours_rate = parseInt(hoursPlan.rate)
                hourlyRate += hours_rate
            }

            if (field_own_equipments.value == "Yes") {
                hourlyRate += bookingPricingPlan.planAdditionalChargesAsMap.CLEANING_MATERIAL;
            }
            let friday_charge = 0;

            if (booking_date != "") {
                var selected_booking_date = new Date(booking_date);
                if (selected_booking_date.getDay() == "5") {
                    friday_charge = (numberOfCleaners * numberOfHours) * bookingPricingPlan.planAdditionalChargesAsMap.FRIDAY_SURCHARGE;
                }
            }

            total_price = (numberOfCleaners * numberOfHours * hourlyRate) + friday_charge;


        }
        //let afterWalletAmount = total_price;
        let walletUsed = 0;
        if (isWalletAvailable) {
            const walletAmount = this.props.userProfile.userWallet.totalAmount;
            const walletCurrency = this.props.userProfile.userWallet.currency.code;

            if (walletCurrency === currentCurrency && walletAmount > 0) {
                if (walletAmount >= total_price) {
                    walletUsed = total_price;
                    total_price = 0;
                }
                else if (walletAmount < total_price) {
                    walletUsed = walletAmount;
                    total_price = total_price - walletAmount;
                }

            }

        }


        const prefferedDate = this.state.booking_date;

        const taxChargesTotal = TaxCalculator.calculateVAT(total_price, prefferedDate);

        let total = total_price + taxChargesTotal;

        if (this.props.userProfile && this.props.userProfile.userWallet) {
            const walletAmount = this.props.userProfile.userWallet.totalAmount;
            const walletCurrency = this.props.userProfile.userWallet.currency.code;

            if (walletCurrency === currentCurrency && walletAmount > 0) {

                total = total > 0 ? total : 0.00;
            }
        }

        if (getTotal == false && (typeof discountPrices != "undefined" && typeof discountPrices.price != "undefined")) {
            // console.log("calculatePrice", discountPrices)
            discountPrices["planId"] = planId;
            discountPrices["walletUsed"] = walletUsed;
            return discountPrices;
        }
        
        return {
            price: total_price,
            total: total,
            subtotal: total_price,
            vat: taxChargesTotal,
            hourlyRate: hourlyRate,
            planId: planId,
            field_multiple_times_week: field_multiple_times_week,
            walletUsed: walletUsed
        }
    }
    handleCouponChange(value, newPrice = 0) {
        this.props.handleCouponChange(value, this.state, newPrice);
    }
    sumarryItems() {
        let {
            field_service_needed,
            number_of_cleaners,
            hours_required,
            field_own_equipments,
            booking_date,
            booking_time,
            subtotal, 
            vat, 
            hourlyRate,
            discountData,
            field_multiple_times_week,
            isSubscription, 
            packages,
            typeOfSubscription,
            couponCode,
            walletUsed,
            payment } = this.state;

        let {PricePlan} =  this.props;

        let timeValue = field_service_needed.label;

        if(( field_service_needed.value == dataValues.DATA_VALUE_MULTIPLE_TIMES_WEEK && field_multiple_times_week.length ) && typeOfSubscription != "daily"){
            var field_multiple_times_week_text = field_multiple_times_week.map(item => (item.label).substring(0, 3)).join(", ");
            timeValue += "\n( "+field_multiple_times_week_text+" )\n";
        }

        if(booking_date != "" ){
            let dateOfBooking = booking_date.split('-').reverse().join('-');

            dateOfBooking = typeof booking_time.value != "undefined" ? dateOfBooking+' '+booking_time.value : dateOfBooking;

            dateOfBooking = moment( dateOfBooking );
            
            if(field_service_needed.value != dataValues.DATA_VALUE_MULTIPLE_TIMES_WEEK){
                timeValue += " ("+ dateOfBooking.format('ddd') +") \n";
            }

            if(typeof booking_time.value != "undefined"){
                timeValue += booking_time.label +" to "+ dateOfBooking.add(hours_required.value, 'hours').format('h A')+"\n";
            }
            
            let dateStr = dateOfBooking.format('DD-MM-YYYY');

            timeValue += field_service_needed.value == dataValues.DATA_VALUE_ONCE ? " On "+ dateStr : " Starting "+ dateStr;
        }

        let jobDetails = [];
        
        jobDetails.push(number_of_cleaners.label);
        jobDetails.push(hours_required.label);
        let fridayValue = "";
        if(isSubscription){
            var rate = 0;
            if (typeof packages.rate != "undefined") {
                rate = packages.rate;
            }
            jobDetails.push(<span className="text-warning">{ currentCurrency + " " + rate+" /Month" } { locationHelper.getCurrentCountryId() != QATAR_ID ? " + VAT" : ""}</span>);
        }else{
            let friday_charge = 0;
            if (booking_date != "") {
                let newBookingDate = booking_date != "" ? booking_date.split('-').reverse().join('-') : "";
                var selected_booking_date = new Date(newBookingDate);
                
                if (selected_booking_date.getDay() == "5") {
                    friday_charge =  PricePlan.planAdditionalChargesAsMap.FRIDAY_SURCHARGE;
                    fridayValue =  currentCurrency +" " + PricePlan.planAdditionalChargesAsMap.FRIDAY_SURCHARGE+"/ hour";
                }
                hourlyRate = hourlyRate+friday_charge;
            }
            jobDetails.push(<span className="text-warning">{ currentCurrency + " " + hourlyRate+" /hour" } { locationHelper.getCurrentCountryId() != QATAR_ID ? " + VAT" : ""}</span>);
        }
        
        
      let materialValue = "";

     

       if(field_own_equipments.value == "Yes"){
           materialValue = isSubscription ? 'Yes ( '+currentCurrency +" " + packages.data_cleaning_materials+"/ Month)" : 'Yes ( '+currentCurrency +" " + PricePlan.planAdditionalChargesAsMap.CLEANING_MATERIAL+"/ hour)";
       }

       var items = [
            {label: 'Time', value: timeValue},
            {label: 'Details', value: jobDetails, conClass: 'col-12 mb-4'}
       ];

        const isWalletAvailable = this.props.isWalletAvailable;

        if (isWalletAvailable) {
            const walletAmount = walletUsed; 
            const walletCurrency = this.props.userProfile.userWallet.currency.code;
            if (walletCurrency === currentCurrency && walletAmount > 0) {
                items.push({ label: stringConstants.WALLET, value: `-  ${walletCurrency}  ${walletAmount}` });
            }
        }
        if (typeof discountData.promoCodeDiscountText != "undefined" && couponCode != "") {
            var index_to_insert = items.length - 3;
            items.push({label: 'Promo code', value: discountData.promoCodeDiscountText});
            items.push({label: 'Subtotal', value: currentCurrency+" "+subtotal, text_strike:"yes"});
        }else{
            items.push({label: 'Subtotal', value: currentCurrency+" "+subtotal, classNme:'text-warning'});
        }
        
        items.push({label: 'Materials', value: materialValue, classNme:'text-warning'});
        if(fridayValue != ""){
            items.push({label: 'Friday surcharge ', value: fridayValue, classNme:'text-warning'});
        }
        //{label: 'VAT', value: currentCurrency+" "+vat},
        items.push(
            {label: 'Payment', value: payment}
        );
        return items;
    }
    hashChangeHandler() {
        let hashVal = window.location.hash;
        let step;
        hashVal.length ? step = parseInt(window.location.hash.replace('#', '')) : step = 1;
        this.props.moveNext(step);
        scrollToTop();
    }
    setFridayPopup(value) {
        var value = typeof value != "undefined" ? value : this.state.input_address_city;
        var calender_date_of_service = this.state.booking_date;
        calender_date_of_service = calender_date_of_service != "" ? calender_date_of_service.split('-').reverse().join('-') : "";
        var prefferedDate = calender_date_of_service == "" ? new Date() : new Date(calender_date_of_service);
        const toggleServiceDateChangeDialog = prefferedDate.getDay() == 5 && value.value != 'dubai';
        this.setState({
            showDialogToChangeServiceDate: toggleServiceDateChangeDialog
        });
        return toggleServiceDateChangeDialog;
    }
    moveNext(step) {
        const { moveNextStep, formCurrentStep } = this.props;

        var { booking_date, booking_time } = this.state;

        var bookingTimeVal = document.getElementsByName("booking_time")[0].value;

        if (booking_date != "" && (typeof booking_time == "object" && typeof booking_time.label != "undefined") && bookingTimeVal != "") {
            if (commonHelper.isSlotAvailable(booking_date, booking_time)) {
                this.setState({
                    isSlotNotAvailable: true
                })
                return false;
            }
        }

        if (formCurrentStep == 2) {
            if (this.setFridayPopup()) {
                return false;
            }
        }
        //console.log('plan id at next step',this.state.planId);
        moveNextStep(step, this.state, true);
    }
    applyCreditCardCoupon(value) {
        if (!this.state.isSubscription) {
            var applyCreditCardCoupon = false;

            var couponCode = this.state.couponCode;

            var payment = typeof value != "undefined" ? value : this.state.payment;

            if (this.state.payment == "") {
                this.setState({
                    payment: 'credit',
                });
                applyCreditCardCoupon = true
            } else {
                applyCreditCardCoupon = (payment != "" && payment == "credit") ? true : false;
            }
            if (this.props.isCreditCardCouponApplied && (couponCode != "" && couponCode == CREDITCARD_COUPON)) {
                this.setState({
                    couponCode: ""
                });
            } else {
                if (!this.props.isCreditCardCouponApplied && ((applyCreditCardCoupon && payment == "credit") && (couponCode == "" || couponCode == CREDITCARD_COUPON))) {
                    if (couponCode == "") {
                        this.setState({
                            couponCode: CREDITCARD_COUPON
                        });
                    }
                } else {
                    if (payment == "cash" && couponCode == CREDITCARD_COUPON) {
                        this.setState({
                            couponCode: ""
                        });
                    }
                }
            }
        } else {
            this.setState({
                payment: 'credit'
            });
        }
    }
    handleShowMoreOptionChange() {
        this.setState({
            show_more_option: !this.state.show_more_option
        });
    }
    handleCustomChange(name, value) {
        this.setState({
            booking_time: value
        });
    }
    toggleServiceDateChangeDialog(boolVal) {
        this.setState({ showDialogToChangeServiceDate: boolVal });
    }
    toggleSlotNotAvilable(boolVal) {
        this.setState({ isSlotNotAvailable: boolVal });
    }
    handleInputChange(name, value) {
        //console.log(name, value);
        if (name == "hours_required" && value.value == "dummy") {
            value = { id: 5, value: 5, label: "5 hours" };
        }
        if (name == "payment") {
            this.applyCreditCardCoupon(value);
            this.setState({
                [name]: value,
            });
        }
        if (name == "input_address_city") {
            this.setFridayPopup(value);
            this.setState({
                [name]: value,
                input_address_area: '',
            });
            document.getElementsByName("input_address_area")[0].value = "";
        } else if (name == "packages") {
            var data_no_of_cleaners = value.data_no_of_cleaners;
            var data_no_of_hours = value.data_no_of_hours;

            this.setState({
                [name]: value,
                hours_required: data_no_of_hours
            });
        } else {

            this.setState({
                [name]: value,
            });
            if (name == "couponCode") {
                if (this.state.couponCode !== value)
                    this.handleCouponChange(value);
            }
        }

    }
    handleDropDownChange(name, value) {
        // console.log(name, value);
    }
    hoursRequiredOptions(return_more_hours = false) {
        var hours_required = [];
        var more_hours = [];
        var bookingPricingPlan = typeof bookingPricingPlan == "undefined" ? this.props.pricePlan : bookingPricingPlan;

        if (typeof bookingPricingPlan != "undefined" && typeof bookingPricingPlan.planChargesDtoList != "undefined") {
            var planChargesDtoList = bookingPricingPlan.planChargesDtoList;
            var hour = 0;

            planChargesDtoList.map((item, index) => {

                hour = item.value;
                if (hour <= 8) {
                    if (hour <= 4) {
                        hours_required.push({
                            id: index,
                            value: hour,
                            label: hour + " hours",
                            price: item.rate,
                            currentCurrency: currentCurrency,
                            is_hourly_price: true
                        })
                        if (hour == 4) {
                            hours_required.push({
                                id: 5,
                                value: 'dummy',
                                label: "5+ hours",
                                price: "35",
                                currentCurrency: currentCurrency,
                                is_hourly_price: true
                            })
                        }
                    } else {
                        more_hours.push({
                            id: index,
                            value: hour,
                            label: hour + " hours"
                        })
                    }
                }
            })
        }
        if (return_more_hours) {
            return more_hours;
        }
        return hours_required;
    }
    netflixCleaningPackages(packageType = "weekly", noOfCleaners = 1, noOfHours = null) {
        var selectedPackage = this.state.packages;
        var netflixPackages = [];
        var newPackage = [];

        //console.log("bookingPricingPlan", bookingPricingPlan);

        if (bookingPricingPlan.length) {

            var packages = bookingPricingPlan.filter(
                (item) => (item.planChargesDtoList[0].constantJson.PACKAGE_TYPE == packageType && item.planChargesDtoList[0].constantJson.NO_OF_CLEANERS == noOfCleaners) && ((noOfHours != null && noOfHours == item.planChargesDtoList[0].constantJson.HOURS) || noOfHours == null)
            );


            if (packages.length) {
                packages.sort(compare);
                var planChargesDtoList = "";
                var features = {};
                var singlePackage = [];
                var noOfCleaners = 0,
                    noOfHours = 0,
                    packageName = "",
                    weekdays_nos = [],
                    weekdays = [];
                packages.map((item, i) => {
                    features = {};
                    singlePackage = [];
                    planChargesDtoList = item.planChargesDtoList[0];
                    noOfCleaners = planChargesDtoList.constantJson.NO_OF_CLEANERS;
                    noOfHours = planChargesDtoList.constantJson.HOURS;
                    singlePackage["id"] = item.id;
                    packageName = noOfHours + " Hrs Every Week";
                    if (packageType == "daily") {
                        packageName = noOfHours + " Hrs " + (planChargesDtoList.constantJson.WEEKDAYS).length + " days a week";
                    }
                    singlePackage["name"] = packageName;
                    if (packageType == "weekly") {
                        features[noOfCleaners + " Cleaner"] = "Yes";
                        features[noOfHours + " Hours every week"] = "Yes";
                        features["Monthly flat fee for a weekly cleaning service"] = "Yes";
                    }
                    if (packageType == "daily") {
                        features["Same cleaner everyday"] = "Yes";
                        features["Pay monthly, enjoy daily"] = "Yes";
                        var hourlyRate = (planChargesDtoList.rate /
                            ((planChargesDtoList.constantJson.HOURS * (planChargesDtoList.constantJson.WEEKDAYS).length * 4.33))).toFixed(2);
                        features["Hourly rate of " + currentCurrency + " " + hourlyRate + " per hour"] = "Yes";
                        weekdays_nos = planChargesDtoList.constantJson.WEEKDAYS;
                        singlePackage["data_weekdays"] = weekdays_nos;
                        weekdays = field_multiple_times_week_options.filter((item) => (weekdays_nos.includes(item.value)));
                        //console.log(weekdays);
                        singlePackage["weekdays"] = weekdays;
                        singlePackage["data_first_weekday"] = planChargesDtoList.constantJson.WEEKDAYS[0];
                    }
                    singlePackage["features"] = features;
                    singlePackage["rate"] = planChargesDtoList.rate;
                    singlePackage["per_month"] = "Yes";
                    singlePackage["data_no_of_hours"] = {
                        id: noOfHours,
                        value: noOfHours,
                        label: noOfHours + " hours"
                    };
                    singlePackage["data_no_of_cleaners"] = {
                        id: noOfCleaners,
                        value: noOfCleaners,
                        label: noOfCleaners + " Cleaner"
                    };
                    /*singlePackage["data_weekdays"] = planChargesDtoList.constantJson.WEEKDAYS.join(",")
                     singlePackage["data_first_weekday"] = planChargesDtoList.constantJson.WEEKDAYS[0];*/
                    singlePackage["data_cleaning_materials"] = item.planAdditionalChargesAsMap.CLEANING_MATERIAL;
                    singlePackage["data_frequency"] = {
                        id: "3",
                        value: dataValues.DATA_VALUE_MULTIPLE_TIMES_WEEK,
                        label: "Several times a week",
                        desc: "Same cleaner every time"
                    };

                    //data_no_of_hours data_no_of_cleaners , data_weekdays, data_first_weekday, data_cleaning_materials , data_frequency
                    newPackage.push(singlePackage);
                    // netflixPackages.push(this.package(item));
                })
            }
        }

        return newPackage;
    }
    CleaningRequest() {

        var { isSubscription, typeOfSubscription } = this.state;

        var selectedPackage = this.state.packages;

        var number_of_cleaners = [
            { id: 1, value: "1", label: "1 Cleaner" },
            { id: 2, value: "2", label: "2 Cleaners" },
            { id: 3, value: "3", label: "3 Cleaners" },
            { id: 4, value: "4", label: "4 Cleaners" }
        ];

        if (isSubscription && typeOfSubscription == "weekly") {
            number_of_cleaners = [
                { id: 1, value: "1", label: "1 Cleaner" },
                { id: 2, value: "2", label: "2 Cleaners" }
            ];
        }

        var hours_required = [];

        var more_hours_required = [];

        if ((isSubscription && typeOfSubscription == "daily")) {
            hours_required = [
                { id: 4, value: "4", label: "4 Hours" },
                { id: 6, value: "6", label: "6 Hours" },
                { id: 8, value: "8", label: "8 Hours" }
            ];
        } else {
            hours_required = this.hoursRequiredOptions();

            more_hours_required = this.hoursRequiredOptions(true);
        }

        var field_own_equipments = [
            { id: 'yes', value: "Yes", label: "Yes" },
            { id: 'no', value: "No", label: "No" }
        ];

        var other_services = [
            { id: 'ironing', value: "ironing", label: "Ironing" },
            { id: 'inside-window-cleaning', value: "inside-window-cleaning", label: "Interior window cleaning " },
        ];

        var show_more_option = [
            { id: 'show-more', value: "no", label: "Show more options" }
        ];

        var isShowMore = this.state.show_more_option;

        var service_needed_value = this.state.field_service_needed;

        var hours_required_value = this.state.hours_required;

        var number_of_cleaners_value = this.state.number_of_cleaners;


        var field_own_equipments_value = this.state.field_own_equipments;

        var other_services_values = this.state.other_services;

        //other_services
        var currentStepClass = this.currentStep() === 1 ? '' : "d-none";

        var show_subscription = false;

        var show_daily_subscription = false;


        if (number_of_cleaners_value.value <= 2 && hours_required_value.value <= 4 && service_needed_value.value == dataValues.DATA_VALUE_EVERY_WEEK && current_city != DOHA) {
            show_subscription = true;
        }


        var field_multiple_times_week_values = this.state.field_multiple_times_week;

        var booking_date = this.state.booking_date;

        var time_picker = this.timePicker(booking_date);

        var selected_days = [];

        var show_error = "";

        if (service_needed_value.value == dataValues.DATA_VALUE_MULTIPLE_TIMES_WEEK) {
            selected_days = field_multiple_times_week_values;
            if (selected_days.length == 0) {
                show_error = "Please select days";
            }
        }
        var netflixCleaningPackages = [];
        if (isSubscription) {
            var noOfHours = typeOfSubscription == "daily" ? hours_required_value.value : null;
            netflixCleaningPackages = this.netflixCleaningPackages(typeOfSubscription, number_of_cleaners_value.value, noOfHours);
            //console.log(netflixCleaningPackages);
        }

        var date_picker = this.datePicker(show_error);
        //{show_error!="" &&  <p className="error">{show_error}</p>}
        //console.log( isSubscription && typeOfSubscription == "weekly" );

        var dailySubscriptionText = (!isSubscription && service_needed_value.value == dataValues.DATA_VALUE_MULTIPLE_TIMES_WEEK) ? (<div className="netflix-cleaning-message row mb-4">
            <div className="col">
                Save money when you sign up for a daily subscription. <a href="book-online/daily-subscription">Click Here</a>
            </div>
        </div>) : "";

        //

        var isTaxApplicable = true;
        if (locationHelper.getCurrentCountryId() == QATAR_ID) {
            isTaxApplicable = false;
        }
        let discountData = this.state.discountData;


        let showDiscountNotification = (typeof discountData.success != "undefined" && discountData.success) ? true : false;

        // console.log("showDiscountNotification discountData",showDiscountNotification, discountData);
        /*if () {
            
        }*/
        return (
            <div id="section-request-form" className={currentStepClass}>
                {this.props.showDiscountNotification()}
                {/*&& (<div className="alert alert-success">
                    {/*discountData.description}
                    Congratulations! Your discount code has been applied
                </div>)*/}
                {!isSubscription && (<section>
                    <FormFieldsTitle title="Tell us what you need and book your cleaner online. Pay by cash on delivery or credit card at check-out." titleClass="h5 mb-4" />
                </section>)}
                <section className={isSubscription ? "d-none" : ""}>
                    <FormFieldsTitle title="How often do you need cleaning?" />
                    <CheckRadioBoxInput
                        InputType="radio"
                        name="field_service_needed"
                        inputValue={service_needed_value}
                        items={cleaning_frequency}
                        onInputChange={this.handleInputChange}
                        parentClass="row mb-1"
                        childClass="col-6 col-sm-3 mb-3 d-flex min-height-130" />
                </section>
                {
                    (!isSubscription && show_subscription) && (
                        <div className="netflix-cleaning-message row mb-4">
                            <div className="col">
                                Save money when you sign up for a monthly subscription. <a href="book-online/subscription">Click Here</a>
                            </div>
                        </div>)
                }
                {
                    dailySubscriptionText
                }
                {
                    /* titleClass={!isMobile() ? "h3 mb-4" : "h3 mb-2"} 
                    parentClass={ !isMobile() ? "row" : "row mb-2"}*/
                    service_needed_value.value == dataValues.DATA_VALUE_MULTIPLE_TIMES_WEEK && (
                        <section className={(isSubscription && typeOfSubscription == "daily") ? "d-none" : ""}>
                            <FormFieldsTitle title="On which weekdays would you like cleaning?" titleClass="h3 mb-4" />
                            { /*isMobile() && dailySubscriptionText */}
                            <CheckRadioBoxInput
                                InputType="checkbox"
                                inputValue={field_multiple_times_week_values}
                                onInputChange={this.handleInputChange}
                                name="field_multiple_times_week" items={field_multiple_times_week_options}
                                parentClass={"row mb-2"}
                                childClass="col-6 col-sm-4 mb-3 d-flex"
                                validationClasses="min-check-2" />
                            { /*!isMobile() && dailySubscriptionText */}
                        </section>)
                }

                <section className={(isSubscription && typeOfSubscription != "weekly") ? "d-none" : ""}>
                    <FormFieldsTitle title="How many cleaners do you need?" />
                    <CheckRadioBoxInput
                        InputType="radio"
                        name="number_of_cleaners"
                        inputValue={number_of_cleaners_value}
                        items={number_of_cleaners}
                        onInputChange={this.handleInputChange}
                        childClass="col-6 col-sm-3 mb-3 d-flex"
                        parentClass="row mb-2" />
                </section>
                <section className={(isSubscription && typeOfSubscription != "daily") ? "d-none" : "mb-2"}>
                    <FormFieldsTitle title={typeOfSubscription == "daily" ? "How many hours do you need the cleaner for?" : "How long should they stay?"} />
                    <CheckRadioBoxInputWithPrice
                        InputType="radio"
                        name="hours_required"
                        inputValue={hours_required_value}
                        items={hours_required}
                        onInputChange={this.handleInputChange}
                        parentClass="row"
                        childClass="col-6 col-sm-3 mb-3 d-flex"
                        {...((isSubscription && typeOfSubscription == "daily") ? { isTaxApplicable: false } : { isTaxApplicable: isTaxApplicable })} />
                    {(hours_required_value.value == "dummy" || hours_required_value.value >= 5) && <CheckRadioBoxInput InputType="radio" name="hours_required" inputValue={hours_required_value} items={more_hours_required} onInputChange={this.handleInputChange} childClass="col-6 col-sm-3 mb-3 d-flex" parentClass="row mb-2" />}

                </section>

                {netflixCleaningPackages.length != 0 && (
                    <section className="netflix-cleaning-package mb-4">
                        <FormFieldsTitle title="Select a subscription package" />
                        <PackageBox typeOfPackage="booking" items={netflixCleaningPackages} name="packages" inputValue={selectedPackage} onInputChange={this.handleInputChange} />
                    </section>)
                }
                <section className={(isSubscription && typeOfSubscription == "daily") ? "d-none" : ""}>
                    <FormFieldsTitle title="Do you need cleaning materials? " />
                    <CheckRadioBoxInput
                        InputType="radio"
                        name="field_own_equipments"
                        inputValue={field_own_equipments_value}
                        items={field_own_equipments}
                        onInputChange={this.handleInputChange}
                        childClass="col-6 col-sm-3 mb-3 d-flex"
                        parentClass="row mb-2" />
                </section>
                <section>
                    <FormFieldsTitle title="Pick your start date and time" />
                    <div className="row mb-2">
                        {date_picker}
                        {time_picker}
                    </div>
                </section>
                <section>
                    <FormFieldsTitle title="Any special requirements or comments? (Optional)" />
                    <div className="row mb-2">
                        <div className="col-6 col-sm-4 mb-3 d-flex">
                            <label className="btn-block pointer">
                                <input name="show_more_option" className="d-none" value={this.state.show_more_option} onChange={(e) => this.handleShowMoreOptionChange()} type="checkbox" />
                                <span className="btn-radio border rounded">Show more options</span>
                            </label>
                        </div>
                    </div>
                </section>
                <section className={!isShowMore && "d-none"} >
                    <FormFieldsTitle title="Do you need ironing or window cleaning?" />
                    <CheckRadioBoxInput
                        InputType="checkbox"
                        inputValue={other_services_values}
                        onInputChange={this.handleInputChange}
                        name="other_services" items={other_services}
                        childClass="col-6 col-sm-4 mb-3 d-flex"
                        parentClass="row mb-2" />
                    <FormFieldsTitle title="Any special instructions" />
                    <div className="row mb-4">
                        <div className="col mb-3">
                            <TextareaInput name="details" onInputChange={this.handleInputChange} placeholder="Example: The keys are under the door mat, and the money is on the kitchen counter." />
                        </div>
                    </div>
                </section>
                <section>
                    <BookNextStep total={this.state.total} title="Contact Details" moveNext={this.moveNext} toStep="2" setModal={this.mobileSummary} />
                </section>
            </div>
        )
    }
    timePicker(booking_date) {
        //console.log("timePicker"+booking_date)
        return (<CleaningTimePicker
            inputValue={this.state.booking_time}
            name="booking_time"
            booking_date={booking_date}
            onInputChange={this.handleCustomChange}
            validationClasses="required"
            startDate={this.props.startDate} />);
    }
    datePicker(show_error) {
        var { hours_required, field_service_needed, number_of_cleaners, field_multiple_times_week, isSubscription, typeOfSubscription } = this.state;
        //console.log("field_multiple_times_week", field_multiple_times_week);
        return (<CleaningDatePick
            inputValue={this.state.booking_date}
            name="booking_date"
            hours_required={hours_required}
            number_of_cleaners={number_of_cleaners}
            service_needed={field_service_needed}
            selected_days={field_multiple_times_week}
            show_error={show_error}
            dataValues={dataValues}
            onInputChange={this.handleInputChange}
            isSubscription={isSubscription}
            typeOfSubscription={typeOfSubscription}
            validationClasses="required"
            startDate={this.props.startDate}
        />);
    }
    ContactDetails(isLocationDetailShow) {

        var currentStepClass = this.currentStep() === 2 ? '' : "d-none";
        //var currentStepClass =  ''
        var isLocationDetailShow = "yes";

        var contact_details = {
            input_email: this.state.input_email,
            input_phone: this.state.input_phone,
            input_name: this.state.input_name,
            input_last_name: this.state.input_last_name
        }

        if (isLocationDetailShow) {
            contact_details.input_address_city = this.state.input_address_city;
            contact_details.input_address_area = this.state.input_address_area;
            contact_details.input_address_area_building_name = this.state.input_address_area_building_name;
            contact_details.input_address_area_building_apartment = this.state.input_address_area_building_apartment;
        }

        var autocompleteShow = true;

        var cityOptions = this.props.cityOptions;

        var areasOptions = getAreaOptions(cityOptions, this.state.input_address_city);

        var userProfile = this.props.userProfile;

        if (!this.state.loader || (status == "cancel" || status == "decline")) {

            return (
                <div id="personal-information-form" className={currentStepClass}>
                    <ContactDetailsStep
                        total={this.state.total}
                        cityOptions={cityOptions}
                        areasOptions={areasOptions}
                        userProfile={userProfile}
                        signInDetails={this.props.signInDetails}
                        autocompleteShow={autocompleteShow}
                        isLocationDetailShow={isLocationDetailShow}
                        contact_details={contact_details}
                        handleDropDownChange={this.handleInputChange}
                        handleInputChange={this.handleInputChange}
                        mobileSummary={this.mobileSummary}
                        moveNext={this.moveNext}
                        isBooking={true}
                        showDialogToChangeServiceDate={this.state.showDialogToChangeServiceDate}
                        couponValue={this.state.couponCode}
                        handleCouponChange={this.handleCouponChange}
                        isCurrentCityOnly={false}
                        currentCity={this.props.current_city}
                    />
                </div>
            )
        } else {
            return (
                <main loader={this.state.loader ? "show" : "hide"}><Loader /></main>
            );
        }
    }
    PaymentSection() {
        var payment = this.state.payment;

        var currentStepClass = this.currentStep() === 3 ? '' : "d-none";

        var showCOD = this.state.isSubscription ? false : true;

        if (!this.state.paymentLoader) {

            return (
                <div id="payment-method-form" className={currentStepClass}>
                    <section className="payment">
                        <PaymentMethods
                            isCreditCardCouponApplied={this.props.isCreditCardCouponApplied}
                            name="payment"
                            inputValue={payment}
                            status={status}
                            bid={bid}
                            tt={tt}
                            onInputChange={this.handleInputChange}
                            showCOD={showCOD}
                            isSubscription={this.state.isSubscription} />
                    </section>
                    <section>
                        <BookNextStep total={this.state.total} title="Looking forward to serving you" noNextStep={false} moveNext={this.moveNext} toStep="4" setModal={this.mobileSummary} />
                    </section>
                </div>
            );
        } else {
            return (
                <main loader={this.state.paymentLoader ? "show" : "hide"}><Loader /></main>
            );
        }
    }
    mobileSummary(boolVal) {
        this.setState({
            showMobileSummary: boolVal
        })
    }
    getChangeServiceDateModalContent() {
        const selectedCity = this.state.input_address_city;
        const citySlug = selectedCity ? "in " + selectedCity.label : "";
        return (

            <div className="modal-body">
                <p className="info-text"><strong>Oops! Unfortunately we cannot book Fridays {citySlug} just yet. Would you like to pick a different day instead?</strong></p>
                <div className="popup city-change-pop-up-buttons text-center mth">
                    <button className="btn btn-inline-block btn-warning py-2 text-uppercase font-weight-bold" onClick={() => { this.toggleServiceDateChangeDialog(false) }}  >
                        <span className="text-uppercase">Change my service date</span>
                    </button>

                    <a className="text-uppercase " id="change-flow-button-modal" aria-d-none="true" data-dismiss="modal" data-switch="switchToQuote"> </a>
                </div>
            </div>
        );
    }
    getChangeServiceTimeModalContent() {
        const selectedCity = this.state.input_address_city;
        const citySlug = selectedCity ? "in " + selectedCity.label : "";
        return (
            <div className="modal-body">
                <p className="info-text"><strong>Oops! Selected Booking time not available</strong></p>
                <div className="popup city-change-pop-up-buttons text-center mth">
                    <button className="btn btn-inline-block btn-warning py-2 text-uppercase font-weight-bold" onClick={() => { this.toggleSlotNotAvilable(false) }}  >
                        <span className="text-uppercase">Change my service time</span>
                    </button>
                </div>
            </div>
        );
    }
    componentDidUpdate(prevProps, prevState) {
        if (status == null) {
            
            var {
                field_service_needed,
                booking_date,
                couponCode,
                hours_required,
                field_own_equipments,
                number_of_cleaners,
                field_multiple_times_week,
                input_address_city,
                payment, discountPrices, walletUsed } = this.state;
            var city = current_city;
            var prices = this.calculatePrice();
            var new_price = this.state.price;

            if (prevState.discountPrices != discountPrices &&
                (typeof discountPrices != "undefined" && typeof discountPrices.price != "undefined")) {
                this.setState(discountPrices);
            }
            /*if( prevState.booking_date != booking_date ){
                var popUp = this.setFridayPopup();
            }*/
            if (prevState.total != prices.total || 
                prevState.walletUsed != prices.walletUsed ) {
                this.setState(prices);
                //console.log(" sdsad sad sadsa 12232", prevState.total, prices.total, prevState.walletUsed, prices)
            }
            if ((prevState.field_service_needed !== field_service_needed ||
                prevState.hours_required !== hours_required ||
                prevState.field_own_equipments !== field_own_equipments ||
                prevState.number_of_cleaners !== number_of_cleaners ||
                prevState.field_multiple_times_week !== field_multiple_times_week ||
                prevState.booking_date !== booking_date ||
                prevState.input_address_city !== input_address_city ||
                prevState.payment !== payment ||
                prevState.walletUsed !== walletUsed ||
                prevState.couponCode !== couponCode)) {
                var totalPrice = this.calculatePrice(true);
                this.handleCouponChange(couponCode, totalPrice.price);
            }
        }
    }
    componentWillReceiveProps(newProps) {
        //console.log("componentWillReceiveProps", status);
        if (status == null) {
            if ((newProps.isCreditCardCouponApplied !== this.props.isCreditCardCouponApplied) && newProps.isCreditCardCouponApplied) {
                this.setState({
                    couponCode: ""
                });
            }
            if (newProps.softSafeBookingId !== this.props.softSafeBookingId) {
                var payment = this.state.payment;
                if (payment == "") {
                    payment = "credit";
                }
                this.setState({
                    softSafeBookingId: newProps.softSafeBookingId,
                    softSafeRequestId: newProps.softSafeRequestId
                })
                //Credit card coupon
                this.applyCreditCardCoupon(payment);

            }
            if (newProps.discountData !== this.props.discountData) {
                this.setState({
                    discountData: newProps.discountData,
                })
            }
            if (newProps.discountPrices !== this.props.discountPrices) {
                this.setState(
                    { discountPrices: newProps.discountPrices }
                );
            }
            if (newProps.setUserArea !== this.props.setUserArea) {
                this.setState({
                    input_address_area: newProps.setUserArea
                })
            }
            if (newProps.setUserArea !== this.props.setUserArea) {
                this.setState({
                    input_address_area: newProps.setUserArea
                })
            }
        }

    }
    render() {
        var items = this.sumarryItems();
        let showPromo = true;
        const showPrice = true;

        //var calculate_price = this.calculatePrice();
        let {total, vat, packages, isSubscription, discountData, booking_date, walletUsed,field_own_equipments} = this.state;

        var isLoading = this.state.loader || this.props.loader;

        let packagePrice = isSubscription && typeof packages.rate != "undefined" ? packages.rate :0;
        
        if (isSubscription) {
            showPromo = false;
        }

        if(field_own_equipments.value == "Yes" && isSubscription){
            packagePrice += packages.data_cleaning_materials;
        }

        let {userProfile, isWalletAvailable} = this.props;

        let userWalletAmount = isWalletAvailable ? userProfile.userWallet.totalAmount : 0;

        var bookingSummary = <NewBookingSummary
            items={items}
            showPromo={showPromo}
            showPrice={showPrice}
            total={total}
            vat = {vat}
            couponValue={this.state.couponCode}
            handleCouponChange={this.handleInputChange}
            showMobileSummary={this.state.showMobileSummary}
            setModal={this.mobileSummary}
            discountData={discountData}
            isCoreBooking={true}
            walletAvailable={isWalletAvailable}
            walletUsed = {walletUsed}
            userWalletAmount = {userWalletAmount}
            isSubscription={isSubscription}
            packagePrice = {packagePrice} />

        if (isLoading) {
            return (
                <React.Fragment>
                    <div className="col-lg-8">
                        <main loader={isLoading ? "show" : "hide"}><Loader /></main>
                    </div>
                    <div className="col-md-3 ml-auto">
                        {bookingSummary}
                    </div>
                </React.Fragment>
            );

        } else {

            return (
                <React.Fragment>
                    <div className="col-lg-8">
                        {isWalletAvailable ?
                            <div className='wallet-div'>
                                <img src={"../../../../dist/images/wallet-filled-money-tool-light.png"} className="mh-100" height="28" alt="" />
                                <span className='margin-left30' style={{ verticalAlign: 'middle', fontSize: '15px' }}>{stringConstants.YOU_HAVE_TXT}
                                    <span className='wallet-amount-span'>{this.props.userProfile.userWallet.currency.code + ' ' + this.props.userProfile.userWallet.totalAmount} </span>{stringConstants.WALLET_BALANCE_TXT}
                                </span>
                            </div>
                            : null
                        }
                        {this.CleaningRequest()}
                        {this.ContactDetails()}
                        {this.PaymentSection()}
                    </div>
                    <div className="col-md-3 ml-auto">
                        {bookingSummary}
                    </div>
                    {
                        this.state.isSlotNotAvailable &&
                        <GeneralModal modalBody={this.getChangeServiceTimeModalContent()} showHeader={false}
                            setModal={this.toggleSlotNotAvilable} modalClass="max-selected-modal model-no-header" />
                    }
                    {
                        this.state.showDialogToChangeServiceDate &&
                        <GeneralModal modalBody={this.getChangeServiceDateModalContent()} showHeader={false}
                            setModal={this.toggleServiceDateChangeDialog} modalClass="max-selected-modal model-no-header" />
                    }
                </React.Fragment >
            )
        }
    }
}
function compare(a, b) {
    if (a.planChargesDtoList[0].rate < b.planChargesDtoList[0].rate)
        return -1;
    if (a.planChargesDtoList[0].rate > b.planChargesDtoList[0].rate)
        return 1;
    return 0;
}

function mapStateToProps(state) {
    return {
        myCreditCardsData: state.myCreditCardsData,
        userProfile: state.userProfile,
        lang:state.lang,
        currentCity: state.currentCity,
        PricePlan: state.PricePlan
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ getAllCreditCard }, dispatch);
}

export default withCookies(connect(mapStateToProps, mapDispatchToProps)(BookCleaningPage));