import React from "react";
import FormFieldsTitle from "../../../components/FormFieldsTitle";
import DatePick from "../../../components/Form_Fields/DatePick";
import TimePicker from "../../../components/Form_Fields/TimePicker";
import TextareaInput from "../../../components/Form_Fields/TextareaInput";
import PaymentMethods from "../../../components/Form_Fields/PaymentMethods";
import NewBookingSummary from "../../../components/Form_Fields/NewBookingSummary";
import BookNextStep from "../../../components/Form_Fields/BookNextStep";
import FormTitleDescription from "../../../components/FormTitleDescription";
import ContactDetailsStep from "../../../components/ContactDetailsStep";
import SelectInput from "../../../components/Form_Fields/SelectInput";
import {scrollToTop, zendeskChatBox, isMobile} from "../../../actions/index";
import locationHelper from "../../../helpers/locationHelper";
import commonHelper from "../../../helpers/commonHelper";
import TaxCalculator from "./TaxCalculator";
import BookingVoucherify from "./BookingVoucherify";
import {withCookies} from "react-cookie";
import {connect} from "react-redux";
import Loader from "../../../components/Loader";
import stringConstants from '../../../constants/stringConstants';
let allServiceConstant = {};
let URLConstant = {};
let currentCurrency = "";
let current_city = "dubai";
import {
    CREDITCARD_COUPON
} from "../../../actions";
import GeneralModal from "../../../components/GeneralModal";
var status = null;
var bid = null;
var tt = null;
class BookMattressCleaningPage extends React.Component {

    constructor(props) {
        super(props);
        const { cookies } = props;
        var bookingData = {
            mattress: [{ mattress_sizes: { label: "", value: "" }, quantity: { label: "", value: "" }, cleaning_method: { label: "", value: "" } }],
            booking_date: '',
            booking_time: '',
            input_email: props.userProfile.email,
            input_phone: props.userProfile.address ? props.userProfile.address.phoneNumber : '',
            input_name: props.userProfile.customerFirstName,
            input_last_name: props.userProfile.customerLastName,
            input_address_city: {},
            input_address_area: props.setUserArea,
            input_address_area_building_name: props.userProfile.address ? props.userProfile.address.building : '',
            input_address_area_building_apartment: props.userProfile.address ? props.userProfile.address.apartment : '',
            showMobileSummary: false,
            discountData: {},
            voucherCode: "",
            hourlyRate: 0,
            subtotal: 0,
            vat: 0,
            totalAfterDiscount: 0,
            total: 0,
            price: 0,
            payment: '',
            loader: false,
            steamCleaningItems: {},
            normalCleaningItems: {},
            showMinMessage: false,
            learnMore: false,
            discountPrices: {},
            isNewBookingEngine: true,
            softSafeBookingId: '',
            softSafeRequestId: '',
            mattressCounts: {},
            walletUsed: '',
            isWalletAvailable: false,
        }

        var cookieData = props.getCookieFormData();

        if (Object.keys(cookieData).length) { // restore from cookies
            bookingData = cookieData;
            bookingData["softSafeBookingId"] = "";
            bookingData["softSafeRequestId"] = "";
            props.removeCookie("booking_data");
            props.removeCookie("submitted_data");
            props.removeCookie("confirmation_summary");
        }

        this.state = bookingData;

        this.ContactDetails = this.ContactDetails.bind(this);
        this.mattressCleaningRequest = this.mattressCleaningRequest.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleDropDownChange = this.handleDropDownChange.bind(this);
        this.currentStep = this.currentStep.bind(this);
        this.getPrices = this.getPrices.bind(this);
        this.moveNext = this.moveNext.bind(this);
        this.handleCustomChange = this.handleCustomChange.bind(this);
        this.handleAddMattress = this.handleAddMattress.bind(this);
        //this.handleRemoveMattress = this.handleRemoveMattress.bind(this);
        this.hashChangeHandler = this.hashChangeHandler.bind(this);
        this.calculatePrice = this.calculatePrice.bind(this);
        this.handleVoucherChange = this.handleVoucherChange.bind(this);
        this.mobileSummary = this.mobileSummary.bind(this);
        this.updateTotal = this.updateTotal.bind(this);
        this.mattressCleaningOptions = this.mattressCleaningOptions.bind(this);
        this.handleBookingTimeChange = this.handleBookingTimeChange.bind(this);
        this.toggleLearnMorePopUp = this.toggleLearnMorePopUp.bind(this);
        this.applyCreditCardCoupon = this.applyCreditCardCoupon.bind(this);

    }
    componentDidMount() {
        allServiceConstant = this.props.service_constants;
        URLConstant = this.props.url_constants;
        current_city = this.props.current_city;

        status = commonHelper.getParameterByName("status");
        bid = commonHelper.getParameterByName("bid");
        tt = commonHelper.getParameterByName("tt");

        if (status == null) {
            var city = locationHelper.getLocationByName(current_city);
            var promoCode = commonHelper.getParameterByName("promo") != null ? commonHelper.getParameterByName("promo") : "";
            this.setState({
                input_address_city: city,
                loader: false,
                voucherCode: promoCode
            });
        }
        currentCurrency = locationHelper.getCurrentCurrency();
        const walletAvailable = this.props.userProfile && this.props.userProfile.userWallet && this.props.userProfile.userWallet.currency.code === currentCurrency && this.props.userProfile.userWallet.totalAmount > 0 ? true : false;
        this.setState({ isWalletAvailable: walletAvailable });

        zendeskChatBox();
        window.addEventListener("hashchange", this.hashChangeHandler, false);
    }
    componentWillUnmount() {
        window.removeEventListener("hashchange", this.hashChangeHandler, false);
    }
    calculatePrice(getTotal = false) {
        let {
            mattress,
            booking_date,
            voucherCode,
            discountData,
            discountPrices,
            isWalletAvailable } = this.state;


        /*if(changed_element == "mattress"){
            mattress = changed_value;
        }*/

        var mattressPrices = this.getPrices(),
            item_label = "",
            no_of_seat = 0,
            key = "",
            item_price = 0,
            total_price = 0,
            selectedMattress = {
                'steam': {
                    'single': 0,
                    'king': 0,
                    'queen': 0,
                    'pillow': 0
                },
                'normal': {
                    'single': 0,
                    'king': 0,
                    'queen': 0,
                    'pillow': 0
                },
            };

        /*mattressPrices = mattressPrices["per-seat"];

        var steam_prince = mattressPrices["steam"];

        var normal_prince = mattressPrices["normal"];*/

        let mattressCounts = {
            no_of_pillow_for_steam_cleaning: 0,
            no_of_single_mattress_for_steam_cleaning: 0,
            no_of_queen_mattress_for_steam_cleaning: 0,
            no_of_king_mattress_for_steam_cleaning: 0,
            no_of_pillow_for_shampoo_cleaning: 0,
            no_of_single_mattress_for_shampoo_cleaning: 0,
            no_of_queen_mattress_for_shampoo_cleaning: 0,
            no_of_king_mattress_for_shampoo_cleaning: 0
        };
        let mattressQuantity = 0
        mattress.map((mattres, idx) => {
            var quantity = (typeof mattres.quantity.value != "undefined" && mattres.quantity.value != "") ? parseInt(mattres.quantity.value) : 0;
            var mattres_size = typeof mattres.mattress_sizes.value != "undefined" ? mattres.mattress_sizes.value : "";
            var cleaning_type = typeof mattres.cleaning_method.value != "undefined" ? mattres.cleaning_method.value : "";
            if ((mattres_size != "" && quantity != 0 && cleaning_type != "")) {
                mattressQuantity = selectedMattress[cleaning_type][mattres_size] + quantity;
                selectedMattress[cleaning_type][mattres_size] = mattressQuantity; //selectedMattress[cleaning_type][mattres_size] + quantity;
                mattres_size = mattres_size == "pillow" ? mattres_size : mattres_size + "_mattress";
                cleaning_type = cleaning_type == 'normal' ? 'shampoo' : cleaning_type;
                mattressCounts["no_of_" + mattres_size + "_for_" + cleaning_type + "_cleaning"] = mattressQuantity;
            }
        });

        //console.log("mattressCounts", mattressCounts);

        var single_price = 0;

        var steamCleaningItems = [],
            normalCleaningItems = [];
        /*steam_price = this.getPrices(mattress_sizes_val, "steam");
        normal_price = this.getPrices(mattress_sizes_val, "normal");*/

        Object.keys(selectedMattress["normal"]).map((key) => {
            if (selectedMattress["normal"][key] != 0) {
                var val = selectedMattress["normal"][key];
                single_price = this.getPrices(key, "normal"); //mattressPrices[key]["normal"];
                item_price = val * single_price;
                total_price += item_price;
                if (key == "pillow") {
                    item_label = "Pillow/ Cushions";
                } else {
                    item_label = key + ' size';
                }
                
                normalCleaningItems.push(val+" x "+item_label.capitalize());
                normalCleaningItems.push("Shampoo cleaning");
                normalCleaningItems.push(<span className="text-warning">{currentCurrency + " "+item_price.toFixed(2)}</span>);
            }
        });

        var item_price = 0;

        Object.keys(selectedMattress["steam"]).map((key) => {
            if (selectedMattress["steam"][key] != 0) {
                var val = selectedMattress["steam"][key];
                single_price = single_price = this.getPrices(key, "steam");
                item_price = val * single_price;
                total_price += item_price;
                if (key == "pillow") {
                    item_label = "Pillow/ Cushions";
                } else {
                    item_label = key + ' size';
                }
                steamCleaningItems.push(val+" x "+item_label.capitalize());
                steamCleaningItems.push("Steam cleaning");
                steamCleaningItems.push(<span className="text-warning">{currentCurrency + " "+item_price.toFixed(2)}</span>);
            }
        });
        var showMinMessage = false;
        if (total_price != 0) {
            total_price = parseInt(total_price);
            if (total_price <= 149) {
                total_price = 149;
                showMinMessage = true;
            }
        }

        let walletUsed = 0;
        if (!getTotal && isWalletAvailable) {
            const walletAmount = this.props.userProfile.userWallet.totalAmount;
            const walletCurrency = this.props.userProfile.userWallet.currency.code;

            if (walletCurrency === currentCurrency && walletAmount > 0) {
                if (walletAmount >= total_price) {
                    walletUsed = total_price;
                    total_price = 0;
                }
                else if (walletAmount < total_price) {
                    walletUsed = walletAmount;
                    total_price = total_price - walletAmount;
                }

            }

        }

        var prefferedDate = booking_date;

        var taxChargesTotal = TaxCalculator.calculateVAT(total_price, prefferedDate);

        var total = total_price + taxChargesTotal;

        var total = total_price + taxChargesTotal;
        if (this.props.userProfile && this.props.userProfile.userWallet) {
            const walletAmount = this.props.userProfile.userWallet.totalAmount;
            const walletCurrency = this.props.userProfile.userWallet.currency.code;

            if (walletCurrency === currentCurrency && walletAmount > 0) {

                total = total > 0 ? total : 0.00;
                //this.setState({walletUsed: walletUsed});
            }
        }

        var returnData = {
            total: total,
            subtotal: total_price,
            vat: taxChargesTotal,
            steamCleaningItems: steamCleaningItems,
            normalCleaningItems: normalCleaningItems,
            showMinMessage: showMinMessage,
            mattressCounts: mattressCounts,
            walletUsed: walletUsed
        }

        if (!getTotal && (typeof discountData != "undefined" && (typeof discountData.success != "undefined" && discountData.success))) {
            returnData = this.updateTotal(discountPrices, true);
            returnData["mattressCounts"] = mattressCounts;
            returnData["walletUsed"] = walletUsed;
            returnData["steamCleaningItems"] = steamCleaningItems;
            returnData["normalCleaningItems"] = normalCleaningItems;
        }

        return returnData
    }
    handleVoucherChange(value, new_price = 0) {
        var changed_element = null, changed_value = null;
        var couponValue = value.trim();
        this.setState({
            discountData: "load"
        })
        let { booking_date, voucherCode, booking_time, discountData, subtotal, vat, payment, price } = this.state;

        if (price == 0 && new_price == 0) {
            var response = BookingVoucherify.error("Please select the mattress");
            this.setState({
                discountData: response.data
            });
        } else {
            if (typeof couponValue == 'undefined' || couponValue.length == 0) {
                var response = BookingVoucherify.error("Please provide coupon.");
                this.setState({
                    discountData: response.data
                });
                //this.calculatePrice("reset_coupon", true);
            } else if (couponValue.trim().length < 3) { // minimum length of a promo code is more than 3 characters, so need to go to server if less than that
                var response = BookingVoucherify.error("Promotion code is invalid");
                this.setState({
                    discountData: response.data
                })
            }
            else {
                var updatedPrice = false;

                if (new_price != 0) {
                    price = new_price;
                }

                var rules = BookingVoucherify.generateRules(couponValue, price, allServiceConstant.SERVICE_MATTRESS_CLEANING, 0, booking_date, current_city, "", payment);

                BookingVoucherify.validate(couponValue, rules).then((response) => {
                    var res = response;
                    if (this.state.voucherCode != "" || (this.state.voucherCode == CREDITCARD_COUPON && this.state.payment == "credit")) {
                        this.updateTotal(res);
                    }
                });
            }
        }
    }
    updateTotal(res, isReturnData = false) {
        let { walletUsed, booking_date } = this.state;
        // console.log('vouvher res', res);
        // console.log('wallet used', walletUsed);
        let response = res;
        let discountData = response.data;
        let currentTotal = response.currentTotal;
        let taxChargesTotal = response.taxChargesTotal;
        let total = response.total;

        if (walletUsed > 0) {
            currentTotal = parseFloat((response.totalAfterDiscount - walletUsed).toFixed(2));
            if (currentTotal < 0)
                currentTotal = 0.0;
            taxChargesTotal = TaxCalculator.calculateVAT(currentTotal, booking_date);
            total = parseFloat((currentTotal + taxChargesTotal).toFixed(2));

        }

        var returnData = {
            price: currentTotal,
            total: total,
            subtotal: currentTotal,
            vat: taxChargesTotal,
            walletUsed: walletUsed
        };

        if (typeof res.isCreditCardCouponApplied != "undefined" && res.isCreditCardCouponApplied) {
            returnData["voucherCode"] = "";
            this.props.updateCreditCardCouponApplied();
        }

        if (isReturnData) {
            return returnData;
        } else {
            returnData["discountData"] = discountData;
            returnData["discountPrices"] = res
        }

        this.setState(returnData);
    }
    hashChangeHandler() {
        let hashVal = window.location.hash;
        let step;
        hashVal.length ? step = parseInt(window.location.hash.replace('#', '')) : step = 1;
        this.props.moveNext(step);
        scrollToTop();
    }
    sumarryItems(){
        const {mattress, booking_date, booking_time,discountData,subtotal, vat, payment, steamCleaningItems, normalCleaningItems, total, walletUsed} = this.state;

        let timeValue = "";

        if( booking_date != "" ){
            timeValue += booking_date;
            if( typeof booking_time.label != "undefined" ){
                timeValue += " "+booking_time.label;
            }
        }
        var items = [
            {label: 'Time', value: timeValue}
        ];
        let detailLblAdded = false;
        if(typeof steamCleaningItems == "object" && Object.keys(steamCleaningItems).length){
            detailLblAdded = true
            items.push({label: 'Details', value: steamCleaningItems, conClass: 'col-12 mb-4'});
        }
        if(typeof normalCleaningItems == "object" && Object.keys(normalCleaningItems).length){
            items.push({label: !detailLblAdded ? 'Details' : '', value: normalCleaningItems, conClass: 'col-12 mb-4'});
        }
        if(total != 0 ){
            if (this.props.userProfile && this.props.userProfile.userWallet) {
                const walletAmount = walletUsed; //this.props.userProfile.userWallet.totalAmount;
                const walletCurrency = this.props.userProfile.userWallet.currency.code;
                if (walletCurrency === currentCurrency && walletAmount > 0) {
                    items.push({ label: stringConstants.WALLET, value: `-  ${walletCurrency}  ${walletAmount}` });
                }
            }
            if(typeof discountData.promoCodeDiscountText != "undefined"){
                var index_to_insert = items.length - 3;
                items.push({ label: 'Promo code', value: discountData.promoCodeDiscountText });
                items.push({ label: 'Subtotal', value: currentCurrency + " " + subtotal.toFixed(2), text_strike: "yes" });
            } else {
                items.push({ label: 'Subtotal', value: currentCurrency + " " + subtotal.toFixed(2) });
            }
            items.push(
                { label: 'Payment', value: payment }
            );
        }

        return items;
    }
    
    currentStep() {
        return this.props.formCurrentStep;
    }
    moveNext(step) {
        const { formCurrentStep, signInDetails, showLoginModal, showLoginMenu, moveNextStep } = this.props;
        moveNextStep(step, this.state, true);
    }
    handleCustomChange = (idx) => (name, value) => {
        var new_name = name.replace("_" + idx, "")

        const newMattress = this.state.mattress.map((mattres, sidx) => {
            if (idx !== sidx) return mattres;
            return { ...mattres, [new_name]: value };
        });

        this.setState({ mattress: newMattress });

        //this.calculatePrice("mattress", newMattress);
    }
    handleBookingTimeChange(name, value) {
        this.setState({
            booking_time: value
        });
    }
    handleInputChange(name, value) {
        if (name == "payment") {
            this.applyCreditCardCoupon(value);
        }
        this.setState({
            [name]: value
        });
    }
    handleDropDownChange(name, value) {
        console.log(name, value);
    }
    getPrices(type, method) {
        var lite_weight_prices = (typeof this.props.pricePlan != "undefined" && typeof this.props.pricePlan.planDetail != "undefined") ? this.props.pricePlan.planDetail.packages : 0;//this.props.lite_weight_prices;
        if (type != "" && lite_weight_prices.length) {
            return lite_weight_prices.filter((item) => item.type == type && item.method == method).length ? lite_weight_prices.filter((item) => item.type == type && item.method == method)[0].price : 0;
        }
        return lite_weight_prices;
    }
    handleAddMattress = () => {
        //{mattress_sizes:{label:"",value:""},quantity:{label:"",value:""},cleaning_method:{label:"",value:""}
        var newMattress = this.state.mattress.concat([{ mattress_sizes: { label: "", value: "" }, quantity: { label: "", value: "" }, cleaning_method: { label: "", value: "" } }])
        this.setState({ mattress: newMattress });
        //this.calculatePrice("mattress", newMattress);
    }
    handleRemoveMattress = (idx) => () => {
        var newMattress = this.state.mattress.filter((s, sidx) => idx !== sidx);

        this.setState({ mattress: newMattress });

        //this.calculatePrice("mattress", newMattress);
    }
    mattressCleaningOptions(mattres) {
        var steam_price = 0;
        var normal_price = 0;
        var steam_price_label = "";
        var normal_price_label = "";

        if (typeof mattres != "undefined") {

            //var mattressPrices = this.getPrices();

            var mattress_sizes = mattres.mattress_sizes;

            if (mattress_sizes != "" && (Object.keys(mattress_sizes).length && mattress_sizes.value != "")) {
                var mattress_sizes_val = mattress_sizes.value;
                steam_price = this.getPrices(mattress_sizes_val, "steam");
                normal_price = this.getPrices(mattress_sizes_val, "normal");
            }
            if (steam_price != 0 && normal_price != 0) {
                steam_price_label = " ( " + currentCurrency + " " + steam_price + " ) ";

                normal_price_label = " ( " + currentCurrency + " " + normal_price + " ) ";
            }
        }

        var mattress_cleaning_option = [
            { id: 1, value: "steam", label: "Steam Cleaning" + steam_price_label },
            { id: 2, value: "normal", label: "Shampoo Cleaning" + normal_price_label }
        ];

        return mattress_cleaning_option;

    }
    mattressCleaningRequest() {
        var currentStepClass = this.currentStep() === 1 ? '' : "d-none";

        var mattress_sizes_option = [
            { id: 1, value: "single", label: "Single Size" },
            { id: 2, value: "queen", label: "Queen Size" },
            { id: 3, value: "king", label: "King Size" },
            { id: 4, value: "pillow", label: "Pillow/ Cushions" }
        ];

        var mattress_count_option = [
            { id: 1, value: "1", label: "1" },
            { id: 2, value: "2", label: "2" },
            { id: 3, value: "3", label: "3" },
            { id: 4, value: "4", label: "4" },
            { id: 5, value: "5", label: "5" }
        ];

        var i = 1;
        return (
            <div id="section-request-form" className={currentStepClass}>
                <section>
                    {this.props.showDiscountNotification(this.state.discountData)}
                    <FormTitleDescription title="Please describe the mattresses you want cleaned" desc="You can select multiple mattress sizes and cleaning methods" />
                    {
                        this.state.mattress.map((mattres, idx) => (
                            <div className={isMobile() ? "row mb-4 multiple-row" : "row mb-2 multiple-row"}>
                                <div className="col-12 col-md-4 mb-3">
                                    <SelectInput
                                        name={"mattress_sizes_" + idx}
                                        inputValue={mattres.mattress_sizes}
                                        label="Select Mattress"
                                        options={mattress_sizes_option}
                                        onInputChange={this.handleCustomChange(idx)} />
                                </div>
                                <div className="col-12 col-md-4 mb-3">
                                    <SelectInput
                                        name={"quantity_" + idx}
                                        inputValue={mattres.quantity}
                                        label="Select Quantity"
                                        options={mattress_count_option}
                                        onInputChange={this.handleCustomChange(idx)} />
                                </div>
                                <div className="col-12 col-md-4 mb-3">
                                    {(idx == 0) && <a href="#" className="cleaning-method-more" onClick={() => this.toggleLearnMorePopUp(true)}>Learn More</a>}
                                    {(idx != 0) && <a href="javascript:void(0)" onClick={this.handleRemoveMattress(idx)}> <i className="fa fa-trash text-danger"></i> </a>}
                                    <SelectInput name={"cleaning_method_" + idx} inputValue={mattres.cleaning_method} label="Select Cleaning method" options={this.mattressCleaningOptions(mattres)} onInputChange={this.handleCustomChange(idx)} />
                                </div>
                            </div>

                        ))}
                </section>
                <div className="mb-4">
                    <button className="btn border rounded bg-white text-warning px-3" onClick={this.handleAddMattress}><i className="fa fa-plus-circle"></i> Add</button>
                </div>
                <section>
                    <FormFieldsTitle title="Pick your start date and time" />
                    <div className="row mb-2">
                        <DatePick
                            name="booking_date"
                            inputValue={this.state.booking_date}
                            selectedDate={this.state.booking_date}
                            disable_friday={true}
                            onInputChange={this.handleInputChange}
                            validationClasses="required" />

                        <TimePicker inputValue={this.state.booking_time}
                            name="booking_time"
                            disableFirstSlot={true}
                            onInputChange={this.handleBookingTimeChange}
                            validationClasses="required" 
                            booking_date={this.state.booking_date}/>
                    </div>
                </section>
                <section>
                    <FormFieldsTitle title="What else should we know? (Optional)" />
                    <div className="row mb-4">
                        <div className="col mb-3">
                            <TextareaInput
                                name="details"
                                inputValue={this.state.details}
                                placeholder="Please describe the job you need to have done."
                                onInputChange={this.handleInputChange} />
                        </div>
                    </div>
                </section>
                <section>
                    <BookNextStep total={this.state.total} title="Contact Details" moveNext={this.moveNext} toStep="2" setModal={this.mobileSummary} />
                </section>
            </div>
        )
    }
    ContactDetails(isLocationDetailShow) {
        var cityOptions = locationHelper.getLocationByName(current_city);

        var currentStepClass = this.currentStep() === 2 ? '' : "d-none";
        //var currentStepClass =  ''
        var isLocationDetailShow = "yes";

        var contact_details = {
            input_email: this.state.input_email,
            input_phone: this.state.input_phone,
            input_name: this.state.input_name,
            input_last_name: this.state.input_last_name
        }

        if (isLocationDetailShow) {
            contact_details.input_address_city = this.state.input_address_city;
            contact_details.input_address_area = this.state.input_address_area;
            contact_details.input_address_area_building_name = this.state.input_address_area_building_name;
            contact_details.input_address_area_building_apartment = this.state.input_address_area_building_apartment;
        }

        var userProfile = this.props.userProfile;

        return (
            <div id="personal-information-form" className={currentStepClass}>
                <ContactDetailsStep
                    cityOptions={cityOptions}
                    userProfile={userProfile}
                    signInDetails={this.props.signInDetails}
                    isLocationDetailShow={isLocationDetailShow}
                    contact_details={contact_details}
                    handleDropDownChange={this.handleInputChange}
                    handleInputChange={this.handleInputChange}
                    mobileSummary={this.mobileSummary}
                    moveNext={this.moveNext}
                    toStep={3}
                    isBooking={true}
                    total={this.state.total}
                    currentCity={this.props.current_city}
                />
            </div>
        )
    }
    PaymentSection() {
        var payment = this.state.payment;
        var currentStepClass = this.currentStep() === 3 ? '' : "d-none";
        //var currentStepClass ='';
        if (!this.state.paymentLoader) {

            return (
                <div id="payment-method-form" className={currentStepClass}>
                    <section className="payment">
                        <PaymentMethods
                            isCreditCardCouponApplied={this.props.isCreditCardCouponApplied}
                            name="payment"
                            inputValue={payment}
                            status={status}
                            tt={tt}
                            onInputChange={this.handleInputChange} />
                    </section>
                    <section>
                        <BookNextStep total={this.state.total} title="Looking forward to serving you" noNextStep={false} moveNext={this.moveNext} setModal={this.mobileSummary} />
                    </section>
                </div>
            );
        } else {
            return (
                <main loader={this.state.paymentLoader ? "show" : "hide"}><Loader /></main>
            );
        }
    }
    mobileSummary(boolVal) {
        this.setState({
            showMobileSummary: boolVal
        })
    }
    componentDidUpdate(prevProps, prevState) {
        var { mattress, booking_date, voucherCode, normalCleaningItems, steamCleaningItems, payment } = this.state;
        var city = current_city;
        var prices = this.calculatePrice();
        var new_price = this.state.price;

        if (prevState.total != prices.total || prevState.mattress !== mattress) {
            this.setState({
                price: prices.subtotal,
                total: prices.total,
                subtotal: prices.subtotal,
                vat: prices.vat,
                normalCleaningItems: prices.normalCleaningItems,
                steamCleaningItems: prices.steamCleaningItems,
                showMinMessage: prices.showMinMessage,
                mattressCounts: prices.mattressCounts,
                walletUsed: prices.walletUsed
            })
            new_price = prices.subtotal;
        }

        //console.log(prevState.booking_date , booking_date);

        if ((prevState.mattress !== mattress ||
            prevState.voucherCode !== voucherCode ||
            prevState.booking_date !== booking_date
            || prevState.payment !== payment)
            && (voucherCode != "")) {
            var prices = this.calculatePrice(true);
            new_price = prices.subtotal;
            this.handleVoucherChange(voucherCode, new_price);
        }

    }
    componentWillReceiveProps(newProps) {

        if (status == null) {
            if ((newProps.isCreditCardCouponApplied !== this.props.isCreditCardCouponApplied) && newProps.isCreditCardCouponApplied) {
                this.setState({
                    couponCode: ""
                });
                //this.applyCreditCardCoupon();
            }
            if ((newProps.formCurrentStep == 3) && (newProps.formCurrentStep !== this.props.formCurrentStep)) {
                var payment = this.state.payment;
                if (payment == "") {
                    payment = "credit";
                    this.setState({
                        payment: payment
                    });
                    this.applyCreditCardCoupon(payment);
                }
            }
            if (newProps.setUserArea !== this.props.setUserArea) {
                this.setState({
                    input_address_area: newProps.setUserArea
                })
            }
        }
    }
    learnMoreCleaningMethod() {
        return (<div className="learn-more-body">
            <h4 className="text-primary">Steam cleaning</h4>
            <ul className="steam-cleaning">
                <li>Steam cleaning is great for killing bacteria, fungus, dust mites and germs. The heat of the steam is sanitizing and kills 99.99% of bacteria.</li>
                <li>Since only water is used for the cleaning, it is especially suitable for people who suffer allergies.</li>
                <li>Easily removes stains. Hard to remove substances such as wax, glue and chewing gum is dissolved by the steam.</li>
                <li>Suitable for delicate materials (not leather or silk).</li>
                <li>The drying process is quick. Most of the steam evaporates instantly.</li>
            </ul>
            <h4 className="text-primary">Shampoo cleaning</h4>
            <ul className="shampoo-cleaning">
                <li>Shampoo cleaning starts with vacuuming the mattress with a high-powered vacuum.</li>
                <li>Stains are removed using either machines or manually using professional fabric shampoo.</li>
                <li>Excess water is extracted from the mattress.</li>
                <li>A second round of vacuum deep cleaning ensures that remainder shampoo and dirt is removed.</li>
                <li>The approximate drying time in an air conditioned room is 6-8 hours depending on the fiber type used in the mattress.</li>
            </ul>
        </div>)
    }
    toggleLearnMorePopUp(state) {
        this.setState({
            learnMore: state
        })
    }
    applyCreditCardCoupon(value) {
        var returnData = this.props.applyCreditCardCoupon(value, this.state.voucherCode);
        var { voucherCode } = this.state;
        var applyCreditCardCoupon = false;
        var payment = typeof value != "undefined" ? value : this.state.payment;
        if (this.state.payment == "") {
            this.setState({
                payment: 'credit',
            });
            applyCreditCardCoupon = true
        } else {
            applyCreditCardCoupon = (payment != "" && payment == "credit") ? true : false;
        }
        if (this.props.isCreditCardCouponApplied && (voucherCode != "" && voucherCode == CREDITCARD_COUPON)) {
            this.setState({
                voucherCode: ""
            });
        } else {
            if (!this.props.isCreditCardCouponApplied && ((applyCreditCardCoupon && payment == "credit") && (voucherCode == "" || voucherCode == CREDITCARD_COUPON))) {
                if (voucherCode == "") {
                    this.setState({
                        voucherCode: CREDITCARD_COUPON
                    });
                }
            } else {
                if (payment == "cash" && voucherCode == CREDITCARD_COUPON) {
                    this.setState({
                        voucherCode: ""
                    });
                }
            }
        }
    }
    render() {
        const items = this.sumarryItems();
        const showPromo = true;
        const showPrice = true;
        
        var discountData = this.state.discountData;

        var booking_date = this.state.booking_date;

        var showMinMessage = this.state.showMinMessage;
        
        const {isWalletAvailable} = this.props;

        let {total, vat} = this.state;

        var bookingSummary = <NewBookingSummary
            items={items}
            showPromo={showPromo}
            showPrice={showPrice}
            total={total}
            vat = {vat}
            booking_date={booking_date}
            discountData={discountData}
            couponValue={this.state.voucherCode}
            handleCouponChange={this.handleInputChange}
            showMobileSummary={this.state.showMobileSummary}
            setModal={this.mobileSummary}
            formData={this.state}
            updateTotal={this.updateTotal}
            isLW={true}
            isSpecializedCleaning={true}
            showMinMessage={showMinMessage}
            walletAvailable={isWalletAvailable}
        />;

        var isLoading = this.state.loader || this.props.loader;
        if (isLoading) {
            return (
                <React.Fragment>
                    <div className="col-lg-8">
                        <main loader={isLoading ? "show" : "hide"}><Loader /></main>
                    </div>
                    <div className="col-md-3 ml-auto">
                        {bookingSummary}
                    </div>
                </React.Fragment>
            )
        } else {
            return (<React.Fragment>
                <div className="col-lg-8">
                    {isWalletAvailable ?
                        <div className='wallet-div'>
                            <img src={"../../../../dist/images/wallet-filled-money-tool-light.png"} className="mh-100" height="28" alt="" />
                            <span className='margin-left30' style={{ verticalAlign: 'middle', fontSize: '15px' }}>{stringConstants.YOU_HAVE_TXT}
                                <span className='wallet-amount-span'>{this.props.userProfile.userWallet.currency.code + ' ' + this.props.userProfile.userWallet.totalAmount} </span>{stringConstants.WALLET_BALANCE_TXT}
                            </span>
                        </div>
                        : null
                    }
                    {this.mattressCleaningRequest()}
                    {this.ContactDetails()}
                    {this.PaymentSection()}
                </div>
                <div className="col-md-3 ml-auto">
                    {bookingSummary}
                </div>
                {this.state.learnMore && <GeneralModal modalSize="modal-lg" title="About our cleaning methods" modalBody={this.learnMoreCleaningMethod()}
                    setModal={this.toggleLearnMorePopUp} />}
            </React.Fragment>)
        }
    }
}
function mapStateToProps(state) {
    return {
        myCreditCardsData: state.myCreditCardsData,
        showLoginMenu: state.showLoginMenu,
        currentCity: state.currentCity,
        lang: state.lang
    }
}
export default withCookies(connect(mapStateToProps)(BookMattressCleaningPage));