import React from "react";
import FormFieldsTitle from "../../../components/FormFieldsTitle";
import CheckRadioBoxInput from "../../../components/Form_Fields/CheckRadioBoxInput";
import CheckRadioBoxInputWithPrice from "../../../components/Form_Fields/CheckRadioBoxInputWithPrice";
import DatePick from "../../../components/Form_Fields/DatePick";
import TextareaInput from "../../../components/Form_Fields/TextareaInput";
import PaymentMethods from "../../../components/Form_Fields/PaymentMethods";
import NewBookingSummary from "../../../components/Form_Fields/NewBookingSummary";
import BookNextStep from "../../../components/Form_Fields/BookNextStep";
import FormTitleDescription from "../../../components/FormTitleDescription";
import ContactDetailsStep from "../../../components/ContactDetailsStep";
import {isValidSection, scrollToTop} from "../../../actions/index";
import locationHelper from "../../../helpers/locationHelper";
import commonHelper from "../../../helpers/commonHelper";
import TaxCalculator from "./TaxCalculator";
import BookingVoucherify from "./BookingVoucherify";
import {withCookies} from "react-cookie";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import Loader from "../../../components/Loader";
import stringConstants from '../../../constants/stringConstants';
let allServiceConstant = {};
import moment from 'moment';
let URLConstant = {};
let currentCurrency = "";
let current_city = "dubai";
var discountData = "";
import {
    CREDITCARD_COUPON,
    zendeskChatBox,
    UAE_ID
} from "../../../actions";
import TimePicker from "../../../components/Form_Fields/TimePicker";
var status = null;
var bid = null;
var tt = null;

class BookDeepCleaningPage extends React.Component{

    constructor(props) {
        super(props);
        const { cookies } = props;
        var bookingData = {
            homeType: {id: 1, value: "Apartment", label: "Apartment"},
            numberOfUnits: "",
            field_furnished: "",
            field_scrubbing: {},
            booking_date: '',
            input_email: props.userProfile.email,
            input_phone: props.userProfile.address ? props.userProfile.address.phoneNumber : '',
            input_name: props.userProfile.customerFirstName,
            input_last_name: props.userProfile.customerLastName,
            input_address_city: {},
            input_address_area: props.setUserArea,
            input_address_area_building_name: props.userProfile.address ? props.userProfile.address.building : '',
            input_address_area_building_apartment: props.userProfile.address ? props.userProfile.address.apartment : '',
            details: '',
            payment: '',
            deep_cleaning_options: [],
            showMobileSummary: false,
            discountData: {},
            voucherCode: "",
            hourlyRate: 0,
            subtotal: 0,
            vat: 0,
            totalAfterDiscount: 0,
            total: 0,
            price: 0,
            loader: false,
            discountPrices: {},
            deepCleaningPricePlan: {},
            isNewBookingEngine: true,
            softSafeBookingId: '',
            softSafeRequestId: '',
            walletUsed: '',
            isWalletAvailable:false,
            otherServices: [],
            booking_time: '',
        }

        let cookieData = props.getCookieFormData();

        if(Object.keys(cookieData).length){ // restore from cookies
            bookingData = cookieData;
            bookingData["softSafeBookingId"] = "";
            bookingData["softSafeRequestId"] = "";
            props.removeCookie("booking_data");
            props.removeCookie("submitted_data");
            props.removeCookie("confirmation_summary");
        }

        this.state = bookingData;

        this.ContactDetails = this.ContactDetails.bind(this);
        this.DeepCleaningRequest = this.DeepCleaningRequest.bind(this);
        this.handleCustomChange = this.handleCustomChange.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleDropDownChange = this.handleDropDownChange.bind(this);
        this.currentStep = this.currentStep.bind(this);
        this.getPrices = this.getPrices.bind(this);
        this.deepCleaningPriceOptions = this.deepCleaningPriceOptions.bind(this);
        this.moveNext = this.moveNext.bind(this);
        this.hashChangeHandler = this.hashChangeHandler.bind(this);
        this.calculatePrice = this.calculatePrice.bind(this);
        this.handleVoucherChange = this.handleVoucherChange.bind(this);
        this.updateTotal = this.updateTotal.bind(this);
        this.mobileSummary = this.mobileSummary.bind(this);
        this.applyCreditCardCoupon = this.applyCreditCardCoupon.bind(this);
        this.fetchPestPrice = this.fetchPestPrice.bind(this);
        
        let {service_constants} = props;

        this.fetchPestPrice(service_constants.SERVICE_DEEP_CLEANING_SERVICE);
    }
    componentDidMount() {
        allServiceConstant = this.props.service_constants;
        URLConstant =  this.props.url_constants;
        current_city = this.props.current_city;
        const url_params = this.props.url_params;

        status = commonHelper.getParameterByName("status");
        bid = commonHelper.getParameterByName("bid");
        tt = commonHelper.getParameterByName("tt");

        if( status == null ){
            var city = locationHelper.getLocationByName( current_city );
            var promoCode =  commonHelper.getParameterByName("promo") != null ? commonHelper.getParameterByName("promo") : "";
            this.setState({
                input_address_city: city,
                loader: false,
                voucherCode:promoCode
            });
        }else{

        }
        currentCurrency = locationHelper.getCurrentCurrency();
        const walletAvailable = this.props.userProfile && this.props.userProfile.userWallet && this.props.userProfile.userWallet.currency.code === currentCurrency && this.props.userProfile.userWallet.totalAmount > 0 ? true : false;
        this.setState({ isWalletAvailable: walletAvailable });
       
        zendeskChatBox();
        window.addEventListener("hashchange", this.hashChangeHandler, false);
    }
    componentWillUnmount(){
        window.removeEventListener("hashchange", this.hashChangeHandler, false);
    }
    fetchPestPrice(service_id){
        this.props.fetchPrices(service_id);
    }
    hashChangeHandler(){
        let hashVal = window.location.hash;
        let step;
        hashVal.length ? step=parseInt( window.location.hash.replace('#','') ) : step = 1;
        this.props.moveNext(step);
        scrollToTop();
    }
    sumarryItems(){
        let {deepCleaningPricePlan} = this.state;
        var deepCleaningAddtinalPrices = typeof deepCleaningPricePlan.planDetail != "undefined" ?  deepCleaningPricePlan.planDetail.additional_charges : [];
        //console.log("deepCleaningAddtinalPrices", deepCleaningPricePlan, deepCleaningAddtinalPrices);
        const {
            homeType, 
            numberOfUnits, 
            field_furnished, 
            field_scrubbing, 
            booking_date, 
            booking_time,
            discountData,
            subtotal, 
            vat, 
            payment, 
            total, 
            walletUsed,
            otherServices
            } = this.state;
            
        let jobDetails = [];
        
        let homeLbl = homeType.label;
        
        homeLbl += typeof numberOfUnits.label != "undefined" ?  " - " + numberOfUnits.label : "";
        
        jobDetails.push(homeLbl);

        let furnishedValue = "";
        let additionalPrices = [];
        let numberOfUnitsValue = numberOfUnits.value;
        if(deepCleaningAddtinalPrices.length && typeof numberOfUnits.value != "undefined"){
            var priceKey = homeType.value.toLowerCase();
            //console.log("numberOfUnitsValue", numberOfUnitsValue, priceKey);
            additionalPrices = deepCleaningAddtinalPrices.filter(item => item.rooms == numberOfUnitsValue && item.type == priceKey);
            additionalPrices = additionalPrices.length ? additionalPrices[0] : [];
        }

        if( (typeof field_furnished.value != "undefined" && field_furnished.value == "yes") && typeof numberOfUnits.value != "undefined"){
            jobDetails.push("Furnished");
            if(typeof additionalPrices.furnished_charges != "undefined"){
                furnishedValue = 'Yes ( '+currentCurrency +" " + additionalPrices.furnished_charges+" )";
            }
        }
        if(typeof numberOfUnits.hours != "undefined"){
            jobDetails.push(<span className="text-warning">{ numberOfUnits.hours.replace("Hours", "Hrs")+" estimate" }</span>);
        }
        
        var items = [
            {label: 'Time', value: booking_date},
            {label: 'Details', value: jobDetails, conClass: 'col-12 mb-4'}
        ];

        if (this.props.userProfile && this.props.userProfile.userWallet) {
            const walletAmount = walletUsed; //this.props.userProfile.userWallet.totalAmount;
            const walletCurrency = this.props.userProfile.userWallet.currency.code;
            if (walletCurrency === currentCurrency && walletAmount > 0) {
                items.push({ label: stringConstants.WALLET, value: `-  ${walletCurrency}  ${walletAmount}` });
            }
        }
        if(total != 0){
            if(typeof discountData.promoCodeDiscountText != "undefined"){
                var index_to_insert = items.length - 3;
                items.push({label: 'Promo code', value: discountData.promoCodeDiscountText});
                items.push({label: 'Subtotal', value: currentCurrency+" "+subtotal, text_strike:"yes"});
            }else{
                items.push({label: 'Subtotal', value: currentCurrency+" "+subtotal, classNme:'text-warning'});
            }
            items.push(
                {label: 'Payment', value: payment}
            );
        }
        
        items.push({label: 'Furnished', value: furnishedValue, classNme:'text-warning'});
        
        /*if(typeof field_scrubbing.label != "undefined"){
            items.push(
                {label: 'Scrubbing ', value: field_scrubbing.label},
            );
        }*/
        
        if(otherServices.length && typeof additionalPrices.steam_or_grout_cleaning_charges != "undefined"){
            let additionalPricesValue = currentCurrency +" " + additionalPrices.steam_or_grout_cleaning_charges;
            items.push({label: 'Additional services', value: additionalPricesValue, classNme:'text-warning'});
        }

        return items;

    }
    calculatePrice(getTotal = false){

        var changed_element = null , changed_value = null;

        var {
            homeType, 
            numberOfUnits, 
            booking_date,
            voucherCode, 
            discountData, 
            discountPrices, 
            isWalletAvailable, 
            field_furnished,
            deepCleaningPricePlan,
            otherServices} = this.state;
        let additionalPrices = [];
        let furnishedValue = 0;
        let steamOrGroutValue = 0;
        let numberOfUnitsValue = numberOfUnits.value;

        var deepCleaningAddtinalPrices = typeof deepCleaningPricePlan.planDetail != "undefined" ?  deepCleaningPricePlan.planDetail.additional_charges : [];
        
        if(deepCleaningAddtinalPrices.length && typeof numberOfUnitsValue != "undefined"){
            var priceKey = homeType.value.toLowerCase();
            additionalPrices = deepCleaningAddtinalPrices.filter(item => item.rooms == numberOfUnitsValue && item.type == priceKey);
            additionalPrices = additionalPrices.length ? additionalPrices[0] : [];
        }
        if( (typeof field_furnished.value != "undefined" && field_furnished.value == "yes") && typeof numberOfUnitsValue != "undefined"){
            if(typeof additionalPrices.furnished_charges != "undefined"){
                furnishedValue = additionalPrices.furnished_charges;
            }
        }
        if(otherServices.length && typeof additionalPrices.steam_or_grout_cleaning_charges != "undefined"){
            steamOrGroutValue = additionalPrices.steam_or_grout_cleaning_charges
        }

        var total_price = 0;
        var taxRate = 0 ;
        var updatedPrice = false;

        if(numberOfUnits != null && typeof numberOfUnits.value != "undefined"){
            total_price = parseInt(numberOfUnits.price) + furnishedValue + steamOrGroutValue;
        }

        let walletUsed = 0;
        if (!getTotal && isWalletAvailable) {
            const walletAmount = this.props.userProfile.userWallet.totalAmount;
            const walletCurrency = this.props.userProfile.userWallet.currency.code;

            if (walletCurrency === currentCurrency && walletAmount > 0) {
                if (walletAmount >= total_price) {
                    walletUsed = total_price;
                    total_price = 0;
                }
                else if (walletAmount < total_price) {
                    walletUsed = walletAmount;
                    total_price = total_price - walletAmount;
                }

            }

        }

        var prefferedDate = booking_date;

        var taxChargesTotal = TaxCalculator.calculateVAT(total_price,prefferedDate);

        var total = total_price + taxChargesTotal;
        if (this.props.userProfile && this.props.userProfile.userWallet) {
            const walletAmount = this.props.userProfile.userWallet.totalAmount;
            const walletCurrency = this.props.userProfile.userWallet.currency.code;

            if (walletCurrency === currentCurrency && walletAmount > 0) {

                total = total > 0 ? total : 0.00;
                //this.setState({walletUsed: walletUsed});
            }
        }
        var returnData = {
            total: total,
            subtotal : total_price,
            vat:taxChargesTotal,
            walletUsed: walletUsed
        };

        if( !getTotal && ( typeof discountData != "undefined" &&  ( typeof discountData.success != "undefined" && discountData.success ) )){ 
            returnData = this.updateTotal(discountPrices, true );
        }
        

        return returnData;
    }
    handleVoucherChange(value, new_price = 0) {

        var couponValue = value.trim();
        this.setState({
            discountData: "load"
        })
        let {type_of_pest, homeType, numberOfUnits, booking_date, booking_time,discountData,subtotal, vat, payment, price} = this.state;
        if(price == 0 && new_price == 0){
            var response = BookingVoucherify.error("Please select the package");
            this.setState({
                discountData: response.data
            });
        }else{
            if (typeof couponValue == 'undefined' || couponValue.length == 0) {
                var response = BookingVoucherify.error("Please provide coupon.");
                this.setState({
                    discountData: response.data
                });
                //this.calculatePrice("reset_coupon", true);
            } else if (couponValue.trim().length < 3) { // minimum length of a promo code is more than 3 characters, so need to go to server if less than that
                var response = BookingVoucherify.error("Promotion code is invalid");
                this.setState({
                    discountData: response.data
                })
                //this.calculatePrice("reset_coupon", true);
            }
            else {
                if(new_price != 0 ){
                    price = new_price;
                }
                var rules = BookingVoucherify.generateRules( couponValue, price, allServiceConstant.SERVICE_DEEP_CLEANING_SERVICE, 0, booking_date, current_city, "", payment);
            
                BookingVoucherify.validate(couponValue, rules).then((response)=> {
                    var res = response;
                    if(this.state.voucherCode != "" || (this.state.voucherCode == CREDITCARD_COUPON && this.state.payment == "credit")){
                        this.updateTotal(res);
                    }
                });
                
            }
        }
    }
    updateTotal( res, isReturnData = false ){
        let { walletUsed , booking_date} = this.state;
        var response = res;
        var discountData = response.data;
        var currentTotal = response.currentTotal;
        var taxChargesTotal = response.taxChargesTotal;
        var total = response.total;    
       

        if (walletUsed > 0) {
            currentTotal = parseFloat((response.totalAfterDiscount - walletUsed).toFixed(2));
            taxChargesTotal = TaxCalculator.calculateVAT(currentTotal, booking_date);
            total = parseFloat((currentTotal + taxChargesTotal).toFixed(2));
            //console.log('here',currentTotal);
        }
        var returnData = {
            price: currentTotal,
            total: total,
            subtotal : currentTotal,
            vat:taxChargesTotal,
            walletUsed : walletUsed
        };
        if(typeof res.isCreditCardCouponApplied != "undefined" && res.isCreditCardCouponApplied){
            returnData["voucherCode"] = "";
            this.props.updateCreditCardCouponApplied();
        }
        if( isReturnData ){
            return returnData;
        }else{
            returnData["discountData"] = discountData;
            returnData["discountPrices"] =  res
        }

        this.setState( returnData );
    }
    currentStep(){
        return this.props.formCurrentStep;
    }
    moveNext(step){
        const {formCurrentStep, signInDetails, showLoginModal, showLoginMenu, moveNextStep} = this.props;
        moveNextStep(step, this.state, true);
    }
    handleCustomChange(name, value){
        this.setState({
            booking_time: value
        });
    }
    handleInputChange(name, value){
        let {numberOfUnits} = this.state;
        if(name == "homeType" && typeof numberOfUnits.value != "undefined"){
            var numberOfUnitsValue = "";
            if(value.label == "Villa" && numberOfUnits.label == "Studio"){
                numberOfUnitsValue = "";
            }else{
                numberOfUnitsValue = this.deepCleaningPriceOptions(value, true);
                //console.log("numberOfUnits", numberOfUnitsValue);
            }
            this.setState({
                [name]: value,
                numberOfUnits: numberOfUnitsValue
            });
        }else{
            if(name == "payment"){
                this.applyCreditCardCoupon(value);
            }
            this.setState({
                [name]: value
            });
        }

        //console.log(name, value);
    }
    handleDropDownChange(name, value){
        console.log(name, value);
    }
    getPrices(){
        var lite_weight_prices = this.props.lite_weight_prices;

        return lite_weight_prices
    }
    deepCleaningPriceOptions(typeOfHome, selectedOpt = false){
        let {deepCleaningPricePlan} = this.state;

        var isTaxApplicable = locationHelper.getCurrentCountryId() == UAE_ID ? true : false;
        
        var prices = this.getPrices();

        //console.log("deepCleaningPriceOptions", prices);

        var sub_pest_key = "";

        var numberOfUnits = [];

        var homeTypeValue = typeof typeOfHome == "undefined" ? this.state.homeType : typeOfHome;

        var priceKey = homeTypeValue.value.toLowerCase();

        var deepCleaningPrices = typeof deepCleaningPricePlan.planDetail != "undefined" ?  deepCleaningPricePlan.planDetail.packages : []; //prices[priceKey];

        var i = 1;

        var key_label = "";
        var option_value = {};

        var numberOfUnits_val =  this.state.numberOfUnits;

        var selectedUnit = [];

        if(deepCleaningPrices.length){
            let pest_prices = [];
            if(typeof homeTypeValue.value != "undefined" ) {
                let i = 1;
                let key_label = "";
                //console.log("homeTypeValue.value 1213",homeTypeValue, priceKey);
                deepCleaningPrices.find((item) => {
                    let key = item.rooms;
                    if(item.type == priceKey && item.furnished == false) {
                        if (key == 0) {
                            key_label = "Studio";
                        } else {
                            key_label = key + " BR";
                        }
                        numberOfUnits.push({
                            id: i,
                            value: key,
                            label: key_label,
                            price: item.price,
                            currentCurrency: currentCurrency,
                            planId : deepCleaningPricePlan.id
                        })
                        i++;
                    }
                 });
            }
        }

        if(selectedOpt && ( typeof numberOfUnits_val.value != "undefined" && Object.keys(numberOfUnits_val).length )){
            let numberOfUnitsVal = numberOfUnits_val.value;
            selectedUnit =  numberOfUnits.filter(item => item.value == numberOfUnitsVal);
            return selectedUnit.length ? selectedUnit[0] : {};
        }else{
            return (<section>
                    <CheckRadioBoxInputWithPrice 
                        isTaxApplicable={isTaxApplicable}
                        InputType="radio" 
                        name="numberOfUnits" 
                        inputValue={this.state.numberOfUnits} 
                        items={numberOfUnits} 
                        onInputChange={this.handleInputChange} 
                        validationClasses="required" 
                        parentClass="row mb-2"
                        childClass="col-6 col-sm-2 mb-3 d-flex" 
                        validationClasses="radio-required"
                        validationMessage="Please select your home size."/>
                    </section>);
        }

    }
    DeepCleaningRequest(){
        var homeType = [
            {id: 1, value: "Apartment", label: "Apartment"},
            {id: 2, value: "Villa", label: "Villa"}
        ];
        var numberOfUnits_options = this.state.deep_cleaning_options;

        var field_furnished = [
            {id: 1, value: "yes", label: "Yes", boolVal:true},
            {id: 2, value: "no", label: "No", boolVal:false}
        ];

        var field_scrubbing = [
            {id: 1, value: "yes", label: "Yes", boolVal:true},
            {id: 2, value: "no", label: "No", boolVal:false}
        ];

        var homeTypeValue = this.state.homeType;

        var numberOfUnits_value = this.state.numberOfUnits;

        var field_furnished_value = this.state.field_furnished;

        var field_scrubbing_value = this.state.field_scrubbing;

        var booking_date = this.state.booking_date;

        //otherServices
        var currentStepClass = this.currentStep() === 1 ? '' : "d-none";

        var otherServices = [
            { id: 'steam-cleaning', value: "steam_cleaning", label: "Steam cleaning" },
            { id: 'grout-cleaning', value: "scrubbing", label: "Grout cleaning & tile scrubbing" },
        ];
        
        var otherServicesValues = this.state.otherServices;
        return (
            <div id="section-request-form" className={currentStepClass}>
                {this.props.showDiscountNotification(this.state.discountData)}
                <section>
                    <div className="row mb-3">
                        <div className="col">
                            <div className="row m-0 bg-white border rounded">
                                <div className="col-1 bg-primary">
                                    
                                </div>
                                <div className="py-3 col">
                                    <div className="h2 mb-4">Book Deep Cleaning</div>
                                        <ul className="list-unstyled pl-1 deep-cleaning-list">
                                            <li>Our professional cleaning teams will bring cleaning materials to make your home sparkle</li>
                                            <li>Grout cleaning and steam cleaning ( great for chemical free sanitation of floors and surfaces) requires special equipment</li>
                                            <li>All prices below are excluding VAT</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                </section> 
                <section>
                    <FormFieldsTitle title="What is the size of your home?" />
                    <CheckRadioBoxInput 
                    InputType="radio" 
                    name="homeType" 
                    inputValue={homeTypeValue} 
                    items={homeType} 
                    onInputChange={this.handleInputChange} 
                    parentClass="row mb-2" 
                    childClass="col-12 col-md-4 mb-2 d-flex"
                    validationClasses="radio-required"
                    validationMessage="Please describe your home type." />
                </section>
                { this.deepCleaningPriceOptions(homeTypeValue) }
                <section>
                    <FormTitleDescription 
                        title="Will your home be furnished at the time of the service?" 
                        desc="Additional charges apply for a furnished home"/>
                    <CheckRadioBoxInput 
                        InputType="radio" 
                        name="field_furnished" 
                        inputValue={field_furnished_value} 
                        items={field_furnished} 
                        onInputChange={this.handleInputChange}  
                        childClass="col-6 col-sm-4 mb-3 d-flex"
                        validationClasses="radio-required"
                        />
                </section>
                <section>
                    <FormTitleDescription title="Do you require any of the following additional services?" desc="Additional charges apply" />
                    <CheckRadioBoxInput
                        InputType="checkbox"
                        inputValue={otherServicesValues}
                        onInputChange={this.handleInputChange}
                        name="otherServices" items={otherServices}
                        childClass="col-6 col-sm-4 mb-3 d-flex"
                        parentClass="row mb-2 text-center" />
                    {/*<CheckRadioBoxInput 
                        InputType="radio" name="field_scrubbing" 
                        inputValue={field_scrubbing_value} 
                        items={field_scrubbing} 
                        onInputChange={this.handleInputChange}  
                    childClass="col-6 col-sm-4 mb-3 d-flex" /> */ }
                </section>
                <section>
                    <FormFieldsTitle title="When do you need the service?" />
                    <div className="row mb-2">
                        <DatePick
                            inputValue = {booking_date}
                            name="booking_date"
                            selectedDate = {booking_date}
                            disable_friday={true}
                            onInputChange={this.handleInputChange}
                            validationClasses="required"/>
                        <div className="col-12 col-md-6">
                            <TimePicker
                                inputValue={this.state.booking_time}
                                name="booking_time"
                                disableFirstSlot={false}
                                onInputChange={this.handleCustomChange}
                                validationClasses="required"
                                childClass="mb-3"
                                validationMessage="Please select time"
                                booking_date={booking_date}
                                startDate={moment(this.props.startDate)}
                                workingHours={1}
                            />
                        </div>
                    </div>
                </section>
                <section>
                    <FormTitleDescription title="Any special instructions or additional details" desc="Please select the date you need the service on. If there are any changes, you can inform us later!" />
                    <div className="row mb-4">
                        <div className="col mb-3">
                            <TextareaInput
                                name="details" inputValue={this.state.details}
                                placeholder="Example: You will need to bring a gate pass, I will not be home and am leaving the key under the mat, et cetera"
                                onInputChange={this.handleInputChange}  />
                        </div>
                    </div>
                </section>
                <section>
                    <BookNextStep total={this.state.total} title="Contact Details" moveNext={this.moveNext} toStep="2" setModal={this.mobileSummary}/>
                </section>
            </div>
        )
    }
    ContactDetails(isLocationDetailShow){
        var cityOptions = locationHelper.getLocationByName(current_city);

        var currentStepClass = this.currentStep() === 2 ? '' : "d-none";
        //var currentStepClass =  ''
        var isLocationDetailShow = "yes";

        var contact_details = {
            input_email:this.state.input_email,
            input_phone:this.state.input_phone,
            input_name:this.state.input_name,
            input_last_name:this.state.input_last_name
        }

        if(isLocationDetailShow){
            contact_details.input_address_city = this.state.input_address_city;
            contact_details.input_address_area = this.state.input_address_area;
            contact_details.input_address_area_building_name = this.state.input_address_area_building_name;
            contact_details.input_address_area_building_apartment = this.state.input_address_area_building_apartment;
        }

        var userProfile =  this.props.userProfile;

        return (
            <div id="personal-information-form" className={currentStepClass}>
                <ContactDetailsStep
                    cityOptions={cityOptions}
                    userProfile = {userProfile}
                    signInDetails={this.props.signInDetails}
                    isLocationDetailShow={isLocationDetailShow}
                    contact_details={contact_details}
                    handleDropDownChange={this.handleInputChange}
                    handleInputChange={this.handleInputChange}
                    mobileSummary={this.mobileSummary}
                    moveNext={this.moveNext}
                    toStep={3}
                    total={this.state.total}
                    isBooking={true}
                    currentCity = {this.props.current_city}
                />
            </div>
        )
    }
    PaymentSection(){
        var payment = this.state.payment;
        var currentStepClass = this.currentStep() === 3 ? '' : "d-none";
        //var currentStepClass ='';
        if(!this.state.paymentLoader) {

            return (
                <div id="payment-method-form" className={currentStepClass}>
                    <section className="payment">
                        <PaymentMethods 
                        isCreditCardCouponApplied = {this.props.isCreditCardCouponApplied}
                        name="payment" 
                        inputValue={payment} 
                        status={status} 
                        bid={bid} 
                        onInputChange={this.handleInputChange} />
                    </section>
                    <section>
                        <BookNextStep total={this.state.total} title="Looking forward to serving you" noNextStep={false} moveNext={this.moveNext} setModal={this.mobileSummary} />
                    </section>
                </div>
            );
        } else {
            return (
                <main loader={this.state.paymentLoader ? "show" : "hide"}><Loader/></main>
            );
        }
    }
    mobileSummary(boolVal){
        this.setState({
            showMobileSummary: boolVal
        })
    }
    componentDidUpdate(prevProps, prevState) {
        var {homeType, numberOfUnits, booking_date,voucherCode, payment} = this.state;
        var city = current_city;
        var prices = this.calculatePrice();
        var new_price = this.state.price;

        if(prevState.total != prices.total) {
            this.setState({
                price: prices.subtotal,
                total: prices.total,
                subtotal: prices.subtotal,
                vat: prices.vat,
                walletUsed: prices.walletUsed
            })
            new_price = prices.subtotal;
        }
        if((
            prevState.numberOfUnits !== numberOfUnits || 
            prevState.homeType !== homeType || 
            prevState.voucherCode !== voucherCode || 
            prevState.payment !== payment ||
            prevState.booking_date !== booking_date) && ( voucherCode != "" ) ){
            var prices = this.calculatePrice(true);
            new_price = prices.subtotal;
            this.handleVoucherChange(voucherCode, new_price);
        }
        //console.log("call componentDidUpdate");
    }
    componentWillReceiveProps (newProps) {

        if (status == null) {
            if((newProps.isCreditCardCouponApplied !== this.props.isCreditCardCouponApplied) && newProps.isCreditCardCouponApplied){
                this.setState({
                    voucherCode: ""
                });
                //this.applyCreditCardCoupon();
            }
            if ( (newProps.formCurrentStep == 3 ) && ( newProps.formCurrentStep !== this.props.formCurrentStep )) {
                var payment = this.state.payment;
                if (payment == "") {
                    payment = "credit";
                    this.setState({
                        payment: payment
                    });
                    this.applyCreditCardCoupon(payment);
                }
            }
            if((newProps.pricePlan !== this.props.pricePlan) ){
                this.setState({
                    deepCleaningPricePlan: newProps.pricePlan
                });
                //this.deepCleaningPriceOptions(newProps.pricePlan, this.state.homeType ) 
                //.pestPriceOptions( newProps.pricePlan, this.state.typeOfPest, this.state.homeType );
                //this.applyCreditCardCoupon();
            }
            if (newProps.setUserArea !== this.props.setUserArea) {
                this.setState({
                    input_address_area: newProps.setUserArea
                })
            }
        }
    }
    applyCreditCardCoupon(value){
        var returnData = this.props.applyCreditCardCoupon(value, this.state.voucherCode);
        var {voucherCode} = this.state;
        var applyCreditCardCoupon = false;
        var payment = typeof value != "undefined" ? value : this.state.payment;
        if (this.state.payment == "") {
            this.setState({
                payment: 'credit',
            });
            applyCreditCardCoupon = true
        } else {
            applyCreditCardCoupon = (payment != "" && payment == "credit") ? true : false;
        }
        if (this.props.isCreditCardCouponApplied && (voucherCode != "" && voucherCode == CREDITCARD_COUPON)) {
            this.setState({
                voucherCode: ""
            });
        } else {
            if (!this.props.isCreditCardCouponApplied && ((applyCreditCardCoupon && payment == "credit") && (voucherCode == "" || voucherCode == CREDITCARD_COUPON))) {
                if (voucherCode == "") {
                    this.setState({
                        voucherCode: CREDITCARD_COUPON
                    });
                }
            } else {
                if (payment == "cash" && voucherCode == CREDITCARD_COUPON) {
                    this.setState({
                        voucherCode: ""
                    });
                }
            }
        }
    }
    render(){
        const items = this.sumarryItems();

        const showPromo = true;

        const showPrice = true;

        let {total, vat} = this.state;

        var discountData = this.state.discountData;

        var booking_date = this.state.booking_date;

        var isLoading =  this.state.loader || this.props.loader;

        let {userProfile, isWalletAvailable} = this.props;

        var bookingSummary = <NewBookingSummary
            items = {items}
            showPromo = {showPromo}
            showPrice = {showPrice}
            total = {total}
            vat= {vat}
            booking_date = {booking_date}
            discountData = {discountData}
            couponValue = {this.state.voucherCode}
            handleCouponChange = {this.handleInputChange}
            showMobileSummary = {this.state.showMobileSummary}
            setModal = {this.mobileSummary}
            formData = {this.state}
            updateTotal = {this.updateTotal}
            isLW = {true}
            walletAvailable={isWalletAvailable}
        />;

        if(isLoading) {
            return (
                <React.Fragment>
                    <div className="col-lg-8">
                        <main loader={ isLoading ? "show" : "hide"}><Loader/></main>
                    </div>
                    <div className="col-md-3 ml-auto">
                        {bookingSummary}
                    </div>
                </React.Fragment>
            );

        }else {
            return (
                <React.Fragment>
                    <div className="col-lg-8">
                        {isWalletAvailable ?
                            <div className='wallet-div'>
                                <img src={"../../../../dist/images/wallet-filled-money-tool-light.png"} className="mh-100" height="28" alt="" />
                                <span className='margin-left30' style={{verticalAlign: 'middle',fontSize: '15px'}}>{stringConstants.YOU_HAVE_TXT}
                                    <span className='wallet-amount-span'>{this.props.userProfile.userWallet.currency.code + ' ' + this.props.userProfile.userWallet.totalAmount} </span>{stringConstants.WALLET_BALANCE_TXT}
                                </span>
                            </div>
                            : null
                        }
                        {this.DeepCleaningRequest()}
                        {this.ContactDetails()}
                        {this.PaymentSection()}
                    </div>
                    <div className="col-md-3 ml-auto">
                        {bookingSummary}
                    </div>
                </React.Fragment>
            )
        }
    }
}
function mapStateToProps(state){
    return {
        myCreditCardsData: state.myCreditCardsData,
        showLoginMenu: state.showLoginMenu,
        currentCity: state.currentCity,
        lang: state.lang,
        pricePlan:state.PricePlan
    }
}

export default withCookies(connect(mapStateToProps)(BookDeepCleaningPage));