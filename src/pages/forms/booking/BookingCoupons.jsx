import {backend_url} from "../../../api";
import axios from 'axios';
import {couponValidate} from "../../../actions/"
import commonHelper from "../../../helpers/commonHelper";
const Coupons = {
    validate:function (coupon, data, user_token = "") {
        var coupon = coupon.trim()
        if (typeof coupon == 'undefined' || coupon.length == 0) {
            return this.error("Please provide coupon.");
        }else if (coupon.trim().length <= 3) { // minimum length of a promo code is more than 3 characters, so need to go to server if less than that
            //this.couponMessage.error()
            return this.error("Please provide coupon.")
        } else {
            var isCustomerLoggedIn = data["customerId"] != "0" ? true : false;

            var jsonData = data;

            var userDetails = commonHelper.getCookieByName("_user_details");

            var signedInUserId = 0;

            var signedInCustomerId = 0;

            //console.log(userDetails, typeof userDetails);

            if(userDetails != ""){
                userDetails = JSON.parse(userDetails);
                signedInUserId = parseInt(userDetails.userId);
                signedInCustomerId = parseInt(userDetails.customer.id);
            }

            return couponValidate(jsonData, isCustomerLoggedIn, user_token, signedInUserId, signedInCustomerId).then((res)=> {
                //{"coupon":"{\"status\":\"OK\",\"success\":true,\"size\":null,\"data\":{\"finalPrice\":126.0,\"discountType\":\"PERCENT_OFF\",\"discount\":10.00,\"actualPrice\":140,\"description\":\"Hard coded coupon for credit card\",\"maxAmountDiscount\":null},\"errors\":[]}"}

                var data = JSON.parse(res.coupon);
                //console.log("couponValidate",data);
                if(data.status == "BAD_REQUEST"){
                    return this.error(data.errors);
                }else{
                    return data;
                }
            });
        }
    },
    generateRules(coupon, rules, isRecurring, price,  serviceId, serviceLocationId, validDate, customerId){
        var new_rules = {};
        Object.keys(rules).map(function(key, value) {
            if(key == "DAYS_OF_WEEK"){
                if(rules[key].length != 0){
                    new_rules[key] = rules[key].join(",");
                }
            }else{
                new_rules[key] = rules[key];
            }
        });

        var serviceLocationId = [parseInt(serviceLocationId)];

        var data = {};

        data["couponCode"] = coupon;

        data["couponRules"] = new_rules;

        if(validDate != ""){
            data["validDate"] = validDate;
        }

        data["isRecurring"] = isRecurring;
        data["serviceId"] = serviceId;
        data["serviceLocationId"] = serviceLocationId;
        data["price"] = parseFloat(price);

        if(customerId != 0){
            data["customerId"] = customerId;
        }

        return data;
    },
    error:function (error) {
        var errors = {"data":{"error":true}};
        if(typeof error == "string"){
            errors["data"]["error_message"] =  error;
        }else{
            var error_message = "";
            if(error){
                error.map((item,index) =>{
                    error_message = item.errorMessage;
                })
            }
            errors["data"]["error_message"] = error_message;
        }
        return errors;
    },
    financial(x) {
        return Number.parseFloat(x).toFixed(2);
    }
}
export default Coupons;