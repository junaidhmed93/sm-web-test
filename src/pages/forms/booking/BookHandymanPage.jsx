import React from "react";
import FormFieldsTitle from "../../../components/FormFieldsTitle";
import CheckRadioBoxInput from "../../../components/Form_Fields/CheckRadioBoxInput";
import CheckRadioBoxInputWithPrice from "../../../components/Form_Fields/CheckRadioBoxInputWithPrice";
import DatePick from "../../../components/Form_Fields/DatePick";
import TimePicker from "../../../components/Form_Fields/TimePicker";
import TextareaInput from "../../../components/Form_Fields/TextareaInput";
import PaymentMethods from "../../../components/Form_Fields/PaymentMethods";
import NewBookingSummary from "../../../components/Form_Fields/NewBookingSummary";
import BookNextStep from "../../../components/Form_Fields/BookNextStep";
import ContactDetailsStep from "../../../components/ContactDetailsStep";
import { isValidSection, scrollToTop, zendeskChatBox, UAE_ID } from "../../../actions/index";
import locationHelper from "../../../helpers/locationHelper";
import commonHelper from "../../../helpers/commonHelper";
import TaxCalculator from "./TaxCalculator";
import BookingVoucherify from "./BookingVoucherify";
import moment from 'moment';
import { withCookies } from "react-cookie";
import { connect } from "react-redux";
import Loader from "../../../components/Loader";
import GeneralModal from "../../../components/GeneralModal";
import GreatForPopUp from "../../../components/GreatForPopUp";
import stringConstants from '../../../constants/stringConstants';
var allServiceConstant = {};
var URLConstant = {};
var currentCurrency = "";
var current_city = "dubai";
var discountData = "";
var typeOfHandymanOpt = []
import {
    OXYCLEAN_PRICE,
    CREDITCARD_COUPON,
    getAreaOptions
} from "../../../actions";
import FormTitleDescription from "../../../components/FormTitleDescription";
let status = null;
let bid = null;
let tt = null;
class BookHandymanPage extends React.Component {
    constructor(props) {
        super(props);

        let defaultServiceOption = props.defaultServiceOption != null ? props.defaultServiceOption : "";

        let defaultSubServiceOption = props.defaultSubServiceOption != null ? props.defaultSubServiceOption : "";
        
        let {service_constants, isNoonPage, getServiceCode} = props;
        if(isNoonPage){
            defaultSubServiceOption = {id: 1, value: service_constants.SERVICE_TV_MOUNTING, label: "TV Mounting", serviceCode: getServiceCode(service_constants.SERVICE_TV_MOUNTING)}
        }
        
        let bookingData = {
            typeOfHandyman: defaultServiceOption,
            sub_service_options: [],
            sub_service: defaultSubServiceOption,
            numberOfUnits: {},
            tvSize: "",
            numberOfHours: "",
            booking_date: '',
            homeType: "",
            oxyclean: '',
            booking_time: '',
            wallMount: '',
            input_email: props.userProfile.email,
            input_phone: props.userProfile.address ? props.userProfile.address.phoneNumber : '',
            input_name: props.userProfile.customerFirstName,
            input_last_name: props.userProfile.customerLastName,
            input_address_city: {},
            input_address_area: props.setUserArea,
            input_address_area_building_name: props.userProfile.address ? props.userProfile.address.building : '',
            input_address_area_building_apartment: props.userProfile.address ? props.userProfile.address.apartment : '',
            payment: isNoonPage ? 'cash' : '',
            discountData: {},
            voucherCode: "",
            isHourlyRate: false,
            subtotal: 0,
            vat: 0,
            totalAfterDiscount: 0,
            total: 0,
            price: 0,
            showMobileSummary: false,
            loader: false,
            details: "",
            popupURL: "",
            showDialogToGreatFor: false,
            discountPrices: {},
            showMoreHours: false,
            handyPricePlan: {},
            isNewBookingEngine: true,
            softSafeBookingId: '',
            softSafeRequestId: '',
            walletUsed: '',
            orderNo: '',
            tvSizeParams: '',
            voucherLoader : false
        }

        let cookieData = props.getCookieFormData();

        if (Object.keys(cookieData).length) { // restore from cookies
            bookingData = cookieData;
            //console.log("cookieData 123", cookieData);
            bookingData["softSafeBookingId"] = "";
            bookingData["softSafeRequestId"] = "";
            props.removeCookie("booking_data");
            props.removeCookie("submitted_data");
            props.removeCookie("confirmation_summary");
        }

        this.state = bookingData;

        this.ContactDetails = this.ContactDetails.bind(this);
        this.ContactDetailsWithAllCity = this.ContactDetailsWithAllCity.bind(this);
        this.HandymanRequest = this.HandymanRequest.bind(this);
        this.handleShowMoreOptionChange = this.handleShowMoreOptionChange.bind(this);
        this.handleCustomChange = this.handleCustomChange.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleDropDownChange = this.handleDropDownChange.bind(this);
        this.currentStep = this.currentStep.bind(this);
        this.moveNext = this.moveNext.bind(this);
        this.handleNoOfUnits = this.handleNoOfUnits.bind(this);
        this.loadSubServiceOptions = this.loadSubServiceOptions.bind(this);
        this.handleTypeOfHandymanChange = this.handleTypeOfHandymanChange.bind(this);
        this.getPrices = this.getPrices.bind(this);
        this.hoursOptions = this.hoursOptions.bind(this);
        this.acCleaningOptions = this.acCleaningOptions.bind(this);
        this.tvMountingOptions = this.tvMountingOptions.bind(this);
        this.calculatePrice = this.calculatePrice.bind(this);
        this.hashChangeHandler = this.hashChangeHandler.bind(this);
        this.handleVoucherChange = this.handleVoucherChange.bind(this);
        this.mobileSummary = this.mobileSummary.bind(this);
        this.updateTotal = this.updateTotal.bind(this);
        this.greatForPopUp = this.greatForPopUp.bind(this);
        this.toggleGreatForPopUp = this.toggleGreatForPopUp.bind(this);
        this.isPlumbingAndElectrician = this.isPlumbingAndElectrician.bind(this);
        this.isPlumbing = this.isPlumbing.bind(this);
        this.applyCreditCardCoupon = this.applyCreditCardCoupon.bind(this);
        this.oxyCleanPopUp = this.oxyCleanPopUp.bind(this);
        this.isAcServices = this.isAcServices.bind(this);
        this.fetchHandymanPrice = this.fetchHandymanPrice.bind(this);
        this.tvSizes = this.tvSizes.bind(this);
        /*if(isNoonPage){
            this.fetchHandymanPrice(service_constants.SERVICE_TV_MOUNTING);
        }*/

    }
    toggleGreatForPopUp(boolVal, url = "") {
        this.setState({
            showDialogToGreatFor: boolVal,
            popupURL: url
        });
    }
    greatForPopUp(serviceUrl = "") {
        return (<GreatForPopUp service={serviceUrl} />);
    }
    componentDidMount() {
        allServiceConstant = this.props.service_constants;
        URLConstant = this.props.url_constants;
        current_city = this.props.current_city;

        const url_params = this.props.url_params;

        status = commonHelper.getParameterByName("status");
        bid = commonHelper.getParameterByName("bid");
        tt = commonHelper.getParameterByName("tt");
        
        let getServiceCode = this.props.getServiceCode;

        typeOfHandymanOpt = [
            {id: 1, value: allServiceConstant.SERVICE_HANDYMAN_SERVICE, label: "General handyman", serviceCode: getServiceCode(allServiceConstant.SERVICE_HANDYMAN_SERVICE)},
            {id: 2, value: allServiceConstant.SERVICE_AC_AND_HEATING, label: "AC Technician", serviceCode: getServiceCode(allServiceConstant.SERVICE_AC_AND_HEATING)},
            {id: 3, value: allServiceConstant.SERVICE_ELECTRICIAN, label: "Electrician", serviceCode: getServiceCode(allServiceConstant.SERVICE_ELECTRICIAN)},
            {id: 4, value: allServiceConstant.SERVICE_PLUMBING, label: "Plumber", serviceCode: getServiceCode(allServiceConstant.SERVICE_PLUMBING)}
        ];

        if (status == null) {
            let city = locationHelper.getLocationByName(current_city);
            let promoCode = commonHelper.getParameterByName("promo") != null ? commonHelper.getParameterByName("promo") : "";
            let orderNo = commonHelper.getParameterByName("order_no") != null ? commonHelper.getParameterByName("order_no") : "";
            let tvSize = commonHelper.getParameterByName("tv_size") != null ? commonHelper.getParameterByName("tv_size") : "";
            
            //console.log("tvSize", tvSize, this.tvSizes());
            
            if (typeof this.props.match != "undefined") {

                let defaultServiceOption = this.props.defaultServiceOption != null ? this.props.defaultServiceOption : "";

                if(typeof defaultServiceOption == "object" && defaultServiceOption.value != ""){
                    this.loadSubServiceOptions(defaultServiceOption);
                    if(this.isPlumbingAndElectrician()){
                        this.fetchHandymanPrice(defaultServiceOption.value);
                    }   
                }

                this.setState({
                    input_address_city: city,
                    loader: false,
                    voucherCode: promoCode,
                    orderNo: orderNo,
                    tvSizeParams: tvSize
                });

            }
        }
        currentCurrency = locationHelper.getCurrentCurrency();
        
        zendeskChatBox();
        window.addEventListener("hashchange", this.hashChangeHandler, false);
    }
    componentWillUnmount() {
        window.removeEventListener("hashchange", this.hashChangeHandler, false);
    }
    hashChangeHandler() {
        let hashVal = window.location.hash;
        let step;
        hashVal.length ? step = parseInt(window.location.hash.replace('#', '')) : step = 1;
        this.props.moveNext(step);
        scrollToTop();
    }

    sumarryItems() {
        let service_constants = allServiceConstant;
        const {
            typeOfHandyman,
            homeType,
            sub_service,
            oxyclean,
            booking_date,
            numberOfUnits,
            numberOfHours,
            tvSize,
            booking_time,
            discountData,
            subtotal,
            vat,
            payment,
            total,
            walletUsed
        } = this.state;
        
        let timeValue = "";
        
        let serviceLbl = typeOfHandyman.label;
        let  {isNoonPage} = this.props;

        if(!this.isPlumbingAndElectrician() && typeof sub_service.label != "undefined"){
            if(!isNoonPage){
                serviceLbl += " - "+sub_service.label;
            }else{
                serviceLbl = sub_service.label;
            }
        }

        let jobDetails = [serviceLbl];

        if(booking_date != "" ){
            timeValue = booking_date;
            if(typeof booking_time.label != "undefined" ){
                timeValue += " "+booking_time.label
            }
        }
        
        if( sub_service.value == service_constants.SERVICE_TV_MOUNTING ){
            if( typeof tvSize.label != "undefined"){
                jobDetails.push('TV Size - '+tvSize.label);
                if(!isNoonPage){
                    jobDetails.push(<span className="text-warning">{currentCurrency + " " +tvSize.price}</span>);
                }
            }
        }
        else if (sub_service.value == service_constants.SERVICE_AC_CLEANING && typeof homeType.label != "undefined") {

            let item_text = homeType.label;

            item_text = typeof numberOfUnits.label != "undefined" ?  item_text+" - "+numberOfUnits.label : item_text;
            
            jobDetails.push(item_text);

            if(typeof numberOfUnits.price != "undefined"){
                let ac_price = parseInt(numberOfUnits.price);
                jobDetails.push(<span className="text-warning">{currentCurrency + " " +ac_price}</span>);
            }
        }else{
            //console.log("numberOfHours.label", numberOfHours);

            if( typeof numberOfHours.id != "undefined" && numberOfHours.label != ""){
                jobDetails.push('No. of hours - '+numberOfHours.label);
                let priceItem = "";
                if(typeof numberOfHours.price != "undefined"){
                    priceItem = numberOfHours.price;
                }else{
                    if(numberOfHours.value != "dummy" && (typeof numberOfHours.per_hour_price != "undefined")){
                        let numberOfHoursVal = parseInt(numberOfHours.value);
                        let per_hour_price = parseInt(numberOfHours.per_hour_price);
                        priceItem = per_hour_price;
                    }
                }
                jobDetails.push(<span className="text-warning">{currentCurrency + " " +priceItem}/hour</span>);
            }
        }
        var items = [
            {label: 'Time', value: timeValue},
            {label: 'Details', value: jobDetails, conClass: 'col-12 mb-4'}
        ];

        const {isWalletAvailable} = this.props;
        
        let subTotalText =  (typeof subtotal !="undefined" && !Number.isInteger(subtotal)) ? (subtotal).toFixed(2) : subtotal;

        if (isWalletAvailable) {
            const walletAmount = walletUsed;//this.props.userProfile.userWallet.totalAmount;
            const walletCurrency = this.props.userProfile.userWallet.currency.code;
            if (walletCurrency === currentCurrency && walletAmount > 0) {
                items.push({ label: stringConstants.WALLET, value: `-  ${walletCurrency}  ${walletAmount}` });
            }
        }
        if(total != 0 && !isNoonPage) {
            if (typeof discountData.promoCodeDiscountText != "undefined") {
                let index_to_insert = items.length - 3;
                items.push({ label: 'Promo code', value: discountData.promoCodeDiscountText });
                items.push({ label: 'Subtotal', value: currentCurrency + " " + subTotalText, text_strike: "yes" });
            } else {
                items.push({ label: 'Subtotal', value: currentCurrency + " " + subTotalText });
            }
        }else{
            if(typeof discountData.promoCodeDiscountText != "undefined"){
                items.push({ label: 'Promo code', value: discountData.promoCodeDiscountText });
            }
        }
        if(!isNoonPage){
            items.push({ label: 'Payment', value: payment });
        }

        return items;

    }
    calculatePrice(getTotal = false) {

        let service_constants = allServiceConstant, changed_element = null, changedValue = null;

        let { typeOfHandyman, sub_service, oxyclean, homeType, numberOfUnits, numberOfHours, tvSize, booking_date, voucherCode, discountPrices, discountData } = this.state;

        let total_price = 0;
        let taxRate = 0;
        let updatedPrice = false;
        let hourlyRate = false;

        if (sub_service.value == service_constants.SERVICE_AC_CLEANING && typeof numberOfUnits.price != "undefined") { // AC Cleaning
            total_price = numberOfUnits.price;
            if (oxyclean.value == "yes") {
                total_price += parseInt(numberOfUnits.value) * OXYCLEAN_PRICE;
            }
        }
        else if (sub_service.value == service_constants.SERVICE_TV_MOUNTING && typeof tvSize.price != "undefined") { // TV mounting
            //console.log("tvSize", tvSize);
            total_price = tvSize.price;
            
        } else if ((typeof numberOfHours.value != "undefined")) { // Hourly price Service
            if (numberOfHours.value != "dummy" && (typeof numberOfHours.per_hour_price != "undefined")) {
                let numberOfHoursVal = parseInt(numberOfHours.value);
                let per_hour_price = parseInt(numberOfHours.per_hour_price);
                total_price = numberOfHoursVal != 0 ? per_hour_price * numberOfHoursVal : per_hour_price;
                hourlyRate = typeof numberOfHours.is_hourly_price != "undefined" && numberOfHours.is_hourly_price == true ? true : false;
            } else {
                total_price = typeof numberOfHours.hourPrice != "undefined" ? parseInt(numberOfHours.hourPrice) : parseInt(numberOfHours.price);

                hourlyRate = (typeof numberOfHours.hourPrice == "undefined" && typeof numberOfHours.is_hourly_price != "undefined") && numberOfHours.is_hourly_price == true ? true : false;
            }

        }
        let walletUsed = 0;
        let isCoreService = this.isAcServices(sub_service.value) || this.isPlumbing() ? true : false;

        let {priceAfterWallet, isWalletAvailable} = this.props;

        if (!getTotal && isWalletAvailable) {
           
            let priceAfter = priceAfterWallet(total_price);
            /*const walletAmount = this.props.userProfile.userWallet.totalAmount;
            const walletCurrency = this.props.userProfile.userWallet.currency.code;
            //console.log("total_price", total_price);
            if (walletCurrency === currentCurrency && walletAmount > 0) {
                if (walletAmount >= total_price) {
                    walletUsed = total_price;
                    total_price = 0;
                }
                else if (walletAmount < total_price) {
                    walletUsed = walletAmount;
                    total_price = total_price - walletAmount;
                }
            }*/
            //console.log("walletUsed", walletUsed)
            total_price =  priceAfter.total_price;
            walletUsed = priceAfter.walletUsed;
        }

        let prefferedDate = booking_date;

        let taxChargesTotal = TaxCalculator.calculateVAT(total_price, prefferedDate);

        let total = total_price + taxChargesTotal;

        let returnData = {
            total: total,
            subtotal: total_price,
            vat: taxChargesTotal,
            isHourlyRate: hourlyRate,
            walletUsed: walletUsed
        };

        if (!getTotal && (typeof discountData != "undefined" && (typeof discountData.success != "undefined" && discountData.success))) {
            returnData = this.updateTotal(discountPrices, true);
        }

        return returnData;
    }
    handleVoucherChange(value, new_price = 0) {

        let couponValue = value.trim(),
            changed_element = null, changedValue = null;
        let {
            typeOfHandyman,
            sub_service,
            oxyclean,
            homeType,
            numberOfUnits,
            tvSize,
            numberOfHours,
            booking_date,
            voucherCode,
            isHourlyRate,
            booking_time,
            discountData,
            subtotal,
            vat,
            payment,
            price,
            walletUsed
        } = this.state;

        let {isNoonPage} = this.props

        //console.log("discountData", discountData);

        if( typeof discountData.success == "undefined" && isNoonPage){
            this.setState({
                discountData: "load"
            })
        }/*else{
            if(!isNoonPage){
                this.setState({
                    discountData: "load"
                })
            }
        }*/

        let isPlumbingAndElectrician = this.isPlumbingAndElectrician();

        if ((price == 0 && new_price == 0) || (typeof sub_service.value == "undefined" && !isPlumbingAndElectrician)) {
            let errorMessage = isNoonPage ? "Please select the TV size" : "Please select the package";
            var response = BookingVoucherify.error(errorMessage);
            this.setState({
                discountData: response.data
            });
        } else {
            if (typeof couponValue == 'undefined' || couponValue.length == 0) {
                let response = BookingVoucherify.error("Please provide coupon.");
                this.setState({
                    discountData: response.data
                });
                //this.calculatePrice("reset_coupon", true);
            } else if (couponValue.trim().length < 3) { // minimum length of a promo code is more than 3 characters, so need to go to server if less than that
                let response = BookingVoucherify.error("Promotion code is invalid");
                this.setState({
                    discountData: response.data
                })
                //this.calculatePrice("reset_coupon", true);
            }
            else {
                let service_constants = allServiceConstant;
                let updatedPrice = false;

                if (new_price != 0) {
                    price = new_price;
                }

                let $package_detail = {};

                if (sub_service.value == service_constants.SERVICE_AC_CLEANING && oxyclean.value == "yes") {
                    price = numberOfUnits.value * OXYCLEAN_PRICE;
                }

                let city = current_city;
                
                //price = walletUsed > 0 ? walletUsed + price : price;

                let rules = BookingVoucherify.generateRules(couponValue, price, typeOfHandyman.value, sub_service.value, booking_date, city, "", payment);

                this.setState({
                    voucherLoader: true
                })

                BookingVoucherify.validate(couponValue, rules).then((response) => {
                    let res = response;
                    this.setState({
                        voucherLoader: false
                    })
                    if (this.state.voucherCode != "" || (this.state.voucherCode == CREDITCARD_COUPON && this.state.payment == "credit")) {
                        this.updateTotal(res);
                    }
                });
            }
        }
    }
    updateTotal(res, isReturnData = false) {
        let { walletUsed, booking_date } = this.state;
        let response = res;
        let discountData = response.data;
        let currentTotal = response.currentTotal;
        let taxChargesTotal = response.taxChargesTotal;
        let total = response.total;
        let {priceAfterWallet} = this.props;

        if (walletUsed > 0) {
            let totalAfterDiscount = typeof response.totalAfterDiscount != "undefined" ? response.totalAfterDiscount : currentTotal;
            let priceAfter = priceAfterWallet(totalAfterDiscount);
            currentTotal = priceAfter.total_price;
            taxChargesTotal = TaxCalculator.calculateVAT(currentTotal, booking_date);
            total = parseFloat((currentTotal + taxChargesTotal).toFixed(2));
            walletUsed = priceAfter.walletUsed;
        }

        let returnData = {
            price: currentTotal,
            total: total,
            subtotal: currentTotal,
            vat: taxChargesTotal,
            isHourlyRate: this.state.isHourlyRate,
            walletUsed : walletUsed
        };
        if (typeof res.isCreditCardCouponApplied != "undefined" && res.isCreditCardCouponApplied) {
            returnData["voucherCode"] = "";
            this.props.updateCreditCardCouponApplied();
        }
        if (isReturnData) {
            return returnData;
        } else {

            returnData["discountData"] = discountData;
            returnData["discountPrices"] = res
        }

        this.setState(returnData);
    }
    currentStep() {
        return this.props.formCurrentStep;
    }
    moveNext(step){
        const {formCurrentStep,moveNextStep} = this.props;
        
        const {sub_service} = this.state;
        
        let isCoreService = true;

        if (formCurrentStep == '1' || formCurrentStep == '2' || formCurrentStep == '3') {
            moveNextStep(step, this.state, isCoreService);
        }
    }
    handleShowMoreOptionChange() {
        this.setState({
            show_more_option: !this.state.show_more_option
        });
    }
    handleCustomChange(name, value) {
        this.setState({
            booking_time: value
        });
    }
    handleInputChange(name, value) {
        let {isNoonPage} = this.props
        if (typeof value.isQuotes != "undefined" && value.isQuotes) {
            const { cookies } = this.props;
            cookies.set('passedService', JSON.stringify({ serviceId: value.value }), { path: '/', maxAge: 300 });
        } else {
            if (name == "payment") {
                this.applyCreditCardCoupon(value);
            }
            if (name == "sub_service") {
                let isNewBookingEngine = false;
                if (this.isAcServices(value.value)) {
                    this.fetchHandymanPrice(value.value);
                    isNewBookingEngine = true;
                }
            }
            if (name == "sub_service" && (value.value == allServiceConstant.SERVICE_AC_CLEANING || value.value == allServiceConstant.SERVICE_TV_MOUNTING)) {
                let updateOption = {
                    numberOfHours: ""
                }
                if (value.value == allServiceConstant.SERVICE_AC_CLEANING) {
                    updateOption["tvSize"] = "";
                }
                if (value.value == allServiceConstant.SERVICE_TV_MOUNTING) {
                    updateOption["numberOfUnits"] = "";
                }
                this.setState(updateOption);
            }
            if (isNoonPage && name == "input_address_city") {
                this.setState({
                    [name]: value,
                    input_address_area: '',
                });
                document.getElementsByName("input_address_area")[0].value = "";
            }
            if (name == "numberOfHours" && value.value == "dummy") {
                this.setState({
                    showMoreHours: !this.state.showMoreHours
                });
            } else {
                this.setState({
                    [name]: value
                });
            }
        }
    }
    handleDropDownChange(name, value) {
        //console.log(name, value);
    }
    fetchHandymanPrice(service_id) {
        this.props.fetchPrices(service_id);
        var thisVar = this;
        setTimeout(function () {
            let { numberOfHours, typeOfHandyman } = thisVar.state;

            let sub_service_url = thisVar.serviceUrlMap(typeOfHandyman.value);
            let hoursOptions = thisVar.hoursOptions(sub_service_url, 'hours_opts');

            if (typeof hoursOptions != "undefined" && hoursOptions.length) {
                let selectedHours = hoursOptions.filter((item) => item.id == numberOfHours.id);
                numberOfHours = selectedHours.length ? selectedHours[0] : numberOfHours;
                //console.log("callin 134", numberOfHours);
            }

            thisVar.setState({
                numberOfHours: numberOfHours
            });
        }, 1000);

    }
    isAcServices(serviceId){
        return true; 
    }
    handleNoOfUnits(name, value) {
        if (value.value == "dummy") {
            this.setState({
                showMoreHours: !this.state.showMoreHours
            });
        } else {
            this.setState({
                numberOfUnits: value
            });
        }
    }
    loadSubServiceOptions(typeOfHandyman) {

        let type_of_service_options = [];

        let service_constants = allServiceConstant;

        typeOfHandyman = typeof typeOfHandyman == "undefined" ? { id: 1, value: service_constants.SERVICE_HANDYMAN_SERVICE, label: "General handyman" } : typeOfHandyman;

        let {getServiceCode, isNoonPage} = this.props;

        let type_of_general_maintenance = [
            {id: 1, value: service_constants.SERVICE_TV_MOUNTING, label: "TV Mounting", serviceCode: getServiceCode(service_constants.SERVICE_TV_MOUNTING)},
            {id: 2, value: service_constants.SERVICE_FURNITURE_ASSEMBLY, label: "Furniture Assembly", serviceCode: getServiceCode(service_constants.SERVICE_FURNITURE_ASSEMBLY)},
            {id: 3, value: service_constants.SERVICE_CURTAIN_HANGING, label: "Curtain Hanging", serviceCode: getServiceCode(service_constants.SERVICE_CURTAIN_HANGING)},
            {id: 4, value: service_constants.SERVICE_LIGHTBULBS_AND_LIGHTING, label: "Light Bulbs / Spotlights", serviceCode: getServiceCode(service_constants.SERVICE_LIGHTBULBS_AND_LIGHTING)},
            {id: 5, value: service_constants.SERVICE_HANDYMAN_SERVICE, label: "Other",serviceCode: getServiceCode(service_constants.SERVICE_HANDYMAN_SERVICE)}
        ];
       
        let type_of_ac_maintance = [
            { id: 1, value: service_constants.SERVICE_AC_CLEANING, label: "AC Cleaning", serviceCode: getServiceCode(service_constants.SERVICE_AC_CLEANING) },
            { id: 2, value: service_constants.SERVICE_AC_REPAIR, label: "AC Repair", serviceCode: getServiceCode(service_constants.SERVICE_AC_REPAIR) },
            { id: 3, value: service_constants.SERVICE_AC_INSTALLATION, label: "AC Installation", serviceCode: getServiceCode(service_constants.SERVICE_AC_INSTALLATION) },
            { id: 4, value: service_constants.SERVICE_AC_DUCT_CLEANING, label: "AC Duct Cleaning", isQuotes: true, quotesLink: "../" + URLConstant.MAINTENANCE_PAGE_URI + "/journey1" }
        ];

        if (typeOfHandyman.value === service_constants.SERVICE_HANDYMAN_SERVICE) {
            type_of_service_options = type_of_general_maintenance;
        } else if (typeOfHandyman.value === service_constants.SERVICE_AC_AND_HEATING) {
            type_of_service_options = type_of_ac_maintance;
        }

        let numberOfHours = this.state.numberOfHours;

        if (typeof numberOfHours.is_hourly_price != "undefined") {
            let sub_service_url = this.serviceUrlMap(typeOfHandyman.value);
            let hoursOptions = this.hoursOptions(sub_service_url,'hours_opts');
            if(typeof hoursOptions != "undefined" && hoursOptions.length){
                let selectedHours = hoursOptions.filter((item) => item.id == numberOfHours.id);
                numberOfHours = selectedHours.length ? selectedHours[0] : numberOfHours;
            }
        }
        if(isNoonPage){
            this.setState({
                sub_service_options: type_of_service_options
            });
        }else{
            this.setState({
                sub_service_options: type_of_service_options,
                sub_service: '',
                numberOfHours: numberOfHours
            });
        }

    }
    handleTypeOfHandymanChange(name, item){
        if(name == "typeOfHandyman" && this.isPlumbingAndElectrician(item)){
            this.fetchHandymanPrice(item.value);
            this.setState({
                typeOfHandyman: item
            })
        }else{
            this.setState({
                typeOfHandyman: item
            });
        }
        this.loadSubServiceOptions(item);
    }
    acCleaningOptions(){
        let isTaxApplicable = locationHelper.getCurrentCountryId() == UAE_ID ? true : false;

        let handyPricePlan = this.state.handyPricePlan;

        let handyPricePlanPrices = typeof handyPricePlan.planDetail != "undefined" ? handyPricePlan.planDetail.packages : [];

        //let prices = this.getPrices(URLConstant.AC_MAINTENANCE_PAGE_URI, 'ac-cleaning');

        let homeType = [
            { id: 1, value: "Apartment", label: "Apartment" },
            { id: 2, value: "Villa", label: "Villa" }
        ];

        let homeTypeValue = this.state.homeType;

        let numberOfUnitsValue = this.state.numberOfUnits;

        let price_key = typeof homeTypeValue == "object" && typeof homeTypeValue.value != "undefined" ? homeTypeValue.value.toLowerCase() : "apartment";

        let numberOfUnits = [];
        let key_label = "";
        let i = 1;

        handyPricePlanPrices.find((item) => {
            let key = item.units;
            if (item.type == price_key) {
                if (key == 1) {
                    key_label = "Unit";
                } else {
                    key_label = key + " Units";
                }
                numberOfUnits.push({
                    id: i,
                    value: key,
                    label: key_label,
                    price: item.price,
                    currentCurrency: currentCurrency,
                    planId: handyPricePlan.id
                })
                i++;
            }
        });
        return (
            <React.Fragment>
                <section>
                    <FormFieldsTitle title="What type of home do you live in?" />
                    <CheckRadioBoxInput
                        isTaxApplicable={isTaxApplicable}
                        InputType="radio"
                        name="homeType"
                        inputValue={homeTypeValue}
                        items={homeType}
                        onInputChange={this.handleInputChange}
                        parentClass="row mb-2"
                        childClass="col-12 col-md-4 mb-2 d-flex"
                        validationClasses="radio-required"
                        validationMessage="Please select the home type"
                    />
                </section>
                <section>
                    <FormFieldsTitle title="How many AC Units do you need cleaned?" />
                    <CheckRadioBoxInputWithPrice
                        isTaxApplicable={isTaxApplicable}
                        InputType="radio" name="numberOfUnits"
                        inputValue={numberOfUnitsValue}
                        items={numberOfUnits}
                        onInputChange={this.handleInputChange}
                        validationClasses="radio-required"
                        validationMessage="Please select the no. of units"
                        parentClass="row"
                        childClass="col-6 col-sm-3 mb-3 d-flex" />
                </section>
                {/*oxyclean_element */}
            </React.Fragment>
        )
        //}

    }
    tvSizes(handyPricePlanParams = null, tvSizeParams = ""){
        let handyPricePlan = handyPricePlanParams != null ? handyPricePlanParams : this.state.handyPricePlan;

        let TVpricePlanPrices = typeof handyPricePlan.planDetail != "undefined" ?  handyPricePlan.planDetail.packages : [];

        let planId = typeof handyPricePlan.id != "undefined" ? handyPricePlan.id : 0;
        
        let tvMountSizes = [];

        let i = 1, key_label = "";

        TVpricePlanPrices.find((item) => {
            if (i == 1) {
                key_label = "Below 55 inches";
            } else if (i == 2) {
                key_label = "55 to 65 inches";
            }
            else {
                key_label = "Above 65 inches";
            }
            tvMountSizes.push({
                id: i,
                value: key_label,
                label: key_label,
                price: item.price,
                currentCurrency: currentCurrency,
                planId : planId,
                inches: item.inches,
            })
            i++;
        })
        if(tvSizeParams != ""){
            //below_55
            //above_65
            //55_to_65
            let selectedId = 0;
            if(tvSizeParams == "below_55"){
                selectedId = 1;
            }else if(tvSizeParams == "above_65"){
                selectedId = 2;
            }else if(tvSizeParams == "55_to_65"){
                selectedId = 3;
            }
            let selectedTvSize = [];
            //console.log("tvSizeParams", selectedId, tvMountSizes);
            if(selectedId !=0 ){
                selectedTvSize = tvMountSizes.filter((item) => {
                    return item.id == selectedId
                })
            }
            return selectedTvSize.length ? selectedTvSize[0] : {};
            
        }
        return tvMountSizes;
    }
    tvMountingOptions(){
        let {isNoonPage} = this.props;

        let {wallMount} = this.state;

        let tvMountSizes = this.tvSizes();

        let mountOpts = [
            { id: 'yes', value: "Yes", label: "Yes" },
            { id: 'no', value: "No", label: "No" }
        ];
        
        let tvMountSizesValue = this.state.tvSize;

        let isTaxApplicable = ( locationHelper.getCurrentCountryId() == UAE_ID && !isNoonPage ) ? true : false;

        let linkText = (<a href=" https://www.noon.com/uae-en/electronics-and-mobiles/television-and-video/television-accessories-16510/tv-mounts-22554" target="_blank" className="text-primary">here</a>)
        let BracketDesc = (typeof wallMount.value != "undefined" && wallMount.value == "No") ? 
            ['You need a TV bracket if your TV is to be wall mounted. The installation does not include one ( you can get one ',linkText,' )'] : '';
        return (<section>
                    <FormFieldsTitle title={isNoonPage ? "Select your TV size" : "Tv Size"} />
                    <CheckRadioBoxInputWithPrice
                        isTaxApplicable={isTaxApplicable}
                        InputType="radio"
                        name="tvSize"
                        inputValue={tvMountSizesValue}
                        items={tvMountSizes}
                        onInputChange={this.handleInputChange}
                        validationClasses="radio-required"
                        parentClass="row"
                        childClass="col-12 col-md-4 mb-3 d-flex"
                        validationMessage="Please select your Tv Size"
                        isNoonPage = {isNoonPage} />
                        {
                            isNoonPage ? (<React.Fragment>
                            <FormTitleDescription title="Do you have a TV Bracket?" desc={BracketDesc} />
                                <CheckRadioBoxInput
                                    InputType="radio"
                                    inputValue={this.state.wallMount}
                                    name="wallMount"
                                    items={mountOpts}
                                    onInputChange={this.handleInputChange}
                                    parentClass="row"
                                    childClass="col-12 col-md-3 mb-3 d-flex"
                                    validationClasses="radio-required"
                                    validationMessage="Please confirm if you have a TV bracket"
                                />
                        </React.Fragment>) : '' }
                        
                </section>
        );
    }
    getPrices(service, sub_service) {
        let lite_weight_prices = this.props.lite_weight_prices;
        if (lite_weight_prices[service]) {
            if (typeof sub_service != "undefined") {
                return lite_weight_prices[service][sub_service];
            } else {
                return lite_weight_prices[service];
            }
        } else {
            return {};
        }
    }
    hoursOptions(service, selectedOpt = false){
        
        let isPlumbingAndElectrician =  this.isPlumbingAndElectrician();

        if(typeof this.state.sub_service.value != "undefined" || isPlumbingAndElectrician){

        let hours_options_elements = "";
        
        let isTaxApplicable = locationHelper.getCurrentCountryId() == UAE_ID ? true : false;

        if(typeof service != "undefined" && service != "") {

            let min_price = 0, per_hour_price = 0, hourly_prices = [];

            let noOfHoursValue = this.state.numberOfHours;

            let subServiceValue = this.state.sub_service;

            let noOfHoursVal = noOfHoursValue;

            let hours_opts = [], moreHourOpts = [], key_val = "", priceValue = 0, optionValue = {}, isHourlyPrice = false, planId = 0;

            if(this.isAcServices(subServiceValue.value)){
                per_hour_price = 0;
                min_price = 0;
                let handyPricePlan = this.state.handyPricePlan;
                let handyPricePlanPrices = typeof handyPricePlan.planDetail != "undefined" ?  handyPricePlan.planDetail : [];
                
                if(typeof handyPricePlanPrices.packages != "undefined"){
                    if(typeof handyPricePlanPrices.packages[0].price_per_hour != "undefined" ){
                        per_hour_price = handyPricePlanPrices.packages[0].price_per_hour
                        min_price = handyPricePlanPrices.min_charges;
                        planId = handyPricePlan.id
                    }else{
                        per_hour_price = handyPricePlanPrices.min_charges;
                        hourly_prices = handyPricePlanPrices.packages;
                        if(hourly_prices.length){ // filtering fractional price, no need to show.
                            hourly_prices = hourly_prices.filter(item => Number.isInteger(item.hours))
                        }
                        planId = handyPricePlan.id;
                    }
                }
            }
            if(typeof hourly_prices != "undefined" && hourly_prices.length){
                
                let i = 0, hourPrice = 0, key = "", not_sure_opt = null;

                hourly_prices.forEach(function (item) {
                    typeof item == ""
                    hourPrice = item.price/item.hours;
                    key = item.hours == 1 ? item.hours + " Hour" : item.hours + " Hours";

                    optionValue =  {
                        id: item.hours, 
                        value: item.hours, 
                        label: key, 
                        hourPrice: item.price, 
                        price: hourPrice,
                        currentCurrency:currentCurrency, 
                        is_hourly_price:true,
                        planId: planId
                    };

                    if(key_val == noOfHoursVal.value){
                        noOfHoursVal = optionValue;
                    }
                    if(i == 0){
                        not_sure_opt = {
                            id: 0, 
                            value: "Not Sure", 
                            label: "Not Sure", 
                            price:per_hour_price,
                            currentCurrency:currentCurrency,
                            is_hourly_price:true
                        };
                        hours_opts.push(not_sure_opt);
                        hours_opts.push(optionValue);
                    }else {
                        if(i <= 3){
                            hours_opts.push(optionValue);
                            if (i == 3) {
                                hours_opts.push({
                                    id: "5-dummy",
                                    value: 'dummy',
                                    label: "5+ hours",
                                    currentCurrency:currentCurrency, 
                                    is_hourly_price:true
                                });
                            }
                        }else{
                            moreHourOpts.push(optionValue)
                        }
                    }
                    i++;
                })
            }else{
                let no_of_hours = 8;
                for (let i = 0; i <= 8; i++) {
                    isHourlyPrice = false;
                    if (i == 0) {
                        key_val = "Not Sure";
                        priceValue = per_hour_price;
                        isHourlyPrice = true;
                    } else if (i == 1) {
                        priceValue = i * per_hour_price;
                        key_val = "1 Hour";
                    } else {
                        priceValue = i * per_hour_price;
                        key_val = i + " Hours";
                    }
                    
                    optionValue = {
                        id: i,
                        value: i,
                        per_hour_price: per_hour_price,
                        label: key_val,
                        currentCurrency: currentCurrency,
                        is_hourly_price: isHourlyPrice
                    };

                    if (planId != 0) {
                        optionValue["planId"] = planId;
                    }

                    if (key_val == noOfHoursVal.value) {
                        noOfHoursVal = optionValue;
                    }
                    if (i <= 4) {
                        hours_opts.push(optionValue);
                        if (i == 4) {
                            hours_opts.push({
                                id: "5-dummy",
                                value: 'dummy',
                                label: "5+ hours",
                                per_hour_price: per_hour_price,
                                currentCurrency: currentCurrency
                            })
                        }
                    } else {
                        moreHourOpts.push(optionValue)
                    }
                }
            }
            let parentClass = "row mb-2 hours-option";
            if (this.state.showMoreHours) {
                hours_opts = hours_opts.concat(moreHourOpts);
                parentClass += typeof noOfHoursValue.value == "undefined" ? " show-more" : "";
            }
            if (selectedOpt == "hours_opts") {
                return hours_opts;
            }
            if (selectedOpt) {
                return noOfHoursVal;
            }
            let label = "Select number of hours for a cost estimate";

            hours_options_elements = typeof this.state.sub_service.value != "undefined" || isPlumbingAndElectrician ? (<section>
                <FormFieldsTitle title={label} />
                <CheckRadioBoxInputWithPrice
                    InputType="radio"
                    name="numberOfHours"
                    inputValue={noOfHoursValue}
                    items={hours_opts}
                    onInputChange={this.handleInputChange}
                    isTaxApplicable={isTaxApplicable}
                    parentClass={parentClass}
                    childClass="col-6 col-md-2 mb-3 d-flex"
                    validationClasses="radio-required"
                    validationMessage="Please select how many hours you need"
                />
            </section>) : "";
        }

            return hours_options_elements;
        }

    }
    isHourlyPriceService(service_id) {
        let service_constants = allServiceConstant;
        let services = [
            service_constants.SERVICE_HANDYMAN_SERVICE,
            service_constants.SERVICE_AC_AND_HEATING,
            service_constants.SERVICE_ELECTRICIAN,
            service_constants.SERVICE_PLUMBING];

        return services.includes(service_id);

    }
    serviceUrlMap(typeOfHandymanValue) {
        let sub_service_url = "";
        let service_constants = allServiceConstant;
        if (typeOfHandymanValue == service_constants.SERVICE_HANDYMAN_SERVICE) {
            sub_service_url = URLConstant.MAINTENANCE_PAGE_URI;
        } else if (typeOfHandymanValue == service_constants.SERVICE_AC_AND_HEATING) {
            sub_service_url = URLConstant.AC_MAINTENANCE_PAGE_URI;
        }
        else if (typeOfHandymanValue == service_constants.SERVICE_ELECTRICIAN) {
            sub_service_url = URLConstant.ELECTRICIAN_PAGE_URI;
        }
        else if (typeOfHandymanValue == service_constants.SERVICE_PLUMBING) {
            sub_service_url = URLConstant.PLUMBING_PAGE_URI;
        }
        return sub_service_url;
    }
    isPlumbing() {
        let typeOfHandymanValue = this.state.typeOfHandyman;
        let service_constants = allServiceConstant;
        return typeOfHandymanValue.value == service_constants.SERVICE_PLUMBING ? true : false;
    }
    isPlumbingAndElectrician( typeOfHandymanValue = {} ){
        typeOfHandymanValue = Object.keys(typeOfHandymanValue).length ? typeOfHandymanValue : this.state.typeOfHandyman;

        let service_constants = allServiceConstant;

        let isPlumbingAndElectrician = typeOfHandymanValue.value == service_constants.SERVICE_ELECTRICIAN || typeOfHandymanValue.value == service_constants.SERVICE_PLUMBING ? true : false;

        return isPlumbingAndElectrician;
    }
    HandymanRequest() {
        let {dataValues, isNoonPage} = this.props;

        let service_constants = allServiceConstant;

        let typeOfHandymanValue = this.state.typeOfHandyman;

        let sub_serviceValue = this.state.sub_service;

        
        //other_services
        let currentStepClass = this.currentStep() === 1 ? '' : "d-none";

        let sub_service_options = this.state.sub_service_options;

        let isACTecnician = typeof typeOfHandymanValue.value != "undefined" && typeOfHandymanValue.value == service_constants.SERVICE_AC_AND_HEATING ? true : false;

        let typeOfHandymanValueLabel = isACTecnician ? "AC Technician" : "handyman";

        let serviceURL = isACTecnician ? URLConstant.AC_MAINTENANCE_PAGE_URI : "handyman";

        let validationMsg = "Please select what type of " + typeOfHandymanValueLabel + " service you needed";

        let sub_service_show = sub_service_options.length != 0 ? (
            <section className={isNoonPage ? 'd-none' : ''}>
                <FormFieldsTitle title={"What do you need the " + typeOfHandymanValueLabel + " for ?"} titleClass="h3 mb-2" />
                <div className="learn-more mb-3">
                    <a href="javascript:void(0)" className="text-primary" onClick={() => this.toggleGreatForPopUp(true, serviceURL)}> (Which option is right for me?) </a>
                </div>
                <CheckRadioBoxInput
                    InputType="radio"
                    name="sub_service"
                    inputValue={this.state.sub_service}
                    items={sub_service_options}
                    onInputChange={this.handleInputChange}
                    childClass="col-12 col-md-3 mb-3 d-flex"
                    validationClasses="radio-required"
                    validationMessage={validationMsg}
                    parentClass="row mb-2"
                />
            </section>
        ) : "";


        let hours_options_show = '';

        let sub_service_options_to_show = "";

        //console.log("sub_serviceValue.value", sub_serviceValue.value);

        if (sub_serviceValue.value != service_constants.SERVICE_TV_MOUNTING && sub_serviceValue.value != service_constants.SERVICE_AC_CLEANING) {
            let sub_service_url = this.serviceUrlMap(typeOfHandymanValue.value);
            sub_service_options_to_show = this.hoursOptions(sub_service_url);
        }
        else if (sub_serviceValue.value == service_constants.SERVICE_TV_MOUNTING) {
            sub_service_options_to_show = this.tvMountingOptions();
        }
        else if (sub_serviceValue.value == service_constants.SERVICE_AC_CLEANING) {
            sub_service_options_to_show = this.acCleaningOptions();
        }
        /*else if(sub_serviceValue.value == service_constants.SERVICE_AC_DUCT_CLEANING){
             console.log("sadsa dasdsa das");
 
             return false;
         }*/

        let isPlumbingAndElectrician = this.isPlumbingAndElectrician();

        let details_section = (<section>
            <FormFieldsTitle title="Tell us about the job, and share any special instructions" />
            <div className="row mb-2">
                <div className="col mb-2">
                    <TextareaInput
                        inputValue={this.state.details}
                        onInputChange={this.handleInputChange} name="details"
                        placeholder="Please describe the job you need to have done."
                        {...(isPlumbingAndElectrician ? { "validationClasses": "required" } : {})}
                    />
                </div>
            </div>
        </section>)

        return (
            <div id="section-request-form" className={currentStepClass}>
                {
                    isNoonPage ? (
                        <section>
                            <div className="row mb-3">
                                <div className="col">
                                    <div className="row m-0 bg-white border rounded">
                                        <div className="col-1 bg-primary">
                                            {/*<img src="https://k.nooncdn.com/s/app/2019/com-www-bigalog/4c2e46ea68a7005cca8be48b6db1d5bc39d5ad48/static/images/noon_logo_black_english.svg" />*/}
                                        </div>
                                        <div className="py-3 col">
                                            <div className="h2 mb-4">Book your complimentary TV installation</div>
                                                <ul className="list-unstyled pl-1">
                                                    <li className="font-weight-bold mb-3">noon.com order: {this.state.orderNo} </li>
                                                    <li>- We have partnered with ServiceMarket.com deliver your TV installation</li>
                                                    <li>- Book your appointment by filling the form below</li>
                                                    <li>- This service is complimentary and <span className="text-warning">free</span></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </section>
                    ) : ''
                }
                <section className={isNoonPage ? 'd-none' : ''}>
                    {this.props.showDiscountNotification(this.state.discountData)} 
                    <FormFieldsTitle title="What type of maintenance service do you need?" titleClass="h3 mb-2" />
                    <div className="learn-more mb-3"><a href="javascript:void(0)" className="text-primary" onClick={() => this.toggleGreatForPopUp(true, URLConstant.MAINTENANCE_PAGE_URI)}> (Which option is right for me?) </a></div>
                    <CheckRadioBoxInput
                        InputType="radio"
                        name="typeOfHandyman"
                        inputValue={typeOfHandymanValue}
                        items={typeOfHandymanOpt}
                        onInputChange={this.handleTypeOfHandymanChange}
                        childClass="col-12 col-md-3 mb-3 d-flex"
                        validationClasses="radio-required"
                        parentClass="row mb-2" />
                </section>
                {
                    (typeOfHandymanValue.value == service_constants.SERVICE_ELECTRICIAN || typeOfHandymanValue.value == service_constants.SERVICE_PLUMBING) && details_section
                }

                {sub_service_show}
                {<div>{sub_service_options_to_show}</div>}
                <section>
                    <FormFieldsTitle title="Pick your start date and time" />
                    <div className="row mb-2">
                        <DatePick
                            inputValue={this.state.booking_date}
                            name="booking_date"
                            selectedDate={this.state.booking_date}
                            disable_friday={true}
                            onInputChange={this.handleInputChange}
                            validationClasses="required"
                            validationMessage="Please select date"
                        />
                        <div className="col-12 col-md-6">
                            <TimePicker
                                inputValue={this.state.booking_time}
                                name="booking_time"
                                disableFirstSlot={true}
                                onInputChange={this.handleCustomChange}
                                validationClasses="required"
                                childClass="mb-3"
                                validationMessage="Please select time"
                                booking_date={this.state.booking_date}
                                startDate={moment(this.props.startDate)}
                            />
                        </div>
                    </div>
                </section>

                {
                    (typeOfHandymanValue.value != service_constants.SERVICE_ELECTRICIAN && typeOfHandymanValue.value != service_constants.SERVICE_PLUMBING) && details_section
                }
                <section>
                    <BookNextStep total={this.state.total} title="Contact Details" moveNext={this.moveNext} toStep="2" setModal={this.mobileSummary} />
                </section>
            </div>
        )
    }
    ContactDetailsWithAllCity(isLocationDetailShow = "yes") {

        var currentStepClass = this.currentStep() === 2 ? '' : "d-none";
        //var currentStepClass =  ''
        var contact_details = {
            input_email: this.state.input_email,
            input_phone: this.state.input_phone,
            input_name: this.state.input_name,
            input_last_name: this.state.input_last_name
        }

        if (isLocationDetailShow) {
            contact_details.input_address_city = this.state.input_address_city;
            contact_details.input_address_area = this.state.input_address_area;
            contact_details.input_address_area_building_name = this.state.input_address_area_building_name;
            contact_details.input_address_area_building_apartment = this.state.input_address_area_building_apartment;
        }

        var autocompleteShow = true;

        var cityOptions = this.props.cityOptions;

        var areasOptions = getAreaOptions(cityOptions, this.state.input_address_city);

        let {userProfile, isNoonPage} = this.props;
        let nextTle = "Select payment method",
            noNextStep = true;

        if(isNoonPage){
            nextTle = "Looking forward to serving you"
            noNextStep = false;
        }

        if (!this.state.loader || (status == "cancel" || status == "decline")) {

            return (
                <div id="personal-information-form" className={currentStepClass}>
                    <ContactDetailsStep
                        total={this.state.total}
                        cityOptions={cityOptions}
                        areasOptions={areasOptions}
                        userProfile={userProfile}
                        signInDetails={this.props.signInDetails}
                        autocompleteShow={autocompleteShow}
                        isLocationDetailShow={isLocationDetailShow}
                        contact_details={contact_details}
                        handleDropDownChange={this.handleInputChange}
                        handleInputChange={this.handleInputChange}
                        mobileSummary={this.mobileSummary}
                        moveNext={this.moveNext}
                        isBooking={true}
                        showDialogToChangeServiceDate={this.state.showDialogToChangeServiceDate}
                        couponValue={this.state.couponCode}
                        handleCouponChange={this.handleCouponChange}
                        isCurrentCityOnly={false}
                        currentCity={this.props.current_city}
                        buttonNextStepText = {nextTle}
                        noNextStep = {noNextStep}
                    />
                </div>
            )
        } else {
            return (
                <main loader={this.state.loader ? "show" : "hide"}><Loader /></main>
            );
        }
    }
    ContactDetails(isLocationDetailShow) {
        let cityOptions = locationHelper.getLocationByName(current_city);
        let currentStepClass = this.currentStep() === 2 ? '' : "d-none";
        //let currentStepClass =  ''
        isLocationDetailShow = typeof isLocationDetailShow != "undefined" ? isLocationDetailShow : "yes";

        let contact_details = {
            input_email: this.state.input_email,
            input_phone: this.state.input_phone,
            input_name: this.state.input_name,
            input_last_name: this.state.input_last_name
        }

        if (isLocationDetailShow) {
            contact_details.input_address_city = this.state.input_address_city;
            contact_details.input_address_area = this.state.input_address_area;
            contact_details.input_address_area_building_name = this.state.input_address_area_building_name;
            contact_details.input_address_area_building_apartment = this.state.input_address_area_building_apartment;
        }

        let {userProfile, isNoonPage} = this.props;
        let nextTle = "Select payment method",
            noNextStep = true;

        if(isNoonPage){
            nextTle = "Looking forward to serving you"
            noNextStep = false;
        }
        
        return (
            <div id="personal-information-form" className={currentStepClass}>
                <ContactDetailsStep
                    cityOptions={cityOptions}
                    userProfile={userProfile}
                    signInDetails={this.props.signInDetails}
                    isLocationDetailShow={isLocationDetailShow}
                    contact_details={contact_details}
                    handleDropDownChange={this.handleInputChange}
                    handleInputChange={this.handleInputChange}
                    mobileSummary={this.mobileSummary}
                    moveNext={this.moveNext}
                    toStep={3}
                    isBooking={true}
                    currentCity={this.props.current_city}
                />
                <section>
                    <BookNextStep 
                        total={this.state.total} 
                        title={nextTle}
                        moveNext={this.moveNext}
                        toStep="3" 
                        noNextStep = {noNextStep}
                        setModal={this.mobileSummary} />
                </section>
            </div>
        )
    }
    PaymentSection() {
        let payment = this.state.payment;
        let currentStepClass = this.currentStep() === 3 ? '' : "d-none";
        //let currentStepClass ='';
        return (
            <div id="payment-method-form" className={currentStepClass}>
                <section className="payment">
                    <PaymentMethods
                        isCreditCardCouponApplied={this.props.isCreditCardCouponApplied}
                        name="payment"
                        inputValue={payment}
                        status={status}
                        tt={tt}
                        onInputChange={this.handleInputChange} />
                </section>
                <section>
                    <BookNextStep total={this.state.total} title="Looking forward to serving you" noNextStep={false} moveNext={this.moveNext} setModal={this.mobileSummary} />
                </section>
            </div>
        );
    }
    mobileSummary(boolVal) {
        this.setState({
            showMobileSummary: boolVal
        })
    }
    componentDidUpdate(prevProps, prevState) {
        let { voucherCode, price, typeOfHandyman, sub_service, tvSize, numberOfHours, numberOfUnits, homeType, booking_date, input_email, payment,walletUsed } = this.state;
        let city = current_city;
        let prices = this.calculatePrice();

        //console.log("componentDidUpdate", prices);

        let new_price = this.state.price;
        
        if ( prevState.total != prices.total || 
            prevState.isHourlyRate != prices.isHourlyRate || 
            prevState.walletUsed != prices.walletUsed ) {
            this.setState({
                price: prices.subtotal,
                total: prices.total,
                subtotal: prices.subtotal,
                vat: prices.vat,
                isHourlyRate: prices.isHourlyRate,
                walletUsed: prices.walletUsed
            })
            new_price = prices.subtotal;
        }
        //&& (voucherCode != "")
        if ((
            (prevState.typeOfHandyman !== typeOfHandyman ||
                prevState.sub_service !== sub_service ||
                prevState.numberOfUnits !== numberOfUnits ||
                prevState.numberOfHours !== numberOfHours ||
                prevState.tvSize !== tvSize ||
                prevState.homeType !== homeType ||
                prevState.voucherCode !== voucherCode ||
                prevState.booking_date !== booking_date) ||
                prevState.payment !== payment ||
                //prevState.walletUsed !== walletUsed ||
            (commonHelper.isUserLoggedIn() && prevState.input_email !== input_email)
        ) ) {
            let prices = this.calculatePrice(true);
            new_price = prices.subtotal;
            this.handleVoucherChange(voucherCode, new_price);
        }
    }
    componentWillReceiveProps(newProps) {
        let {isNoonPage} = this.props;
        let {sub_service, tvSizeParams} = this.state;
        if (status == null) {
            if ((newProps.isCreditCardCouponApplied !== this.props.isCreditCardCouponApplied) && newProps.isCreditCardCouponApplied) {
                this.setState({
                    couponCode: ""
                });
                //this.applyCreditCardCoupon();
            }
            if ((newProps.formCurrentStep == 3) && (newProps.formCurrentStep !== this.props.formCurrentStep)) {
                let payment = this.state.payment;
                if (payment == "") {
                    payment = "credit";
                    this.setState({
                        payment: payment
                    });
                    this.applyCreditCardCoupon(payment);
                }
            }
            if (newProps.setUserArea !== this.props.setUserArea) {
                this.setState({
                    input_address_area: newProps.setUserArea
                })
            }
            if ((newProps.pricePlan !== this.props.pricePlan)) {
                if(( isNoonPage && typeof sub_service.value != "undefined" ) && tvSizeParams != ""){
                    let tvSize = this.tvSizes(newProps.pricePlan, tvSizeParams);
                    this.setState({
                        handyPricePlan: newProps.pricePlan,
                        tvSize: tvSize
                    });
                }else{
                    this.setState({
                        handyPricePlan: newProps.pricePlan
                    });
                }
                //this.pestPriceOptions( newProps.pricePlan,this.state.typeOfPest, this.state.homeType );
                //this.applyCreditCardCoupon();
            }
            if (newProps.softSafeBookingId !== this.props.softSafeBookingId) {
                var payment = this.state.payment;
                if (payment == "") {
                    payment = "credit";
                }
                this.setState({
                    softSafeBookingId: newProps.softSafeBookingId,
                    softSafeRequestId: newProps.softSafeRequestId
                })
                //Credit card coupon
                this.applyCreditCardCoupon(payment);

            }
        }
    }
    applyCreditCardCoupon(value) {
        var returnData = this.props.applyCreditCardCoupon(value, this.state.voucherCode);
        var {voucherCode} = this.state;
        var applyCreditCardCoupon = false;
        var payment = typeof value != "undefined" ? value : this.state.payment;
        if (this.state.payment == "") {
            this.setState({
                payment: 'credit',
            });
            applyCreditCardCoupon = true
        } else {
            applyCreditCardCoupon = (payment != "" && payment == "credit") ? true : false;
        }
        if (this.props.isCreditCardCouponApplied && (voucherCode != "" && voucherCode == CREDITCARD_COUPON)) {
            this.setState({
                voucherCode: ""
            });
        } else {
            if (!this.props.isCreditCardCouponApplied && ((applyCreditCardCoupon && payment == "credit") && (voucherCode == "" || voucherCode == CREDITCARD_COUPON))) {
                if (voucherCode == "") {
                    this.setState({
                        voucherCode: CREDITCARD_COUPON
                    });
                }
            } else {
                if (payment == "cash" && voucherCode == CREDITCARD_COUPON) {
                    this.setState({
                        voucherCode: ""
                    });
                }
            }
        }
    }
    oxyCleanPopUp() {
        return (<ul className="oxyclean-ul">
            <li>OxyClean <sup>TM</sup> is a method of disinfecting your air conditioning and circulation system through the use of steam and a bio-disinfectant, which improves the overall quality of the air you breathe.</li>
            <li>The treatment is chemical-free, non-toxic and environment-friendly and does not require you to leave the home.</li>
            <li>It is recommended if you would like to get rid of bacteria, fungi and other germs from your AC.</li>
        </ul>);
    }
    render() {
        const items = this.sumarryItems();
        const showPromo = true;
        const showPrice = true;

        let {total,vat} = this.state;

        let {discountData, booking_date, isHourlyRate, walletUsed, numberOfHours, voucherLoader} = this.state;

        let isLoading = this.state.loader || this.props.loader;

        const { sub_service } = this.state;
        let defaultSubServiceOption = this.props.defaultSubServiceOption != null ? this.props.defaultSubServiceOption : "";
        const defaultServiceOption = this.props.defaultServiceOption != null ? this.props.defaultServiceOption : "";
        
        let isCoreService = true;
        let {userProfile, isWalletAvailable, isNoonPage} = this.props;

        let userWalletAmount = isWalletAvailable ? userProfile.userWallet.totalAmount : 0;

        let bookingSummary = <NewBookingSummary
            items={items}
            numberOfHours = {numberOfHours}
            isHourlyRate={isHourlyRate}
            showPromo={showPromo}
            showPrice={showPrice}
            total = {total}
            vat = {vat}
            couponValue={this.state.voucherCode}
            booking_date={booking_date}
            discountData={discountData}
            handleCouponChange={this.handleInputChange}
            showMobileSummary={this.state.showMobileSummary}
            setModal={this.mobileSummary}
            isLW={true}
            walletAvailable={isWalletAvailable}
            walletUsed = {walletUsed}
            userWalletAmount = {userWalletAmount}
            isNoonPage = {isNoonPage}
            voucherLoader = {voucherLoader}
        />;

        let serviceURL = this.state.popupURL != "" ? this.state.popupURL : URLConstant.MAINTENANCE_PAGE_URI;

        let greatFor = serviceURL != "oxy-clean" ? this.greatForPopUp(serviceURL) : this.oxyCleanPopUp();

        let greatForTitle = serviceURL != "oxy-clean" ? "Which option is right for me?" : ["OxyClean", <sup>TM</sup>];

        if (isLoading) {
            return (
                <React.Fragment>
                    <div className="col-lg-8">
                        <main loader={isLoading ? "show" : "hide"}><Loader /></main>
                    </div>
                    <div className="col-md-3 ml-auto">
                        {bookingSummary}
                    </div>
                </React.Fragment>
            );

        } else {
            return (
                <React.Fragment>
                    <div className="col-lg-8">
                        {isWalletAvailable ?
                            <div className='wallet-div'>
                                <img src={"../../../../dist/images/wallet-filled-money-tool-light.png"} className="mh-100" height="28" alt="" />
                                <span className='margin-left30' style={{verticalAlign: 'middle',fontSize: '15px'}}>{stringConstants.YOU_HAVE_TXT}
                                    <span className='wallet-amount-span'>{this.props.userProfile.userWallet.currency.code + ' ' + this.props.userProfile.userWallet.totalAmount} </span>{stringConstants.WALLET_BALANCE_TXT}
                                </span>
                            </div>
                            : null
                        }
                        {this.HandymanRequest()}
                        { isNoonPage ? this.ContactDetailsWithAllCity() : this.ContactDetails()}
                        { !isNoonPage ? this.PaymentSection() : ''}
                    </div>
                    <div className="col-md-3 ml-auto">
                        {bookingSummary}
                    </div>
                    {this.state.showDialogToGreatFor &&
                        <GeneralModal modalSize="modal-lg" title={greatForTitle} modalBody={greatFor}
                            setModal={this.toggleGreatForPopUp} />}
                </React.Fragment>
            )
        }
    }
}
function mapStateToProps(state) {
    return {
        myCreditCardsData: state.myCreditCardsData,
        showLoginMenu: state.showLoginMenu,
        currentCityID: state.currentCityID,
        pricePlan: state.PricePlan,
        currentCity: state.currentCity,
        lang: state.lang
    }
}
export default withCookies(connect(mapStateToProps)(BookHandymanPage));