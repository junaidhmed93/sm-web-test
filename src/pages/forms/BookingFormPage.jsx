import React from "react";
import Loader from "../../components/Loader";
import Loadable from 'react-loadable';
const loading = () => (<Loader />);
const BookCleaningPage = Loadable({
    loader: () => import('./booking/BookCleaningPage'),
    loading
});
const BookPoolCleaningPage = Loadable({
    loader: () => import('./booking/BookPoolCleaningPage'),
    loading
});
const BookWindowCleaningPage = Loadable({
    loader: () => import('./booking/BookWindowCleaningPage'),
    loading
});
const BookDeepCleaningPage = Loadable({
    loader: () => import('./booking/BookDeepCleaningPage'),
    loading
});

const BookMattressCleaningPage = Loadable({
    loader: () => import('./booking/BookMattressCleaningPage'),
    loading
});
const BookCarpetCleaningPage = Loadable({
    loader: () => import('./booking/BookCarpetCleaningPage'),
    loading
});
const BookSofaCleaningPage = Loadable({
    loader: () => import('./booking/BookSofaCleaningPage'),
    loading
});
const BookWaterTankCleaningPage = Loadable({
    loader: () => import('./booking/BookWaterTankCleaningPage'),
    loading
});
const BookDryCleaningPage = Loadable({
    loader: () => import('./booking/BookDryCleaningPage'),
    loading
});
const BookPaintingPage = Loadable({
    loader: () => import('./booking/BookPaintingPage'),
    loading
});
const BookPestControlPage = Loadable({
    loader: () => import('./booking/BookPestControlPage'),
    loading
});
const BookHandymanPage = Loadable({
    loader: () => import('./booking/BookHandymanPage'),
    loading
});
const BookMovingPage = Loadable({
    loader: () => import('./booking/BookMovingPage'),
    loading
});
//import MovingFormPage from "../MovingFormPage"
import TrackerNav from "../../components/TrackerNav";
import BookingVoucherify from "./booking/BookingVoucherify";
import BookingCoupons from "./booking/BookingCoupons";
import TaxCalculator from "./booking/TaxCalculator";
import {
    toggleLoginModal,
    fetchCitiesAreas,
    getCityOptions,
    isValidSection,
    scrollToTop,
    fetchUserProfile,
    putUpdateCustomerProfile,
    getAllCreditCard,
    fetchSignIn,
    fetchSignUp,
    setSignIn,
    registerCreditCard,
    postLWRequest,
    postRequest,
    activateBookingRequest,
    deleteLWRequest,
    fetchDateTimeAvailability,
    saveLocalStorage,
    fetchDataDictionaryValues,
    fetchDataConstantValues,
    fetchServices,
    fetchAllCities,
    OXYCLEAN_PRICE, CREDITCARD_COUPON, isLiteWeightBooking, UAE_ID, DOHA_ID, QATAR_ID, isMobile, fetchDateTime, SHARJAH_ID, SHARJAH_MAPPED_ID, fetchZohoMigratedService, postCreateBookingRequestDto, URLCONSTANT, DOHA, DOHA_CITY_ID
} from "../../actions/index";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import locationHelper from "../../helpers/locationHelper";
import commonHelper from "../../helpers/commonHelper";

import { fetchBookingPricingPlan, fetchTaxPlan } from "../../actions/index";
import { withCookies } from "react-cookie";
import moment from 'moment';
import tz from 'moment-timezone';
import { setBodyClass } from "../../actions";
var status = null;
var bid = null;
var tt = null;
let current_currency = "AED";
let current_city = "dubai";
class BookingFormPage extends React.Component {
    constructor(props) {
        super(props);
        this.getCookieFormData = this.getCookieFormData.bind(this);

        let pathLocation = props.pathLocation;

        let softSafeRequestId = "";

        let softSafeBookingId = "";

        if (typeof pathLocation.search != "undefined" && pathLocation.search != "") {
            let cookieData = this.getCookieFormData();

            status = commonHelper.getParameterByName("status", pathLocation.search);

            if (Object.keys(cookieData).length && status != null) { // restore from cookies

                var bookingData = cookieData;

                softSafeRequestId = cookieData.softSafeRequestId;

                softSafeBookingId = cookieData.softSafeBookingId;

                console.log("BookingFormPage componentDidMount", softSafeRequestId, softSafeBookingId);
            }
        }

        this.state = {
            currentStep: 1,
            loading: this.needToFetchData(props),
            loader: false,
            softSafeBookingId: softSafeRequestId,
            softSafeRequestId: softSafeBookingId,
            discountData: '',
            discountPrices: {},
            setUserArea: "",
            startDate: props.dateAndTime,
            isCreditCardCouponApplied: false
        };


        current_city = this.props.currentCity;

        this.currentStep = this.currentStep.bind(this);
        this.moveNext = this.moveNext.bind(this);
        this.moveNextStep = this.moveNextStep.bind(this);
        this.updateState = this.updateState.bind(this);
        this.getLiteWeightServicesPrices = this.getLiteWeightServicesPrices.bind(this);
        this.showLoginModal = this.showLoginModal.bind(this);
        this.getCleanPricingPlan = this.getCleanPricingPlan.bind(this);
        this.setUserLogedIn = this.setUserLogedIn.bind(this);
        this.signUpThenSignIn = this.signUpThenSignIn.bind(this);
        this.fetchDataIfNeeded = this.fetchDataIfNeeded.bind(this);
        this.submitData = this.submitData.bind(this);
        this.generateBookingJSON = this.generateBookingJSON.bind(this);
        this.submitSoftBooking = this.submitSoftBooking.bind(this);
        this.updateUserProfileAndGoToPayment = this.updateUserProfileAndGoToPayment.bind(this);
        this.activatBooking = this.activatBooking.bind(this);
        this.handleCouponChange = this.handleCouponChange.bind(this);
        this.generateCouponRules = this.generateCouponRules.bind(this);
        this.saveMovingSoftBooking = this.saveMovingSoftBooking.bind(this);
        this.generateMovingData = this.generateMovingData.bind(this);
        this.saveCookies = this.saveCookies.bind(this);
        this.pestControlSubServiceUrlMap = this.pestControlSubServiceUrlMap.bind(this);
        this.handymanSubServiceUrlMap = this.handymanSubServiceUrlMap.bind(this);
        this.applyCreditCardCoupon = this.applyCreditCardCoupon.bind(this);
        this.getSubmitData = this.getSubmitData.bind(this);
        this.removeCookie = this.removeCookie.bind(this);
        this.updateCreditCardCouponApplied = this.updateCreditCardCouponApplied.bind(this);
        this.getServiceCode = this.getServiceCode.bind(this);
        this.generateNewBookingJSON = this.generateNewBookingJSON.bind(this);
        this.pestControlbookingJson = this.pestControlbookingJson.bind(this);
        this.fetchPrices = this.fetchPrices.bind(this);
        this.isNewBookingEngService = this.isNewBookingEngService.bind(this);
        this.isVoucherSuccess = this.isVoucherSuccess.bind(this);
        this.voucherRules = this.voucherRules.bind(this);
        this.showDiscountNotification = this.showDiscountNotification.bind(this);
        this.isWalletAvailable = this.isWalletAvailable.bind(this);
        this.noPaymentStep  = this.noPaymentStep.bind(this);
        this.priceAfterWallet  = this.priceAfterWallet.bind(this);
        this.fetchDataIfNeeded(props);
    }
    componentWillUnmount() {
        this._mounted = false;
    }
    fetchPrices(service_id = 0, city_id, isSubscription = false, returnPromise = false) {

        city_id = typeof city_id != "undefined" ? city_id : this.props.currentCityID;

        if (service_id != 0) {
            const pricePlan = this.props.fetchBookingPricingPlan(service_id, city_id, isSubscription);
            if (returnPromise) {
                return pricePlan;
            } else {
                let promiseFetch = [pricePlan];
                Promise.all(promiseFetch).then(() => {
                    this.setState({
                        loading: false
                    })
                }).catch(function (error) {
                    console.log("error fetchPrices");
                });
            }
        }
    }
    fetchDataIfNeeded(props) {
        //if(this.state.loading){
        const url_constants = URLCONSTANT;
        const city = props.url_params.params.city;
        const slug = props.url_params.params.slug;
        var isSubscription = typeof props.url_params.params.subscription != "undefined" ? true : false;
        const service_constants = props.service_constants;
        let lang = props.lang;
        var service_id = 0;
        //locationHelper
        if (slug == url_constants.POOL_CLEANING_PAGE_URI) {
            service_id = service_constants.SERVICE_POOL_CLEANING_SERVICE;
        }
        else if (slug == url_constants.CLEANING_MAID_PAGE_URI) {
            service_id = service_constants.SERVICE_HOME_CLEANING;
        }
        else if (slug == url_constants.WINDOW_CLEANING_PAGE_URI) {
            service_id = service_constants.SERVICE_WINDOW_CLEANING;
        }
        else if (slug == url_constants.WATER_TANK_CLEANING_PAGE_URI) {
            service_id = service_constants.SERVICE_WATER_TANK_CLEANING;
        }
        else if (slug == url_constants.SOFA_AND_UPHOLSTERY_CLEANING_PAGE_URI) { // Sofa Cleaning
            service_id = service_constants.SERVICE_SOFA_CLEANING;
        }
        else if (slug == url_constants.MATTRESS_CLEANING_PAGE_URI) { // mattress Cleaning
            service_id = service_constants.SERVICE_MATTRESS_CLEANING;
        }
        else if (slug == url_constants.CARPET_CLEANING_PAGE_URI) { // carpet Cleaning
            service_id = service_constants.SERVICE_CARPET_CLEANING;
        }else if (slug == url_constants.NOON_TV_INSTALLATION_PAGE_URI) { // NOON TV Mounting
            service_id = service_constants.SERVICE_TV_MOUNTING;
        }
        else if (slug == url_constants.LAUNDRY_DRY_CLEANING) { // Dry Cleaning
            service_id = service_constants.SERVICE_LAUNDRY_AND_DRY_CLEANING;
        }

        var city_id = props.currentCityID; //locationHelper.getCurrentCityId();
        //this.saveCookies('check', {});
        if (typeof city_id != "undefined") {
            var promiseFetch = [];
            const citiesPromise =  this.props.fetchAllCities(city, lang, false);
            promiseFetch.push(citiesPromise);
            const dataConstants = this.props.fetchDataDictionaryValues(lang);
            promiseFetch.push(dataConstants);
            const newDataConstants = this.props.fetchDataConstantValues(lang);
            promiseFetch.push(newDataConstants);
            const servicesPromise = this.props.fetchServices(city,lang);
            promiseFetch.push(servicesPromise);
            const taxPlan = this.props.fetchTaxPlan(city_id);
            promiseFetch.push(taxPlan);
            if (service_id != 0) {
                const pricePlan = this.fetchPrices(service_id, city_id, isSubscription, true);
                promiseFetch.push(pricePlan);
            }
            const zohoMigratedService = this.props.fetchZohoMigratedService(lang);
            promiseFetch.push(zohoMigratedService);
            const DateTimeAvailability = this.props.fetchDateTimeAvailability('',lang);
            promiseFetch.push(DateTimeAvailability);
            const currentDateTime = this.props.fetchDateTime();
            promiseFetch.push(currentDateTime);  
            Promise.all(promiseFetch).then(() => {
                this.setState({
                    loading: false
                })
            }).catch(function (error) {
                console.log("Error fetchDataIfNeeded", error);
            });
        }
        //}
    }
    getCleanPricingPlan() {
        if (this.state && this.state.extras && this.state.extras.PricePlan) {
            return this.state.extras.PricePlan;
        }
        return {}
    }
    needToFetchData(props) {
        const slug = props.url_params.params.slug;
        const url_constants = URLCONSTANT;
        return (slug == url_constants.POOL_CLEANING_PAGE_URI || slug == url_constants.CLEANING_MAID_PAGE_URI
            || slug == url_constants.WINDOW_CLEANING_PAGE_URI);
    }
    signUpThenSignIn(email, firstname, lastname, password) {

        // this.updateUserProfileAndGoToPayment(true);
        let {lang} = this.props;
        if (password == '' || !password || password.length == 0) password = Math.random().toString(36).slice(-8);
        return fetchSignUp(email, firstname, lastname, password, lang)
            .then((result) => {
                // console.log(result);
                return fetchSignIn(email, password).then((res) => {
                    // console.log(res);
                    this.setUserLogedIn(res);
                });
            });
    }
    setUserLogedIn(details) {
        if ( (typeof details.data !="undefined" && typeof details.data.access_token !="undefined" ) && 
             ( details.data.access_token && details.data.access_token.length > 0)) {
         //if (details.data.access_token && details.data.access_token.length > 0) {
            this.props.setSignIn(details.data);
            const { cookies } = this.props;
            cookies.set('_user_details', JSON.stringify(details.data), { path: '/', maxAge: details.data.expires_in });
        }
    }
    showDiscountNotification(discount) {
        let discountData = typeof discount != "undefined" ? discount : this.state.discountData;
        let showNotification = (typeof discountData.success != "undefined" && discountData.success) ? true : false;
        //console.log("discountData", discountData);
        /*discountData.description*/
        return showNotification ? (<div className="alert alert-success">Congratulations! Your discount code has been applied</div>) : "";
    }
    componentDidMount() {
        this.props.setBodyClass('booking');

        current_currency = locationHelper.getCurrentCurrency();

        status = commonHelper.getParameterByName("status");
        bid = commonHelper.getParameterByName("bid");
        tt = commonHelper.getParameterByName("tt");

        /*console.log("status 1234213", status);

        var cookieData = this.getCookieFormData();

        console.log("cookieData 123412", cookieData);

        if(Object.keys(cookieData).length && status != null ){ // restore from cookies

            var bookingData = cookieData;

            var softSafeRequestId = cookieData.softSafeRequestId;

            var softSafeBookingId = cookieData.softSafeBookingId;

            this.setState({
                softSafeRequestId: softSafeRequestId,  
                softSafeBookingId: softSafeBookingId
            });

            console.log("BookingFormPage componentDidMount", softSafeRequestId, softSafeBookingId);
        }*/


        if (status == null) {
            let hashVal = window.location.hash;
            let step;
            hashVal.length ? step = parseInt(window.location.hash.replace('#', '')) : step = 1;
            if (step == 2 || step == 3) {
                window.location = window.location.pathname;
            }
        } else {
            this.setState({
                loader: true
            })
            var thisVar = this;
            setTimeout(function () {
                thisVar.setState({
                    loader: false
                })
                window.location.hash = 3;
            }, 2000);

        }

        if (this.props.citiesAreas.length == 0) {
            this.props.fetchCitiesAreas();
        }

        var url_params = this.props.url_params.params;

        var serviceURL = url_params.slug;

        var city = url_params.city;

        const url_constants = URLCONSTANT;

        var areaData = serviceURL == url_constants.CLEANING_MAID_PAGE_URI ? this.setUserArea(true) : this.setUserArea();

        this.setState({
            setUserArea: areaData
        })
        this.interval = setInterval(() => commonHelper.getCurrentTime(this), 20000);

    }
    componentDidUpdate(prevProps, prevState) {
        /*if (prevState.startDate !== this.state.startDate){
            console.log("newProps.dateAndTime startDate", this.state.startDate);
        }*/
    }
    componentWillReceiveProps(newProps) {
        let url_constants = URLCONSTANT;
        if (newProps.dataConstants !== this.props.dataConstants) {
            window.REDUX_DATA["dataConstants"] = newProps.dataConstants;
        }
        if (newProps.cities !== this.props.cities) {
            window.REDUX_DATA["cities"] = newProps.cities;
            locationHelper.allCites = newProps.cities;
            var areaData = serviceURL == url_constants.CLEANING_MAID_PAGE_URI ? this.setUserArea(true) : this.setUserArea();
            this.setState({
                setUserArea: areaData
            })
        }
        if (newProps.newDataConstants !== this.props.newDataConstants) {
            window.REDUX_DATA["newDataConstants"] = newProps.newDataConstants;
        }
        
        if (newProps.apiServices !== this.props.apiServices) {
            console.log("newProps.apiServices", newProps.apiServices);
        }
        if (newProps.bookingDateTimeAvailabilityReducer !== this.props.bookingDateTimeAvailabilityReducer) {
            window.REDUX_DATA["bookingDateTimeAvailabilityReducer"] = newProps.bookingDateTimeAvailabilityReducer;
        }
        if (newProps.TaxPlanReducer !== this.props.TaxPlanReducer) {
            var extraServices = typeof this.state.extraServices != "undefined" ? this.state.extraServices : {};
            window.REDUX_DATA["TaxPlanReducer"] = newProps.TaxPlanReducer;
            //extraServices["TaxPlanReducer"] = newProps.PricePlan;
            //console.log("newProps.TaxPlanReducer", extraServices);
        }
        if (newProps.PricePlan !== this.props.PricePlan) {
            var extraServices = typeof this.state.extraServices != "undefined" ? this.state.extraServices : {};
            window.REDUX_DATA["PricePlan"] = newProps.PricePlan;
            extraServices["PricePlan"] = newProps.PricePlan;
            this.updateState(this.currentStep(), false, extraServices);
        }
        if (newProps.dateAndTime !== this.props.dateAndTime) {
            //console.log("dateAndTime", dateAndTime);
            window.REDUX_DATA["dateAndTime"] = newProps.dateAndTime;
            //console.log("this.state.startDate", this.state.startDate, newProps.dateAndTime);
            this.setState({
                startDate: newProps.dateAndTime
            })
        }
        if (newProps.zohoMigratedServices !== this.props.zohoMigratedServices) {
            //console.log("dateAndTime", dateAndTime);
            window.REDUX_DATA["zohoMigratedServices"] = newProps.zohoMigratedServices;
        }

        if (newProps.userProfile !== this.props.userProfile) {
            var url_params = this.props.url_params.params;
            const url_constants = URLCONSTANT;
            var serviceURL = url_params.slug;
            var areaData = serviceURL == url_constants.CLEANING_MAID_PAGE_URI ? this.setUserArea(true, newProps.userProfile) : this.setUserArea(false, newProps.userProfile);
            this.setState({
                setUserArea: areaData
            })
        }
    }
    showLoginModal(guest) {
        this.props.toggleLoginModal({ visibility: true, guest: guest });
    }
    currentStep() {
        return this.state.currentStep;
    }
    moveNext(step) {
        //this.updateState(parseInt(step),this.state.loading)
        this.setState({
            currentStep: step
        });
    }
    updateState(step, loadingState, extras) {
        var newState = { "currentStep": step, 'loading': loadingState, 'extras': extras };
        this.setState(newState);
    }
    getLiteWeightServicesPrices(city, service, sub_service = "") {
        const lite_weight_prices = this.props.lite_weight_prices;
        return sub_service != "" ? lite_weight_prices[city][service][sub_service] : lite_weight_prices[city][service];
    }
    isLiteWeightBookingService() {
        var lite_weight_booking_service = []
    }
    priceAfterWallet(total_price){
        let { userProfile } = this.props; 
        let walletUsed = 0;
        if(this.isWalletAvailable()){
            let walletAmount = userProfile.userWallet.totalAmount;
            if (walletAmount >= total_price) {
                walletUsed = total_price;
                total_price = 0;
            }
            else if (walletAmount < total_price) {
                walletUsed = walletAmount;
                total_price = total_price - walletAmount;
            }
        }
        return {walletUsed:walletUsed,total_price: total_price}
    }
    isWalletAvailable() {
        let currentCurrency = locationHelper.getCurrentCurrency();
        let { userProfile, url_params } = this.props;
        var serviceURL = url_params.params.slug;
        let isCoreService = serviceURL != URLCONSTANT.LOCAL_MOVER_PAGE_URI ? true : false;
        let isWalletAvailable = (isCoreService &&
            ((userProfile && userProfile.userWallet) &&
                (userProfile.userWallet.currency.code === currentCurrency && userProfile.userWallet.totalAmount > 0)
            ))

        return isWalletAvailable;
    }
    form_meta(match, city) {
        const { cookies } = this.props;

        const url_params = this.props.url_params.params;

        const url_constants = URLCONSTANT;

        const currentStep = this.state.currentStep;

        const service_constants = this.props.service_constants;

        const { signInDetails, userProfile, citiesAreas } = this.props;

        const cityOptions = getCityOptions(citiesAreas, city);

        var formData = {};

        var dataConstants = this.props.dataConstants;


        var showLoginMenu = this.props.showLoginMenu;

        const allServiceConstant = this.props.service_constants;
        let isWalletAvailable = this.isWalletAvailable();
        var commonProps = {
            signUpThenSignIn: this.signUpThenSignIn,
            showLoginModal: this.showLoginModal,
            showLoginMenu: showLoginMenu,
            userProfile: userProfile,
            signInDetails: signInDetails,
            url_constants: url_constants,
            formCurrentStep: currentStep,
            moveNext: this.moveNext,
            service_constants: service_constants,
            dataConstants: dataConstants,
            current_city: city,
            url_params: url_params,
            moveNextStep: this.moveNextStep,
            loader: this.state.loader,
            getCookieFormData: this.getCookieFormData,
            removeCookie: this.removeCookie,
            setUserArea: this.state.setUserArea,
            startDate: this.state.startDate,
            isCreditCardCouponApplied: this.state.isCreditCardCouponApplied,
            updateCreditCardCouponApplied: this.updateCreditCardCouponApplied,
            getServiceCode: this.getServiceCode,
            fetchPrices: this.fetchPrices,
            softSafeBookingId: this.state.softSafeBookingId,
            softSafeRequestId: this.state.softSafeRequestId,
            showDiscountNotification: this.showDiscountNotification,
            isWalletAvailable: isWalletAvailable,
            priceAfterWallet: this.priceAfterWallet
        };

        if (match == url_constants.CLEANING_MAID_PAGE_URI) {
            const pricingPlan = this.getCleanPricingPlan();
            var Steps = [
                { id: 'section-request-form', title: 'Your Clean' },
                { id: 'personal-information-form', title: 'Contact Details' },
                { id: 'payment-method-form', title: 'Payment' }
            ];
            formData = {
                Steps: Steps, loadForm: <BookCleaningPage
                    pricePlan={pricingPlan}
                    {...commonProps}
                    discountData={this.state.discountData}
                    discountPrices={this.state.discountPrices}
                    handleCouponChange={this.handleCouponChange}
                />
            }


        }
        else if (match == url_constants.POOL_CLEANING_PAGE_URI) {
            const pricingPlan = this.getCleanPricingPlan();

            var Steps = [
                { id: 'section-request-form', title: 'Your Pool' },
                { id: 'personal-information-form', title: 'Contact Details' },
                { id: 'payment-method-form', title: 'Payment' }

            ];

            formData = {
                Steps: Steps, loadForm: <BookPoolCleaningPage
                    pricingPlan={pricingPlan}
                    discountData={this.state.discountData}
                    discountPrices={this.state.discountPrices}
                    handleCouponChange={this.handleCouponChange}
                    {...commonProps}
                />
            }


        }
        else if (match == url_constants.WINDOW_CLEANING_PAGE_URI) {
            const pricingPlan = this.getCleanPricingPlan();

            var Steps = [
                { id: 'section-request-form', title: 'Your Request' },
                { id: 'personal-information-form', title: 'Contact Details' },
                { id: 'payment-method-form', title: 'Payment' }
            ];
            formData = {
                Steps: Steps, loadForm: <BookWindowCleaningPage
                    pricingPlan={pricingPlan}
                    discountData={this.state.discountData}
                    discountPrices={this.state.discountPrices}
                    handleCouponChange={this.handleCouponChange}
                    {...commonProps}
                />
            }
        }
        else if (match == url_constants.DEEP_CLEANING_PAGE_URI) {

            var Steps = [
                { id: 'section-request-form', title: 'Your Request' },
                { id: 'personal-information-form', title: 'Contact Details' },
                { id: 'payment-method-form', title: 'Payment' }
            ];
            formData = {
                Steps: Steps, loadForm: <BookDeepCleaningPage
                    submitData={this.submitData}
                    applyCreditCardCoupon={this.applyCreditCardCoupon}
                    {...commonProps}
                />
            }
        }
        else if (match == url_constants.MATTRESS_CLEANING_PAGE_URI) {
            const pricingPlan = this.getCleanPricingPlan();
            var Steps = [
                { id: 'section-request-form', title: 'Your Request' },
                { id: 'personal-information-form', title: 'Contact Details' },
                { id: 'payment-method-form', title: 'Payment' }
            ];
            formData = {
                Steps: Steps, loadForm: <BookMattressCleaningPage
                    pricePlan={pricingPlan}
                    submitData={this.submitData}
                    applyCreditCardCoupon={this.applyCreditCardCoupon}
                    {...commonProps}
                />
            }

        }
        else if (match == url_constants.SOFA_AND_UPHOLSTERY_CLEANING_PAGE_URI) {
            const pricingPlan = this.getCleanPricingPlan();
            //console.log("SOFA_AND_UPHOLSTERY_CLEANING_PAGE_URI pricingPlan", pricingPlan);
            var Steps = [
                { id: 'section-request-form', title: 'Your Request' },
                { id: 'personal-information-form', title: 'Contact Details' },
                { id: 'payment-method-form', title: 'Payment' }
            ];

            formData = {
                Steps: Steps, loadForm: <BookSofaCleaningPage
                    pricePlan={pricingPlan}
                    submitData={this.submitData}
                    applyCreditCardCoupon={this.applyCreditCardCoupon}
                    {...commonProps}
                />
            }
        }
        else if (match == url_constants.CARPET_CLEANING_PAGE_URI) {
            const pricingPlan = this.getCleanPricingPlan();
            var Steps = [
                { id: 'section-request-form', title: 'Your Request' },
                { id: 'personal-information-form', title: 'Contact Details' },
                { id: 'payment-method-form', title: 'Payment' }
            ];
            formData = {
                Steps: Steps, loadForm: <BookCarpetCleaningPage
                    pricePlan={pricingPlan}
                    submitData={this.submitData}
                    applyCreditCardCoupon={this.applyCreditCardCoupon}
                    {...commonProps}
                />
            }
        }
        else if (match == url_constants.WATER_TANK_CLEANING_PAGE_URI) {
            const pricingPlan = this.getCleanPricingPlan();
            var Steps = [
                { id: 'section-request-form', title: 'Your Request' },
                { id: 'personal-information-form', title: 'Contact Details' },
                { id: 'payment-method-form', title: 'Payment' }
            ];
            formData = {
                Steps: Steps, loadForm: <BookWaterTankCleaningPage
                    submitData={this.submitData}
                    applyCreditCardCoupon={this.applyCreditCardCoupon}
                    pricePlan={pricingPlan}
                    {...commonProps}
                />
            }
        }
        else if (match == url_constants.PAINTERS_PAGE_URI) {
            var Steps = [
                { id: 'section-request-form', title: 'Your Home' },
                { id: 'personal-information-form', title: 'Contact Details' }
            ];

            formData = {
                Steps: Steps, loadForm: <BookPaintingPage
                    submitData={this.submitData}
                    {...commonProps}
                />
            }
        }
        else if (match == url_constants.PEST_CONTROL_PAGE_URI) {

            var defaultServiceOption = null;

            var parnetServiceId = service_constants.SERVICE_PEST_CONTROL;

            var defaultServiceId = service_constants.SERVICE_COCKROACHES_PEST_CONTROL;

            var servicesOptions = commonHelper.lookupServices('parent_service_id', parnetServiceId);

            //console.log("servicesOptions" , servicesOptions);
            var landingPageServiceId = cookies.get("landingPageServiceId") || "";

            /*if (landingPageServiceId != "") {

                if (typeof service_constants[landingPageServiceId] != "undefined") {
                    defaultServiceId = service_constants[landingPageServiceId];
                }
                cookies.remove("landingPageServiceId", { path: '/' });
            }*/


            if (typeof url_params.subservice != "undefined") {

                var pestSubService = this.pestControlSubServiceUrlMap();

                defaultServiceId = pestSubService[url_params.subservice];
            }



            if (servicesOptions.length) {
                defaultServiceOption = servicesOptions.filter(item => item.value == defaultServiceId);
                if (defaultServiceOption.length) {
                    defaultServiceOption = defaultServiceOption[0];
                    defaultServiceOption["label"] = defaultServiceOption["label"].replace(" (Pest control)", "");
                    defaultServiceOption["label"] = defaultServiceOption["label"].replace(" (pest control)", "");
                    defaultServiceOption["label"] = defaultServiceOption["label"].replace(" (pest control)", "");
                    defaultServiceOption["serviceCode"] = this.getServiceCode(defaultServiceOption.value)
                }
            }
            else {
                defaultServiceOption = { label: '', value: '' };
            }

            //console.log("defaultServiceOption", defaultServiceOption);

            var Steps = [
                { id: 'section-request-form', title: 'Job details' },
                { id: 'personal-information-form', title: 'Contact Details' },
                { id: 'payment-method-form', title: 'Payment' }
            ];


            formData = {
                Steps: Steps, loadForm: <BookPestControlPage
                    submitData={this.submitData}
                    defaultServiceOption={defaultServiceOption}
                    applyCreditCardCoupon={this.applyCreditCardCoupon}
                    {...commonProps}
                />
            }
        }
        else if (
            match == url_constants.MAINTENANCE_PAGE_URI ||
            match == url_constants.AC_MAINTENANCE_PAGE_URI ||
            match == url_constants.PLUMBING_PAGE_URI ||
            match == url_constants.ELECTRICIAN_PAGE_URI ||
            match == url_constants.NOON_TV_INSTALLATION_PAGE_URI
        ) {


            var maintenance_uri = url_constants.MAINTENANCE_PAGE_URI;
            var city_params = city;

            var Steps = [
                { id: 'section-request-form', title: 'Job details' },
                { id: 'personal-information-form', title: 'Contact Details' },
                { id: 'payment-method-form', title: 'Payment' }
            ];

            var defaultSubserviceID = 0;

            var defaultSubServiceOption = "";

            var parnetServiceId = service_constants.SERVICE_MAINTENANCE;

            var defaultServiceId = service_constants.SERVICE_HANDYMAN_SERVICE;

            var servicesOptions = commonHelper.lookupServices('parent_service_id', parnetServiceId);

            let isNoonPage = match == url_constants.NOON_TV_INSTALLATION_PAGE_URI ? true : false;

            if(isNoonPage){
                Steps = [
                    { id: 'section-request-form', title: 'Job details' },
                    { id: 'personal-information-form', title: 'Contact Details' }
                ];
            }

            if (match == url_constants.MAINTENANCE_PAGE_URI || match == url_constants.NOON_TV_INSTALLATION_PAGE_URI ) {
                defaultServiceId = service_constants.SERVICE_HANDYMAN_SERVICE;
            } else if (match == url_constants.AC_MAINTENANCE_PAGE_URI) {
                defaultServiceId = service_constants.SERVICE_AC_AND_HEATING;
            } else if (match == url_constants.PLUMBING_PAGE_URI) {
                defaultServiceId = service_constants.SERVICE_PLUMBING;
            }
            else if (match == url_constants.ELECTRICIAN_PAGE_URI) {
                defaultServiceId = service_constants.SERVICE_ELECTRICIAN;
            }
            //console.log("defaultServiceId", defaultServiceId);

            if (servicesOptions.length) {
                defaultServiceOption = servicesOptions.filter(item => item.value == defaultServiceId);
                if (defaultServiceOption.length) {
                    defaultServiceOption = defaultServiceOption[0];
                    if (defaultServiceId == service_constants.SERVICE_HANDYMAN_SERVICE) {
                        defaultServiceOption["label"] = "General handyman";
                    }
                    if (defaultServiceId == service_constants.SERVICE_AC_AND_HEATING) {
                        defaultServiceOption["label"] = "AC Technician";
                    }
                    defaultServiceOption["serviceCode"] = this.getServiceCode(defaultServiceOption.value)
                }
            }
            else {
                defaultServiceOption = { label: '', value: '' };
            }
            //console.log("defaultServiceOption", defaultServiceOption);

            if (typeof url_params.subservice != "undefined") {

                var handymanSunServices = this.handymanSubServiceUrlMap();

                defaultSubserviceID = handymanSunServices[url_params.subservice];

                var subServiceOpt = servicesOptions.filter(item => item.value == defaultSubserviceID);

                defaultSubServiceOption = subServiceOpt.length ? subServiceOpt[0] : "";
                //console.log("defaultSubServiceOption",defaultSubServiceOption);
            }

            formData = {
                Steps: Steps, loadForm: <BookHandymanPage
                    submitData={this.submitData}
                    match={match}
                    defaultServiceOption={defaultServiceOption}
                    defaultSubServiceOption={defaultSubServiceOption}
                    applyCreditCardCoupon={this.applyCreditCardCoupon}
                    isNoonPage = {isNoonPage} 
                    {...commonProps}
                />
            }
        }
        else if (match == url_constants.LOCAL_MOVER_PAGE_URI) {
            /*var transportation_charges = this.props.lite_weight_prices["local-moving"]["transportation_charges"];
            var full_move_package = this.props.lite_weight_prices["local-moving"]["full"];
            var small_move_package = this.props.lite_weight_prices["local-moving"]["small"];*/
            var Steps = [
                { id: 'section-request-form', title: 'Move details' },
                { id: 'personal-information-form', title: 'Contact Details' },
                { id: 'section-moving-package', title: 'Package' }
            ];
            formData = {
                Steps: Steps,
                loadForm: <BookMovingPage
                    submitData={this.submitData}
                    match={match}
                    /*full_move_package={full_move_package}
                    small_move_package={small_move_package}
                    transportation_charges={transportation_charges}*/
                    softSafeBookingId={this.state.softSafeBookingId}
                    applyCreditCardCoupon={this.applyCreditCardCoupon}
                    {...commonProps}
                />


            }
        }else if (match == url_constants.LAUNDRY_DRY_CLEANING) {
            const pricingPlan = this.getCleanPricingPlan();
            var Steps = [
                { id: 'section-request-form', title: 'Your Clean' },
                { id: 'personal-information-form', title: 'Contact Details' },
                { id: 'payment-method-form', title: 'Payment' }
            ];
            formData = {
                Steps: Steps, loadForm: <BookDryCleaningPage
                    submitData={this.submitData}
                    applyCreditCardCoupon={this.applyCreditCardCoupon}
                    PricePlan={pricingPlan}
                    {...commonProps}
                />
            }
        }

        return formData;
    }
    getSubmitData(serviceURL, stateData, redemptionData = {}) {
        let data = null;

        let URLConstant = URLCONSTANT;
        //console.log('serviceURL', serviceURL);

        let service_constants = this.props;

        if (typeof stateData.typeOfHandyman != "undefined") {
            data = this.generateNewBookingJSON(stateData);
        }
        else if (serviceURL == URLConstant.PEST_CONTROL_PAGE_URI) {
            data = this.generateNewBookingJSON(stateData);
            //data = this.generatePestControlData(stateData, redemptionData);
        }
        else if (serviceURL == URLConstant.DEEP_CLEANING_PAGE_URI) {
            data = this.generateNewBookingJSON(stateData);
        }
        else if (serviceURL == URLConstant.MATTRESS_CLEANING_PAGE_URI) {
            data = this.generateMattressCleaningData(stateData, redemptionData);
            //console.log('generate Mattress Cleaning Data', data)
        }
        else if (serviceURL == URLConstant.SOFA_AND_UPHOLSTERY_CLEANING_PAGE_URI) {
            data = this.generateSofaCleaningData(stateData, redemptionData);
        }
        else if (serviceURL == URLConstant.CARPET_CLEANING_PAGE_URI) {
            data = this.generateCarpetCleaningData(stateData, redemptionData);
        }
        else if (serviceURL == URLConstant.WATER_TANK_CLEANING_PAGE_URI) {
            data = this.generateNewBookingJSON(stateData);
            //data = this.generateWaterTankCleaningData(stateData, redemptionData);
        } else if (
            serviceURL == URLConstant.MAINTENANCE_PAGE_URI ||
            serviceURL == URLConstant.ELECTRICIAN_PAGE_URI ||
            serviceURL == URLConstant.PLUMBING_PAGE_URI ||
            serviceURL == URLConstant.AC_MAINTENANCE_PAGE_URI
        ) {
            data = this.generateHandymanData(stateData, redemptionData);
        }
        return data;
    }
    submitData(stateData, dataSubmit = true) {
        var URLConstant = URLCONSTANT;
        var url_params = this.props.url_params;
        var serviceURL = url_params.params.slug;
        this.setState({
            loader: true
        });
        const { signInDetails, showLoginModal, showLoginMenu } = this.props;

        var confirmationSummary = {
            service: 'Pest Control',
            date: stateData.booking_date,
            name: stateData.input_name + " " + stateData.input_last_name,
            email: stateData.input_email,
            subtotal: stateData.subtotal,
            total: stateData.total,
            vat: stateData.vat,
            payment: stateData.payment,
            walletAmount: stateData.walletUsed
        };
        if (isLiteWeightBooking(serviceURL)) {
            var booking_date = stateData.booking_date;
            //booking_date = booking_date.split('-').reverse().join('-');
            confirmationSummary["date"] = booking_date;
        }

        if (typeof stateData.booking_time != "undefined") {
            confirmationSummary["time"] = stateData.booking_time.label;
        }

        var city_object = stateData.input_address_city;

        if (serviceURL == URLConstant.LOCAL_MOVER_PAGE_URI) {

            var confirmationSummary = {
                service: 'Local Moving',
                date: stateData.booking_date,
                name: stateData.input_name + " " + stateData.input_last_name,
                email: stateData.input_email,
                subtotal: stateData.subtotal,
                total: stateData.total,
                vat: stateData.vat,
                payment: stateData.payment
            };

            stateData["package_options"] = "";

            var data = this.generateMovingData(stateData);

            // console.log(data);

            const { cookies } = this.props;

            this.saveCookies('confirmation_summary', confirmationSummary);

            if (typeof stateData.sub_service_options != "undefined") {
                stateData["sub_service_options"] = "";
            }
            if (typeof stateData.handyPricePlan != "undefined") {
                stateData["handyPricePlan"] = "";
            }
            if (typeof stateData.pestPricePlan != "undefined") {
                stateData["pestPricePlan"] = "";
            }
            if (typeof stateData.deepCleaningPricePlan != "undefined") {
                stateData["deepCleaningPricePlan"] = "";
            }
            if (typeof stateData.pestOptions != "undefined") {
                stateData["pestOptions"] = "";
            }

            this.saveCookies('booking_data', stateData);

            this.saveCookies('submitted_data', data);

            if (dataSubmit) {
                scrollToTop();
                var url = window.location.protocol + '//' + window.location.host + window.location.pathname;
                postLWRequest(JSON.stringify(data)).then((response) => {
                    //console.log(response);
                    var res = response.data;
                    if (res.data[0].code == "SUCCESS") {
                        var rid = res.data[0].details.id;
                        if (this.state.softSafeBookingId != "") {
                            deleteLWRequest(this.state.softSafeBookingId).then((deleteRes) => {
                                // console.log("deleteRes", deleteRes);
                                var deleteRes = deleteRes.data;
                                // console.log("deleteRes after", deleteRes);
                                if (deleteRes.data[0].code == "SUCCESS") {
                                    window.location = url + "/confirmation/" + rid;
                                } else {
                                    window.location = url + "/confirmation/" + rid;
                                }
                            })
                        } else {
                            window.location = url + "/confirmation/" + rid;
                        }
                    } else {
                        this.setState({
                            loader: false
                        })
                    }
                });
            }

        } else {
            //console.log('walletUsed confirm', confirmationSummary.walletAmount)
            
            scrollToTop();
            let addressDetails = commonHelper.getUserAddress(stateData);
            putUpdateCustomerProfile(this.props.signInDetails.access_token,
                this.props.signInDetails.customer.id,
                stateData.input_email,
                stateData.input_name,
                stateData.input_last_name, addressDetails).then(() => {

                    this.props.fetchUserProfile(this.props.signInDetails.access_token).then(res => {

                    });
                    var data = this.getSubmitData(serviceURL, stateData);

                    if (typeof data[0]["voucherRules"] != "undefined" && typeof stateData.sub_service_options != "undefined") {
                        stateData["sub_service_options"] = "";
                    }

                    const { cookies } = this.props;

                    this.saveCookies('confirmation_summary', confirmationSummary);

                    this.saveCookies('booking_data', stateData);

                    this.saveCookies('submitted_data', data);

                    var userToken = commonHelper.getUserToken();

                    if (!dataSubmit) {

                        if (stateData.payment !== 'cash' && !(this.props.myCreditCardsData.data && this.props.myCreditCardsData.data.length)) {
                            var url = window.location.protocol + '//' + window.location.host + "/en/handle-teller-response/";
                            var urlParams = "/null/null/" + current_city + "/" + serviceURL;
                            var cancelUrl = url + 'cancel' + urlParams,
                                declineUrl = url + 'decline' + urlParams,
                                successUrl = url + 'success' + urlParams;
                            if (typeof data[0]["voucherRules"] != "undefined") {
                                var voucherRules = data[0]["voucherRules"];
                                BookingVoucherify.voucherRedeem(voucherRules, userToken).then((voucherRes) => {
                                    if (voucherRes.success) {
                                        var redemptionData = voucherRes.data;

                                        data = this.getSubmitData(serviceURL, stateData, redemptionData);

                                        this.saveCookies('submitted_data', data);

                                        this.props.registerCreditCard(
                                            this.props.signInDetails.access_token,
                                            this.props.signInDetails.customer.id,
                                            cancelUrl,
                                            declineUrl,
                                            successUrl
                                        ).then((res) => {
                                            window.location = this.props.myCreditCardsData.redirect_url;
                                        });
                                    }
                                })
                            } else {
                                this.props.registerCreditCard(
                                    this.props.signInDetails.access_token,
                                    this.props.signInDetails.customer.id,
                                    cancelUrl,
                                    declineUrl,
                                    successUrl
                                ).then((res) => {
                                    window.location = this.props.myCreditCardsData.redirect_url;
                                });
                            }
                        }

                    }
                    if (dataSubmit) {
                        scrollToTop();
                        var url = window.location.protocol + '//' + window.location.host + window.location.pathname;

                        if (typeof data[0]["voucherRules"] != "undefined") {

                            var voucherRules = data[0]["voucherRules"]
                            //console.log("voucherRules", data["voucherRules"]);

                            var userToken = this.props.signInDetails.access_token;

                            BookingVoucherify.voucherRedeem(voucherRules, userToken).then((voucherRes) => {
                                if (voucherRes.success) {
                                    var redemptionData = voucherRes.data;

                                    data = this.getSubmitData(serviceURL, stateData, redemptionData);

                                    this.saveCookies('submitted_data', data);
                                }
                                postLWRequest(JSON.stringify(data)).then((response) => {
                                    // console.log(response);
                                    var res = response.data;
                                    if (res.data[0].code == "SUCCESS") {
                                        var rid = res.data[0].details.id;
                                        window.location = url + "/confirmation/" + rid;
                                    } else {
                                        this.setState({
                                            loader: false
                                        })
                                    }
                                });
                            })
                        } else {
                            postLWRequest(JSON.stringify(data)).then((response) => {
                                // console.log(response);
                                var res = response.data;
                                if (res.data[0].code == "SUCCESS") {
                                    var rid = res.data[0].details.id;
                                    window.location = url + "/confirmation/" + rid;
                                } else {
                                    this.setState({
                                        loader: false
                                    })
                                }
                            });
                        }
                    }
                })
        }
    }
    getUserSelectedArea(stateData, returnText = false){
        /*console.log("stateData.input_address_area", stateData.input_address_area);
        let areaId = (stateData.input_address_area != null && typeof stateData.input_address_area.id != "undefined") ? stateData.input_address_area.id : null;
        return returnText ? stateData.input_address_area : areaId;*/
        return commonHelper.getUserSelectedArea(stateData, returnText);
    }
    getCityID(city_object){
        let cityID = city_object.id;
        if(city_object.id == SHARJAH_ID){
            cityID = SHARJAH_MAPPED_ID;
        }else if(city_object.id == DOHA_ID){
            cityID = DOHA_CITY_ID;
        }

        return cityID;
    }
    generateBookingJSON(stateData, isSoftBooking = true) {

        const allServiceConstant = this.props.service_constants;

        var url_params = this.props.url_params;

        var serviceURL = url_params.params.slug;

        var booking_date = stateData.booking_date;

        var URLConstant = URLCONSTANT;

        booking_date = booking_date.split('-').reverse().join('-');

        var numberOfWeekDays = [];

        var bookingFrequencyDataConstants = this.props.dataConstants["BOOKING_FREQUENCY"];
        var dataValues = commonHelper.processDataValues(bookingFrequencyDataConstants);

        if (typeof stateData.field_service_needed != "undefined" && stateData.field_service_needed.value == dataValues.DATA_VALUE_MULTIPLE_TIMES_WEEK) {
            var field_multiple_times_week = stateData.field_multiple_times_week;
            field_multiple_times_week.map((item) => {
                numberOfWeekDays.push(item.value == 0 ? 7 : item.value);
            });

        } else {
            var booking_day = parseInt(moment(booking_date).format('d'));
            numberOfWeekDays.push(booking_day == 0 ? 7 : booking_day);
        }
        var serviceIds = [];
        var other_services = typeof stateData.other_services != "undefined" ? stateData.other_services : [];
        var extraServices = [];


        var createCustomerProfile = false; //

        var city_object = stateData.input_address_city;

        var area_object = stateData.input_address_area;

        var serviceLocationIdsToAdd = parseInt(city_object.id);

        var couponValidationModel = {};

        var discountData = stateData.discountData;

        var payment_method = "";

        if (stateData.payment === 'credit') {
            payment_method = "CREDIT_CARD"
        }
        else if (stateData.payment === 'cash') {
            payment_method = "CASH_ON_DELIVERY"
        } else {
            payment_method = "BOOKING_PAYMENT_METHOD_EMPTY"
        }
        var isPaymentMethodVerified = false;

        var creditCardId = 0;

        if (!isSoftBooking) {
            isPaymentMethodVerified = (this.props.myCreditCardsData.data && this.props.myCreditCardsData.data.length > 0) || (stateData.payment !== 'credit');
            creditCardId = (this.props.myCreditCardsData.data && this.props.myCreditCardsData.data.length) ? this.props.myCreditCardsData.data[0].id : 0;
        }

        if (serviceURL == URLConstant.PAINTERS_PAGE_URI) {
            isPaymentMethodVerified = true;
            payment_method = "CASH_ON_DELIVERY";
        }
        let cityID = this.getCityID(city_object);
         //city_object.id == SHARJAH_ID ? SHARJAH_MAPPED_ID : city_object.id;
        var data = {
            "householdRequestModel": {
                "isBooking": 1,
                "serviceLocationIdsToAdd": [
                    serviceLocationIdsToAdd
                ],
                "bookingDetails": {
                    "bookingPaymentDetail": {
                        "paymentMethod": payment_method,
                        "creditCardId": creditCardId,
                        "isPaymentMethodVerified": isPaymentMethodVerified
                    },
                    "createCustomerProfile": createCustomerProfile
                },
                "contactInformationModel": {
                    "personName": stateData.input_name + " " + stateData.input_last_name,
                    "personPhone": stateData.input_phone,
                    "personEmail": stateData.input_email
                },
                "requestFrequency": typeof stateData.field_service_needed != "undefined" ? stateData.field_service_needed.value : null,
                "numberOfWeekDays": numberOfWeekDays,
                "address": {
                    "addressLine1": "",
                    "addressLine2": "",
                    "city": cityID,
                    "building": stateData.input_address_area_building_name,
                    "apartment": stateData.input_address_area_building_apartment
                },
                "hoursRequired": null,
                "numberOfWorkers": null,
                "equipmentNeeded": null,
                "serviceIds": null,
                "description": !!stateData.details ? stateData.details : '',
                "serviceRequestTime": (new Date(this.state.startDate)),
                "customerId": this.props.signInDetails.customer.id,
                "couponValidationModel": {},
                "isCouponAttached": false

            }
        };

        if (serviceURL == URLConstant.CLEANING_MAID_PAGE_URI) { // Home Cleaning

            var service_id = parseInt(allServiceConstant.SERVICE_HOME_CLEANING);
            serviceIds.push(service_id);

            if (other_services.length) {
                other_services.map((item, index) => {
                    if (item.value == "ironing") {
                        serviceIds.push(parseInt(allServiceConstant.SERVICE_IRONING_CLEANING_AND_MAID_SERVICES))
                    }
                    if (item.value == "inside-window-cleaning") {
                        serviceIds.push(parseInt(allServiceConstant.SERVICE_INTERIOR_WINDOW_CLEANING_CLEANING_AND_MAID_SERVICES))
                    }
                })
            }

            /*
             "hoursRequired": this.state.hours_required.value,
             "numberOfWorkers": this.state.number_of_cleaners.value,
             "requestFrequency": this.state.field_service_needed.value,
             "equipmentNeeded": this.state.field_own_equipments.value,
             */

            data["householdRequestModel"]["hoursRequired"] = stateData.hours_required.value;
            data["householdRequestModel"]["numberOfWorkers"] = stateData.number_of_cleaners.value;
            data["householdRequestModel"]["equipmentNeeded"] = stateData.field_own_equipments.value;
            //data["householdRequestModel"]["serviceIds"] = serviceIds;
            data["householdRequestModel"]["hoursRequired"] = stateData.field_service_needed.value;

            data["householdRequestModel"]["planId"] = stateData.planId;

            data["householdRequestModel"]["serviceIds"] = serviceIds;

            data["householdRequestModel"]["bookingDetails"]["partTimeCleaningBookingModel"] = {
                "bookingStartDate": booking_date,
                "bookingTime": stateData.booking_time.value,
                "hoursRequired": stateData.hours_required.value,
                "numberOfWorkers": stateData.number_of_cleaners.value,
                "requestFrequency": stateData.field_service_needed.value,
                "equipmentNeeded": stateData.field_own_equipments.value,
                "numberOfWeekDays": numberOfWeekDays,
            };
        } else if (serviceURL == URLConstant.WINDOW_CLEANING_PAGE_URI) {
            var service_id = parseInt(allServiceConstant.SERVICE_WINDOW_CLEANING);

            serviceIds.push(service_id);

            data["householdRequestModel"]["serviceIds"] = serviceIds;

            data["householdRequestModel"]["bookingDetails"]["windowCleaningBookingModel"] = {
                "bookingStartDate": booking_date,
                "requestFrequency": stateData.field_service_needed.value,
                "numberOfWeekDays": numberOfWeekDays,
                "roomSize": stateData.windowSize.value
            };

            data["householdRequestModel"]["planId"] = stateData.planId;

        }
        else if (serviceURL == URLConstant.POOL_CLEANING_PAGE_URI) {
            var service_id = parseInt(allServiceConstant.SERVICE_POOL_CLEANING_SERVICE);

            serviceIds.push(service_id);

            data["householdRequestModel"]["serviceIds"] = serviceIds;

            data["householdRequestModel"]["bookingDetails"]["poolCleaningBookingModel"] = {
                "bookingStartDate": booking_date,
                "requestFrequency": dataValues.DATA_VALUE_MULTIPLE_TIMES_WEEK,
                "numberOfWeekDays": numberOfWeekDays,
                "poolSize": stateData.poolSize.value
            };

            data["householdRequestModel"]["planId"] = stateData.planId;

        }
        else if (serviceURL == URLConstant.PAINTERS_PAGE_URI) {
            var service_id = parseInt(allServiceConstant.SERVICE_HOME_PAINTING);

            var { typeOfPainting,
                homeType,
                numberOfUnits,
                fieldFurnished,
                fieldCeilingsPainted,
                colorToBePainted,
                colorAreNowOnYourWalls,
                additionalRooms,
                noOfCellings } = stateData

            var moreRooms = [];


            if (additionalRooms.length) {
                additionalRooms.map((item) => {
                    moreRooms.push(item.value);
                })
            }


            var homePaintingBookingModel = {
                "typeOfPainting": typeOfPainting,
                "oldWallColor": colorAreNowOnYourWalls.value,
                "newWallColor": colorToBePainted.value,
                "typeOfHome": homeType.value,
                "sizeOfHome": numberOfUnits.value,
                "paintAdditionalRooms": moreRooms.join(", "),
                "needCeilingsPainted": fieldCeilingsPainted.value,
                "numberOfCeilingsToPaint": noOfCellings,
                "homeFurnished": fieldFurnished.value,
                "startDate1": booking_date
            }

            serviceIds.push(service_id);

            data["householdRequestModel"]["hoursRequired"] = "";

            data["householdRequestModel"]["numberOfWorkers"] = 0;

            data["householdRequestModel"]["bookingDetails"]["homePaintingBookingModel"] = homePaintingBookingModel;

            data["householdRequestModel"]["equipmentNeeded"] = "NO";

            data["householdRequestModel"]["serviceIds"] = serviceIds;

        }

        if (discountData.success) {

            couponValidationModel = this.generateCouponRules(stateData.couponCode, stateData); //this.handleCouponChange(stateData.couponCode, true);

            //console.log(couponValidationModel);

            data["householdRequestModel"]["couponValidationModel"] = couponValidationModel;

            data["householdRequestModel"]["isCouponAttached"] = true;
        }
        let userSelectedArea = this.getUserSelectedArea(stateData);
        if(userSelectedArea == null){
            //"area": this.getUserSelectedArea(stateData),
            data['householdRequestModel']['address']['area'] = null;
            data['householdRequestModel']['address']['areaTitle'] = this.getUserSelectedArea(stateData, true);
        }else{
            data['householdRequestModel']['address']['area'] = userSelectedArea;
        }
        
        return data;
    }
    submitSoftBooking(stateData, submitBooking = false) {
        const url_constants = URLCONSTANT;
        const service_constants = this.props;
        const slug = this.props.url_params.params.slug;

        var isNewBookingEngService = this.isNewBookingEngService(stateData) || slug == url_constants.LAUNDRY_DRY_CLEANING;

        //console.log("submitSoftBooking isNewBookingEngService", isNewBookingEngService);
        var data = isNewBookingEngService ? this.generateNewBookingJSON(stateData) : this.generateBookingJSON(stateData);

        this.setState({
            loader: true
        });

        if (isNewBookingEngService) {
            postCreateBookingRequestDto(this.props.signInDetails.access_token || '', JSON.stringify(data)).then((response) => {
                var rows = JSON.parse(response.data.addBookingRequest).data;
                //console.log("postCreateBookingRequestDto");
                var bookingNumber = rows.requestUuid;
                var bookingId = rows.bookingId;
                if (submitBooking) {
                    this.setState({
                        softSafeBookingId: bookingId,
                        softSafeRequestId: bookingNumber
                    })
                    this.updateUserProfileAndGoToPayment(stateData);
                } else {
                    this.setState({
                        softSafeBookingId: bookingId,
                        softSafeRequestId: bookingNumber,
                        loader: false
                    })
                    window.location.hash = 3;
                    scrollToTop();
                }
            })
        } else {
            postRequest(this.props.signInDetails.access_token || '', JSON.stringify(data)).then((response) => {
                var rows = JSON.parse(response.data.addRequest).rows;
                if (!!rows) {
                    var bookingNumber = rows[0].requestUuid;
                    var bookingId = rows[0].bookingId;
                    if (submitBooking) {
                        this.setState({
                            softSafeBookingId: bookingId,
                            softSafeRequestId: bookingNumber
                        })
                        this.updateUserProfileAndGoToPayment(stateData);
                    } else {
                        this.setState({
                            softSafeBookingId: bookingId,
                            softSafeRequestId: bookingNumber,
                            loader: false
                        })
                        window.location.hash = 3;
                        scrollToTop();
                    }
                }
                else {
                    this.setState({
                        loader: false
                    });
                }
            })
        }

    }
    updateUserProfileAndGoToPayment(stateData, withProfile) {
        this.setState({
            loader: true
        })
        scrollToTop();

        var { discountData, payment } = stateData;

        var { softSafeRequestId, softSafeBookingId } = this.state;

        let service_constants = URLCONSTANT;

        var URLConstant = URLCONSTANT;

        var url_params = this.props.url_params;

        var serviceURL = url_params.params.slug;

        var city_object = stateData.input_address_city;

        var url = window.location.protocol + '//' + window.location.host + window.location.pathname;

        if (url.substr(-1) != '/') url += '/';
        
        var city_id = this.getCityID(city_object);//city_object.id == SHARJAH_ID ? SHARJAH_MAPPED_ID : city_object.id;

        var couponValidationModel = "";

        let addressDetails = commonHelper.getUserAddress(stateData);

        putUpdateCustomerProfile(this.props.signInDetails.access_token,
            this.props.signInDetails.customer.id,
            stateData.input_email,
            stateData.input_name,
            stateData.input_last_name, addressDetails).then(() => {
                if (withProfile) {
                    this.getUserProfile(this.props.signInDetails.access_token);
                }
                var isSubscription = false;
                //const isWalletAvailable = this.props.userProfile && this.props.userProfile.userWallet && this.props.userProfile.userWallet.currency.code === current_currency && this.props.userProfile.userWallet.totalAmount > 0 ? true:false;
                //const walletBalance = this.props.userProfile.userWallet.totalAmount;
                //const consumedWallet =0;

                // if (walletBalance >= stateData.total)
                //     consumedWallet = stateData.total;
                //return false;


                var confirmationSummary = {};
                if (this.isNewBookingEngService(stateData)) {
                    couponValidationModel = this.voucherRules(stateData);
                    confirmationSummary = {
                        service: 'Pest Control',
                        date: stateData.booking_date,
                        name: stateData.input_name + " " + stateData.input_last_name,
                        email: stateData.input_email,
                        subtotal: stateData.subtotal,
                        total: stateData.total,
                        vat: stateData.vat,
                        payment: stateData.payment,
                        walletAmount: stateData.walletUsed,//isWalletAvailable? this.props.userProfile.userWallet.totalAmount : 'N/A'
                    };
                    if(!commonHelper.isLocalStorageAvailable()){

                        if (typeof stateData.sub_service_options != "undefined") {
                            stateData["sub_service_options"] = "";
                        }
                        if (typeof stateData.handyPricePlan != "undefined") {
                            stateData["handyPricePlan"] = "";
                        }
                        if (typeof stateData.pestPricePlan != "undefined") {
                            stateData["pestPricePlan"] = "";
                        }
                        if (typeof stateData.deepCleaningPricePlan != "undefined") {
                            stateData["deepCleaningPricePlan"] = "";
                        }
                        if (typeof stateData.pestOptions != "undefined") {
                            stateData["pestOptions"] = "";
                        }
                        if (typeof stateData.wallMount != "undefined") {
                            stateData["wallMount"] = "";
                        }
                        if (typeof stateData.voucherLoader != "undefined") {
                            stateData["voucherLoader"] = "";
                        }
                        if (typeof stateData.tvSizeParams != "undefined") {
                            stateData["tvSizeParams"] = "";
                        }
                        if (typeof stateData.showMobileSummary != "undefined") {
                            stateData["showMobileSummary"] = "";
                        }
                        if (typeof stateData.input_address_area_building_name != "undefined") {
                            stateData["input_address_area_building_name"] = "";
                        }
                        if (typeof stateData.input_address_area_building_apartment != "undefined") {
                            stateData["input_address_area_building_apartment"] = "";
                        }
                        if (typeof stateData.input_address_city != "undefined") {
                            stateData["input_address_city"] = "";
                        }
                        if (typeof stateData.discountPrices != "undefined") {
                            stateData["discountPrices"] = "";
                        }
                    }
                    
                } else {
                    var data = this.generateBookingJSON(stateData, false);
                    couponValidationModel = this.generateCouponRules(stateData.couponCode, stateData);
                    if (serviceURL == URLConstant.CLEANING_MAID_PAGE_URI) {
                        confirmationSummary = {
                            service: 'Home Cleaning',
                            date: data.householdRequestModel.bookingDetails.partTimeCleaningBookingModel.bookingStartDate,
                            time: data.householdRequestModel.bookingDetails.partTimeCleaningBookingModel.bookingTime,
                            name: data.householdRequestModel.contactInformationModel.personName,
                            email: data.householdRequestModel.contactInformationModel.personEmail,
                            subtotal: stateData.subtotal,
                            total: stateData.total,
                            payment: stateData.payment,
                            walletAmount: stateData.walletUsed,//isWalletAvailable? this.props.userProfile.userWallet.totalAmount : 'N/A'

                        };

                    } else if (serviceURL == URLConstant.WINDOW_CLEANING_PAGE_URI) {

                        confirmationSummary = {
                            service: 'Window Cleaning',
                            date: data.householdRequestModel.bookingDetails.windowCleaningBookingModel.bookingStartDate,
                            name: data.householdRequestModel.contactInformationModel.personName,
                            email: data.householdRequestModel.contactInformationModel.personEmail,
                            subtotal: stateData.subtotal,
                            total: stateData.total,
                            payment: stateData.payment

                        };
                    } else if (serviceURL == URLConstant.POOL_CLEANING_PAGE_URI) {

                        confirmationSummary = {
                            service: 'Pool Cleaning',
                            date: data.householdRequestModel.bookingDetails.poolCleaningBookingModel.bookingStartDate,
                            name: data.householdRequestModel.contactInformationModel.personName,
                            email: data.householdRequestModel.contactInformationModel.personEmail,
                            subtotal: stateData.subtotal,
                            total: stateData.total,
                            payment: stateData.payment

                        };
                    }
                    else if (serviceURL == URLConstant.PAINTERS_PAGE_URI) {
                        if (typeof stateData.numberOfUnitsOptions != "undefined") {
                            stateData["numberOfUnitsOptions"] = "";
                        }
                        if (typeof stateData.itemData != "undefined") {
                            stateData["itemData"] = "";
                        }
                        confirmationSummary = {
                            service: 'Home Painting',
                            date: data.householdRequestModel.bookingDetails.homePaintingBookingModel.startDate1,
                            name: data.householdRequestModel.contactInformationModel.personName,
                            email: data.householdRequestModel.contactInformationModel.personEmail,
                            subtotal: stateData.subtotal,
                            total: stateData.total,
                            payment: stateData.payment,
                            walletAmount: stateData.walletUsed,//isWalletAvailable? this.props.userProfile.userWallet.totalAmount : 'N/A'

                        };
                    }
                }

                const { cookies } = this.props;

                this.saveCookies('confirmation_summary', confirmationSummary);

                this.saveCookies('booking_data', stateData);

                /*console.log("booking_data stateData", stateData);

                return false;*/

                if (couponValidationModel != "") {
                    this.saveCookies('coupon_data', couponValidationModel);
                }

                if (stateData.payment !== 'cash' && !(this.props.myCreditCardsData.data && this.props.myCreditCardsData.data.length)) {

                    var url = window.location.protocol + '//' + window.location.host + "/en/handle-teller-response/";

                    var urlParams = "/" + softSafeBookingId + "/" + softSafeRequestId + "/" + current_city + "/" + serviceURL;

                    var cancelUrl = url + 'cancel' + urlParams,
                        declineUrl = url + 'decline' + urlParams,
                        successUrl = url + 'success' + urlParams;
                    this.props.registerCreditCard(
                        this.props.signInDetails.access_token,
                        this.props.signInDetails.customer.id,
                        cancelUrl,
                        declineUrl,
                        successUrl
                    ).then((res) => {
                        //console.log(res, this.props.myCreditCardsData);
                        window.location = this.props.myCreditCardsData.redirect_url;
                    });

                } else {
                    //if (serviceURL == URLConstant.PAINTERS_PAGE_URI) {
                        if (this.noPaymentStep()) {    
                        var url = window.location.protocol + '//' + window.location.host + window.location.pathname;
                        window.location = url + '/confirmation/' + softSafeRequestId;
                    } else {
                        this.activatBooking(stateData);
                    }
                }
            });
    }
    saveCookies(name, data) {

        var url_params = this.props.url_params;

        var serviceURL = url_params.params.slug;

        var currentLanguage = url_params.params.lang;

        var currentCityName = current_city;

        var currentPath = serviceURL;

        var cookieName = currentLanguage + "." + current_city + "." + currentPath + "." + name;

        let isLocalStorageAvailable = commonHelper.isLocalStorageAvailable();
        if(isLocalStorageAvailable){
            localStorage.setItem(cookieName, JSON.stringify(data));
            let localData = {};
            localData[cookieName] = JSON.stringify(data);
            saveLocalStorage(localData);
        }else{
            const { cookies } = this.props;
            var date = new Date();
            date.setTime(date.getTime() + (300 * 1000));
            var expireDate = new Date(date);
            cookies.set(cookieName, JSON.stringify(data), { path: '/', maxAge: 300 });
        }

    }
    removeCookie(name = "") {
        var url_params = this.props.url_params;

        var serviceURL = url_params.params.slug;

        var currentLanguage = url_params.params.lang;

        var currentPath = serviceURL;

        var cookieName = currentLanguage + "." + current_city + "." + currentPath + "." + name;

        const { cookies } = this.props;

        let isLocalStorageAvailable = commonHelper.isLocalStorageAvailable();

        if(isLocalStorageAvailable){
            localStorage.removeItem(cookieName);
        }else{
            cookies.remove(cookieName, { path: '/' });
        }
    }
    getCookieFormData() {
        var url_params = this.props.url_params;

        var serviceURL = url_params.params.slug;

        var currentLanguage = url_params.params.lang;

        var currentCityName = current_city;

        var currentPath = serviceURL;

        var cookieName = currentLanguage + "." + current_city + "." + currentPath + ".booking_data";

        const { cookies } = this.props;
        
        let isLocalStorageAvailable = commonHelper.isLocalStorageAvailable();

        if(isLocalStorageAvailable){
           return localStorage.getItem(cookieName) != null ? JSON.parse(localStorage.getItem(cookieName)) : {};
        }else{
            return cookies.get(cookieName) || {};
        }
        //return isLocalStorageAvailable ? JSON.parse(localStorage.getItem(cookieName)) : cookies.get(cookieName)
        //return cookies.get(cookieName) || {};
    }
    isNewBookingEngService(stateData) {
        let service_constants = this.props.service_constants;

        let url_constants = URLCONSTANT;

        let url_params = this.props.url_params;

        let slug = url_params.params.slug;

        return ((slug == url_constants.PEST_CONTROL_PAGE_URI || slug == url_constants.DEEP_CLEANING_PAGE_URI || slug == url_constants.WATER_TANK_CLEANING_PAGE_URI || slug == url_constants.SOFA_AND_UPHOLSTERY_CLEANING_PAGE_URI || slug == url_constants.CARPET_CLEANING_PAGE_URI || slug == url_constants.MATTRESS_CLEANING_PAGE_URI) || (typeof stateData.typeOfHandyman != "undefined") ||  (typeof stateData.isNewBookingEngine != "undefined" && stateData.isNewBookingEngine) ) ? true : false
    }
    activatBooking(stateData) {
        const service_constants = this.props.service_constants;

        const url_constants = URLCONSTANT;

        var url_params = this.props.url_params;

        var serviceURL = url_params.params.slug;

        var url = window.location.protocol + '//' + window.location.host + window.location.pathname;

        if (url.substr(-1) != '/') url += '/';

        var { discountData, payment, voucherCode } = stateData;

        var { softSafeRequestId, softSafeBookingId } = this.state;

        var paymentMethod = commonHelper.getPaymentMethod(payment);

        var userCreditCardId = (this.props.myCreditCardsData.data && this.props.myCreditCardsData.data.length) ? this.props.myCreditCardsData.data[0].id : 0;

        var creditCardId = paymentMethod == "CREDIT_CARD" ? userCreditCardId : "";

        var couponValidationModel = {};

        var accessToken = this.props.signInDetails.access_token;

        var signedInCustomerId = this.props.signInDetails.customer.id;

        var isNewBookingEngine = false;

        let isNewBookingEngService = this.isNewBookingEngService(stateData);

        if (isNewBookingEngService) {
            isNewBookingEngine = true;
            couponValidationModel = this.voucherRules(stateData);
        } else {
            if (discountData.success) {
                couponValidationModel = this.generateCouponRules(stateData.couponCode, stateData); //this.handleCouponChange(stateData.couponCode, true);
            }
        }
        activateBookingRequest(accessToken, signedInCustomerId, softSafeBookingId, creditCardId, paymentMethod, couponValidationModel, isNewBookingEngine).then((response) => {
            if (response.data.success) {
                window.location = url + 'confirmation/' + softSafeRequestId;
            } else {
                this.setState({
                    loader: false
                })
            }
        });

    }
    noPaymentStep(){
        let {url_params} = this.props;
        let url_constants = URLCONSTANT;
        let slug = url_params.params.slug;
        return (slug == url_constants.PAINTERS_PAGE_URI || slug == url_constants.NOON_TV_INSTALLATION_PAGE_URI)
    }
    moveNextStep(step, stateData, isCoreBooking = false) {
        const url_constants = URLCONSTANT;

        const slug = this.props.url_params.params.slug;

        const allServiceConstant = this.props.service_constants;

        const { signInDetails, showLoginMenu } = this.props;

        var continueAsGuest = typeof showLoginMenu.guest != "undefined" && showLoginMenu.guest == false ? true : false

        var formCurrentStep = this.state.currentStep;

        var valid = true;
        let noPaymentStep = this.noPaymentStep()
        if (formCurrentStep == '1') {
            valid = isValidSection('section-request-form');
            if (valid) {
                window.location.hash = step;
                scrollToTop();
                // Show Login Modal
                if (!signInDetails.customer && !continueAsGuest) {
                    this.showLoginModal(true);
                }
            }
        }
        else if (formCurrentStep == '2') {
            valid = isValidSection('personal-information-form');
            if (valid) {
                if (!signInDetails.customer) {
                    this.setState({
                        loader: true
                    });
                    this.signUpThenSignIn(stateData.input_email, stateData.input_name, stateData.input_last_name, '').then(() => {
                        if (!isCoreBooking) {
                            if (this.state.softSafeBookingId == "" && slug == url_constants.LOCAL_MOVER_PAGE_URI) { // local move soft booking
                                this.saveMovingSoftBooking(stateData);
                            } else {
                                this.setState({
                                    loader: false
                                })
                                window.location.hash = 3;
                                scrollToTop();
                            }

                        } else {
                            if (noPaymentStep) {
                                this.submitSoftBooking(stateData, true);
                            } else {
                                this.submitSoftBooking(stateData);
                            }
                        }

                    });
                } else {
                    scrollToTop();
                    //if (status == null) {
                    this.setState({
                        loader: true
                    });
                    if (noPaymentStep) { // painting submission
                        this.submitSoftBooking(stateData, true);
                    } else {
                        this.props.getAllCreditCard(this.props.signInDetails.access_token).then(() => {
                            if (isCoreBooking) {
                                if (this.state.softSafeBookingId != "") {
                                    this.setState({
                                        loader: false
                                    })
                                    window.location.hash = 3;
                                    scrollToTop();
                                } else {
                                    this.submitSoftBooking(stateData);
                                }
                            } else {
                                if (this.state.softSafeBookingId == "" && slug == url_constants.LOCAL_MOVER_PAGE_URI) { // local move soft booking
                                    this.saveMovingSoftBooking(stateData);
                                } else {
                                    this.setState({
                                        loader: false
                                    })
                                    window.location.hash = 3;
                                    scrollToTop();
                                }
                            }

                        });
                    }
                    //}
                }
            }
        }
        else if (formCurrentStep == '3') {
            //updateUserProfileAndGoToPayment

            var isLocalMovingBooking = slug == url_constants.LOCAL_MOVER_PAGE_URI ? true : false;

            if (stateData.payment !== 'cash' && !(this.props.myCreditCardsData.data && this.props.myCreditCardsData.data.length)) {
                if (!isCoreBooking) {
                    this.submitData(stateData, false);
                } else {
                    this.updateUserProfileAndGoToPayment(stateData, false)
                }
            } else {
                if (!isCoreBooking) {
                    this.submitData(stateData);
                } else {
                    this.updateUserProfileAndGoToPayment(stateData, false)
                }
            }
        }
    }
    saveMovingSoftBooking(stateData) {

        var softBookingData = this.generateMovingData(stateData, true);

        // console.log("saveMovingSoftBooking", softBookingData);

        postLWRequest(JSON.stringify(softBookingData)).then((response) => {
            var res = response.data;
            var rid = "";
            if (res.data[0].code == "SUCCESS") {
                rid = res.data[0].details.id;
            }
            this.setState({
                loader: false,
                softSafeBookingId: rid
            })
            window.location.hash = 3;
            scrollToTop();
        });
    }
    generateMovingData(stateData, isSoftBooking = false) {
        var {
            typeOfMove,
            moving_size,
            from_emirate,
            from_area,
            from_address,
            to_emirate,
            to_area,
            to_address,
            details,
            item_lists,
            input_email,
            input_phone,
            input_name,
            input_last_name,
            input_address_city,
            input_address_area,
            need_handyman,
            booking_date,
            voucherCode,
            packages,
            payment,
            subtotal,
            vat,
            totalAfterDiscount,
            total,
            price
        } = stateData;

        var moveFrom = from_emirate.label + (from_area.length ? ' - ' + from_area : '') + ' - ' + from_address;
        var moveTo = to_emirate.label + (to_area.length ? ' - ' + to_area : '') + ' - ' + to_address;

        var needHandyman = need_handyman.value == "Yes" ? true : false;

        var bookingDate = booking_date.split("-").reverse().join('-') + " 00:00:00";

        bookingDate = moment(bookingDate).format();

        var movingSize = typeOfMove.value == "full_move" ? moving_size.value : 'Small Move';

        var itemListsValue = "";

        if (movingSize == "Small Move") {
            itemListsValue = packages.name + " " + item_lists;
        }

        if (locationHelper.getCurrentCountryId() == UAE_ID) {
            bookingDate = moment(bookingDate).tz("Asia/Dubai").format();
        } else if (locationHelper.getCurrentCountryId() == QATAR_ID) {
            bookingDate = moment(bookingDate).tz("Asia/Qatar").format();
        } else {
            bookingDate = moment(bookingDate).format();
        }

        var data = {
            "Booking_Status": 'Open',
            "Currency": current_currency,
            "Lead_Source": 'Website',
            "First_Name": input_name,
            "Last_Name": input_last_name,
            "Email": input_email,
            "Phone": input_phone,
            "Apartment_Villa_Number": from_address,
            "Area": (from_area.length ? from_area : ''),
            "City": from_emirate.label,
            "Required_when": 'I need them at a specific day and time',
            "Required_on": bookingDate,
            "isTest": false,
            "UserId": this.props.signInDetails.userId,
            "Requester_comments": stateData.details,
            "Residence_Types": (movingSize == "full_move" ? moving_size.homeType : ""),
            "Service": 'Moving',
            "Rooms": (movingSize == "full_move" ? String(moving_size.rooms) : ""),
            "Move_from": moveFrom,
            "Move_to": moveTo,
            "Move_request_notes": itemListsValue,
            "Move_size": movingSize,
        };

        if (!isSoftBooking) {

            data["Move_provider_category"] = packages.move_provider_category;
            data["Need_a_handyman"] = needHandyman;

            var price = packages.new_pricing;

            var tax = vat;

            var zohoPriceData = BookingVoucherify.zohoPriceData(price, tax, booking_date);

            var submitData = Object.assign(data, zohoPriceData);
        } else {
            data["Booking_Status"] = 'Soft Booking';
        }

        var datas = [data];

        return datas;

    }
    generateCouponRules(value, stateData, newPrice = 0) {

        const url_constants = URLCONSTANT;

        const slug = this.props.url_params.params.slug;

        const allServiceConstant = this.props.service_constants;

        var bookingFrequencyDataConstants = this.props.dataConstants["BOOKING_FREQUENCY"];

        var dataValues = commonHelper.processDataValues(bookingFrequencyDataConstants);

        let { booking_date, field_service_needed, discountData, subtotal, vat, payment, price } = stateData;

        price = newPrice != 0 ? newPrice : price;

        var week_days = new Array();

        /*var paymentMethod = "";

        if(payment == ""){
            paymentMethod = "BOOKING_PAYMENT_METHOD_EMPTY"
        }*/

        var serviceId = allServiceConstant.SERVICE_HOME_CLEANING;

        if (slug == url_constants.POOL_CLEANING_PAGE_URI) {
            serviceId = allServiceConstant.SERVICE_POOL_CLEANING_SERVICE;
        }
        if (slug == url_constants.CLEANING_MAID_PAGE_URI) {
            serviceId = allServiceConstant.SERVICE_HOME_CLEANING;
        }
        if (slug == url_constants.WINDOW_CLEANING_PAGE_URI) {
            serviceId = allServiceConstant.SERVICE_WINDOW_CLEANING;
        }

        var serviceLocationId = locationHelper.getCurrentCityId(); // todo: dynamic value

        if(stateData.input_address_city != "undefined"){
            let city_object = stateData.input_address_city;
            //serviceLocationId = city_object.id == SHARJAH_ID ? SHARJAH_MAPPED_ID : city_object.id;
            serviceLocationId = city_object.id;
        }

        var validDate = "";

        var customerId = !this.props.signInDetails.customer ? 0 : parseInt(this.props.signInDetails.customer.id);

        if (booking_date != "") {
            booking_date = booking_date.split('-').reverse().join('-');
            validDate = booking_date;
            booking_date = new Date(booking_date);
            var week_day = "" + booking_date.getDay();
            week_days = [week_day];
        }

        var paymentMethod = commonHelper.getPaymentMethod(payment);

        //payment == "credit" ? "CREDIT_CARD" : "CASH_ON_DELIVERY";

        var rules = {
            PAYMENT_METHOD: paymentMethod,
            DAYS_OF_WEEK: week_days,
        };

        if (slug == url_constants.CLEANING_MAID_PAGE_URI) {
            var hours_of_job = parseInt(stateData.hours_required.value);

            rules["HOURS_OF_JOB"] = hours_of_job;
        }

        var isRecurring = (typeof field_service_needed != "undefined" && field_service_needed.value != dataValues.DATA_VALUE_ONCE) ? true : false;


        var data = BookingCoupons.generateRules(value, rules, isRecurring, price, serviceId, serviceLocationId, validDate, customerId);

        return data;
    }
    handleCouponChange(value, stateData, newPrice = 0, returnRules = false, returnVal = false) {

        const url_constants = URLCONSTANT;

        const slug = this.props.url_params.params.slug;

        const allServiceConstant = this.props.service_constants;

        var bookingFrequencyDataConstants = this.props.dataConstants["BOOKING_FREQUENCY"];

        var dataValues = commonHelper.processDataValues(bookingFrequencyDataConstants);

        var coupon_value = value.trim(), changed_element = null, changed_value = null;

        this.setState({
            discountData: "load"
        })

        if (typeof coupon_value == 'undefined' || coupon_value.length == 0) {
            var response = BookingCoupons.error("Please provide coupon.");
            this.setState({
                discountData: response.data,
                discountPrices: {}
            });
        }else if (coupon_value.trim().length <= 3) { // minimum length of a promo code is more than 3 characters, so need to go to server if less than that
            var response = BookingCoupons.error("Promotion code is invalid");
            this.setState({
                discountData: response.data,
                discountPrices: {}
            })
        }
        else {
            let { booking_date, field_service_needed, discountData, subtotal, vat, payment, price, walletUsed } = stateData;

            var bookingDate = booking_date;

            price = newPrice != 0 ? newPrice : price;

            //console.log("coupon change");
            var data = this.generateCouponRules(value, stateData, price);

            var user_token = !this.props.signInDetails ? "" : this.props.signInDetails.access_token;

            BookingCoupons.validate(value, data, user_token).then((response) => {
                var response = response.data;
                if(typeof response == "undefined" || response == null){
                    response = BookingCoupons.error("Promotion code is invalid");
                        this.setState({
                            discountData: response.data,
                            discountPrices: {}
                        })
                }else{
                    if ( (response != null && typeof response.error != "undefined" ) && response.error) {

                        if (value == CREDITCARD_COUPON && response.error_message == "Coupon total limit has ended.") {
                            this.setState({
                                isCreditCardCouponApplied: true,
                                discountPrices: {}
                            });
                        } else {
                            this.setState({
                                discountPrices: {}
                            });
                        }
                    } else {
                        
                        var discountData = response;
                        var currentTotal = walletUsed > 0 ? walletUsed + discountData.actualPrice : discountData.actualPrice;
                        var promoCodeDiscountText = "";
                        var totalAfterDiscount = 0;

                        if (discountData.discountType == 'PERCENT_OFF') {
                            promoCodeDiscountText = discountData.discount + '% OFF';
                            //totalAfterDiscount = discountData.finalPrice; //(currentTotal * ((100 - discountData.discount) / 100)).toFixed(2);
                            /*console.log("totalAfterDiscount", totalAfterDiscount);
                            let discountAmt  = currentTotal - totalAfterDiscount;
                            console.log("totalAfterDiscount", discountAmt);
                            if(discountData.maxAmountDiscount != null && discountAmt > discountData.maxAmountDiscount){
                                promoCodeDiscountText = current_currency + " " + discountData.maxAmountDiscount + ' OFF';
                                totalAfterDiscount = currentTotal - discountData.maxAmountDiscount
                            }*/
                            //currentTotal = totalAfterDiscount;
                        }
                        else if (discountData.discountType == 'AMOUNT_OFF') {
                            promoCodeDiscountText = current_currency + " " + discountData.discount + ' OFF';
                            //totalAfterDiscount = currentTotal - discountData.discount;
                            //currentTotal = totalAfterDiscount;
                        }
                        totalAfterDiscount = discountData.finalPrice;

                        currentTotal = walletUsed > 0 ? currentTotal - walletUsed : currentTotal;
                        //console.log('totalAfterDiscount',totalAfterDiscount);
                        if (this.props.userProfile && this.props.userProfile.userWallet) {
                            const walletAmount = this.props.userProfile.userWallet.totalAmount;
                            const walletCurrency = this.props.userProfile.userWallet.currency.code;

                            if (walletCurrency === current_currency && walletAmount > 0) {
                                totalAfterDiscount = totalAfterDiscount - walletAmount > 0 ? totalAfterDiscount - walletAmount : 0;
                            }
                        }


                        var taxChargesTotal = TaxCalculator.calculateVAT(totalAfterDiscount, bookingDate);

                        if (totalAfterDiscount) {
                            totalAfterDiscount = parseFloat(totalAfterDiscount);
                        }

                        var total = parseFloat((totalAfterDiscount + taxChargesTotal).toFixed(2));

                        // if (this.props.userProfile && this.props.userProfile.userWallet) {
                        //     const walletAmount = this.props.userProfile.userWallet.totalAmount;
                        //     const walletCurrency = this.props.userProfile.userWallet.currency.code;

                        //     if (walletCurrency === current_currency && walletAmount > 0) {
                        //         total = total-walletAmount; 
                        //     }
                        // }

                        var discountPrices = {
                            price: currentTotal,
                            total: total,
                            subtotal: currentTotal,
                            vat: taxChargesTotal
                        };

                        response["success"] = true;

                        response["promoCodeDiscountText"] = promoCodeDiscountText;
                    }
            }
                this.setState({
                    discountData: response,
                    discountPrices: discountPrices
                })
            });

        }
    }
    updateCreditCardCouponApplied() {
        this.setState({
            isCreditCardCouponApplied: true
        });
    }
    pestControlSubServiceUrlMap() {
        const allServiceConstant = this.props.service_constants;
        var subserviceUrl = {
            'ants': allServiceConstant.SERVICE_ANTS_PEST_CONTROL,
            'cockroaches': allServiceConstant.SERVICE_COCKROACHES_PEST_CONTROL,
            'bed-bugs': allServiceConstant.SERVICE_BED_BUGS_PEST_CONTROL,
            'move-in': allServiceConstant.SERVICE_MOVE_IN_PEST_CONTROL
        };
        return subserviceUrl;
    }
    handymanSubServiceUrlMap() {
        const allServiceConstant = this.props.service_constants;
        var subserviceUrl = {
            'furniture-assembly': allServiceConstant.SERVICE_FURNITURE_ASSEMBLY,
            'tv-wall-mounting': allServiceConstant.SERVICE_TV_MOUNTING,
            'curtain-fixing': allServiceConstant.SERVICE_CURTAIN_HANGING,
            'light-installation': allServiceConstant.SERVICE_LIGHTBULBS_LIGHTING,
            'ac-cleaning': allServiceConstant.SERVICE_AC_CLEANING,
            'ac-installation': allServiceConstant.SERVICE_AC_INSTALLATION,
            'ac-repair': allServiceConstant.SERVICE_AC_REPAIR
        };
        return subserviceUrl;
    }
    applyCreditCardCoupon(value, voucherCode, isLWB = true, ) {
        var applyCreditCardCoupon = false;

        var voucherCode = voucherCode;

        var payment = value;

        var returnData = {};

        //if((newProps.isCreditCardCouponApplied !== this.props.isCreditCardCouponApplied) && newProps.isCreditCardCouponApplied){

        if (payment == "") {
            applyCreditCardCoupon = true
        } else {
            applyCreditCardCoupon = (payment != "" && payment == "credit") ? true : false;
        }
        if (this.state.isCreditCardCouponApplied && (voucherCode != "" && voucherCode == CREDITCARD_COUPON)) {
            applyCreditCardCoupon = false;
            voucherCode = "";
        }
        else {
            if (!this.state.isCreditCardCouponApplied && ((applyCreditCardCoupon && payment == "credit") && (voucherCode == "" || voucherCode == CREDITCARD_COUPON))) {
                if (voucherCode == "") {
                    voucherCode = CREDITCARD_COUPON
                }
            } else {
                if (payment == "cash" && voucherCode == CREDITCARD_COUPON) {
                    voucherCode = "";
                }
            }
        }

        return { applyCreditCardCoupon: applyCreditCardCoupon, voucherCode: voucherCode };
    }
    /*setUserCity(isCleaningBooking = false){

    }*/
    setUserArea(isCleaningBooking = false, userProfile = false, returnUserCity = false) {
        var userArea = "";
        var countryID = locationHelper.getCurrentCountryId();
        var userData = userProfile == false ? this.props.userProfile : userProfile;

        if((commonHelper.isUserLoggedIn() && (userData != false && userData.address != null)) && (userData.address.area_id == null)){
            userArea = userData.address.area;
        }else{
            if (commonHelper.isUserLoggedIn() && (userData != false && userData.address != null)) {
                if (this.props.userProfile.customerCityId != "") {
                    let customerCityId = userData.customerCityId;
                    if(userData.customerCityId == SHARJAH_MAPPED_ID){
                        customerCityId = SHARJAH_ID;
                    }
                    else if(userData.customerCityId == DOHA_CITY_ID){
                        customerCityId = DOHA_ID;
                    }

                    //var customerCityId = userData.customerCityId == SHARJAH_MAPPED_ID ? SHARJAH_ID : userData.customerCityId;

                    var city = locationHelper.getLocationById(customerCityId);

                    if (isCleaningBooking && userData.address.area != null) {
                        var cityAreas = locationHelper.getAreasByCity(city.id);
                        userArea = locationHelper.getAreaByName(cityAreas, userData.address.area);

                    } else {

                        if (current_city == city.value && userData.address.area != null) {
                            var cityAreas = locationHelper.getAreasByCity(city.id);
                            userArea = locationHelper.getAreaByName(cityAreas, userData.address.area);
                        }
                    }
                }
            }
        }
        return userArea;
    }
    getServiceCode(serviceId) {
        var { allServices } = this.props;
        var service = [];
        service = typeof allServices.parentServices != "undefined" ? allServices.parentServices.filter((item) => { return item.id == serviceId }) : [];
        if (service.length == 0) {
            service = typeof allServices.subServices != "undefined" ? allServices.subServices.filter((item) => { return item.id == serviceId }) : [];
        }
        return service.length ? service[0].serviceCode : "";
    }
    handymanPayload(stateData) {
        var {
            sub_service,
            booking_date,
            booking_time,
            homeType,
            numberOfUnits,
            numberOfHours,
            tvSize
        } = stateData;

        booking_date = booking_date.split('-').reverse().join('-');

        var numberOfWeekDays = [];

        var booking_day = parseInt(moment(booking_date).format('d'));

        numberOfWeekDays.push(booking_day == 0 ? 7 : booking_day);

        let homeTypeValue = homeType.value;
        let service_detail = {};

        let { service_constants, PricePlan } = this.props;

        let price_plan_id = 0

        if (sub_service.value == service_constants.SERVICE_AC_CLEANING) {
            price_plan_id = typeof numberOfUnits.planId != "undefined" ? numberOfUnits.planId : PricePlan.id;
            service_detail = {
                "no_of_units": numberOfUnits.value,
                "property_type": homeTypeValue.toLowerCase()
            }
        } else if (sub_service.value == service_constants.SERVICE_TV_MOUNTING) {
            price_plan_id = tvSize.planId;
            service_detail = {
                "tv_size_in_inches": tvSize.inches
            }
        } else {
            price_plan_id = typeof numberOfHours.planId != "undefined" ? numberOfHours.planId : PricePlan.id;
            service_detail = {
                "no_of_hours": numberOfHours.id == 0 ? 0 : parseInt(numberOfHours.value),
            }
        }

        var bookingJson = {
            "frequency": "ONCE",
            "number_of_week_days": numberOfWeekDays,
            "price_plan_id": price_plan_id,
            "service_detail": service_detail,
            "start_date": booking_date,
            "time": booking_time.value + ":00"
        };

        return bookingJson;

    }
    pestControlbookingJson(stateData, getRedeemRule = false) {
        var {
            booking_date,
            booking_time,
            typeOfPest,
            homeType,
            numberOfUnits,
            payment
        } = stateData;

        booking_date = booking_date.split('-').reverse().join('-');

        var numberOfWeekDays = [];

        var booking_day = parseInt(moment(booking_date).format('d'));

        numberOfWeekDays.push(booking_day == 0 ? 7 : booking_day);

        let homeTypeValue = homeType.value;

        var price = numberOfUnits.price;

        var bookingJson = {
            "frequency": "ONCE",
            "number_of_week_days": numberOfWeekDays,
            "price_plan_id": numberOfUnits.planId,
            "service_detail": {
                "no_of_rooms": numberOfUnits.value,
                "property_type": homeTypeValue.toLowerCase()
            },
            "start_date": booking_date,
            "time": booking_time.value + ":00"
        };


        return bookingJson;
    }
    cleaningBookingJson(stateData) {
        var {
            booking_date,
            booking_time,
            homeType,
            numberOfUnits,
            field_furnished,
            field_scrubbing,
            otherServices
        } = stateData;

        booking_date = booking_date.split('-').reverse().join('-');

        var numberOfWeekDays = [];

        var booking_day = parseInt(moment(booking_date).format('d'));

        numberOfWeekDays.push(booking_day == 0 ? 7 : booking_day);

        //var price = numberOfUnits.price;
        let url_constants = URLCONSTANT;
        let { service_constants, PricePlan } = this.props;
        let slug = this.props.url_params.params.slug;
        var bookingJson = {
            "frequency": "ONCE",
            "number_of_week_days": numberOfWeekDays,
            "price_plan_id": PricePlan.id,
            "service_detail": {},
            "start_date": booking_date,
            //'time': ''
        };

        if (slug == url_constants.DEEP_CLEANING_PAGE_URI) {
            let homeTypeValue = homeType.value;
            
            bookingJson["service_detail"] = {
                "no_of_rooms": numberOfUnits.value,
                "property_type": homeTypeValue.toLowerCase(),
                "is_home_furnished": field_furnished.boolVal,
                //"scrubbing": field_scrubbing.boolVal
                //"steam_cleaning"
            }
            bookingJson["time"] = booking_time.value + ":00"
            if(otherServices.length){
                otherServices.map((item) => {
                    bookingJson["service_detail"][item.value] = true;
                })
                //steam_cleaning
                //scrubbing
                //bookingJson["service_detail"]
            }
        }
        else if (slug == url_constants.MATTRESS_CLEANING_PAGE_URI) {
            bookingJson['time'] = `${stateData.booking_time && stateData.booking_time.id ?
                    stateData.booking_time.value + ':00' : 0}`;
            
            let mattressCounts = stateData.mattressCounts;
            Object.keys(mattressCounts).map((item) => {
                if (mattressCounts[item] != 0) {
                    bookingJson["service_detail"][item] = mattressCounts[item];
                }
            });
        }
        else if (slug == url_constants.SOFA_AND_UPHOLSTERY_CLEANING_PAGE_URI) {
            if (stateData.no_of_seat_for_steam_cleaning != 0) {
                bookingJson["service_detail"]["no_of_seat_for_steam_cleaning"] = stateData.no_of_seat_for_steam_cleaning;
            }
            if (stateData.no_of_seat_for_shampoo_cleaning != 0) {
                bookingJson["service_detail"]["no_of_seat_for_shampoo_cleaning"] = stateData.no_of_seat_for_shampoo_cleaning;
            }
            bookingJson["time"] = booking_time.value + ":00"
        }
        else if (slug == url_constants.CARPET_CLEANING_PAGE_URI) {
            if (stateData.carpet_area_for_steam_cleaning != 0) {
                bookingJson["service_detail"]["carpet_area_for_steam_cleaning"] = stateData.carpet_area_for_steam_cleaning;
            }
            if (stateData.carpet_area_for_shampoo_cleaning != 0) {
                bookingJson["service_detail"]["carpet_area_for_shampoo_cleaning"] = stateData.carpet_area_for_shampoo_cleaning;
            }
            bookingJson["time"] = booking_time.value + ":00"
        }
        else if (slug == url_constants.WATER_TANK_CLEANING_PAGE_URI) {
            bookingJson["time"] = booking_time.value + ":00"
        }
        else if (slug == url_constants.LAUNDRY_DRY_CLEANING) {
            bookingJson["service_detail"]["delivery_option"] = parseInt(stateData.dropOff.value);
            bookingJson["time"] = booking_time.value + ":00"
        }

        return bookingJson;
    }
    voucherRules(stateData) {
        let rules = "";
        if (this.isVoucherSuccess(stateData)) {
            let url_constants = URLCONSTANT;
            let url_params = this.props.url_params.params;
            let city = url_params.city;
            let slug = this.props.url_params.params.slug;
            let { service_constants } = this.props;
            let {
                booking_date,
                booking_time,
                typeOfHandyman,
                typeOfPest,
                homeType,
                numberOfUnits,
                payment,
                voucherCode,
                discountData,
                sub_service,
                numberOfHours
            } = stateData;

            let price = 0;
            let serviceId = 0, subServiceId = 0, packageDetail = {}, isHourlyPrice = false, isHourlyRate = false;

            if (slug == url_constants.PEST_CONTROL_PAGE_URI) {
                price = numberOfUnits.price;
                serviceId = typeOfPest.value;
                subServiceId = 0;
            }
            else if (slug == url_constants.DEEP_CLEANING_PAGE_URI) {
                price = numberOfUnits.price;
                serviceId = service_constants.SERVICE_DEEP_CLEANING_SERVICE;
                subServiceId = 0;
            }
            else if (slug == url_constants.WATER_TANK_CLEANING_PAGE_URI) {
                //price = this.props.pricePlan.planDetail.packages[0].price;
                price = stateData.price;
                serviceId = service_constants.SERVICE_WATER_TANK_CLEANING;
                subServiceId = 0;
            }
            else if (slug == url_constants.SOFA_AND_UPHOLSTERY_CLEANING_PAGE_URI) {
                price = stateData.price;
                serviceId = service_constants.SERVICE_SOFA_CLEANING;
                subServiceId = 0;
            }
            else if (slug == url_constants.CARPET_CLEANING_PAGE_URI) {
                price = stateData.price;
                serviceId = service_constants.SERVICE_CARPET_CLEANING;
                subServiceId = 0;
            }
            else if (slug == url_constants.MATTRESS_CLEANING_PAGE_URI) {
                price = stateData.price;
                serviceId = service_constants.SERVICE_MATTRESS_CLEANING;
                subServiceId = 0;
            }
            else if (slug == url_constants.LAUNDRY_DRY_CLEANING) {
                //price = this.props.pricePlan.planDetail.packages[0].price;
                price = 0;
                serviceId = service_constants.SERVICE_LAUNDRY_AND_DRY_CLEANING;
                subServiceId = 0;
            }
            else if (this.isNewBookingEngService(stateData)) {
                serviceId = typeOfHandyman.value;

                subServiceId = typeof sub_service.value != "undefined" ? sub_service.value : 0;

                if (sub_service.value == service_constants.SERVICE_AC_CLEANING) {
                    price = numberOfUnits.price;
                } else {
                    let numberOfHoursVal = numberOfHours.value;
                    isHourlyRate = true;
                    if (isHourlyRate && (numberOfHoursVal == 0 || numberOfHours.id == 0)) {
                        isHourlyPrice = true;
                        price = typeof numberOfHours.per_hour_price != "undefined" ? parseInt(numberOfHours.per_hour_price) : parseInt(numberOfHours.price);
                    } else {
                        isHourlyPrice = true;

                        packageDetail["No_of_Hours"] = numberOfHoursVal;

                        if (numberOfHoursVal != 0 && numberOfHours.id != 0) {
                            packageDetail["Price"] = (numberOfHoursVal * numberOfHours.per_hour_price);
                            packageDetail["hourly_price"] = typeof numberOfHours.price != "undefined" ? numberOfHours.price : numberOfHours.per_hour_price;
                            price = typeof numberOfHours.hourPrice != "undefined" ? parseInt(numberOfHours.hourPrice) : (numberOfHoursVal * numberOfHours.per_hour_price);
                        } else {
                            isHourlyPrice = false;
                            price = typeof numberOfHours.per_hour_price != "undefined" ? parseInt(numberOfHours.per_hour_price) : parseInt(numberOfHours.price);
                            packageDetail["Price"] = price;
                            packageDetail["hourly_price"] = price;

                        }
                    }
                }
            }
            rules = BookingVoucherify.generateRules(voucherCode, price, serviceId, subServiceId, booking_date, city, packageDetail, payment, true);
        } else {
            rules = "";
        }
        return rules;
    }
    isVoucherSuccess(stateData) {
        var { voucherCode, discountData } = stateData;
        return (voucherCode != "" && (typeof discountData != "undefined" && (typeof discountData.success != "undefined" && discountData.success)));
    }
    generateNewBookingJSON(stateData, isSoftBooking = true) {
        let serviceCode = "";
        let bookingData = {};
        const url_constants = URLCONSTANT;
        const slug = this.props.url_params.params.slug;
        let { service_constants } = this.props;

        if (slug == url_constants.PEST_CONTROL_PAGE_URI) { // PEST CONTROL
            bookingData = this.pestControlbookingJson(stateData);
            let { typeOfPest } = stateData;
            serviceCode = typeOfPest.serviceCode;
        }
        else if (slug == url_constants.DEEP_CLEANING_PAGE_URI) { // Deep Cleaning
            bookingData = this.cleaningBookingJson(stateData);
            serviceCode = this.getServiceCode(service_constants.SERVICE_DEEP_CLEANING_SERVICE);
        }
        else if (slug == url_constants.WATER_TANK_CLEANING_PAGE_URI) { // Deep Cleaning
            bookingData = this.cleaningBookingJson(stateData);
            serviceCode = this.getServiceCode(service_constants.SERVICE_WATER_TANK_CLEANING);
        }
        else if (slug == url_constants.SOFA_AND_UPHOLSTERY_CLEANING_PAGE_URI) { // Sofa Cleaning
            bookingData = this.cleaningBookingJson(stateData);
            serviceCode = this.getServiceCode(service_constants.SERVICE_SOFA_CLEANING);
        }
        else if (slug == url_constants.MATTRESS_CLEANING_PAGE_URI) { // mattress Cleaning
            bookingData = this.cleaningBookingJson(stateData);
            serviceCode = this.getServiceCode(service_constants.SERVICE_MATTRESS_CLEANING);
        }
        else if (slug == url_constants.CARPET_CLEANING_PAGE_URI) { // carpet Cleaning
            bookingData = this.cleaningBookingJson(stateData);
            serviceCode = this.getServiceCode(service_constants.SERVICE_CARPET_CLEANING);
        }
        else if (slug == url_constants.LAUNDRY_DRY_CLEANING) {
            bookingData = this.cleaningBookingJson(stateData);
            serviceCode = this.getServiceCode(service_constants.SERVICE_LAUNDRY_AND_DRY_CLEANING);
        }
        else if (typeof stateData.typeOfHandyman != "undefined") { // AC maintenance
            let typeOfHandymanValue = stateData.typeOfHandyman;
            let { sub_service } = stateData;
            let isPlumbingAndElectrician = typeOfHandymanValue.value == service_constants.SERVICE_ELECTRICIAN || typeOfHandymanValue.value == service_constants.SERVICE_PLUMBING ? true : false;
            bookingData = this.handymanPayload(stateData);
            serviceCode = isPlumbingAndElectrician ? typeOfHandymanValue.serviceCode : sub_service.serviceCode;
            //console.log(serviceCode, isPlumbingAndElectrician, typeOfHandymanValue);
        }

        let payment_method = "";

        if (stateData.payment === 'credit') {
            payment_method = "CREDIT_CARD"
        }
        else if (stateData.payment === 'cash') {
            payment_method = "CASH_ON_DELIVERY"
        } else {
            payment_method = "BOOKING_PAYMENT_METHOD_EMPTY"
        }
        let isPaymentMethodVerified = false;

        let creditCardId = 0;

        if (!isSoftBooking) {
            isPaymentMethodVerified = (this.props.myCreditCardsData.data && this.props.myCreditCardsData.data.length > 0) || (stateData.payment !== 'credit');
            creditCardId = (this.props.myCreditCardsData.data && this.props.myCreditCardsData.data.length) ? this.props.myCreditCardsData.data[0].id : 0;
        } else {
            payment_method = "BOOKING_PAYMENT_METHOD_EMPTY"
        }

        if (payment_method == "CASH_ON_DELIVERY") {
            isPaymentMethodVerified = true;
        }
        if (this.noPaymentStep()) {
            isPaymentMethodVerified = true;
            payment_method = "CASH_ON_DELIVERY";
        }
        let currentCityData = this.props.currentCityData;

        let city_object = stateData.input_address_city;
        //console.log("city_object", city_object);
        let voucherRules = this.voucherRules(stateData);
        let cityID = this.getCityID(city_object);//city_object.id == SHARJAH_ID ? SHARJAH_MAPPED_ID : city_object.id;

        let description = !!stateData.details ? stateData.details : '';
        if(typeof stateData.wallMount != "undefined" && ( typeof stateData.wallMount.value != "undefined" && stateData.wallMount.value != "")){
            description+= "|| Customer have a TV Bracket? "+ stateData.wallMount.value+"\n";
        }

        var json = {
            "address": {
                "apartment": stateData.input_address_area_building_apartment,
                "building": stateData.input_address_area_building_name,
                "city_id": cityID,
                "line1": "",
                "line2": ""
            },
            "contact_information": {
                "personName": stateData.input_name,
                "personLastName": stateData.input_last_name,
                "personPhone": stateData.input_phone,
                "personEmail": stateData.input_email
            },
            "customer_id": this.props.signInDetails.customer.id,
            "description": description,
            "payment": {
                "paymentMethod": payment_method
            },
            "service_location_code": city_object.code,
            "booking": bookingData,
            "service_code": serviceCode,
        };
        //if(isPaymentMethodVerified){}
        if (voucherRules != "") {
            json["voucher"] = voucherRules;
        }
        if (creditCardId) {
            json["payment"]["creditCardId"] = creditCardId;
        }
        let userSelectedArea = this.getUserSelectedArea(stateData);

        if(userSelectedArea == null){
            //"area": this.getUserSelectedArea(stateData),
            json['address']['area_id'] = null;
            json['address']['area'] = this.getUserSelectedArea(stateData, true);
        }else{
            json['address']['area_id'] = userSelectedArea;
        }
        //"area_id": stateData.input_address_area.id,
        return json;
    }
    render() {
        var url_params = this.props.url_params;
        var stepContent = "";
        var currentStep = this.state.currentStep;
        var form_meta = this.form_meta(url_params.params.slug, url_params.params.city);
        if (Object.keys(form_meta).length == 0 || typeof form_meta.Steps == "undefined") {
            return (<section className="error-404 image-404 text-center text-align-centre margin-top60">
                <h1>Wrong address?</h1>
                <p>The page you are looking for was moved, renamed, or may have never existed. </p>
                <p>Go <a href="/">home</a> or try a search:</p>
            </section>);
        } else {

            var Steps = form_meta.Steps;
            var form = form_meta.loadForm;

            return (
                <React.Fragment>
                    <main className="container-1200 mb-120">
                        <TrackerNav Steps={Steps} formCurrentStep={currentStep} />
                        <div className={isMobile() ? "container pt-5" : "container pt-3"}>
                            <div className="row">
                                {this.state.loading ? (<main className="full-width" loader={"show"}><Loader /></main>) : form}
                            </div>
                        </div>
                    </main>
                </React.Fragment>
            );
        }
    }
}
String.prototype.toTitle = function () {
    return this.replace(/(^|\s)\S/g, function (t) { return t.toUpperCase() });
}
function mapStateToProps(state) {
    return {
        allServices: state.allServices,
        service_constants: state.serviceConstants,
        dataConstants: state.dataConstants,
        lite_weight_prices: state.liteWeightPrices,
        signInDetails: state.signInDetails,
        userProfile: state.userProfile,
        citiesAreas: state.citiesAreas,
        showLoginMenu: state.showLoginMenu,
        myCreditCardsData: state.myCreditCardsData,
        currentCity: state.currentCity,
        dateAndTime: state.dateAndTime,
        apiServices: state.apiServices,
        cities: state.cities,
        PricePlan: state.PricePlan,
        TaxPlanReducer: state.TaxPlanReducer,
        currentCityID: state.currentCityID,
        zohoMigratedServices: state.zohoMigratedServices,
        currentCityData: state.currentCityData,
        PricePlan: state.PricePlan,
        currentCurrency: state.currentCurrency,
        bookingDateTimeAvailabilityReducer: state.bookingDateTimeAvailabilityReducer,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(
        { toggleLoginModal,
          setBodyClass, 
          fetchCitiesAreas, 
          fetchUserProfile, 
          setSignIn, 
          getAllCreditCard, 
          registerCreditCard, 
          fetchDateTime, 
          fetchBookingPricingPlan, 
          fetchTaxPlan, 
          fetchZohoMigratedService, 
          fetchDateTimeAvailability,
          fetchDataDictionaryValues,
          fetchDataConstantValues,
          fetchAllCities,
          fetchServices, }, dispatch);
}

export default withCookies(connect(mapStateToProps, mapDispatchToProps)(BookingFormPage));

//export default BookingFormPage;
