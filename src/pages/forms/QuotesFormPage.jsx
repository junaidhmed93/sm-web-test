import React from "react";
import Loader from "../../components/Loader";
import Loadable from 'react-loadable';
const loading = () => (<Loader />);
const QuotesHouseHold = Loadable({
    loader: () => import('./quotes/QuotesHouseHold'),
    loading
});

const QuotesStoragePage = Loadable({
    loader: () => import('./quotes/QuotesStoragePage'),
    loading
});

const QuotesCarShippingPage = Loadable({
    loader: () => import('./quotes/QuotesCarShippingPage'),
    loading
});

const QuotesBabySittingPage = Loadable({
    loader: () => import('./quotes/QuotesBabySittingPage'),
    loading
});

const QuotesPhotographyPage = Loadable({
    loader: () => import('./quotes/QuotesPhotographyPage'),
    loading
});

const QuotesCateringPage = Loadable({
    loader: () => import('./quotes/QuotesCateringPage'),
    loading
});

const QuotesCurtainsPage = Loadable({
    loader: () => import('./quotes/QuotesCurtainsPage'),
    loading
});

const QuotesMovingForm = Loadable({
    loader: () => import('./quotes/QuotesMovingForm'),
    loading
});

const QuotesRequestPage = Loadable({
    loader: () => import('./quotes/QuotesRequestPage'),
    loading
}); 
import locationHelper from "../../helpers/locationHelper";
import commonHelper from "../../helpers/commonHelper";
import moment from 'moment';
import {
    setBodyClass,
    toggleLoginModal,
    isValidSection,
    scrollToTop,
    putUpdateCustomerProfile,
    fetchSignIn,
    fetchSignUp,
    setSignIn,
    postRequest,
    swapArrayElements,
    JOURNEY2,
    JOURNEY3,
    zendeskChatBox,
    UAE_ID,
    QATAR_ID,
    DOHA_ID,
    DOHA_CITY_ID,
    isMobile,
    SHARJAH_ID,
    SHARJAH_MAPPED_ID,
    fetchDateTime,
    LANG_AR,
    URLCONSTANT,
    fetchDateTimeAvailability,
    fetchDataDictionaryValues,
    fetchDataConstantValues,
    fetchServices,
    fetchAllCities,
} from "../../actions/index";
import TrackerNav from "../../components/TrackerNav";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withCookies } from "react-cookie";
let current_city = "dubai";

class QuotesFormPage extends React.Component {
    constructor(props) {
        super(props);
        var stateData = {
            currentStep: 1,
            showSelectCompanyError: false,
            isSkipCompany: false,
            loader: false,
            journey: 1,
            setUserArea: '',
            startDate: props.dateAndTime
        }
        this.locationHelperInit = locationHelper.init.bind(this);
        this.locationHelperInit();
        current_city = this.props.currentCity;
        var journey = props.url_params.params.journey;
        if ((typeof journey != "undefined") && (journey == JOURNEY2 || journey == JOURNEY3)) {
            var journeyId = journey == JOURNEY2 ? 2 : 3;
            stateData["journey"] = journeyId;
            stateData["currentStep"] = 2;
        }

        //console.log("journey", journey);
        this.state = stateData;
        
        this.currentStep = this.currentStep.bind(this);
        this.moveNext = this.moveNext.bind(this);
        this.showLoginModal = this.showLoginModal.bind(this);
        this.setUserLogedIn = this.setUserLogedIn.bind(this);
        this.signUpThenSignIn = this.signUpThenSignIn.bind(this);
        this.moveNextStep = this.moveNextStep.bind(this);
        this.howToProceed = this.howToProceed.bind(this);
        this.fetchDataIfNeeded = this.fetchDataIfNeeded.bind(this);
        this.showOrHideModalSelectCompanyError = this.showOrHideModalSelectCompanyError.bind(this);
        this.submitLeadData = this.submitLeadData.bind(this);
        this.generateHouseholdRequestData = this.generateHouseholdRequestData.bind(this);
        this.generateBabySittingData = this.generateBabySittingData.bind(this);
        this.generateCurtainsData = this.generateCurtainsData.bind(this);
        this.generateCateringData = this.generateCateringData.bind(this);
        this.submitPostRequest = this.submitPostRequest.bind(this);
        this.typeOfJourney = this.typeOfJourney.bind(this);
        this.getCookieFormData = this.getCookieFormData.bind(this);
        this.generateMoveDataForSubmission = this.generateMoveDataForSubmission.bind(this);
        this.hashChangeHandler = this.hashChangeHandler.bind(this);
        this.setUserArea = this.setUserArea.bind(this);
        this.getEventDate = this.getEventDate.bind(this);
        this.fetchDataIfNeeded(props);
    }
    fetchDataIfNeeded(props) {
        const url_constants = URLCONSTANT;
        const city = props.url_params.params.city;
        const slug = props.url_params.params.slug;
        var isSubscription = typeof props.url_params.params.subscription != "undefined" ? true : false;
        const service_constants = props.service_constants;
        let lang = props.lang;
        var city_id = props.currentCityID;
        if (typeof city_id != "undefined") {
            var promiseFetch = [];
            const citiesPromise =  this.props.fetchAllCities(city, lang, false);
            promiseFetch.push(citiesPromise);
            const dataConstants = this.props.fetchDataDictionaryValues(lang);
            promiseFetch.push(dataConstants);
            const newDataConstants = this.props.fetchDataConstantValues(lang);
            promiseFetch.push(newDataConstants);
            const servicesPromise = this.props.fetchServices(city,lang);
            promiseFetch.push(servicesPromise);
            const currentDateTime = this.props.fetchDateTime();
            promiseFetch.push(currentDateTime);  
            Promise.all(promiseFetch).then(() => {
                this.setState({
                    loader: false
                })
            }).catch(function (error) {
                console.log("Error fetchDataIfNeeded", error);
            });
        }
        //}
    }
    typeOfJourney() {
        return this.state.journey
    }
    componentDidMount() {
        let {lang} = this.props;
        this.props.setBodyClass('quotes');
        let hashVal = window.location.hash;
        let step;
        hashVal.length ? step = parseInt(window.location.hash.replace('#', '')) : step = 1;
        if (step == 2 || step == 3) {
            window.location = window.location.pathname;
        }
        var areaData = this.setUserArea();

        this.setState({
            setUserArea: areaData
        })

        window.addEventListener("hashchange", this.hashChangeHandler, false);

        zendeskChatBox(lang); // zendesk chat box
        
        /*setTimeout(function(){
            if(typeof $zopim != "undefined"){
                $zopim.livechat.setLanguage(lang);
            }
        }, 3000)*/
        this.interval = setInterval(() => commonHelper.getCurrentTime(this), 20000);
    }
    componentWillUnmount() {
        window.removeEventListener("hashchange", this.hashChangeHandler, false);
    }
    hashChangeHandler() {
        let hashVal = window.location.hash;
        let step;
        var typeOfJourney = this.typeOfJourney();
        if (typeOfJourney == 1) {
            hashVal.length ? step = parseInt(window.location.hash.replace('#', '')) : step = 1;
        } else {
            hashVal.length ? step = parseInt(window.location.hash.replace('#', '')) : step = 2;
        }
        this.moveNext(step);
        scrollToTop();
    }
    showLoginModal(guest = true) {
        this.props.toggleLoginModal({ visibility: true, guest: guest });
    }
    currentStep() {
        return this.state.currentStep;
    }
    moveNext(step) {
        this.setState({
            currentStep: parseInt(step)
        });
    }
    signUpThenSignIn(email, firstname, lastname, password) {

        // this.updateUserProfileAndGoToPayment(true);
        let {lang} = this.props;
        if (password == '' || !password || password.length == 0) password = Math.random().toString(36).slice(-8);
        return fetchSignUp(email, firstname, lastname, password, lang)
            .then((result) => {
                // console.log(result);
                return fetchSignIn(email, password).then((res) => {
                    // console.log(res);
                    this.setUserLogedIn(res);
                });
            });
    }
    setUserLogedIn(details) {
        if ( (typeof details.data != "undefined" && typeof details.data.access_token != "undefined" ) && 
             ( details.data.access_token && details.data.access_token.length > 0)) {
            this.props.setSignIn(details.data);
            const { cookies } = this.props;
            cookies.set('_user_details', JSON.stringify(details.data), { path: '/', maxAge: details.data.expires_in });
        }
    }
    getCookieFormData(cookieName) {
        var url_params = this.props.url_params;

        var serviceURL = url_params.params.slug;

        var currentLanguage = url_params.params.lang;

        var currentCityName = url_params.params.city;

        var currentPath = serviceURL;

        var cookieName = currentLanguage + "." + currentCityName + "." + currentPath + "." + cookieName;

        const { cookies } = this.props;

        return cookies.get(cookieName) || {};
    }
    form_meta(match, city) {

        const url_constants = URLCONSTANT;

        const currentStep = this.state.currentStep;

        const service_constants = this.props.service_constants;

        const { signInDetails, userProfile, countries, url_params } = this.props;

        var formData = {};

        let dataConstants = this.props.dataConstants;

        const { cookies } = this.props;

        var passedService = cookies.get("passedService") || {};

        var landingPageServiceId = cookies.get("landingPageServiceId") || "";

        if (landingPageServiceId != "") {

            if (typeof service_constants[landingPageServiceId] != "undefined") {
                passedService = { serviceId: service_constants[landingPageServiceId] }
            }
            cookies.remove("landingPageServiceId", { path: '/' });
        }

        var defaultServiceId = 0;

        var defaultServiceOption = [];


        var quotesData = {};

        if (this.typeOfJourney() != 1) {
            quotesData = this.getCookieFormData("quotes_data");

            // console.log("quotesData", quotesData);

            if (typeof quotesData.selectedSubService != "undefined") {
                var selectedSubService = quotesData.selectedSubService;
                passedService = { serviceId: selectedSubService.id }
            }



            if (typeof window != "undefined" && typeof quotesData.selected_companies == "undefined") {
                var lang = url_params.params.lang;

                var url = window.location.protocol + '//' + window.location.host;

                url += "/" + lang + "/" + city + "/" + match + "/journey1";

                window.location = url;

                return false;
            }
        }
        
        var commonProps = {
            signUpThenSignIn: this.signUpThenSignIn,
            showLoginModal: this.showLoginModal,
            userProfile: userProfile,
            signInDetails: signInDetails,
            url_constants: url_constants,
            formCurrentStep: currentStep,
            moveNext: this.moveNext,
            service_constants: service_constants,
            dataConstants: dataConstants,
            current_city: city,
            showLoginMenu: this.props.showLoginMenu,
            moveNextStep: this.moveNextStep,
            howToProceed: this.howToProceed,
            showSelectCompanyError: this.state.showSelectCompanyError,
            showOrHideModalSelectCompanyError: this.showOrHideModalSelectCompanyError,
            submitLeadData: this.submitLeadData,
            loader: this.state.loader,
            isSkipCompany: this.state.isSkipCompany,
            typeOfJourney: this.typeOfJourney,
            formCurrentStep: currentStep,
            setUserArea: this.state.setUserArea,
            startDate: this.state.startDate,
            url_params: url_params.params
        }

        //console.log("quotesData", quotesData);

        if (match == url_constants.CLEANING_MAID_PAGE_URI) {
            var Steps = [
                { id: 'section-request-form', title: locationHelper.translate('YOUR_REQUEST') },
                { id: 'section-company-selector', title: locationHelper.translate('SELECT_COMPANIES') },
                { id: 'payment-method-form', title: locationHelper.translate('CONTACT_DETAILS') }
            ];

            var parnetServiceId = service_constants.SERVICE_CLEANING_AND_MAID_SERVICES;

            if (typeof passedService.serviceId != "undefined" && passedService.serviceId != parnetServiceId) {
                defaultServiceId = passedService.serviceId;
            } else {
                defaultServiceId = service_constants.SERVICE_HOME_CLEANING;
            }

            defaultServiceOption = this.getDefaultServiceOption(parnetServiceId, defaultServiceId);

            formData = {
                Steps: Steps, loadForm: <QuotesHouseHold
                    quotesData={quotesData}
                    defaultServiceOption={defaultServiceOption}
                    {...commonProps}
                />
            }

        }
        else if (match == url_constants.MAINTENANCE_PAGE_URI ||
            match == url_constants.PEST_CONTROL_PAGE_URI ||
            match == url_constants.GARDENING_PAGE_URI ||
            match == url_constants.PAINTERS_PAGE_URI
        ) {
            var Steps = [
                { id: 'section-request-form', title: locationHelper.translate('DETAILS') },
                { id: 'section-company-selector', title: locationHelper.translate('SELECT_COMPANIES') },
                { id: 'payment-method-form', title: locationHelper.translate('CONTACT_DETAILS') }
            ];

            var parnetServiceId = service_constants.SERVICE_MAINTENANCE;

            var defaultServiceId = 0;

            var defaultServiceOption = [];

            var isPainting = false;


            if (match == url_constants.MAINTENANCE_PAGE_URI) {
                parnetServiceId = service_constants.SERVICE_MAINTENANCE
                defaultServiceId = service_constants.SERVICE_OTHER__MAINTENANCE;
            } else if (match == url_constants.PEST_CONTROL_PAGE_URI) {
                parnetServiceId = service_constants.SERVICE_PEST_CONTROL;
                defaultServiceId = service_constants.SERVICE_COCKROACHES_PEST_CONTROL;
            } else if (match == url_constants.GARDENING_PAGE_URI) {
                parnetServiceId = service_constants.SERVICE_GARDENING;
                defaultServiceId = service_constants.SERVICE_GARDEN_MAINTENANCE;
            }
            else if (match == url_constants.PAINTERS_PAGE_URI) {
                parnetServiceId = service_constants.SERVICE_PAINTING;
                defaultServiceId = service_constants.SERVICE_HOME_PAINTING;
                isPainting = true;
            }
            if (typeof passedService.serviceId != "undefined") {
                var passedDefaultServiceOption = this.getDefaultServiceOption(parnetServiceId, passedService.serviceId);
                if (Object.keys(passedDefaultServiceOption).length) {
                    defaultServiceId = passedService.serviceId;
                    defaultServiceOption = passedDefaultServiceOption;
                }
            }

            if (!Object.keys(defaultServiceOption).length) {
                defaultServiceOption = this.getDefaultServiceOption(parnetServiceId, defaultServiceId);
            }

            var servicesOptions = commonHelper.lookupServices('parent_service_id', parnetServiceId);

            formData = {
                Steps: Steps, loadForm:
                    <QuotesRequestPage signUpThenSignIn={this.signUpThenSignIn}
                        servicesOptions={servicesOptions}
                        defaultServiceId={defaultServiceId}
                        defaultServiceOption={defaultServiceOption}
                        parnetServiceId={parnetServiceId}
                        isPainting={isPainting}
                        quotesData={quotesData}
                        {...commonProps}
                    />
            };


        }
        else if (match == url_constants.LOCAL_MOVER_PAGE_URI) {
            var Steps = [
                { id: 'section-request-form', title: locationHelper.translate('MOVE_DETAILS') },
                { id: 'section-company-selector', title: locationHelper.translate('SELECT_COMPANIES') },
                { id: 'payment-method-form', title: locationHelper.translate('CONTACT_DETAILS') }
            ];
            var typeOfMove = {id: service_constants.SERVICE_LOCAL_MOVE, value: "Local", label: locationHelper.translate("WITHIN_SAME_COUNTRY")};
            formData = {
                Steps: Steps, loadForm: <QuotesMovingForm
                    servicesOptions={servicesOptions}
                    defaultServiceId={defaultServiceId}
                    defaultServiceOption={defaultServiceOption}
                    parnetServiceId={parnetServiceId}
                    isPainting={isPainting}
                    quotesData={quotesData}
                    {...commonProps}
                    typeOfMove = {typeOfMove}
                />
            }
        }
        else if (match == url_constants.INTERNATIONAL_MOVER_PAGE_URI) {
            var Steps = [
                { id: 'section-request-form', title: locationHelper.translate('MOVE_DETAILS') },
                { id: 'section-company-selector', title: locationHelper.translate('SELECT_COMPANIES') },
                { id: 'payment-method-form', title: locationHelper.translate('CONTACT_DETAILS') }
            ];
            
            var typeOfMove = {id: service_constants.SERVICE_INTERNATIONAL_MOVE, value: "International", label: locationHelper.translate("INTERNATIONALLY")};

            formData = {
                Steps: Steps, loadForm:
                    <QuotesMovingForm
                        quotesData={quotesData}
                        defaultServiceOption={defaultServiceOption}
                        {...commonProps}
                        typeOfMove = {typeOfMove}
                    />
            }
        }
        else if (match == url_constants.STORAGE_COMPANIES_PAGE_URI) {
            var Steps = [
                { id: 'section-request-form', title: 'What you need' },
                { id: 'section-company-selector', title: locationHelper.translate('SELECT_COMPANIES') },
                { id: 'payment-method-form', title: locationHelper.translate('YOUR_DETAILS') }
            ];
            formData = {
                Steps: Steps, loadForm:
                    <QuotesStoragePage
                        quotesData={quotesData}
                        defaultServiceOption={defaultServiceOption}
                        {...commonProps}
                    />
            }

        }
        else if (match == url_constants.CAR_SHIPPING_PAGE_URI) {
            var Steps = [
                { id: 'section-request-form', title: 'What you need' },
                { id: 'section-company-selector', title: locationHelper.translate('SELECT_COMPANIES') },
                { id: 'payment-method-form', title: locationHelper.translate('YOUR_DETAILS') }
            ];
            formData = {
                Steps: Steps, loadForm:
                    <QuotesCarShippingPage
                        quotesData={quotesData}
                        defaultServiceOption={defaultServiceOption}
                        {...commonProps}
                    />
            }
        }

        else if (match == url_constants.BABYSITTING_PAGE_URI) {
            var Steps = [
                { id: 'section-request-form', title: locationHelper.translate('DETAILS') },
                { id: 'section-company-selector', title: locationHelper.translate('SELECT_COMPANIES') },
                { id: 'payment-method-form', title: locationHelper.translate('CONTACT_DETAILS') }
            ];
            formData = {
                Steps: Steps, loadForm: <QuotesBabySittingPage
                    quotesData={quotesData}
                    defaultServiceOption={defaultServiceOption}
                    {...commonProps}
                />
            };
        }
        else if (match == url_constants.CATERING_PAGE_URI) {
            var Steps = [
                { id: 'section-request-form', title: locationHelper.translate('DETAILS') },
                { id: 'section-company-selector', title: locationHelper.translate('SELECT_COMPANIES') },
                { id: 'payment-method-form', title: locationHelper.translate('CONTACT_DETAILS') }
            ];
            var parnetServiceId = service_constants.SERVICE_CATERING;

            if (typeof passedService.serviceId != "undefined" && passedService.serviceId != parnetServiceId) {
                defaultServiceId = passedService.serviceId;
            }

            defaultServiceOption = this.getDefaultServiceOption(parnetServiceId, defaultServiceId);

            var selecteCuisine = [];
            if (typeof quotesData.cuisine != "undefined" && quotesData.cuisine.length) {
                selecteCuisine = quotesData.cuisine
            }
            formData = {
                Steps: Steps, loadForm: <QuotesCateringPage
                    defaultServiceOption={defaultServiceOption}
                    quotesData={quotesData}
                    selecteCuisine={selecteCuisine}
                    {...commonProps}
                />
            };
        }
        else if (match == url_constants.PHOTOGRAPHY_PAGE_URI) {
            var Steps = [
                { id: 'section-request-form', title: locationHelper.translate('DETAILS') },
                { id: 'section-company-selector', title: locationHelper.translate('SELECT_COMPANIES') },
                { id: 'payment-method-form', title: locationHelper.translate('CONTACT_DETAILS') }
            ];
            var parnetServiceId = service_constants.SERVICE_PHOTOGRAPHY;

            if (typeof passedService.serviceId != "undefined" && passedService.serviceId != parnetServiceId) {
                defaultServiceId = passedService.serviceId;
            }

            defaultServiceOption = this.getDefaultServiceOption(parnetServiceId, defaultServiceId);

            formData = {
                Steps: Steps, loadForm: <QuotesPhotographyPage
                    quotesData={quotesData}
                    defaultServiceOption={defaultServiceOption}
                    {...commonProps}
                />
            };


        }
        else if (match == url_constants.CURTAINS_PAGE_URI) {
            var Steps = [
                { id: 'section-request-form', title: 'Describe your requirements' },
                { id: 'section-company-selector', title: locationHelper.translate('SELECT_COMPANIES') },
                { id: 'payment-method-form', title: locationHelper.translate('CONTACT_DETAILS') }
            ];
            formData = {
                Steps: Steps, loadForm: <QuotesCurtainsPage
                    quotesData={quotesData}
                    defaultServiceOption={defaultServiceOption}
                    {...commonProps}
                />
            };
        }
        /*else if (match == url_constants.PAINTERS_PAGE_URI) {
            var Steps = [
                { id: 'section-request-form', title: locationHelper.translate('DETAILS') },
                { id: 'section-company-selector', title: 'Company Selection' },
                { id: 'payment-method-form', title: 'Contact Details' }
            ];
            formData = { Steps: Steps, loadForm: <QuotesPaintingPage showLoginModal={this.showLoginModal} userProfile={userProfile} signInDetails={signInDetails} url_constants={url_constants} formCurrentStep={currentStep} moveNext={this.moveNext} service_constants={service_constants} dataConstants={dataConstants} current_city={city} isSkipCompany={this.state.isSkipCompany} /> }
        }*/

        return formData;
    }
    howToProceed(isSkipCompany, stateData) {

        var nextStep = isSkipCompany ? 3 : 2;

        var selectedCompanies = isSkipCompany ? [] : stateData.selectedCompanies;

        this.setState({
            isSkipCompany: isSkipCompany
        })

        this.moveNextStep(nextStep, stateData);

    }
    moveNextStep(step, stateData) {

        var { selectedCompanies } = stateData;

        const url_constants = URLCONSTANT;

        const slug = this.props.url_params.params.slug;

        const allServiceConstant = this.props.service_constants;

        const { signInDetails, showLoginMenu, lang } = this.props;

        var continueAsGuest = typeof showLoginMenu.guest != "undefined" && showLoginMenu.guest == false ? true : false

        var formCurrentStep = this.state.currentStep;

        var valid = true;

        var typeOfJourney = this.typeOfJourney();

        if (typeOfJourney != 1) { // for journey 2 and journey 3
            formCurrentStep = formCurrentStep - 1;
        }

        if (formCurrentStep == '1') {
            valid = isValidSection('section-request-form', lang);
            if (valid) {
                window.location.hash = step;
                scrollToTop();
                // Show Login Modal
                if (((!signInDetails.customer && step == 3)) && !continueAsGuest) {
                    this.showLoginModal(true);
                }
            }
        }
        // Step 2
        if (formCurrentStep == '2') {
            if (selectedCompanies.length != 0) {
                this.showOrHideModalSelectCompanyError(false);
                window.location.hash = step;
                scrollToTop();
                if (((!signInDetails.customer && step == 3)) && !continueAsGuest) {
                    this.showLoginModal(true);
                }
            } else {
                this.showOrHideModalSelectCompanyError(true);
            }
        }
    }
    showOrHideModalSelectCompanyError(boolVal) {
        this.setState({
            showSelectCompanyError: boolVal
        })
    }
    submitLeadData(stateData) {
        const { showLoginModal, userProfile, showLoginMenu, lang } = this.props;
        var isUserLoggedIn = typeof userProfile == "object" && Object.keys(userProfile).length ? true : false;
        var continueAsGuest = typeof showLoginMenu.guest != "undefined" && showLoginMenu.guest == false ? true : false

        if (isUserLoggedIn || continueAsGuest) {
            var valid = isValidSection('personal-information-form', lang);
            if (valid) {
                this.setState({
                    loader: true
                });
                if (isUserLoggedIn) {
                    this.submitPostRequest(stateData);
                }
                else {
                    this.signUpThenSignIn(stateData.input_email, stateData.input_name, stateData.input_last_name, '').then((res) => {
                        this.submitPostRequest(stateData);
                    });
                }
            }
        } else {
            this.showLoginModal(false);
        }
    }
    /*getEventDate(move_date, timeInterval = 14400){
        if(this.props.lang == LANG_AR){
            moment.locale("ar_SA")
        }

        move_date = move_date.split("-").reverse().join('-');

        var expectedMoveDate = moment(move_date).unix() + timeInterval;
        
        expectedMoveDate = this.props.lang == LANG_AR ? moment.unix(expectedMoveDate).locale('en').format("MMMM DD, YYYY HH:mm") : moment.unix(expectedMoveDate).format("MMMM DD, YYYY HH:mm"); 

        return expectedMoveDate;
    }*/
    getEventDate(move_date, timeInterval = 14400){
        /*if(this.props.lang == LANG_AR){
            moment.locale("ar_SA")
        }*/

        move_date = move_date.split("-").reverse().join('-');

        var expectedMoveDate = moment(move_date).unix() + timeInterval;
        
        expectedMoveDate = moment.unix(expectedMoveDate).format("MMMM DD, YYYY HH:mm"); 

        return expectedMoveDate;
    }
    generateMoveDataForSubmission(stateData) {
        const allServiceConstant = this.props.service_constants;
        var {
            type_of_move,
            home_type,
            rooms,
            moving_size,
            from_emirate,
            from_area,
            from_address,
            to_emirate,
            to_area,
            to_address,
            other_services,
            other_comments_text,
            item_lists,
            move_date,
            input_email,
            input_phone,
            input_name,
            input_last_name,
            selectedCompanies,
            selectedOffers,
            isSkipCompany
        } = stateData;

        var service_id = parseInt(allServiceConstant.SERVICE_LOCAL_MOVE);

        var serviceLocationIdsToAdd = parseInt(from_emirate.id);

        var serviceIds = parseInt(commonHelper.sizeToServiceConverter(moving_size.value, service_id));
        
        var moving_size_value = commonHelper.movingSizeIdConverter(moving_size.value);

        var expectedMoveDate = this.getEventDate(move_date, 28800)
        
        var otherServices = [];
        if (other_services.length) {
            other_services.map((item) => {
                if(item.localServiceId !=0 ){
                    otherServices.push({ serviceId: item.localServiceId });
                }
            })
        }

        var fromCityID = locationHelper.getServiceLocationById(from_emirate.id);

        var toCityID = locationHelper.getServiceLocationById(to_emirate.id);

        var data = {
            "requestModel": {
                "customerOtherDetails": other_comments_text,
                "contactInformationModel": {
                    "personName": input_name + " " + input_last_name,
                    "personPhone": input_phone,
                    "personEmail": input_email
                },
                "moveRequestModel": {
                    "serviceId": serviceIds,
                    "movingSizeId": moving_size_value,
                    "fromAddress": {
                        "addressLine1": from_area + ", " + from_address,
                        "city": fromCityID
                    },
                    "toAddress": {
                        "addressLine1": to_area + ", " + to_address,
                        "city": toCityID
                    },
                    "expectedMoveDate": expectedMoveDate,
                    "moveDateFinal": false
                },
                "otherRequiredServiceModels": otherServices,
                "carMoveRequestModel": null,
                "serviceLocationIdsToAdd": [serviceLocationIdsToAdd],
                "customerId": this.props.signInDetails.customer.id
            }
        }
        var serviceProviderIds = [];

        var serviceProviderName = [];

        if ((!isSkipCompany && selectedCompanies.length) || this.typeOfJourney() != 1) {
            selectedCompanies.map((item) => {
                serviceProviderIds.push(item.id);
                serviceProviderName.push(item.name)
            })
            data["requestModel"]["serviceProviderIds"] = serviceProviderIds;
        }
        if (selectedOffers.length) {
            var confirmationDeals = [];
            selectedOffers.map((item) => {
                confirmationDeals.push(item.id);
            });
            data['claimOfferModel'] = { dealIds: [], requestServiceTypeId: 0 };
            data['claimOfferModel']['dealIds'] = confirmationDeals;
            data['claimOfferModel']['requestServiceTypeId'] = 1;
        }

        return data;

    }

    generatePhotographyDataForSubmission(stateDate) {
        let {

            other_services,
            other_comments_text,
            calender_date_of_service,
            dates_flexible,
            input_email,
            input_phone,
            input_name,
            input_last_name,
            selectedCompanies,
            selectedOffers,
            isSkipCompany,
            storage_size,
            item_lists,
            needed_services,
            event_city,
            service
        } = stateDate;

        let service_id = parseInt(service.value);

        let serviceLocationIdsToAdd = locationHelper.getCurrentCityId();//{ //serviceLocationIdsToAddparseInt(from_emirate.id);

        let expectedMoveDate = calender_date_of_service.split("-").reverse().join('-');
        expectedMoveDate = moment(expectedMoveDate).unix() + 14400;

        expectedMoveDate = moment.unix(expectedMoveDate).format("YYYY-MM-DD  HH:mm"); //MMMM DD, YYYY HH:mm
        let otherServices = [];



        if (other_services && other_services.length) {
            other_services.map((item) => {
                otherServices.push({ serviceId: item.id });
            })
        }
        let additionalItems = '';
        if (item_lists && item_lists.length) {
            other_services.map((item) => {
                additionalItems = additionalItems + '' + item.value + ', ';
            })
        }
        var eventCityID = event_city.id == SHARJAH_ID ? SHARJAH_MAPPED_ID : event_city.id;

        let data = {
            "serviceId": service_id,
            "extraInfo": other_comments_text,
            "isDateFlexible": dates_flexible.value,
            "eventDate": expectedMoveDate,
            "photographyType": needed_services.value,
            "addressModel": {
                "apartment": null,
                "building": null,
                "area": null,
                "addressLine1": null,
                "cityId": eventCityID
            },
            "baseRequest": {
                "contactInformationModel": {
                    "personName": input_name,
                    "personLastName": input_last_name,
                    "personEmail": input_email,
                    "personPhone": input_phone
                },
                "autoAllocateProviders": 0,
                "customerId": this.props.signInDetails.customer.id
            },
            "serviceLocationIdsToAdd": [serviceLocationIdsToAdd]
        }

        let serviceProviderIds = [];
        let serviceProviderName = [];

        if (!isSkipCompany && selectedCompanies && selectedCompanies.length) {
            selectedCompanies.map((item) => {
                serviceProviderIds.push(item.id);
                serviceProviderName.push(item.name)
            })
            data["baseRequest"]["serviceProviderIds"] = serviceProviderIds;
        }

        if (selectedOffers && selectedOffers.length) {
            let confirmationDeals = [];
            selectedOffers.map((item) => {
                confirmationDeals.push(item.id);
            });
            data['claimOfferModel'] = { dealIds: [], requestServiceTypeId: 0 };
            data['claimOfferModel']['dealIds'] = confirmationDeals;
            data['claimOfferModel']['requestServiceTypeId'] = 1;
        }

        return data;

    }

    geneateIntlMovingData(stateData) {
        var {
            moving_size,
            from_address,
            field_to,
            field_from,
            to_address,
            other_services,
            other_comments_text,
            move_date,
            input_email,
            input_phone,
            input_name,
            input_last_name,
            selectedCompanies,
            selectedOffers,
            isSkipCompany,
            cars_shipped,
            countries
        } = stateData;
        const allServiceConstant = this.props.service_constants;

        var service_id = parseInt(allServiceConstant.SERVICE_INTERNATIONAL_MOVE);

        var serviceLocationIdsToAdd = locationHelper.getCurrentCityId();//{ //serviceLocationIdsToAddparseInt(from_emirate.id);

        var serviceIds = parseInt(commonHelper.sizeToServiceConverter(moving_size.value, service_id));

        var moving_size_value = commonHelper.movingSizeIdConverter(moving_size.value);
        
        let expectedMoveDate = this.getEventDate(move_date);

        var otherServices = [];
        
        const carShippingServiceId = allServiceConstant.SERVICE_CAR_SHIPPING_WITH_MOVE_INTERNATIONAL_MOVE;
        
        let carShippedSelected = false;

        if (other_services.length) {
            other_services.map((item) => {
                if (item.intlServiceId == carShippingServiceId)
                    carShippedSelected = true;
                otherServices.push({ serviceId: item.intlServiceId });
            })
        }

        const countryOptions = countries;//COUNTRIES_LIST();
        const toCountryObj = countryOptions.find(c => c.label === field_to)
        //if(toCountryObj) const toCountryId = toCountryObj.id;
        const fromCountryObj = countryOptions.find(c => c.label === field_from);
        //if(fromCountryObj) const fromCountryId.id;

        var data = {
            "requestModel": {
                "customerOtherDetails": other_comments_text,
                "contactInformationModel": {
                    "personName": input_name + " " + input_last_name,
                    "personPhone": input_phone,
                    "personEmail": input_email
                },
                "moveRequestModel": {
                    "serviceId": serviceIds,
                    "movingSizeId": moving_size_value,
                    "fromAddress": {
                        "addressLine1": from_address,
                        "countryId": fromCountryObj ? fromCountryObj.id : null
                    },
                    "toAddress": {
                        "addressLine1": to_address,
                        "countryId": toCountryObj ? toCountryObj.id : null
                    },
                    "expectedMoveDate": expectedMoveDate,
                    "moveDateFinal": false
                },
                "otherRequiredServiceModels": otherServices,
                "carMoveRequestModel": null,
                "serviceLocationIdsToAdd": [serviceLocationIdsToAdd],
                "customerId": this.props.signInDetails.customer.id,

            }
        }
        let serviceProviderIds = [];
        let serviceProviderName = [];

        if (!isSkipCompany && selectedCompanies && selectedCompanies.length) {
            selectedCompanies.map((item) => {
                serviceProviderIds.push(item.id);
                serviceProviderName.push(item.name)
            })
            data["requestModel"]["serviceProviderIds"] = serviceProviderIds;
            data["requestModel"]["autoAllocateProviders"] = 0;
        }

       if (carShippedSelected) {
            /* let carsModel = cars_shipped.map(c => {
                const item = {
                    "carModelName": (c.field_car_trim && c.field_car_trim.label) ? c.field_car_trim.label : '',
                    "carMake": (c.field_car_make && c.field_car_make.label) ? c.field_car_make.label : '',
                    "carModelYear": (c.field_car_year && c.field_car_year.label) ? c.field_car_year.label : '',
                };
                return item;
            });*/
            
            let carsModel =  [{
                "carModelName": '',
                "carMake": '',
                "carModelYear":'',
            }];

            const carShippingDetailModels = {
                'carShippingDetailModels': carsModel,
                'serviceId': carShippingServiceId,
                "fromAddress": {
                    "addressLine1": from_address,
                    "countryId": fromCountryObj ? fromCountryObj.id : null
                },
                "toAddress": {
                    "addressLine1": to_address,
                    "countryId": toCountryObj ? toCountryObj.id : null
                },
                "expectedMoveDate": expectedMoveDate
            };
            data["requestModel"]["carMoveRequestModel"] = carShippingDetailModels;

        }
        
        if (selectedOffers && selectedOffers.length) {
            var confirmationDeals = [];
            selectedOffers.map((item) => {
                confirmationDeals.push(item.id);
            });
            data['claimOfferModel'] = { dealIds: [], requestServiceTypeId: 0 };
            data['claimOfferModel']['dealIds'] = confirmationDeals;
            data['claimOfferModel']['requestServiceTypeId'] = 1;
        }

        return data;
    }
    generateStorageData(stateData) {
        var {
            other_services,
            other_comments_text,
            item_lists,
            calender_date_of_service_start,
            calender_date_of_service_end,
            input_email,
            input_phone,
            input_name,
            input_last_name,
            selectedCompanies,
            selectedOffers,
            isSkipCompany,
            storage_size,
            item_lists,
            storage_use
        } = stateData;
        const allServiceConstant = this.props.service_constants;

        var service_id = parseInt(storage_use.id); //parseInt(allServiceConstant.SERVICE_STORAGE);

        const city = locationHelper.getCurrentCityDetails();
        //console.log('city details',city);
        const serviceLocationIdsToAdd = city.id;//{ //serviceLocationIdsToAddparseInt(from_emirate.id);
        const cityId = city.cityId;


        calender_date_of_service_start = calender_date_of_service_start.split("-").reverse().join('-');
        calender_date_of_service_end = calender_date_of_service_end.split("-").reverse().join('-');

        let expectedMoveDateStart = moment(calender_date_of_service_start).unix() + 14400;
        let expectedMoveDateEnd = moment(calender_date_of_service_end).unix() + 14400;

        expectedMoveDateStart = moment.unix(expectedMoveDateStart).format("MMMM DD, YYYY HH:mm");
        expectedMoveDateEnd = moment.unix(expectedMoveDateEnd).format("MMMM DD, YYYY HH:mm");
        let otherServices = [];

        if (other_services.length) {
            other_services.map((item) => {
                otherServices.push({ serviceId: item.id });
            })
        }
        let additionalItems = '';
        if (item_lists.length) {
            other_services.map((item) => {
                additionalItems = additionalItems + '' + item.value + ', ';
            })
        }

        var data = {
            "requestModel": {
                "customerOtherDetails": other_comments_text,
                "contactInformationModel": {
                    "personName": input_name + " " + input_last_name,
                    "personPhone": input_phone,
                    "personEmail": input_email
                },
                "storageRequestModel": {
                    "sizeToBeRentedOut": storage_size.value,
                    "storageReason": storage_use.value,
                    "storeItemTillDate": expectedMoveDateEnd,
                    "otherDetail": additionalItems,
                    "serviceId": service_id,
                    "expectedMoveDate": expectedMoveDateStart,
                    "storageLocationCityId": cityId,
                    "fromAddress": {
                        "addressLine1": " ",
                        "city": cityId
                    },
                    "toAddress": {
                        "addressLine1": " ",
                        "city": cityId
                    }
                },
                "otherRequiredServiceModels": otherServices,
                "serviceLocationIdsToAdd": [serviceLocationIdsToAdd],
                "customerId": this.props.signInDetails.customer.id
            }
        }
        var serviceProviderIds = [];
        var serviceProviderName = [];

        if (!isSkipCompany && selectedCompanies && selectedCompanies.length) {
            selectedCompanies.map((item) => {
                serviceProviderIds.push(item.id);
                serviceProviderName.push(item.name)
            })
            data["requestModel"]["serviceProviderIds"] = serviceProviderIds;
            data["requestModel"]["autoAllocateProviders"] = 0;
        }

        if (selectedOffers && selectedOffers.length) {
            var confirmationDeals = [];
            selectedOffers.map((item) => {
                confirmationDeals.push(item.id);
            });
            data['claimOfferModel'] = { dealIds: [], requestServiceTypeId: 0 };
            data['claimOfferModel']['dealIds'] = confirmationDeals;
            data['claimOfferModel']['requestServiceTypeId'] = 1;
        }
        /*if ((!isSkipCompany && selectedCompanies.length) || this.typeOfJourney() != 1) {

            selectedCompanies.map((item) => {
                serviceProviderIds.push(item.id)
            });
            data["baseRequest"]["serviceProviderIds"] = serviceProviderIds;
            data["baseRequest"]["autoAllocateProviders"] = 0;

        } else {

            data["baseRequest"]["autoAllocateProviders"] = 1;
        }*/
        return data;

    }

    submitPostRequest(stateData) {
        scrollToTop();
        const { cookies,url_params } = this.props;
        const url_constants = URLCONSTANT;

        const allServiceConstant = this.props.service_constants;

        const slug = url_params.params.slug;

        const city = url_params.params.city;

        const lang = url_params.params.lang;

        var url = window.location.protocol + '//' + window.location.host;

        url += "/" + lang + "/" + city + "/" + slug + "/journey" + this.state.journey;

        var queryParams = {};

        var rid = "";

        var data = "";

        var submissionEndPoint = "/request/add";
        let moveDetails = {};

        if (slug == url_constants.BABYSITTING_PAGE_URI) {
            data = this.generateBabySittingData(stateData);
            queryParams["service_id"] = data["serviceId"];
            submissionEndPoint = "/babysitters";
        } else if (slug == url_constants.CURTAINS_PAGE_URI) {
            data = this.generateCurtainsData(stateData);
            queryParams["service_id"] = stateData.service;

        } else if (slug == url_constants.CATERING_PAGE_URI) {
            data = this.generateCateringData(stateData);
            queryParams["service_id"] = data["serviceId"];
            submissionEndPoint = "/catering";
        } else if (slug == url_constants.CAR_SHIPPING_PAGE_URI) {
            data = this.generateCarShippingData(stateData);
            queryParams["service_id"] = data["serviceId"];
        }
        /*else if (slug == url_constants.LOCAL_MOVER_PAGE_URI) {
            data = this.generateMoveDataForSubmission(stateData);
            queryParams["service_id"] = allServiceConstant.SERVICE_LOCAL_MOVE;
        }*/
        else if (slug == url_constants.PHOTOGRAPHY_PAGE_URI) {
            data = this.generatePhotographyDataForSubmission(stateData);
            queryParams["service_id"] = stateData.service.value;
            submissionEndPoint = "/photography";
        }
        else if (slug == url_constants.STORAGE_COMPANIES_PAGE_URI) {
            data = this.generateStorageData(stateData);
            queryParams["service_id"] = data.requestModel.storageRequestModel.serviceId;
            //allServiceConstant.SERVICE_STORAGE;
        }
        else if (slug == url_constants.INTERNATIONAL_MOVER_PAGE_URI || slug == url_constants.LOCAL_MOVER_PAGE_URI) {
            url = window.location.protocol + '//' + window.location.host;

            var {type_of_move} = stateData;
            stateData.userAddress = this.props.userProfile.address; 
            if(type_of_move.id == allServiceConstant.SERVICE_INTERNATIONAL_MOVE){
                data = this.geneateIntlMovingData(stateData);
                queryParams["service_id"] = allServiceConstant.SERVICE_INTERNATIONAL_MOVE;
                url += "/" + lang + "/" + city + "/" + url_constants.INTERNATIONAL_MOVER_PAGE_URI + "/journey" + this.state.journey;
                
            }else{
                data = this.generateMoveDataForSubmission(stateData);
                queryParams["service_id"] = allServiceConstant.SERVICE_LOCAL_MOVE;
                url += "/" + lang + "/" + city + "/" + url_constants.LOCAL_MOVER_PAGE_URI + "/journey" + this.state.journey;
                
                let movingTotext =  stateData.to_emirate.label;

                movingTotext += stateData.to_area != "" ? " - " + stateData.to_area : "";

                movingTotext += "- "+stateData.to_address;

                moveDetails = {
                    custName: stateData.input_name+" "+stateData.input_last_name,
                    custEmail: stateData.input_email,
                    custPhone: stateData.input_phone,
                    custAddress: movingTotext,
                    moveDate:stateData.move_date
                }
                
            }
        }
        else {
            data = this.generateHouseholdRequestData(stateData);
            queryParams["service_id"] = stateData.service.value;
        }

        if(slug != url_constants.INTERNATIONAL_MOVER_PAGE_URI){
            //console.log("url_constants.INTERNATIONAL_MOVER_PAGE_URI", Object.keys(moveDetails).length);
            if(!Object.keys(moveDetails).length){
                moveDetails = {
                    custName: stateData.input_name+" "+stateData.input_last_name,
                    custEmail: stateData.input_email,
                    custPhone: stateData.input_phone,
                    custAddress: '',
                    moveDate:''
                }
            }
            cookies.set( this.getCookieName("local_move_details"), moveDetails, { path: '/', maxAge: 300 });
        }

        queryParams["fromEmirate"] = locationHelper.getCurrentCityId();

        var serviceProviderName = [];

        var {
            selectedCompanies
        } = stateData;

        var { isSkipCompany } = this.state;

        if ((!isSkipCompany && selectedCompanies.length) || this.typeOfJourney() != 1) {
            selectedCompanies.map((item) => {
                serviceProviderName.push(item.name)
            })
        }

        let addressDetails = {}, updateUserSelectedArea = true;
        if (typeof stateData.input_address_area != "undefined" && stateData.input_address_area != null) {
            
            var city_object = stateData.input_address_city;

            console.log("city_object",city_object, stateData);

            var city_id = city_object.id == SHARJAH_ID ? SHARJAH_MAPPED_ID : city_object.id;

            addressDetails = {
                "apartment": stateData.input_address_area_building_apartment,
                //"area": stateData.input_address_area.id,
                "building": stateData.input_address_area_building_name,
                "cityId": city_id,
                "phoneNumber": stateData.input_phone
            };

        } else if(typeof stateData.userAddress != "undefined" && stateData.userAddress != null) { // International Moving case
            addressDetails = {
                "apartment": stateData.userAddress.apartment,
                //"area": stateData.userAddress.area_id,
                "building": stateData.userAddress.building,
                "cityId": stateData.userAddress.city_id,
                "phoneNumber": stateData.userAddress.phoneNumber,
            };

        }else {
            updateUserSelectedArea = false;
            
            var city_object = locationHelper.getLocationByNameLocationID(city);//getLocationByName(city);

            var area = null;
            area = this.setUserArea(this.props.userProfile);
            if (area != "" && area != null && (typeof area == "object" && typeof area.id != "undefined")) {
                area = area.id;
            }

            var city_id = city_object.id == SHARJAH_ID ? SHARJAH_MAPPED_ID : city_object.id;

            addressDetails = {
                //"area": area,
                "cityId": city_id,
                "phoneNumber": stateData.input_phone,
                "apartment": this.props.userProfile.address ? this.props.userProfile.address.apartment : '',
                "building": this.props.userProfile.address ? this.props.userProfile.address.building : ''
            };

            if(area != null && (typeof area == "object" && typeof area.id != "undefined")){
                addressDetails['area'] = area.id;
            }else if(typeof area == "string" && area != ""){
                addressDetails['area'] = null;
                addressDetails['areaTitle'] = area;
            }else{
                addressDetails['area'] = null;
            }
        }
        
        if(updateUserSelectedArea){ // for update Profile
            let userSelectedArea = this.getUserSelectedArea(stateData);
            if(userSelectedArea == null){
                addressDetails['area'] = null;
                addressDetails['areaTitle'] = this.getUserSelectedArea(stateData, true);
            }else{
                addressDetails['area'] = userSelectedArea;
            }
        }
        
        //console.log("addressDetails", addressDetails);

        //return false;
        let signedInUserId = this.props.userProfile && this.props.userProfile.signedInUserId ? this.props.userProfile.signedInUserId : 8;
        putUpdateCustomerProfile(this.props.signInDetails.access_token,
            this.props.signInDetails.customer.id,
            stateData.input_email,
            stateData.input_name,
            stateData.input_last_name, addressDetails, lang).then(() => {

                postRequest(this.props.signInDetails.access_token || '', JSON.stringify(data), submissionEndPoint, signedInUserId, lang).then((response) => {
                    var response_row = JSON.parse(response.data.addRequest);
                    var rid = 0;
                    if (response_row.success) {
                        var rows = typeof response_row.rows != "undefined" ? response_row.rows[0] : response_row.data;
                        rid = rows.requestUuid;
                        queryParams["status"] = "success";
                        if (serviceProviderName.length) {
                            queryParams["service_providers"] = JSON.stringify(serviceProviderName)
                        }
                        cookies.set('rid', rid, { path: '/', maxAge: 300 });
                    } else {
                        var status = 'OUCH- There was an error while fulfulling your request.' + JSON.stringify(response_row);
                        queryParams["status"] = status;
                        cookies.set('rid', 0, { path: '/', maxAge: 300 });
                    }
                    var query = commonHelper.objToParams(queryParams);
                    query = encodeURI(query);
                    //console.log("confirmation Query", url + "/confirmation/" + rid + "?" + query)
                    window.location = url + "/confirmation/" + rid + "?" + query;
                });
            });
    }
    getCookieName(cookie_name) {
        var url_params = this.props.url_params;

        var serviceURL = url_params.params.slug;

        var currentLanguage = url_params.params.lang;

        var current_city_name = url_params.params.city;

        var currentPath = serviceURL;

        var cookieName = currentLanguage + "." + current_city_name + "." + currentPath + "." + cookie_name;

        return cookieName;
    }
    getUserSelectedArea(stateData, returnText = false){
        /*let areaId = (stateData.input_address_area != null && typeof stateData.input_address_area.id != "undefined") ? stateData.input_address_area.id : null;
        areaId = ( typeof stateData.userAddress != "undefined" && (stateData.userAddress.area_id != null && typeof stateData.userAddress.area_id != "undefined")) ? stateData.userAddress.area_id : areaId;
        return returnText ? stateData.input_address_area : areaId;*/
        return commonHelper.getUserSelectedArea(stateData, returnText);
    }
    generateCateringData(stateData) {
        var {
            service,
            dateOfService,
            howManyGuest,
            budgetPerGuest,
            details,
            otherEqipments,
            cuisine,
            input_email,
            input_phone,
            input_name,
            input_last_name,
            eventCity,
            selectedCompanies
        } = stateData;

        var { isSkipCompany } = this.state;

        var serviceLocationIdsToAdd = locationHelper.getCurrentCityId(),
            serviceProviderIds = [],
            cateringCuisines = [],
            cateringEquips = [];

        if (otherEqipments.length) {
            otherEqipments.map((item) => {
                cateringEquips.push(parseInt(item.value));
            })
        }

        if (cuisine.length) {
            cuisine.map((item) => {
                cateringCuisines.push(parseInt(item.value));
            })
        }
        var eventCityID = eventCity.id == SHARJAH_ID ? SHARJAH_MAPPED_ID : eventCity.id;

        var data = {
            "requestModel": {
                "contactInformationModel": {
                    "personName": input_name,
                    "personLastName": input_last_name,
                    "personPhone": input_phone,
                    "personEmail": input_email
                },
                "cateringModel": {
                    "serviceId": service.value,
                    "numberOfGuest": howManyGuest,
                    "budgetPerGuest": budgetPerGuest.value,
                    "specialRequirements": details,
                    "cateringCuisines": cateringCuisines,
                    "cateringEquips": cateringEquips,
                    "eventDate": dateOfService.split("-").reverse().join('-'),
                    "eventTime": "4:00",
                    "addressModel": {
                        "apartment": null,
                        "building": null,
                        "area": null,
                        "addressLine1": null,
                        "cityId": eventCityID
                    }
                },
                "serviceLocationIdsToAdd": [serviceLocationIdsToAdd],
                "customerId": this.props.signInDetails.customer.id
            }
        }

        if ((!isSkipCompany && selectedCompanies.length) || this.typeOfJourney() != 1) {
            selectedCompanies.map((item) => {
                serviceProviderIds.push(item.id);
            });
            data["requestModel"]["serviceProviderIds"] = serviceProviderIds;
            data["requestModel"]["letMovesouqChoseProviders"] = 0;
        }

        return data;
    }
    generateBabySittingData(stateData) {
        const allServiceConstant = this.props.service_constants;
        var {
            neededServices,
            requiredDaysAWeek,
            requiredForHowLong,
            requiredHowLongTextField,
            noOfKids,
            kids,
            hoursNeeded,
            eventTime,
            dateOfService,
            details,
            input_email,
            input_phone,
            input_name,
            input_last_name,
            input_address_city,
            input_address_area,
            input_address_area_building_name,
            input_address_area_building_apartment,
            selectedCompanies
        } = stateData;

        var { isSkipCompany } = this.state;

        var serviceTime = neededServices.value == "1" ? eventTime.value : null;

        var days = neededServices.value == "2" ? requiredDaysAWeek.value : null;

        var childrenAgeIds = [];

        if (kids.length) {
            kids.map((kid) => {
                childrenAgeIds.push(kid.value);
            })
        }
        var eventCityID = input_address_city.id == SHARJAH_ID ? SHARJAH_MAPPED_ID : input_address_city.id;

        var serviceLocationIdsToAdd = locationHelper.getCurrentCityId(),
            serviceProviderIds = [];
        var data = {
            "typeId": neededServices.value,
            "serviceTime": serviceTime,
            "days": requiredDaysAWeek.value,
            "extraInformation": details,
            "serviceDate": dateOfService.split("-").reverse().join('-'),
            "hours": hoursNeeded.value,
            "serviceId": allServiceConstant.SERVICE_PARTTIME_NANNIES__BABYSITTERS,
            "children": noOfKids.value,
            "childrenAgeIds": childrenAgeIds,
            "addressModel": {
                "apartment": input_address_area_building_apartment,
                "building": input_address_area_building_name,
                //"area": input_address_area.id,
                "addressLine1": null,
                "cityId": eventCityID
            },
            "baseRequest": {
                "contactInformationModel": {
                    "personName": input_name,
                    "personLastName": input_last_name,
                    "personPhone": input_phone,
                    "personEmail": input_email
                },
                "serviceProviderIds": [],
                "autoAllocateProviders": 0,
                "customerId": this.props.signInDetails.customer.id
            },
            "serviceLocationIdsToAdd": [serviceLocationIdsToAdd]
        };

        if (neededServices.value == "2") {
            data["weeks"] = requiredForHowLong.value == "WEEKS" && requiredHowLongTextField != 0 ? parseInt(requiredHowLongTextField).toFixed(1) : null;

            data["years"] = requiredForHowLong.value == "YEARS" && requiredHowLongTextField != 0 ? parseInt(requiredHowLongTextField).toFixed(1) : null;

            data["months"] = requiredForHowLong.value == "MONTHS" && requiredHowLongTextField != 0 ? parseInt(requiredHowLongTextField).toFixed(1) : null;
        }

        if ((!isSkipCompany && selectedCompanies.length) || this.typeOfJourney() != 1) {

            selectedCompanies.map((item) => {
                serviceProviderIds.push(item.id)
            });
            data["baseRequest"]["serviceProviderIds"] = serviceProviderIds;
            data["baseRequest"]["autoAllocateProviders"] = 0;

        } else {

            data["baseRequest"]["autoAllocateProviders"] = 1;
        }

        let userSelectedArea = this.getUserSelectedArea(stateData);
        if(userSelectedArea == null){
            //"area": this.getUserSelectedArea(stateData),
            data['addressModel']['area'] = null;
            data['addressModel']['areaTitle'] = this.getUserSelectedArea(stateData, true);
        }else{
            data['addressModel']['area'] = userSelectedArea;
        }

        return data;

    }
    generateCurtainsData(stateData) {
        let {
            other_comments_text,
            calender_date_of_service,
            input_email,
            input_phone,
            input_name,
            input_last_name,
            input_address_city,
            input_address_area,
            input_address_area_building_name,
            input_address_area_building_apartment,
            selectedCompanies,
            selectedOffers,
            isSkipCompany,
            window_coverings_types,
            no_of_windows,
            premises_to_install,
            service,
        } = stateData;

        var serviceLocationIdsToAdd = locationHelper.getCurrentCityId();//{ //serviceLocationIdsToAddparseInt(from_emirate.id);

        calender_date_of_service = calender_date_of_service.split("-").reverse().join('-');


        let expectedMoveDate = moment(calender_date_of_service).unix() + 14400;

        expectedMoveDate = moment.unix(expectedMoveDate).format("YYYY-MM-DD  HH:mm");

        var eventCityID = serviceLocationIdsToAdd == SHARJAH_ID ? SHARJAH_MAPPED_ID : serviceLocationIdsToAdd;

        var data = {
            "requestModel": {
                "contactInformationModel": {
                    "personName": input_name,
                    "personLastName": input_last_name,
                    "personPhone": input_phone,
                    "personEmail": input_email
                },
                "curtainRequestModel": {
                    "service": service,
                    "dateOfSurvey": expectedMoveDate,
                    "typeOfCovering": window_coverings_types.value,
                    "noOfWindows": no_of_windows,
                    "locationType": premises_to_install.value,
                    "additionalInfo": other_comments_text,
                    "addressModel": {
                        "apartment": input_address_area_building_apartment,
                        "building": input_address_area_building_name,
                        //"area": input_address_area.id,
                        "cityId": eventCityID,
                    },
                },

                "letMovesouqChoseProviders": 0,
                "serviceLocationIdsToAdd": [serviceLocationIdsToAdd],
                "customerId": this.props.signInDetails.customer.id
            }
        }
        var serviceProviderIds = [];
        var serviceProviderName = [];

        if ((!isSkipCompany && selectedCompanies.length) || this.typeOfJourney() != 1) {
            selectedCompanies.map((item) => {
                serviceProviderIds.push(item.id);
                serviceProviderName.push(item.name)
            })
            data["requestModel"]["serviceProviderIds"] = serviceProviderIds;
            data["requestModel"]["letMovesouqChoseProviders"] = 0;
        }

        if (selectedOffers && selectedOffers.length) {
            var confirmationDeals = [];
            selectedOffers.map((item) => {
                confirmationDeals.push(item.id);
            });
            data['claimOfferModel'] = { dealIds: [], requestServiceTypeId: 0 };
            data['claimOfferModel']['dealIds'] = confirmationDeals;
            data['claimOfferModel']['requestServiceTypeId'] = 1;
        }
        let userSelectedArea = this.getUserSelectedArea(stateData);
        if(userSelectedArea == null){
            //"area": this.getUserSelectedArea(stateData),
            data['requestModel']['curtainRequestModel']['addressModel']['area'] = null;
            data['requestModel']['curtainRequestModel']['addressModel']['areaTitle'] = this.getUserSelectedArea(stateData, true);
        }else{
            data['requestModel']['curtainRequestModel']['addressModel']['area'] = userSelectedArea;
        }
        return data;
    }
    generateCarShippingData(stateData) {
        let {
            other_comments_text,
            move_date,
            input_email,
            input_phone,
            input_name,
            input_last_name,

            selectedCompanies,
            selectedOffers,
            isSkipCompany,
            service,
            sourceCountryId,
            targetCountryId,
            cars_shipped,
            from_address,
            to_address
        } = stateData;

        var serviceLocationIdsToAdd = locationHelper.getCurrentCityId();//{ //serviceLocationIdsToAddparseInt(from_emirate.id);

        move_date = move_date.split("-").reverse().join('-');


        let expectedMoveDate = moment(move_date).unix() + 14400;

        expectedMoveDate = moment.unix(expectedMoveDate).format("MMMM DD, YYYY HH:mm");


        var data = {
            "requestModel": {
                "contactInformationModel": {
                    "personName": input_name + ' ' + input_last_name,
                    "personPhone": input_phone,
                    "personEmail": input_email
                },
                "customerOtherDetails": other_comments_text,
                "carMoveRequestModel": {
                    "fromAddress": {
                        "addressLine1": from_address,
                        "countryId": sourceCountryId
                    },
                    "toAddress": {
                        "addressLine1": to_address,
                        "countryId": targetCountryId
                    },
                    "serviceId": service,
                    "expectedMoveDate": expectedMoveDate,
                    'carShippingDetailModels': ''
                },
                "serviceLocationIdsToAdd": [serviceLocationIdsToAdd],
                "customerId": this.props.signInDetails.customer.id
            }
        }
        var serviceProviderIds = [];
        var serviceProviderName = [];

        if ((!isSkipCompany && selectedCompanies.length) || this.typeOfJourney() != 1) {
            selectedCompanies.map((item) => {
                serviceProviderIds.push(item.id);
                serviceProviderName.push(item.name)
            })
            data["requestModel"]["serviceProviderIds"] = serviceProviderIds;

        } else {
            data["requestModel"]["autoAllocateProviders"] = 1;
        }
        if (cars_shipped && cars_shipped.length) {
            //
            let carsModel = cars_shipped.map(c => {
                const item = {
                    "carModelName": (c.field_car_model && c.field_car_model.label) ? c.field_car_model.label : '',
                    "carMake": (c.field_car_make && c.field_car_make.label) ? c.field_car_make.label : '',
                    "carModelYear": (c.field_car_year && c.field_car_year.label) ? c.field_car_year.label : '',
                };
                return item;
            });

            data["requestModel"]["carMoveRequestModel"]['carShippingDetailModels'] = carsModel;

        }

        if (selectedOffers && selectedOffers.length) {
            var confirmationDeals = [];
            selectedOffers.map((item) => {
                confirmationDeals.push(item.id);
            });
            data['claimOfferModel'] = { dealIds: [], requestServiceTypeId: 0 };
            data['claimOfferModel']['dealIds'] = confirmationDeals;
            data['claimOfferModel']['requestServiceTypeId'] = 1;
        }
        return data;
    }
    generateHouseholdRequestData(stateData) {
        var {
            homeSize,
            service,
            homeType,
            numberOfUnits,
            dateOfService,
            field_service_needed,
            hours_required,
            field_own_equipments,
            details,
            input_email,
            input_phone,
            input_name,
            input_last_name,
            input_address_city,
            input_address_area,
            input_address_area_building_name,
            input_address_area_building_apartment,
            selectedCompanies,
            isPainting
        } = stateData;

        var { isSkipCompany } = this.state;

        var serviceID = service.value;

        var serviceProviderIds = [];
        var serviceProviderName = [];


        var serviceLocationIdsToAdd = locationHelper.getCurrentCityId();

        var move_date = dateOfService.split("-").reverse().join('-');

        let expectedServiceDate = this.getEventDate(dateOfService);

        var hoursRequired = typeof hours_required != "undefined" ? hours_required.value : null;

        var equipmentNeeded = typeof field_own_equipments != "undefined" ? field_own_equipments.value : null;

        var requestFrequency = typeof field_service_needed != "undefined" ? field_service_needed.value : null;

        var movingSizeId = typeof homeSize != "undefined" ? homeSize.value : null;

        if (movingSizeId != null) {
            movingSizeId = commonHelper.movingSizeIdConverter(movingSizeId);
        }

        var eventCityID = locationHelper.getServiceLocationById(input_address_city.id);
        
        var data = {
            "householdRequestModel": {
                "contactInformationModel": {
                    "personName": input_name + " " + input_last_name,
                    "personPhone": input_phone,
                    "personEmail": input_email
                },
                "serviceIds": [serviceID],
                "promoCode": null,
                "description": details,
                "address": {
                    "addressLine1": "",
                    "addressLine2": "",
                    "city": eventCityID,
                    //"area": input_address_area.id,
                    "building": input_address_area_building_name,
                    "apartment": input_address_area_building_apartment
                },

                "serviceLocationIdsToAdd": [serviceLocationIdsToAdd],
                "customerId": this.props.signInDetails.customer.id
            }
        }
        if (isPainting) {
            var typeOfHome = "",
                sizeOfHome = "";
            if (typeof homeType.value != "undefined") {
                typeOfHome = homeType.value;
                sizeOfHome = numberOfUnits.value;
            } else {
                var dataConstants = this.props.dataConstants;

                var typeofHomeDataValues = commonHelper.processDataValues(dataConstants["TYPE_OF_HOME_TO_PAINT"]);

                var apartmentUnitsDataValues = commonHelper.processDataValues(dataConstants["SIZE_OF_APARTMENT"]);

                var villaUnitsDataValues = commonHelper.processDataValues(dataConstants["SIZE_OF_VILLA"]);

                var allDataValues = {};

                allDataValues = Object.assign(
                    typeofHomeDataValues,
                    apartmentUnitsDataValues,
                    villaUnitsDataValues,
                    allDataValues
                );

                var dataValues = allDataValues;

                typeOfHome = dataValues.DATA_VALUE_HOME_TYPE_VILLA;
                sizeOfHome = dataValues.DATA_VALUE_1_BR_VILLA;
            }
            data["householdRequestModel"]["paintingHouseholdRequestModel"] = {
                "typeOfHome": typeOfHome,
                "sizeOfHome": sizeOfHome,
                "startDate1": dateOfService.split("-").reverse().join('-')
            }

        } else {
            data["householdRequestModel"]["serviceRequestTime"] = expectedServiceDate;
            data["householdRequestModel"]["hoursRequired"] = hoursRequired;
            data["householdRequestModel"]["equipmentNeeded"] = equipmentNeeded;
            data["householdRequestModel"]["requestFrequency"] = requestFrequency;
            data["householdRequestModel"]["movingSizeId"] = movingSizeId;
        }

        if ((!isSkipCompany && selectedCompanies.length) || this.typeOfJourney() != 1) {
            selectedCompanies.map((item) => {
                serviceProviderIds.push(item.id);
                serviceProviderName.push(item.name)
            });
            data["householdRequestModel"]["serviceProviderIds"] = serviceProviderIds;
            data["householdRequestModel"]["letMovesouqChoseProviders"] = 0;
        } else {
            data["householdRequestModel"]["letMovesouqChoseProviders"] = 1;
        }

        let userSelectedArea = this.getUserSelectedArea(stateData);
        //console.log("userSelectedArea", userSelectedArea, stateData);
        //return false;
        if(userSelectedArea == null){
            //"area": this.getUserSelectedArea(stateData),
            data['householdRequestModel']['address']['area'] = null;
            data['householdRequestModel']['address']['areaTitle'] = this.getUserSelectedArea(stateData, true);
        }else{
            data['householdRequestModel']['address']['area'] = userSelectedArea;
        }

        return data;
    }
    getDefaultServiceOption(parnetServiceId, defaultServiceId) {
        var defaultServiceOption = {};

        var servicesOptions = commonHelper.lookupServices('parent_service_id', parnetServiceId);

        if (servicesOptions.length) {

            defaultServiceOption = servicesOptions.filter(item => item.value == defaultServiceId);

            defaultServiceOption = defaultServiceOption.length ? defaultServiceOption[0] : {};
        }
        return defaultServiceOption;
    }
    componentWillReceiveProps(newProps) {
        let url_constants = URLCONSTANT;
        if (newProps.dataConstants !== this.props.dataConstants) {
            window.REDUX_DATA["dataConstants"] = newProps.dataConstants;
        }
        if (newProps.cities !== this.props.cities) {
            window.REDUX_DATA["cities"] = newProps.cities;
            locationHelper.allCites = newProps.cities;
            var areaData = this.setUserArea();
            this.setState({
                setUserArea: areaData
            })
        }
        if (newProps.newDataConstants !== this.props.newDataConstants) {
            window.REDUX_DATA["newDataConstants"] = newProps.newDataConstants;
        }
        if (newProps.dateAndTime !== this.props.dateAndTime) {
            //console.log("dateAndTime", dateAndTime);
            window.REDUX_DATA["dateAndTime"] = newProps.dateAndTime;
        }
        if (newProps.userProfile !== this.props.userProfile) {
            /*var url_params = this.props.url_params.params;
            const url_constants = URLCONSTANT;
            var serviceURL = url_params.slug;*/
            var areaData = this.setUserArea(newProps.userProfile);
            this.setState({
                setUserArea: areaData
            })
        }
    }
    setUserArea(userProfile = false) {
        let {lang, currentCity} = this.props;
        var userArea = "";
        var countryID = locationHelper.getCurrentCountryId();
        var userData = userProfile == false ? this.props.userProfile : userProfile;
        
        if((commonHelper.isUserLoggedIn() && (userData != false && userData.address != null)) && (userData.address.area_id == null)){
            userArea = userData.address.area;
        }else{
            if (commonHelper.isUserLoggedIn() && (userData != false && userData.address != null) ) {
                let userCity = (userData.address && userData.address.city) ? userData.address.city : '';
                userCity = (userCity != '' && lang == LANG_AR) ? userCity : commonHelper.slugify(userCity);
                if (userCity != "") {
                    //var customerCityId =  userData.customerCityId;
                    let customerCityId = userData.customerCityId;
                    
                    if(userData.customerCityId == SHARJAH_MAPPED_ID){
                        customerCityId = SHARJAH_ID;
                    }
                    else if(userData.customerCityId == DOHA_CITY_ID){
                        customerCityId = DOHA_ID;
                    }

                    var city = locationHelper.getLocationByName(userCity);
                    if( typeof city.id == "undefined" && typeof customerCityId != "undefined"){ //RW-1129
                        city = locationHelper.getLocationById(customerCityId);
                        if( typeof city.id == "undefined" ){
                            let serviceLocationById = locationHelper.getServiceLocationById(customerCityId,'itemId');
                            city = locationHelper.getLocationById(serviceLocationById);
                        }
                    }
                    var cityAreas = locationHelper.getAreasByCity(city.id);
                    if (currentCity == city.value && userData.address.area != null) {
                        userArea = locationHelper.getAreaByName(cityAreas, userData.address.area, lang);
                    }
                }
            }
        }
        //console.log("getServiceLocationById", userArea);
        return userArea;
    }
    render() {
        var url_params = this.props.url_params;

        var stepContent = "";
        var currentStep = this.state.currentStep;
        var form_meta = this.form_meta(url_params.params.slug, url_params.params.city);
        if (Object.keys(form_meta).length == 0 || typeof form_meta.Steps == "undefined") {
            return (<section className="error-404 image-404 text-center text-align-centre margin-top60">
                <h1>Wrong address?</h1>
                <p>The page you are looking for was moved, renamed, or may have never existed. </p>
                <p>Go <a href="/">home</a> or try a search:</p>
            </section>);
        } else {
            var Steps = typeof form_meta.Steps != "undefined" ? form_meta.Steps : [];
            var form = typeof form_meta.loadForm != "undefined" ? form_meta.loadForm : "";

            if (this.state.journey != 1 && typeof Steps != "undefined") {
                Steps = swapArrayElements(Steps, 1, 0);
            }
            //console.log("url_params", url_params);
            if (Steps.length) {
                return (
                    <React.Fragment>
                        <main className="container-1200 mb-120">
                            <TrackerNav typeOfFlow="quotes" Steps={Steps} formCurrentStep={currentStep} typeOfJourney={this.typeOfJourney()} />
                            <div className={isMobile() ? "container pt-3" : "container pt-4"}>
                                <div className="row">
                                    { this.state.loader ? (<main className="full-width" loader={"show"}><Loader /></main>) : form} 
                                </div>
                            </div>
                        </main>
                    </React.Fragment>
                );
            }
        }
    }
}

function mapStateToProps(state) {
    return {
        service_constants: state.serviceConstants,
        dataConstants: state.dataConstants,
        signInDetails: state.signInDetails,
        userProfile: state.userProfile,
        showLoginMenu: state.showLoginMenu,
        countries: state.countries,
        currentCity: state.currentCity,
        lang: state.lang,
        dateAndTime: state.dateAndTime,
        currentCityID: state.currentCityID,
        currentCityData: state.currentCityData,
        dataConstants: state.dataConstants,
        newDataConstants: state.newDataConstants,
        cities: state.cities,
        bookingDateTimeAvailabilityReducer: state.bookingDateTimeAvailabilityReducer,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ setBodyClass, toggleLoginModal, setSignIn, fetchDateTime,fetchDateTimeAvailability,
        fetchDataDictionaryValues,
        fetchDataConstantValues,
        fetchServices,
        fetchAllCities}, dispatch);
}

export default withCookies(connect(mapStateToProps, mapDispatchToProps)(QuotesFormPage));