import React from "react";
import FormFieldsTitle from "../../../components/FormFieldsTitle";
import CheckRadioBoxInput from "../../../components/Form_Fields/CheckRadioBoxInput";
import CheckRadioBoxInputWithPrice from "../../../components/Form_Fields/CheckRadioBoxInputWithPrice";
import DatePick from "../../../components/Form_Fields/DatePick";
import SelectInput from "../../../components/Form_Fields/SelectInput";
import TextFloatInput from "../../../components/Form_Fields/TextFloatInput";
import TextareaInput from "../../../components/Form_Fields/TextareaInput";
import PaymentMethods from "../../../components/Form_Fields/PaymentMethods";
import SubmitBooking from "../../../components/Form_Fields/SubmitBooking";
import BookingSummary from "../../../components/Form_Fields/BookingSummary";
import PackageBox from "../../../components/Form_Fields/PackageBox";
import BookNextStep from "../../../components/Form_Fields/BookNextStep";
import FormTitleDescription from "../../../components/FormTitleDescription";
import ContactDetailsStep from "../../../components/ContactDetailsStep";
import QuotesNextStep from "../../../components/Form_Fields/QuotesNextStep";
import SelectCompanyWithFilters from "../../../components/SelectCompanyWithFilters";
import { isValidSection, scrollToTop } from "../../../actions/index";
import commonHelper from '../../../helpers/commonHelper';
import locationHelper from "../../../helpers/locationHelper";
import Loader from "../../../components/Loader";

import moment from 'moment';
var allServiceConstant = {};
var URLConstant = {};
var current_currency = "AED";
var current_city = "dubai";
class QuotesCurtainsPage extends React.Component {

    constructor(props) {
        super(props);
        var quotesData = props.quotesData;
        var selectedCompanies = (typeof quotesData != "undefined" && typeof quotesData.selected_companies != "undefined") ?
            quotesData.selected_companies : [];

        this.state = {
            window_coverings_types: '',
            premises_to_install: '',
            no_of_windows: '',
            calender_date_of_service: '',
            other_comments_text: '',
            input_email: props.userProfile.email,
            input_phone: props.userProfile.address ? props.userProfile.address.phoneNumber : '',
            input_name: props.userProfile.customerFirstName,
            input_last_name: props.userProfile.customerLastName,
            input_address_city: '',
            input_address_area: props.setUserArea,
            input_address_area_building_name: props.userProfile.address ? props.userProfile.address.building : '',
            input_address_area_building_apartment: props.userProfile.address ? props.userProfile.address.apartment : '',
            selectedCompanies: selectedCompanies,
            service: 0,
            loader: props.loader,
        }
        this.curtainsRequest = this.curtainsRequest.bind(this);
        this.ContactDetails = this.ContactDetails.bind(this);
        this.handleCustomChange = this.handleCustomChange.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.currentStep = this.currentStep.bind(this);
        this.moveNext = this.moveNext.bind(this);
        this.submitData = this.submitData.bind(this);
        this.selectCompanyChange = this.selectCompanyChange.bind(this);
        this.howToProceed = this.howToProceed.bind(this);
        this.showOrHideModalSelectCompanyError = this.showOrHideModalSelectCompanyError.bind(this);
        allServiceConstant = props.service_constants;

    }
    componentDidMount() {
        allServiceConstant = this.props.service_constants;
        let parentServiceId = allServiceConstant.SERVICE_CURTAINS__BLINDS;
        let servicesOptions = commonHelper.lookupServices('parent_service_id', parentServiceId);
        URLConstant = this.props.url_constants;
        current_city = this.props.current_city;
        const dataConstantsWindowCovering = commonHelper.processDataValues(this.props.dataConstants["WINDOW_COVERINGS_TYPE"]);
        var cityOptions = locationHelper.getLocationByName(current_city);
        this.setState({
            input_address_city: cityOptions
        });
        this.setState({
            service: allServiceConstant.SERVICE_CURTAINS,
            window_coverings_types: { id: 1, value: dataConstantsWindowCovering.DATA_VALUE_WINDOW_COVERINGS_TYPE_CURTAINS, label: "Curtains" },
        });

    }
    sumarryItems() {
        const { window_coverings_types, premises_to_install, no_of_windows, calender_date_of_service } = this.state;
        return [
            { label: 'Type', value: window_coverings_types.label },
            { label: 'Premises', value: premises_to_install.label },
            { label: 'No. of windows ', value: no_of_windows },
            { label: 'Date', value: calender_date_of_service }
        ];
    }
    hashChangeHandler() {
        let hashVal = window.location.hash;
        let step;
        hashVal.length ? step = parseInt(window.location.hash.replace('#', '')) : step = 1;
        this.props.moveNext(step);
        scrollToTop();
    }
    currentStep() {
        return this.props.formCurrentStep;
    }
    moveNext(step) {
        const { moveNextStep } = this.props;
        moveNextStep(step, this.state);
    }
    howToProceed(isSkipCompany) {

        const { howToProceed } = this.props;

        var selectedCompanies = isSkipCompany ? [] : this.state.selectedCompanies;

        this.setState({
            selectedCompanies: selectedCompanies
        });

        howToProceed(isSkipCompany, this.state);

    }
    submitData() {
        const { submitLeadData } = this.props;
        submitLeadData(this.state);
    }
    selectCompanyChange(value) {
        this.setState({
            selectedCompanies: value
        });
    }
    showOrHideModalSelectCompanyError(boolVal) {
        const { showOrHideModalSelectCompanyError } = this.props;
        showOrHideModalSelectCompanyError(boolVal);
    }
    handleCustomChange(name, value) {

        this.setState({
            [name]: value
        });
    }
    handleInputChange(name, value) {
        if (name === 'no_of_windows' && value < 0)
            return;

        if (name === 'window_coverings_types') {

            const val = value.label;
            let srvc = '';
            switch (val) {
                case 'Blinds':
                    srvc = allServiceConstant.SERVICE_BLINDS
                    break;
                case 'Shutters':
                    srvc = allServiceConstant.SERVICE_SHUTTERS;
                    break;
                case 'Let me describe what I need':
                    srvc = allServiceConstant.SERVICE_CUSTOM_CURTAINS;
                    break;
                case 'Curtains':
                default:
                    srvc = allServiceConstant.SERVICE_CURTAINS;
                    break;
            }
            this.setState({
                [name]: value,
                service: srvc
            });
            return;
        }

        this.setState({
            [name]: value
        });
    }
    curtainsRequest() {

        const data_values = this.props.dataConstants;

        const window_coverings_types_value = this.state.window_coverings_types;

        const premises_to_install_value = this.state.premises_to_install;

        const no_of_windows_value = this.state.no_of_windows;

        const other_comments_text_value = this.state.other_comments_text;

        const calender_date_of_service_value = this.state.calender_date_of_service;

        const dataConstantsWindowCovering = commonHelper.processDataValues(data_values["WINDOW_COVERINGS_TYPE"]);
        const window_coverings_types_options = //commonHelper.processDataValues(data_values["WINDOW_COVERINGS_TYPE"]);
            [
                { id: 1, value: dataConstantsWindowCovering.DATA_VALUE_WINDOW_COVERINGS_TYPE_CURTAINS, label: "Curtains" },
                { id: 2, value: dataConstantsWindowCovering.DATA_VALUE_WINDOW_COVERINGS_TYPE_BLINDS, label: "Blinds" },
                { id: 3, value: dataConstantsWindowCovering.DATA_VALUE_WINDOW_COVERINGS_TYPE_SHUTTERS, label: "Shutters" },
                { id: 4, value: dataConstantsWindowCovering.DATA_VALUE_WINDOW_COVERINGS_TYPE_LET_ME_DESCRIBE_WHAT_I_NEED, label: "Let me describe what I need" }
            ];


        const installOptions = commonHelper.processDataValues(data_values["CURTAINS_LOCATION_TYPE"]);

        const premises_to_install_options =//commonHelper.processDataValues(data_values["CURTAINS_LOCATION_TYPE"]);
            [
                { id: 1, value: installOptions.DATA_VALUE_CURTAINS_LOCATION_TYPE_APARTMENTS, label: "Apartment" },
                { id: 2, value: installOptions.DATA_VALUE_CURTAINS_LOCATION_TYPE_VILLA, label: "Villa" },
                { id: 3, value: installOptions.DATA_VALUE_CURTAINS_LOCATION_TYPE_OFFICE, label: "Office" }
            ];

        const countWindows = window_coverings_types_value.label === 'Let me describe what I need' ? 'How many windows need window coverings' : 'How many windows need ' + window_coverings_types_value.label;

        var typeOfJourney = this.props.typeOfJourney();

        var currentStepClass = "";

        if (typeOfJourney == 1) {
            currentStepClass = this.currentStep() === 1 ? '' : "d-none";
        } else {
            currentStepClass = this.currentStep() === 2 ? '' : "d-none";
        }

        return (
            <div id="section-request-form" className={currentStepClass + " no-bottom-space"}>
                <section className="coverings-types">
                    <FormFieldsTitle title="Please select the type of window coverings you want" />
                    <CheckRadioBoxInput
                        InputType="radio"
                        name="window_coverings_types"
                        inputValue={window_coverings_types_value}
                        items={window_coverings_types_options}
                        childClass="col-6 col-md-4 mb-3 d-flex"
                        onInputChange={this.handleInputChange}
                        validationClasses="radio-required"
                        validationMessage="Please select what type of coverings you would like for your windows." />
                </section>
                <section>
                    <FormFieldsTitle title="Where will the curtains be installed?" />
                    <CheckRadioBoxInput
                        InputType="radio"
                        name="premises_to_install"
                        inputValue={premises_to_install_value}
                        items={premises_to_install_options}
                        childClass="col-6 col-md-4 mb-3 d-flex"
                        onInputChange={this.handleInputChange}
                        validationClasses="radio-required"
                        validationMessage="Please select the property in which you need the curtains installed."
                    />
                </section>
                <section>
                    <FormFieldsTitle title={countWindows} />
                    <div className="row mb-2">
                        <div className="col-12 col-md-6 mb-3">
                            <TextFloatInput
                                InputType="number"
                                min="1"
                                max="50"
                                name="no_of_windows"
                                inputValue={no_of_windows_value}
                                label="How many windows"
                                onInputChange={this.handleInputChange}
                                validationClasses="required numbers"
                                validationMessage="Please enter a value greater than or equal to 1."
                            />
                        </div>
                    </div>
                </section>
                <section>
                    <div className="row mb-2">
                        <DatePick
                            name="calender_date_of_service"
                            inputValue={calender_date_of_service_value}
                            title="By when do you need them installed?"
                            validationClasses="required"
                            validationMessage="Please select a date on which the suppliers can conduct a survey of your home."
                            onInputChange={this.handleInputChange}
                            disableFriday={false} />
                    </div>
                </section>
                <section>
                    <FormFieldsTitle title="Provide a short description of what you need (optional)" />
                    <div className="row mb-4">
                        <div className="col mb-3">
                            <TextareaInput inputValue={other_comments_text_value} name="other_comments_text" placeholder="" onInputChange={this.handleInputChange} />
                        </div>
                    </div>
                </section>
                <section>
                    <QuotesNextStep moveNext={this.moveNext} formCurrentStep={this.props.formCurrentStep} howToProceed={this.howToProceed} submitData={this.submitData} />
                </section>
            </div>
        )
    }
    selectCompanies() {
        var currentStepClass = this.currentStep() === 2 ? '' : "d-none";
        const serviceID = allServiceConstant.SERVICE_CURTAINS__BLINDS;
        const currentCity = this.props.current_city;
        const { selectedCompanies } = this.state;
        const { showSelectCompanyError } = this.props;

        return (
            <div className={currentStepClass}>
                <SelectCompanyWithFilters
                    serviceID={serviceID}
                    currentCity={currentCity}
                    selectChange={this.selectCompanyChange}
                    selectedCompanies={selectedCompanies}
                    showSelectCompanyError={showSelectCompanyError}
                    showOrHideModalSelectCompanyError={this.showOrHideModalSelectCompanyError} />
                <section>
                    <QuotesNextStep moveNext={this.moveNext} formCurrentStep={this.props.formCurrentStep} howToProceed={this.howToProceed} submitData={this.submitData} />
                </section>
            </div>
        )
    }
    ContactDetails(isLocationDetailShow) {
        var cityOptions = locationHelper.getLocationByName(current_city);
        var countryOptions = [{ id: 1, value: "1", label: "Dubai" }];
        var currentStepClass = this.currentStep() === 3 ? '' : "d-none";
        //var currentStepClass =  ''
        //var isLocationDetailShow = typeof isLocationDetailShow != "undefined" ? isLocationDetailShow : "yes";
        var isLocationDetailShow = "yes";

        var contact_details = {
            input_email: this.state.input_email,
            input_phone: this.state.input_phone,
            input_name: this.state.input_name,
            input_last_name: this.state.input_last_name
        }
        if (isLocationDetailShow) {
            contact_details.input_address_city = this.state.input_address_city != "" ? this.state.input_address_city : cityOptions;
            contact_details.input_address_area = this.state.input_address_area;
            contact_details.input_address_area_building_name = this.state.input_address_area_building_name;
            contact_details.input_address_area_building_apartment = this.state.input_address_area_building_apartment;
        }
        var userProfile = this.props.userProfile;

        if (!this.state.loader) {
            return (
                <div id="personal-information-form" className={currentStepClass}>
                    <ContactDetailsStep
                        cityOptions={cityOptions}
                        userProfile={userProfile}
                        signInDetails={this.props.signInDetails}
                        isLocationDetailShow={isLocationDetailShow}
                        contact_details={contact_details}
                        handleDropDownChange={this.handleInputChange}
                        handleInputChange={this.handleInputChange}
                        mobileSummary={this.mobileSummary}
                        moveNext={this.moveNext}
                        formCurrentStep={this.props.formCurrentStep}
                        submitData={this.submitData}
                        isSkipCompany={this.props.isSkipCompany}
                        currentCity = {this.props.current_city}
                    />
                </div>
            )
        } else {
            return (
                <main loader={this.state.loader ? "show" : "hide"}><Loader /></main>
            );
        }
    }
    componentWillReceiveProps(newProps) {
        if (newProps.setUserArea !== this.props.setUserArea) {
            this.setState({
                input_address_area: newProps.setUserArea
            })
        }
    }
    render() {
        const { selectedCompanies } = this.state;
        const items = this.sumarryItems();
        const showPromo = false;
        const showPrice = false;
        var typeOfJourney = this.props.typeOfJourney();
        var isLoading = this.state.loader || this.props.loader;

        return (
            <React.Fragment>
                <div className="col-lg-8">
                    {
                        isLoading ? <main loader={isLoading ? "show" : "hide"}><Loader /></main> :
                            (<React.Fragment>
                                {this.curtainsRequest()}
                                {typeOfJourney == 1 && this.selectCompanies()}
                                {this.ContactDetails("no")}
                            </React.Fragment>)
                    }
                </div>
                <div className="col-md-3 ml-auto d-lg-block d-none">
                    <BookingSummary
                        typeOfFlow="quotes"
                        items={items}
                        showPromo={showPromo}
                        showPrice={showPrice}
                        selectedCompanies={selectedCompanies}
                        selectChange={this.selectCompanyChange}
                        formCurrentStep={this.props.formCurrentStep}
                        typeOfJourney={typeOfJourney} />
                </div>
            </React.Fragment>
        )
    }
}

export default QuotesCurtainsPage;