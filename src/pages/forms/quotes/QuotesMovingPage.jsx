import React from "react";
import FormFieldsTitle from "../../../components/FormFieldsTitle";
import CheckRadioBoxInput from "../../../components/Form_Fields/CheckRadioBoxInput";
import DatePick from "../../../components/Form_Fields/DatePick";
import SelectInput from "../../../components/Form_Fields/SelectInput";
import TextFloatInput from "../../../components/Form_Fields/TextFloatInput";
import AreaSuggestionInput from "../../../components/Form_Fields/AreaSuggestionInput";
import TextareaInput from "../../../components/Form_Fields/TextareaInput";
import BookingSummary from "../../../components/Form_Fields/BookingSummary";
import FormTitleDescription from "../../../components/FormTitleDescription";
import ContactDetailsStep from "../../../components/ContactDetailsStep";
import QuotesNextStep from "../../../components/Form_Fields/QuotesNextStep";
import SelectCompanyWithFilters from "../../../components/SelectCompanyWithFilters";
import SelectOffers from "../../../components/SelectOffers";
import {isValidSection, scrollToTop, isEmailExist, validEmail} from "../../../actions/index";
import locationHelper from "../../../helpers/locationHelper";
import commonHelper from "../../../helpers/commonHelper";
import GeneralModal from "../../../components/GeneralModal";
import moment from 'moment';
import Loader from "../../../components/Loader";
let allServiceConstant = {};
let URLConstant = {};
let current_currency = "AED";
let current_city = "dubai";
class QuotesMovingPage extends React.Component{

    constructor(props) {
        super(props);
        var quotesData = props.quotesData;
        var selectedCompanies = (typeof quotesData != "undefined" && typeof quotesData.selected_companies != "undefined") ? 
        quotesData.selected_companies : [];
        this.state = {
            type_of_move: 'Local',
            home_type:"",
            rooms:"",
            moving_size: {value: '', label: ''},
            from_emirate:{value: '', label: ''},
            from_area:"",
            from_address:"",
            to_emirate:{value: '', label: ''},
            to_area:"",
            to_address:"",
            other_services: [],
            other_comments_text:"",
            item_lists:"",
            move_date:'',
            input_email: props.userProfile.email,
            input_phone: props.userProfile.address ? props.userProfile.address.phoneNumber : '',
            input_name: props.userProfile.customerFirstName,
            input_last_name: props.userProfile.customerLastName,
            selectedCompanies: selectedCompanies,
            selectedOffers: [],
            loginRequired: false,
            isSkipCompany:false,
            showSelectCompanyError: false
        }
        this.movingRequest = this.movingRequest.bind(this);
        this.selectCompanies = this.selectCompanies.bind(this);
        this.ContactDetails = this.ContactDetails.bind(this);
        this.handleCustomChange = this.handleCustomChange.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.currentStep = this.currentStep.bind(this);
        this.moveNext = this.moveNext.bind(this);
        //this.hashChangeHandler = this.hashChangeHandler.bind(this);
        this.submitData = this.submitData.bind(this);
        this.selectCompanyChange = this.selectCompanyChange.bind(this);
        this.selectOffersChange = this.selectOffersChange.bind(this);
        this.howToProceed = this.howToProceed.bind(this);
        this.showOrHideModalSelectCompanyError = this.showOrHideModalSelectCompanyError.bind(this);
        allServiceConstant = props.service_constants;
    }
    componentDidMount() {
        allServiceConstant = this.props.service_constants;
        URLConstant =  this.props.url_constants;
        current_city = this.props.current_city;
        var defaultCity = locationHelper.getLocationByName(current_city);
        var selectedCityVal = {id: defaultCity.id, value: commonHelper.slugify(defaultCity.name), label: defaultCity.name}
        this.setState({
            from_emirate: selectedCityVal,
            to_emirate: selectedCityVal
        });
    }
    currentStep(){
        return this.props.formCurrentStep;
    }
    moveNext(step){
        const {moveNextStep} = this.props;
        moveNextStep(step, this.state);
    }
    showOrHideModalSelectCompanyError(boolVal){
        const {showOrHideModalSelectCompanyError} = this.props;
        showOrHideModalSelectCompanyError(boolVal);
    }
    howToProceed(isSkipCompany){
        const {howToProceed} = this.props;

        var selectedCompanies = isSkipCompany ? [] : this.state.selectedCompanies;

        this.setState({
            selectedCompanies: selectedCompanies
        });

        howToProceed(isSkipCompany, this.state);
    }
    submitData(){
        const {submitLeadData} = this.props;
        submitLeadData(this.state);
    }
    selectCompanyChange(value){
        this.setState({
            selectedCompanies: value
        });
    }
    selectOffersChange(value){
        this.setState({
            selectedOffers: value
        });
    }
    sumarryItems(){
        const {moving_size, from_emirate, from_area, to_emirate, to_area, move_date} = this.state;
        var movingFromtext = from_emirate.label;

        var movingTotext = to_emirate.label;
        
        movingFromtext += from_area != "" ? " - "+from_area : "";

        movingTotext += to_area != "" ? " - "+to_area : "";

        return [
            {label: 'Type of move', value: 'Local'},
            {label: 'Type of home', value: moving_size.label},
            {label: 'Moving from', value: movingFromtext},
            {label: 'Moving to', value: movingTotext},
            {label: 'Scheduled for', value: move_date}
        ]; 
    }
 
    handleCustomChange(name, item){
        this.setState({
            [name]: item
        });

        if( name == "moving_size" ){
            let home_type = "";
            let rooms = "";
            if(item.value == "Studio Apartment"){
                home_type = "Apartment";
                rooms = "Studio";
            }else{
                let home = item.value.split(" ");
                home_type = home[2];
                rooms = home[0];
            }

            this.setState({
                "moving_size":item
            });
        }
        /*else if( name == "other_services"){
            //console.log(value);
            //var other_services_val = this.state.other_services;
        }*/
        else if( name == "from_emirate" || name == "to_emirate"){
            let from_emirate = "";
            let to_emirate = "";
            if( name == "from_emirate"){
                /*from_emirate = item.value;
                to_emirate = this.state.to_emirate.value;*/
                var from_area_ele = document.getElementsByName("from_area")[0];
                from_area_ele.value = "";
                from_area_ele.classList.remove("sm-valid");
                this.setState({
                    "from_area":''
                });

            }
            if( name == "to_emirate"){
                /*from_emirate = this.state.from_emirate.value;
                to_emirate = item.value;*/
                var to_area_ele =  document.getElementsByName("to_area")[0];
                to_area_ele.value = "";
                to_area_ele.classList.remove("sm-valid");
                this.setState({
                    "to_area":''
                });
            }
        }

    }
    handleInputChange(name, value) {
        this.setState({
            [name]: value
        });

    }
    handleNoOfUnits(name, value){
        this.setState({
            number_of_units: value
        });
    }
    movingRequest(){
        var data_values = this.props.data_values;

        var service_constants = allServiceConstant;

        var move_type = [
            {id: 1, value: "full_move", label: "Full home"},
            {id: 2, value: "small_move", label: "Few items"}
        ];

        var moving_size = [
            {id: 9, value: "studio", label: "Studio"},
            {id: 1, value: "1 Br", label: "1 bedroom apartment"},
            {id: 2, value: "2 Br", label: "2 bedroom apartment"},
            {id: 3, value: "3 Br", label: "3 bedroom apartment"},
            {id: 4, value: "4 Br", label: "4+ bedroom apartment"},
            {id: 5, value: "2 Br h/v", label: "2 bedroom house/villa"},
            {id: 6, value: "3 Br h/v", label: "3 bedroom house/villa"},
            {id: 7, value: "4 Br h/v", label: "4 bedroom house/villa"},
            {id: 8, value: "5 Br h/v", label: "5+ bedroom house/villa"},
            {id: 10, value: "other", label: "Other"},
            {id: 11, value: "office", label: "Office"},

        ];

        var other_services = [
            {id: allServiceConstant.SERVICE_BASIC_HANDYMAN_SERVICES_DRILLING_ON_THE_WALL_CURTAIN_HANGING_LOCAL_MOVE, value: "Handyman", label: "Handyman"},
            {id: allServiceConstant.SERVICE_STORAGE_LOCAL_MOVE, value: "Storage", label: "Storage"}
        ];

        var other_services_value = this.state.other_services;

        var moving_size_value = this.state.moving_size;

        var from_emirate_value = this.state.from_emirate;

        var from_area_value = this.state.from_area;

        var from_address_value = this.state.from_address;

        var to_emirate_value = this.state.to_emirate;

        var to_area_value = this.state.to_area;

        var to_address_value = this.state.to_address;

        var other_comments_text_value = this.state.other_comments_text;

        var items_list_value = this.state.item_lists;

        var calender_date_of_service_value = this.state.calender_date_of_service;

        //other_services
        var typeOfJourney = this.props.typeOfJourney();

        var currentStepClass = "";
        
        if( typeOfJourney == 1 ){
            currentStepClass = this.currentStep() === 1 ? '' : "d-none";
        }else{
            currentStepClass = this.currentStep() === 2 ? '' : "d-none";
        }

        var allCities = locationHelper.fetchCities();

        var cityOptions = [];

        allCities.map(function(item){
            let value = commonHelper.slugify(item.label);
            cityOptions.push({id: item.id, value: value, label: item.label});
        })

        var from_city_id = from_emirate_value !="" && Object.keys(from_emirate_value).length ? from_emirate_value.id : locationHelper.getCurrentCityId();

        var to_city_id = to_emirate_value !="" && Object.keys(to_emirate_value).length ? to_emirate_value.id : locationHelper.getCurrentCityId();

        var from_areas =  locationHelper.getAreasByCity(from_city_id);

        var to_areas =  locationHelper.getAreasByCity(to_city_id);
        // console.log("this.props.startDate", moment(this.props.startDate));
        return (
            <div id="section-request-form" className={currentStepClass+" no-bottom-space"}>
                <section>
                    <FormFieldsTitle title="Please share the details about your move to receive multiple quotations from moving companies." titleClass="h5 mb-4" />
                </section>
                <section>
                    <FormFieldsTitle title="When do you need to move?" />
                    <div className="row mb-2">
                        <DatePick name="move_date"
                        selectedDate ={this.state.move_date}
                        inputValue={this.state.move_date}
                        onInputChange={ this.handleCustomChange } 
                        startDate={moment(this.props.startDate)} 
                        disableFriday={false} 
                        validationClasses="required"/>
                    </div>
                </section>

                <section>
                    <FormFieldsTitle title="Where are you moving from?"/>
                    <div className="row mb-1">
                        <div className="col-12 col-md-4 mb-3">
                            <SelectInput name="from_emirate" inputValue={from_emirate_value} label="City" options={cityOptions} onInputChange={this.handleCustomChange} />
                        </div>
                        <div className="col-12 col-md-4 mb-3">
                            <AreaSuggestionInput allowNonArea = {true} InputType="text" name="from_area" areas={from_areas} inputValue={from_area_value} label="Area" onInputChange={this.handleInputChange} validationClasses="required" />
                        </div>
                        <div className="col-12 col-md-4 mb-3">
                            <TextFloatInput InputType="text" name="from_address" inputValue={from_address_value} label="Building or Street no" onInputChange={this.handleInputChange} validationClasses="required" />
                        </div>
                    </div>
                </section>
                <section>
                    <FormFieldsTitle title="Where are you moving to?"/>
                    <div className="row mb-1">
                        <div className="col-12 col-md-4 mb-3">
                            <SelectInput name="to_emirate" inputValue={to_emirate_value} label="City" options={cityOptions} onInputChange={this.handleCustomChange} />
                        </div>
                        <div className="col-12 col-md-4 mb-3">
                            <AreaSuggestionInput allowNonArea = {true} InputType="text" name="to_area" areas={to_areas} inputValue={to_area_value} label="Area" onInputChange={this.handleInputChange} validationClasses="required" />
                        </div>
                        <div className="col-12 col-md-4 mb-3">
                            <TextFloatInput InputType="text" name="to_address" inputValue={to_address_value} label="Building or Street no" onInputChange={this.handleInputChange} validationClasses="required" />
                        </div>
                    </div>
                    <div className="row mb-1">
                        <div className="col move-international-quotes-link">
                            <p><small>Moving internationally? <a href="../international-movers/journey1">Click here</a> to get quotes </small></p>
                        </div>
                    </div>
                </section>
                <section>
                    <FormFieldsTitle title="Moving size" />
                    <div className="row mb-2">
                        <div className="col-12 col-md-6 mb-3">
                            <SelectInput name="moving_size" inputValue={moving_size_value} label="Select size" options={moving_size} onInputChange={this.handleCustomChange} />
                        </div>
                    </div>
                </section>
                <section>
                    <FormFieldsTitle title="Do you need any of these services?" />
                    <CheckRadioBoxInput 
                    InputType="checkbox" 
                    name="other_services" 
                    inputValue={other_services_value} 
                    items={other_services}  
                    childClass="col-6 mb-3 d-flex" 
                    onInputChange={this.handleCustomChange}/>
                </section>
                <section>
                    <FormFieldsTitle title="What else should we know? (Optional)" />
                    <div className="row mb-4">
                        <div className="col mb-3">
                            <TextareaInput name="other_comments_text" placeholder="If you have any other requirements, feel free to describe them here in as much detail as you want. Or just leave us a message here to call you if its easier to explain on the phone" inputValue={other_comments_text_value} onInputChange={this.handleCustomChange} />
                        </div>
                    </div>
                </section>

                <section>
                    <QuotesNextStep moveNext={this.moveNext} formCurrentStep={this.props.formCurrentStep} howToProceed={this.howToProceed} submitData={this.submitData} />
                </section>
            </div>
        )
    }
    selectCompanies(){
        var currentStepClass = this.currentStep() === 2 ? '' : "d-none";
        
        const serviceID = allServiceConstant.SERVICE_LOCAL_MOVE;
        
        const currentCity = this.props.current_city;

        const {selectedCompanies} = this.state;

        const {showSelectCompanyError} = this.props;

        if( typeof(serviceID) != 'undefined' ){
            return (
                <div id="section-company-selector" className={currentStepClass}>
                    <SelectCompanyWithFilters 
                        serviceID={serviceID}
                        currentCity={currentCity}
                        selectChange={this.selectCompanyChange}
                        selectedCompanies={selectedCompanies}
                        showSelectCompanyError={showSelectCompanyError}
                        showOrHideModalSelectCompanyError={this.showOrHideModalSelectCompanyError}  />
                    <section>
                        <QuotesNextStep moveNext={this.moveNext} formCurrentStep={this.props.formCurrentStep}  submitData={this.submitData} />
                    </section>
                </div>
            )
        }
    }
    ContactDetails(isLocationDetailShow){
        var cityOptions = locationHelper.getLocationByName(current_city);
        var currentStepClass = this.currentStep() === 3 ? '' : "d-none";
        
        //var currentStepClass =  ''
        var isLocationDetailShow = typeof isLocationDetailShow != "undefined" ? isLocationDetailShow : "yes";
        var contact_details = {
            input_email:this.state.input_email,
            input_phone:this.state.input_phone,
            input_name:this.state.input_name,
            input_last_name:this.state.input_last_name
        }
        
        const serviceID = allServiceConstant.SERVICE_LOCAL_MOVE;
        const currentCity = this.props.current_city;
        const {moving_size} = this.state;
        var userProfile =  this.props.userProfile;
        
        var backButtonLink = "#1";

        var {isSkipCompany} = this.props;

        if(typeof isSkipCompany != "undefined"){

            backButtonLink = isSkipCompany ? "#1" : "#2";
        }

        if(!this.state.loader) {
            return (
                <div id="personal-information-form" className={currentStepClass}>
                    <ContactDetailsStep
                        cityOptions={cityOptions}
                        userProfile = {userProfile}
                        signInDetails={this.props.signInDetails}
                        isLocationDetailShow={isLocationDetailShow}
                        contact_details={contact_details}
                        handleDropDownChange={this.handleInputChange}
                        handleInputChange={this.handleInputChange}
                        mobileSummary={this.mobileSummary}
                        moveNext={this.moveNext}
                        formCurrentStep = {this.props.formCurrentStep}
                        submitData={this.submitData}
                        isSkipCompany = {this.props.isSkipCompany}
                        backButton = {false}
                        currentCity = {this.props.current_city}
                    />
                    { serviceID && currentCity && moving_size.value &&
                        <SelectOffers serviceID={serviceID} currentCity={currentCity} selectOffersChange={this.selectOffersChange} selectedOffers={this.state.selectedOffers} movingSize={moving_size.value} />
                    }
                    <div className="row mt-2 goback">
                            <div className="col-12">
                                <a href={backButtonLink} className="text-black"> <i className="fa fa-angle-left"></i> Back</a>
                            </div>
                    </div>
                </div>
            )
        } else {
            return (
                <main loader={this.state.loader ? "show" : "hide"}><Loader/></main>
            );
        }
    }
    showOrHideModalSelectCompanyError(boolVal){
        const {showOrHideModalSelectCompanyError} = this.props;
        showOrHideModalSelectCompanyError(boolVal);
    }
    render(){
        const {selectedCompanies} = this.state;

        var typeOfJourney = this.props.typeOfJourney(); 
        
        const items = this.sumarryItems();
        const showPromo = false;
        const showPrice = false;

        var isLoading =  this.state.loader || this.props.loader;

        var bookingSummary = <BookingSummary 
        typeOfFlow="quotes" 
        items={items} 
        showPromo={showPromo} 
        showPrice={showPrice}
        formCurrentStep = {this.props.formCurrentStep}
        selectedCompanies={selectedCompanies}
        selectChange={this.selectCompanyChange}
        typeOfJourney = {typeOfJourney}
        />
        //<BookingSummary typeOfFlow="quotes" items={items} showPromo={showPromo} showPrice={showPrice}  selectChange={this.selectCompanyChange} selectedCompanies={selectedCompanies} formCurrentStep={this.props.formCurrentStep}/>
        return(
            <React.Fragment>
            <div className="col-lg-8">
                { 
                    isLoading ?  <main loader={ isLoading ? "show" : "hide"}><Loader/></main> : 
                (<React.Fragment>
                    {this.movingRequest()}
                    { typeOfJourney == 1 && this.selectCompanies()}
                    {this.ContactDetails("no")}
                </React.Fragment>)
            }             
            </div>
            <div className="col-md-3 ml-auto d-lg-block d-none">
                 {bookingSummary}
            </div>
            </React.Fragment>
        )
    }
}

export default QuotesMovingPage;