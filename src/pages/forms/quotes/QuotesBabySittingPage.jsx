import React from "react";
import FormFieldsTitle from "../../../components/FormFieldsTitle";
import CheckRadioBoxInput from "../../../components/Form_Fields/CheckRadioBoxInput";
import CheckRadioBoxInputWithPrice from "../../../components/Form_Fields/CheckRadioBoxInputWithPrice";
import DatePick from "../../../components/Form_Fields/DatePick";
import SelectInput from "../../../components/Form_Fields/SelectInput";
import TextFloatInput from "../../../components/Form_Fields/TextFloatInput";
import TextareaInput from "../../../components/Form_Fields/TextareaInput";
import PaymentMethods from "../../../components/Form_Fields/PaymentMethods";
import SubmitBooking from "../../../components/Form_Fields/SubmitBooking";
import BookingSummary from "../../../components/Form_Fields/BookingSummary";
import PackageBox from "../../../components/Form_Fields/PackageBox";
import BookNextStep from "../../../components/Form_Fields/BookNextStep";
import FormTitleDescription from "../../../components/FormTitleDescription";
import ContactDetailsStep from "../../../components/ContactDetailsStep";
import QuotesNextStep from "../../../components/Form_Fields/QuotesNextStep";
import SelectCompanyWithFilters from "../../../components/SelectCompanyWithFilters";
import { isValidSection, scrollToTop } from "../../../actions/index";
import locationHelper from "../../../helpers/locationHelper";
import commonHelper from "../../../helpers/commonHelper";
import Loader from "../../../components/Loader";
let allServiceConstant = {};
let URLConstant = {};
let current_currency = "AED";
let current_city = "dubai";
class QuotesBabySittingPage extends React.Component {

    constructor(props) {
        super(props);
        var quotesData = props.quotesData;
        var selectedCompanies = (typeof quotesData != "undefined" && typeof quotesData.selected_companies != "undefined") ?
            quotesData.selected_companies : [];
        this.state = {
            neededServices: "",
            requiredDaysAWeek: { value: "", label: "" },
            requiredForHowLong: {  value: "", label: "" },
            requiredHowLongTextField: "",
            noOfKids: { value: "", label: "" },
            kids: [],
            hoursNeeded: { value: "", label: "" },
            dateOfService: '',
            eventTime: { value: "", label: "" },
            details: "",
            input_email: props.userProfile.email,
            input_phone: props.userProfile.address ? props.userProfile.address.phoneNumber : '',
            input_name: props.userProfile.customerFirstName,
            input_last_name: props.userProfile.customerLastName,
            input_address_city: '',
            input_address_area: props.setUserArea,
            input_address_area_building_name: props.userProfile.address ? props.userProfile.address.building : '',
            input_address_area_building_apartment: props.userProfile.address ? props.userProfile.address.apartment : '',
            selectedCompanies: selectedCompanies,
            loader: false
        }
        this.babySittingRequest = this.babySittingRequest.bind(this);
        this.ContactDetails = this.ContactDetails.bind(this);
        this.handleCustomChange = this.handleCustomChange.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.currentStep = this.currentStep.bind(this);
        this.moveNext = this.moveNext.bind(this);
        this.noOfChildOptions = this.noOfChildOptions.bind(this);
        this.moreThenOneOption = this.moreThenOneOption.bind(this);
        this.howToProceed = this.howToProceed.bind(this);
        this.submitData = this.submitData.bind(this);
        this.selectCompanyChange = this.selectCompanyChange.bind(this);
        this.showOrHideModalSelectCompanyError = this.showOrHideModalSelectCompanyError.bind(this);

    }
    componentDidMount() {
        allServiceConstant = this.props.service_constants;
        URLConstant = this.props.url_constants;
        current_city = this.props.current_city;
        var cityOptions = locationHelper.getLocationByName(current_city);
        this.setState({
            input_address_city: cityOptions
        })
    }
    currentStep() {
        return this.props.formCurrentStep;
    }
    moveNext(step) {
        const { moveNextStep } = this.props;
        moveNextStep(step, this.state);
    }
    showOrHideModalSelectCompanyError(boolVal) {
        const { showOrHideModalSelectCompanyError } = this.props;
        showOrHideModalSelectCompanyError(boolVal);
    }
    howToProceed(isSkipCompany) {
        const { howToProceed } = this.props;

        var selectedCompanies = isSkipCompany ? [] : this.state.selectedCompanies;

        this.setState({
            selectedCompanies: selectedCompanies
        });

        howToProceed(isSkipCompany, this.state);
    }
    submitData() {
        const { submitLeadData } = this.props;
        submitLeadData(this.state);
    }
    selectCompanyChange(value) {
        this.setState({
            selectedCompanies: value
        });
    }
    sumarryItems() {
        const { neededServices, hoursNeeded, noOfKids, dateOfService, kids } = this.state;
        return [
            { label: 'Frequency', value: neededServices.label },
            { label: 'Hours', value: hoursNeeded.label },
            { label: 'Number of children', value: noOfKids.label },
            { label: 'Date', value: dateOfService }
        ];
    }
    handleCustomChange(name, item) {
        if (name == "noOfKids") {
            var kids = this.state.kids;

            var newkids = [];

            var noOfKids = parseInt(item.value);

            for (var i = 0; i < item.value; i++) {
                if (typeof kids[i] == "undefined") {
                    newkids.push({
                        value: "",
                        label: ""
                    });
                } else {
                    newkids.push(kids[i]);
                }
            }
            this.setState({
                kids: newkids,
                noOfKids: item
            });

        } else {
            this.setState({
                [name]: item
            });
        }


    }
    handleInputChange(name, value) {
        this.setState({
            [name]: value
        });
    }
    handleNoOfUnits(name, value) {
        this.setState({
            number_of_units: value
        });
    }
    selectCompanyChange(value) {
        this.setState({
            selectedCompanies: value
        });
    }
    kidsAgeSelectChange = (idx) => (name, kidItem) => {

        var idx_val = parseInt(idx) + 1;

        var new_name = name.replace("_" + idx_val, "")

        // console.log("kidItem", kidItem);

        const newkidAge = this.state.kids.map((kid, sidx) => {
            if (idx !== sidx) return kid;
            return { ...kidItem };
        });

        // console.log("newkidAge", newkidAge);

        this.setState({ kids: newkidAge });
    }
    parseCount = (t) => {
        switch (t) {
            case 0:
                return 'First'; break;
            case 1:
                return 'Second'; break;
            case 2:
                return 'Third'; break;
            case 3:
                return 'Fourth'; break;
            case 4:
            default:
                return 'Fifth'; break;
        };

    }
    noOfChildOptions() {
        var kids_age_options = [
            { id: 1, value: "1", label: "0-3 months" },
            { id: 2, value: "2", label: "3-6 months" },
            { id: 3, value: "3", label: "6-12 months" },
            { id: 4, value: "4", label: "1-3 yrs" },
            { id: 5, value: "5", label: "3-6 yrs" },
            { id: 6, value: "6", label: "6 years or older" }
        ];
        var noOfKids = (this.state.kids).length;

        var noOfCol = Math.floor(12 / noOfKids);

        //<div className={"col-12 mb-3 col-md-"+noOfCol}>

        return (
            <section>
                <FormFieldsTitle title="What are their ages?" />
                <div className="row">

                    {
                        this.state.kids.map((kid, idx) => (

                            <div className={"col-12 mb-3 col-md-3"}>
                                <FormFieldsTitle title={this.parseCount(idx) +" child "} />
                                <SelectInput
                                    inputValue={kid}
                                    name={"kids_age_" + (idx + 1)}
                                    label="Age"
                                    options={kids_age_options}
                                    onInputChange={this.kidsAgeSelectChange(idx)}
                                    validationClasses="required" />
                            </div>
                        ))
                    }
                </div>
            </section>
        )
    }
    moreThenOneOption() {

        var requiredDaysAWeekValue = this.state.requiredDaysAWeek;

        var requiredDaysAWeek_options = [
            { id: 1, value: "1", label: "1 Day" },
            { id: 2, value: "2", label: "2 Days" },
            { id: 3, value: "3", label: "3 Days" },
            { id: 4, value: "4", label: "4 Days" },
            { id: 5, value: "5", label: "5 Days" },
            { id: 6, value: "6", label: "6 Days" },
            { id: 7, value: "7", label: "7 Days" }
        ]

        var requiredForHowLongOptions = [
            { id: 1, value: "WEEKS", label: "Weeks" },
            { id: 2, value: "MONTHS", label: "Months" },
            { id: 3, value: "YEARS", label: "Years" }
        ];
        var requiredForHowLongValue = this.state.requiredForHowLong;

        var requiredHowLongTextField_value = this.state.requiredHowLongTextField;

        var required_how_long_text_label = "Enter number of " + requiredForHowLongValue.label.toLowerCase();

        var requiredHowLongTextField = (
            <div className="col-6">
                <TextFloatInput
                    InputType="text"
                    name="requiredHowLongTextField"
                    inputValue={requiredHowLongTextField_value}
                    label={required_how_long_text_label}
                    onInputChange={this.handleInputChange}
                    validationClasses="required numbers"
                    validationMessage="Please select how long you need the service"
                />
            </div>);
        //{requiredForHowLongValue != "" && requiredHowLongTextField}
        var howLongElement = (
            <section>
                <div className="row mb-2">
                    <div className="col-12 mb-3 col-md-6">
                        <FormFieldsTitle title="How many days a week do you need the service?" titleClass="h4 mb-4" />
                        <SelectInput name="requiredDaysAWeek" inputValue={requiredDaysAWeekValue} label="Select number of days/week" options={requiredDaysAWeek_options} onInputChange={this.handleCustomChange} validationClasses="required" />
                    </div>
                    <div className="col-12 mb-3 col-md-6 how-long-you-need-service">
                        <FormFieldsTitle title="How long do you need the service for?" titleClass="h4 mb-4" />
                        <div className="row">
                            <div className="col-6">
                                <SelectInput name="requiredForHowLong" inputValue={requiredForHowLongValue} label="Select the service duration" options={requiredForHowLongOptions} onInputChange={this.handleCustomChange} validationClasses="required" />
                            </div>
                            {requiredForHowLongValue.value != "" && requiredHowLongTextField}
                        </div>
                    </div>
                </div>
            </section>
        );

        return howLongElement;
    }

    babySittingRequest() {

        var service_constants = allServiceConstant;

        var neededServicesValue = this.state.neededServices;

        var requiredForHowLongValue = this.state.requiredForHowLong;

        var details_value = this.state.details;

        var noOfKidsValue = this.state.noOfKids;

        var dateOfService_value = this.state.dateOfService;

        var neededServices_options = [
            { id: 1, value: "1", label: "Just once" },
            { id: 2, value: "2", label: "More than once" }
        ];

        var no_of_child_options = [
            { id: 1, value: "1", label: "1 Child" },
            { id: 2, value: "2", label: "2 Children" },
            { id: 3, value: "3", label: "3 Children" },
            { id: 4, value: "4", label: "4 Children" },
            { id: 5, value: "5", label: "5 Children" }
        ];

        var hoursNeeded_options = [
            { id: 1, value: "3", label: "3 Hours" },
            { id: 2, value: "4", label: "4 Hours" },
            { id: 3, value: "5", label: "5 Hours" },
            { id: 4, value: "6", label: "6 Hours" },
            { id: 5, value: "7", label: "7 Hours" },
            { id: 6, value: "8", label: "8 Hours" },
            { id: 7, value: "9", label: "9 Hours" },
            { id: 8, value: "10", label: "10 Hours" },
            { id: 9, value: "11", label: "11 Hours" },
            { id: 10, value: "12", label: "12 Hours" }
        ];

        var eventTimeOpt = [
            { id: 1, value: "07:00", label: "07:00" },
            { id: 2, value: "08:00", label: "08:00" },
            { id: 3, value: "09:00", label: "09:00" },
            { id: 4, value: "10:00", label: "10:00" },
            { id: 5, value: "11:00", label: "11:00" },
            { id: 6, value: "12:00", label: "12:00" },
            { id: 7, value: "13:00", label: "13:00" },
            { id: 8, value: "14:00", label: "14:00" },
            { id: 9, value: "15:00", label: "15:00" },
            { id: 10, value: "16:00", label: "16:00" },
            { id: 11, value: "17:00", label: "17:00" },
            { id: 12, value: "18:00", label: "18:00" },
            { id: 13, value: "19:00", label: "19:00" },
            { id: 14, value: "20:00", label: "20:00" },
            { id: 15, value: "21:00", label: "21:00" },
            { id: 16, value: "22:00", label: "22:00" },
        ];

        var hoursNeededValue = this.state.hoursNeeded;
        //other_services
        var typeOfJourney = this.props.typeOfJourney();

        var currentStepClass = "";

        if (typeOfJourney == 1) {
            currentStepClass = this.currentStep() === 1 ? '' : "d-none";
        } else {
            currentStepClass = this.currentStep() === 2 ? '' : "d-none";
        }

        var moreThenOneElements = neededServicesValue.value === "2" ? this.moreThenOneOption() : '';

        var noOfChildOptionsElements = noOfKidsValue.value !== "" ? this.noOfChildOptions() : '';
        /*<section>
         <FormFieldsTitle title="How many children require babysitting?"/>
         <div className="row mb-4">
         <div className="col-12 col-md-6">
         <SelectInput name="noOfKids" inputValue={noOfKidsValue} label="Select" options={no_of_child_options} onInputChange={this.handleCustomChange} />
         </div>
         </div>
         </section>*/

        return (
            <div id="section-request-form" className={currentStepClass}>
                <section>
                    <FormFieldsTitle title="How often do you need a babysitter? " />
                    <CheckRadioBoxInput
                        InputType="radio"
                        name="neededServices"
                        inputValue={neededServicesValue}
                        items={neededServices_options}
                        childClass="col-6 mb-3 d-flex"
                        onInputChange={this.handleCustomChange}
                        validationClasses="radio-required"
                        validationMessage="Please select service type"
                    />
                </section>
                {moreThenOneElements}

                <section>
                    <div className="row mb-2">
                        <div className="col-12 mb-3 col-md-6">
                            <FormFieldsTitle title="How many hours a day will you need them?" />
                            <SelectInput
                                name="hoursNeeded"
                                inputValue={hoursNeededValue}
                                label="Select hours"
                                options={hoursNeeded_options}
                                onInputChange={this.handleCustomChange}
                                validationClasses="required"
                            />
                        </div>
                        <div className="col-12 mb-3 col-md-6 how-many-children">
                            <FormFieldsTitle title="How many children require babysitting?" />
                            <SelectInput name="noOfKids" inputValue={noOfKidsValue} label="Select" options={no_of_child_options} onInputChange={this.handleCustomChange} validationClasses="required" />
                        </div>
                    </div>
                </section>

                {noOfChildOptionsElements}
                <section>
                    <div className="row mb-2">
                        <DatePick name="dateOfService"
                            disableFriday={false}
                            selectedDate={dateOfService_value}
                            inputValue={dateOfService_value}
                            onInputChange={this.handleInputChange}
                            title="When do you need the service?"
                            validationClasses="required"
                            validationMessage="Please select date"
                        />
                        {neededServicesValue.value != "2" && <div className="col-12 col-md-6">
                            <FormFieldsTitle title="What time do you need the service?" />
                            <SelectInput name="eventTime" inputValue={this.state.eventTime} label="Select" options={eventTimeOpt} onInputChange={this.handleCustomChange} validationClasses="required" />
                        </div>}
                    </div>
                </section>

                <section>
                    <FormTitleDescription title="Is there anything else you'd like to add?" />
                    <div className="row mb-4">
                        <div className="col mb-3">
                            <TextareaInput
                                name="details"
                                inputValue={this.state.details}
                                onInputChange={this.handleCustomChange}
                                placeholder="Are there any specific requirements or special circumstances babysitting companies should be aware of? Please describe the babysitting job/duties in as much detail as possible." />
                        </div>
                    </div>
                </section>
                <section>
                    <QuotesNextStep moveNext={this.moveNext} formCurrentStep={this.props.formCurrentStep} howToProceed={this.howToProceed} submitData={this.submitData} />
                </section>
            </div>
        )
    }
    selectCompanies() {
        var currentStepClass = this.currentStep() === 2 ? '' : "d-none";

        const serviceID = 151; // Should Be dynamic will connect with Jahahir

        const currentCity = this.props.current_city;

        const { selectedCompanies } = this.state;

        const { showSelectCompanyError } = this.props;

        if (typeof (serviceID) != 'undefined') {
            return (
                <div id="section-company-selector" className={currentStepClass}>
                    <SelectCompanyWithFilters
                        serviceID={serviceID}
                        currentCity={currentCity}
                        selectChange={this.selectCompanyChange}
                        selectedCompanies={selectedCompanies}
                        showSelectCompanyError={showSelectCompanyError}
                        showOrHideModalSelectCompanyError={this.showOrHideModalSelectCompanyError} />
                    <section>
                        <QuotesNextStep moveNext={this.moveNext} formCurrentStep={this.props.formCurrentStep} submitData={this.submitData} />
                    </section>
                </div>
            )
        }
    }
    ContactDetails() {

        var cityOptions = locationHelper.getLocationByName(current_city);

        var currentStepClass = this.currentStep() === 3 ? '' : "d-none";
        //var currentStepClass =  ''
        var isLocationDetailShow = "yes";

        var contact_details = {
            input_email: this.state.input_email,
            input_phone: this.state.input_phone,
            input_name: this.state.input_name,
            input_last_name: this.state.input_last_name
        }

        if (isLocationDetailShow) {
            contact_details.input_address_city = this.state.input_address_city != "" ? this.state.input_address_city : cityOptions;
            contact_details.input_address_area = this.state.input_address_area;
            contact_details.input_address_area_building_name = this.state.input_address_area_building_name;
            contact_details.input_address_area_building_apartment = this.state.input_address_area_building_apartment;
        }

        var userProfile = this.props.userProfile;

        if (!this.state.loader) {
            return (
                <div id="personal-information-form" className={currentStepClass}>
                    <ContactDetailsStep
                        cityOptions={cityOptions}
                        userProfile={userProfile}
                        signInDetails={this.props.signInDetails}
                        isLocationDetailShow={isLocationDetailShow}
                        contact_details={contact_details}
                        handleDropDownChange={this.handleInputChange}
                        handleInputChange={this.handleInputChange}
                        mobileSummary={this.mobileSummary}
                        moveNext={this.moveNext}
                        formCurrentStep={this.props.formCurrentStep}
                        submitData={this.submitData}
                        isSkipCompany={this.props.isSkipCompany}
                        currentCity = {this.props.current_city}
                    />
                </div>
            )
        } else {
            return (
                <main loader={this.state.loader ? "show" : "hide"}><Loader /></main>
            );
        }
    }
    componentWillReceiveProps(newProps) {
        if (newProps.setUserArea !== this.props.setUserArea) {
            this.setState({
                input_address_area: newProps.setUserArea
            })
        }
    }
    render() {
        const { selectedCompanies } = this.state;
        const items = this.sumarryItems();
        const showPromo = false;
        const showPrice = false;
        var isLoading = this.state.loader || this.props.loader;

        var typeOfJourney = this.props.typeOfJourney();

        var bookingSummary = <BookingSummary
            typeOfFlow="quotes"
            items={items}
            showPromo={showPromo}
            showPrice={showPrice}
            selectedCompanies={selectedCompanies}
            selectChange={this.selectCompanyChange}
            formCurrentStep={this.props.formCurrentStep}
            typeOfJourney={typeOfJourney} />;

        if (isLoading) {
            return (
                <React.Fragment>
                    <div className="col-lg-8">
                        <main loader={isLoading ? "show" : "hide"}><Loader /></main>
                    </div>
                    <div className="col-md-3 ml-auto">
                        {bookingSummary}
                    </div>
                </React.Fragment>
            );

        } else {
            return (
                <React.Fragment>
                    <div className="col-lg-8">
                        {this.babySittingRequest()}
                        {typeOfJourney == 1 && this.selectCompanies()}
                        {this.ContactDetails()}
                    </div>
                    <div className="col-md-3 ml-auto d-lg-block d-none">
                        {bookingSummary}
                    </div>
                </React.Fragment>
            )
        }
    }
}

export default QuotesBabySittingPage;