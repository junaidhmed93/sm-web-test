import React from "react";
import FormFieldsTitle from "../../../components/FormFieldsTitle";
import CheckRadioBoxInput from "../../../components/Form_Fields/CheckRadioBoxInput";
import CheckRadioBoxInputWithPrice from "../../../components/Form_Fields/CheckRadioBoxInputWithPrice";
import DatePick from "../../../components/Form_Fields/DatePick";
import SelectInput from "../../../components/Form_Fields/SelectInput";
import TextFloatInput from "../../../components/Form_Fields/TextFloatInput";
import TextareaInput from "../../../components/Form_Fields/TextareaInput";
import PaymentMethods from "../../../components/Form_Fields/PaymentMethods";
import SubmitBooking from "../../../components/Form_Fields/SubmitBooking";
import BookingSummary from "../../../components/Form_Fields/BookingSummary";
import PackageBox from "../../../components/Form_Fields/PackageBox";
import BookNextStep from "../../../components/Form_Fields/BookNextStep";
import FormTitleDescription from "../../../components/FormTitleDescription";
import ContactDetailsStep from "../../../components/ContactDetailsStep";
import QuotesNextStep from "../../../components/Form_Fields/QuotesNextStep";
import SelectCompanyWithFilters from "../../../components/SelectCompanyWithFilters";
import {isValidSection, scrollToTop} from "../../../actions/index";
let allServiceConstant = {};
let URLConstant = {};
let data_values = {};
let current_currency = "AED";
let current_city = "dubai";
class QuotesPaintingPage extends React.Component{

    constructor(props) {
        super();
        this.state = {
            service:"",
            home_type: {id: 1, value: props.data_values.DATA_VALUE_HOME_TYPE_APARTMENT, label: "Apartment"},
            needed_services:"",
            dates_flexible:"",
            event_city:"",
            service_date:'',
            other_comments_text:'',
            input_email: props.userProfile.email,
            input_phone: props.userProfile.address ? props.userProfile.address.phoneNumber : '',
            input_name: props.userProfile.customerFirstName,
            input_last_name: props.userProfile.customerLastName,
            number_of_units: '',
            painting_room_options:[],
            selectedCompanies: []
        }
        this.paintingRequest = this.paintingRequest.bind(this);
        this.ContactDetails = this.ContactDetails.bind(this);
        this.handleCustomChange = this.handleCustomChange.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.currentStep = this.currentStep.bind(this);
        this.moveNext = this.moveNext.bind(this);
        this.homeRoomOptions = this.homeRoomOptions.bind(this);
        this.hashChangeHandler = this.hashChangeHandler.bind(this);
        this.submitData = this.submitData.bind(this);
        this.selectCompanyChange = this.selectCompanyChange.bind(this);
        allServiceConstant = props.service_constants;
    }
    componentDidMount() {
        allServiceConstant = this.props.service_constants;
        URLConstant =  this.props.url_constants;
        current_city = this.props.current_city;
        data_values = this.props.data_values;
        this.homeRoomOptions();
        window.addEventListener("hashchange", this.hashChangeHandler, false);
    }
    componentWillUnmount(){
        window.removeEventListener("hashchange", this.hashChangeHandler, false);
    }
    sumarryItems(){
        const {service, service_date} = this.state;
        return [
            {label: 'Type of Painting', value: service.label},
            {label: 'Date', value: service_date}
        ]; 
    }
    hashChangeHandler(){
        let hashVal = window.location.hash;
        let step;
        hashVal.length ? step=parseInt( window.location.hash.replace('#','') ) : step = 1;
        this.props.moveNext(step);
        scrollToTop();
    }
    currentStep(){
        return this.props.formCurrentStep;
    }
    moveNext(step){
        const {formCurrentStep, signInDetails, showLoginModal} = this.props;
        var valid = true;
        // Step 1 validation
        if( formCurrentStep == '1' ){
            valid = isValidSection('section-request-form');
            if( valid ){
                window.location.hash = step;
                scrollToTop();
                // Show Login Modal
                if( !signInDetails.customer ){
                    showLoginModal();
                }
            }
        }
        // Step 2
        if( formCurrentStep == '2' ){
            window.location.hash = step;
            scrollToTop();
            // Show Login Modal
            if( !signInDetails.customer ){
                showLoginModal();
            }
        }
    }
    submitData(){
        var valid = isValidSection('personal-information-form');
    }
    selectCompanyChange(value){
        this.setState({
            selectedCompanies: value
        });
    }
    handleCustomChange(name, value){
        this.setState({
            [name]: value
        });
        if(name == "home_type"){
            this.homeRoomOptions(value);
        }
    }
    handleInputChange(name, value) {
        this.setState({
            [name]: value
        });
    }
    homeRoomOptions(type_of_home){

        var number_of_units = [];

        var number_of_units_apartment = [
            {id: 1, value: data_values.DATA_VALUE_STUDIO_APARTMENT, label: "Studio"},
            {id: 2, value: data_values.DATA_VALUE_1_BR_APARTMENT, label: "1 BR"},
            {id: 3, value: data_values.DATA_VALUE_2_BR_APARTMENT, label: "2 BR"},
            {id: 4, value: data_values.DATA_VALUE_3_BR_APARTMENT, label: "3 BR"},
            {id: 5, value: data_values.DATA_VALUE_4_BR_APARTMENT, label: "4 BR"},
            {id: 6, value: data_values.DATA_VALUE_5_BR_APARTMENT, label: "5 BR"}
        ];

        var number_of_units_villa = [
            {id: 1, value: data_values.DATA_VALUE_1_BR_VILLA, label: "1 BR"},
            {id: 2, value: data_values.DATA_VALUE_2_BR_VILLA, label: "2 BR"},
            {id: 3, value: data_values.DATA_VALUE_3_BR_VILLA, label: "3 BR"},
            {id: 4, value: data_values.DATA_VALUE_4_BR_VILLA, label: "4 BR"},
            {id: 5, value: data_values.DATA_VALUE_5_BR_VILLA, label: "5 BR"}
        ];

        var home_type_value = typeof type_of_home ==  "undefined" ? data_values.DATA_VALUE_HOME_TYPE_APARTMENT : type_of_home.value;

        number_of_units = home_type_value === data_values.DATA_VALUE_HOME_TYPE_VILLA ? number_of_units_villa : number_of_units_apartment;

        this.setState({
            painting_room_options : number_of_units
        });


    }
    paintingRequest(){

        var data_values = this.props.data_values;

        var service_value = this.state.service;

        var other_comments_text_value = this.state.other_comments_text;

        var service_date_value = this.state.service_date;

        var home_type_value = this.state.home_type;

        var number_of_units_value = this.state.number_of_units;

        var number_of_units_options = this.state.painting_room_options;


        var home_type = [
            {id: 1, value: data_values.DATA_VALUE_HOME_TYPE_APARTMENT, label: "Apartment"},
            {id: 2, value: data_values.DATA_VALUE_HOME_TYPE_VILLA, label: "Villa"}
        ];


        var painting_options = [
            {id: 1, value: allServiceConstant.SERVICE_EXTERIOR_PAINTING, label: "Exterior Painting"},
            {id: 2, value: allServiceConstant.SERVICE_HOME_PAINTING, label: "Home Painting"},
            {id: 3, value: allServiceConstant.SERVICE_MOVE_IN_MOVE_OUT_PAINTING, label: "Move in move out painting"},
            {id: 4, value: allServiceConstant.SERVICE_OFFICE_PAINTING, label: "Office Painting"},
            {id: 5, value: allServiceConstant.SERVICE_PAINT_INDIVIDUAL_ROOMS, label: "Paint individual rooms"},
            {id: 6, value: allServiceConstant.SERVICE_PAINT_INDIVIDUAL_WALLS, label: "Paint individual walls"}
        ];

        var home_element = (<section>
            <FormFieldsTitle title="Describe your home" />
            <CheckRadioBoxInput InputType="radio" name="home_type" inputValue={home_type_value} items={home_type} onInputChange={this.handleCustomChange} parentClass="row mb-2"  childClass="col-12 col-md-4 mb-2 d-flex" />
            <CheckRadioBoxInputWithPrice InputType="radio" name="number_of_units" inputValue={number_of_units_value} items={number_of_units_options} onInputChange={this.handleCustomChange}  childClass="col-6 col-sm-2 mb-3 d-flex" />
        </section>)

        var currentStepClass = this.currentStep() === 1 ? '' : "d-none";

        return (
            <div id="section-request-form" className={currentStepClass+" no-bottom-space"}>
                <section>
                    <FormFieldsTitle title="What type of painting service do you need?"/>
                    <CheckRadioBoxInput 
                    InputType="radio" 
                    name="service" inputValue={service_value} items={painting_options} 
                    parentClass="row mb-1" 
                    childClass="col-6 col-md-4 mb-3 d-flex" 
                    onInputChange={this.handleCustomChange}/>
                </section>
                { ( service_value.value == allServiceConstant.SERVICE_EXTERIOR_PAINTING || service_value.value == allServiceConstant.SERVICE_HOME_PAINTING) && home_element }
                <section>
                    <FormFieldsTitle title="Do you have anything else you'd like to add?"/>
                    <div className="row mb-4">
                        <div className="col mb-3">
                            <TextareaInput inputValue={other_comments_text_value} name="other_comments_text" placeholder="Please tell us more about the type of photography service you are looking for, the number of people you'd like photographed or the size of the event."  />
                        </div>
                    </div>
                </section>
                <section>
                    <div className="row mb-4">
                        <DatePick name="service_date" 
                        inputValue={service_date_value} 
                        label="When do you need the service?" 
                        onInputChange={this.handleInputChange}/>
                    </div>
                </section>
                <section>
                    <QuotesNextStep moveNext={this.moveNext} formCurrentStep={this.props.formCurrentStep} submitData={this.submitData} />
                </section>
            </div>
        )
    }
    selectCompanies(){
        var currentStepClass = this.currentStep() === 2 ? '' : "d-none";
        const serviceID = 1;
        const currentCity = this.props.current_city;
        const {selectedCompanies} = this.state;
        return (
            <div className={currentStepClass}>
                <SelectCompanyWithFilters serviceID={serviceID} currentCity={currentCity} selectChange={this.selectCompanyChange} selectedCompanies={selectedCompanies} />
                <section>
                    <QuotesNextStep moveNext={this.moveNext} formCurrentStep={this.props.formCurrentStep} submitData={this.submitData} />
                </section>
            </div>
        )
    }
    ContactDetails(isLocationDetailShow){
        var countryOptions = [{id: 1, value: "1", label: "Dubai"}];
        var currentStepClass = this.currentStep() === 3 ? '' : "d-none";
        //var currentStepClass =  ''
        var isLocationDetailShow = typeof isLocationDetailShow != "undefined" ? isLocationDetailShow : "yes";


        var contact_details = {
            input_email:this.state.input_email,
            input_phone:this.state.input_phone,
            input_name:this.state.input_name,
            input_last_name:this.state.input_last_name
        }

        return (
            <div id="personal-information-form" className={currentStepClass}>
                <ContactDetailsStep countryOptions={countryOptions} isLocationDetailShow={isLocationDetailShow} contact_details={contact_details} handleInputChange={this.handleInputChange} isSkipCompany = {this.props.isSkipCompany} currentCity = {this.props.current_city}/>
                <section>
                    <QuotesNextStep moveNext={this.moveNext} formCurrentStep={this.props.formCurrentStep} submitData={this.submitData} />
                </section>
            </div>
        )
    }

    render(){
        const {selectedCompanies} = this.state;
        const items = this.sumarryItems();
        const showPromo = false;
        const showPrice = false;
        return(
            <React.Fragment>
            <div className="col-lg-8">
                {this.paintingRequest()}
                {this.selectCompanies()}
                {this.ContactDetails("no")}                  
            </div>
            <div className="col-md-3 ml-auto d-lg-block d-none">
                <BookingSummary 
                typeOfFlow="quotes" 
                items={items} 
                showPromo={showPromo} 
                showPrice={showPrice} 
                selectedCompanies={selectedCompanies} 
                formCurrentStep = {this.props.formCurrentStep}
                selectChange={this.selectCompanyChange}
                />
            </div>
            </React.Fragment>
        )
    }
}

export default QuotesPaintingPage;