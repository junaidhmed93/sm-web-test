import React from "react";
import FormFieldsTitle from "../../../components/FormFieldsTitle";
import CheckRadioBoxInput from "../../../components/Form_Fields/CheckRadioBoxInput";
import CheckRadioBoxInputWithPrice from "../../../components/Form_Fields/CheckRadioBoxInputWithPrice";
import DatePick from "../../../components/Form_Fields/DatePick";
import SelectInput from "../../../components/Form_Fields/SelectInput";
import TextFloatInput from "../../../components/Form_Fields/TextFloatInput";
import TextareaInput from "../../../components/Form_Fields/TextareaInput";
import PaymentMethods from "../../../components/Form_Fields/PaymentMethods";
import SubmitBooking from "../../../components/Form_Fields/SubmitBooking";
import BookingSummary from "../../../components/Form_Fields/BookingSummary";
import PackageBox from "../../../components/Form_Fields/PackageBox";
import BookNextStep from "../../../components/Form_Fields/BookNextStep";
import FormTitleDescription from "../../../components/FormTitleDescription";
import ContactDetailsStep from "../../../components/ContactDetailsStep";
import QuotesNextStep from "../../../components/Form_Fields/QuotesNextStep";
import SelectCompanyWithFilters from "../../../components/SelectCompanyWithFilters";
import {isValidSection, scrollToTop} from "../../../actions/index";
import locationHelper from "../../../helpers/locationHelper";
import commonHelper from "../../../helpers/commonHelper";
import Loader from "../../../components/Loader";
let allServiceConstant = {};
let URLConstant = {};
let current_currency = "AED";
let current_city = "dubai";
var budgetPerGuestOpts = "";
class QuotesCateringPage extends React.Component{

    constructor(props) {
        super(props);
        var quotesData = props.quotesData;
        var selectedCompanies = (typeof quotesData != "undefined" && typeof quotesData.selected_companies != "undefined") ? 
        quotesData.selected_companies : [];
        var service = Object.keys(props.defaultServiceOption).length ? props.defaultServiceOption : "";
        this.state = {
            service: service,
            howManyGuest: "",
            budgetPerGuest: {value: "", label: ""},
            otherEqipments:[],
            cuisine:props.selecteCuisine,
            eventCity: {value: "", label: ""},
            dateOfService:'',
            details:'',
            input_email: props.userProfile.email,
            input_phone: props.userProfile.address ? props.userProfile.address.phoneNumber : '',
            input_name: props.userProfile.customerFirstName,
            input_last_name: props.userProfile.customerLastName,
            selectedCompanies: selectedCompanies
        }
        this.cateringRequest = this.cateringRequest.bind(this);
        this.ContactDetails = this.ContactDetails.bind(this);
        this.handleCustomChange = this.handleCustomChange.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.currentStep = this.currentStep.bind(this);
        this.moveNext = this.moveNext.bind(this);
        this.howToProceed = this.howToProceed.bind(this);
        this.submitData = this.submitData.bind(this);
        this.selectCompanyChange = this.selectCompanyChange.bind(this);
        this.showOrHideModalSelectCompanyError = this.showOrHideModalSelectCompanyError.bind(this);
        allServiceConstant = props.service_constants;

    }
    componentDidMount() {
        allServiceConstant = this.props.service_constants;
        URLConstant =  this.props.url_constants;
        current_city = this.props.current_city;
        var cityOptions = locationHelper.getLocationByName(current_city);
        budgetPerGuestOpts = commonHelper.getNewDataValues("cateringBudgetPerGuestConstant");
        
        var defaultOpt = budgetPerGuestOpts.length ? budgetPerGuestOpts.filter((item) => (
            item.name == "I don't know"
        )) : "";

        if(typeof defaultOpt == "object" && defaultOpt.length){
            defaultOpt = defaultOpt[0];
        }

        // console.log( "budgetPerGuestOpts", defaultOpt );

        //budgetPerGuestOpts.filter()
        
        this.setState({
            eventCity : cityOptions,
            budgetPerGuest: defaultOpt
        })
    }
    sumarryItems(){
        var {service, howManyGuest, budgetPerGuest, cuisine, otherEqipments, dateOfService, eventCity} = this.state,
            cateringCuisines = [],
            cateringEquips = [],items = [
                {label: 'Occasion', value: service.label}
            ];

        if(cuisine.length){
            cuisine.map((item) => {
                cateringCuisines.push(item.label);
            })
            items.push({label: 'Cuisines', value: cateringCuisines.join(", ")})
        }
        if(otherEqipments.length){
            otherEqipments.map((item) => {
                cateringEquips.push(item.label);
            })
            items.push({label: 'Equipments', value: cateringEquips.join(", ")})
        }
        
        howManyGuest = howManyGuest != "" ? parseInt(howManyGuest) : 0;

        if( howManyGuest != 0 ){
            items.push({label: 'Guests', value: howManyGuest})
        }
        if(budgetPerGuest.label !=""){
            items.push({label: 'Budget', value: budgetPerGuest.label})
        }
        items.push(
            {label: 'Date', value: dateOfService},
            {label: 'Place', value: eventCity.label});

        return items;
    }
    hashChangeHandler(){
        let hashVal = window.location.hash;
        let step;
        hashVal.length ? step=parseInt( window.location.hash.replace('#','') ) : step = 1;
        this.props.moveNext(step);
        scrollToTop();
    }
    currentStep(){
        return this.props.formCurrentStep;
    }
    moveNext(step){
        const {moveNextStep} = this.props;
        moveNextStep(step, this.state);
    }
    showOrHideModalSelectCompanyError(boolVal){
        const {showOrHideModalSelectCompanyError} = this.props;
        showOrHideModalSelectCompanyError(boolVal);
    }
    howToProceed(isSkipCompany){
        const {howToProceed} = this.props;

        var selectedCompanies = isSkipCompany ? [] : this.state.selectedCompanies;

        this.setState({
            selectedCompanies: selectedCompanies
        });

        howToProceed(isSkipCompany, this.state);
    }
    submitData(){
        const {submitLeadData} = this.props;
        submitLeadData(this.state);
    }
    selectCompanyChange(value){
        this.setState({
            selectedCompanies: value
        });
    }
    handleCustomChange(name, value){
        this.setState({
            [name]: value
        });
    }
    handleInputChange(name, value) {
        this.setState({
            [name]: value
        });
    }
    cateringRequest(){

        var service_value = this.state.service;

        var budgetPerGuestValue = this.state.budgetPerGuest;

        var detailsValue = this.state.details;

        var dateOfServiceValue = this.state.dateOfService;

        var eventCityValue = this.state.eventCity;

        var howManyGuestValue = this.state.howManyGuest;

        var catering_options = commonHelper.lookupServices('parent_service_id', allServiceConstant.SERVICE_CATERING);

        var budgetPerGuestOpts = commonHelper.getNewDataValues("cateringBudgetPerGuestConstant");

        var dates_flexible_options = [
            {id: 1, value: "yes", label: "Yes"},
            {id: 2, value: "no", label: "No"}
        ];

        var cateringCuisineConstant = commonHelper.getNewDataValues("cateringCuisineConstant");

        //console.log("cateringCuisineConstant", cateringCuisineConstant);

        var cuisineValue = this.state.cuisine;

        var cateringEquipConstant = commonHelper.getNewDataValues("cateringEquipConstant");

        var otherEqipmentsValue = this.state.otherEqipments;

        var cityOptions = locationHelper.fetchCities();

        var hours_needed_value = this.state.hours_needed;

        //other_services
        var typeOfJourney = this.props.typeOfJourney();

        var currentStepClass = "";

        if( typeOfJourney == 1 ){
            currentStepClass = this.currentStep() === 1 ? '' : "d-none";
        }else{
            currentStepClass = this.currentStep() === 2 ? '' : "d-none";
        }

        currentStepClass += " catering-options";

        return (
            <div id="section-request-form" className={currentStepClass+" no-bottom-space"}>
                <section className="type-of-catering">
                    <FormFieldsTitle title="Please select the occasion you require catering for ?"/>
                    <CheckRadioBoxInput
                        InputType="radio"
                        name="service"
                        inputValue={service_value}
                        items={catering_options}
                        parentClass="row mb-1"
                        validationClasses="radio-required"
                        validationMessage="Please select service"
                        childClass="col-12 col-md-3 mb-3"
                        onInputChange={this.handleCustomChange}/>
                </section>
                <section>
                    <div className="row mb-2">
                        <div className="col-12 col-md-6 mb-3">
                            <FormFieldsTitle title="How many guests will attend this event?" />
                            <TextFloatInput InputType="text"
                                            name="howManyGuest"
                                            inputValue={howManyGuestValue}
                                            label="How many guests"
                                            onInputChange={this.handleInputChange}
                                            validationClasses="required numbers"
                                            validationMessage="Please select number of guests"
                                             />
                        </div>
                        <div className="col-12 col-md-6 mb-3">
                            <FormFieldsTitle title="What's your food &amp; drinks budget per guest?" />
                            <SelectInput name="budgetPerGuest"
                                         inputValue={budgetPerGuestValue}
                                         label="Select Budget"
                                         options={budgetPerGuestOpts}
                                         onInputChange={this.handleCustomChange} />
                        </div>
                     </div>
                </section>
                <section className="type-of-catering">
                    <FormTitleDescription title="What cuisines would you like to serve your guests?" desc="Select as many as you like" />
                    <CheckRadioBoxInput
                        InputType="checkbox"
                        name="cuisine"
                        inputValue={cuisineValue}
                        items={cateringCuisineConstant}
                        parentClass="row mb-1" c
                        hildClass="col-6 col-md-4 mb-3 d-flex"
                        onInputChange={this.handleCustomChange}
                        validationClasses = "min-check-1"
                        validationMessage = "Please check at least one cuisine."
                    />
                </section>
                <section>
                    <FormTitleDescription title="Will you require any of the following?" desc="Our partners may provide these at an additional cost" />
                    <CheckRadioBoxInput
                        InputType="checkbox" name="otherEqipments"
                        inputValue={otherEqipmentsValue}
                        items={cateringEquipConstant}
                        parentClass="row mb-1" childClass="col-6 col-md-4 mb-3 d-flex"
                        onInputChange={this.handleCustomChange}/>
                </section>

                <section>
                    <div className="row mb-2">
                        <DatePick name="dateOfService"
                                  disableFriday = {false}
                                  inputValue={dateOfServiceValue}
                                  onInputChange={this.handleInputChange}
                                  title="When will the event take place?"
                                  validationClasses="required"/>
                        <div className="col-12 col-md-6">
                            <FormFieldsTitle title="Where will the event take place?" />
                            <SelectInput
                                name="eventCity"
                                inputValue={eventCityValue}
                                label="City"
                                options={cityOptions}
                                onInputChange={this.handleCustomChange} />
                        </div>
                    </div>
                </section>
                <section>
                    <FormFieldsTitle title="Do you have any special requirements?"/>
                    <div className="row mb-4">
                        <div className="col mb-3">
                            <TextareaInput
                                inputValue={detailsValue}
                                name="details"
                                onInputChange={this.handleCustomChange}
                                placeholder="Example: We may need extra lighting at venue"  />
                        </div>
                    </div>
                </section>
                <section>
                    <QuotesNextStep
                        moveNext={this.moveNext}
                        formCurrentStep={this.props.formCurrentStep}
                        howToProceed={this.howToProceed}
                        submitData={this.submitData} />
                </section>
            </div>
        )
    }
    selectCompanies(){
        var currentStepClass = this.currentStep() === 2 ? '' : "d-none";

        var {service} = this.state;

        const serviceID = allServiceConstant.SERVICE_CATERING; //this.props.parnetServiceId;

        const filterServiceID = service.value;

        const currentCity = this.props.current_city;

        const {selectedCompanies} = this.state;

        const {showSelectCompanyError} = this.props;

        if( typeof(serviceID) != 'undefined' ){
            return (
                <div id="section-company-selector" className={currentStepClass}>
                    <SelectCompanyWithFilters
                        filterService = {filterServiceID}
                        serviceID={serviceID}
                        currentCity={currentCity}
                        selectChange={this.selectCompanyChange}
                        selectedCompanies={selectedCompanies}
                        showSelectCompanyError={showSelectCompanyError}
                        showOrHideModalSelectCompanyError={this.showOrHideModalSelectCompanyError}
                        showCuisineFilter = {true}
                        cuisineFilterValue = {this.state.cuisine}
                        cuisineFilterChange = {this.handleCustomChange}
                    />
                    <section>
                        <QuotesNextStep moveNext={this.moveNext} formCurrentStep={this.props.formCurrentStep}  submitData={this.submitData} />
                    </section>
                </div>
            )
        }
    }
    ContactDetails(isLocationDetailShow){
        var cityOptions = locationHelper.getLocationByName(current_city);
        var currentStepClass = this.currentStep() === 3 ? '' : "d-none";
        //var currentStepClass =  ''
        var isLocationDetailShow = typeof isLocationDetailShow != "undefined" ? isLocationDetailShow : "yes";
        var contact_details = {
            input_email:this.state.input_email,
            input_phone:this.state.input_phone,
            input_name:this.state.input_name,
            input_last_name:this.state.input_last_name
        }

        const serviceID = allServiceConstant.SERVICE_CATERING;
        const currentCity = this.props.current_city;
        const {moving_size} = this.state;
        var userProfile =  this.props.userProfile;

        if(!this.state.loader) {
            return (
                <div id="personal-information-form" className={currentStepClass}>
                    <ContactDetailsStep
                        cityOptions={cityOptions}
                        userProfile = {userProfile}
                        signInDetails={this.props.signInDetails}
                        isLocationDetailShow={isLocationDetailShow}
                        contact_details={contact_details}
                        handleDropDownChange={this.handleInputChange}
                        handleInputChange={this.handleInputChange}
                        mobileSummary={this.mobileSummary}
                        moveNext={this.moveNext}
                        formCurrentStep = {this.props.formCurrentStep}
                        submitData={this.submitData}
                        isSkipCompany = {this.props.isSkipCompany}
                        currentCity = {this.props.current_city}
                    />
                </div>
            )
        } else {
            return (
                <main loader={this.state.loader ? "show" : "hide"}><Loader/></main>
            );
        }
    }
    render(){
        
        const {selectedCompanies} = this.state;
        const items = this.sumarryItems();
        const showPromo = false;
        const showPrice = false;
        var isLoading =  this.state.loader || this.props.loader;

        var typeOfJourney = this.props.typeOfJourney(); 

        var bookingSummary =  <BookingSummary 
        typeOfFlow="quotes" items={items} 
        showPromo={showPromo} showPrice={showPrice}
        selectedCompanies={selectedCompanies} 
        selectChange={this.selectCompanyChange}
        formCurrentStep = {this.props.formCurrentStep}
        typeOfJourney = {typeOfJourney}/>;

        if(isLoading) {
            return (
                <React.Fragment>
                    <div className="col-lg-8">
                        <main loader={ isLoading ? "show" : "hide"}><Loader/></main>
                    </div>
                    <div className="col-md-3 ml-auto">
                        {bookingSummary}
                    </div>
                </React.Fragment>
            );

        }else {
            return (
                <React.Fragment>
                    <div className="col-lg-8">
                        { this.cateringRequest() }
                        { typeOfJourney == 1 && this.selectCompanies()}
                        {this.ContactDetails("no")}
                    </div>
                    <div className="col-md-3 ml-auto d-lg-block d-none">
                        {bookingSummary}
                    </div>
                </React.Fragment>
            )
        }
    }
}

export default QuotesCateringPage;