import React from "react";
import cloneDeep from "lodash/cloneDeep";
import FormFieldsTitle from "../../../components/FormFieldsTitle";
import CheckRadioBoxInput from "../../../components/Form_Fields/CheckRadioBoxInput";
import DatePick from "../../../components/Form_Fields/DatePick";
import SelectInput from "../../../components/Form_Fields/SelectInput";
import TextFloatInput from "../../../components/Form_Fields/TextFloatInput";
import AreaSuggestionInput from "../../../components/Form_Fields/AreaSuggestionInput";
import TextareaInput from "../../../components/Form_Fields/TextareaInput";
import BookingSummary from "../../../components/Form_Fields/BookingSummary";
import FormTitleDescription from "../../../components/FormTitleDescription";
import ContactDetailsStep from "../../../components/ContactDetailsStep";
import QuotesNextStep from "../../../components/Form_Fields/QuotesNextStep";
import SelectCompanyWithFilters from "../../../components/SelectCompanyWithFilters";
import SelectOffers from "../../../components/SelectOffers";
import {isValidSection, scrollToTop, isEmailExist, validEmail, fetchCountriesListAPI, fetchCarDetailsAPI, URLCONSTANT, LANG_AR, DUBAI} from "../../../actions/index";
import locationHelper from "../../../helpers/locationHelper";
import commonHelper from "../../../helpers/commonHelper";
import GeneralModal from "../../../components/GeneralModal";
import PlacesAutocomplete from 'react-places-autocomplete';
import {
    getCountryCodeById, field_cars_number_opt_Constanst, move_type_Constanst, moving_size_Constanst, }
    from '../../../utils/commonUtils';
import moment from 'moment';
import Loader from "../../../components/Loader";
import {connect} from "react-redux";
let allServiceConstant = {};
let URLConstant = {};
let current_currency = "AED";
let current_city = "dubai";
class QuotesMovingForm extends React.Component{
    _isMounted = false;
    constructor(props) {
        super(props);
        var {currentCityData,quotesData} = props;
        var selectedCompanies = (typeof quotesData != "undefined" && typeof quotesData.selected_companies != "undefined") ? 
        quotesData.selected_companies : [];
        allServiceConstant = props.service_constants;
        var typeOfMove = typeof props.typeOfMove != "undefined" ? props.typeOfMove : {id: allServiceConstant.SERVICE_LOCAL_MOVE, value: "Local", label: "Within same country"};
        this.locationHelperInit = locationHelper.init.bind(this);
        this.locationHelperInit();
        let field_from = "";
        if(currentCityData.length && typeof currentCityData[0].cityDto.countryDto != "undefined"){
            let countryData = currentCityData[0].cityDto.countryDto;
            field_from = countryData.name.toString(); //{id: countryData.id, value: countryData.id.toString(), label: countryData.name.toString()}
            //console.log("field_from", field_from);
        }
        this.state = {
            type_of_move:  typeOfMove,
            home_type:"",
            rooms:"",
            moving_size: {value: '', label: ''},
            from_emirate:{value: '', label: ''},
            from_area:"",
            from_address:"",
            to_emirate:{value: '', label: ''},
            to_area:"",
            to_address:"",
            field_from: field_from,//locationHelper.translate("UNITED_ARAB_EMIRATES"),
            field_to: '',
            other_services: [],
            other_comments_text:"",
            item_lists:"",
            move_date:'',
            input_email: props.userProfile.email,
            input_phone: props.userProfile.address ? props.userProfile.address.phoneNumber : '',
            input_name: props.userProfile.customerFirstName,
            input_last_name: props.userProfile.customerLastName,
            selectedCompanies: selectedCompanies,
            selectedOffers: [],
            loginRequired: false,
            isSkipCompany:false,
            showSelectCompanyError: false,
            pickupInvalid: false,
            dropInvalid: false,
            fromCountryInvalid: false,
            toCountryInvalid: false,
            countries: [],
            cars_shipped: [{ field_car_year: "", field_car_make: "", field_car_model: "", field_car_trim: "" }],
            field_cars_number: { id: 1, value: "1", label: "One car" },
            years: [{ id: 1, label: 'Nothing selected' }],
            carDetails: [],
            make: [[], [], []],
            modal: [[], [], []],
            modal_number: [[], [], []]
        }
        this.movingRequest = this.movingRequest.bind(this);
        this.selectCompanies = this.selectCompanies.bind(this);
        this.ContactDetails = this.ContactDetails.bind(this);
        this.handleCustomChange = this.handleCustomChange.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.currentStep = this.currentStep.bind(this);
        this.moveNext = this.moveNext.bind(this);
        this.submitData = this.submitData.bind(this);
        this.selectCompanyChange = this.selectCompanyChange.bind(this);
        this.selectOffersChange = this.selectOffersChange.bind(this);
        this.howToProceed = this.howToProceed.bind(this);
        this.isInternationalMove = this.isInternationalMove.bind(this);
        this.showOrHideModalSelectCompanyError = this.showOrHideModalSelectCompanyError.bind(this);
        this.localLocationOpt = this.localLocationOpt.bind(this);
        this.intlLocationOpt = this.intlLocationOpt.bind(this);
        this.setPickupFocus = this.setPickupFocus.bind(this);
        this.setDropFocus = this.setDropFocus.bind(this);
        this.carShippingSection = this.carShippingSection.bind(this);
        this.carShippingDetails = this.carShippingDetails.bind(this);
        this.loadCarDetails = this.loadCarDetails.bind(this);
        this.carShippedSelectChange = this.carShippedSelectChange.bind(this);
        this.forIntlAddressValidation = this.forIntlAddressValidation.bind(this);

    }
    componentWillUnmount(){
        this._isMounted = false;
    }
    componentDidMount() {
        this._isMounted = true;
        allServiceConstant = this.props.service_constants;
        let {lang} = this.props;
        URLConstant =  this.props.url_constants;
        current_city = this.props.current_city;
        var defaultCity = locationHelper.getLocationByName(current_city);
        var selectedCityVal = {id: defaultCity.id, value: defaultCity.slug, label: defaultCity.name, slug: defaultCity.slug}

        this.setState({
            from_emirate: selectedCityVal,
            to_emirate: selectedCityVal
        });
        const script = document.createElement("script");
        const googleApiKey = commonHelper.getGoogleAPIKey();
        script.src = `https://maps.googleapis.com/maps/api/js?key=${googleApiKey}&libraries=places&callback=myCallbackFunc&language=`+this.props.lang;
        script.async = true;
        document.body.appendChild(script);
        this.setState({ scriptLoaded: window.scriptLoaded });

        //let getCurrentCountry = "";

        fetchCountriesListAPI(res => {
            if (this._isMounted) {
                let countriesList = [];
                if (res.data && res.data.length) {
                    countriesList = res.data.map((d) => {
                        return {id: d.id, value: d.id.toString(), label: d.name.toString()}
                    });
                    if(current_city == DUBAI){

                    }
                    this.setState({countries: countriesList});
                }
            }
        },
        (err) => console.log(err), lang);

        /*fetchCarYearsAPI(response => {
            if (this._isMounted) {
                let years = [];

                if (response.data && response.data.length) {
                    let data = response.data;
                    years = response.data.map((d, i) => {
                        return {id: i + 1, value: d.toString(), label: d.toString()}
                    });

                    this.setState({years: years});
                }
            }
        },
            (err) => console.log(err));*/

    }
    currentStep(){
        return this.props.formCurrentStep;
    }
    moveNext(step) {
        const { moveNextStep } = this.props;
        this.forIntlAddressValidation();
        moveNextStep(step, this.state);
    }
    showOrHideModalSelectCompanyError(boolVal){
        const {showOrHideModalSelectCompanyError} = this.props;
        showOrHideModalSelectCompanyError(boolVal);
    }
    forIntlAddressValidation(){
        let { field_to, field_from, pickupInvalid, dropInvalid, fromCountryInvalid, toCountryInvalid } = this.state;
        pickupInvalid = this.state.from_address == '' ? true : false;
        dropInvalid = this.state.to_address == '' ? true : false;
        fromCountryInvalid = field_from == '' ? true : false;
        toCountryInvalid = field_to == '' ? true : false;
        this.setState({
            pickupInvalid: pickupInvalid,
            dropInvalid: dropInvalid,
            fromCountryInvalid: fromCountryInvalid,
            toCountryInvalid: toCountryInvalid
        });
    }
    howToProceed(isSkipCompany){
        const {howToProceed} = this.props;

        var selectedCompanies = isSkipCompany ? [] : this.state.selectedCompanies;

        this.forIntlAddressValidation();

        this.setState({
            selectedCompanies: selectedCompanies
        });

        howToProceed(isSkipCompany, this.state);
    }
    submitData(){
        const {submitLeadData} = this.props;
        submitLeadData(this.state);
    }
    selectCompanyChange(value){
        this.setState({
            selectedCompanies: value
        });
    }
    selectOffersChange(value){
        this.setState({
            selectedOffers: value
        });
    }
    sumarryItems() {
        const { moving_size, from_emirate, from_area, to_emirate, to_area, move_date, type_of_move, field_from, field_to } = this.state;
        
        var isIntlMove = this.isInternationalMove();

        var movingFromtext = isIntlMove ? field_from : from_emirate.label;

        var movingTotext = isIntlMove ? field_to : to_emirate.label;

        if(!isIntlMove){
            movingFromtext += from_area != "" ? " - " + from_area : "";

            movingTotext += to_area != "" ? " - " + to_area : "";
        }

        return [
            {label: locationHelper.translate('TYPE_OF_MOVE'), value: type_of_move.label},
            {label: locationHelper.translate('TYPE_OF_HOME'), value: moving_size.label},
            {label: locationHelper.translate('MOVING_FROM'), value: movingFromtext},
            {label: locationHelper.translate('MOVING_TO'), value: movingTotext},
            {label: locationHelper.translate('SCHEDULED_FOR'), value: move_date}
        ]; 
    }
    
    handleCustomChange(name, item){
        let fromCountryId = '', toCountryId = '';
        
        const countryOptions = this.state.countries;

        const Val = !!item.label ? item.label : item;

        if (name == 'field_from') {
            fromCountryId = countryOptions.find(c => c.label === Val).id;

        }

        if (name == 'field_to') {
            toCountryId = countryOptions.find(c => c.label === Val).id;

        }
        this.setState({
            [name]: (name == 'field_from' || name == 'field_to' ? Val : item),
            sourceCountryId: fromCountryId,
            targetCountryId: toCountryId,
        });

        /*this.setState({
            [name]: item
        });*/

        if( name == "moving_size" ){
            let home_type = "";
            let rooms = "";
            if(item.value == "Studio Apartment"){
                home_type = "Apartment";
                rooms = "Studio";
            }else{
                let home = item.value.split(" ");
                home_type = home[2];
                rooms = home[0];
            }

            this.setState({
                "moving_size":item
            });
        }
        else if (name == "field_cars_number") {
            var value = item;
            var car_models = [];
            var cars_shipped = this.state.cars_shipped;
            const idx = this.state.other_services.findIndex((a) => a.value === "car-shipping-carrier");
            if (idx > -1) {
                for (var i = 0; i < value.value; i++) {
                    if (!!cars_shipped[i])
                        car_models.push(cars_shipped[i]);
                    else
                        car_models.push({ field_car_year: { value: "", label: "" }, field_car_make: { value: "", label: "" }, field_car_model: { value: "", label: "" }, field_car_trim: { value: "", label: "" } });
                }

                this.setState({
                    cars_shipped: car_models
                });
            }
        }
        else if( name == "from_emirate" || name == "to_emirate"){
            let from_emirate = "";
            let to_emirate = "";
            if( name == "from_emirate"){
                /*from_emirate = item.value;
                to_emirate = this.state.to_emirate.value;*/
                var from_area_ele = document.getElementsByName("from_area")[0];
                from_area_ele.value = "";
                from_area_ele.classList.remove("sm-valid");
                this.setState({
                    "from_area":''
                });

            }
            if( name == "to_emirate"){
                /*from_emirate = this.state.from_emirate.value;
                to_emirate = item.value;*/
                var to_area_ele =  document.getElementsByName("to_area")[0];
                to_area_ele.value = "";
                to_area_ele.classList.remove("sm-valid");
                this.setState({
                    "to_area":''
                });
            }
        }

    }
    handleInputChange(name, value) {
        if(name === 'type_of_move'){
            this.setState({
                [name]: value,
                to_area: "",
                to_address: "",
                from_area: "",
                from_address: "",
            });
        }
        else if (name === 'from_address') {
            this.setState({
                [name]: value,
                pickupInvalid: !!value ? false : true,
            });
        }
        else if (name === 'to_address') {
            this.setState({
                [name]: value,
                dropInvalid: !!value ? false : true,
            });
        }
        else {
            this.setState({
                [name]: value
            });
        }
    }
    handleNoOfUnits(name, value){
        this.setState({
            number_of_units: value
        });
    }
    setPickupFocus(val) {
        this.setState({ pickupFocus: val });
    }
    setDropFocus(val) {
        this.setState({ dropFocus: val });
    }
    initPickup() {
        //console.log('initPickup');
    }
    onError = (status, clearSuggestions) => {
        console.log('Google Maps API returned error with status: ', status)
        clearSuggestions();
    }
    isInternationalMove(){
        var  {type_of_move} = this.state;
        return type_of_move.id == allServiceConstant.SERVICE_INTERNATIONAL_MOVE ? true : false;
    }
    carShippedSelectChange = (idx) => (name, value) => {

        var idx_val = parseInt(idx) + 1;

        var new_name = name.replace("_" + idx_val, "")

        const newCarShip = this.state.cars_shipped.map((car_shipped, sidx) => {
            if (idx !== sidx) return car_shipped;
            return { ...car_shipped, [new_name]: value };
        });


        this.setState({ cars_shipped: newCarShip });

        if (name.indexOf('_year') > -1) {
            this.loadCarDetails('year', value.label, idx);
        }

        if (name.indexOf('_make') > -1) {
            this.loadCarDetails('make', value.label, idx);
        }

        if (name.indexOf('_model') > -1) {
            this.loadCarDetails('model', value.label, idx);
        }
    }
    loadCarDetails(type, val, id) {

        let carMake = this.state.make;
        let carModel = this.state.modal;
        let carNumber = this.state.modal_number;
        let years = cloneDeep(this.state.years);
        let { cars_shipped } = this.state;

        if (type === 'year') {
            fetchCarDetailsAPI(val, response => {

                if (response.data && response.data.length) {
                    let res = response.data;
                    res = res.filter(a => { if (a != '') return a; });
                    carMake[id] = res.map((model, i) => {
                        return { id: i + 1, value: model.modelMakeName.toString(), label: model.modelMakeName.toString() };
                    });

                    const yearObj = years.find(x => x.label === val);

                    cars_shipped[id] = {
                        field_car_year: yearObj ? yearObj : '',
                        field_car_make: carMake[id] && carMake[id][0] ? carMake[id][0] : '',
                        field_car_model: '', field_car_trim: ''
                    };

                    this.setState({
                        carDetails: res, make: carMake,
                        cars_shipped: cars_shipped,

                    });
                }
                this.loadCarDetails('make', carMake[id] && carMake[id][0] ? carMake[id][0].value : '', id);
            },
                (err) => console.log(err));
        }
        else if (type === 'make') {
            const { carDetails } = this.state;

            let make = carDetails.find(a => a.modelMakeName === val);

            if (make && make.modelNames && make.modelNames.length > 0) {
                make.modelNames = make.modelNames.filter(a => { if (a != '') return a; });
                carModel[id] = make.modelNames.map((a, i) => {
                    return { id: i + 1, value: a.modelName.toString(), label: a.modelName.toString() };
                });
            }
            else
                carModel[id] = [];


            cars_shipped[id] = {
                field_car_year: cars_shipped[id].field_car_year,
                field_car_make: carMake[id].find(a => a.value == val),
                field_car_model: carModel[id] && carModel[id][0] ? carModel[id][0] : '',
                field_car_trim: ''
            };

            this.setState({ modal: carModel, cars_shipped: cars_shipped });
            this.loadCarDetails('model', carModel[id] && carModel[id][0] ? carModel[id][0].value : '', id);
        }
        else if (type === 'model') {
            //carNumber = [];
            const { carDetails } = this.state;
            const { make } = cloneDeep(this.state);


            const item = carDetails.find(a => a.modelMakeName == cars_shipped[id].field_car_make.value);

            let modelName = '';
            if (item && item.modelNames) {
                modelName = item.modelNames.find(a => a.modelName == val);
            }


            let modelNo = '';
            if (modelName && modelName.modelNumbers)
                modelNo = modelName.modelNumbers
            else modelNo = '';


            if (modelNo && modelNo != '' && modelNo.length > 0) {
                modelNo = modelNo.filter(a => { if (a != '') return a; });
                carNumber[id] = modelNo.map((a, i) => {
                    return { id: i + 1, value: a.toString(), label: a.toString() };
                });
            }
            else
                carNumber[id] = [];

            cars_shipped[id] = {
                field_car_year: cars_shipped[id].field_car_year,
                field_car_make: cars_shipped[id].field_car_make,
                field_car_model: carModel[id].find(a => a.value == val),//cars_shipped[id].field_car_model,
                field_car_trim: carNumber[id] && carNumber[id][0] ? carNumber[id][0] : ''
            }

            this.setState({ modal_number: carNumber, cars_shipped: cars_shipped });
        }

    }
    carShippingSection() {
        var field_cars_number_opt = field_cars_number_opt_Constanst;

        var field_cars_number_value = this.state.field_cars_number;

        return (
            <section>
                <FormFieldsTitle title="How many cars do you want shipped?" />
                <CheckRadioBoxInput InputType="radio" name="field_cars_number" inputValue={field_cars_number_value} items={field_cars_number_opt} childClass="col-4 mb-3 d-flex" onInputChange={this.handleCustomChange} />
            </section>
        )
    }
    carShippingDetails() {
        const { years, make, modal, modal_number } = this.state;
        const year_opt = years;


        const make_opt = make;

        const modal_opt = modal;

        const modal_number_opt = modal_number;
        const count = this.state.cars_shipped.length;
        return (
            this.state.cars_shipped.map((car_shipped, idx) => (

                <section key={idx}>
                    <FormFieldsTitle title={`Describe your car ${count > 1 ? '(' + (idx + 1) + ')' : ''}`} />
                    <div className="row">
                        <div className="col-12 col-md-3 mb-3">
                            <FormFieldsTitle title="Model year:" className='h4' />
                            <SelectInput searchable={true} inputValue={car_shipped.field_car_year} 
                                name={"field_car_" + (idx + 1) + "_year"} label="Select model year" options={year_opt}
                                onInputChange={this.carShippedSelectChange(idx)} validationClasses="required" />
                        </div>
                        <div className="col-12 col-md-3 mb-3">
                            <FormFieldsTitle title="Car make:" className='h4' />
                            <SelectInput searchable={true} inputValue={car_shipped.field_car_make} 
                                name={"field_car_" + (idx + 1) + "_make"} label='Nothing selected' options={make_opt[idx]}
                                onInputChange={this.carShippedSelectChange(idx)} />
                        </div>
                        <div className="col-12 col-md-3 mb-3">
                            <FormFieldsTitle title="Car model:" className='h4' />
                            <SelectInput searchable={true} inputValue={car_shipped.field_car_model} 
                                name={"field_car_" + (idx + 1) + "_model"} label='Nothing selected' options={modal_opt[idx]}
                                onInputChange={this.carShippedSelectChange(idx)} />
                        </div>
                        <div className="col-12 col-md-3 mb-3">
                            <FormFieldsTitle title="Model number:" className='h4' />
                            <SelectInput searchable={true} 
                                inputValue={car_shipped.field_car_trim && car_shipped.field_car_trim.label != '' ? car_shipped.field_car_trim : 'Nothing selected'}
                                name={"field_car_" + (idx + 1) + "_trim"} label='Nothing selected' options={modal_number_opt[idx]}
                                onInputChange={this.carShippedSelectChange(idx)} />
                        </div>
                    </div>
                </section>
            ))
        )

    }
    movingRequest(){
        var isIntlMove = this.isInternationalMove();

        var data_values = this.props.data_values;

        var service_constants = allServiceConstant;

        var move_type = [
            {id: allServiceConstant.SERVICE_LOCAL_MOVE, value: "Local", label: locationHelper.translate("WITHIN_SAME_COUNTRY")},
            {id: allServiceConstant.SERVICE_INTERNATIONAL_MOVE, value: "International", label: locationHelper.translate("INTERNATIONALLY")}
        ];

        var moving_size = [
            {id: 9, value: "studio", label: locationHelper.translate("STUDIO")},
            {id: 1, value: "1 Br", label: locationHelper.translate("BR_1")},
            {id: 2, value: "2 Br", label: locationHelper.translate("BR_2")},
            {id: 3, value: "3 Br", label: locationHelper.translate("BR_3")},
            {id: 4, value: "4 Br", label: locationHelper.translate("BR_4")},
            {id: 5, value: "2 Br h/v", label: locationHelper.translate("BR_2_VILLA")},
            {id: 6, value: "3 Br h/v", label: locationHelper.translate("BR_3_VILLA")},
            {id: 7, value: "4 Br h/v", label: locationHelper.translate("BR_4_VILLA")},
            {id: 8, value: "5 Br h/v", label: locationHelper.translate("BR_5_VILLA")},
            {id: 10, value: "other", label: locationHelper.translate("OTHER")},
            {id: 11, value: "office", label: locationHelper.translate("OFFICE")},

        ];
        var other_services = [
            {id: 1, value: "Handyman", label: locationHelper.translate("HANDYMAN"), intlServiceId: allServiceConstant.SERVICE_BASIC_HANDYMAN_SERVICES_DRILLING_ON_THE_WALL_CURTAIN_HANGING_INTERNATIONAL_MOVE, localServiceId: allServiceConstant.SERVICE_BASIC_HANDYMAN_SERVICES_DRILLING_ON_THE_WALL_CURTAIN_HANGING_LOCAL_MOVE},
            {id: 2, value: "Storage", label: locationHelper.translate("STORAGE"), intlServiceId: allServiceConstant.SERVICE_STORAGE_INTERNATIONAL_MOVE, localServiceId: allServiceConstant.SERVICE_STORAGE_LOCAL_MOVE}
        ];
        if(isIntlMove){
            /*other_services = [
                { id: allServiceConstant.SERVICE_BASIC_HANDYMAN_SERVICES_DRILLING_ON_THE_WALL_CURTAIN_HANGING_INTERNATIONAL_MOVE, value: "handyman", label: "Handyman" },
                { id: allServiceConstant.SERVICE_STORAGE_INTERNATIONAL_MOVE, value: "storage", label: "Storage" },
                { id: allServiceConstant.SERVICE_CAR_SHIPPING_WITH_MOVE_INTERNATIONAL_MOVE, value: "car-shipping-carrier", label: "Car shipping" }
            ];*/
            other_services.push({id: 3, value: "car-shipping-carrier", label: locationHelper.translate("CAR_SHIPPING"), intlServiceId: allServiceConstant.SERVICE_CAR_SHIPPING_WITH_MOVE_INTERNATIONAL_MOVE, localServiceId: 0});
        }
        /*else{
            other_services = [
                {id: 1, value: "Handyman", label: "Handyman"},
                {id: 2, value: "Storage", label: "Storage"}
            ];
            other_services = [
                {id: allServiceConstant.SERVICE_BASIC_HANDYMAN_SERVICES_DRILLING_ON_THE_WALL_CURTAIN_HANGING_LOCAL_MOVE, value: "Handyman", label: "Handyman"},
                {id: allServiceConstant.SERVICE_STORAGE_LOCAL_MOVE, value: "Storage", label: "Storage"}
            ];
        }*/

        var other_services_value = this.state.other_services;

        var moving_size_value = this.state.moving_size;

        var from_emirate_value = this.state.from_emirate;

        var from_area_value = this.state.from_area;

        var from_address_value = this.state.from_address;

        var to_emirate_value = this.state.to_emirate;

        var to_area_value = this.state.to_area;

        var to_address_value = this.state.to_address;

        var other_comments_text_value = this.state.other_comments_text;

        var items_list_value = this.state.item_lists;

        var calender_date_of_service_value = this.state.calender_date_of_service;

        //other_services
        var typeOfJourney = this.props.typeOfJourney();

        var currentStepClass = "";
        
        if( typeOfJourney == 1 ){
            currentStepClass = this.currentStep() === 1 ? '' : "d-none";
        }else{
            currentStepClass = this.currentStep() === 2 ? '' : "d-none";
        }

        var allCities = locationHelper.fetchCities();

        var cityOptions = [];

        allCities.map(function(item){
            let value = commonHelper.slugify(item.label);
            cityOptions.push({id: item.id, value: value, label: item.label});
        })

        var from_city_id = from_emirate_value !="" && Object.keys(from_emirate_value).length ? from_emirate_value.id : locationHelper.getCurrentCityId();

        var to_city_id = to_emirate_value !="" && Object.keys(to_emirate_value).length ? to_emirate_value.id : locationHelper.getCurrentCityId();

        var from_areas =  locationHelper.getAreasByCity(from_city_id);

        var to_areas =  locationHelper.getAreasByCity(to_city_id);
        // console.log("this.props.startDate", moment(this.props.startDate));
        
        
        const idx = other_services_value.findIndex((a) => a.value === "car-shipping-carrier");
        const cap_shipping_section = idx > -1 ? this.carShippingSection() : "";
        const cap_to_be_shipped_section = idx > -1 ? this.carShippingDetails() : '';

        var typeOfJourney = this.props.typeOfJourney();

        var frmTitle = isIntlMove ? locationHelper.translate("PLEASE_SHARE_THE_DETAILS_ABOUT_YOUR_MOVE_INTL") : locationHelper.translate("PLEASE_SHARE_THE_DETAILS_ABOUT_YOUR_MOVE");
        return (
            <div id="section-request-form" className={currentStepClass+" no-bottom-space"}>
                <section>
                    <FormFieldsTitle title={frmTitle} titleClass="h5 mb-4" />
                </section>
                <section className={ typeOfJourney != 1 ? "d-none" : ""}>
                    <FormFieldsTitle title={locationHelper.translate("ARE_YOU_MOVING_WITHIN_THE_SAME_COUNTRY_OR_INTERNATIONALLY")} />
                    <CheckRadioBoxInput 
                        InputType="radio" 
                        name="type_of_move" 
                        inputValue={this.state.type_of_move} 
                        items={move_type}  
                        childClass="col-6 mb-3 d-flex" 
                        onInputChange={this.handleInputChange}/>
                </section>
                <section>
                    <FormFieldsTitle title={locationHelper.translate("WHEN_DO_YOU_NEED_TO_MOVE")} />
                    <div className="row mb-2">
                        <DatePick name="move_date"
                        selectedDate ={this.state.move_date}
                        inputValue={this.state.move_date}
                        onInputChange={ this.handleCustomChange } 
                        startDate={moment(this.props.startDate)} 
                        disableFriday={false} 
                        validationClasses="required"/>
                    </div>
                </section>
                { isIntlMove ? this.intlLocationOpt() : this.localLocationOpt() }
                <section>
                    <FormFieldsTitle title={locationHelper.translate("MOVING_SIZE")} />
                    <div className="row mb-2">
                        <div className="col-12 col-md-6 mb-3">
                            <SelectInput name="moving_size" inputValue={moving_size_value} 
                            label={locationHelper.translate("SELECT_SIZE")} options={moving_size} onInputChange={this.handleCustomChange} />
                        </div>
                    </div>
                </section>
                <section>
                    <FormFieldsTitle title={locationHelper.translate("DO_YOU_NEED_ANY_OF_THESE_SERVICES")} />
                    <CheckRadioBoxInput 
                        InputType="checkbox" 
                        name="other_services" 
                        inputValue={other_services_value} 
                        items={other_services}  
                        childClass={ !isIntlMove ? "col-6 mb-3 d-flex" : "col-4 mb-3 d-flex" }
                        onInputChange={this.handleCustomChange}
                    />
                </section>
                {/*cap_shipping_section}
                {cap_to_be_shipped_section */}
                <section>
                    <FormFieldsTitle title={locationHelper.translate("WHAT_ELSE_SHOULD_WE_KNOW")} />
                    <div className="row mb-4">
                        <div className="col mb-3">
                            <TextareaInput name="other_comments_text" 
                            placeholder={locationHelper.translate("MOVING_TEXTAREA_PLACEHOLDER")} inputValue={other_comments_text_value} onInputChange={this.handleCustomChange} />
                        </div>
                    </div>
                </section>

                <section>
                    <QuotesNextStep moveNext={this.moveNext} formCurrentStep={this.props.formCurrentStep} howToProceed={this.howToProceed} submitData={this.submitData} />
                </section>
            </div>
        )
    }
    intlLocationOpt(){

        let from_country_value = this.state.field_from;

        let from_address_value = this.state.from_address;

        let to_country_value = this.state.field_to;

        let to_address_value = this.state.to_address;

        const pickupFocused = this.state.pickupFocus;
        
        const dropFocused = this.state.dropFocus;

        const countryOptions = this.state.countries;

        let selectedFromCountry = countryOptions.find(x => x.label === from_country_value);

        selectedFromCountry = !!selectedFromCountry ? selectedFromCountry : { code: 'AE' }

        let selectedToCountry = countryOptions.find(x => x.label == to_country_value);
        selectedToCountry = !!selectedToCountry ? selectedToCountry : { value: '', label: '', code: 'AE' }

        const fromCountryCode = getCountryCodeById(selectedFromCountry.value)
        const toCountryCode = getCountryCodeById(selectedToCountry.value)

        let invalidClassFrom = this.state.pickupInvalid ? 'btn-radio form-control p-3 border rounded border-danger invalidClassFrom' : 'btn-radio form-control p-3 border rounded invalidClassFrom';


        if (this.state.pickupInvalid) {
            let sectionP = document.getElementById('pickup');
            if( sectionP != null ){
                if( sectionP.nextSibling != null ){ sectionP.nextSibling.remove(); }
                sectionP.classList.add('border-danger')
                let span = document.createElement("span");
                span.className = "error-msg";
                span.innerText = locationHelper.translate('THIS_FIELD_IS_REQUIRED');
                if (!!sectionP) sectionP.parentNode.appendChild(span);
            }

           
        }
        let invalidClassTo = this.state.dropInvalid ? 'btn-radio form-control p-3 border rounded border-danger invalidClassTo' : 'btn-radio form-control p-3 border rounded invalidClassTo';
        if (this.state.dropInvalid) {
            let sectionD = document.getElementById('dropoff');
            if( sectionD != null ){
                if( sectionD.nextSibling != null){ sectionD.nextSibling.remove(); }
                sectionD.classList.add('border-danger');
                let span = document.createElement("span");
                span.className = "error-msg";
                span.innerText = locationHelper.translate('THIS_FIELD_IS_REQUIRED');
                if (!!sectionD) sectionD.parentNode.appendChild(span);
            }
        }

        if (this.state.fromCountryInvalid) {
            let sectionP = document.getElementById('fromCountry');
            let span = document.createElement("span");
            span.className = "error-msg";
            span.innerText = locationHelper.translate('THIS_FIELD_IS_REQUIRED');

            if (!!sectionP) sectionP.parentNode.appendChild(span);
        }

        if (this.state.toCountryInvalid) {
            let sectionP = document.getElementById('toCountry');
            let span = document.createElement("span");
            span.className = "error-msg";
            span.innerText = locationHelper.translate('THIS_FIELD_IS_REQUIRED');

            if (!!sectionP) sectionP.parentNode.appendChild(span);
        }
        
        return (<section>
                    <FormFieldsTitle title={locationHelper.translate("WHERE_ARE_YOU_MOVING_FROM")} />
                    <div className="row mb-2">
                        <div className="col-12 col-md-6 mb-3">
                            <SelectInput name="field_from" id='fromCountry' inputValue={selectedFromCountry} label={locationHelper.translate("SELECT_COUNTRY")} searchable={true} options={countryOptions} onInputChange={this.handleCustomChange} validationClasses="required" />
                        </div>
                        <div className="col-12 col-md-6 mb-3">

                            <div className="floating-label ">
                                <PlacesAutocomplete
                                    value={from_address_value}
                                    onChange={value => this.handleInputChange('from_address', value)}
                                    onSelect={values => this.handleInputChange('from_address', values)}
                                    onError={() => this.onError}
                                    searchOptions={{  componentRestrictions: { country: fromCountryCode } }}
                                    label={locationHelper.translate("ADDRESS")} googleCallbackName="initPickup"
                                >
                                    {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                                        <div>
                                            <div className={"floating-label autofill " + (from_address_value || pickupFocused ? ' active' : '')} >
                                                <div className="form-item form-type-textfield form-item-field-international-address-from"
                                                    onClick={() => this.setPickupFocus(true)} onBlur={() => this.setPickupFocus(false)}
                                                >
                                                    <input
                                                        {...getInputProps({
                                                            className: invalidClassFrom,
                                                            placeholder: locationHelper.translate('ENTER_A_LOCATION'),
                                                            id: 'pickup',
                                                            autoComplete: 'off',
                                                            disabled: false
                                                        })}

                                                    /></div>
                                                <label htmlFor="pickup">{locationHelper.translate("ADDRESS")}</label>
                                            </div>
                                            <div className={'pac-container pac-logo w-100' + (!!suggestions.length ? ' show' : '')}
                                                style={!!suggestions.length ? { display: 'block' } :
                                                    { display: 'none' }}>
                                                {loading && <div>{locationHelper.translate('LOADING')}</div>}
                                                {suggestions.map(suggestion => {
                                                    const className = 'pac-item-query pac-matched';
                                                    // inline style for demonstration purpose
                                                    const style = suggestion.active
                                                        ? { backgroundColor: '#fafafa', cursor: 'pointer' }
                                                        : { backgroundColor: '#ffffff', cursor: 'pointer' };
                                                    return (
                                                        <div className='pac-item' >
                                                            <div
                                                                {...getSuggestionItemProps(suggestion, {
                                                                    className
                                                                })}
                                                                style={{ textOverflow: 'ellipsis', overflow: 'hidden' }}
                                                            >
                                                                <span className='pac-icon pac-icon-marker' />
                                                                <span>{suggestion.description}</span>
                                                            </div>
                                                        </div>

                                                    );
                                                })}
                                            </div>
                                        </div>
                                    )}
                                </PlacesAutocomplete>
                            </div>
                        </div>
                    </div>
                    <FormFieldsTitle title={locationHelper.translate("WHERE_ARE_YOU_MOVING_TO")} />
                    <div className="row mb-2">
                        <div className="col-12 col-md-6 mb-3">
                            <SelectInput searchable={true} id='toCountry' name="field_to" inputValue={selectedToCountry} label={locationHelper.translate("SELECT_COUNTRY")} options={countryOptions} onInputChange={this.handleCustomChange} />
                        </div>
                        <div className="col-12 col-md-6 mb-3">
                            <div className="floating-label ">
                                <PlacesAutocomplete
                                    label={locationHelper.translate("SELECT_COUNTRY")}
                                    value={to_address_value}
                                    onError={() => this.onError}
                                    onChange={value => this.handleInputChange('to_address', value)}
                                    onSelect={values => this.handleInputChange('to_address', values)}
                                    searchOptions={{ componentRestrictions: { country: toCountryCode } }}
                                    googleCallbackName="initDropoff"
                                >
                                    {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                                        <div>
                                            <div className={"floating-label autofill " + (to_address_value || dropFocused ? ' active' : '')} >
                                                <div className="form-item form-type-textfield form-item-field-international-address-from"
                                                    onClick={() => this.setDropFocus(true)} onBlur={() => this.setDropFocus(false)}>
                                                    <input
                                                        {...getInputProps({
                                                            placeholder: locationHelper.translate('ENTER_A_LOCATION'),
                                                            className: invalidClassTo,
                                                            id: 'dropoff',
                                                            autoComplete: 'off',
                                                            disabled: false
                                                        })}


                                                    /></div>
                                                <label htmlFor="dropoff">{locationHelper.translate("ADDRESS")}</label>
                                            </div>
                                            <div className={'pac-container pac-logo w-100' + (!!suggestions.length ? ' show' : 'show')}
                                                style={!!suggestions.length ? { display: 'block' } : { display: 'none' }} >
                                                {loading && <div>{locationHelper.translate('LOADING')}</div>}
                                                {suggestions.map(suggestion => {
                                                    const className = 'pac-item-query pac-matched';

                                                    // inline style for demonstration purpose
                                                    const style = suggestion.active
                                                        ? { backgroundColor: '#fafafa', cursor: 'pointer' }
                                                        : { backgroundColor: '#ffffff', cursor: 'pointer' };
                                                    return (
                                                        <div className='pac-item' >
                                                            <div
                                                                {...getSuggestionItemProps(suggestion, {
                                                                    className
                                                                })}
                                                                style={{ textOverflow: 'ellipsis', overflow: 'hidden' }}
                                                            >
                                                                <span className='pac-icon pac-icon-marker' />
                                                                <span>{suggestion.description}</span>
                                                            </div>
                                                        </div>
                                                    );
                                                })}
                                            </div>
                                        </div>
                                    )}
                                </PlacesAutocomplete>
                            </div>
                        </div>
                    </div>
                </section>);
    }
    localLocationOpt(){
        var from_emirate_value = this.state.from_emirate;

        var from_area_value = this.state.from_area;

        var from_address_value = this.state.from_address;

        var to_emirate_value = this.state.to_emirate;

        var to_area_value = this.state.to_area;

        var to_address_value = this.state.to_address;

        var allCities = locationHelper.fetchCities();

        var cityOptions = [];
        //console.log("from_emirate_value", from_emirate_value);
        
        allCities.map(function(item){
            let city_value = item.slug;
            cityOptions.push({ id: item.id, value: city_value, name: item.name, label: item.name, slug: city_value });
        })

        var from_city_id = from_emirate_value !="" && Object.keys(from_emirate_value).length ? from_emirate_value.id : locationHelper.getCurrentCityId();

        var to_city_id = to_emirate_value !="" && Object.keys(to_emirate_value).length ? to_emirate_value.id : locationHelper.getCurrentCityId();

        var from_areas =  locationHelper.getAreasByCity(from_city_id);

        var to_areas =  locationHelper.getAreasByCity(to_city_id);

        return(
            <section>
                <FormFieldsTitle title={locationHelper.translate("WHERE_ARE_YOU_MOVING_FROM")}/>
                <div className="row mb-1">
                    <div className="col-12 col-md-4 mb-3">
                        <SelectInput name="from_emirate" 
                            inputValue={from_emirate_value} 
                            label={locationHelper.translate("CITY")} 
                            options={cityOptions} 
                            onInputChange={this.handleCustomChange} />
                    </div>
                    <div className="col-12 col-md-4 mb-3">
                        <AreaSuggestionInput allowNonArea = {true} InputType="text" name="from_area" areas={from_areas} inputValue={from_area_value} label={locationHelper.translate("AREA")} onInputChange={this.handleInputChange} validationClasses="required" />
                    </div>
                    <div className="col-12 col-md-4 mb-3">
                        <TextFloatInput InputType="text" name="from_address" inputValue={from_address_value} label={locationHelper.translate("BUILDING_OR_STREET_NO")} onInputChange={this.handleInputChange} validationClasses="required" />
                    </div>
                </div>
                <FormFieldsTitle title={locationHelper.translate("WHERE_ARE_YOU_MOVING_TO")}/>
                <div className="row mb-2">
                    <div className="col-12 col-md-4 mb-3">
                        <SelectInput name="to_emirate" inputValue={to_emirate_value} label={locationHelper.translate("CITY")} options={cityOptions} onInputChange={this.handleCustomChange} />
                    </div>
                    <div className="col-12 col-md-4 mb-3">
                        <AreaSuggestionInput allowNonArea = {true} InputType="text" name="to_area" areas={to_areas} inputValue={to_area_value} label={locationHelper.translate("AREA")} onInputChange={this.handleInputChange} validationClasses="required" />
                    </div>
                    <div className="col-12 col-md-4 mb-3">
                        <TextFloatInput InputType="text" name="to_address" inputValue={to_address_value} label={locationHelper.translate("BUILDING_OR_STREET_NO")} onInputChange={this.handleInputChange} validationClasses="required" />
                    </div>
                </div>
                <div className="row mb-1 d-none">
                    <div className="col move-international-quotes-link">
                        <p><small>Moving internationally? <a href="../international-movers/journey1">Click here</a> to get quotes </small></p>
                    </div>
                </div>
            </section>
        );
    }
    selectCompanies(){
        var currentStepClass = this.currentStep() === 2 ? '' : "d-none";
        
        const serviceID = this.isInternationalMove() ? allServiceConstant.SERVICE_INTERNATIONAL_MOVE : allServiceConstant.SERVICE_LOCAL_MOVE;
        
        const currentCity = this.props.current_city;

        const {selectedCompanies} = this.state;

        const {showSelectCompanyError} = this.props;

        if( typeof(serviceID) != 'undefined' ){
            return (
                <div id="section-company-selector" className={currentStepClass}>
                    <SelectCompanyWithFilters 
                        serviceID={serviceID}
                        currentCity={currentCity}
                        selectChange={this.selectCompanyChange}
                        selectedCompanies={selectedCompanies}
                        showSelectCompanyError={showSelectCompanyError}
                        showOrHideModalSelectCompanyError={this.showOrHideModalSelectCompanyError}  />
                    <section>
                        <QuotesNextStep moveNext={this.moveNext} formCurrentStep={this.props.formCurrentStep}  submitData={this.submitData} />
                    </section>
                </div>
            )
        }
    }
    ContactDetails(isLocationDetailShow){
        var cityOptions = locationHelper.getLocationByName(current_city);
        var currentStepClass = this.currentStep() === 3 ? '' : "d-none";
        
        //var currentStepClass =  ''
        var isLocationDetailShow = typeof isLocationDetailShow != "undefined" ? isLocationDetailShow : "yes";
        var contact_details = {
            input_email:this.state.input_email,
            input_phone:this.state.input_phone,
            input_name:this.state.input_name,
            input_last_name:this.state.input_last_name
        }
        
        //const serviceID = allServiceConstant.SERVICE_LOCAL_MOVE;
        const serviceID = this.isInternationalMove() ? allServiceConstant.SERVICE_INTERNATIONAL_MOVE : allServiceConstant.SERVICE_LOCAL_MOVE;
        const currentCity = this.props.current_city;
        const {moving_size} = this.state;
        var {userProfile, lang} =  this.props;
        
        var backButtonLink = "#1";

        var {isSkipCompany} = this.props;

        if(typeof isSkipCompany != "undefined"){

            backButtonLink = isSkipCompany ? "#1" : "#2";
        }

        if(!this.state.loader) {
            return (
                <div id="personal-information-form" className={currentStepClass}>
                    <ContactDetailsStep
                        cityOptions={cityOptions}
                        userProfile = {userProfile}
                        signInDetails={this.props.signInDetails}
                        isLocationDetailShow={isLocationDetailShow}
                        contact_details={contact_details}
                        handleDropDownChange={this.handleInputChange}
                        handleInputChange={this.handleInputChange}
                        mobileSummary={this.mobileSummary}
                        moveNext={this.moveNext}
                        formCurrentStep = {this.props.formCurrentStep}
                        submitData={this.submitData}
                        isSkipCompany = {this.props.isSkipCompany}
                        backButton = {false}
                        currentCity = {this.props.current_city}
                    />
                    { serviceID && currentCity && moving_size.value &&
                        <SelectOffers serviceID={serviceID} currentCity={currentCity} selectOffersChange={this.selectOffersChange} selectedOffers={this.state.selectedOffers} movingSize={moving_size.value} />
                    }
                    <div className="row mt-2 goback">
                            <div className="col-12">
                                <a href={backButtonLink} className="text-black"> <i className={lang == LANG_AR ? "fa fa-angle-right" : "fa fa-angle-left"}></i> {locationHelper.translate("BACK")}</a>
                            </div>
                    </div>
                </div>
            )
        } else {
            return (
                <main loader={this.state.loader ? "show" : "hide"}><Loader/></main>
            );
        }
    }
    showOrHideModalSelectCompanyError(boolVal){
        const {showOrHideModalSelectCompanyError} = this.props;
        showOrHideModalSelectCompanyError(boolVal);
    }
    render(){
        const {selectedCompanies} = this.state;

        var typeOfJourney = this.props.typeOfJourney(); 
        
        const items = this.sumarryItems();
        const showPromo = false;
        const showPrice = false;

        var isLoading =  this.state.loader || this.props.loader;

        var isIntlMove = this.isInternationalMove()
        
        var bookingSummary = <BookingSummary 
            typeOfFlow="quotes" 
            items={items} 
            showPromo={showPromo} 
            showPrice={showPrice}
            formCurrentStep = {this.props.formCurrentStep}
            selectedCompanies={selectedCompanies}
            selectChange={this.selectCompanyChange}
            typeOfJourney = {typeOfJourney}
            serviceSlug = { isIntlMove ? URLCONSTANT.INTERNATIONAL_MOVER_PAGE_URI : URLCONSTANT.LOCAL_MOVER_PAGE_URI }
        />
        //<BookingSummary typeOfFlow="quotes" items={items} showPromo={showPromo} showPrice={showPrice}  selectChange={this.selectCompanyChange} selectedCompanies={selectedCompanies} formCurrentStep={this.props.formCurrentStep}/>
        return(
            <React.Fragment>
            <div className="col-lg-8">
                { 
                    isLoading ?  <main loader={ isLoading ? "show" : "hide"}><Loader/></main> : 
                (<React.Fragment>
                    {this.movingRequest()}
                    { typeOfJourney == 1 && this.selectCompanies()}
                    {this.ContactDetails("no")}
                </React.Fragment>)
            }             
            </div>
            <div className="col-md-3 ml-auto d-lg-block d-none">
                 {bookingSummary}
            </div>
            </React.Fragment>
        )
    }
}
function mapStateToProps(state){
    return {
        currentCityData: state.currentCityData,
        currentCity: state.currentCity,
        lang: state.lang
    }
}
export default connect(mapStateToProps)(QuotesMovingForm);