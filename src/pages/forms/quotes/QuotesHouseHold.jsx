import React from "react";
import FormFieldsTitle from "../../../components/FormFieldsTitle";
import CheckRadioBoxInput from "../../../components/Form_Fields/CheckRadioBoxInput";
import SelectInput from "../../../components/Form_Fields/SelectInput";
import DatePick from "../../../components/Form_Fields/DatePick";
import TextareaInput from "../../../components/Form_Fields/TextareaInput";
import SubmitBooking from "../../../components/Form_Fields/SubmitBooking";
import BookingSummary from "../../../components/Form_Fields/BookingSummary";
import QuotesNextStep from "../../../components/Form_Fields/QuotesNextStep";
import FormTitleDescription from "../../../components/FormTitleDescription";
import ContactDetailsStep from "../../../components/ContactDetailsStep";
import SelectCompanyWithFilters from "../../../components/SelectCompanyWithFilters";
import {isValidSection, scrollToTop, formTitle, URLCONSTANT} from "../../../actions/index";
import locationHelper from "../../../helpers/locationHelper";
import commonHelper from "../../../helpers/commonHelper";
import Loader from "../../../components/Loader";
import {connect} from "react-redux";
import moment from 'moment';
let allServiceConstant = {};
let URLConstant = {};
let current_currency = "AED";
let currentCity = "dubai";
let dataValues = {};
let bookingFrequencyDataConstants = {};
class QuotesHouseHold extends React.Component{

    constructor(props) {
        super(props);
        var quotesData = props.quotesData;
        var selectedCompanies = (typeof quotesData != "undefined" && typeof quotesData.selected_companies != "undefined") ? 
        quotesData.selected_companies : [];
        this.locationHelperInit = locationHelper.init.bind(this);
        this.locationHelperInit();
        this.state = {
            service : !!props.defaultServiceOption ? props.defaultServiceOption : '',
            booking_time: '',
            dateOfService: '',
            field_service_needed: {},
            hours_required: {},
            field_own_equipments: {id: 'no', value: "No", label: locationHelper.translate("NO")},
            details:"",
            input_email: props.userProfile.email,
            input_phone: props.userProfile.address ? props.userProfile.address.phoneNumber : '',
            input_name: props.userProfile.customerFirstName,
            input_last_name: props.userProfile.customerLastName,
            input_address_city:'',
            input_address_area: props.setUserArea,
            input_address_area_building_name: props.userProfile.address ? props.userProfile.address.building : '',
            input_address_area_building_apartment: props.userProfile.address ? props.userProfile.address.apartment : '',
            selectedCompanies: selectedCompanies,
            loader: false

        }
        this.ContactDetails = this.ContactDetails.bind(this);
        this.CleaningRequest = this.CleaningRequest.bind(this);
        this.handleCustomChange = this.handleCustomChange.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleDropDownChange = this.handleDropDownChange.bind(this);
        this.currentStep = this.currentStep.bind(this);
        this.moveNext = this.moveNext.bind(this);
        this.howToProceed = this.howToProceed.bind(this);
        this.submitData = this.submitData.bind(this);
        this.selectCompanyChange = this.selectCompanyChange.bind(this);
        this.showOrHideModalSelectCompanyError = this.showOrHideModalSelectCompanyError.bind(this);
        this.isHoursOptionToShow = this.isHoursOptionToShow.bind(this);
        allServiceConstant = props.service_constants;

    }
    componentDidMount() {
        allServiceConstant = this.props.service_constants;
        URLConstant =  this.props.url_constants;
        currentCity = this.props.currentCity;

        bookingFrequencyDataConstants = typeof this.props.dataConstants["BOOKING_FREQUENCY"] != "undefined" ? this.props.dataConstants["BOOKING_FREQUENCY"] : {};
        dataValues = commonHelper.processDataValues(bookingFrequencyDataConstants);
        var cityOptions = locationHelper.getLocationByName(currentCity);
        console.log("cityOptions", cityOptions);
        let hoursLbl = locationHelper.translate("HOURS");
        this.setState({
            field_service_needed : {id: "1", value: dataValues.DATA_VALUE_EVERY_WEEK, label: locationHelper.translate("EVERY_WEEK")},
            hours_required:{id: 4, value: "4", label: "4 "+hoursLbl},
            input_address_city : cityOptions
        })
    }
    currentStep(){
        return this.props.formCurrentStep;
    }
    moveNext(step){
        const {moveNextStep} = this.props;
        moveNextStep(step, this.state);
    }
    showOrHideModalSelectCompanyError(boolVal){
        const {showOrHideModalSelectCompanyError} = this.props;
        showOrHideModalSelectCompanyError(boolVal);
    }
    howToProceed(isSkipCompany){
        const {howToProceed} = this.props;

        var selectedCompanies = isSkipCompany ? [] : this.state.selectedCompanies;

        this.setState({
            selectedCompanies: selectedCompanies
        });

        howToProceed(isSkipCompany, this.state);
    }
    submitData(){
        const {submitLeadData} = this.props;
        submitLeadData(this.state);
    }
    selectCompanyChange(value){
        this.setState({
            selectedCompanies: value
        });
    }
    sumarryItems(){
        const {service, dateOfService, hours_required, field_own_equipments, field_service_needed} = this.state;
        var items = [
            {label: locationHelper.translate("SERVICE"), value: service.label},
            {label: locationHelper.translate("DATE"), value: dateOfService}
        ]; 
        var hoursOptionShow = this.isHoursOptionToShow();

        if(hoursOptionShow){
            items.push(
                {label: locationHelper.translate("HOURS"), value: hours_required.label},
                {label: locationHelper.translate("EQUIPMENT"), value: field_own_equipments.label},
                {label: locationHelper.translate("FREQUENCY"), value: field_service_needed.label}
            );
        }
        return items;
    }
    handleCustomChange(name, value){
        this.setState({
            [name]: value
        });
    }
    handleInputChange(name, value){
        this.setState({
            [name]: value
        });
    }
    handleDropDownChange(name, value){
        // console.log(name, value);
    }
    isHoursOptionToShow(){
        var service = this.state.service;
        var servicesOptions = commonHelper.lookupServices('parent_service_id', allServiceConstant.SERVICE_CLEANING_AND_MAID_SERVICES);

        var services = servicesOptions.length ? servicesOptions : [];
        var hoursOptionShow = true;
        if(services.length){
            var hoursOptionShowServices = [allServiceConstant.SERVICE_HOME_CLEANING,
                allServiceConstant.SERVICE_OFFICE_CLEANING,
                allServiceConstant.SERVICE_DEEP_CLEANING_SERVICE,
                allServiceConstant.SERVICE_WINDOW_CLEANING,
                allServiceConstant.SERVICE_OTHER__CLEANING
            ];

            if( hoursOptionShowServices.includes(service.value) ){
                hoursOptionShow = true;
            }else{
                hoursOptionShow = false;
            }
        }
        return hoursOptionShow;
    }
    CleaningRequest(){
        var servicesOptions = commonHelper.lookupServices('parent_service_id', allServiceConstant.SERVICE_CLEANING_AND_MAID_SERVICES);

        var services = servicesOptions.length ? servicesOptions : [];

        var cleaning_frequency = [
            {id: "3", value: dataValues.DATA_VALUE_ONCE, label: locationHelper.translate("ONE_TIME_ONLY")},
            {id: "1", value: dataValues.DATA_VALUE_EVERY_WEEK, label: locationHelper.translate("EVERY_WEEK")},
            {id: "2", value: dataValues.DATA_VALUE_EVERY_TWO_WEEKS, label:locationHelper.translate("EVERY_2_WEEKS")}
        ];
        let hoursLbl = locationHelper.translate("HOURS");
        
        var hours_required = [
            {id: 2, value: "2", label: "2 "+hoursLbl},
            {id: 3, value: "3", label: "3 "+hoursLbl},
            {id: 4, value: "4", label: "4 "+hoursLbl},
            {id: 5, value: "5", label: "5 "+hoursLbl},
            {id: 6, value: "6", label: "6 "+hoursLbl},
            {id: 7, value: "7", label: "7 "+hoursLbl},
            {id: 8, value: "8", label: "8 "+hoursLbl}
        ];

        var field_own_equipments = [
            {id: 'yes', value: "Yes", label: locationHelper.translate("YES")},
            {id: 'no', value: "No", label: locationHelper.translate("NO")}
        ];

        var service_needed_value = this.state.field_service_needed;

        var hours_required_value = this.state.hours_required;

        var field_own_equipments_value = this.state.field_own_equipments;

        var service = this.state.service;

        var typeOfJourney = this.props.typeOfJourney();

        var currentStepClass = "";

        //console.log("typeOfJourney", typeOfJourney, this.currentStep());

        if( typeOfJourney == 1 ){
            currentStepClass = this.currentStep() === 1 ? '' : "d-none";
        }else{
            currentStepClass = this.currentStep() === 2 ? '' : "d-none";
        }

        var hoursOptionShow = this.isHoursOptionToShow();

        var now = moment(this.props.startDate);
        var {lang} = this.props;
        return (
            <div id="section-request-form" className={currentStepClass+" no-bottom-space"}>
                <FormFieldsTitle title={formTitle( URLCONSTANT.CLEANING_MAID_PAGE_URI, lang )} titleClass="h5 mb-4" />
                <section>
                    <div className="row mb-2 multiple-row">
                        <div className="col-12 col-md-6 mb-3">
                            <FormFieldsTitle title={locationHelper.translate("WHICH_SERVICE_DO_YOU_NEED_QUOTES_FOR")} />
                            <SelectInput name="service" inputValue={service} label={locationHelper.translate("SELECT_SERVICE_TLE")} options={services} onInputChange={this.handleInputChange} validationClasses="required"/>
                        </div>
                        <DatePick 
                        title = {locationHelper.translate("WHEN_DO_YOU_NEED_THE_SERVICE")}
                        selectedDate={this.state.dateOfService}
                        inputValue={this.state.dateOfService}
                        name="dateOfService" 
                        onInputChange={this.handleInputChange}
                        disableFriday = {false}
                        placeholderText={locationHelper.translate("SELECT_DATE")}
                        validationClasses="required"
                        validationMessage = {locationHelper.translate("PLEASE_SELECT_DATE")}
                        startDate={now}/>
                    </div>
                </section>
                {
                    hoursOptionShow &&
                    (<section>
                        <FormFieldsTitle title={locationHelper.translate("HOW_MANY_HOURS_OF_SERVICE_DO_YOU_NEED")} />
                        <div className="row mb-2 multiple-row">
                            <div className="col-12 col-md-6 mb-3">
                                <SelectInput 
                                    name="hours_required" 
                                    inputValue={hours_required_value} 
                                    label={locationHelper.translate("SELECT_HOURS")} 
                                    options={hours_required} 
                                    onInputChange={this.handleInputChange} 
                                    validationClasses="required"/>
                            </div>
                        </div>
                        <FormFieldsTitle title={locationHelper.translate("DO_YOU_WANT_US_TO_BRING_OUR_OWN_CLEANING_EQUIPMENT")} />
                        <CheckRadioBoxInput 
                        InputType="radio" 
                        name="field_own_equipments" 
                        inputValue={field_own_equipments_value} items={field_own_equipments}  
                        childClass="col-6 col-md-3 mb-3 d-flex" 
                        onInputChange={this.handleCustomChange} />

                        <FormFieldsTitle title={locationHelper.translate("HOW_OFTEN_DO_YOU_NEED_THE_SERVICE")} />
                        <CheckRadioBoxInput 
                        InputType="radio" 
                        name="field_service_needed" 
                        inputValue={service_needed_value} 
                        items={cleaning_frequency} 
                        childClass="col-6 col-sm-4 mb-3 d-flex" 
                        onInputChange={this.handleCustomChange}/>
                    </section>)
                }
                <section>
                    <FormFieldsTitle title={locationHelper.translate("WHAT_ELSE_WOULD_YOU_LIKE_US_TO_KNOW")} />
                    <div className="row mb-4">
                        <div className="col mb-3">
                            <TextareaInput
                                name="details"
                                inputValue={this.state.details}
                                onInputChange={this.handleCustomChange}
                                placeholder={locationHelper.translate("HOUSE_HOLD_SERVICE_TEXTAREA")}  />
                        </div>
                    </div>
                </section>
                <section>
                    <QuotesNextStep moveNext={this.moveNext} formCurrentStep={this.props.formCurrentStep} howToProceed={this.howToProceed} submitData={this.submitData} />
                </section>
            </div>
        )
    }
    selectCompanies(){
        var currentStepClass = this.currentStep() === 2 ? '' : "d-none";

        var {service} = this.state;

        const serviceID = allServiceConstant.SERVICE_CLEANING_AND_MAID_SERVICES;

        const filterServiceID = service.value;

        const currentCity = this.props.currentCity;

        const {selectedCompanies} = this.state;

        const {showSelectCompanyError} = this.props;

        if( typeof(serviceID) != 'undefined' ){
            return (
                <div id="section-company-selector" className={currentStepClass}>
                    <SelectCompanyWithFilters
                        filterService = {filterServiceID}
                        serviceID={serviceID}
                        currentCity={currentCity}
                        selectChange={this.selectCompanyChange}
                        selectedCompanies={selectedCompanies}
                        showSelectCompanyError={showSelectCompanyError}
                        showOrHideModalSelectCompanyError={this.showOrHideModalSelectCompanyError}  />
                    <section>
                        <QuotesNextStep moveNext={this.moveNext} formCurrentStep={this.props.formCurrentStep}  submitData={this.submitData} />
                    </section>
                </div>
            )
        }
    }
    ContactDetails(){

        var cityOptions = locationHelper.getLocationByName(currentCity);

        var currentStepClass = this.currentStep() === 3 ? '' : "d-none";
        //var currentStepClass =  ''
        var isLocationDetailShow = "yes";

        var contact_details = {
            input_email:this.state.input_email,
            input_phone:this.state.input_phone,
            input_name:this.state.input_name,
            input_last_name:this.state.input_last_name
        }

        if(isLocationDetailShow){
            contact_details.input_address_city = this.state.input_address_city != "" ? this.state.input_address_city : cityOptions;
            contact_details.input_address_area = this.state.input_address_area;
            contact_details.input_address_area_building_name = this.state.input_address_area_building_name;
            contact_details.input_address_area_building_apartment = this.state.input_address_area_building_apartment;
        }

        var userProfile =  this.props.userProfile;

        if(!this.state.loader) {
            return (
                <div id="personal-information-form" className={currentStepClass}>
                    <ContactDetailsStep
                        cityOptions={cityOptions}
                        userProfile={userProfile}
                        signInDetails={this.props.signInDetails}
                        isLocationDetailShow={isLocationDetailShow}
                        contact_details={contact_details}
                        handleDropDownChange={this.handleInputChange}
                        handleInputChange={this.handleInputChange}
                        mobileSummary={this.mobileSummary}
                        moveNext={this.moveNext}
                        formCurrentStep={this.props.formCurrentStep}
                        submitData={this.submitData}
                        isSkipCompany = {this.props.isSkipCompany}
                        currentCity = {this.props.currentCity}
                    />
                </div>
            )
        }else {
            return (
                <main loader={this.state.loader ? "show" : "hide"}><Loader/></main>
            );
        }
    }
    componentWillReceiveProps (newProps) {
        if (newProps.setUserArea !== this.props.setUserArea) {
            this.setState({
                input_address_area: newProps.setUserArea
            })
        }
    }
    render(){
        const {selectedCompanies, formCurrentStep} = this.state;
        const items = this.sumarryItems();
        const showPromo = false;
        const showPrice = false;

        var typeOfJourney = this.props.typeOfJourney(); 


        var bookingSummary = <BookingSummary 
        typeOfFlow="quotes" 
        items={items} 
        showPromo={showPromo} 
        showPrice={showPrice}
        formCurrentStep = {formCurrentStep}
        selectedCompanies={selectedCompanies}
        selectChange={this.selectCompanyChange}
        formCurrentStep = {this.props.formCurrentStep}
        typeOfJourney = {typeOfJourney}/>;

        var isLoading =  this.state.loader || this.props.loader;

        if(isLoading) {
            return (
                <React.Fragment>
                    <div className="col-lg-8">
                        <main loader={ isLoading ? "show" : "hide"}><Loader/></main>
                    </div>
                    <div className="col-md-3 ml-auto">
                        {bookingSummary}
                    </div>
                </React.Fragment>
            );

        }else {
            return (
                <React.Fragment>
                    <div className="col-lg-8">
                        {this.CleaningRequest()}
                        { typeOfJourney == 1 && this.selectCompanies() }
                        {this.ContactDetails()}
                    </div>
                    <div className="col-md-3 ml-auto d-lg-block d-none">
                        {bookingSummary}
                    </div>
                </React.Fragment>
            )
        }
    }
}
function mapStateToProps(state){
    return {
        currentCity: state.currentCity,
        lang: state.lang
    }
}
export default connect(mapStateToProps)(QuotesHouseHold);
