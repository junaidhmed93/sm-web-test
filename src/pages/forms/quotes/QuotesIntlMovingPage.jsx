import React from "react";
import cloneDeep from "lodash/cloneDeep";
import FormFieldsTitle from "../../../components/FormFieldsTitle";
import CheckRadioBoxInput from "../../../components/Form_Fields/CheckRadioBoxInput";
import DatePick from "../../../components/Form_Fields/DatePick";
import SelectInput from "../../../components/Form_Fields/SelectInput";
import TextareaInput from "../../../components/Form_Fields/TextareaInput";
import BookingSummary from "../../../components/Form_Fields/BookingSummary";
import ContactDetailsStep from "../../../components/ContactDetailsStep";
import QuotesNextStep from "../../../components/Form_Fields/QuotesNextStep";
import SelectCompanyWithFilters from "../../../components/SelectCompanyWithFilters";
import {  scrollToTop, fetchCountriesListAPI, fetchCarYearsAPI, fetchCarDetailsAPI } from "../../../actions/index";
import Loader from "../../../components/Loader";
import commonHelper from "../../../helpers/commonHelper";
import {
    getCountryCodeById, field_cars_number_opt_Constanst, move_type_Constanst, moving_size_Constanst, }
    from '../../../utils/commonUtils';
import PlacesAutocomplete from 'react-places-autocomplete';
import moment from 'moment';
import disclaimer_reducer from "../../../reducers/disclaimer_reducer";
let allServiceConstant = {};
let URLConstant = {};
let current_currency = "AED";
let current_city = "dubai";
var countryOptions = '';

class QuotesIntlMovingPage extends React.Component {
    _isMounted = false;

    constructor(props) {
        super(props);
        var quotesData = props.quotesData;
        var selectedCompanies = (typeof quotesData != "undefined" && typeof quotesData.selected_companies != "undefined") ?
            quotesData.selected_companies : [];
        this.state = {
            type_of_move: { id: 1, label: 'International', value: 'full_move' },
            home_type: "",
            rooms: "",
            moving_size: { value: '', label: '' },
            field_from: 'UAE',
            from_address: "",
            field_to: '',
            to_address: "",
            other_services: [],
            cars_shipped: [{ field_car_year: "", field_car_make: "", field_car_model: "", field_car_trim: "" }],
            field_cars_number: { id: 1, value: "1", label: "One car" },
            other_comments_text: "",
            item_lists: "",
            calender_date_of_service: '',
            input_email: props.userProfile.email,
            input_phone: props.userProfile.address ? props.userProfile.address.phoneNumber : '',
            input_name: props.userProfile.customerFirstName,
            input_last_name: props.userProfile.customerLastName,
            payment: 'credit',
            package_options: [],
            transportation_fees: 0,
            selectedCompanies: selectedCompanies,
            pickupInvalid: false,
            dropInvalid: false,
            fromCountryInvalid: false,
            toCountryInvalid: false,
            countries: [],
            years: [{ id: 1, label: 'Nothing selected' }],
            carDetails: [],
            make: [[], [], []],
            modal: [[], [], []],
            modal_number: [[], [], []],
            showSelectCompanyError: false,
            pickupFocus: false,
            dropFocus: false,
            scriptLoaded: false,
            loader: props.loader,
            scriptLoaded:false,
        }
        this.movingRequest = this.movingRequest.bind(this);
        this.ContactDetails = this.ContactDetails.bind(this);
        this.handleCustomChange = this.handleCustomChange.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.currentStep = this.currentStep.bind(this);
        this.moveNext = this.moveNext.bind(this);
        this.carShippingSection = this.carShippingSection.bind(this);
        this.carShippingDetails = this.carShippingDetails.bind(this);
        this.submitData = this.submitData.bind(this);
        this.selectCompanyChange = this.selectCompanyChange.bind(this);
        this.howToProceed = this.howToProceed.bind(this);
        this.loadCarDetails = this.loadCarDetails.bind(this);
        this.handleCountryChange = this.handleCountryChange.bind(this);
        this.showOrHideModalSelectCompanyError = this.showOrHideModalSelectCompanyError.bind(this);
        this.setPickupFocus = this.setPickupFocus.bind(this);
        this.setDropFocus = this.setDropFocus.bind(this);
        allServiceConstant = props.service_constants;

    }

    componentWillUnmount(){
        this._isMounted = false;
    }
    componentDidMount() {
        this._isMounted = true;
        const script = document.createElement("script");
        const googleApiKey = commonHelper.getGoogleAPIKey();
        script.src = `https://maps.googleapis.com/maps/api/js?key=${googleApiKey}&libraries=places&callback=myCallbackFunc`;
        script.async = true;
        document.body.appendChild(script);
        this.setState({ scriptLoaded: window.scriptLoaded });

        fetchCountriesListAPI(res => {
            if (this._isMounted) {
                let countriesList = [];

                if (res.data && res.data.length) {
                    countriesList = res.data.map((d) => {
                        return {id: d.id, value: d.id.toString(), label: d.name.toString()}
                    });

                    this.setState({countries: countriesList});

                }
            }
        },
            (err) => console.log(err));
        fetchCarYearsAPI(response => {
            if (this._isMounted) {
                let years = [];

                if (response.data && response.data.length) {
                    let data = response.data;
                    years = response.data.map((d, i) => {
                        return {id: i + 1, value: d.toString(), label: d.toString()}
                    });

                    this.setState({years: years});
                }
            }
        },
            (err) => console.log(err));
        allServiceConstant = this.props.service_constants;
        URLConstant = this.props.url_constants;
        current_city = this.props.current_city;
    }

    loadCarDetails(type, val, id) {

        let carMake = this.state.make;
        let carModel = this.state.modal;
        let carNumber = this.state.modal_number;
        let years = cloneDeep(this.state.years);
        let { cars_shipped } = this.state;

        if (type === 'year') {
            fetchCarDetailsAPI(val, response => {

                if (response.data && response.data.length) {
                    let res = response.data;
                    res = res.filter(a => { if (a != '') return a; });
                    carMake[id] = res.map((model, i) => {
                        return { id: i + 1, value: model.modelMakeName.toString(), label: model.modelMakeName.toString() };
                    });

                    const yearObj = years.find(x => x.label === val);

                    cars_shipped[id] = {
                        field_car_year: yearObj ? yearObj : '',
                        field_car_make: carMake[id] && carMake[id][0] ? carMake[id][0] : '',
                        field_car_model: '', field_car_trim: ''
                    };

                    this.setState({
                        carDetails: res, make: carMake,
                        cars_shipped: cars_shipped,

                    });
                }
                this.loadCarDetails('make', carMake[id] && carMake[id][0] ? carMake[id][0].value : '', id);
            },
                (err) => console.log(err));
        }
        else if (type === 'make') {
            const { carDetails } = this.state;

            let make = carDetails.find(a => a.modelMakeName === val);

            if (make && make.modelNames && make.modelNames.length > 0) {
                make.modelNames = make.modelNames.filter(a => { if (a != '') return a; });
                carModel[id] = make.modelNames.map((a, i) => {
                    return { id: i + 1, value: a.modelName.toString(), label: a.modelName.toString() };
                });
            }
            else
                carModel[id] = [];


            cars_shipped[id] = {
                field_car_year: cars_shipped[id].field_car_year,
                field_car_make: carMake[id].find(a => a.value == val),
                field_car_model: carModel[id] && carModel[id][0] ? carModel[id][0] : '',
                field_car_trim: ''
            };

            this.setState({ modal: carModel, cars_shipped: cars_shipped });
            this.loadCarDetails('model', carModel[id] && carModel[id][0] ? carModel[id][0].value : '', id);
        }
        else if (type === 'model') {
            //carNumber = [];
            const { carDetails } = this.state;
            const { make } = cloneDeep(this.state);


            const item = carDetails.find(a => a.modelMakeName == cars_shipped[id].field_car_make.value);

            let modelName = '';
            if (item && item.modelNames) {
                modelName = item.modelNames.find(a => a.modelName == val);
            }


            let modelNo = '';
            if (modelName && modelName.modelNumbers)
                modelNo = modelName.modelNumbers
            else modelNo = '';


            if (modelNo && modelNo != '' && modelNo.length > 0) {
                modelNo = modelNo.filter(a => { if (a != '') return a; });
                carNumber[id] = modelNo.map((a, i) => {
                    return { id: i + 1, value: a.toString(), label: a.toString() };
                });
            }
            else
                carNumber[id] = [];

            cars_shipped[id] = {
                field_car_year: cars_shipped[id].field_car_year,
                field_car_make: cars_shipped[id].field_car_make,
                field_car_model: carModel[id].find(a => a.value == val),//cars_shipped[id].field_car_model,
                field_car_trim: carNumber[id] && carNumber[id][0] ? carNumber[id][0] : ''
            }

            this.setState({ modal_number: carNumber, cars_shipped: cars_shipped });
        }

    }
    sumarryItems() {
        const { type_of_move, field_from, field_to, moving_size, calender_date_of_service, move_date } = this.state;

        return [
            { label: 'Type of move', value: type_of_move.label },
            { label: 'Date', value: calender_date_of_service },
            { label: 'Moving from', value: field_from },
            { label: 'Moving to', value: field_to },
            { label: 'Type of home', value: moving_size.label },
            { label: 'Scheduled for', value: move_date }

        ];
    }
    hashChangeHandler() {
        let hashVal = window.location.hash;
        let step;
        hashVal.length ? step = parseInt(window.location.hash.replace('#', '')) : step = 1;
        this.props.moveNext(step);
        scrollToTop();
    }
    currentStep() {
        return this.props.formCurrentStep;
    }
    howToProceed(isSkipCompany) {
        const { howToProceed } = this.props;
        let { field_to, field_from, pickupInvalid, dropInvalid, fromCountryInvalid, toCountryInvalid } = this.state;

        pickupInvalid = this.state.from_address == '' ? true : false;
        dropInvalid = this.state.to_address == '' ? true : false;
        fromCountryInvalid = field_from == '' ? true : false;
        toCountryInvalid = field_to == '' ? true : false;

        var nextStep = isSkipCompany ? 3 : 2;

        var selectedCompanies = isSkipCompany ? [] : this.state.selectedCompanies;

        this.setState({
            isSkipCompany: isSkipCompany,
            selectedCompanies: selectedCompanies,
            pickupInvalid: pickupInvalid,
            dropInvalid: dropInvalid,
            fromCountryInvalid: fromCountryInvalid,
            toCountryInvalid: toCountryInvalid

        });

        //this.moveNext(nextStep);
        howToProceed(isSkipCompany, this.state);

    }
    moveNext(step) {
        const { moveNextStep } = this.props;

        let { field_to, field_from, pickupInvalid, dropInvalid, fromCountryInvalid, toCountryInvalid } = this.state;

        pickupInvalid = this.state.from_address == '' ? true : false;
        dropInvalid = this.state.to_address == '' ? true : false;
        fromCountryInvalid = field_from == '' ? true : false;
        toCountryInvalid = field_to == '' ? true : false;

        this.setState({
            pickupInvalid: pickupInvalid,
            dropInvalid: dropInvalid,
            fromCountryInvalid: fromCountryInvalid,
            toCountryInvalid: toCountryInvalid

        });
        moveNextStep(step, this.state);
    }
    submitData() {
        const { submitLeadData } = this.props;
        submitLeadData(this.state);
    }

    selectCompanyChange(value) {
        this.setState({
            selectedCompanies: value
        });

    }
    handleCountryChange = id => (value, name) => {

        const Val = !!value.label ? value.label : value;

        this.setState({
            [id]: id == 'field_from' || id == 'field_to' ? Val : value,
        });

    }
    handleCustomChange(name, value) {
        let fromCountryId = '', toCountryId = '';
        const countryOptions = this.state.countries;
        const Val = !!value.label ? value.label : value;

        if (name == 'field_from') {
            fromCountryId = countryOptions.find(c => c.label === Val).id;

        }

        if (name == 'field_to') {
            toCountryId = countryOptions.find(c => c.label === Val).id;

        }
        this.setState({
            [name]: (name == 'field_from' || name == 'field_to' ? Val : value),
            sourceCountryId: fromCountryId,
            targetCountryId: toCountryId,
        });

        if (name == "moving_size") {
            let home_type = "";
            let rooms = "";
            if (value.value == "Studio Apartment") {
                home_type = "Apartment";
                rooms = "Studio";
            } else {
                let home = value.value.split(" ");
                home_type = home[2];
                rooms = home[0];
            }

            this.setState({
                "moving_size": value
            });
        }
        else if (name == "field_cars_number") {

            var car_models = [];
            var cars_shipped = this.state.cars_shipped;
            const idx = this.state.other_services.findIndex((a) => a.value === "car-shipping-carrier");
            if (idx > -1) {
                for (var i = 0; i < value.value; i++) {
                    if (!!cars_shipped[i])
                        car_models.push(cars_shipped[i]);
                    else
                        car_models.push({ field_car_year: { value: "", label: "" }, field_car_make: { value: "", label: "" }, field_car_model: { value: "", label: "" }, field_car_trim: { value: "", label: "" } });
                }

                this.setState({
                    cars_shipped: car_models
                });
            }
        }
        else if (name == "other_services") {

            var other_services_val = this.state.other_services;
        }
        else if (name == "from_emirate" || name == "to_emirate") {
            let from_emirate = "";
            let to_emirate = "";
            if (name == "from_emirate") {
                from_emirate = value.value;
                to_emirate = this.state.to_emirate.value;
            }
            if (name == "to_emirate") {
                from_emirate = this.state.from_emirate.value;
                to_emirate = value.value;
            }
            var transport_fee = 0;
            if (from_emirate != "" && to_emirate != "") {
                transport_fee = this.getTransportationCharges(from_emirate, to_emirate);
            }

            this.setState({
                transportation_fees: transport_fee
            });
        }

    }
    handleInputChange(name, value) {

        if (name === 'from_address') {
            this.setState({
                [name]: value,
                pickupInvalid: !!value ? false : true,
            });
        }
        else if (name === 'to_address') {
            this.setState({
                [name]: value,
                dropInvalid: !!value ? false : true,
            });
        }
        else {
            this.setState({
                [name]: value
            });
        }
    }
    handleNoOfUnits(name, value) {
        this.setState({
            number_of_units: value
        });
    }
    carShippedSelectChange = (idx) => (name, value) => {

        var idx_val = parseInt(idx) + 1;

        var new_name = name.replace("_" + idx_val, "")

        const newCarShip = this.state.cars_shipped.map((car_shipped, sidx) => {
            if (idx !== sidx) return car_shipped;
            return { ...car_shipped, [new_name]: value };
        });


        this.setState({ cars_shipped: newCarShip });

        if (name.indexOf('_year') > -1) {
            this.loadCarDetails('year', value.label, idx);
        }

        if (name.indexOf('_make') > -1) {
            this.loadCarDetails('make', value.label, idx);
        }

        if (name.indexOf('_model') > -1) {
            this.loadCarDetails('model', value.label, idx);
        }
    }
    carShippingDetails() {
        const { years, make, modal, modal_number } = this.state;
        const year_opt = years;


        const make_opt = make;

        const modal_opt = modal;

        const modal_number_opt = modal_number;
        const count = this.state.cars_shipped.length;
        return (
            this.state.cars_shipped.map((car_shipped, idx) => (

                <section key={idx}>
                    <FormFieldsTitle title={`Describe your car ${count > 1 ? '(' + (idx + 1) + ')' : ''}`} />
                    <div className="row">
                        <div className="col-12 col-md-3 mb-3">
                            <FormFieldsTitle title="Model year:" className='h4' />
                            <SelectInput searchable={true} inputValue={car_shipped.field_car_year} 
                                name={"field_car_" + (idx + 1) + "_year"} label="Select model year" options={year_opt}
                                onInputChange={this.carShippedSelectChange(idx)} validationClasses="required" />
                        </div>
                        <div className="col-12 col-md-3 mb-3">
                            <FormFieldsTitle title="Car make:" className='h4' />
                            <SelectInput searchable={true} inputValue={car_shipped.field_car_make} 
                                name={"field_car_" + (idx + 1) + "_make"} label='Nothing selected' options={make_opt[idx]}
                                onInputChange={this.carShippedSelectChange(idx)} />
                        </div>
                        <div className="col-12 col-md-3 mb-3">
                            <FormFieldsTitle title="Car model:" className='h4' />
                            <SelectInput searchable={true} inputValue={car_shipped.field_car_model} 
                                name={"field_car_" + (idx + 1) + "_model"} label='Nothing selected' options={modal_opt[idx]}
                                onInputChange={this.carShippedSelectChange(idx)} />
                        </div>
                        <div className="col-12 col-md-3 mb-3">
                            <FormFieldsTitle title="Model number:" className='h4' />
                            <SelectInput searchable={true} 
                                inputValue={car_shipped.field_car_trim && car_shipped.field_car_trim.label != '' ? car_shipped.field_car_trim : 'Nothing selected'}
                                name={"field_car_" + (idx + 1) + "_trim"} label='Nothing selected' options={modal_number_opt[idx]}
                                onInputChange={this.carShippedSelectChange(idx)} />
                        </div>
                    </div>
                </section>
            ))
        )

    }
    carShippingSection() {
        var field_cars_number_opt = field_cars_number_opt_Constanst;

        var field_cars_number_value = this.state.field_cars_number;

        return (
            <section>
                <FormFieldsTitle title="How many cars do you want shipped?" />
                <CheckRadioBoxInput InputType="radio" name="field_cars_number" inputValue={field_cars_number_value} items={field_cars_number_opt} childClass="col-4 mb-3 d-flex" onInputChange={this.handleCustomChange} />
            </section>
        )
    }
    setPickupFocus(val) {
        this.setState({ pickupFocus: val });
    }
    setDropFocus(val) {
        this.setState({ dropFocus: val });
    }
    initPickup() {
        //console.log('initPickup');
    }
    onError = (status, clearSuggestions) => {
        console.log('Google Maps API returned error with status: ', status)
        clearSuggestions();
    }
    movingRequest() {
        let data_values = this.props.data_values;

        let service_constants = allServiceConstant;

        let move_type = move_type_Constanst;

        let moving_size = moving_size_Constanst;

        let moving_size_value = this.state.moving_size;

        let from_country_value = this.state.field_from;

        let from_address_value = this.state.from_address;

        let to_country_value = this.state.field_to;

        let to_address_value = this.state.to_address;

        let other_comments_text_value = this.state.other_comments_text;

        var typeOfJourney = this.props.typeOfJourney();

        var currentStepClass = "";

        if (typeOfJourney == 1) {
            currentStepClass = this.currentStep() === 1 ? '' : "d-none";
        } else {
            currentStepClass = this.currentStep() === 2 ? '' : "d-none";
        }

        const countryOptions = this.state.countries;

        let other_services_value = this.state.other_services;

        const other_services = [
            { id: allServiceConstant.SERVICE_BASIC_HANDYMAN_SERVICES_DRILLING_ON_THE_WALL_CURTAIN_HANGING_INTERNATIONAL_MOVE, value: "handyman", label: "Handyman" },
            { id: allServiceConstant.SERVICE_STORAGE_INTERNATIONAL_MOVE, value: "storage", label: "Storage" },
            { id: allServiceConstant.SERVICE_CAR_SHIPPING_WITH_MOVE_INTERNATIONAL_MOVE, value: "car-shipping-carrier", label: "Car shipping" }
        ];

        const idx = other_services_value.findIndex((a) => a.value === "car-shipping-carrier");
        const cap_shipping_section = idx > -1 ? this.carShippingSection() : "";
        const cap_to_be_shipped_section = idx > -1 ? this.carShippingDetails() : '';

        let selectedFromCountry = countryOptions.find(x => x.label === from_country_value);
        selectedFromCountry = !!selectedFromCountry ? selectedFromCountry : { code: 'AE' }

        let selectedToCountry = countryOptions.find(x => x.label == to_country_value);
        selectedToCountry = !!selectedToCountry ? selectedToCountry : { value: '', label: '', code: 'AE' }

        const fromCountryCode = getCountryCodeById(selectedFromCountry.value)
        const toCountryCode = getCountryCodeById(selectedToCountry.value)

        let invalidClassFrom = this.state.pickupInvalid ? 'btn-radio form-control p-3 border rounded border-danger invalidClassFrom' : 'btn-radio form-control p-3 border rounded invalidClassFrom';


        if (this.state.pickupInvalid) {
            let sectionP = document.getElementById('pickup');
            sectionP.classList.add('border-danger')
            let span = document.createElement("span");
            span.className = "error-msg";
            span.innerText = 'This field is required';

            if (!!sectionP) sectionP.parentNode.appendChild(span);
        }
        let invalidClassTo = this.state.dropInvalid ? 'btn-radio form-control p-3 border rounded border-danger invalidClassTo' : 'btn-radio form-control p-3 border rounded invalidClassTo';
        if (this.state.dropInvalid) {
            let sectionD = document.getElementById('dropoff');
            sectionD.classList.add('border-danger');
            let span = document.createElement("span");
            span.className = "error-msg";
            span.innerText = 'This field is required';

            if (!!sectionD) sectionD.parentNode.appendChild(span);
        }

        if (this.state.fromCountryInvalid) {
            let sectionP = document.getElementById('fromCountry');
            let span = document.createElement("span");
            span.className = "error-msg";
            span.innerText = 'This field is required';

            if (!!sectionP) sectionP.parentNode.appendChild(span);
        }

        if (this.state.toCountryInvalid) {
            let sectionP = document.getElementById('toCountry');
            let span = document.createElement("span");
            span.className = "error-msg";
            span.innerText = 'This field is required';

            if (!!sectionP) sectionP.parentNode.appendChild(span);
        }
        const pickupFocused = this.state.pickupFocus;
        const dropFocused = this.state.dropFocus;
        var now = moment(this.props.startDate);
        return (
            <div id="section-request-form" className={currentStepClass + " no-bottom-space"}>
                <section>
                    <FormFieldsTitle title="Please share the details about your international move to receive multiple quotations from relocation companies." titleClass="h5 mb-4" />
                </section>
                <section>
                    <FormFieldsTitle title="When do you need to move?" />
                    <div className="row mb-2">
                        <DatePick onInputChange={this.handleInputChange}
                            inputValue={this.state.move_date}
                            name="move_date"
                            startDate={now}
                            disableFriday={false}
                            validationClasses="required" />
                    </div>
                </section>
                <section>
                    <FormFieldsTitle title="Where are you moving from?" />
                    <div className="row mb-2">
                        <div className="col-12 col-md-6 mb-3">
                            <SelectInput name="field_from" id='fromCountry' inputValue={selectedFromCountry} label="Select country" searchable={true} options={countryOptions} onInputChange={this.handleCustomChange} validationClasses="required" />
                        </div>
                        <div className="col-12 col-md-6 mb-3">

                            <div className="floating-label ">
                                <PlacesAutocomplete
                                    value={from_address_value}
                                    onChange={value => this.handleInputChange('from_address', value)}
                                    onSelect={values => this.handleInputChange('from_address', values)}
                                    onError={() => this.onError}
                                    searchOptions={{ types: ['address'], componentRestrictions: { country: fromCountryCode } }}
                                    label={'Address'} googleCallbackName="initPickup"
                                >
                                    {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                                        <div>
                                            <div className={"floating-label autofill " + (from_address_value || pickupFocused ? ' active' : '')} >
                                                <div className="form-item form-type-textfield form-item-field-international-address-from"
                                                    onClick={() => this.setPickupFocus(true)} onBlur={() => this.setPickupFocus(false)}
                                                >
                                                    <input
                                                        {...getInputProps({
                                                            className: invalidClassFrom,
                                                            placeholder: 'Enter a location',
                                                            id: 'pickup',
                                                            autoComplete: 'off',
                                                            disabled: false
                                                        })}

                                                    /></div>
                                                <label for="">Address</label>
                                            </div>
                                            <div className={'pac-container pac-logo w-100' + (!!suggestions.length ? ' show' : '')}
                                                style={!!suggestions.length ? { display: 'block' } :
                                                    { display: 'none' }}>
                                                {loading && <div>Loading...</div>}
                                                {suggestions.map(suggestion => {
                                                    const className = 'pac-item-query pac-matched';
                                                    // inline style for demonstration purpose
                                                    const style = suggestion.active
                                                        ? { backgroundColor: '#fafafa', cursor: 'pointer' }
                                                        : { backgroundColor: '#ffffff', cursor: 'pointer' };
                                                    return (
                                                        <div className='pac-item' >
                                                            <div
                                                                {...getSuggestionItemProps(suggestion, {
                                                                    className
                                                                })}
                                                                style={{ textOverflow: 'ellipsis', overflow: 'hidden' }}
                                                            >
                                                                <span className='pac-icon pac-icon-marker' />
                                                                <span>{suggestion.description}</span>
                                                            </div>
                                                        </div>

                                                    );
                                                })}
                                            </div>
                                        </div>
                                    )}
                                </PlacesAutocomplete>
                            </div>
                        </div>
                    </div>
                </section>
                <section>
                    <FormFieldsTitle title="Where are you moving to?" />
                    <div className="row mb-2">
                        <div className="col-12 col-md-6 mb-3">
                            <SelectInput searchable={true} id='toCountry' name="field_to" inputValue={selectedToCountry} label="Select country" options={countryOptions} onInputChange={this.handleCustomChange} />
                        </div>
                        <div className="col-12 col-md-6 mb-3">
                            <div className="floating-label ">
                                <PlacesAutocomplete
                                    label="Select country"
                                    value={to_address_value}
                                    onError={() => this.onError}
                                    onChange={value => this.handleInputChange('to_address', value)}
                                    onSelect={values => this.handleInputChange('to_address', values)}
                                    searchOptions={{ types: ['address'], componentRestrictions: { country: toCountryCode } }}
                                    googleCallbackName="initDropoff"
                                >
                                    {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                                        <div>
                                            <div className={"floating-label autofill " + (to_address_value || dropFocused ? ' active' : '')} >
                                                <div className="form-item form-type-textfield form-item-field-international-address-from"
                                                    onClick={() => this.setDropFocus(true)} onBlur={() => this.setDropFocus(false)}>
                                                    <input
                                                        {...getInputProps({
                                                            placeholder: 'Enter a location',
                                                            className: invalidClassTo,
                                                            id: 'dropoff',
                                                            autoComplete: 'off',
                                                            disabled: false
                                                        })}


                                                    /></div>
                                                <label for="">Address</label>
                                            </div>
                                            <div className={'pac-container pac-logo w-100' + (!!suggestions.length ? ' show' : 'show')}
                                                style={!!suggestions.length ? { display: 'block' } : { display: 'none' }} >
                                                {loading && <div>Loading...</div>}
                                                {suggestions.map(suggestion => {
                                                    const className = 'pac-item-query pac-matched';

                                                    // inline style for demonstration purpose
                                                    const style = suggestion.active
                                                        ? { backgroundColor: '#fafafa', cursor: 'pointer' }
                                                        : { backgroundColor: '#ffffff', cursor: 'pointer' };
                                                    return (
                                                        <div className='pac-item' >
                                                            <div
                                                                {...getSuggestionItemProps(suggestion, {
                                                                    className
                                                                })}
                                                                style={{ textOverflow: 'ellipsis', overflow: 'hidden' }}
                                                            >
                                                                <span className='pac-icon pac-icon-marker' />
                                                                <span>{suggestion.description}</span>
                                                            </div>
                                                        </div>
                                                    );
                                                })}
                                            </div>
                                        </div>
                                    )}
                                </PlacesAutocomplete>
                            </div>
                        </div>
                    </div>
                </section>
                <section>
                    <FormFieldsTitle title="Moving size" />
                    <div className="row mb-2">
                        <div className="col-12 col-md-6 mb-3">
                            <SelectInput name="moving_size" inputValue={moving_size_value} label="Select size" options={moving_size} onInputChange={this.handleCustomChange} />
                        </div>
                    </div>
                </section>
                <section>
                    <FormFieldsTitle title="Do you need any of these services?" />
                    <CheckRadioBoxInput InputType="checkbox" name="other_services" inputValue={other_services_value} items={other_services} childClass="col-4 mb-3 d-flex" onInputChange={this.handleCustomChange} />
                </section>
                {cap_shipping_section}
                {cap_to_be_shipped_section}
                <section>
                    <FormFieldsTitle title="What else should we know? (Optional)" />
                    <div className="row mb-4">
                        <div className="col mb-3">
                            <TextareaInput inputValue={other_comments_text_value} name="other_comments_text" placeholder="If you have any other requirements, feel free to describe them here in as much detail as you want. Or just leave us a message here to call you if its easier to explain on the phone" onInputChange={this.handleCustomChange} />
                        </div>
                    </div>
                </section>
                <section>
                    <QuotesNextStep moveNext={this.moveNext} howToProceed={this.howToProceed} formCurrentStep={this.props.formCurrentStep} submitData={this.submitData} />
                </section>
            </div>
        )
    }
    selectCompanies() {
        var currentStepClass = this.currentStep() === 2 ? '' : "d-none";
        const serviceID = allServiceConstant.SERVICE_INTERNATIONAL_MOVE;
        const currentCity = this.props.current_city;
        const { selectedCompanies } = this.state;
        const {showSelectCompanyError} = this.props;

        return (
            <div className={currentStepClass}>
                <SelectCompanyWithFilters
                    serviceID={serviceID}
                    currentCity={currentCity}
                    selectChange={this.selectCompanyChange}
                    selectedCompanies={selectedCompanies}
                    showSelectCompanyError={showSelectCompanyError}
                    showOrHideModalSelectCompanyError={this.showOrHideModalSelectCompanyError}
                />
                <section>
                    <QuotesNextStep moveNext={this.moveNext} howToProceed={this.howToProceed} formCurrentStep={this.props.formCurrentStep} submitData={this.submitData} />
                </section>
            </div>
        )
    }

    showOrHideModalSelectCompanyError(boolVal) {
        const {showOrHideModalSelectCompanyError} = this.props;
        showOrHideModalSelectCompanyError(boolVal);
    }
    ContactDetails(isLocationDetailShow) {
        const countryOptions = [{ id: 1, value: "1", label: "Dubai" }];
        const currentStepClass = this.currentStep() === 3 ? '' : "d-none";
        const isLocationDetailDisplay = typeof isLocationDetailShow != "undefined" ? isLocationDetailShow : "yes";


        var contact_details = {
            input_email: this.state.input_email,
            input_phone: this.state.input_phone,
            input_name: this.state.input_name,
            input_last_name: this.state.input_last_name
        }
        let userProfile = this.props.userProfile;
        if (!this.state.loader) {

            return (
                <div id="personal-information-form" className={currentStepClass}>
                    <ContactDetailsStep
                        countryOptions={countryOptions}
                        userProfile={userProfile}
                        signInDetails={this.props.signInDetails}
                        isLocationDetailShow={isLocationDetailDisplay}
                        contact_details={contact_details}
                        handleDropDownChange={this.handleInputChange}
                        handleInputChange={this.handleInputChange}
                        mobileSummary={this.mobileSummary}
                        moveNext={this.moveNext}
                        formCurrentStep={this.props.formCurrentStep}
                        submitData={this.submitData}
                        isSkipCompany={this.props.isSkipCompany}
                        currentCity = {this.props.current_city}
                    />

                    <section>
                        <QuotesNextStep moveNext={this.moveNext} formCurrentStep={this.props.formCurrentStep} howToProceed={this.howToProceed} submitData={this.submitData} />
                    </section>
                </div>
            )
        } else {
            return (
                <main loader={this.state.loader ? "show" : "hide"}><Loader /></main>
            );
        }
    }

    render() {
        var package_opt = this.state.package_options;

        var transport_fee = this.state.transportation_fees;
        var isLoading = this.state.loader || this.props.loader;
        const { selectedCompanies } = this.state;
        const items = this.sumarryItems();
        const showPromo = false;
        const showPrice = false;
        var typeOfJourney = this.props.typeOfJourney();
        return (

            <React.Fragment>
                <div className="col-lg-8">
                    {
                        isLoading ? <main loader={isLoading ? "show" : "hide"}><Loader /></main> :
                            (<React.Fragment>
                                {this.movingRequest()}
                                {typeOfJourney == 1 && this.selectCompanies()}
                                {this.ContactDetails("no")}
                            </React.Fragment>)
                    }
                </div>
                <div className="col-md-3 ml-auto d-lg-block d-none">
                    <BookingSummary
                        typeOfFlow="quotes"
                        items={items}
                        showPromo={showPromo}
                        showPrice={showPrice}
                        selectChange={this.selectCompanyChange}
                        selectedCompanies={selectedCompanies}
                        formCurrentStep={this.props.formCurrentStep}
                        typeOfJourney={typeOfJourney} />
                </div>
            </React.Fragment>
        )
    }
}

export default QuotesIntlMovingPage;