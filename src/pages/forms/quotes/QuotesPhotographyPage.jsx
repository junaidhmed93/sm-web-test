import React from "react";
import FormFieldsTitle from "../../../components/FormFieldsTitle";
import CheckRadioBoxInput from "../../../components/Form_Fields/CheckRadioBoxInput";
import CheckRadioBoxInputWithPrice from "../../../components/Form_Fields/CheckRadioBoxInputWithPrice";
import DatePick from "../../../components/Form_Fields/DatePick";
import SelectInput from "../../../components/Form_Fields/SelectInput";
import TextFloatInput from "../../../components/Form_Fields/TextFloatInput";
import TextareaInput from "../../../components/Form_Fields/TextareaInput";
import PaymentMethods from "../../../components/Form_Fields/PaymentMethods";
import SubmitBooking from "../../../components/Form_Fields/SubmitBooking";
import BookingSummary from "../../../components/Form_Fields/BookingSummary";
import PackageBox from "../../../components/Form_Fields/PackageBox";
import BookNextStep from "../../../components/Form_Fields/BookNextStep";
import FormTitleDescription from "../../../components/FormTitleDescription";
import ContactDetailsStep from "../../../components/ContactDetailsStep";
import QuotesNextStep from "../../../components/Form_Fields/QuotesNextStep";
import SelectCompanyWithFilters from "../../../components/SelectCompanyWithFilters";
import { isValidSection, scrollToTop } from "../../../actions/index";
import LocationHelper from '../../../helpers/locationHelper';
import commonHelper from '../../../helpers/commonHelper';
import Loader from "../../../components/Loader";
import moment from 'moment';
import {
    registerCreditCard,
    postPhotographyRequest,

} from "../../../actions";
let allServiceConstant = {};
let URLConstant = {};
let current_currency = "AED";
let current_city = "dubai";
class QuotesPhotographyPage extends React.Component {

    constructor(props) {
        super(props);
        var quotesData = props.quotesData;
        var selectedCompanies = (typeof quotesData != "undefined" && typeof quotesData.selected_companies != "undefined") ?
            quotesData.selected_companies : [];
        var service = Object.keys(props.defaultServiceOption).length ? props.defaultServiceOption : "";
        this.state = {
            service: service,
            needed_services: "",
            dates_flexible: "",
            event_city: "",
            calender_date_of_service: '',
            other_comments_text: '',
            input_email: props.userProfile.email,
            input_phone: props.userProfile.address ? props.userProfile.address.phoneNumber : '',
            input_name: props.userProfile.customerFirstName,
            input_last_name: props.userProfile.customerLastName,
            selectedCompanies: selectedCompanies,
            loader: props.loader,
        }
        this.photographyRequest = this.photographyRequest.bind(this);
        this.ContactDetails = this.ContactDetails.bind(this);
        this.handleCustomChange = this.handleCustomChange.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.currentStep = this.currentStep.bind(this);
        this.moveNext = this.moveNext.bind(this);
        this.submitData = this.submitData.bind(this);
        this.selectCompanyChange = this.selectCompanyChange.bind(this);
        this.howToProceed = this.howToProceed.bind(this);
        this.showOrHideModalSelectCompanyError = this.showOrHideModalSelectCompanyError.bind(this);
        allServiceConstant = props.service_constants;
    }
    componentDidMount() {
        allServiceConstant = this.props.service_constants;
        URLConstant = this.props.url_constants;
        current_city = this.props.current_city;
        var defaultCity = LocationHelper.getLocationByName(current_city);


        var cityOptions = LocationHelper.getLocationByName(current_city);
        this.setState({
            event_city: cityOptions
        })

        var selectedCityVal = { id: defaultCity.id, value: commonHelper.slugify(defaultCity.name), label: defaultCity.name }

        this.setState({
            event_city: selectedCityVal,
            // service: allServiceConstant.SERVICE_WEDDING_PHOTOGRAPHY,
            // needed_services:{ id: 1, value: "PHOTOGRAPHY_AND_VIDEOGRAPHY", label: "Both photography & videography " }
        })
    }
    sumarryItems() {
        const { service, needed_services, dates_flexible, calender_date_of_service, event_city } = this.state;
        return [
            { label: 'Service type ', value: service.label },
            { label: 'Other services ', value: needed_services.label },
            { label: 'Flexible ', value: dates_flexible.label },
            { label: 'Date ', value: calender_date_of_service },
            { label: 'City ', value: event_city.label }
        ];
    }
    currentStep() {
        return this.props.formCurrentStep;
    }

    howToProceed(isSkipCompany) {

        const { howToProceed } = this.props;

        const selectedCompanies = isSkipCompany ? [] : this.state.selectedCompanies;

        this.setState({
            selectedCompanies: selectedCompanies
        });

        howToProceed(isSkipCompany, this.state);
    }

    currentStep() {
        return this.props.formCurrentStep;
    }
    moveNext(step) {
        const { moveNextStep } = this.props;
        moveNextStep(step, this.state);
    }
    submitData() {
        const { submitLeadData } = this.props;
        submitLeadData(this.state);
    }

    selectCompanyChange(value) {
        this.setState({
            selectedCompanies: value
        });
    }
    handleCustomChange(name, value) {
        this.setState({
            [name]: value
        });
    }
    handleInputChange(name, value) {
        this.setState({
            [name]: value
        });
    }
    photographyRequest() {
        let allCities = LocationHelper.fetchCities();

        let cityOptions = [];

        allCities.map(function (item) {
            let value = commonHelper.slugify(item.label);
            cityOptions.push({ id: item.id, value: value, label: item.label });
        })
        let service_value = this.state.service;

        let needed_services_value = this.state.needed_services;

        let dates_flexible_value = this.state.dates_flexible;

        let other_comments_text_value = this.state.other_comments_text;

        let calender_date_of_service_value = this.state.calender_date_of_service;

        let event_city_value = this.state.event_city;

        let photography_options = [
            { id: 1, value: allServiceConstant.SERVICE_WEDDING_PHOTOGRAPHY, label: "Wedding photography" },
            { id: 2, value: allServiceConstant.SERVICE_NEW_BORN__CHILDREN_PHOTOGRAPHY, label: "New born & children photography" },
            { id: 3, value: allServiceConstant.SERVICE_COMMERCIAL_PHOTOGRAPHY, label: "Commercial photography" },
            { id: 4, value: allServiceConstant.SERVICE_EVENT_PHOTOGRAPHY, label: "Event photography" },
            { id: 5, value: allServiceConstant.SERVICE_MATERNITY_PORTRAITS, label: "Maternity portraits" },
            { id: 6, value: allServiceConstant.SERVICE_PORTRAIT_PHOTOGRAPHY, label: "Portrait photography" },
            { id: 7, value: allServiceConstant.SERVICE_PROFESSIONAL_HEAD_SHOTS, label: "Professional headshots" },
            { id: 8, value: allServiceConstant.SERVICE_OTHER_PHOTOGRAPHY, label: "Other photography" }
        ];

        let needed_services_options = [
            { id: 1, value: "PHOTOGRAPHY_AND_VIDEOGRAPHY", label: "Both photography & videography " },
            { id: 2, value: "PHOTOGRAPHY", label: "Only photography" },
            { id: 3, value: "VIDEOGRAPHY", label: "Only videography " }
        ];

        let dates_flexible_options = [
            { id: 1, value: "true", label: "Yes" },
            { id: 2, value: "false", label: "No" }
        ];

        //let cityOptions = [{ id: 1, value: "dubai", label: "Dubai" }, { id: 2, value: "abu-dhabi", label: "Abu Dhabi" }, { id: 3, value: "sharjah", label: "Sharjah" }];

        let hours_needed_value = this.state.hours_needed;

        var typeOfJourney = this.props.typeOfJourney();

        var currentStepClass = "";

        if (typeOfJourney == 1) {
            currentStepClass = this.currentStep() === 1 ? '' : "d-none";
        } else {
            currentStepClass = this.currentStep() === 2 ? '' : "d-none";
        }

        return (
            <div id="section-request-form" className={currentStepClass + " no-bottom-space"}>
                <section style={{ textAlign: 'center' }}>
                    <FormFieldsTitle title="Please fill out the form below so we can find the perfect photographer for you." />
                </section>
                <section>
                    <FormFieldsTitle title="What type of photography service are you looking for?"  />
                    <CheckRadioBoxInput
                        InputType="radio" name="service"
                        inputValue={service_value}
                        items={photography_options}
                        childClass="col-6 col-md-4 mb-3 d-flex ceilings-painted "
                        onInputChange={this.handleCustomChange}
                        validationClasses="radio-required"
                        validationMessage="Please select service"
                    />
                </section>
                <section>
                    <FormFieldsTitle title="Which of the following services do you need?" />
                    <CheckRadioBoxInput
                        InputType="radio"
                        name="needed_services"
                        inputValue={needed_services_value} items={needed_services_options}
                        childClass="col-6 col-md-4 mb-3 d-flex ceilings-painted"
                        onInputChange={this.handleCustomChange}
                        validationClasses="radio-required"
                        validationMessage="Please select photography service"
                    />
                </section>

                <section>
                    <div className="row mb-2">
                        <DatePick name="calender_date_of_service"
                            inputValue={calender_date_of_service_value}
                            title="When do you need the service?"
                            onInputChange={this.handleInputChange}
                            validationClasses="required"
                            placeholderText="Select a date"
                            disableFriday={false} />

                        <div className="col-12 col-md-6 mb-3">
                            <FormFieldsTitle title="Where do you need the service?" />
                            <SelectInput name="event_city" inputValue={event_city_value} label="City" options={cityOptions} onInputChange={this.handleCustomChange} />
                        </div>
                    </div>
                </section>
                <section>
                    <FormFieldsTitle title="My dates are flexible ( +/- 3 days)?" />
                    <CheckRadioBoxInput 
                        InputType="radio" 
                        name="dates_flexible" 
                        inputValue={dates_flexible_value} 
                        items={dates_flexible_options} 
                        parentClass="row mb-1" 
                        childClass="col-6 col-md-3 mb-3 d-flex" 
                        onInputChange={this.handleCustomChange}
                        validationClasses="radio-required"
                        validationMessage="Please select is dates are flexible" />
                </section>
                <section>
                    <FormFieldsTitle title="Is there anything else you'd like to add?" />
                    <div className="row mb-4">
                        <div className="col mb-3">
                            <TextareaInput inputValue={other_comments_text_value} name="other_comments_text" placeholder="Please tell us more about the type of photography service you are looking for, the number of people you'd like photographed or the size of the event." onInputChange={this.handleCustomChange} />
                        </div>
                    </div>
                </section>
                <section>
                    <QuotesNextStep moveNext={this.moveNext} howToProceed={this.howToProceed} formCurrentStep={this.props.formCurrentStep} submitData={this.submitData} />
                </section>
            </div>
        )
    }
    showOrHideModalSelectCompanyError(boolVal) {
        const { showOrHideModalSelectCompanyError } = this.props;
        showOrHideModalSelectCompanyError(boolVal);
    }
    selectCompanies() {
        let currentStepClass = this.currentStep() === 2 ? '' : "d-none";
        const serviceID = allServiceConstant.SERVICE_PHOTOGRAPHY;
        const currentCity = this.props.current_city;
        const { selectedCompanies } = this.state;
        const { showSelectCompanyError } = this.props;

        return (
            <div className={currentStepClass}>
                <SelectCompanyWithFilters
                    serviceID={serviceID}
                    currentCity={currentCity}
                    selectChange={this.selectCompanyChange}
                    selectedCompanies={selectedCompanies}
                    showSelectCompanyError={showSelectCompanyError}
                    showOrHideModalSelectCompanyError={this.showOrHideModalSelectCompanyError} />/>
                <section>
                    <QuotesNextStep moveNext={this.moveNext} howToProceed={this.howToProceed} formCurrentStep={this.props.formCurrentStep} submitData={this.submitData} />
                </section>
            </div>
        )
    }
    ContactDetails(isLocationDetailShow) {
        const countryOptions = [{ id: 1, value: "1", label: "Dubai" }];
        const currentStepClass = this.currentStep() === 3 ? '' : "d-none";

        const isLocationDetailDisplay = typeof isLocationDetailShow != "undefined" ? isLocationDetailShow : "yes";


        const contact_details = {
            input_email: this.state.input_email,
            input_phone: this.state.input_phone,
            input_name: this.state.input_name,
            input_last_name: this.state.input_last_name
        }
        let userProfile = this.props.userProfile;
        if (!this.state.loader) {
            return (
                <div id="personal-information-form" className={currentStepClass}>
                    <ContactDetailsStep
                        countryOptions={countryOptions}
                        userProfile={userProfile}
                        signInDetails={this.props.signInDetails}
                        isLocationDetailShow={isLocationDetailDisplay}
                        contact_details={contact_details}
                        handleDropDownChange={this.handleInputChange}
                        handleInputChange={this.handleInputChange}
                        mobileSummary={this.mobileSummary}
                        moveNext={this.moveNext}
                        formCurrentStep={this.props.formCurrentStep}
                        submitData={this.submitData}
                        isSkipCompany={this.props.isSkipCompany} 
                        currentCity = {this.props.current_city}/>
                    <section>
                        <QuotesNextStep moveNext={this.moveNext} howToProceed={this.howToProceed} formCurrentStep={this.props.formCurrentStep} submitData={this.submitData} isSkipCompany={this.props.isSkipCompany} />
                    </section>
                </div>
            )
        } else {
            return (
                <main loader={this.state.loader ? "show" : "hide"}><Loader /></main>
            );
        }
    }

    render() {
        const { selectedCompanies } = this.state;
        const items = this.sumarryItems();
        var isLoading = this.state.loader || this.props.loader;
        const showPromo = false;
        const showPrice = false;
        var typeOfJourney = this.props.typeOfJourney();
        return (
            <React.Fragment>
                <div className="col-lg-8">
                    {
                        isLoading ? <main loader={isLoading ? "show" : "hide"}><Loader /></main> :
                            (<React.Fragment>
                                {this.photographyRequest()}
                                {typeOfJourney == 1 && this.selectCompanies()}
                                {this.ContactDetails("no")}
                            </React.Fragment>)
                    }
                </div>
                <div className="col-md-3 ml-auto d-lg-block d-none">
                    <BookingSummary
                        typeOfFlow="quotes"
                        items={items}
                        showPromo={showPromo}
                        showPrice={showPrice}
                        selectedCompanies={selectedCompanies}
                        formCurrentStep={this.props.formCurrentStep}
                        selectChange={this.selectCompanyChange}
                        typeOfJourney={typeOfJourney} />
                </div>
            </React.Fragment>
        )
    }
}

export default QuotesPhotographyPage;