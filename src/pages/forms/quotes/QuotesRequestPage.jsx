import React from "react";
import FormFieldsTitle from "../../../components/FormFieldsTitle";
import CheckRadioBoxInput from "../../../components/Form_Fields/CheckRadioBoxInput";
import SelectInput from "../../../components/Form_Fields/SelectInput";
import DatePick from "../../../components/Form_Fields/DatePick";
import TextareaInput from "../../../components/Form_Fields/TextareaInput";
import SubmitBooking from "../../../components/Form_Fields/SubmitBooking";
import BookingSummary from "../../../components/Form_Fields/BookingSummary";
import QuotesNextStep from "../../../components/Form_Fields/QuotesNextStep";
import FormTitleDescription from "../../../components/FormTitleDescription";
import ContactDetailsStep from "../../../components/ContactDetailsStep";
import SelectCompanyWithFilters from "../../../components/SelectCompanyWithFilters";
import {isValidSection, scrollToTop, formTitle} from "../../../actions/index";
import locationHelper from "../../../helpers/locationHelper";
import commonHelper from "../../../helpers/commonHelper";
import Loader from "../../../components/Loader";
import {connect} from "react-redux";
import moment from 'moment';
let allServiceConstant = {};
let URLConstant = {};
let dataValues = {};
let current_currency = "AED";
let current_city = "dubai";
var hompPaintingOptions = [];
class QuotesRequestPage extends React.Component{

    constructor(props) {
        super(props);
        var quotesData = props.quotesData;
        var selectedCompanies = (typeof quotesData != "undefined" && typeof quotesData.selected_companies != "undefined") ? 
        quotesData.selected_companies : [];
        this.locationHelperInit = locationHelper.init.bind(this);
        this.locationHelperInit();
        this.state = {
            service: props.defaultServiceOption,
            homeType: {value: '', label: ''},
            homeSize: {value: '', label: ''},
            numberOfUnits: "",
            numberOfUnitsOptions:[],
            dateOfService: '',
            details:"",
            input_email: props.userProfile.email,
            input_phone: props.userProfile.address ? props.userProfile.address.phoneNumber : '',
            input_name: props.userProfile.customerFirstName,
            input_last_name: props.userProfile.customerLastName,
            input_address_city:'',
            input_address_area: props.setUserArea,
            input_address_area_building_name: props.userProfile.address ? props.userProfile.address.building : '',
            input_address_area_building_apartment: props.userProfile.address ? props.userProfile.address.apartment : '',
            selectedCompanies: selectedCompanies,
            loader: false,
            isPainting: props.isPainting,
            formTitle: '',
        }
        this.serviceRequest = this.serviceRequest.bind(this);
        this.ContactDetails = this.ContactDetails.bind(this);
        this.handleCustomChange = this.handleCustomChange.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.currentStep = this.currentStep.bind(this);
        this.moveNext = this.moveNext.bind(this);
        this.submitData = this.submitData.bind(this);
        this.howToProceed = this.howToProceed.bind(this);
        this.submitData = this.submitData.bind(this);
        this.selectCompanyChange = this.selectCompanyChange.bind(this);
        this.handleNoOfUnits = this.handleNoOfUnits.bind(this);
        this.showOrHideModalSelectCompanyError = this.showOrHideModalSelectCompanyError.bind(this);
        this.numberOfUnitsOptions = this.numberOfUnitsOptions.bind(this);
        allServiceConstant = props.service_constants;

    }
    componentDidMount() {
        allServiceConstant = this.props.service_constants;
        URLConstant =  this.props.url_constants;
        current_city = this.props.current_city;
        dataValues = this.props.dataValues;

        var typeofHomeDataValues = commonHelper.processDataValues( this.props.dataConstants["TYPE_OF_HOME_TO_PAINT"] );

        var apartmentUnitsDataValues = commonHelper.processDataValues(this.props.dataConstants["SIZE_OF_APARTMENT"]);

        var villaUnitsDataValues = commonHelper.processDataValues(this.props.dataConstants["SIZE_OF_VILLA"]);

        var allDataValues = {};

        allDataValues = Object.assign(
            typeofHomeDataValues,
            apartmentUnitsDataValues,
            villaUnitsDataValues,
            allDataValues
        );

        dataValues = allDataValues;

        hompPaintingOptions = [
            allServiceConstant.SERVICE_HOME_PAINTING,
            allServiceConstant.SERVICE_EXTERIOR_PAINTING
        ];
        var cityOptions = locationHelper.getLocationByName(current_city);
        this.setState({
            input_address_city: cityOptions
        });
        if(this.props.isPainting) {
            this.setState({
                homeType: ""
            });
            this.numberOfUnitsOptions();
        }
        
        //formTitle
    }
    sumarryItems(){
        const {service, homeSize, homeType, numberOfUnits, dateOfService} = this.state;
        //{label: 'Service', value: service.label},
        var items = [];
        var isPainting = this.props.isPainting;
        if(!isPainting){
            var serviceLabel = service.label;

            items =  [
                {label: locationHelper.translate("RECEIVE_OFFERS_FOR"), value: service.label},
                {label: locationHelper.translate("YOUR_HOME"), value: homeSize.label},
                {label: locationHelper.translate("DATE"), value: dateOfService}
            ];
        }else{
            var typeOfPainting = service;

            if(hompPaintingOptions.includes(typeOfPainting.value)) {
                items = [
                    {label: locationHelper.translate("TYPE_OF_PAINTING"), value: typeOfPainting.label},
                    {label: locationHelper.translate('HOME_TYPE'), value: homeType.label},
                    {label: locationHelper.translate('UNITS'), value: numberOfUnits.label},
                    {label: locationHelper.translate("DATE"), value: dateOfService},
                ];
            }else{
                items = [
                    {label: locationHelper.translate('TYPE_OF_PAINTING'), value: typeOfPainting.label},
                    {label: locationHelper.translate("DATE"), value: dateOfService},
                ];
            }

        }

        return items;

    }
    currentStep(){
        return this.props.formCurrentStep;
    }
    moveNext(step){
        const {moveNextStep} = this.props;
        moveNextStep(step, this.state);
    }
    showOrHideModalSelectCompanyError(boolVal){
        const {showOrHideModalSelectCompanyError} = this.props;
        showOrHideModalSelectCompanyError(boolVal);
    }
    howToProceed(isSkipCompany){
        const {howToProceed} = this.props;

        var selectedCompanies = isSkipCompany ? [] : this.state.selectedCompanies;

        this.setState({
            selectedCompanies: selectedCompanies
        });

        howToProceed(isSkipCompany, this.state);
    }
    submitData(){
        const {submitLeadData} = this.props;
        submitLeadData(this.state);
    }
    selectCompanyChange(value){
        this.setState({
            selectedCompanies: value
        });
    }
    handleCustomChange(name, value){
        this.setState({
            [name]: value
        });
    }
    handleInputChange(name, value) {
        this.setState({
            [name]: value
        });
    }
    handleNoOfUnits(name, value){
        this.setState({
            numberOfUnits: value
        });
    }
    numberOfUnitsOptions(){

        var {homeType} = this.state;

        var numberOfUnitsApartment = [
            {id: 1, value: dataValues.DATA_VALUE_STUDIO_APARTMENT, label: "Studio", price:650, current_currency: current_currency, constant: 'STUDIO_APARTMENT'},
            {id: 2, value: dataValues.DATA_VALUE_1_BR_APARTMENT, label: "1 BR", price:900, current_currency: current_currency, constant: '1_BR_APARTMENT'},
            {id: 3, value: dataValues.DATA_VALUE_2_BR_APARTMENT, label: "2 BR", price:1100, current_currency: current_currency, constant: '2_BR_APARTMENT'},
            {id: 4, value: dataValues.DATA_VALUE_3_BR_APARTMENT, label: "3 BR", price:1450, current_currency: current_currency, constant: '3_BR_APARTMENT'},
            {id: 5, value: dataValues.DATA_VALUE_4_BR_APARTMENT, label: "4 BR", price:2000, current_currency: current_currency, constant: '4_BR_APARTMENT'},
            {id: 6, value: dataValues.DATA_VALUE_5_BR_APARTMENT, label: "5 BR", price:2650, current_currency: current_currency, constant: '5_BR_APARTMENT'}
        ];

        var numberOfUnitsVilla = [
            {id: 1, value: dataValues.DATA_VALUE_1_BR_VILLA, label: "1 BR", price:1650, current_currency: current_currency, constant: '1_BR_VILLA'},
            {id: 2, value: dataValues.DATA_VALUE_2_BR_VILLA, label: "2 BR", price:2250, current_currency: current_currency, constant: '2_BR_VILLA'},
            {id: 3, value: dataValues.DATA_VALUE_3_BR_VILLA, label: "3 BR", price:2800, current_currency: current_currency, constant: '3_BR_VILLA'},
            {id: 4, value: dataValues.DATA_VALUE_4_BR_VILLA, label: "4 BR", price:3350, current_currency: current_currency, constant: '4_BR_VILLA'},
            {id: 5, value: dataValues.DATA_VALUE_5_BR_VILLA, label: "5 BR", price:4050, current_currency: current_currency, constant: '5_BR_VILLA'}
        ];

        var homeUnitOptions = [];

        if( typeof homeType.value != "undefined"){
            homeUnitOptions = homeType.value == dataValues.DATA_VALUE_HOME_TYPE_APARTMENT ? numberOfUnitsApartment : numberOfUnitsVilla;
        }else{
            homeUnitOptions = numberOfUnitsApartment;
        }

        this.setState({
            numberOfUnitsOptions : homeUnitOptions,
            numberOfUnits: ""
        });
    }
    serviceRequest(){

        var {isPainting, url_params, lang} = this.props;

        var service_value = this.state.service;

        var details_value = this.state.details;

        var dateOfServiceValue = this.state.dateOfService;

        var homeSizeValue = this.state.homeSize;

        var number_of_units_value = this.state.number_of_units;

        var number_of_units_options = this.state.painting_room_options;

        var servicesOptions = this.props.servicesOptions; //commonHelper.lookupServices('parent_service_id', allServiceConstant.SERVICE_MAINTENANCE);

        var service_options = servicesOptions.length ? servicesOptions : [];

        var typeOfJourney = this.props.typeOfJourney();

        var currentStepClass = "";
        
        if( typeOfJourney == 1 ){
            currentStepClass = this.currentStep() === 1 ? '' : "d-none";
        }else{
            currentStepClass = this.currentStep() === 2 ? '' : "d-none";
        }

        var homeOptionsEle = "";

        if(!isPainting) {

            var homeSizeOptions = [
                {id: 9, value: "studio", label: locationHelper.translate("STUDIO")},
                {id: 1, value: "1 Br", label: locationHelper.translate("BR_1")},
                {id: 2, value: "2 Br", label: locationHelper.translate("BR_2")},
                {id: 3, value: "3 Br", label: locationHelper.translate("BR_3")},
                {id: 4, value: "4 Br", label: locationHelper.translate("BR_4")},
                {id: 5, value: "2 Br h/v", label: locationHelper.translate("BR_2_VILLA")},
                {id: 6, value: "3 Br h/v", label: locationHelper.translate("BR_3_VILLA")},
                {id: 7, value: "4 Br h/v", label: locationHelper.translate("BR_4_VILLA")},
                {id: 8, value: "5 Br h/v", label: locationHelper.translate("BR_5_VILLA")},
                {id: 10, value: "other", label: locationHelper.translate("OTHER")},
                {id: 11, value: "office", label: locationHelper.translate("OFFICE")}
            ];

            homeOptionsEle =
                <section>
                    <FormFieldsTitle title={locationHelper.translate("DESCRIBE_YOUR_HOME")} />
                <div className="row mb-2">
                    <div className="col-12 col-md-6 mb-3">
                        <SelectInput name="homeSize"
                                     inputValue={homeSizeValue}
                                     label={locationHelper.translate("SELECT")}
                                     options={homeSizeOptions}
                                     onInputChange={this.handleCustomChange}/>
                    </div>
                </div>
                </section>;
        }else{


            var homeType = [
                {id: 2, value: dataValues.DATA_VALUE_HOME_TYPE_VILLA, label: "Villa", constant: 'HOME_TYPE_VILLA'},
                {id: 1, value: dataValues.DATA_VALUE_HOME_TYPE_APARTMENT, label: "Apartment", constant: 'HOME_TYPE_APARTMENT'}
            ];
            var homeTypeValue = this.state.homeType;

            var numberOfUnitsItem = typeof homeTypeValue.value != "";

            var numberOfUnitsValue = this.state.numberOfUnits;

            var typeOfPainting = this.state.service;

            if(hompPaintingOptions.includes(typeOfPainting.value)) {
                homeOptionsEle = <section>
                    <FormFieldsTitle title={locationHelper.translate("DESCRIBE_YOUR_HOME")} />
                    <CheckRadioBoxInput
                        InputType="radio"
                        name="homeType"
                        inputValue={homeTypeValue}
                        items={homeType}
                        onInputChange={this.handleInputChange}
                        parentClass="row mb-2"
                        childClass="col-12 col-md-4 mb-2 d-flex"
                        validationClasses="radio-required"
                        validationMessage={locationHelper.translate("PLEASE_DESCRIBE_YOUR_HOME_TYPE")} 
                    />
                    <CheckRadioBoxInput
                        InputType="radio"
                        name="numberOfUnitsApartment"
                        inputValue={numberOfUnitsValue}
                        items={this.state.numberOfUnitsOptions}
                        onInputChange={this.handleNoOfUnits}
                        childClass="col-6 col-sm-2 mb-3 d-flex"
                        validationClasses="radio-required"
                        validationMessage={locationHelper.translate("PLEASE_SELECT_YOUR_HOME_SIZE")}
                    />
                </section>
            }
        }
        
        var now = moment(this.props.startDate);

       // console.log("formTitle", url_params.slug, lang);

        return (
            <div id="section-request-form" className={currentStepClass+" no-bottom-space"}>
                <section>
                <FormFieldsTitle title={formTitle(url_params.slug, lang)} titleClass="h5 mb-4" />
                    <div className="row mb-2">
                        <div className="col-12 col-md-6 mb-3">
                            <FormFieldsTitle title={ isPainting ? locationHelper.translate("WHAT_TYPE_OF_PAINTING_SERVICE_DO_YOU_NEED") : locationHelper.translate("I_WANT_TO_RECEIVE_OFFERS_FOR")}  />
                            <SelectInput 
                                name="service" 
                                inputValue={service_value} 
                                label={locationHelper.translate("SELECT_SERVICE_TLE")}
                                options={service_options} 
                                onInputChange={this.handleCustomChange} 
                                validationClasses="required"/>
                        </div>
                        { !isPainting && <DatePick 
                            selected={this.state.dateOfService}
                            inputValue={this.state.dateOfService}
                            name="dateOfService" 
                            onInputChange={this.handleInputChange} 
                            validationClasses="required"
                            title ={locationHelper.translate("WHEN_DO_YOU_NEED_THE_SERVICE")}
                            disableFriday = {false}
                            placeholderText={locationHelper.translate("SELECT_DATE")}
                            startDate={now}/>}
                    </div>
                </section>
                <section>
                    {homeOptionsEle}
                </section>
                { isPainting && <section>
                    <FormFieldsTitle title={locationHelper.translate("WHEN_DO_YOU_WANT_THE_JOB_TO_BE_DONE")} />
                    <div className="row mb-2">
                        <DatePick 
                        selectedDate={this.state.dateOfService}
                        inputValue={this.state.dateOfService}
                        name="dateOfService" 
                        onInputChange={this.handleInputChange} 
                        validationClasses="required"
                        disableFriday = {false}/>
                    </div>
                </section>}
                <section>
                    <FormFieldsTitle title={ !isPainting ? locationHelper.translate("PLEASE_DESCRIBE_THE_JOB_IN_AS_MUCH_DETAIL_AS_POSSIBLE") : locationHelper.translate("DESCRIBE_YOUR_PAINTING_JOB")}/>
                    <div className="row mb-4">
                        <div className="col mb-3">
                            <TextareaInput
                                name="details"
                                inputValue={this.state.details}
                                onInputChange={this.handleCustomChange}
                                validationClasses="required"
                                validationMessage = {locationHelper.translate("PLEASE_PROVIDE_DETAILS_OF_THE_JOB")}
                                placeholder={locationHelper.translate("HOUSE_HOLD_SERVICE_TEXTAREA")}  />
                        </div>
                    </div>
                </section>
                <section>
                    <QuotesNextStep
                        moveNext={this.moveNext}
                        formCurrentStep={this.props.formCurrentStep}
                        howToProceed={this.howToProceed}
                        submitData={this.submitData} />
                </section>

            </div>
        )
    }
    selectCompanies(){
        var currentStepClass = this.currentStep() === 2 ? '' : "d-none";

        var {service} = this.state;

        const serviceID = this.props.parnetServiceId;

        const filterServiceID = service.value;

        const currentCity = this.props.current_city;

        const {selectedCompanies} = this.state;

        const {showSelectCompanyError} = this.props;

        if( typeof(serviceID) != 'undefined' ){
            return (
                <div id="section-company-selector" className={currentStepClass}>
                    <SelectCompanyWithFilters
                        filterService = {filterServiceID}
                        serviceID={serviceID}
                        currentCity={currentCity}
                        selectChange={this.selectCompanyChange}
                        selectedCompanies={selectedCompanies}
                        showSelectCompanyError={showSelectCompanyError}
                        showOrHideModalSelectCompanyError={this.showOrHideModalSelectCompanyError}  />
                    <section>
                        <QuotesNextStep moveNext={this.moveNext} formCurrentStep={this.props.formCurrentStep}  submitData={this.submitData} />
                    </section>
                </div>
            )
        }
    }
    ContactDetails(){

        var cityOptions = locationHelper.getLocationByName(current_city);

        var currentStepClass = this.currentStep() === 3 ? '' : "d-none";
        //var currentStepClass =  ''
        var isLocationDetailShow = "yes";

        var contact_details = {
            input_email:this.state.input_email,
            input_phone:this.state.input_phone,
            input_name:this.state.input_name,
            input_last_name:this.state.input_last_name
        }

        if(isLocationDetailShow){
            contact_details.input_address_city = this.state.input_address_city != "" ? this.state.input_address_city : cityOptions;
            contact_details.input_address_area = this.state.input_address_area;
            contact_details.input_address_area_building_name = this.state.input_address_area_building_name;
            contact_details.input_address_area_building_apartment = this.state.input_address_area_building_apartment;
        }

        var userProfile =  this.props.userProfile;

        if(!this.state.loader) {
            return (
                <div id="personal-information-form" className={currentStepClass}>
                    <ContactDetailsStep
                        cityOptions={cityOptions}
                        userProfile={userProfile}
                        signInDetails={this.props.signInDetails}
                        isLocationDetailShow={isLocationDetailShow}
                        contact_details={contact_details}
                        handleDropDownChange={this.handleInputChange}
                        handleInputChange={this.handleInputChange}
                        mobileSummary={this.mobileSummary}
                        moveNext={this.moveNext}
                        formCurrentStep={this.props.formCurrentStep}
                        submitData={this.submitData}
                        isSkipCompany = {this.props.isSkipCompany}
                        currentCity = {this.props.current_city}
                    />
                </div>
            )
        }else {
            return (
                <main loader={this.state.loader ? "show" : "hide"}><Loader/></main>
            );
        }
    }
    componentDidUpdate(prevProps, prevState) {
        var {homeType} = this.state;

        if(prevState.homeType != homeType) {

            this.numberOfUnitsOptions();
        }
    }
    componentWillReceiveProps (newProps) {
        if ((newProps.dataConstants !== this.props.dataConstants) && this.props.isPainting) {
            dataValues = newProps.dataConstants;
            var typeofHomeDataValues = commonHelper.processDataValues( newProps.dataConstants["TYPE_OF_HOME_TO_PAINT"] );

            var apartmentUnitsDataValues = commonHelper.processDataValues(newProps.dataConstants["SIZE_OF_APARTMENT"]);

            var villaUnitsDataValues = commonHelper.processDataValues(newProps.dataConstants["SIZE_OF_VILLA"]);

            var allDataValues = {};

            allDataValues = Object.assign(
                typeofHomeDataValues,
                apartmentUnitsDataValues,
                villaUnitsDataValues,
                allDataValues
            );

            dataValues = allDataValues;

            this.numberOfUnitsOptions();
        }
        if (newProps.setUserArea !== this.props.setUserArea) {
            this.setState({
                input_address_area: newProps.setUserArea
            })
        }
    }
    render(){
        const {selectedCompanies} = this.state;
        const items = this.sumarryItems();
        const showPromo = false;
        const showPrice = false;

        var isLoading =  this.state.loader || this.props.loader;

        var typeOfJourney = this.props.typeOfJourney(); 

        var bookingSummary = <BookingSummary 
        typeOfFlow="quotes" 
        items={items} 
        showPromo={showPromo} 
        showPrice={showPrice}
        selectChange={this.selectCompanyChange}
        selectedCompanies={selectedCompanies} 
        formCurrentStep = {this.props.formCurrentStep}
        typeOfJourney = {typeOfJourney}
        />
        
        //formTitle
        if(isLoading) {
            return (
                <React.Fragment>
                    <div className="col-lg-8">
                        <main loader={ isLoading ? "show" : "hide"}><Loader/></main>
                    </div>
                    <div className="col-md-3 ml-auto">
                        {bookingSummary}
                    </div>
                </React.Fragment>
            );

        }else {
            return (
                <React.Fragment>
                    <div className="col-lg-8">
                        {this.serviceRequest()}
                        { typeOfJourney == 1 && this.selectCompanies()}
                        {this.ContactDetails()}
                    </div>
                    <div className="col-md-3 ml-auto d-lg-block d-none">
                        {bookingSummary}
                    </div>
                </React.Fragment>
            )
        }
    }
}
function mapStateToProps(state){
    return {
        currentCity: state.currentCity,
        lang: state.lang,
        dataConstants: state.dataConstants,
        newDataConstants: state.newDataConstants,
    }
}
export default connect(mapStateToProps)(QuotesRequestPage);