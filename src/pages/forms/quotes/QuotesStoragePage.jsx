import React from "react";
import FormFieldsTitle from "../../../components/FormFieldsTitle";
import CheckRadioBoxInput from "../../../components/Form_Fields/CheckRadioBoxInput";
import CheckRadioBoxInputWithPrice from "../../../components/Form_Fields/CheckRadioBoxInputWithPrice";
import DatePick from "../../../components/Form_Fields/DatePick";
import SelectInput from "../../../components/Form_Fields/SelectInput";
import TextFloatInput from "../../../components/Form_Fields/TextFloatInput";
import TextareaInput from "../../../components/Form_Fields/TextareaInput";
import PaymentMethods from "../../../components/Form_Fields/PaymentMethods";
import SubmitBooking from "../../../components/Form_Fields/SubmitBooking";
import BookingSummary from "../../../components/Form_Fields/BookingSummary";
import PackageBox from "../../../components/Form_Fields/PackageBox";
import BookNextStep from "../../../components/Form_Fields/BookNextStep";
import FormTitleDescription from "../../../components/FormTitleDescription";
import ContactDetailsStep from "../../../components/ContactDetailsStep";
import QuotesNextStep from "../../../components/Form_Fields/QuotesNextStep";
import SelectCompanyWithFilters from "../../../components/SelectCompanyWithFilters";
import { isValidSection, scrollToTop } from "../../../actions/index";
import LocationHelper from '../../../helpers/locationHelper';
import commonHelper from '../../../helpers/commonHelper';
import Loader from "../../../components/Loader";
import moment from 'moment';
import ReactTooltip from 'react-tooltip'
import {
    registerCreditCard,
    postRequest,
    SERVICE_COMMERCIAL_STORAGE,
    SERVICE_PERSONAL_STORAGE

} from "../../../actions";
let allServiceConstant = {};
let URLConstant = {};
let current_currency = "AED";
let current_city = "dubai";

class QuotesStoragePage extends React.Component {

    constructor(props) {
        super(props);
        var quotesData = props.quotesData;
        var selectedCompanies = (typeof quotesData != "undefined" && typeof quotesData.selected_companies != "undefined") ?
            quotesData.selected_companies : [];
        this.state = {
            storage_use: { id: SERVICE_PERSONAL_STORAGE, value: "PERSONAL", label: "Personal storage" },
            storage_size: { value: '', lable: '' },
            storage_size_value: '',
            item_lists: [],
            other_services: [],
            other_comments_text: "",
            calender_date_of_service_start: '',
            calender_date_of_service_end: '',
            input_email: props.userProfile.email,
            input_phone: props.userProfile.address ? props.userProfile.address.phoneNumber : '',
            input_name: props.userProfile.customerFirstName,
            input_last_name: props.userProfile.customerLastName,
            selectedCompanies: selectedCompanies,
            minDateInvalid: false,
            loader: props.loader,
        }
        this.storageRequest = this.storageRequest.bind(this);
        this.ContactDetails = this.ContactDetails.bind(this);
        this.handleCustomChange = this.handleCustomChange.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.currentStep = this.currentStep.bind(this);
        this.moveNext = this.moveNext.bind(this);
        this.submitData = this.submitData.bind(this);
        this.selectCompanyChange = this.selectCompanyChange.bind(this);
        this.howToProceed = this.howToProceed.bind(this);
        this.showOrHideModalSelectCompanyError = this.showOrHideModalSelectCompanyError.bind(this);
        allServiceConstant = props.service_constants;

    }
    componentDidMount() {
        allServiceConstant = this.props.service_constants;
        URLConstant = this.props.url_constants;
        current_city = this.props.current_city;
    }
    sumarryItems() {
        const { storage_use, calender_date_of_service_start, calender_date_of_service_end, item_lists, storage_size } = this.state;
        var storItems = item_lists.map(item => item.label).join(' & ');
        return [
            { label: 'Type', value: storage_use.label },
            { label: 'Start date', value: calender_date_of_service_start },
            { label: 'End date', value: calender_date_of_service_end },
            { label: 'Store item', value: storItems },
            { label: 'Storage size', value: storage_size.label }
        ];
    }
    hashChangeHandler() {
        let hashVal = window.location.hash;
        let step;
        hashVal.length ? step = parseInt(window.location.hash.replace('#', '')) : step = 1;
        this.props.moveNext(step);
        scrollToTop();
    }
    currentStep() {
        return this.props.formCurrentStep;
    }
    howToProceed(isSkipCompany) {

        const { howToProceed } = this.props;
        var nextStep = isSkipCompany ? 3 : 2;

        var selectedCompanies = isSkipCompany ? [] : this.state.selectedCompanies;

        this.setState({
            isSkipCompany: isSkipCompany,
            selectedCompanies: selectedCompanies
        })

        howToProceed(isSkipCompany, this.state);

    }
    moveNext(step) {
        const { moveNextStep } = this.props;
        moveNextStep(step, this.state);
    }
    submitData() {
        const { submitLeadData } = this.props;
        submitLeadData(this.state);

    }
    generateStorageDataForSubmission() {
        var {
            type_of_move,
            home_type,
            rooms,
            moving_size,
            from_emirate,
            from_area,
            from_address,
            field_to,
            field_from,
            to_emirate,
            to_area,
            to_address,
            other_services,
            other_comments_text,
            item_lists,
            calender_date_of_service_start,
            calender_date_of_service_end,
            input_email,
            input_phone,
            input_name,
            input_last_name,
            selectedCompanies,
            selectedOffers,
            isSkipCompany,
            storage_size,
            item_lists
        } = this.state;

        var service_id = parseInt(allServiceConstant.SERVICE_STORAGE);

        var serviceLocationIdsToAdd = LocationHelper.getCurrentCityId();//{ //serviceLocationIdsToAddparseInt(from_emirate.id);

        //var serviceIds = parseInt(commonHelper.sizeToServiceConverter(moving_size.value, service_id));

        calender_date_of_service_start = calender_date_of_service_start.split("-").reverse().join('-');
        calender_date_of_service_end = calender_date_of_service_end.split("-").reverse().join('-');

        //var moving_size_value = commonHelper.movingSizeIdConverter(moving_size.value);

        // todo otherRequiredServiceModels dynamic

        let expectedMoveDateStart = moment(calender_date_of_service_start).unix() + 28800;
        let expectedMoveDateEnd = moment(calender_date_of_service_end).unix() + 28800;

        expectedMoveDateStart = moment.unix(expectedMoveDateStart).format("MMMM DD, YYYY HH:mm");
        expectedMoveDateEnd = moment.unix(expectedMoveDateEnd).format("MMMM DD, YYYY HH:mm");
        let otherServices = [];


        if (other_services.length) {
            other_services.map((item) => {
                otherServices.push({ serviceId: item.id });
            })
        }
        let additionalItems = '';
        if (item_lists.length) {
            other_services.map((item) => {
                additionalItems = additionalItems + '' + item.value + ', ';
            })
        }

        var data = {
            "requestModel": {
                "customerOtherDetails": other_comments_text,
                "contactInformationModel": {
                    "personName": input_name + " " + input_last_name,
                    "personPhone": input_phone,
                    "personEmail": input_email
                },
                "storageRequestModel": {
                    "sizeToBeRentedOut": storage_size.value,
                    "storageReason": this.state.storage_use.value,
                    "storeItemTillDate": expectedMoveDateEnd,
                    "otherDetail": additionalItems,
                    "serviceId": service_id,
                    "expectedMoveDate": expectedMoveDateStart,
                    "storageLocationCityId": serviceLocationIdsToAdd,
                    "fromAddress": {
                        "addressLine1": " ",
                        "city": serviceLocationIdsToAdd
                    },
                    "toAddress": {
                        "addressLine1": " ",
                        "city": serviceLocationIdsToAdd
                    }
                },

                "otherRequiredServiceModels": otherServices,

                "serviceLocationIdsToAdd": [serviceLocationIdsToAdd],
                "customerId": this.props.signInDetails.customer.id
            }
        }
        var serviceProviderIds = [];
        var serviceProviderName = [];

        if (!isSkipCompany && selectedCompanies && selectedCompanies.length) {
            selectedCompanies.map((item) => {
                serviceProviderIds.push(item.id);
                serviceProviderName.push(item.name)
            })
            data["requestModel"]["serviceProviderIds"] = serviceProviderIds;
        }

        if (selectedOffers && selectedOffers.length) {
            var confirmationDeals = [];
            selectedOffers.map((item) => {
                confirmationDeals.push(item.id);
            });
            data['claimOfferModel'] = { dealIds: [], requestServiceTypeId: 0 };
            data['claimOfferModel']['dealIds'] = confirmationDeals;
            data['claimOfferModel']['requestServiceTypeId'] = 1;
        }




        var url = window.location.protocol + '//' + window.location.host + window.location.pathname;
        var queryParams = {};
        queryParams["service_id"] = service_id;
        queryParams["fromEmirate"] = serviceLocationIdsToAdd;
        var rid = "";

        postRequest(this.props.signInDetails.access_token || '', JSON.stringify(data)).then((response) => {

            var response_row = JSON.parse(response.data.addRequest);
            if (response_row.success) {
                var rows = response_row.rows[0];
                var rid = rows.requestUuid;
                queryParams["status"] = "success";
                if (serviceProviderName.length) {
                    queryParams["service_providers"] = JSON.stringify(serviceProviderName)
                }

            } else {
                var status = 'OUCH- There was an error while fulfulling your request. Please dont get frightened because of the error below, if this issue presist contact team servicemarket.';
                queryParams["status"] = status;
            }
            var query = commonHelper.objToParams(queryParams);
            query = encodeURI(query);
            window.location = url + "/confirmation/" + rid + "?" + query;
        });

    }
    showOrHideModalSelectCompanyError(boolVal) {
        console.log('showOrHideModalSelectCompanyError');
        const {showOrHideModalSelectCompanyError} = this.props;
        showOrHideModalSelectCompanyError(boolVal);
    } 
    selectCompanyChange(value) {
        this.setState({
            selectedCompanies: value
        });
    }
    handleCustomChange(name, value) {
        this.setState({
            [name]: value
        });
    }
    handleInputChange(name, value) {
        this.setState({
            [name]: value
        });
    }
    handleNoOfUnits(name, value) {
        this.setState({
            number_of_units: value
        });
    }
    storageRequest() {
        var data_values = this.props.data_values;

        var service_constants = allServiceConstant;

        var storage_use_option = [
            { id: SERVICE_PERSONAL_STORAGE, value: "PERSONAL", label: "Personal storage" },
            { id: SERVICE_COMMERCIAL_STORAGE, value: "COMMERCIAL", label: "Commercial storage" }
        ];

        var storage_use_value = this.state.storage_use;

        var storage_size_options = [
            { id: 1, value: "SQUARE_SQUARE_FEET_10", label: "10 square feet" },
            { id: 2, value: "SQUARE_SQUARE_FEET_25", label: "25 square feet" },
            { id: 3, value: "SQUARE_SQUARE_FEET_50", label: "50 square feet" },
            { id: 4, value: "SQUARE_SQUARE_FEET_75", label: "75 square feet" },
            { id: 5, value: "SQUARE_SQUARE_FEET_100", label: "100 square feet" },
            { id: 6, value: "SQUARE_SQUARE_FEET_150", label: "150 square feet" },
            { id: 7, value: "SQUARE_SQUARE_FEET_250_AND_BIGGER", label: "250 square feet and bigger" }
        ];

        var items_to_store_options = [
            { id: 1, value: "furniture/personal items", label: "Furniture/Personal items" },
            { id: 2, value: "car", label: "Car" },
            { id: 3, value: "perishable items", label: "Perishable items" },
            { id: 4, value: "documents", label: "Documents" },
            { id: 5, value: "other", label: "Other" }
        ];



        var storage_size_value = this.state.storage_size;

        var other_comments_text_value = this.state.other_comments_text;

        var items_list_value = this.state.item_lists;

        var calender_date_of_service_start_value = this.state.calender_date_of_service_start;

        var calender_date_of_service_end_value = this.state.calender_date_of_service_end;

        var typeOfJourney = this.props.typeOfJourney();

        var currentStepClass = "";

        if (typeOfJourney == 1) {
            currentStepClass = this.currentStep() === 1 ? '' : "d-none";
        } else {
            currentStepClass = this.currentStep() === 2 ? '' : "d-none";
        }

        var other_services_value = this.state.other_services;

        var other_services = [
            { id: allServiceConstant.SERVICE_PACKINGUNPACKING_STORAGE, value: "packing-unpacking", label: "Packing" },
            { id: allServiceConstant.SERVICE_PICK_UP_AND_DELIVERY_SERVICE, value: "pickup-deliver", label: "Pick up and delivery" },
            { id: allServiceConstant.SERVICE_PACKINGUNPACKING, value: "drop-off", label: "Redelivery to final destination" },
            { id: allServiceConstant.SERVICE_CLIMATE_CONTROLLED_STORAGE_STORAGE, value: "climate-control", label: "Climate controlled storage" },
            { id: allServiceConstant.SERVICE_24_HOURS_ACCESS_STORAGE, value: "24hour-access", label: "24 hour access" },
            { id: allServiceConstant.SERVICE_COLD_STORAGE_FOR_PERISHABLE_ITEMS_STORAGE, value: "cold-storage", label: "Cold storage (for perishable items)" },

        ];

        const invalidDate = moment(this.state.calender_date_of_service_end, 'DD-MM-YYYY').isBefore(moment(this.state.calender_date_of_service_start, 'DD-MM-YYYY')) ? true : false;
        const dateClass = `required ${invalidDate ? 'minDate' : ''}`;

        const validationMessage = invalidDate ? 'Till date cannot be before from date' : 'Please select a date';
        return (
            <div id="section-request-form" className={currentStepClass + " no-bottom-space"}>
                <section>
                    <FormFieldsTitle title="What kind of storage do you need?" />
                    <CheckRadioBoxInput
                        InputType="radio" name="storage_use"
                        inputValue={storage_use_value}
                        items={storage_use_option}
                        childClass="col-6 mb-3 d-flex"
                        onInputChange={this.handleCustomChange} />
                </section>

                <section>
                    <div className="row mb-2">
                        <DatePick
                            name="calender_date_of_service_start"
                            inputValue={calender_date_of_service_start_value}
                            placeholderText="Select a date"
                            title="Store from"
                            onInputChange={this.handleInputChange}
                            validationClasses="required"
                            validationMessage='Please select a date'
                            disableFriday={false}
                        />

                        <DatePick name="calender_date_of_service_end"
                            inputValue={calender_date_of_service_end_value}
                            placeholderText="Select a date"
                            title="Store until"
                            onInputChange={this.handleInputChange}
                            validationClasses={dateClass}
                            validationMessage={validationMessage}
                            disableFriday={false}
                        />
                    </div>
                </section>
                <section>
                    <FormFieldsTitle title="What would you like to store? You can select more than one." />
                    <CheckRadioBoxInput
                        InputType="checkbox"
                        name="item_lists"
                        inputValue={items_list_value} items={items_to_store_options}
                        childClass="col-6 col-md-4 mb-3 d-flex ceilings-painted"
                        onInputChange={this.handleCustomChange} />
                </section>
                <section>
                    <FormFieldsTitle title="How much storage space do you need?" />
                    <div className="row mb-2">
                        <div className="col-12 col-md-6 mb-3">
                            <SelectInput name="storage_size" inputValue={storage_size_value} label="Select storage size" options={storage_size_options} onInputChange={this.handleCustomChange} />
                            <p key="hover1" data-tip="<u><li>Less than 5 boxes: Choose the 10 sq ft unit</li><li>15-20 boxes: Choose the 25 sq ft unit</li><li>35-40 boxes or equivalent: Choose the 50 sq ft unit</li><li>A full Studio - Choose the 75 sq ft unit</li><li>A 1 BR Apt - Choose the 100 sq ft unit</li><li>A 2 BR Apt/Villa - Choose the 150 sq ft unit</li><li>A 3 BR Apt/ Vill or larger - Choose the 250 sq ft unit</li></u>"
                                className="small d-inline-block mt-1" >Not sure? We can help!</p>
                            <ReactTooltip html={true} place='bottom' />
                        </div>
                    </div>
                </section>
                <section>
                    <FormFieldsTitle title="Do you also need these services?" />
                    <CheckRadioBoxInput
                        InputType="checkbox"
                        name="other_services"
                        inputValue={other_services_value}
                        items={other_services}
                        childClass="col-6 col-md-4 mb-3 d-flex ceilings-painted"
                        onInputChange={this.handleCustomChange} />
                </section>
                <section>
                    <FormFieldsTitle title="Is there anything else you would like us to know? (Optional)" />
                    <div className="row mb-4">
                        <div className="col mb-3">
                            <TextareaInput inputValue={other_comments_text_value} name="other_comments_text" placeholder="If you have any other requirements, feel free to describe them here in as much details as you want. Or just leave us a message here to call you if its easier to explain on the phone" onInputChange={this.handleCustomChange} />
                        </div>
                    </div>
                </section>
                <section>
                    <QuotesNextStep moveNext={this.moveNext} howToProceed={this.howToProceed} formCurrentStep={this.props.formCurrentStep} submitData={this.submitData} />
                </section>
            </div>
        )
    }
    selectCompanies() {
        var currentStepClass = this.currentStep() === 2 ? '' : "d-none";
        const serviceID = allServiceConstant.SERVICE_STORAGE;
        const currentCity = this.props.current_city;
        const { selectedCompanies } = this.state;
        const { showSelectCompanyError } = this.props;
        return (
            <div className={currentStepClass}>
                <SelectCompanyWithFilters
                    serviceID={serviceID}
                    currentCity={currentCity}
                    selectChange={this.selectCompanyChange}
                    selectedCompanies={selectedCompanies}
                    showSelectCompanyError={showSelectCompanyError}
                    showOrHideModalSelectCompanyError={this.showOrHideModalSelectCompanyError} />
                <section>
                    <QuotesNextStep moveNext={this.moveNext} howToProceed={this.howToProceed} formCurrentStep={this.props.formCurrentStep} submitData={this.submitData} />
                </section>
            </div>
        )
    }
    ContactDetails(isLocationDetailShow) {
        const cityOptions = [{ id: 1, value: "1", label: "Dubai" }];
        const currentStepClass = this.currentStep() === 3 ? '' : "d-none";
        //var currentStepClass =  ''
        const isLocationDetailDisplay = typeof isLocationDetailShow != "undefined" ? isLocationDetailShow : "yes";

        var contact_details = {
            input_email: this.state.input_email,
            input_phone: this.state.input_phone,
            input_name: this.state.input_name,
            input_last_name: this.state.input_last_name
        }
        let userProfile = this.props.userProfile;


        if (!this.state.loader) {
            return (
                <div id="personal-information-form" className={currentStepClass}>
                    <ContactDetailsStep
                        userProfile={userProfile}
                        signInDetails={this.props.signInDetails}
                        isLocationDetailShow={isLocationDetailDisplay}
                        contact_details={contact_details}
                        handleDropDownChange={this.handleInputChange}
                        handleInputChange={this.handleInputChange}
                        mobileSummary={this.mobileSummary}
                        moveNext={this.moveNext}
                        formCurrentStep={this.props.formCurrentStep}
                        submitData={this.submitData}
                        isSkipCompany={this.props.isSkipCompany}
                        currentCity = {this.props.current_city} />
                    <section>
                        <QuotesNextStep moveNext={this.moveNext} howToProceed={this.howToProceed} formCurrentStep={this.props.formCurrentStep} submitData={this.submitData} />
                    </section>
                </div>
            )

        } else {
            return (
                <main loader={this.state.loader ? "show" : "hide"}><Loader /></main>
            );
        }
    }

    render() {
        var package_opt = this.state.package_options;
        var transport_fee = this.state.transportation_fees;
        var isLoading = this.state.loader || this.props.loader;

        const { selectedCompanies } = this.state;
        const items = this.sumarryItems();
        const showPromo = false;
        const showPrice = false;
        var typeOfJourney = this.props.typeOfJourney();
        return (
            <React.Fragment>
                <div className="col-lg-8">
                    {
                        isLoading ? <main loader={isLoading ? "show" : "hide"}><Loader /></main> :
                            (<React.Fragment>
                                {this.storageRequest()}
                                {typeOfJourney == 1 && this.selectCompanies()}
                                {this.ContactDetails("no")}
                            </React.Fragment>)
                    }
                </div>
                <div className="col-md-3 ml-auto d-lg-block d-none">
                    <BookingSummary
                        typeOfFlow="quotes"
                        items={items}
                        showPromo={showPromo}
                        showPrice={showPrice}
                        selectedCompanies={selectedCompanies}
                        selectChange={this.selectCompanyChange}
                        formCurrentStep={this.props.formCurrentStep}
                        typeOfJourney={typeOfJourney} />
                </div>
            </React.Fragment>
        )
    }
}

export default QuotesStoragePage;