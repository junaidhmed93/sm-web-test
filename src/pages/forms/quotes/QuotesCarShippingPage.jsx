import React from "react";
import cloneDeep from "lodash/cloneDeep";
import FormFieldsTitle from "../../../components/FormFieldsTitle";
import CheckRadioBoxInput from "../../../components/Form_Fields/CheckRadioBoxInput";
import CheckRadioBoxInputWithPrice from "../../../components/Form_Fields/CheckRadioBoxInputWithPrice";
import DatePick from "../../../components/Form_Fields/DatePick";
import SelectInput from "../../../components/Form_Fields/SelectInput";
import TextFloatInput from "../../../components/Form_Fields/TextFloatInput";
import TextareaInput from "../../../components/Form_Fields/TextareaInput";
import PaymentMethods from "../../../components/Form_Fields/PaymentMethods";
import SubmitBooking from "../../../components/Form_Fields/SubmitBooking";
import BookingSummary from "../../../components/Form_Fields/BookingSummary";
import PackageBox from "../../../components/Form_Fields/PackageBox";
import BookNextStep from "../../../components/Form_Fields/BookNextStep";
import FormTitleDescription from "../../../components/FormTitleDescription";
import ContactDetailsStep from "../../../components/ContactDetailsStep";
import QuotesNextStep from "../../../components/Form_Fields/QuotesNextStep";
import SelectCompanyWithFilters from "../../../components/SelectCompanyWithFilters";
import { isValidSection, scrollToTop, fetchCountriesListAPI, fetchCarYearsAPI, fetchCarDetailsAPI } from "../../../actions/index";
import LocationHelper from '../../../helpers/locationHelper';
import commonHelper from '../../../helpers/commonHelper';
import PlacesAutocomplete from 'react-places-autocomplete';
import ReactTooltip from 'react-tooltip'
import moment from 'moment';
import Loader from "../../../components/Loader";

import {
    getCountryCodeById
}
    from '../../../utils/commonUtils';

let allServiceConstant = {};
let URLConstant = {};
let current_currency = "AED";
let current_city = "dubai";
class QuotesCarShippingPage extends React.Component {
    _isMounted = false;

    constructor(props) {
        super(props);
        var quotesData = props.quotesData;
        var selectedCompanies = (typeof quotesData != "undefined" && typeof quotesData.selected_companies != "undefined") ?
            quotesData.selected_companies : [];
        this.state = {
            field_from: "UAE",
            from_address: "",
            field_to: "UAE",
            sourceCountryId: 1,
            targetCountryId: 1,
            to_address: "",
            cars_shipped: [{ field_car_year: { value: "", label: "" }, field_car_make: { value: "", label: "" }, field_car_model: { value: "", label: "" }, field_car_trim: { value: "", label: "" } }],
            field_cars_number: { id: 1, value: "1", label: "One car" },
            other_comments_text: "",
            item_lists: "",
            move_date: '',
            input_email: props.userProfile.email,
            input_phone: props.userProfile.address ? props.userProfile.address.phoneNumber : '',
            input_name: props.userProfile.customerFirstName,
            input_last_name: props.userProfile.customerLastName,
            selectedCompanies: [],
            pickupInvalid: false,
            dropInalid: false,
            countries: [],
            years: [], carDetails: [],
            make: [[], [], []],
            modal: [[], [], []],
            modal_number: [[], [], []],
            showSelectCompanyError: false,
            hoverShow: false,
            pickupFocus: false,
            dropFocus: false,
            loader: props.loader,
        }
        this.carShippingRequest = this.carShippingRequest.bind(this);
        this.ContactDetails = this.ContactDetails.bind(this);
        this.handleCustomChange = this.handleCustomChange.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.currentStep = this.currentStep.bind(this);
        this.moveNext = this.moveNext.bind(this);
        this.carShippingSection = this.carShippingSection.bind(this);
        this.carShippingDetails = this.carShippingDetails.bind(this);
        this.submitData = this.submitData.bind(this);
        this.selectCompanyChange = this.selectCompanyChange.bind(this);
        this.howToProceed = this.howToProceed.bind(this);
        this.loadCarDetails = this.loadCarDetails.bind(this);
        this.handleMouseIn = this.handleMouseIn.bind(this);
        this.handleMouseOut = this.handleMouseOut.bind(this);
        this.setPickupFocus = this.setPickupFocus.bind(this);
        this.setDropFocus = this.setDropFocus.bind(this);
        this.showOrHideModalSelectCompanyError = this.showOrHideModalSelectCompanyError.bind(this);
        allServiceConstant = props.service_constants;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }
    componentDidMount() {
        this._isMounted = true;

        const script = document.createElement("script");
        const googleApiKey = commonHelper.getGoogleAPIKey();
        script.src = `https://maps.googleapis.com/maps/api/js?key=${googleApiKey}&libraries=places&callback=myCallbackFunc`;
        script.async = true;
        document.body.appendChild(script);

        allServiceConstant = this.props.service_constants;
        this.state.service = parseInt(allServiceConstant.SERVICE_CAR_SHIPPING);
        URLConstant = this.props.url_constants;
        current_city = this.props.current_city;

        fetchCountriesListAPI(res => {
            if (this._isMounted) {
                let countries = [];

                if (res && res.data && res.data.length) {
                    countries = res.data.map((d) => {
                        return { id: d.id, value: d.id.toString(), label: d.name.toString() }
                    });
                }
                this.setState({ countries: countries });
            }
        },
            (err) => console.log(err));

        let res = fetchCarYearsAPI();
        
        let years = [];
        if (res && res.length > 0) {
            years = res.map((d, i) => {
                return { id: i + 1, value: d.toString(), label: d.toString() }
            });
        }
        this.setState({ years: years });
    }

    loadCarDetails(type, val, id) {

        let carMake = this.state.make;
        let carModel = this.state.modal;
        let carNumber = this.state.modal_number;
        let years = cloneDeep(this.state.years);
        let { cars_shipped } = this.state;

        if (type === 'year') {
            fetchCarDetailsAPI(val, response => {

                if (response.data && response.data.length) {
                    let res = response.data;
                    res = res.filter(a => { if (a != '') return a; });
                    carMake[id] = res.map((model, i) => {
                        return { id: i + 1, value: model.brandName.toString(), label: model.brandName.toString() };
                    });
                    
                    const yearObj = years.find(x => x.label === val);

                    cars_shipped[id] = {
                        field_car_year: yearObj ? yearObj : '',
                        field_car_make: carMake[id] && carMake[id][0] ? carMake[id][0] : '',
                        field_car_model: '', field_car_trim: ''
                    };

                    this.setState({
                        carDetails: res, make: carMake,
                        cars_shipped: cars_shipped,

                    });
                }
                this.loadCarDetails('make', carMake[id] && carMake[id][0] ? carMake[id][0].value : '', id);
            },
                (err) => console.log(err));
        }
        else if (type === 'make') {
            const { carDetails } = this.state;

            let make = carDetails.find(a => a.brandName === val);
            

            if (make && make.model && make.model.length > 0) {
                make.model = make.model.filter(a => { if (a != '') return a; });
                carModel[id] = make.model.map((a, i) => {
                    return { id: i + 1, value: a, label: a };
                });
            }
            else
                carModel[id] = [];

            cars_shipped[id] = {
                field_car_year: cars_shipped[id].field_car_year,
                field_car_make: carMake[id].find(a => a.value == val),
                field_car_model: carModel[id] && carModel[id][0] ? carModel[id][0] : '',
                field_car_trim: ''
            };

            this.setState({ modal: carModel, cars_shipped: cars_shipped });
            //this.loadCarDetails('model', carModel[id] && carModel[id][0] ? carModel[id][0].value : '', id);
        }
        else if (type === 'model') {

            const { carDetails } = this.state;
            const { make } = cloneDeep(this.state);


            const item = carDetails.find(a => a.modelMakeName == cars_shipped[id].field_car_make.value);


            let modelName = '';
            if (item && item.modelNames) {
                modelName = item.modelNames.find(a => a.modelName == val);
            }


            let modelNo = '';
            if (modelName && modelName.modelNumbers)
                modelNo = modelName.modelNumbers
            else modelNo = '';


            if (modelNo && modelNo != '' && modelNo.length > 0) {
                modelNo = modelNo.filter(a => { if (a != '') return a; });
                carNumber[id] = modelNo.map((a, i) => {
                    return { id: i + 1, value: a.toString(), label: a.toString() };
                });
            }
            else
                carNumber[id] = [];

            cars_shipped[id] = {
                field_car_year: cars_shipped[id].field_car_year,
                field_car_make: cars_shipped[id].field_car_make,
                field_car_model: carModel[id].find(a => a.value == val),//cars_shipped[id].field_car_model,
                field_car_trim: carNumber[id] && carNumber[id][0] ? carNumber[id][0] : ''
            }

            this.setState({ modal_number: carNumber, cars_shipped: cars_shipped });
        }

    }
    sumarryItems() {
        const { move_date, field_from, field_to, field_cars_number } = this.state;
        return [
            { label: 'Date', value: move_date },
            { label: 'From', value: field_from },
            { label: 'To', value: field_to },
            { label: 'Cars Count', value: field_cars_number.label }
        ];
    }
    handleMouseIn = () => {
        this.setState({ hoverShow: true });
    }
    handleMouseOut = () => {
        this.setState({ hoverShow: false });
    }
    currentStep() {
        return this.props.formCurrentStep;
    }
    howToProceed(isSkipCompany) {
        const nextStep = isSkipCompany ? 3 : 2;
        let { field_to, field_from, pickupInvalid, dropInvalid, fromCountryInvalid, toCountryInvalid } = this.state;

        pickupInvalid = this.state.from_address == '' ? true : false;
        dropInvalid = this.state.to_address == '' ? true : false;
        fromCountryInvalid = field_from == '' ? true : false;
        toCountryInvalid = field_to == '' ? true : false;

        const { howToProceed } = this.props;

        var selectedCompanies = isSkipCompany ? [] : this.state.selectedCompanies;

        this.setState({
            selectedCompanies: selectedCompanies,
            pickupInvalid: pickupInvalid,
            dropInvalid: dropInvalid,
            fromCountryInvalid: fromCountryInvalid,
            toCountryInvalid: toCountryInvalid
        });

        //this.moveNext(nextStep);//
        howToProceed(isSkipCompany, this.state);

    }
    moveNext(step) {
        const { moveNextStep } = this.props;

        let { field_to, field_from, pickupInvalid, dropInvalid, fromCountryInvalid, toCountryInvalid } = this.state;

        pickupInvalid = this.state.from_address == '' ? true : false;
        dropInvalid = this.state.to_address == '' ? true : false;
        fromCountryInvalid = field_from == '' ? true : false;
        toCountryInvalid = field_to == '' ? true : false;

        this.setState({
            pickupInvalid: pickupInvalid,
            dropInvalid: dropInvalid,
            fromCountryInvalid: fromCountryInvalid,
            toCountryInvalid: toCountryInvalid

        });
        moveNextStep(step, this.state);
        /*if (formCurrentStep == '1') {

            this.setState({
                pickupInvalid: this.state.from_address == '' ? true : false,
                dropInvalid: this.state.to_address == '' ? true : false,
                fromCountryInvalid: field_from == '' ? true : false,
                toCountryInvalid: field_to == '' ? true : false,

            })

            if (!pickupInvalid && !dropInvalid && !toCountryInvalid && !fromCountryInvalid) {

                moveNextStep(step, this.state);
            }
        } else
            moveNextStep(step, this.state);*/
        // const { formCurrentStep, signInDetails, showLoginModal } = this.props;
        // var valid = true;
        // // Step 1 validation
        // if (formCurrentStep == '1') {
        //     valid = isValidSection('section-request-form');
        //     if (valid) {
        //         window.location.hash = step;
        //         scrollToTop();
        //         // Show Login Modal
        //         if (!signInDetails.customer) {
        //             showLoginModal();
        //         }
        //     }
        // }
        // // Step 2
        // if (formCurrentStep == '2') {
        //     window.location.hash = step;
        //     scrollToTop();
        //     // Show Login Modal
        //     if (!signInDetails.customer) {
        //         showLoginModal();
        //     }
        // }
    }
    submitData() {

        const { submitLeadData } = this.props;
        submitLeadData(this.state);
    }
    selectCompanyChange(value) {
        this.setState({
            selectedCompanies: value
        });
    }
    handleCustomChange(name, value) {
        let fromCountryId = this.state.sourceCountryId, toCountryId = this.state.targetCountryId;
        const countryOptions = this.state.countries;
        const Val = !!value.label ? value.label : value;
        if (name == 'field_from') {
            fromCountryId = countryOptions.find(c => c.label === Val).id;
        }

        if (name == 'field_to') {
            toCountryId = countryOptions.find(c => c.label === Val).id;
        }

        this.setState({
            [name]: (name == 'field_from' || name == 'field_to' ? Val : value),
            sourceCountryId: fromCountryId,
            targetCountryId: toCountryId,
        });

        if (name == "field_cars_number") {

            var car_models = [];
            var cars_shipped = this.state.cars_shipped;
            // const idx = this.state.other_services.findIndex((a) => a.value === "car-shipping-carrier");
            // if (idx > -1) {
            for (var i = 0; i < value.value; i++) {
                if (!!cars_shipped[i])
                    car_models.push(cars_shipped[i]);
                else
                    car_models.push({
                        field_car_year: { value: "", label: "" },
                        field_car_make: { value: "", label: "" },
                        field_car_model: { value: "", label: "" },
                        field_car_trim: { value: "", label: "" }
                    });
            }

            this.setState({
                cars_shipped: car_models
            });
            //}
        }
        else if (name == "other_services") {

            //var other_services_val = this.state.other_services;
        }

    }
    handleInputChange(name, value) {
        if (name === 'from_address') {
            this.setState({
                [name]: value,
                pickupInvalid: !!value ? false : true,
            });
        }
        else if (name === 'to_address') {
            this.setState({
                [name]: value,
                dropInvalid: !!value ? false : true,
            });
        }
        else {
            this.setState({
                [name]: value
            });
        }
    }
    onError = (status, clearSuggestions) => {
        console.log('Google Maps API returned error with status: ', status)
        clearSuggestions();
    }
    handleNoOfUnits(name, value) {
        this.setState({
            number_of_units: value
        });
    }
    carShippedSelectChange = (idx) => (name, value) => {


        var idx_val = parseInt(idx) + 1;

        var new_name = name.replace("_" + idx_val, "");

        const newCarShip = this.state.cars_shipped.map((car_shipped, sidx) => {
            if (idx !== sidx) return car_shipped;
            return { ...car_shipped, [new_name]: value };
        });
        this.setState({ cars_shipped: newCarShip });

        if (name.indexOf('_year') > -1) {
            this.loadCarDetails('year', value.label, idx);
        }

        if (name.indexOf('_make') > -1) {
            this.loadCarDetails('make', value.label, idx);
        }

        if (name.indexOf('_model') > -1) {
            this.loadCarDetails('model', value.label, idx);
        }


    }
    setPickupFocus(val) {
        this.setState({ pickupFocus: val });
    }
    setDropFocus(val) {
        this.setState({ dropFocus: val });
    }

    carShippingDetails() {


        const { years, make, modal, modal_number } = this.state;
        const year_opt = years;


        const make_opt = make;

        const modal_opt = modal;

        const modal_number_opt = modal_number;
        var childClass = "col-12 col-md-3 mb-4"
        const carsCount = this.state.cars_shipped.length;       
        return (            
            this.state.cars_shipped.map((car_shipped, idx) => (
                <React.Fragment>
                <section key={idx}>
                    <FormFieldsTitle title={"Car " + (idx + 1) + " details"} />
                    <div className="row">
                        <div className="col-12 col-md-4 mb-3">
                            <FormFieldsTitle title="Model year:" className='h4' />
                            <SelectInput searchable={true} inputValue={car_shipped.field_car_year}
                                name={"field_car_" + (idx + 1) + "_year"} label="Select model year" options={year_opt}
                                onInputChange={this.carShippedSelectChange(idx)} validationClasses="required" />
                        </div>
                        <div className="col-12 col-md-4 mb-3">
                            <FormFieldsTitle title="Car make:" className='h4' />
                            <SelectInput searchable={true} inputValue={car_shipped.field_car_make}
                                name={"field_car_" + (idx + 1) + "_make"} label='Nothing selected' options={make_opt[idx]}
                                onInputChange={this.carShippedSelectChange(idx)} />
                        </div>
                        <div className="col-12 col-md-4 mb-3">
                            <FormFieldsTitle title="Car model:" className='h4' />
                            <SelectInput searchable={true} inputValue={car_shipped.field_car_model}
                                name={"field_car_" + (idx + 1) + "_model"} label='Nothing selected' options={modal_opt[idx]}
                                onInputChange={this.carShippedSelectChange(idx)} />
                        </div>
                        <div className="col-12 col-md-4 mb-3" style={{display:'none'}}>
                            <FormFieldsTitle title="Model number:" className='h4' />
                            <SelectInput searchable={true}
                                inputValue={car_shipped.field_car_trim && car_shipped.field_car_trim.label != '' ? car_shipped.field_car_trim : 'Nothing selected'}
                                name={"field_car_" + (idx + 1) + "_trim"} label='Nothing selected' options={modal_number_opt[idx]}
                                onInputChange={this.carShippedSelectChange(idx)} />
                        </div>
                    </div>
                </section>                
                { carsCount > idx+1 ? <hr className='d-md-none'></hr>:''}
                </React.Fragment>
            ))
        )

    }
    carShippingSection() {
        var field_cars_number_opt = [
            { id: 1, value: "1", label: "One car" },
            { id: 2, value: "2", label: "Two cars" },
            { id: 3, value: "3", label: "Three cars" }
        ];
        var field_cars_number_value = this.state.field_cars_number;

        return (
            <section>
                <FormFieldsTitle title="How many cars do you want shipped?" />
                <CheckRadioBoxInput
                    InputType="radio"
                    name="field_cars_number"
                    inputValue={field_cars_number_value}
                    items={field_cars_number_opt}
                    parentClass="row"
                    childClass="col-4 mb-3 d-flex"
                    onInputChange={this.handleCustomChange} />
            </section>
        )
    }
    carShippingRequest() {
        var data_values = this.props.data_values;

        var service_constants = allServiceConstant;

        var from_country_value = this.state.field_from;

        var from_address_value = this.state.from_address;

        var to_country_value = this.state.field_to;

        var to_address_value = this.state.to_address;

        var other_comments_text_value = this.state.other_comments_text;

        var move_date_value = this.state.move_date;

        //other_services
        var typeOfJourney = this.props.typeOfJourney();

        var currentStepClass = "";

        if (typeOfJourney == 1) {
            currentStepClass = this.currentStep() === 1 ? '' : "d-none";
        } else {
            currentStepClass = this.currentStep() === 2 ? '' : "d-none";
        }

        var countryOptions = this.state.countries;

        var cap_shipping_section = this.carShippingSection();

        var cap_to_be_shipped_section = this.carShippingDetails();

        const selectedFromCountry = countryOptions.length > 0 && countryOptions.find(x => x.label == from_country_value);
        const selectedToCountry = countryOptions.length > 0 && countryOptions.find(x => x.label == to_country_value);
        // const selectedFromCountry = getCountryCodeById(from_country_value.value);

        // const selectedToCountry = getCountryCodeById(to_country_value.value);

        let invalidClassFrom = this.state.pickupInvalid ? 'btn-radio form-control p-3 border rounded border-danger invalidClassFrom' : 'btn-radio form-control p-3 border rounded invalidClassFrom';

        const fromCountryCode = getCountryCodeById(selectedFromCountry.value)
        const toCountryCode = getCountryCodeById(selectedToCountry.value)

        if (this.state.pickupInvalid) {
            let sectionPick = document.getElementById('host');
            sectionPick.classList.add('border-danger')
            let span = document.createElement("span");
            span.className = "error-msg";
            span.innerText = 'This field is required';

            if (!!sectionPick) sectionPick.parentNode.appendChild(span);
        }
        let invalidClassTo = this.state.dropInvalid ? 'btn-radio form-control p-3 border rounded border-danger invalidClassTo' : 'btn-radio form-control p-3 border rounded invalidClassTo';
        if (this.state.dropInvalid) {
            let sectionDrop = document.getElementById('destination');
            sectionDrop.classList.add('border-danger');
            let span = document.createElement("span");
            span.className = "error-msg";
            span.innerText = 'This field is required';

            if (!!sectionDrop) sectionDrop.parentNode.appendChild(span);
        }

        const tooltipStyle = {
            display: this.state.hoverShow ? 'block' : 'none'
        }

        const pickupFocused = this.state.pickupFocus;
        const dropFocused = this.state.dropFocus;
        return (
            <div id="section-request-form" className={currentStepClass}>
                <section>
                    <FormFieldsTitle title="Expected move date?" />
                    <div className="row mb-2">
                        <DatePick name="move_date"
                            inputValue={move_date_value}
                            onInputChange={this.handleInputChange}
                            placeholderText='Any date'
                            validationClasses="required"
                            disableFriday={false} />
                    </div>
                </section>
                <section>
                    <FormFieldsTitle title="Moving from?" />
                    <div className="row mb-1">
                        <div className="col-12 col-md-6 mb-3">
                            <SelectInput name="field_from" searchable={true} inputValue={selectedFromCountry} label="Select Country" options={countryOptions} onInputChange={this.handleCustomChange} />
                            <p key="hover1" data-tip="We currently do not offer car <br/>shipping services to some <br/>countries in Asia and Africa. We <br/>are working to get all countries <br/> listed very soon."
                                className="small d-inline-block mt-1 mobile-underline">Can't find the country you're looking for?</p>
                            <ReactTooltip html={true} place='bottom' />

                        </div>
                        <div className="col-12 col-md-6 mb-3">
                            <div className="floating-label ">
                                <PlacesAutocomplete
                                    value={from_address_value}
                                    onChange={value => this.handleInputChange('from_address', value)}
                                    onSelect={values => this.handleInputChange('from_address', values)}
                                    onError={() => this.onError}
                                    searchOptions={{  componentRestrictions: { country: fromCountryCode } }}
                                    label={'Address'} googleCallbackName="initPickup"
                                >
                                    {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                                        <div>
                                            <div className={"floating-label autofill " + (from_address_value || pickupFocused ? ' active' : '')} >
                                                <div className="form-item form-type-textfield form-item-field-international-address-from"
                                                    onClick={() => this.setPickupFocus(true)} onBlur={() => this.setPickupFocus(false)}
                                                >
                                                    <input
                                                        {...getInputProps({
                                                            className: invalidClassFrom,
                                                            placeholder: 'Enter a location',
                                                            id: 'host',
                                                            disabled: false
                                                        })}
                                                    /></div>
                                                <label for="">Address</label>
                                            </div>
                                            <div className={'pac-container pac-logo w-100' + (!!suggestions.length ? ' show' : 'show')}
                                                style={!!suggestions.length ? { display: 'block' } : { display: 'none' }}>
                                                {loading && <div>Loading...</div>}
                                                {suggestions.map(suggestion => {
                                                    const className = 'pac-item-query pac-matched';
                                                    // inline style for demonstration purpose
                                                    const style = suggestion.active
                                                        ? { backgroundColor: '#fafafa', cursor: 'pointer' }
                                                        : { backgroundColor: '#ffffff', cursor: 'pointer' };
                                                    return (
                                                        <div className='pac-item' >
                                                            <div
                                                                {...getSuggestionItemProps(suggestion, {
                                                                    className
                                                                })}
                                                                style={{ textOverflow: 'ellipsis', overflow: 'hidden' }}
                                                            >
                                                                <span className='pac-icon pac-icon-marker' />
                                                                <span>{suggestion.description}</span>
                                                            </div>
                                                        </div>
                                                    );
                                                })}
                                            </div>
                                        </div>
                                    )}
                                </PlacesAutocomplete>
                            </div>
                        </div>
                    </div>
                </section>
                <section>
                    <FormFieldsTitle title="Moving to:" />
                    <div className="row mb-1">
                        <div className="col-12 col-md-6 mb-3">
                            <SelectInput name="field_to" inputValue={selectedToCountry} label="Select Country" options={countryOptions} onInputChange={this.handleCustomChange}
                                searchable={true} />
                            <p key="hover1" data-tip="We currently do not offer car <br/>shipping services to some <br/>countries in Asia and Africa. We <br/>are working to get all countries <br/> listed very soon."
                                className="small d-inline-block mt-1 mobile-underline">Can't find the country you're looking for?</p>
                            <ReactTooltip html={true} place='bottom' />

                        </div>
                        <div className="col-12 col-md-6 mb-3">
                            <div className="floating-label ">
                                <PlacesAutocomplete
                                    label="Select Country"
                                    value={to_address_value}
                                    onChange={value => this.handleInputChange('to_address', value)}
                                    onSelect={values => this.handleInputChange('to_address', values)}
                                    onError={() => this.onError}
                                    searchOptions={{ componentRestrictions: { country: toCountryCode } }}
                                    googleCallbackName="initDropoff"
                                >
                                    {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                                        <div>
                                            <div className={"floating-label autofill " + (to_address_value || dropFocused ? ' active' : '')} >
                                                <div className="form-item form-type-textfield form-item-field-international-address-from"
                                                    onClick={() => this.setDropFocus(true)} onBlur={() => this.setDropFocus(false)}
                                                >
                                                    <input
                                                        {...getInputProps({
                                                            placeholder: 'Enter a location',
                                                            className: invalidClassTo,
                                                            id: 'destination',
                                                            disabled: false
                                                        })}


                                                    /></div>
                                                <label for="">Address</label>
                                            </div>
                                            <div className={'pac-container pac-logo w-100' + (!!suggestions.length ? ' show' : 'show')}
                                                style={!!suggestions.length ? { display: 'block' } : { display: 'none' }}>
                                                {loading && <div>Loading...</div>}
                                                {suggestions.map(suggestion => {
                                                    const className = 'pac-item-query pac-matched';
                                                    // inline style for demonstration purpose
                                                    const style = suggestion.active
                                                        ? { backgroundColor: '#fafafa', cursor: 'pointer' }
                                                        : { backgroundColor: '#ffffff', cursor: 'pointer' };
                                                    return (
                                                        <div className='pac-item' >
                                                            <div
                                                                {...getSuggestionItemProps(suggestion, {
                                                                    className
                                                                })}
                                                                style={{ textOverflow: 'ellipsis', overflow: 'hidden' }}
                                                            >
                                                                <span className='pac-icon pac-icon-marker' />
                                                                <span>{suggestion.description}</span>
                                                            </div>
                                                        </div>
                                                    );
                                                })}
                                            </div>
                                        </div>
                                    )}
                                </PlacesAutocomplete>
                            </div>
                        </div>
                    </div>
                </section>
                {cap_shipping_section}
                {cap_to_be_shipped_section}
                <section>
                    <FormFieldsTitle title="Other details you may want to add (optional)" />
                    <div className="row mb-4">
                        <div className="col mb-3">
                            <TextareaInput inputValue={other_comments_text_value} name="other_comments_text" placeholder="If you have any other requirements, feel free to describe them here in as much detail as you want. Or just leave us a message here to call you if its easier to explain on the phone" onInputChange={this.handleCustomChange} />
                        </div>
                    </div>
                </section>
                <section>
                    <QuotesNextStep moveNext={this.moveNext} howToProceed={this.howToProceed} formCurrentStep={this.props.formCurrentStep} submitData={this.submitData} />
                </section>
            </div>
        )
    }

    showModal() {
        return (
            <div>"We currently do not offer car shipping services to some countries in Asia and Africa. We are working to get all countries listed very soon."></div>
        )
    }

    showOrHideModalSelectCompanyError(boolVal) {
        const { showOrHideModalSelectCompanyError } = this.props;
        showOrHideModalSelectCompanyError(boolVal);
    }

    selectCompanies() {
        var currentStepClass = this.currentStep() === 2 ? '' : "d-none";
        const serviceID = allServiceConstant.SERVICE_CAR_SHIPPING;
        const currentCity = this.props.current_city;
        const { selectedCompanies } = this.state;
        const { showSelectCompanyError } = this.props;
        return (
            <div className={currentStepClass}>
                <SelectCompanyWithFilters serviceID={serviceID} currentCity={currentCity} selectChange={this.selectCompanyChange} selectedCompanies={selectedCompanies} showSelectCompanyError={showSelectCompanyError} showOrHideModalSelectCompanyError={this.showOrHideModalSelectCompanyError} maxSelection={3} />
                <section>
                    <QuotesNextStep moveNext={this.moveNext} howToProceed={this.howToProceed} formCurrentStep={this.props.formCurrentStep} submitData={this.submitData} />
                </section>
            </div>
        )
    }
    ContactDetails(isLocationDetailShow) {
        var countryOptions = [{ id: 1, value: "1", label: "Dubai" }];
        var currentStepClass = this.currentStep() === 3 ? '' : "d-none";
        //var currentStepClass =  ''
        var isLocationDetailShow = typeof isLocationDetailShow != "undefined" ? isLocationDetailShow : "yes";


        var contact_details = {
            input_email: this.state.input_email,
            input_phone: this.state.input_phone,
            input_name: this.state.input_name,
            input_last_name: this.state.input_last_name
        }
        let userProfile = this.props.userProfile;
        if (!this.state.loader) {
            return (
                <div id="personal-information-form" className={currentStepClass}>
                    <ContactDetailsStep
                        countryOptions={countryOptions}
                        isLocationDetailShow={isLocationDetailShow}
                        contact_details={contact_details}
                        handleInputChange={this.handleInputChange}
                        isSkipCompany={this.props.isSkipCompany}
                        userProfile={userProfile}
                        signInDetails={this.props.signInDetails}
                        handleDropDownChange={this.handleInputChange}
                        mobileSummary={this.mobileSummary}
                        moveNext={this.moveNext}
                        formCurrentStep={this.props.formCurrentStep}
                        submitData={this.submitData}
                        currentCity={this.props.current_city}
                    />
                    <section>
                        <QuotesNextStep moveNext={this.moveNext} howToProceed={this.howToProceed} formCurrentStep={this.props.formCurrentStep} submitData={this.submitData} />
                    </section>
                </div>
            )
        } else {
            return (
                <main loader={this.state.loader ? "show" : "hide"}><Loader /></main>
            );
        }
    }

    render() {
        const { selectedCompanies } = this.state;
        const items = this.sumarryItems();
        const showPromo = false;
        const showPrice = false;
        var typeOfJourney = this.props.typeOfJourney();
        var isLoading = this.state.loader || this.props.loader;

        return (
            <React.Fragment>
                <div className="col-lg-8">
                    {
                        isLoading ? <main loader={isLoading ? "show" : "hide"}><Loader /></main> :
                            (<React.Fragment>
                                {this.carShippingRequest()}
                                {typeOfJourney == 1 && this.selectCompanies()}
                                {this.ContactDetails("no")}
                            </React.Fragment>)
                    }
                </div>
                <div className="col-md-3 ml-auto d-lg-block d-none">
                    <BookingSummary
                        typeOfFlow="quotes"
                        items={items}
                        showPromo={showPromo}
                        showPrice={showPrice}
                        selectChange={this.selectCompanyChange}
                        selectedCompanies={selectedCompanies}
                        formCurrentStep={this.props.formCurrentStep}
                        typeOfJourney={typeOfJourney} />
                </div>
            </React.Fragment>
        )
    }
}

export default QuotesCarShippingPage;