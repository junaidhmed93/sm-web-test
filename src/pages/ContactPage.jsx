import React from "react";
import ContactMap from "../components/ContactMap";
import FormFieldsTitle from "../components/FormFieldsTitle";
import SocialMedia from "../components/SocialMedia";
import ContactForm from "../components/ContactForm";
import DefaultBanner from "../components/DefaultBanner";
import commonHelper from "../helpers/commonHelper";
import locationHelper from '../helpers/locationHelper';

class ContactPage extends React.Component{
    constructor(props) {
        super(props);
        this.updateMetaData = this.updateMetaData.bind(this);
        //this.locationHelperInit = locationHelper.init.bind(this);
        //this.locationHelperInit();
    }
    componentDidMount(){
        this.updateMetaData();
    }
    render() {
        return (
                <main>
                    <DefaultBanner
                        title={locationHelper.translate('CONTACT_US_TITLE')}
                        background={'https://aratech-blog.servicemarket.com/wp-content/uploads/2018/07/local-move.jpg'}
                    />
                    <section className="py-5 bg-white">
                        <div className="container">
                            <FormFieldsTitle title={locationHelper.translate('CONTACT_US_SEND_MSG')} />
                            <ContactForm />
                            <FormFieldsTitle title={locationHelper.translate('GET_IN_TOUCH_TXT1')} />
                            <SocialMedia />
                            <FormFieldsTitle title={locationHelper.translate('GET_IN_TOUCH_TXT2')} />
                            <ContactMap />
                        </div>
                    </section>
                </main>
        );
    }
    updateMetaData() {

        var seoText = commonHelper.getSeoContactUsTextArray();
        // console.log('updating meta data');
        document.title = seoText.seoTitle;
        var allMetaElements = document.getElementsByTagName('meta');
        for (var i=0; i<allMetaElements.length; i++) {
            if (allMetaElements[i].getAttribute("name") == "description") {
                allMetaElements[i].setAttribute('content', seoText.seoDesc);
                break;
            }
        }
    }
}

export default ContactPage;
