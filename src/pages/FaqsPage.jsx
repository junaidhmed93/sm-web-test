import React from "react";
import DefaultBanner from "../components/DefaultBanner";
import Accordion from "../components/Accordion";
import Loader from "../components/Loader";
import SectionTitle from "../components/SectionTitle";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {fetchFaqsData, showLoader, hideLoader} from "../actions/index";
import { Link } from "react-router-dom";
import locationHelper from "../helpers/locationHelper";

class FaqsPage extends React.Component{
    _isMounted = false;
    constructor(props) {
        super(props);
        this.updateMetaData = this.updateMetaData.bind(this);
        this.locationHelperInit = locationHelper.init.bind(this);
        this.locationHelperInit();
    }
    createMarkup(htmlString) {
        return {__html: htmlString};
    }
    componentDidMount(){
        this._isMounted = true;
        if ( !this.props.faqsData.acf ) {
            this.props.showLoader();
            this.props.fetchFaqsData(this.props.lang)
            .then(()=> {
                if (this._isMounted) {
                    this.props.hideLoader();
                    this.updateMetaData();
                }
            });
        } else {
            this.updateMetaData();
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }
    updateMetaData() {
        // console.log('updating meta data');
        document.title = this.props.faqsData.acf.meta_title ? this.props.faqsData.acf.meta_title : 'ServiceMarket';
        var allMetaElements = document.getElementsByTagName('meta');
        for (var i=0; i<allMetaElements.length; i++) {
            if (allMetaElements[i].getAttribute("name") == "description") {
                allMetaElements[i].setAttribute('content', this.props.faqsData.acf.meta_description ? this.props.faqsData.acf.meta_description : 'ServiceMarket');
                break;
            }
        }
    }
    render() {
        const {faqsData, loader} = this.props;
        if(faqsData.acf && !loader){
        return (
            <main>
                <DefaultBanner title={faqsData.acf.banner_title} background={faqsData.acf.banner_image}/>
                <section className="py-5 bg-white">
                    <div className="container">
                        {faqsData.acf.banner_description.length ? ( <p dangerouslySetInnerHTML={this.createMarkup(faqsData.acf.banner_description)}></p> ) : ''}
                        <div className="row mb-5">
                            <div className="col px-5 px-md-3">
                            <SectionTitle title={locationHelper.translate('ABOUT_SERVICEMARKET')}/>
                            {faqsData.acf.faqs_category1 && <Accordion panels={faqsData.acf.faqs_category1} parentClass="mb-5" />}
                            <SectionTitle title={locationHelper.translate('ABOUT_OUR_SERVICES')}/>
                            {faqsData.acf.faqs_category2 && <Accordion panels={faqsData.acf.faqs_category2} parentClass="mb-5" />}
                            <SectionTitle title={locationHelper.translate('ABOUT_OUR_COMPANY_LISTINGS')}/>
                            {faqsData.acf.faqs_category3 && <Accordion panels={faqsData.acf.faqs_category3} parentClass="mb-5" />}
                            <SectionTitle title={locationHelper.translate('ABOUT_OUR_PRIVACY_POLICY')}/>
                            {faqsData.acf.faqs_category4 && <Accordion panels={faqsData.acf.faqs_category4} parentClass="mb-5" />}
                            
                            </div>
                        </div>
                        <div className="row">
                            <div className="col text-center">
                                <p className="h3 mb-4">{locationHelper.translate('HAVE_A_QUESTION_THAT_WAS_NOT_ANSWERED_HERE')}</p>
                                <div className="row">
                                    <div className="col col-sm-6 col-md-4 mx-auto">
                                        <Link to="/en/contact-us" className="btn font-weight-bold text-uppercase btn-warning btn-lg px-5 mb-3">{locationHelper.translate('GET_IN_TOUCH')}</Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </main>
        );
        }else{
            return ( <main loader={loader ? "show": "hide"}><Loader/></main>);
        }
    }
}

function mapStateToProps(state){
	return {
        loader: state.loader,
        faqsData: state.faqsData,
        CurrentCity: state.CurrentCity,
        lang: state.lang,
        currentCity: state.currentCity,
	}
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({fetchFaqsData, showLoader, hideLoader}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(FaqsPage);
