import React from "react";
import DefaultBanner from "../components/DefaultBanner";
import SectionTitle from "../components/SectionTitle";
import TitleTextBlock from "../components/TitleTextBlock";
import Loader from "../components/Loader";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {fetchDisclaimerData, showLoader, hideLoader} from "../actions/index";

class DisclaimerPage extends React.Component{
    _isMounted = false;
    constructor(props) {
        super(props);
        this.updateMetaData = this.updateMetaData.bind(this);
    }

    createMarkup(htmlString) {
        return {__html: htmlString};
    }
    componentDidMount(){
        this._isMounted = true;
        if ( !this.props.disclaimerData.acf ) {
            this.props.showLoader();
            this.props.fetchDisclaimerData(this.props.lang)
            .then(()=> {
                if (this._isMounted) {
                    this.updateMetaData();
                    this.props.hideLoader();
                }
            });
        } else {
            this.updateMetaData();
        }
    }
    updateMetaData() {
        // console.log('updating meta data');
        document.title = this.props.disclaimerData.acf.meta_title ? this.props.disclaimerData.acf.meta_title : 'ServiceMarket';
        var allMetaElements = document.getElementsByTagName('meta');
        for (var i=0; i<allMetaElements.length; i++) {
            if (allMetaElements[i].getAttribute("name") == "description") {
                allMetaElements[i].setAttribute('content', this.props.disclaimerData.acf.meta_description ? this.props.disclaimerData.acf.meta_description : 'ServiceMarket');
                break;
            }
        }
    }
    componentWillUnmount() {
        this._isMounted = false;
    }
    render() {
        const {disclaimerData, loader} = this.props;
        if(disclaimerData.acf && !loader){
        return (
            <main>
                <DefaultBanner title={disclaimerData.acf.banner_title} background={disclaimerData.acf.banner_image}/>
                <section className="py-5 bg-white">
                    <div className="container">
                        {disclaimerData.acf.banner_description.length ? ( <p dangerouslySetInnerHTML={this.createMarkup(disclaimerData.acf.banner_description)}></p> ) : ''}
                        <SectionTitle title={disclaimerData.title.rendered} />
                        <TitleTextBlock text={disclaimerData.acf.general_content}/>
                    </div>
                </section>
            </main>
        );
        }else{
            return ( <main loader={loader ? "show": "hide"}><Loader/></main>);
        }
    }
}

function mapStateToProps(state){
	return {
        loader: state.loader,
        disclaimerData: state.disclaimerData,
        CurrentCity: state.CurrentCity,
        lang : state.lang,
	}
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({fetchDisclaimerData, showLoader, hideLoader}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(DisclaimerPage);
