import React from "react";
import DefaultBanner from "../components/DefaultBanner";
import IconTextGrid from "../components/IconTextGrid";
import TitleTextBlock from "../components/TitleTextBlock";
import SectionTitle from "../components/SectionTitle";
import Loader from "../components/Loader";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {fetchAboutData, showLoader, hideLoader} from "../actions/index";
import locationHelper from "../helpers/locationHelper";


class AboutPage extends React.Component{
    _isMounted = false;
    constructor(props) {
        super(props);
        this.updateMetaData = this.updateMetaData.bind(this);
    }
    createMarkup(htmlString) {
        return {__html: htmlString};
    }
    componentDidMount(){
        this._isMounted = true;
        if ( !this.props.aboutData.acf ) {
            this.props.showLoader();
            this.props.fetchAboutData(this.props.lang)
            .then(()=> {
                if (this._isMounted) {
                    this.updateMetaData();
                    this.props.hideLoader();
                }
            });
        } else {
            this.updateMetaData();
        }
    }
    componentWillUnmount() {
        this._isMounted = false;
    }
    updateMetaData() {
        // console.log('updating meta data');
        document.title = this.props.aboutData.acf.seo_meta_title ? this.props.aboutData.acf.seo_meta_title : 'ServiceMarket';
        var allMetaElements = document.getElementsByTagName('meta');
        for (var i=0; i<allMetaElements.length; i++) {
            if (allMetaElements[i].getAttribute("name") == "description") {
                allMetaElements[i].setAttribute('content', this.props.aboutData.acf.seo_meta_description ? this.props.aboutData.acf.seo_meta_description : 'ServiceMarket');
                break;
            }
        }
    }

    render() {
        const {aboutData, loader, CurrentCity} = this.props;
        const hasModal=true;
        if(aboutData.acf && !loader){
        return (
            <main loader={loader ? "show": "hide"}>
                <DefaultBanner title={aboutData.acf.banner_title} background={aboutData.acf.banner_image}/>
                <section className="py-5 bg-white">
                    <div className="container">
                        {aboutData.acf.banner_description.length ? ( <p dangerouslySetInnerHTML={this.createMarkup(aboutData.acf.banner_description)}></p> ) : ''}
                        <SectionTitle title={locationHelper.translate('OUR_SERVICES')}/>
                        <IconTextGrid data={aboutData.acf.our_services}  hasModal={hasModal} />
                        <TitleTextBlock title={locationHelper.translate('WHAT_WE_DO')} text={aboutData.acf.what_we_do}/>
                        <TitleTextBlock title={locationHelper.translate('WHY_WE_DO_IT')} text={aboutData.acf.why_we_do_it}/>
                        <SectionTitle title={locationHelper.translate('OUR_MARKETS')}/>
                        <IconTextGrid data={aboutData.acf.our_market} title={locationHelper.translate('OUR_MARKETS')} />
                    </div>
                </section>
            </main>
        );
        }else{
            return ( <main loader={loader ? "show": "hide"}><Loader/></main>);
        }
    }
}

function mapStateToProps(state){
	return {
        loader: state.loader,
        aboutData: state.aboutData,
        CurrentCity: state.CurrentCity,
        lang : state.lang,
	}
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({fetchAboutData, showLoader, hideLoader}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(AboutPage);
