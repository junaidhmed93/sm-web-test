import React from "react";
import FormFieldsTitle from "../components/FormFieldsTitle";
import BookingSummary from "../components/Form_Fields/BookingSummary";
import Loader from "../components/Loader";
import {connect} from "react-redux";
import {
    fetchCitiesAreas, fetchLandingData,
    fetchUserProfile,
    getAllCreditCard, isValidSection,
    registerCreditCard, scrollToTop, setBodyClass,
    setSignIn,
    toggleLoginModal,
    URLCONSTANT
} from "../actions";
import {bindActionCreators} from "redux";
import DefaultBanner from "../components/DefaultBanner";
import SectionTitle from "../components/SectionTitle";
import TitleTextBlock from "../components/TitleTextBlock";
import {withCookies} from "react-cookie";
import SelectCompanyWithFilters from "../components/SelectCompanyWithFilters";
import commonHelper from "../helpers/commonHelper";
let allServiceConstant = {};
let URLConstant = {};
let current_city = "dubai";
class ServiceReviewsPage extends React.Component{

    constructor(props) {
        super();
        
        this.state = {
            service: props.defaultServiceOption,
            details:"",
            selectedCompanies: [],
            loader: false,
            selectedServiceName: '',
            selectedServiceID: '',
            cuisine: [],
            loadingLandingData: false
        }
        this.handleCustomChange = this.handleCustomChange.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.selectCompanyChange = this.selectCompanyChange.bind(this);
        this.showOrHideModalSelectCompanyError = this.showOrHideModalSelectCompanyError.bind(this);
        this.saveCookies = this.saveCookies.bind(this);
        this.moveNext = this.moveNext.bind(this);
        this.updateMetaData = this.updateMetaData.bind(this);

    }
    updateMetaData(serviceID = 0) {
        serviceID = serviceID != 0 ? serviceID : this.state.selectedServiceID;
        if( typeof(serviceID) != 'undefined' ){
            var seoText = commonHelper.getSeoJourney2TextArray(serviceID);
            var seoTitle = Object.keys(seoText).length ? commonHelper.replaceCodedCurrentCityName(seoText.seoTitle, this.props.currentCity) : "";
            var seoDesc = Object.keys(seoText).length ? commonHelper.replaceCodedCurrentCityName(seoText.seoDescription, this.props.currentCity) : "";

            document.title = seoTitle;
            var allMetaElements = document.getElementsByTagName('meta');
            for (var i=0; i<allMetaElements.length; i++) {
                if (allMetaElements[i].getAttribute("name") == "description") {
                    allMetaElements[i].setAttribute('content', seoDesc);
                    break;
                }
            }

        }

    }
    createMarkup(htmlString) {
        return {__html: htmlString};
    }
    componentDidUpdate(prevProps) {
        //console.log("component Did Update");
        const prevCity = prevProps.match.params.city;
        const currentCity = this.props.match.params.city;
        const prevSlug = prevProps.match.params.slug;
        const currentSlug = this.props.match.params.slug;
        if ((prevSlug != currentSlug)) {
            this.setState({loadingLandingData: true});
            this.props.fetchLandingData(this.props.match.params.city, this.props.match.params.slug, [], this.props.lang)
                .then(()=> {
                    if (this._isMounted) {
                        this.setState({loadingLandingData: false});
                        if (this.props.landingData.length == 0) {
                            this.props.history.push('/' + this.props.match.params.lang + "/" + this.props.match.params.city);
                        } else {
                            this.updateMetaData();
                        }
                    }
                });
            var serviceID = commonHelper.getServiceIDBySlug(URLCONSTANT, this.props.service_constants, this.props.match.params.slug);
            this.setSelectedServiceName(serviceID);

            this.updateMetaData(serviceID);
        }
    }
    componentWillMount() {
        allServiceConstant = this.props.service_constants;
        URLConstant =  URLCONSTANT;
        current_city = this.props.current_city;

        this.setSelectedServiceName(commonHelper.getServiceIDBySlug(URLConstant, allServiceConstant, this.props.match.params.slug));
    }
    componentDidMount() {
        if ( window.REDUX_DATA.landingData.length <= 0 ) {
            // this.props.showLoader();
            // Check if the Shared data between pages already requested, if exist we don't need to fetch them again
            const extraData = [];
            if(!this.props.why_sm.acf) extraData.push('whySMData');
            if(!this.props.how_iw.acf) extraData.push('howIWData');
            if(!this.props.logos.acf) extraData.push('logosData');
            this.setState({loadingLandingData: true});
            this.props.fetchLandingData(this.props.match.params.city, this.props.match.params.slug, extraData, this.props.lang)
                .then(()=> {
                    if (this._isMounted) {
                        this.setState({loadingLandingData: false});
                        if (this.props.landingData.length == 0) {
                            this.props.history.push('/' + this.props.match.params.lang + "/" + this.props.match.params.city);
                        } else {
                            this.updateMetaData();
                        }
                    }
                });
        }
        this.props.setBodyClass('service_reviews');
        this.updateMetaData();
    }
    componentWillUnmount(){
        window.REDUX_DATA.landingData = [];
        this.props.setBodyClass('default');
        this._isMounted = false;
    }
    setSelectedServiceName(serviceID) {
        //console.log(serviceID);
        var loadedService = (this.props.allServices.parentServices.find((item) => {
            return item.id === serviceID
        }));
        this.setState({
            selectedServiceID: serviceID,
            selectedServiceName: loadedService.name.charAt(0) + loadedService.name.toLowerCase().slice(1)
        })

    }
    summaryItems(){
        var items = [{id: 1, label: 'City', value: commonHelper.toTitleCase(this.props.currentCity.replace('-',' '))}, {id: 2, label: 'Service type', value: this.state.selectedServiceName}];
        var cateringCuisines = [];
        const serviceID = this.state.selectedServiceID;
        const cuisine = this.state.cuisine;
        if((serviceID==this.props.service_constants.SERVICE_CATERING) && cuisine.length){
            cuisine.map((item) => {
                cateringCuisines.push(item.label);
            })
            items.push({label: 'Cuisines', value: cateringCuisines.join(", ")})
        }
        return items;
    }
    selectCompanyChange(value){
        this.setState({
            selectedCompanies: value
        });
    }
    handleCustomChange(name, value){
        this.setState({
            [name]: value
        });
    }
    handleInputChange(name, value) {
        this.setState({
            [name]: value
        });
    }
    showOrHideModalSelectCompanyError(boolVal) {
        this.setState({
            showSelectCompanyError: boolVal
        })
    }
    selectCompanies(){
        
        var currentStepClass = '';

        var {service} = this.state;

        const serviceID = this.state.selectedServiceID;

        const filterServiceID = 0;

        const currentCity = this.props.current_city;

        const {selectedCompanies} = this.state;

        const showCuisine = (serviceID == this.props.service_constants.SERVICE_CATERING);
        const {showSelectCompanyError} = this.props;
        if( typeof(serviceID) != 'undefined' ){
            return (
                <div id="section-company-selector" className={currentStepClass}>
                    <SelectCompanyWithFilters
                        filterService = {filterServiceID}
                        serviceID={serviceID}
                        subServiceID={this.props.journey3SubserviceDataReducer}
                        currentCity={currentCity}
                        maxSelection={3}
                        showBack={false}
                        history={this.props.history}
                        match={this.props.match}
                        selectChange={this.selectCompanyChange}
                        selectedCompanies={selectedCompanies}
                        showCuisineFilter={showCuisine}
                        cuisineFilterValue = {this.state.cuisine}
                        cuisineFilterChange = {this.handleCustomChange}
                        showSelectCompanyError = {this.state.showSelectCompanyError}
                        showOrHideModalSelectCompanyError={this.showOrHideModalSelectCompanyError}
                        showIn = "reviews"
                        showServiceReviewsFilters = {true}
                    />
                </div>
            )
        }
    }
    saveCookies(name, data){
        var serviceURL = this.props.match.params.slug;
        var currentLanguage = "en";
        var cookieName = currentLanguage + "." + this.props.currentCity + "." + serviceURL + "." + name;
        const { cookies } = this.props;
        cookies.set(cookieName, JSON.stringify(data), {path: '/', maxAge:300});
    }
    moveNext() {
        //console.log("this.props.journey3SubserviceDataReducer");
        //console.log(this.props.journey3SubserviceDataReducer);
        var cookieSelectedCompanies = [];

        var {selectedCompanies} = this.state;

        if (selectedCompanies.length > 0 && this.state.selectedServiceID) {
            this.showOrHideModalSelectCompanyError(false);
            selectedCompanies.map((item, index) => {
                var selectedCompany = {id:item.id, name:item.name};
                cookieSelectedCompanies.push(selectedCompany);
            }); // for to avoid the large cookie issue.
            var cuisineItems = this.state.cuisine;

            var cookieData = {
                'selected_companies': cookieSelectedCompanies, 
                'selectedService':{'id':this.state.selectedServiceID,
                'name':this.state.selectedServiceName}, 
                'selectedSubService':this.props.journey3SubserviceDataReducer
            };
            // console.log("cookieData", cookieData);
            // console.log("cuisineItems", cuisineItems);

            if(cuisineItems.length){
                cookieData['cuisine'] = cuisineItems
            }
            this.saveCookies('quotes_data', cookieData);

            this.props.history.push("/en/"+ this.props.currentCity + "/" + this.props.match.params.slug + '/journey2/step2');
        } else {
            this.showOrHideModalSelectCompanyError(true);
            scrollToTop();
        }
    }
    render(){
        const serviceID = this.state.selectedServiceID;
        var seoText = {};
        var headerText = "";
        var headerDesc = "";

        if( typeof(serviceID) != 'undefined' ){
            seoText = commonHelper.getSeoFilterTextArray(serviceID, '', this.props.service_constants);
            headerText = Object.keys(seoText).length ? commonHelper.replaceCodedCurrentCityName(seoText.header, this.props.currentCity) : "";
            headerDesc = Object.keys(seoText).length ? commonHelper.replaceCodedCurrentCityName(seoText.seoTitle, this.props.currentCity) : "";
        }

        const partners = this.state.selectedCompanies;
        const QuotesText = partners && partners.length>1 ?'Get your free quotes':'Get your free quote';
        return (
            <main>
                {/*TODO remove the static background in ProviderBanner*/}
                <DefaultBanner
                    title={headerText}
                    background={this.props.landingData.length && !this.props.loadingLandingData?this.props.landingData[0].acf.banner_image:''}
                />
                <div className="container pt-3">
                    {/*{headerDesc.length ? ( <p dangerouslySetInnerHTML={this.createMarkup(headerDesc)}></p> ) : ''}*/}

                    { /*<SectionTitle title={"Please select up to 3 landscaping companies in " + this.props.currentCity.split("-").join(" ")} />
                    <TitleTextBlock text={"Please select up to 3 landscaping companies in " + this.props.currentCity.split("-").join(" ") + " that you would like to receive custom quotes from, and the companies will contact you shortly"}/> */}
                    <section>
                        <div className="row">
                            {this.renderContent()}
                        </div>
                    </section>
                    <section>
                        <footer className={"footer-buttons fixed-bottom container-fluid bg-white shadow-1"}>
                            <div className="row btn-fixed">
                                <div className="col">
                                    <div className="row">
                                        <div className="col-12 px-0 d-xs-flex text-center">
                                            {
                                                (this.state.selectedServiceID != this.props.service_constants.SERVICE_HOME_INSURANCE && this.state.selectedServiceID != this.props.service_constants.SERVICE_CAR_INSURANCE) ? (
                                                <button className="login-step-button orange-btn-content btn btn-inline-block btn-warning py-2 text-uppercase font-weight-bold" onClick={()=> this.moveNext()}>
                                                    <span className="text-uppercase">Get your free quotes</span>
                                                </button>
                                                ):(
                                                <a className="login-step-button orange-btn-content btn btn-inline-block btn-warning py-2 text-uppercase font-weight-bold" href={commonHelper.getHomeCarInsuranceLinks(this.state.selectedServiceID, this.props.service_constants)}>
                                                    <span className="text-uppercase">Get your free quotes</span>
                                                </a>
                                                )
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </footer>
                    </section>
                </div>
            </main>
        );
    }
    renderContent() {

        const {selectedCompanies} = this.state;
        const items = this.summaryItems();
        const showPromo = false;
        const showPrice = false;


        var isLoading =  this.state.loader || this.props.loader;

        var bookingSummary = <BookingSummary typeOfFlow="quotes" typeOfJourney={2}
                                             selectChange={this.selectCompanyChange} items={items} showPromo={showPromo} showPrice={showPrice} selectedCompanies={selectedCompanies}/>

        if(isLoading) {
            return (
                <React.Fragment>
                    <div className="col-lg-8">
                        <main loader={ isLoading ? "show" : "hide"}><Loader/></main>
                    </div>
                    <div className="col-md-3 ml-auto">
                        {bookingSummary}
                    </div>
                </React.Fragment>
            );

        }else {
            return (
                <React.Fragment>
                    <div className="col-lg-8">
                        {this.selectCompanies()}
                    </div>
                    <div className="col-md-3 ml-auto d-lg-block d-none">
                        {bookingSummary}
                    </div>
                </React.Fragment>
            )
        }
    }
}

function mapStateToProps(state){
    return {
        service_constants: state.serviceConstants,
        dataConstants: state.dataConstants,
        signInDetails: state.signInDetails,
        userProfile: state.userProfile,
        citiesAreas: state.citiesAreas,
        showLoginMenu: state.showLoginMenu,
        myCreditCardsData: state.myCreditCardsData,
        currentCity: state.currentCity,
        allServices: state.allServices,
        landingData: state.landingData,
        why_sm: state.why_sm,
        how_iw: state.how_iw,
        logos: state.logos,
        journey3SubserviceDataReducer: state.journey3SubserviceDataReducer
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({fetchLandingData, toggleLoginModal, fetchCitiesAreas, fetchUserProfile, setSignIn, getAllCreditCard, registerCreditCard, setBodyClass}, dispatch);
}


export default withCookies(connect(mapStateToProps, mapDispatchToProps)(ServiceReviewsPage));