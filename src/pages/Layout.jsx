import React from "react";
import { Switch, Route, Link } from "react-router-dom";
import Routes from "../Routes";
import { matchPath } from 'react-router';
import Loader from "../components/Loader";
import Loadable from 'react-loadable';
const loading = () => (<Loader />);
const PopupMenu = Loadable({
    loader: () => import('../components/PopupMenu'),
    loading
});
const PopupLogin = Loadable({
    loader: () => import('../components/PopupLogin'),
    loading
});

import Header from "../components/Header";
import Footer from "../components/Footer";
/*import HomePage from "./HomePage";
import LandingPage from "./LandingPage";
import AboutPage from "./AboutPage";
import BookMovingPage from "./BookMovingPage";
import ContactPage from "./ContactPage";
import PartnerPage from "./PartnerPage";
import ReviewPage from "./ReviewPage";
import TeamPage from "./TeamPage";
import FaqsPage from "./FaqsPage";
import CompaniesPage from "./CompaniesPage";*/
import { withRouter } from 'react-router';
import { scrollToTop, JOURNEY1, JOURNEY2, JOURNEY3,URLCONSTANT } from "../actions/index";

import fonts from '../../dist/css/fonts.css';
/*import bootstrap from '../../dist/css/bootstrap.css';*/
/*import bootstrapRtl from '../../dist/css/bootstrap-rtl.css';*/
import slick from '../../dist/css/slick.min.css';
import slickTheme from '../../dist/css/slick-theme.min.css';
import datepicker from '../../dist/css/datepicker.css';
import fontawesome from '../../dist/css/fontawesome-all.min.css';
import customCss from '../../dist/css/custom.css';
import installPopup from '../../dist/css/install-popup.css';
import phoneSelect from '../../dist/css/phone.css';
import reactIntlTel from '../../dist/css/react-intl-tel-input.css';
import {connect} from "react-redux";
import PopupForgetPassword from "../components/PopupForgetPassword";
import PopupFeedback from "../components/PopupFeedback";


class Layout extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			pathname: ''
		}
		this.isJourneyPage = this.isJourneyPage.bind(this);
		this.isZohoPage = this.isZohoPage.bind(this);
		this.isBookingPage = this.isBookingPage.bind(this);
		this.isBookingAvailable = this.isBookingAvailable.bind(this);
		this.defaultService = this.defaultService.bind(this);
		this.isRequestable = this.isRequestable.bind(this);
		this.getServiceIdByURL = this.getServiceIdByURL.bind(this);
		this.isProfilePage = this.isProfilePage.bind(this);
		this.isConfirmationPage = this.isConfirmationPage.bind(this);

	}
	isConfirmationPage(pathname) {
		var pathname = typeof pathname != "undefined" ? pathname : this.state.pathname;
		var pathArray = pathname.split('/');
		if (pathArray.includes('confirmation')){
			return true;
		}
		return false;
	}
	isBookingPage(pathname) {
		var pathname = typeof pathname != "undefined" ? pathname : this.state.pathname;
		var pathArray = pathname.split('/');
		if (pathArray.includes('book-online')){
			return true;
		}
		return false;
	}
	isProfilePage(pathname) {
		var pathname = typeof pathname != "undefined" ? pathname : this.state.pathname;
		
		var pathArray = pathname.split('/');

		if (pathArray.includes('profile') || pathArray.includes('reviews') ) {
			return true;
		} else {
			return false;
		}
	}
	isJourneyPage(pathname) {
		var pathname = typeof pathname != "undefined" ? pathname : this.state.pathname;
		
		var pathArray = pathname.split('/');

		if (pathArray.includes('book-online') || pathArray.includes(JOURNEY1) || pathArray.includes(JOURNEY2) || pathArray.includes(JOURNEY3)) {
			return true;
		} else {
			return false;
		}
	}
	isZohoPage() {
		var pathname = this.state.pathname;
		var pathArray = pathname.split('/');
		if (pathArray.includes('zohosm') || pathArray.includes('book-direct')) {
			return true;
		} else {
			return false;
		}
	}
	isReviewPage() {
		var pathname = this.state.pathname;
		var pathArray = pathname.split('/');
		if (pathArray.includes('maid-review') || pathArray.includes('rate-your-service') || pathArray.includes('service-review')) {
			return true;
		} else {
			return false;
		}
	}
	componentDidMount() {
		var pathname = window.location.pathname;
		this.setState({
			pathname: pathname
		});
		this.isJourneyPage();
		this.isZohoPage();
	}
	componentDidUpdate(prevProps) {
		if (this.props.location.pathname !== prevProps.location.pathname) {
			scrollToTop();
			var pathname = window.location.pathname;
			this.setState({
				pathname: pathname
			});
			this.isJourneyPage()
		}
	}
	defaultService(serviceURI){
		var {serviceConstants} = this.props;
		var services = {};

		services[URLCONSTANT.PAINTERS_PAGE_URI] = [
			serviceConstants.SERVICE_PAINTING, 
			serviceConstants.SERVICE_HOME_PAINTING
		];

		services[URLCONSTANT.CLEANING_MAID_PAGE_URI] = serviceConstants.SERVICE_HOME_CLEANING;

		services[URLCONSTANT.WINDOW_CLEANING_PAGE_URI] = serviceConstants.SERVICE_WINDOW_CLEANING;

		services[URLCONSTANT.POOL_CLEANING_PAGE_URI] = serviceConstants.SERVICE_POOL_CLEANING_SERVICE;

		services[URLCONSTANT.DEEP_CLEANING_PAGE_URI] = serviceConstants.SERVICE_DEEP_CLEANING_SERVICE;

		services[URLCONSTANT.MATTRESS_CLEANING_PAGE_URI] = serviceConstants.SERVICE_MATTRESS_CLEANING;

		services[URLCONSTANT.SOFA_AND_UPHOLSTERY_CLEANING_PAGE_URI] = serviceConstants.SERVICE_SOFA_CLEANING;
		
		services[URLCONSTANT.CARPET_CLEANING_PAGE_URI] = serviceConstants.SERVICE_CARPET_CLEANING;

		services[URLCONSTANT.WATER_TANK_CLEANING_PAGE_URI] = serviceConstants.SERVICE_WATER_TANK_CLEANING;

		services[URLCONSTANT.PEST_CONTROL_PAGE_URI] = [
													serviceConstants.SERVICE_PEST_CONTROL,
													serviceConstants.SERVICE_COCKROACHES_PEST_CONTROL,
													serviceConstants.SERVICE_BED_BUGS_PEST_CONTROL,
													serviceConstants.SERVICE_MOVE_IN_PEST_CONTROL
												];

		services[URLCONSTANT.MAINTENANCE_PAGE_URI] = [
			serviceConstants.SERVICE_TV_MOUNTING,
            serviceConstants.SERVICE_FURNITURE_ASSEMBLY,
           	serviceConstants.SERVICE_CURTAIN_HANGING,
            serviceConstants.SERVICE_LIGHTBULBS_AND_LIGHTING,
            serviceConstants.SERVICE_HANDYMAN_SERVICE
		];

		services[URLCONSTANT.NOON_TV_INSTALLATION_PAGE_URI] = [
			serviceConstants.SERVICE_TV_MOUNTING,
			serviceConstants.SERVICE_HANDYMAN_SERVICE
		];

		services[URLCONSTANT.ELECTRICIAN_PAGE_URI] = [
			serviceConstants.SERVICE_ELECTRICIAN,
			serviceConstants.SERVICE_LIGHT_FIXTURES,
			serviceConstants.SERVICE_INDOOR_AND_OUTDOOR_WIRING,
			serviceConstants.SERVICE_OUTLETS_AND_SWITCHES,
			serviceConstants.SERVICE_ELECTRICAL_INSTALLATIONS_AND_REPAIRS
		]

		services[URLCONSTANT.PLUMBING_PAGE_URI] = [serviceConstants.SERVICE_PLUMBING];

		services[URLCONSTANT.AC_MAINTENANCE_PAGE_URI] = [
			serviceConstants.SERVICE_AC_CLEANING,
            serviceConstants.SERVICE_AC_REPAIR,
            serviceConstants.SERVICE_AC_INSTALLATION,
			serviceConstants.SERVICE_AC_AND_HEATING,
		]

		return services[serviceURI];
		
	}
	isBookingAvailable(serviceIds){
        var arr = typeof this.props.bookingServices.bookingServices != "undefined" ? this.props.bookingServices.bookingServices : [];
		if(typeof serviceIds == "object"){
			return arr.some( services => serviceIds.indexOf(services) !== -1)
		}else{
			return ( arr.length && arr.includes(serviceIds) ) ? true : false;
		}
	}
	isRequestable(serviceIds){
		var arr = typeof this.props.bookingServices.quotesServices != "undefined" ? this.props.bookingServices.quotesServices : [];
		if(typeof serviceIds == "object"){
			return arr.some( services => serviceIds.indexOf(services) !== -1)
		}else{
			return ( arr.length && arr.includes(serviceIds) ) ? true : false;
		}
		
	}
	getServiceIdByURL(serviceURL){
		var bookingServices = typeof this.props.apiServices != "undefined" ? this.props.apiServices : [];
		var arr = [];
		// for noon tv installation
		serviceURL = serviceURL ==  URLCONSTANT.NOON_TV_INSTALLATION_PAGE_URI ? URLCONSTANT.MAINTENANCE_PAGE_URI : serviceURL;

		//serviceURL = serviceURL == URLCONSTANT.LAUNDRY_DRY_CLEANING ? URLCONSTANT.CLEANING_MAID_PAGE_URI : serviceURL;
		
		if(bookingServices.length){
			bookingServices.map((item) => { 
				if( item.urlKeyword == serviceURL){
					arr.push(item.id);
				}
				item.subServices.filter(service => service.urlKeyword == serviceURL ? arr.push(service.id) : [] );
			});
		}
	  return arr;
	}
	render() {
		var isJourneyPage = this.isJourneyPage();
		var isZohoPage = this.isZohoPage();
		var isReviewPage = this.isReviewPage();
		var isBookingPage = this.isBookingPage();
		var isProfilePage = this.isProfilePage();
		var isConfirmationPage = this.isConfirmationPage();

		var pathname = this.state.pathname;
		var showError = false;

		var showFooter = (isJourneyPage == true || isProfilePage == true );
		
		showFooter = isConfirmationPage ? false : showFooter;

		if( (!isZohoPage && !isReviewPage ) && (pathname !="" && pathname.length) ){
			var args = pathname.split('/');
			var serviceURL = args[3];
			var serviceId = this.defaultService(serviceURL);
			var serviceIds = this.getServiceIdByURL(serviceURL);
			
			if(typeof serviceId == "undefined"){
				serviceId = serviceIds;
			}
			if(isBookingPage || isJourneyPage){
				//console.log("serviceIds", isBookingPage, serviceId, this.isBookingAvailable(serviceId));
				if(isBookingPage && !this.isBookingAvailable(serviceId)){
					showError = true
				}
				else if(isJourneyPage && !this.isRequestable(serviceId)){
					showError = true
				}
			}
			
		}
		//showError = false;
		if(showError){
			return (
				<section className="error-404 image-404 text-center text-align-centre margin-top60">
					<h1>Wrong address?</h1>
					<p>The page you are looking for was moved, renamed, or may have never existed. </p>
					<p>Go <a href="/" className="text-white">home</a> or try a search:</p>
				</section>
			);
		}
		else if (isZohoPage || isReviewPage) {
			return (
				<div>
					<Switch>
						{Routes.map(route => <Route key={route.path} {...route} />)}
					</Switch>
				</div>
			);
		} else {
			return (
				<div>
					<PopupMenu location={this.props.location}/>
					<PopupLogin />
					<PopupForgetPassword />
					<PopupFeedback />
					<Switch>
						{Routes.map(route => <Route key={'h' + route.path} path={route.path} exact={route.exact} component={Header} />)}
					</Switch>
					<Switch>
						{Routes.map(route => <Route key={route.path} {...route} />)}
					</Switch>
					{ (!showFooter) && <Footer />}
				</div>
			);
		}
	}
}
function mapStateToProps(state){
    return {
        serviceConstants: state.serviceConstants,
        dataConstants: state.dataConstants,
        currentCity: state.currentCity,
        dateAndTime: state.dateAndTime,
        apiServices: state.apiServices,
		cities: state.cities,
		bookingServices: state.bookingServices,
		lang: state.lang,
        currentCity: state.currentCity
    }
}
export default withRouter(connect(mapStateToProps)(Layout));
//export default withRouter(Layout);