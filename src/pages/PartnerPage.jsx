import React from "react";
import DefaultBanner from "../components/DefaultBanner";
import ImageTitleTextGrid from "../components/ImageTitleTextGrid";
import CenteredText from "../components/CenteredText";
import BecomePartnerForm from "../components/BecomePartnerForm";
import Loader from "../components/Loader";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {fetchBecomePartnerData, showLoader, hideLoader} from "../actions/index";


class PartnerPage extends React.Component{
    _isMounted = false;
    constructor(props) {
        super(props);
        this.updateMetaData = this.updateMetaData.bind(this);
    }
    createMarkup(htmlString) {
        return {__html: htmlString};
    }
    componentDidMount(){
        this._isMounted = true;
        if ( typeof this.props.bePartnerData.acf == "undefined") {
            this.props.showLoader();
            this.props.fetchBecomePartnerData(this.props.lang)
            .then(()=> {
                if (this._isMounted) {
                    this.updateMetaData();
                    this.props.hideLoader();
                }
            });
        } else {
            this.updateMetaData();
        }
    }
    updateMetaData() {
        if( (typeof this.props.bePartnerData != "undefined" && typeof this.props.bePartnerData.acf != "undefined") && typeof this.props.bePartnerData.acf.seo_meta_title != "undefined" ){
            document.title = this.props.bePartnerData.acf.seo_meta_title ? this.props.bePartnerData.acf.seo_meta_title : 'ServiceMarket';
            var allMetaElements = document.getElementsByTagName('meta');
            for (var i=0; i<allMetaElements.length; i++) {
                if (allMetaElements[i].getAttribute("name") == "description") {
                    allMetaElements[i].setAttribute('content', this.props.bePartnerData.acf.seo_meta_description ? this.props.bePartnerData.acf.seo_meta_description : 'ServiceMarket');
                    break;
                }
            }
        }
    }
    componentWillUnmount(){
        this._isMounted = false;
    }
    render() {
        const {bePartnerData, loader} = this.props;
        if(typeof bePartnerData.acf == "undefined"){
            return ( <main loader="show"><Loader/></main>);
        }
        else if(typeof bePartnerData.acf != "undefined" && !loader){
            return (
                <main>
                    <DefaultBanner title={bePartnerData.acf.banner_title} background={bePartnerData.acf.banner_image}/>
                    <section className="py-5 bg-white">
                        <div className="container">
                            {bePartnerData.acf.banner_description.length ? ( <p dangerouslySetInnerHTML={this.createMarkup(bePartnerData.acf.banner_description)}></p> ) : ''}
                            <ImageTitleTextGrid items={bePartnerData.acf.become_a_service_market_partner_features} />
                            <CenteredText FirstText={bePartnerData.acf.become_partner_title} SecondText={bePartnerData.acf.become_partner_description} />
                            <BecomePartnerForm />
                        </div>
                    </section>
                </main>
            );
        }else{
            return ( <main loader={loader ? "show": "hide"}><Loader/></main>);
        }
    }
}

function mapStateToProps(state){
	return {
        loader: state.loader,
        bePartnerData: state.bePartnerData,
        CurrentCity: state.CurrentCity,
        lang : state.lang,
	}
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({fetchBecomePartnerData, showLoader, hideLoader}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(PartnerPage);
