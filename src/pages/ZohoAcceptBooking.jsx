import React from "react";
import {withCookies} from 'react-cookie';
import qs from 'query-string';
//import {findDOMNode} from 'React-dom';
import {updateLWRequest} from "../actions";
import commonHelper from "../helpers/commonHelper"

class ZohoAcceptBooking extends React.Component {
    _isMounted = false;

    constructor(props) {
        super(props);
        this.state={
            resp:{},
            rid:"",
            loading: true,
            isError: false
        };
        this.successResponse = this.successResponse.bind(this);
        this.failureResponse = this.failureResponse.bind(this);
        this.errorResponse = this.errorResponse.bind(this);
    }
    componentWillUnmount(){
        this._isMounted = false;
    }
    componentDidMount() {
        this._isMounted = true;
        const { cookies } = this.props;
        
        var id = commonHelper.getParameterByName('id');
        
        var company = commonHelper.getParameterByName('company');
        
        var email = commonHelper.getParameterByName('email');
        
        var phone = commonHelper.getParameterByName('phone');

        var companyId = commonHelper.getParameterByName('company_id');

        if(company == null || id == null){
            this.setState({
                loading: false,
                isError: true
            })
        }else{
            var submittedData = {
                id: id,
                company: company,
                email: email,
                phone: phone,
                companyId: companyId
            }
            updateLWRequest(JSON.stringify(submittedData)).then((response) => {
                if (this._isMounted) {
                    console.log("updateLWRequest", response);
                    response = response.data;
                    if (response.resp) {
                        this.setState({
                            loading: false,
                            isError: false,
                            resp: {
                                resp: true,
                                First_Name: response.First_Name,
                                Last_Name: response.Last_Name,
                                Email: response.Email,
                                Phone: response.Phone,
                                Service: response.Service,
                                Area: response.Area,
                                City: response.City
                            }
                        })
                    } else {
                        this.setState({
                            loading: false,
                            isError: false,
                            resp: {
                                resp: false
                            }
                        })
                    }
                }
            });
        }

        //https://servicemarket.com/book-direct/zohosm?id=${Leads.Lead%20Id}&company=Duclean%20Residential%20Cleaning%20Services&email=lhizasamala19@gmail.com&phone=0505824648&company_id=391

        /*var url = window.location.href;
        var n = url.indexOf('?');
        url = url.substring(0, n != -1 ? n : url.length);
        history.pushState(null, null, url);*/

    }
    successResponse(){
        const { resp } = this.state;
        var messageStyle = {
            fontSize:'0.8em',
            marginTop:'2em'
        };

        var nameStyle ={
            borderTop : 'none'
        };

        var marginTopStyle ={
            marginTop:'2em'
        };

        return (<div className="success-response">
                    <img id="image" src="../dist/zoho_assets/success.png" />
                    <p id="message">The booking job is yours!</p>
                    <p id="message1" style={marginTopStyle}><em>Job details</em>
                </p>
                <p style ={messageStyle}>Please check your email for more details.</p>
                <table className="zui-table zui-table-horizontal">
                    <tbody>
                    <tr>
                        <td style={nameStyle}>Name</td>
                        <td id="name" style={nameStyle}>{ resp.First_Name+" "+resp.Last_Name }</td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td id="email">{ resp.Email }</td>
                    </tr>
                    <tr>
                        <td>Phone</td>
                        <td id="phone">{ resp.Phone }</td>
                    </tr>
                    <tr>
                        <td>Service requested</td>
                        <td id="service">{ resp.Service }</td>
                    </tr>
                    <tr>
                        <td>Location</td>
                        <td id="location">{ resp.Area+", "+resp.City }</td>
                    </tr>
                    </tbody>
                </table>
            </div>);
    }
    failureResponse(){
        return(
            <div className="response">
                <div><img id="image" src="../dist/zoho_assets/failure.png" />
                </div>
                <p id="message">Sorry, the booking job is no longer available.</p>
            </div>
        );
    }
    errorResponse(){
        return(<div className="response">
            <img id="image" src="../dist/zoho_assets/error.png" />
            <p id="message">Bad request. Please use the link in the email.</p>
        </div>);
    }
    render() {
        const { rid, resp} = this.state;

        var bgPrimary =  { backgroundColor: "#00748d",textAlign:'center' };
        var logoStyle =  { width:'150px',padding:'15px'};

        //console.log("ZohoAcceptBooking", resp.resp);
        
        return (
            <div className="container" id="container">
                <div className="bg-primary text-center" style={bgPrimary}>
                    <img src="../dist/zoho_assets/logo-white.svg" style={logoStyle} />
                </div>
                <div className="starter-template">
                
                { this.state.loading && <div className="processing">
                        <div>
                            <img id="image" src="../dist/zoho_assets/processing.gif" />
                        </div>
                        <p id="message">Processing request...</p>
                    </div>
                }
                    
                    { ( resp.resp == true ) && this.successResponse() }
                    { ( resp.resp == false) && this.failureResponse() }
                    { ( this.state.isError ) && this.errorResponse() }
                </div>
            </div>);

    }
}
export default withCookies(ZohoAcceptBooking);