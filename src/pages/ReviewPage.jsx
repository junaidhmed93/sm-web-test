import React from "react";
import DefaultBanner from "../components/DefaultBanner";
import ReviewForm from "../components/ReviewForm";
import Loader from "../components/Loader";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {fetchReviewData, showLoader, hideLoader} from "../actions/index";
import Accordion from "../components/Accordion";
import SectionTitle from "../components/SectionTitle";
import locationHelper from '../helpers/locationHelper';

class ReviewPage extends React.Component{
    _isMounted = false;
    constructor(props) {
        super(props);
        this.locationHelperInit = locationHelper.init.bind(this);
        this.locationHelperInit();
        this.updateMetaData = this.updateMetaData.bind(this);
    }
    createMarkup(htmlString) {
        return {__html: htmlString};
    }
    componentDidMount(){
        this._isMounted = true;
        if ( !this.props.reviewData.acf ) {
            this.props.showLoader();
            this.props.fetchReviewData(this.props.lang)
            .then(()=> {
                if (this._isMounted) {
                    this.updateMetaData();
                    this.props.hideLoader();
                }
            });
        } else {
            this.updateMetaData();
        }
    }
    componentWillUnmount(){
        this._isMounted = false;
    }
    updateMetaData() {
        // console.log('updating meta data');
        document.title = this.props.reviewData.acf.seo_meta_title ? this.props.reviewData.acf.seo_meta_title : 'ServiceMarket';
        var allMetaElements = document.getElementsByTagName('meta');
        for (var i=0; i<allMetaElements.length; i++) {
            if (allMetaElements[i].getAttribute("name") == "description") {
                allMetaElements[i].setAttribute('content', this.props.reviewData.acf.seo_meta_description ? this.props.reviewData.acf.seo_meta_description : 'ServiceMarket');
                break;
            }
        }
    }
    render() {
        const {reviewData, loader, cities, mainServices} = this.props;
        //console.log("reviewData.acf.reviews_questions", reviewData.acf.reviews_questions);
        if(reviewData.acf && !loader){
            return (
                <main>
                    <DefaultBanner title={reviewData.acf.banner_title} background={reviewData.acf.banner_image}/>
                    <section className="py-5 bg-white review-page">
                        <div className="container">
                            {reviewData.acf.banner_description.length ? ( <SectionTitle title={reviewData.acf.banner_description} subTitle="" parentClass="row mb-4" childClass="col px-4 px-md-3" /> ) : ''}
                            <div className="row">
                                <div className="col-12 col-md-9">
                                    <ReviewForm />
                                </div>
                                <div className="col-12 col-md-3">
                                    <p className={"h3"}>{locationHelper.translate('QUESTIONS')}</p>
                                    {reviewData.acf.reviews_questions && <Accordion panels={reviewData.acf.reviews_questions} parentClass="mb-5" />}
                                </div>
                            </div>
                        </div>
                    </section>
                </main>
            );
            }else{
                return ( <main loader={loader ? "show": "hide"}><Loader/></main>);
            }
    }
}

function mapStateToProps(state){
	return {
        loader: state.loader,
        reviewData: state.reviewData,
        cities: state.cities,
        mainServices: state.mainServices,
        CurrentCity: state.CurrentCity,
        lang : state.lang,
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({fetchReviewData, showLoader, hideLoader}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ReviewPage);

