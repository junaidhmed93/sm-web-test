import React from "react";
import Nav from "../components/Nav";
import Accordion from "../components/Accordion";
import FormFieldsTitle from "../components/FormFieldsTitle";
import CheckRadioBoxInput from "../components/Form_Fields/CheckRadioBoxInput";
import TextFloatInput from "../components/Form_Fields/TextFloatInput";
import SelectInput from "../components/Form_Fields/SelectInput";
import DatePick from "../components/Form_Fields/DatePick";
import TextareaInput from "../components/Form_Fields/TextareaInput";
import PhoneInput from "../components/Form_Fields/PhoneInput";
import Plans from "../components/Form_Fields/Plans";
import PaymentMethods from "../components/Form_Fields/PaymentMethods";
import SubmitBooking from "../components/Form_Fields/SubmitBooking";
import BookingSummary from "../components/Form_Fields/BookingSummary";
import BookNextStep from "../components/Form_Fields/BookNextStep";
import FormTitleDescription from "../components/FormTitleDescription";



class BookMovingPage extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            movetype: 'full',
            hometype: '',
            apartmenttype: '',
            city: '',
            area: '',
            unitno: '',
            moveday: '',
            message: '',
            fname: '',
            lname: '',
            email: '',
            phone: '',
            plan: 'standard',
            payment: 'credit',
            currentStep: 1
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleCustomChange = this.handleCustomChange.bind(this);
        this.moveNext = this.moveNext.bind(this);
        this.jumpTo = this.jumpTo.bind(this);
        this.submitData = this.submitData.bind(this);
        this.calcTotal = this.calcTotal.bind(this);
    }
    handleInputChange(event) {
        var name = event.target.name;
        this.setState({
            [name]: event.target.value
        });
    }
    handleCustomChange(name, value){
        this.setState({
            [name]: value
        });
        // console.log(this.state);
    }
    moveNext(step){
        this.setState({
            "currentStep": parseInt(step)
        });
    }
    jumpTo(step){
        var stepInt = parseInt(step);
        if( stepInt <  this.state.currentStep){
            this.setState({
                "currentStep": stepInt
            });
        }
    }
    calcTotal(){
        // Here should be all the calculations for Total price
        var total = 0;
        switch(this.state.plan) {
            case 'standard':
                total = 1300;
                break;
            case 'premium':
                total = 1700;
                break;
            case 'budget':
                total = 1000;
                break;
        }
        return total;
    }
    submitData(){

    }

    render() {
        var {movetype, hometype, apartmenttype, city, area, unitno, message, moveday, fname, lname, email, phone, plan, payment, currentStep} = this.state;
        var moveTypes = [{id: 1, value: "full", label: "Moving my full home"}, {id: 2, value: "partial", label: "Moving a few items"}];
        var homeTypes = [{id: 1, value: "apartment", label: "Apartment"}, {id: 2, value: "villa", label: "Villa"}];
        var apartmentTypes = [{id: 0, value: "studio", label: "Studio"}, {id: 1, value: "1BR", label: "1 BR"}, {id: 2, value: "2BR", label: "2 BR"}, {id: 3, value: "3BR", label: "3 BR"}, {id: 4, value: "4BR", label: "4 BR"}, {id: 5, value: "5BR", label: "5 BR"}];
        var selectOptions = [{id: 1, value: "1", label: "Option 1"}, {id: 2, value: "2", label: "Option 2"}, {id: 3, value: "3", label: "Option 3"}];
        var plans = [ { id: 0,value: 'standard', title: 'Standard Mover', subTitle: 'Most popular', price: 1300, currency: 'AED', rateSection: [ {title: 'Mover rating', color: 'text-primary', icons: ["fa fa-star"]}, {title: 'Team', color: 'text-primary', icons: ["fa fa-star"]}, {title: 'Packing material', color: 'text-primary', icons: ["fa fa-star"]}, {title: 'Communication skills', color: 'text-primary', icons: ["fa fa-star"]} ], features: [ {title: 'Uniform', color: 'text-danger', icons: ["fa fa-times"]}, {title: 'Boxes', color: 'text-success', icons: ["fa fa-check"]}, {title: 'Dis-Assembly / Assembly', color: 'text-danger', icons: ["fa fa-times"]}, {title: 'Uniform', color: 'text-danger', icons: ["fa fa-times"]}, {title: 'Boxes', color: 'text-success', icons: ["fa fa-check"]}, {title: 'Dis-Assembly Assembly', color: 'text-danger', icons: ["fa fa-times"]} ] }, { id: 1, value: 'premium', title: 'Premium Mover', subTitle: 'Peace of mind', price: 1700, currency: 'AED', rateSection: [ {title: 'Mover rating', color: 'text-primary', icons: ["fa fa-star","fa fa-star","fa fa-star"]}, {title: 'Team', color: 'text-primary', icons: ["fa fa-star", "fa fa-star", "fa fa-star"]}, {title: 'Packing material', color: 'text-primary', icons: ["fa fa-star","fa fa-star","fa fa-star"]}, {title: 'Communication skills', color: 'text-primary', icons: ["fa fa-star","fa fa-star","fa fa-star"]} ], features: [ {title: 'Uniform', color: 'text-success', icons: ["fa fa-check"]}, {title: 'Boxes', color: 'text-success', icons: ["fa fa-check"]}, {title: 'Dis-Assembly / Assembly', color: 'text-success', icons: ["fa fa-check"]}, {title: 'Uniform', color: 'text-success', icons: ["fa fa-success"]}, {title: 'Boxes', color: 'text-success', icons: ["fa fa-check"]}, {title: 'Dis-Assembly / Assembly', color: 'text-success', icons: ["fa fa-check"]} ] }, { id: 2,value: 'budget', title: 'Budget Mover', subTitle: 'Saver option', price: 1000, currency: 'AED', rateSection: [ {title: 'Mover rating', color: 'text-primary', icons: ["fa fa-star","fa fa-star"]}, {title: 'Team', color: 'text-primary', icons: ["fa fa-star", "fa fa-star"]}, {title: 'Packing material', color: 'text-primary', icons: ["fa fa-star","fa fa-star"]}, {title: 'Communication skills', color: 'text-primary', icons: ["fa fa-star","fa fa-star"]} ], features: [ {title: 'Uniform', color: 'text-success', icons: ["fa fa-check"]}, {title: 'Boxes', color: 'text-success', icons: ["fa fa-check"]}, {title: 'Dis-Assembly / Assembly', color: 'text-success', icons: ["fa fa-check"]}, {title: 'Uniform', color: 'text-success', icons: ["fa fa-success"]}, {title: 'Boxes', color: 'text-success', icons: ["fa fa-check"]}, {title: 'Dis-Assembly / Assembly', color: 'text-success', icons: ["fa fa-check"]} ] } ];
        var paymentTypes = [{id: 0, value: "credit", lcurrentStepabel: "Credit card", desc: "Get 10% off on first cleaning when pay by card", image: "visa.png", text: "Verify your card now, pay only when service is delivered. Your card will be verified for 1 AED and then the 1 AED will be reversed"}, {id: 1, value: "cash", label: "Cash on delivery", desc: "100% Secure, Easy payment, Trusted partners", image: "money.svg", text: "No card? No problem, Just pay our cleaners cash when the job is done."}];
        var summaryItems = [{id: 0, label: 'Type of move', value: movetype}, {id: 1, label: 'Type of home', value: hometype}, {id: 2, label: 'Moving from', value: city}, {id: 3, label: 'Scheduled for', value: moveday}, {id: 4, label: 'Plan', value: plan}, {id: 5, label: 'Payment', value: payment}];
        var stepContent;
        var submitElement = '';
        if( currentStep == 1 ){
        stepContent = (
        <div className="book-step">
            <section>
                <FormFieldsTitle title="What is the type of your move?" />
                <CheckRadioBoxInput InputType="radio" name="movetype" inputValue={movetype} items={moveTypes} onInputChange={this.handleCustomChange} childClass="col-6 col-sm-4 mb-3 d-flex" />
            </section>
            <section>
                <FormFieldsTitle title="Describe your home" />
                <CheckRadioBoxInput InputType="radio" name="hometype" inputValue={hometype} items={homeTypes} onInputChange={this.handleCustomChange} parentClass="row" childClass="col-6 col-sm-4 mb-3 d-flex" />
                <CheckRadioBoxInput InputType="radio" name="apartmenttype" inputValue={apartmenttype} items={apartmentTypes} onInputChange={this.handleCustomChange} parentClass={"row mb-4" + (hometype == 'apartment' ? '' : ' d-none')} childClass="col-4 col-sm-2 mb-3 d-flex" />
            </section>
            <section>
                <FormFieldsTitle title="Where are you moving from?" />
                <div className="row mb-4">
                    <div className="col-12 col-sm-6 col-md-4 mb-3">
                        <SelectInput name="city" inputValue={city} label="City" options={selectOptions} onInputChange={this.handleCustomChange} />
                    </div>
                    <div className="col-12 col-sm-6 col-md-4 mb-3">
                        <TextFloatInput InputType="text" name="area" inputValue={area} label="Area" onInputChange={this.handleInputChange} validation={{required: 'required'}} />
                    </div>
                    <div className="col-12 col-sm-6 col-md-4 mb-3">
                        <TextFloatInput InputType="text" name="unitno" inputValue={unitno} label="Building and unit no" onInputChange={this.handleInputChange} validation={{required: 'required'}} />
                    </div>
                </div>
            </section>
            <section>
                <FormFieldsTitle title="When do you need to move?" />
                <DatePick name="moveday" onInputChange={this.handleCustomChange} />
            </section>
            <section>
                <FormFieldsTitle title="What else should we know? (Optional)" />
                <div className="row mb-4">
                    <div className="col mb-3">
                        <TextareaInput name="message" inputValue={message} placeholder="Write anything here" onInputChange={this.handleInputChange} />
                    </div>
                </div>
            </section>
            <section>
                <BookNextStep title="Select companies" moveNext={this.moveNext} toStep="2" />
            </section>
        </div>
        );
        }else if( currentStep == 2 ){
            stepContent = (
            <div className="book-step">
                <section>
                    <FormFieldsTitle title="Select companies you want a quote from (showing 354 companies)" />
                </section>
                <section>
                    <BookNextStep title="Contact details" moveNext={this.moveNext} toStep="3" />
                </section>                    
            </div>
            );
        }else if( currentStep == 3 ){
            stepContent = (
            <div className="book-step">
                <section>
                    <FormTitleDescription title="Your contact details" desc="We need your contact information in case we need to get in touch when coordinating your move" />
                </section>
                <section>
                    <div className="row">
                        <div className="col-6 pr-2 pr-sm-3 mb-3">
                            <TextFloatInput InputType="text" name="fname" inputValue={fname} label="First name" onInputChange={this.handleInputChange} validation={{required: 'required'}} />
                        </div>
                        <div className="col-6 pr-2 pr-sm-3 mb-3">
                            <TextFloatInput InputType="text" name="lname" inputValue={lname} label="Last name" onInputChange={this.handleInputChange} validation={{required: 'required'}} />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-6 pr-2 pr-sm-3 mb-3">
                            <TextFloatInput InputType="text" name="email" inputValue={email} label="Email" onInputChange={this.handleInputChange} validation={{required: 'required'}} />
                        </div>
                        <div className="col-6 pr-2 pr-sm-3 mb-3">
                            <PhoneInput name="phone" inputValue={phone} onInputChange={this.handleCustomChange} validation={{required: 'required'}} />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col">
                            <p><small>* We value your privacy! This information will only be used to contact you about your move.</small></p>
                        </div>
                    </div>
                    <BookNextStep title="Payment details" moveNext={this.moveNext} toStep="4"/>
                </section>
            </div>
            );
        }else if( currentStep == 4 ){
            stepContent = (
            <div className="book-step">
                <section>
                    <FormTitleDescription title="Please choose the right moving package" desc="Choose the type of mover and budget that suits you. Book now and you only pay when the service is complete" />
                    <Plans items={plans} name="plan" inputValue={plan} onInputChange={this.handleCustomChange} />
                </section>
                <section className="payment">
                    <FormFieldsTitle title="How would you like to pay?" />
                    <PaymentMethods items={paymentTypes} name="payment" inputValue={payment} onInputChange={this.handleCustomChange} />
                </section>

            </div>
            );
            submitElement = (<div><hr /><SubmitBooking submitData={this.submitData} /></div>);
        }
        return (
            <main>
                <Nav serviceTitle="Moving" currentStep={currentStep} jumpTo={this.jumpTo} />
                <div className="container pt-5">
                    <div className="row">
                        <div className="col-lg-8">
                            {stepContent}
                        </div>
                        <div className="col-md-3 ml-auto d-lg-block d-none">
                            <div className="row sticky-sidebar">
                                <div className="col-12">
                                    <BookingSummary items={summaryItems} total={this.calcTotal()}/>
                                </div>
                                <div className="col-12">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    {submitElement}
                </div>
            </main>
        );
    }
}

export default BookMovingPage;
