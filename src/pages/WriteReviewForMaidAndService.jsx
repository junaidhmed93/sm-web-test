import React from "react";
import {withCookies} from 'react-cookie';
import qs from 'query-string';
import {LWBLowReviewQuestions, 
    getWorkersDetailByEventId, 
    isEventReviewed, 
    sendMaidReview,
    isServiceReviewed,sendServiceReview, scrollToTop} from "../actions";
import commonHelper from "../helpers/commonHelper"
import SimpleHeader from "../components/SimpleHeader";
import Footer from "../components/Footer";
import RateInput from "../components/Form_Fields/RateInput";
import FormFieldsTitle from "../components/FormFieldsTitle";
import FormTitleDescription from "../components/FormTitleDescription";
import Loader from "../components/Loader";
import TextareaInput from "../components/Form_Fields/TextareaInput";
var isOtherOptAdded = false;
class WriteReviewForMaidAndService extends React.Component {
    _isMounted = false;
    constructor(props) {
        super(props);
        //console.log("urlParams", props.urlParams);
        let urlParams = props.urlParams;
        
        let isRateYourService = typeof urlParams.params.leadId != "undefined" ? true : false;

        let isNewBookingEngine = (typeof urlParams.params.typeOfReview != "undefined"  && urlParams.params.typeOfReview == "service-review") ? true : false;

        let isRateYourMaid = typeof urlParams.params.eventId != "undefined" ? true : false;

        //urlParams.params.typeOfReview == "service-review"

        this.state = {
            loading: true,
            isError: false,
            rating:0,
            lowRatingReason:[],
            rateReviewReason:[],
            isNewBookingEngine: isNewBookingEngine,
            isRateYourService : isRateYourService,
            isRateYourMaid : isRateYourMaid,
            loading: true,
            isNotReviewed: false,
            workerIds:[],
            otherComment: "",
            errorSubmission: false,
            reviewRatingError: false,
            lowRatingReviewError: false,
            otherReviewError: false,
            showSuccessMsg : false
        };
        this.successResponse = this.successResponse.bind(this);
        this.failureResponse = this.failureResponse.bind(this);
        this.getReview = this.getReview.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.badReviewReason = this.badReviewReason.bind(this);
        this.isChecked = this.isChecked.bind(this);
        this.getLowRatingReason = this.getLowRatingReason.bind(this);
        this.submitReview = this.submitReview.bind(this);
        this.submitMaidReview = this.submitMaidReview.bind(this);
        this.submitServiceReview = this.submitServiceReview.bind(this);
        this.isOtherRatingOptionSelected = this.isOtherRatingOptionSelected.bind(this);
        this.generateSubmissionData = this.generateSubmissionData.bind(this);
        

    }
    componentWillUnmount(){
        this._isMounted = false;
    }
    componentDidMount() {
        this._isMounted = true;
        var rateReviewReason = [];

        var { isRateYourMaid, isRateYourService, isNewBookingEngine} = this.state;
        var urlParams = this.props.urlParams;
        var rate = commonHelper.getParameterByName("rating");
        //console.log("rate 234324", rate);
        if(isRateYourMaid){
            rateReviewReason = commonHelper.getNewDataValues("rateReviewReason");
            if(!isOtherOptAdded){
                rateReviewReason.push({id:20, name:"Other", value:"Other"});
                isOtherOptAdded = true;
            }
            var eventId = urlParams.params.eventId;
            var isNotReviewed =  false;
            if(isNewBookingEngine || isRateYourMaid){ // new booking engine
                isEventReviewed(eventId).then(res => {
                    if (this._isMounted) {
                        var responseData = res.data;
                        if (responseData!=null && ( (typeof responseData.success != "undefined" && responseData.success) && responseData.message == "") ) {
                            isNotReviewed = true;
                        } else {
                            isNotReviewed = false;
                        }
                        this.setState({
                            isNotReviewed: isNotReviewed,
                            loading: false
                        })
                    }
                });
            }//else{
                if(isRateYourMaid && !isNotReviewed){
                    getWorkersDetailByEventId(eventId).then(res => {
                        if (this._isMounted) {
                            if (res.success) {
                                var responseWorkersData = res.data;
                                var workerIds = [];
                                responseWorkersData.map((item, index) => {
                                    workerIds.push(item.id);
                                })
                                this.setState({
                                    workerIds: workerIds,
                                    loading: false
                                })
                                //console.log("workerIds", workerIds);
                            }else{
                                this.setState({
                                    isNotReviewed: isNotReviewed,
                                    loading: false
                                })
                            }
                        }
                    })
                }
            //}
        }else if(isRateYourService){
            rateReviewReason = LWBLowReviewQuestions;
            var eventId = urlParams.params.leadId;
            var isNotReviewed =  false;
            isServiceReviewed(eventId).then(res => {
                if (this._isMounted) {
                    var responseData = res.data.data;
                    //console.log(responseData);
                    if (typeof responseData != "undefined") {
                        if (responseData.success && responseData.message == "") {
                            isNotReviewed = true;
                        } else {
                            isNotReviewed = false;
                        }
                    }
                    this.setState({
                        isNotReviewed: isNotReviewed,
                        loading: false
                    });
                }
            });
            
        }
        this.setState({
            rateReviewReason: rateReviewReason,
            rating: rate != null ? rate : 0
        })
        
    }
    successResponse(){
        var {lowRatingReason,
            isRateYourService,
            isRateYourMaid,
            isNotReviewed,
            workerIds,
            rating
        } = this.state;
        var ratingText = rating == 5 ? (<span className="thank-you-page-subheading">Please rate us on <a href="http://plus.google.com/+MovesouqGCC">Google+</a></span>) : "Your review has been submitted";
        return(
            <React.Fragment>
                <div className="of-thankyou-message my-5">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 text-center thank-you-5star-container">
                                <h4 className="h1 text-warning">
                                    Thank you for your feedback!
                                </h4>
                                <p className="h3">{ratingText}</p>
                                <div className="row mt-3">
                                    <div className="col-12 mx-auto d-flex">
                                        <a href="/" className="btn mx-auto btn-warning btn-default text-uppercase py-3 text-bold px-5">Back To Home Page</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
        
    }
    failureResponse(){
        return(
            <div className="container already-reviewed">
                <div className="row maid-detail-row already-reviewed my-5">
                    <div className="p-5 col-10 mx-auto d-flex bg-light border rounded">
                        <div className="order-feedback-not-found">
                            <h3 className="text-warning">Oops! Looks like we can't accept your rating for this service.</h3>
                            <h4 className="text-primary">This might be because:</h4>
                            <ul className="list-unstyled ml-3">
                                <li>a. This service was cancelled</li>
                                <li>b. You've already rated this job</li>
                            </ul>
                            <p>You can always email us at <a href="mail_to:bookings@servicemarket.com">bookings@servicemarket.com</a> in case there is something you'd like to share.</p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    isChecked(item){
        var {lowRatingReason} = this.state;
        return lowRatingReason.filter((obj)=> obj.value == item.value).length > 0;
    }
    handleChange(event, item){
        var {lowRatingReason} = this.state;
        if(event.target.checked){ 
            lowRatingReason.push(item);
        }else{
            lowRatingReason = lowRatingReason.filter((obj)=> obj.value != item.value);
        }
        this.setState({
            lowRatingReason:lowRatingReason
        });
    }
    submitReview(){
        var {lowRatingReason,
            isRateYourService,
            isRateYourMaid,
            isNotReviewed,
            workerIds,
            rating,
            otherComment
        } = this.state;
        //console.log("lowRatingReason", lowRatingReason);
        var failed = 0;
        var reviewRatingError = false;
        var lowRatingReviewError = false;
        var otherReviewError = false; 
        

        if( rating == 0 ){
            reviewRatingError = true;
            failed++;
        }
        if( rating <= 4 && lowRatingReason.length == 0 ){
            lowRatingReviewError = true;
            failed++;
        }
        if(this.isOtherRatingOptionSelected() && otherComment.trim() == ""){
            otherReviewError = true;
            failed++;
        }
        this.setState({
            loading: true
        })
        
        if(failed){
            this.setState({
                loading: false,
                reviewRatingError: reviewRatingError,
                lowRatingReviewError : lowRatingReviewError,
                otherReviewError: otherReviewError
            })
            return false;
        }
        else{
            this.setState({
                reviewRatingError: reviewRatingError,
                lowRatingReviewError : lowRatingReviewError,
                otherReviewError: otherReviewError
            })
            scrollToTop();
            if(isRateYourMaid){
                this.submitMaidReview();
            }
            if(isRateYourService){
                this.submitServiceReview();
            }
        }
    }
    submitServiceReview(){
        var urlParams = this.props.urlParams;
        var eventId = urlParams.params.leadId;
        var {lowRatingReason,
            isRateYourService,
            isRateYourMaid,
            isNotReviewed,
            workerIds,
            otherComment,
            rating
        } = this.state;

        var submitData = this.generateSubmissionData();
        
        sendServiceReview(eventId, submitData).then(res => {
            var responseData = res;
            responseData = res.data.data;
            if(typeof responseData != "undefined"){
                if(responseData.success){
                    this.setState({
                        loading: false,
                        showSuccessMsg : true,
                        isNotReviewed: false
                    })
                }else{
                    this.setState({
                        loading: false,
                        errorSubmission : true  
                    })
                }
            }else{
                this.setState({
                    loading: false,
                    errorSubmission : true  
                })
            }
        })
    }
    submitMaidReview(){
        var {lowRatingReason,
            isRateYourService,
            isRateYourMaid,
            isNotReviewed,
            workerIds,
            otherComment,
            rating
        } = this.state;

        var submitData = this.generateSubmissionData();
        
        sendMaidReview(submitData).then(res => {
            var responseData = JSON.parse(res);
            /*this.setState({
                loading: false,
                showSuccessMsg : true  
            })*/
            //responseData = {success:true}
            //console.log("responseData", responseData);

            if(responseData.success){
                this.setState({
                    loading: false,
                    showSuccessMsg : true,
                    isNotReviewed: false
                })
            }else{
                this.setState({
                    loading: false,
                    errorSubmission : true  
                })
            }
        });
    }
    generateSubmissionData(){
        /*var {lowRatingReason,
            isRateYourService,
            isRateYourMaid,
            isNotReviewed,
            workerIds,
            otherComment,
            rating
        } = this.state;*/

        var {lowRatingReason,
            isRateYourService,
            isRateYourMaid,
            isNotReviewed,
            workerIds,
            otherComment,
            rating
        } = this.state;

        var urlParams = this.props.urlParams;
        var commentText = "";
        
        if( rating <= 4 ){
            var comments = [];

            if(lowRatingReason.length){
                lowRatingReason.map((item,index) =>{
                    if(item.name != "Other"){
                        comments.push(item.name);
                    }
                })
            }
            commentText = comments.length ? comments.join("|") : "";
            
            var isOtherReason = this.isOtherRatingOptionSelected();

            if(isOtherReason){
                commentText += "|"+otherComment.trim();
            }
        }

        var workerIds = workerIds.length == 1 ? workerIds.join(",") : null; 

        var data = {};
        if(isRateYourMaid){
            var eventId = urlParams.params.eventId;
            data["bookingEventId"] = eventId;
            data["workerId"] = workerIds;
            data["bookAgain"] = null;
        }

        data["rating"] = isRateYourService ? parseFloat(rating) : rating;
        data["comment"] = commentText;

        var submitData = JSON.stringify([data]);

        return submitData;
    }
    isOtherRatingOptionSelected(){
        var {lowRatingReason} =  this.state;
        var isOtherReason = lowRatingReason.filter((item) => {
            return item.value == "Other";
        })
        return isOtherReason.length ? true : false;
    }
    badReviewReason(){
        var {rateReviewReason, isRateYourMaid} = this.state;
        
        return rateReviewReason.map((question, index) => {
            return (<div className="checkbox checkbox-primary ml-3">
                <input 
                    className="mr-2 reason-checkboxes d-none" 
                    id={"checkbox-"+index} type="checkbox" 
                    value = {question.id} 
                    name="low_rating_reason"
                    onChange={(e)=> this.handleChange(e, question)} 
                    checked={this.isChecked(question)} 
                    />
                <label className="control-label" for={"checkbox-"+index}>{question.name}</label>
            </div>)
        })
    }
    getLowRatingReason(){
        var {rateReviewReason, isRateYourMaid, isRateYourService, rating, lowRatingReason, lowRatingReviewError, otherReviewError, showSuccessMsg, errorSubmission} = this.state;
        var text = "Please tell us why you have selected "+rating+" stars?"
        var isOtherReason = this.isOtherRatingOptionSelected();
        return (<div className="other-feedback">
                    <FormTitleDescription title={text} desc="(Please select all that apply)" />
                    <div className="row d-block three-or-less-rating">
                        <div className="col-12 question-div-innner d-block">
                            { this.badReviewReason() }
                            <div for="low_rating_reason" className={lowRatingReviewError ? "error text-danger" : "d-none"}>This field is required.</div>
                        </div>
                        <div className={ isOtherReason ? "col-12 col-md-6 question-div-innner" : "d-none" }>
                            <TextareaInput 
                            inputValue={this.state.otherComment}
                            onInputChange={this.handleInputChange} 
                            name="otherComment"
                            placeholder="Please enter reason" />
                            <div for="low_rating_reason" className={ otherReviewError ? "error text-danger" : "d-none"}>This field is required.</div>
                        </div>
                        
                    </div>
                    <div className="row mb-3">
                        <div className="col-12"><hr /></div>
                    </div>
                </div>);
    }
    getReview(){
        var {rateReviewReason, isRateYourMaid, isRateYourService, rating, reviewRatingError, errorSubmission, isNewBookingEngine} = this.state;
        //<h2>Please tell us why you have selected <span className="selected-stars">3 stars?</span> <span>(Please select all that apply:)</span></h2>
        var questionText = (isRateYourService || isNewBookingEngine ) ? "How would you rate the service you booked through ServiceMarket.com" : "How would you rate the cleaner you booked through ServiceMarket.com?"; 
        return(
            <React.Fragment>
                <div className="container mt-5">
                    <FormFieldsTitle titleClass="h3 mb-2" title={questionText} />
                    <div className="row">
                        <div className="col-12">
                            <p>On a scale of 1 to 5, where 1 is <span className="text-danger">"Poor"</span> and 5 is <span className="text-success">"Excellent"</span></p>
                            <div className="rate-start">
                                <div className="star-rating d-block w-100 form-item">
                                    <RateInput name="rating" inputValue={this.state.rating} onInputChange={this.handleInputChange}/>
                                </div>
                            </div>
                            <div className={ reviewRatingError ? "review-rating-error text-danger error" : "d-none"}>Please select rating</div>
                        </div>
                        <div className="col-12"><hr /></div>
                    </div>
                    { ( rating !=0 && rating <= 4 ) && this.getLowRatingReason() }
                    { errorSubmission && (<div className="row my-2">
                        <div className="error text-danger">An error occurred while trying to submit review</div>
                    </div>)}
                    <div className="row mt-3 mb-5">
                        <div className="col-12 mx-auto d-flex">
                            <button type="submit" className="btn mx-auto btn-warning btn-default text-uppercase py-3 text-bold px-5" id="maid-review-submit" onClick={ (e) => this.submitReview() }>submit your feedback</button>
                        </div>
                    </div>
                </div>
                </React.Fragment>)
    }
    handleInputChange(name, value){
        if(name == "rating"){
            value = parseInt(value);
        }
        this.setState({
            [name]: value
        })
    }
    render() {
        //console.log(this.state.lowRatingReason);
        var {loading, isNotReviewed, showSuccessMsg} = this.state;
        return (
            <React.Fragment>
                <SimpleHeader />
                <main className="maid-review">
                    <div className="banner-regular-two">
                        <div className="container">
                            <div className="row">
                                <div className="col-12 text-left">
                                    <h1 className="text-white h2">We'd love if you could share your feedback in order for us to improve.</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                    { loading && <Loader /> }
                    { ( !loading && isNotReviewed ) && this.getReview() }
                    { ( !loading && (!isNotReviewed && !showSuccessMsg )) && this.failureResponse() }
                    { ( !loading && showSuccessMsg && this.successResponse()) }
                </main>
                <Footer showFooterTop={false}/>
            </React.Fragment>
        );
    }
}
export default withCookies(WriteReviewForMaidAndService);