import React from "react";
import Loader from "../components/Loader";
import CompanyItem from "../components/CompanyItem";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {fetchProvidersByCity, getCityCode, showLoader, hideLoader, fetchCompaniesPageData, setBodyClass} from "../actions/index";
import DefaultBanner from "../components/DefaultBanner";
import SectionTitle from "../components/SectionTitle";
import TitleTextBlock from "../components/TitleTextBlock";
import { Link } from "react-router-dom";
import commonHelper from "../helpers/commonHelper";
import locationHelper from '../helpers/locationHelper';

class CompaniesPage extends React.Component{
    _isMounted = false;
    constructor(props) {
        super(props);

        this.state={
            classifications: [],
            filter: null
        }
        this.updateFilter = this.updateFilter.bind(this);
        this.updateMetaData = this.updateMetaData.bind(this);
    }

    componentDidMount(){
        this._isMounted = true;
        this.props.showLoader();
        this.props.setBodyClass('companies');
        const currentCity = this.props.match.params.city;
        const cityCode = getCityCode(currentCity, this.props.cities);
        let {lang} = this.props;
        fetchProvidersByCity(cityCode, lang)
        .then((res)=> {
            if( res.length ){
                if (this._isMounted) {
                    this.setState({
                        classifications: res,
                        filter: res[0].serviceId
                    });
                }
            }
            this.props.hideLoader();
        });

        if ( !this.props.companiesData.acf ) {
            this.props.showLoader();
            this.props.fetchCompaniesPageData(lang)
                .then(()=> {
                    if (this._isMounted) {
                        this.updateMetaData();
                        this.props.hideLoader();
                    }
                });
        } else {
            this.updateMetaData();
        }
    }
    updateMetaData() {
        var seoText = commonHelper.getSeoPartnersTextArray();
        document.title = commonHelper.replaceCodedCurrentCityName(seoText.seoTitle, this.props.currentCity);
        var allMetaElements = document.getElementsByTagName('meta');
        for (var i=0; i<allMetaElements.length; i++) {
            if (allMetaElements[i].getAttribute("name") == "description") {
                allMetaElements[i].setAttribute('content', commonHelper.replaceCodedCurrentCityName(seoText.seoDesc, this.props.currentCity));
                break;
            }
        }
    }
    componentDidUpdate(prevProps){
        const prevCity    = prevProps.match.params.city;
        const currentCity = this.props.match.params.city;
        let {lang} = this.props;
        if( (prevCity != currentCity) ){
            this.props.showLoader();
            const cityCode = getCityCode(currentCity, this.props.cities);
            fetchProvidersByCity(cityCode, lang)
                .then((res)=> {
                    if( res.length ){
                        this.setState({
                            classifications: res,
                            filter: res[0].serviceId
                        });
                    }
                    this.props.hideLoader();
                });
            this.updateMetaData();
        }
    }
    componentWillUnmount() {
        this._isMounted = false;
    }
    updateFilter(event){
        this.setState({
            filter: event.target.value
        });
    }
    renderCompanies(serviceProviderDtos){
       // console.log('props lang', this.props.lang);
        return serviceProviderDtos.map((item, index) => {
            if(this.props.lang === 'ar'){
                return(
                    <li key={item.id}><span >{item.name}</span></li>
                )
            }
            else return(
                <li key={item.id}><Link to={"/service-provider/" + item.url}>{item.name}</Link></li>
            )
        });
    }
    renderResults(){
        return this.state.classifications.map((service)=>{

            // console.log(service);
            return (
                <div className="col-lg-3 col-md-3 col-sm-3 col-xs-12 bg-color"><h2 className="service-title">{service.serviceName}</h2>
                    <div className="scrollbar" id="style-4">
                        <ul>
                            {this.renderCompanies(service.serviceProviderDtos)}
                        </ul>
                    </div>
                </div>
            );

        });
    }
    render() {
        const {companiesData, loader, currentCityData} = this.props;
        
        return (
            <main>
                {companiesData.acf?(
                    <DefaultBanner title={`${locationHelper.translate('PARTNER_BANNER_TXT')}  ${commonHelper.toTitleCase(currentCityData[0].name.split("-").join(" "))}`} description={companiesData.acf.banner_description} background={companiesData.acf.banner_image}/>
                ):''}
                {(this.state.classifications.length && !loader)?(
                    <div className="container pt-3">
                        <SectionTitle title={locationHelper.translate('OUR_PARTNERS_IN')} titleCapitalized={currentCityData[0].name.split("-").join(" ")} />
                        <TitleTextBlock text={`${locationHelper.translate('OUR_PARTNERS_TXT1')}  ${currentCityData[0].name.split("-").join(" ")} ${locationHelper.translate('OUR_PARTNERS_TXT2')}`}/>

                        <section>
                            <div className="row mvl">
                                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div className="partners-list row">
                                        {this.renderResults()}
                                    </div>
                                </div>
                            </div>

                        </section>
                    </div>
                ):(
                    <main loader={loader ? "show": "hide"}><Loader/></main>
                )}

            </main>
        );
    }
}

function mapStateToProps(state){
	return {
        loader: state.loader,
        cities: state.cities,
        companiesData: state.companiesData,
        currentCity: state.currentCity,
        lang: state.lang,
        currentCityData: state.currentCityData
	}
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({showLoader, hideLoader, fetchCompaniesPageData, setBodyClass}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CompaniesPage);
