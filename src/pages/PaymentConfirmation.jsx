import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { URLCONSTANT, OXYCLEAN_PRICE } from "../actions/index";
import { withCookies } from 'react-cookie';
import {
    getAllCreditCard, isLiteWeightBooking, setBodyClass,locationConstants
} from "../actions";
import commonHelper from "../helpers/commonHelper"
import locationHelper from "../helpers/locationHelper"

import moment from 'moment';
import SectionTitle from "../components/SectionTitle";
import ContentSlider from "../components/ContentSlider";
import LatestBlogNews from "../components/LatestBlogNews";
import stringConstants from '../constants/stringConstants';
import {imgixReactBase, imgixWPBase, quality, relativeURL} from '../imgix';
var currentCurrency = "";

var current_city = "dubai";


class PaymentConfirmation extends React.Component {
    _isMounted = false;

    constructor(props) {
        super(props);
        this.state = {
            isLwb: false
        }
        var url_params = props.url_params;
        var serviceURL = url_params.params.slug;
        this.updateMetaData = this.updateMetaData.bind(this);
        this.getCookieName = this.getCookieName.bind(this);
        this.removeBookingData = this.removeBookingData.bind(this);
        this.titleText = this.titleText.bind(this);
        this.getCookieValue = this.getCookieValue.bind(this);
        this.removeCookie = this.removeCookie.bind(this);
        var serviceHeading = this.titleText(serviceURL);
        this.cleaningBookingSummery = this.cleaningBookingSummery.bind(this);
        this.paintingBookingSummery = this.paintingBookingSummery.bind(this);
        this.windowBookingSummery = this.windowBookingSummery.bind(this);
        this.poolBookingSummery = this.poolBookingSummery.bind(this);

        this.pestControlBookingSummery = this.pestControlBookingSummery.bind(this);

        this.deepCleaningBookingSummery = this.deepCleaningBookingSummery.bind(this);

        this.sofaCleaningBookingSummery = this.sofaCleaningBookingSummery.bind(this);

        this.carpetCleaningBookingSummery = this.carpetCleaningBookingSummery.bind(this);

        this.mattressCleaningBookingSummery = this.mattressCleaningBookingSummery.bind(this);

        this.waterTankBookingSummery = this.waterTankBookingSummery.bind(this);

        this.dryCleaningBookingSummery = this.dryCleaningBookingSummery.bind(this);

        this.handymanBookingSummery = this.handymanBookingSummery.bind(this);

        this.localMoveBookingSummery = this.localMoveBookingSummery.bind(this);

        
        let confirmationSummary = this.getCookieValue('confirmation_summary') || null;//cookies.get(confirmationSummaryCookieName) || {};

        let bookingData = this.getCookieValue('booking_data') || null;//cookies.get(bookingDatacookieName) || {};

        let submittedData = this.getCookieValue('submitted_data') || null;

        let {currentCityData} = props;

        currentCurrency = (typeof currentCityData != "undefined" && currentCityData.length) ? currentCityData[0].cityDto.countryDto.currencyDto.code : locationConstants.DEFAULT_CURRENCY;

        this.state = {
            confirmationSummary: confirmationSummary,
            bookingData: bookingData,
            submittedData: submittedData,
            serviceHeading: serviceHeading
        };
        

        //currentCurrency = locationHelper.getCurrentCurrency();
        //console.log("confirmationSummary", confirmationSummary, bookingData, submittedData);
    }
    updateMetaData() {
        //console.log('updating meta data');
        document.title = 'Thank you for using ServiceMarket!';
        var allMetaElements = document.getElementsByTagName('meta');
        for (var i = 0; i < allMetaElements.length; i++) {
            if (allMetaElements[i].getAttribute("name") == "description") {
                allMetaElements[i].setAttribute('content', 'Thank you for using ServiceMarket!');
                break;
            }
        }
    }
    titleText(serviceURL) {
        const serviceConstants = this.props.serviceConstants;

        var headingText = {},
            heading = "";

        headingText[URLCONSTANT.CLEANING_MAID_PAGE_URI] = "Thank you for booking a cleaner with servicemarket!";
        headingText[URLCONSTANT.DEEP_CLEANING_PAGE_URI] = "Thank you for booking a deep cleaning service";
        headingText[URLCONSTANT.SOFA_AND_UPHOLSTERY_CLEANING_PAGE_URI] = "Thank you for booking a sofa cleaning service";
        headingText[URLCONSTANT.CARPET_CLEANING_PAGE_URI] = "Thank you for booking a carpet cleaning service";
        headingText[URLCONSTANT.MATTRESS_CLEANING_PAGE_URI] = "Thank you for booking a mattress cleaning service";
        headingText[URLCONSTANT.POOL_CLEANING_PAGE_URI] = "Thank you for booking a pool cleaning service";
        headingText[URLCONSTANT.WINDOW_CLEANING_PAGE_URI] = "Thank you for booking a window cleaning service";
        headingText[URLCONSTANT.WATER_TANK_CLEANING_PAGE_URI] = "Thank you for booking a water tank cleaning service";
        headingText[URLCONSTANT.PEST_CONTROL_PAGE_URI] = "Thank you for booking pest control";
        headingText[URLCONSTANT.PAINTERS_PAGE_URI] = "Thank you for booking a painting service";
        headingText[URLCONSTANT.ELECTRICIAN_PAGE_URI] = "Thank you for booking a service";
        headingText[URLCONSTANT.AC_MAINTENANCE_PAGE_URI] = "Thank you for booking a service";
        headingText[URLCONSTANT.PLUMBING_PAGE_URI] = "Thank you for booking a service";
        headingText[URLCONSTANT.MAINTENANCE_PAGE_URI] = "Thank you for booking a service";
        headingText[URLCONSTANT.LAUNDRY_DRY_CLEANING] = "Thank you for booking"; //  "Thank you for booking a laundry/dry cleaning service";

        heading = (heading == "" && typeof headingText[serviceURL] != "undefined") ? headingText[serviceURL] : "";

        return heading != "" ? heading : "Thank you for booking";

    }
    getCookieName(cookie_name) {
        var url_params = this.props.url_params;

        var serviceURL = url_params.params.slug;

        var currentLanguage = url_params.params.lang;

        var currentPath = serviceURL;

        var current_city = url_params.params.city;

        var cookieName = currentLanguage + "." + current_city + "." + currentPath + "." + cookie_name;

        return cookieName;
    }
    removeBookingData() {
        const { cookies } = this.props;

        var bookingDatacookieName = this.getCookieName("booking_data");

        var confirmationSummaryCookieName = this.getCookieName("confirmation_summary");

        var submittedDataCookieName = this.getCookieName("submitted_data");

        //console.log("PaymentConfirmation", bookingDatacookieName, confirmationSummaryCookieName, submittedDataCookieName);

        /*cookies.remove(bookingDatacookieName, { path: '/' });

        cookies.remove(confirmationSummaryCookieName, { path: '/' });

        cookies.remove(submittedDataCookieName, { path: '/' });*/

        this.removeCookie("booking_data");
        this.removeCookie("confirmation_summary");
        this.removeCookie("submitted_data");
        this.removeCookie("coupon_data");
    }
    getCookieValue(name){
       const { cookies, rid } = this.props;
       let cookieName =  this.getCookieName(name);
       let isLocalStorageAvailable = commonHelper.isLocalStorageAvailable();
       if(isLocalStorageAvailable){
           return localStorage.getItem(cookieName) != null ? JSON.parse(localStorage.getItem(cookieName)) : null;
       }else{
        return cookies.get(cookieName) || null;
       }
       //return isLocalStorageAvailable ? JSON.parse(localStorage.getItem(cookieName)) : cookies.get(cookieName);
    }
    removeCookie(name){
        const { cookies, rid } = this.props;
        let cookieName =  this.getCookieName(name);
        let isLocalStorageAvailable = commonHelper.isLocalStorageAvailable();
        if(isLocalStorageAvailable){
            localStorage.removeItem(cookieName);
        }else{
            cookies.remove(cookieName, { path: '/' });
        }
    }
    componentDidMount() {
        this._isMounted = true;
        this.props.setBodyClass('paymentconfirmation');
        const { cookies, rid } = this.props;

        let {confirmationSummary} = this.state;

        current_city = this.props.current_city;

        var bookingDatacookieName = this.getCookieName("booking_data");

        var confirmationSummaryCookieName = this.getCookieName("confirmation_summary");

        var submittedDataCookieName = this.getCookieName("submitted_data");


        /*confirmationSummary = this.getCookieValue('confirmation_summary') || {};//cookies.get(confirmationSummaryCookieName) || {};

        bookingData = this.getCookieValue('booking_data') || {};//cookies.get(bookingDatacookieName) || {};

        submittedData = this.getCookieValue('submitted_data') || {};//cookies.get(submittedDataCookieName) || {};

        console.log("confirmationSummary", confirmationSummary, bookingData, submittedData);*/
        
        this.removeBookingData();

        this.updateMetaData();

        //currentCurrency = locationHelper.getCurrentCurrency();

        if (confirmationSummary.payment !== 'cash') {
            this.props.getAllCreditCard(this.props.signInDetails.access_token)
                .then((res) => {
                    if (this._isMounted) {
                        this.setState({
                            loader: false
                        })
                    }
                });
            this.setState({
                loader: true
            });
        }
    }
    pestControlBookingSummery() {
        var moreSummery = "";

        // console.log("pestControlBookingSummery", submittedData);
        let {bookingData, confirmationSummary, submittedData} = this.state;
        
        var {
            typeOfPest,
            homeType,
            numberOfUnits,
            booking_date,
            discountData,
            voucherCode,
        } = bookingData;

        //if (submittedData.length) {
        //var newSubmittedData = submittedData[0];

        let room = numberOfUnits.value == "0" ? "Studio" : numberOfUnits.value;

        let homeTypeValue = homeType.value;

        let PackageType = room != "Studio" ? room + " BR " + homeTypeValue : room + " " + homeTypeValue;

        moreSummery = (<React.Fragment>
            <tr>
                <td className="gray-background heading-text" colSpan="6">Service</td>
                <td className="gray-background title-case"
                    colSpan="6">{typeOfPest.label + " pest control"}</td>
            </tr>
            <tr>
                <td className="heading-text" colSpan="6">Selected package</td>
                <td colSpan="6">{PackageType}</td>
            </tr>
        </React.Fragment>);
        //}
        return moreSummery;
    }
    componentWillUnmount() {
        this._isMounted = false;
    }
    sofaCleaningBookingSummery() {
        var moreSummery = (<React.Fragment><tr>
            <td className="gray-background heading-text" colSpan="6">Service</td>
            <td className="gray-background"
                colSpan="6">Sofa Cleaning</td>
        </tr></React.Fragment>);
        return moreSummery;
    }
    mattressCleaningBookingSummery() {
        var moreSummery = (<React.Fragment><tr>
            <td className="gray-background heading-text" colSpan="6">Service</td>
            <td className="gray-background"
                colSpan="6">Mattress Cleaning</td>
        </tr></React.Fragment>);
        return moreSummery;
    }
    carpetCleaningBookingSummery() {
        var moreSummery = (<React.Fragment><tr>
            <td className="gray-background heading-text" colSpan="6">Service</td>
            <td className="gray-background"
                colSpan="6">Carpet Cleaning</td>
        </tr></React.Fragment>);
        return moreSummery;
    }
    deepCleaningBookingSummery() {
        let {bookingData, confirmationSummary, submittedData} = this.state;
        var {
            numberOfUnits,
            homeType,
            numberOfUnits,
            field_furnished,
            otherServices
        } = bookingData;

        var moreSummery = "";

        let room = numberOfUnits.value == "0" ? "Studio" : numberOfUnits.value;

        let homeTypeValue = homeType.value;

        let PackageType = room != "Studio" ? room + " BR " + homeTypeValue : room + " " + homeTypeValue;

        moreSummery = (<React.Fragment>
            <tr>
                <td className="gray-background heading-text" colSpan="6">Service</td>
                <td className="gray-background"
                    colSpan="6">Deep Cleaning</td>
            </tr>
            <tr>
                <td className="heading-text" colSpan="6">Selected package</td>
                <td colSpan="6">{PackageType}</td>
            </tr>
            <tr>
                <td className="heading-text" colSpan="6">Furnished</td>
                <td colSpan="6">{field_furnished.label}</td>
            </tr>
            {
                otherServices.length ? (<tr>
                    <td className="heading-text" colSpan="6">Additional services</td>
                    <td colSpan="6">{otherServices.map(item => item.label).join(", ")}</td>
                </tr>) : ''
            }
        </React.Fragment>);

        return moreSummery;
    }
    waterTankBookingSummery() {
        var moreSummery = (<React.Fragment>
            <tr>
                <td className="gray-background heading-text" colSpan="6">Service</td>
                <td className="gray-background"
                    colSpan="6">Water tank cleaning</td>
            </tr>
        </React.Fragment>);
        return moreSummery;
    }
    dryCleaningBookingSummery() {
        var moreSummery = (<React.Fragment>
            <tr>
                <td className="gray-background heading-text" colSpan="6">Service</td>
                <td className="gray-background"
                    colSpan="6">Laundry/dry cleaning</td>
            </tr>
        </React.Fragment>);
        return moreSummery;
    }
    localMoveBookingSummery() {
        // console.log("submittedData", submittedData);
        var movingBookingData = {}
        if (submittedData.length) {
            movingBookingData = submittedData[0];
        }
        var {
            Apartment_Villa_Number,
            Area,
            Email,
            First_Name,
            Last_Name,
            Lead_Source,
            Move_from,
            Move_provider_category,
            Move_size,
            Move_to,
            Need_a_handyman,
            Orginal_Price1,
            Payment_Status,
            Phone,
            Price,
            Requester_comments,
            Required_on,
            Required_when,
            Residence_Types,
            Rooms,
            Service,
            Subtotal,
            Tax,
            UserId,
            isCouponApplied
        } = movingBookingData;


        //var movingSize = typeOfMove.value == "full_move" ? moving_size.value : 'Small Move';
        var moreSummery = (<React.Fragment>
            <tr>
                <td className="gray-background heading-text" colSpan="6">Service</td>
                <td className="gray-background" colSpan="6">Moving</td>
            </tr>
            <tr>
                <td className="heading-text" colSpan="6">Moving size</td>
                <td colSpan="6">{Move_size}</td>
            </tr>
            <tr>
                <td className="gray-background heading-text" colSpan="6">Name</td>
                <td className="gray-background" colSpan="6">{First_Name + " " + Last_Name}</td>
            </tr>
            <tr>
                <td className="heading-text" colSpan="6">Email</td>
                <td colSpan="6">{Email}</td>
            </tr>
            <tr>
                <td className="gray-background heading-text" colSpan="6">Package</td>
                <td className="gray-background" colSpan="6">{Move_provider_category}</td>
            </tr>
            <tr>
                <td className="heading-text" colSpan="6">Moving from</td>
                <td colSpan="6">{Move_from}</td>
            </tr>
            <tr>
                <td className="gray-background heading-text" colSpan="6">Moving to</td>
                <td className="gray-background" colSpan="6">{Move_to}</td>
            </tr>
        </React.Fragment>);
        return moreSummery;
    }
    handymanBookingSummery() {
        const serviceConstants = this.props.serviceConstants;

        // console.log("handymanBookingSummery", bookingData);

        var summery = [];
        var moreSummery = "";
        let {bookingData, confirmationSummary, submittedData} = this.state;
        var {
            booking_date,
            typeOfHandyman,
            sub_service,
            homeType,
            numberOfUnits,
            tvSize,
            numberOfHours,
            discountData,
            voucherCode,
            payment,
            subtotal,
            vat,
            totalAfterDiscount,
            total,
            price,
            oxyclean,
            isHourlyRate
        } = bookingData;

        var subServiceLbl = typeof sub_service.label != "undefined" ? " - " + sub_service.label : "";

        if (typeOfHandyman.value == serviceConstants.SERVICE_ELECTRICIAN || typeOfHandyman.value == serviceConstants.SERVICE_PLUMBING) {
            subServiceLbl = "";
        }

        moreSummery = (<tr>
            <td className="gray-background heading-text" colSpan="6">Service</td>
            <td className="gray-background"
                colSpan="6">
                {typeOfHandyman.label + "" + subServiceLbl}
            </td>
        </tr>
        );

        var noOfHoursSummery = "";

        if (typeof numberOfHours.value != "undefined") {
            var hoursText = "";

            if (numberOfHours.id != 0) {
                var hours = parseInt(numberOfHours.value);
                hoursText = hours != 1 ? ' Hours' : ' Hour';
            } else {
                hours = numberOfHours.label;
            }

            noOfHoursSummery = (<tr>
                <td className="heading-text" colSpan="6">No.Of Hours</td>
                <td colSpan="6"> {hours + " " + hoursText} </td>
            </tr>
            );
        }


        var tvSummery = "";
        if (sub_service.value == serviceConstants.SERVICE_TV_MOUNTING) {
            tvSummery = (
                <tr>
                    <td className="gray-background heading-text" colSpan="6">TV Size</td>
                    <td className="gray-background"
                        colSpan="6">
                        {tvSize.label}
                    </td>
                </tr>);
        }
        var acSummery = "";
        var acOxyClean = "";

        if (sub_service.value == serviceConstants.SERVICE_AC_CLEANING) {

            var units = parseInt(numberOfUnits.value);

            var unitsText = units != 1 ? ' Units' : ' Unit';

            let homeTypeValue = homeType.value;

            let PackageType = units + " " + unitsText + " " + homeTypeValue;

            acSummery = (
                <React.Fragment>
                    <tr>
                        <td className="heading-text" colSpan="6">Selected package</td>
                        <td colSpan="6">{homeTypeValue + " - " + units + " " + unitsText}</td>
                    </tr>
                    <tr>
                        <td className="heading-text" colSpan="6">{homeTypeValue + " - " + units + " " + unitsText}</td>
                        <td colSpan="6">
                            <span>{currentCurrency + " " + numberOfUnits.price}</span>
                        </td>
                    </tr>

                </React.Fragment>
            );
            if (oxyclean.value == "yes") {

                var oxycleanPrice = units * OXYCLEAN_PRICE;

                acOxyClean = (
                    <tr>
                        <td colSpan="6" className="heading-text">Add-on: OxyClean<sup>TM</sup> for {units + " " + unitsText}</td>
                        <td colSpan="6">
                            <span>{currentCurrency + " " + oxycleanPrice}</span>
                        </td>
                    </tr>
                );
            }
        }

        return (<React.Fragment>{moreSummery}{noOfHoursSummery}{tvSummery}{acSummery}{acOxyClean}</React.Fragment>);
    }
    getHomeSizeLbl(submittedData) {

        var data = typeof submittedData.Number_of_units != "undefined" ? submittedData.Number_of_units : submittedData.Rooms;

        var room = data == "0" ? "Studio" : data;

        var PackageType = "";

        PackageType = room != "Studio" ? room + " BR " + submittedData.Residence_Types : room + " " + submittedData.Residence_Types;

        return PackageType;
    }
    paintingBookingSummery() {
        let {bookingData, confirmationSummary, submittedData} = this.state;
        var { typeOfPainting,
            homeType,
            numberOfUnits,
            fieldFurnished,
            fieldCeilingsPainted,
            colorToBePainted,
            colorAreNowOnYourWalls,
            additionalRooms,
            noOfCellings,
            duration } = bookingData;

        noOfCellings = parseInt(noOfCellings);

        //Additional Rooms

        var moreSummery = (<React.Fragment><tr>
            <td className="gray-background heading-text" colSpan="6">
                Old Walls Color
            </td>
            <td className="gray-background" colSpan="6">
                {colorAreNowOnYourWalls.label}
            </td>
        </tr>
            <tr>
                <td className="heading-text" colSpan="6">
                    New Wall Color
            </td>
                <td colSpan="6">
                    {colorToBePainted.label}
                </td>
            </tr>
            <tr>
                <td className="gray-background heading-text" colSpan="6">
                    Type of Home
            </td>
                <td className="gray-background" colSpan="6">
                    {homeType.label}
                </td>
            </tr>
            <tr>
                <td className="heading-text" colSpan="6">
                    Size of Home
            </td>
                <td colSpan="6">
                    {numberOfUnits.label}
                </td>
            </tr>
            <tr>
                <td className="gray-background heading-text" colSpan="6">
                    Additional Rooms
            </td>
                <td className="gray-background" colSpan="6">
                    {additionalRooms.map(item => item.label).join(", ")}
                </td>
            </tr>
            <tr>
                <td className="heading-text" colSpan="6">
                    Water Ceilings
            </td>
                <td colSpan="6">
                    {fieldCeilingsPainted.label}
                </td>
            </tr>
            {
                noOfCellings != 0 && (<tr>
                    <td className="heading-text" colSpan="6">
                        No.Of Ceilings
                </td>
                    <td colSpan="6">
                        {noOfCellings}
                    </td>
                </tr>)
            }
            <tr>
                <td className="gray-background heading-text" colSpan="6">
                    Home Furnished
                </td>
                <td className="gray-background" colSpan="6">
                    {fieldFurnished.label}
                </td>
            </tr>
            <tr>
                <td className="heading-text" colSpan="6">
                    Service booked
                </td>
                <td colSpan="6">
                    Full-home Interior Painting
                </td>
            </tr>
            <tr>
                <td className="gray-background heading-text" colSpan="6">
                    Duration
                </td>
                <td className="gray-background" colSpan="6">
                    {duration}
                </td>
            </tr>
        </React.Fragment>);

        return moreSummery;
    }
    poolBookingSummery() {
        let {bookingData, confirmationSummary, submittedData} = this.state;
        var {
            poolSize
        } = bookingData;
        var moreSummery = (typeof bookingData != "undefined" && bookingData != null) ? (<React.Fragment><tr>
            <td className="gray-background heading-text"
                colSpan="6">Weekday(s)
            </td>
            <td className="gray-background"
                colSpan="6">
                {
                   (typeof bookingData.field_multiple_times_week != "undefined" && bookingData.field_multiple_times_week.length != 0) ? bookingData.field_multiple_times_week.map(item => item.label).join(", ") : moment(confirmationSummary.date).format("dddd")
                }
            </td>
        </tr>
            <tr>
                <td className="heading-text" colSpan="6">
                    Pool Size
            </td>
                <td colSpan="6">
                    {poolSize.label}
                </td>
            </tr>
            <tr>
                <td className="gray-background heading-text" colSpan="6">Frequency
            </td>
                <td className="gray-background" colSpan="6">
                    2 times a week
            </td>
            </tr>
            <tr>
                <td className="heading-text" colSpan="6">
                    Service booked
            </td>
                <td colSpan="6">
                    Pool Cleaning
            </td>
            </tr>


        </React.Fragment>) : "";

        return moreSummery;
    }
    windowBookingSummery() {
        let {bookingData, confirmationSummary, submittedData} = this.state;
        var {
            windowSize
        } = bookingData;
        var moreSummery = (<React.Fragment><tr>
            <td className="gray-background heading-text"
                colSpan="6">Weekday(s)
            </td>
            <td className="gray-background"
                colSpan="6">
                {
                    moment(confirmationSummary.date).format("dddd")
                }
            </td>
        </tr>
            <tr>
                <td className="heading-text" colSpan="6">
                    Home Size
            </td>
                <td colSpan="6">
                    {windowSize.label}
                </td>
            </tr>
            <tr>
                <td className="gray-background heading-text" colSpan="6">Frequency
            </td>
                <td className="gray-background" colSpan="6">
                    Every 6 months
            </td>
            </tr>
            <tr>
                <td className="heading-text" colSpan="6">
                    Service booked
            </td>
                <td colSpan="6">
                    Window Cleaning
            </td>
            </tr>


        </React.Fragment>)

        return moreSummery;
    }
    cleaningBookingSummery() {
        let {bookingData, confirmationSummary, submittedData} = this.state;
        //console.log("bookingData",this.state, bookingData);
        var moreSummery = (typeof bookingData != "undefined" && ( bookingData != null && Object.keys(bookingData).length != 0)) ? (<React.Fragment><tr>
            <td className="gray-background heading-text"
                colSpan="6">Weekday(s)
            </td>
            <td className="gray-background"
                colSpan="6">
                {
                   ( typeof bookingData.field_multiple_times_week != "undefined" && bookingData.field_multiple_times_week.length != 0 )? bookingData.field_multiple_times_week.map(item => item.label).join(", ") : moment(confirmationSummary.date).format("dddd")
                }
            </td>
        </tr>
            <tr>
                <td className="gray-background heading-text"
                    colSpan="6">Frequency
                </td>
                <td className="gray-background"
                    colSpan="6">{bookingData.field_service_needed.label}
                </td>
            </tr>
            {typeof bookingData.hours_required != "undefined" &&
                <tr>
                    <td className="gray-background heading-text"
                        colSpan="6">Duration
                    </td>
                    <td className="gray-background"
                        colSpan="6">{bookingData.hours_required.label}
                    </td>
                </tr>
            }
            {typeof bookingData.number_of_cleaners != "undefined" &&
                <tr>
                    <td className="gray-background heading-text"
                        colSpan="6">No. of cleaners
                    </td>
                    <td className="gray-background"
                        colSpan="6">{bookingData.number_of_cleaners.label}
                    </td>
                </tr>
            }
            <tr>
                <td colSpan="6"
                    className="heading-text">Service
                    booked
                </td>
                <td colSpan="6">{confirmationSummary.service}</td>
            </tr>
            {typeof bookingData.field_own_equipments != "undefined" &&
                <tr>
                    <td className="gray-background heading-text"
                        colSpan="6">Cleaning material (at
                        additional price)
                    </td>
                    {bookingData.field_own_equipments.value == "yes" && (
                        <td className="gray-background"
                            colSpan="6">

                        </td>)}
                    {bookingData.field_own_equipments.value != "yes" &&
                        <td className="gray-background"
                            colSpan="6">Not included</td>}
                </tr>
            }
            {bookingData.hourlyRate != 0 && (<tr>
                <td className="gray-background heading-text"
                    colSpan="6">Total Price per hour
                    (per cleaner)
                </td>
                <td className="gray-background"
                    colSpan="6">
                    {currentCurrency + " " + bookingData.hourlyRate}
                </td>
            </tr>)}

            {typeof bookingData.other_services != "undefined" &&
                <React.Fragment>
                    <tr>
                        <td colSpan="6"
                            className="gray-background heading-text">
                            Additional services requested
                        </td>
                        <td colSpan="6"
                            className="gray-background">
                            {
                                bookingData.other_services.length == 0 && "No extra service is selected"
                            }
                            {
                                bookingData.other_services.length != 0 && bookingData.other_services.map(item => item.label).join(", ")
                            }
                        </td>
                    </tr>
                </React.Fragment>
            }
        </React.Fragment>) : "";
        return moreSummery;
    }

    render() {
        const { cookies, rid, landingData, currentCity, myCreditCardsData } = this.props;

        const {bookingData, confirmationSummary, submittedData} = this.state;
        var data = {};

        var url_params = this.props.url_params;

        var serviceURL = url_params.params.slug;

        // console.log("serviceURL", serviceURL);
        // console.log(landingData);

        let showSummaryTable = true; //( (typeof bookingData != "undefined" && ( bookingData != null && Object.keys(bookingData).length) ) && (typeof submittedData != "undefined" && ( submittedData != null && Object.keys(submittedData).length) ));

        console.log("showSummaryTable", showSummaryTable, bookingData, confirmationSummary, submittedData);

        if (landingData.length) {
            data = landingData[0];
        }
        var strikeTotal = false;

        if (confirmationSummary != null ) {
            var userCreditCards = (this.props.myCreditCardsData.data && this.props.myCreditCardsData.data.length) ? this.props.myCreditCardsData.data : [];

            var isDiscountApplied =  ( showSummaryTable && (typeof bookingData != "undefined" && typeof bookingData.discountData != "undefined" && typeof bookingData.discountData.error == "undefined") ) && Object.keys(bookingData.discountData).length ? true : false;

            //console.log("userCreditCards", userCreditCards);

            var discountData = ( showSummaryTable && (isDiscountApplied && typeof bookingData.couponCode != "undefined")) ? (<tr>
                <td colSpan="6" className="heading-text confirmation-total">Discount code: {bookingData.couponCode}</td>
                <td colSpan="6" className="confirmation-total">
                    <span>{commonHelper.getDiscountText(bookingData.discountData.discount, bookingData.discountData.discountType, currentCurrency)}</span>
                </td>
            </tr>) : "";


            if (isLiteWeightBooking(serviceURL) && showSummaryTable) {

                isDiscountApplied = (typeof bookingData != "undefined" && typeof bookingData.discountData != "undefined" && (typeof bookingData.discountData.success != "undefined" && bookingData.discountData.success)) && Object.keys(bookingData.discountData).length ? true : false;

            }

            var lwBdiscountData = ( showSummaryTable && (isDiscountApplied && (typeof bookingData.voucherCode != "undefined" && bookingData.voucherCode != ""))) ? (<tr>
                <td colSpan="6" className="heading-text confirmation-total">Discount code: {bookingData.voucherCode}</td>
                <td colSpan="6" className="confirmation-total">
                    <span>{bookingData.discountData.promoCodeDiscountText}</span>
                </td>
            </tr>) : "";

            if ((discountData != "" || lwBdiscountData != "") && isDiscountApplied) {
                strikeTotal = true;
            }

            //console.log(bookingData.discountData);

            var creditCardInfo = ( showSummaryTable && (bookingData.payment == "credit" && userCreditCards.length) ) ? (
                <React.Fragment>
                    <tr>
                        <td colSpan="6" className="heading-text">Credit Card
                            {<img src={commonHelper.getCardTypeImage(userCreditCards[0].cardPlatform)}
                                width="38" className="mb-1 ml-2" />}
                        </td>
                        <td colSpan="6">{'**** **** **** ' + userCreditCards[0].last4Char}</td>
                    </tr>
                    <tr>
                        <td className="gray-background" colSpan="12">Your card will ONLY be charged after the service
                            has been completed.
                        </td>
                    </tr>
                </React.Fragment>
            ) : (<React.Fragment>
                <tr>
                    <td colSpan="6" className="heading-text">Payment Method</td>
                    <td colSpan="6">Cash on delivery</td>
                </tr>
            </React.Fragment>);
            var isLwb = this.state.isLwb;

            var moreSummery = "";
            if( showSummaryTable ){
                if (serviceURL == URLCONSTANT.CLEANING_MAID_PAGE_URI) {
                    moreSummery = this.cleaningBookingSummery();
                }
                else if (serviceURL == URLCONSTANT.PAINTERS_PAGE_URI) {
                    moreSummery = this.paintingBookingSummery();
                }
                else if (serviceURL == URLCONSTANT.WINDOW_CLEANING_PAGE_URI) {
                    moreSummery = this.windowBookingSummery();
                }
                else if (serviceURL == URLCONSTANT.POOL_CLEANING_PAGE_URI) {
                    moreSummery = this.poolBookingSummery();
                }
                else if (serviceURL == URLCONSTANT.PEST_CONTROL_PAGE_URI) {
                    moreSummery = this.pestControlBookingSummery();
                }
                else if (serviceURL == URLCONSTANT.DEEP_CLEANING_PAGE_URI) {
                    moreSummery = this.deepCleaningBookingSummery();
                }
                else if (serviceURL == URLCONSTANT.SOFA_AND_UPHOLSTERY_CLEANING_PAGE_URI) {
                    moreSummery = this.sofaCleaningBookingSummery();
                }
                else if (serviceURL == URLCONSTANT.CARPET_CLEANING_PAGE_URI) {
                    moreSummery = this.carpetCleaningBookingSummery();
                }
                else if (serviceURL == URLCONSTANT.MATTRESS_CLEANING_PAGE_URI) {
                    // console.log("calling here 1233");
                    moreSummery = this.mattressCleaningBookingSummery();
                }
                else if (serviceURL == URLCONSTANT.WATER_TANK_CLEANING_PAGE_URI) {
                    moreSummery = this.waterTankBookingSummery();
                }
                else if (serviceURL == URLCONSTANT.LAUNDRY_DRY_CLEANING) {
                    moreSummery = this.dryCleaningBookingSummery();
                }
                else if (
                    serviceURL == URLCONSTANT.MAINTENANCE_PAGE_URI ||
                    serviceURL == URLCONSTANT.NOON_TV_INSTALLATION_PAGE_URI ||
                    serviceURL == URLCONSTANT.ELECTRICIAN_PAGE_URI ||
                    serviceURL == URLCONSTANT.PLUMBING_PAGE_URI ||
                    serviceURL == URLCONSTANT.AC_MAINTENANCE_PAGE_URI
                ) {
                    moreSummery = this.handymanBookingSummery();
                }
                else if (serviceURL == URLCONSTANT.LOCAL_MOVER_PAGE_URI) {
                    moreSummery = this.localMoveBookingSummery();
                }
            }
            var isLWB = (isLiteWeightBooking(serviceURL) && serviceURL != URLCONSTANT.LAUNDRY_DRY_CLEANING);

            const isWalletAvailable = this.props.userProfile && this.props.userProfile.userWallet && this.props.userProfile.userWallet.currency.code === currentCurrency && this.props.userProfile.userWallet.totalAmount > 0 ? true : false;
            
            if (isWalletAvailable) {
                const walletAmount = confirmationSummary.walletAmount;
                const walletCurrency = this.props.userProfile.userWallet.currency.code;

                if (walletCurrency === currentCurrency && walletAmount !== 'N/A' && walletAmount > 0) {

                    //confirmationSummary.total = confirmationSummary.total - walletAmount > 0 ? confirmationSummary.total - walletAmount : 0.00;
                }
            }
            confirmationSummary.total = confirmationSummary.total < 0 && isWalletAvailable ? 0.00 : confirmationSummary.total;
            var totalPriceText = <span>{currentCurrency + " " + confirmationSummary.total} </span>;

            if (isLWB && (typeof bookingData.isHourlyRate != "undefined" && bookingData.isHourlyRate)) {
                totalPriceText = <span>{currentCurrency + " " + confirmationSummary.total} - per hour</span>;
            }
            //console.log("showSummaryTable", showSummaryTable);
            let subtotalTxt = showSummaryTable ? (confirmationSummary != null && typeof confirmationSummary.subtotal != "undefined") && ( !Number.isInteger(confirmationSummary.subtotal) ) ? (confirmationSummary.subtotal).toFixed(2) : confirmationSummary.subtotal : "";

            let isDryCleaning = serviceURL == URLCONSTANT.LAUNDRY_DRY_CLEANING;
            return (
                <main className="confirmation">
                    <div className="container ">
                        <div className="journey-row row">
                            <div className="region region-content">
                                <div id="block-system-main" className="block block-system">
                                    <div className="content">
                                        <section>
                                            <div className="request-submission">
                                                <div className="confirmation-container">
                                                    <div className="confirmation-header mth mbh pbm">
                                                        <div className="row show-grid">
                                                            <div className="col-md-12">
                                                                <p className="text-center mvm">
                                                                    <img src="/dist/images/sm-user-icon.png" width="154" />
                                                                </p>
                                                                <h2 className="mvm text-center link-text uppercase">
                                                                    <strong>{this.state.serviceHeading}</strong>
                                                                </h2>
                                                                <div className="reference-text">
                                                                    <p className="text-center mtm">Your reference
                                                                        ID: </p>
                                                                    <p className="text-center mbm text-warning"><span
                                                                        className="request-id">{rid}</span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="row booking-confirmation-info pbm">
                                                        { showSummaryTable ? (<div className="col-xs-12 col-sm-12 col-lg-6 col-md-6">
                                                            <div className="booking-summary-table">
                                                                <section className="booking-cart">
                                                                    <div className="table-responsive">
                                                                        <table className="table table-bordered table-striped">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td className="gray-background heading-text"
                                                                                        colSpan="6">Booking Number
                                                                                </td>
                                                                                    <td className="gray-background"
                                                                                        colSpan="6">{rid}
                                                                                    </td>
                                                                                </tr>
                                                                                { isDryCleaning && (bookingData != null && typeof bookingData.booking_date !="undefined" ) ? (<React.Fragment>
                                                                                    <tr>
                                                                                        <td colSpan="6" className="heading-text">
                                                                                            Pickup time
                                                                                        </td>
                                                                                        <td colSpan="6">
                                                                                            <span> { bookingData.booking_date+", "+moment(bookingData.booking_date + " " + bookingData.booking_time.value, 'YYYY-MM-DD h:mm:ss a').format('h A')}</span>
                                                                                            <div className="small mt-2">Please allow up to 60 min for the driver to reach your location</div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td className="gray-background heading-text" colSpan="6">
                                                                                            Delivery preference
                                                                                         </td>
                                                                                        <td className="gray-background" colSpan="6">
                                                                                            {bookingData.dropOff.value == 24 ? bookingData.dropOff.label+" "+bookingData.dropOff.desc : bookingData.dropOff.label+" - "+bookingData.dropOff.desc}
                                                                                        </td>
                                                                                    </tr>
                                                                                </React.Fragment>) : (<React.Fragment>
                                                                                    <tr>
                                                                                        <td colSpan="6" className="heading-text">
                                                                                        { isDryCleaning ? "Pickup date" : "Date" }
                                                                                        </td>
                                                                                        <td colSpan="6">{
                                                                                            //!isLWB ? moment(confirmationSummary.date).format('DD-MM-YYYY') : confirmationSummary.date
                                                                                            confirmationSummary.date
                                                                                        }</td>
                                                                                    </tr>
                                                                                    {typeof confirmationSummary.time != "undefined" &&
                                                                                        (<tr>
                                                                                            <td className="gray-background heading-text"
                                                                                                colSpan="6">Preferred time
                                                                                        </td>
                                                                                            <td className="gray-background"
                                                                                                colSpan="6">{confirmationSummary.time}
                                                                                            </td>
                                                                                        </tr>)
                                                                                    }
                                                                                </React.Fragment>)}
                                                                                

                                                                                {moreSummery}
                                                                                { ( showSummaryTable && serviceURL != URLCONSTANT.LAUNDRY_DRY_CLEANING ) ? (<React.Fragment>
                                                                                    <tr>
                                                                                        <td className="gray-background heading-text"
                                                                                            colSpan="12">Payment Information
                                                                                    </td>
                                                                                    </tr>
                                                                                    {creditCardInfo}
                                                                                    <tr>
                                                                                        <td colSpan="6"
                                                                                            className="gray-background heading-text">
                                                                                            Subtotal
                                                                                    </td>
                                                                                        <td colSpan="6"
                                                                                            className="gray-background">
                                                                                            <span className={strikeTotal ? "strike" : ""}>{currentCurrency + " " + subtotalTxt} </span>
                                                                                        </td>
                                                                                    </tr>

                                                                                {isDiscountApplied && discountData}
                                                                                {isDiscountApplied && lwBdiscountData}
                                                                                {currentCurrency == "AED" && (<tr>
                                                                                    <td colSpan="6"
                                                                                        className="gray-background heading-text">
                                                                                        VAT
                                                                                </td>
                                                                                    <td colSpan="6"
                                                                                        className="gray-background">
                                                                                        <span>{currentCurrency + " " + bookingData.vat} </span>
                                                                                    </td>
                                                                                </tr>)}
                                                                                {confirmationSummary.walletAmount ?
                                                                                    <tr>
                                                                                        <td colSpan="6"
                                                                                            className="gray-background heading-text">
                                                                                            {stringConstants.WALLET_BOOKING_DETAIL_TXT}
                                                                                        </td>
                                                                                        <td colSpan="6"
                                                                                            className="gray-background">
                                                                                            <span className={strikeTotal ? "strike" : ""}>{currentCurrency + ' ' + confirmationSummary.walletAmount} </span>
                                                                                        </td>
                                                                                    </tr> : ''}
                                                                                <tr>
                                                                                    {(!isLWB) ?
                                                                                        (<td colSpan="6"
                                                                                            className="gray-background heading-text">
                                                                                            Total Price <br /> (due at time of delivery)
                                                                                </td>) : (<td colSpan="6" className="gray-background heading-text">
                                                                                            Cost estimate
                                                                                </td>)
                                                                                    }
                                                                                    <td colSpan="6" className="gray-background">
                                                                                        {totalPriceText}
                                                                                    </td>
                                                                                </tr></React.Fragment>) : '' }
                                                                                {serviceURL == URLCONSTANT.LAUNDRY_DRY_CLEANING ? (<React.Fragment>
                                                                                    <tr>
                                                                                        <td className="gray-background heading-text"
                                                                                            colSpan="12">Payment Information
                                                                                        </td>
                                                                                    </tr>
                                                                                    {creditCardInfo}
                                                                                    {isDiscountApplied && discountData}
                                                                                    {isDiscountApplied && lwBdiscountData}
                                                                                    <tr>
                                                                                        <td colSpan="6"
                                                                                            className="gray-background heading-text">
                                                                                            Total Price <br /> (due at time of delivery)
                                                                                        </td>
                                                                                        <td colSpan="6" className="gray-background heading-text">
                                                                                            <div className="mt-2">
                                                                                                <p className="text-primary small mb-1"><sup>*</sup> Prices confirmed on pickup.</p>
                                                                                                { isDiscountApplied ? (<p className="text-primary small mb-1"><sup>*</sup> Discount will be calculated after pickup.</p>) : ''}
                                                                                                { bookingData.payment == "cash" ? (<p className="text-primary small"><sup>*</sup> Only be charged after delivery.</p>) : ''}
                                                                                                
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </React.Fragment>) : ''}
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </section>
                                                            </div>
                                                        </div>) : '' }
                                                        <div className={showSummaryTable ? "col-xs-12 col-sm-12 col-lg-6 col-md-6 confirmation-info mbh" : "col mbh confirmation-info" }>

                                                            <div className="what-happens-next mb-3">
                                                                <h3>What happens next?</h3>
                                                                {isLWB ? (<ul>
                                                                    { /*<li>We will send you an email with the name of your service provider.</li> */}
                                                                    <li>We will send you an email once your booking is confirmed.</li>
                                                                    <li>A ServiceMarket representative will be in touch with you shortly to confirm all the details of the service you've requested</li>
                                                                    <li>The technician(s) will come to your home to complete the service.
                                                                    </li>
                                                                    { (showSummaryTable && bookingData.payment == "cash") ? (
                                                                        <li>You will pay the technicians in cash when the service is completed.</li>) : ''}
                                                                    </ul>) : (<ul>
                                                                    <li>We will send you an email once your booking is confirmed.</li>
                                                                    <li>A Customer Service agent will be in touch with you to confirm all the details.
                                                                    </li>
                                                                    {
                                                                        serviceURL == URLCONSTANT.CLEANING_MAID_PAGE_URI ? (<li>
                                                                            The cleaner will come to your home to deliver the service.
                                                                        </li>) : ''
                                                                    }
                                                                    { ( showSummaryTable && bookingData.payment == "cash" ) ? (
                                                                        <li>You will pay cash on delivery.</li>) : ''}
                                                                </ul>)}
                                                                {
                                                                    !isLwb ? (<div className="profile-created">
                                                                        <p>In the meantime, you can easily view your bookings and quotes in your
                                                                            &nbsp;<a href="/en/profile#bookings" className="link-text">ServiceMarket account</a>.
                                                                        </p>
                                                                    </div>) : ''
                                                                }
                                                            </div>
                                                            <div className="need-assistance mt-3">
                                                                <h3>Need any assistance?</h3>
                                                                <p className="mtm mbm">You can reach us on &nbsp;<a href="tel://0097144229639" className="ltr d-inline-block">+971 4 4229639</a>
                                                                    <span className="uae-location phone-no-link"> &nbsp;any day of the week between 8am - 8pm and 9am - 6pm on Fridays.</span>
                                                                </p>
                                                                <p className="mbm">Or you can email us on <a
                                                                    href="mailto:bookings@servicemarket.com">bookings@servicemarket.com</a>.
                                                                </p>
                                                            </div>
                                                            <div className="container-fluid after-header-apps mt-3">
                                                                <div className="row">
                                                                    <div className="col-xs-12 pvl download-app">
                                                                        <h3 className="mb-3">The easiest way to cancel or modify your booking is through our app</h3>
                                                                        <a href="https://servicemarket.onelink.me/R9tZ/47e24049" className="ios-download d-inline-block mr-3">
                                                                            <img src={imgixReactBase + "/dist/images/apple-itunes.png?fit=crop&w=150&auto=format,compress"} alt="Download iOS app" />
                                                                        </a>
                                                                        <a href="https://servicemarket.onelink.me/R9tZ/2bf82707" className="google-play d-inline-block">
                                                                            <img src={imgixReactBase + "/dist/images/google-play.png?fit=crop&w=150&auto=format,compress"} alt="Download iOS app" />
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="is_used_logged_in" value="0" />
                            <input type="hidden" id="hidden-login" value="" />
                        </div>
                        {
                            data.acf ? (
                                <div className={"py-3"}>
                                    <SectionTitle title="Related services" subTitle={data.acf.recommended_for_you_static.recommended_main_text} />
                                    {data.acf.recommended_for_you && <ContentSlider items={data.acf.recommended_for_you} currentCity={currentCity} />}
                                    {data.acf.posts && data.acf.posts.length > 0 && <SectionTitle title="ServiceMarket Blog" subTitle="Everything you need to know about organising your home, life and more" parentClass="row mb-4" childClass="col px-4 px-md-3" />}
                                    {data.acf.posts && <LatestBlogNews items={data.acf.posts} />}
                                </div>
                            ) : ''
                        }
                    </div>
                </main>
            );
        }else{
            return (<div>Session Out</div>);
        }

    }
}

function mapStateToProps(state) {
    return {
        signInDetails: state.signInDetails,
        myCreditCardsData: state.myCreditCardsData,
        current_city: state.currentCity,
        currentCityData: state.currentCityData,
        serviceConstants: state.serviceConstants,
        landingData: state.landingConfirmationData,
        currentCity: state.currentCity,
        userProfile: state.userProfile
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ getAllCreditCard, setBodyClass }, dispatch);
}

export default withCookies(connect(mapStateToProps, mapDispatchToProps)(PaymentConfirmation));

