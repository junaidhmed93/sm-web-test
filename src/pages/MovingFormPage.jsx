import React from "react";
import FormFieldsTitle from "../components/FormFieldsTitle";
import CheckRadioBoxInput from "../components/Form_Fields/CheckRadioBoxInput";
import DatePick from "../components/Form_Fields/DatePick";
import TimePicker from "../components/Form_Fields/TimePicker";
import TextareaInput from "../components/Form_Fields/TextareaInput";
import PaymentMethods from "../components/Form_Fields/PaymentMethods";
import SubmitBooking from "../components/Form_Fields/SubmitBooking";
import BookingSummary from "../components/Form_Fields/BookingSummary";
import BookNextStep from "../components/Form_Fields/BookNextStep";
import FormTitleDescription from "../components/FormTitleDescription";
import ContactDetailsStep from "../components/ContactDetailsStep";

class MovingFormPage extends React.Component{

    constructor(props) {
        super();

        this.state = {
            show_more_option : false,
            booking_time: '',
            field_service_needed: "66",
            hours_required: "4",
            number_of_cleaners: "4",
            field_own_equipments: "No",
            other_services:[],
            input_email:'',
            input_phone:'',
            input_name:'',
            input_last_name:'',
            input_address_city:'',
            input_address_area:'',
            input_address_area_building_name:'',
            input_address_area_building_apartment:'',
            payment: 'credit'

        }
        this.ContactDetails = this.ContactDetails.bind(this);
        this.CleaningRequest = this.CleaningRequest.bind(this);
        this.handleShowMoreOptionChange = this.handleShowMoreOptionChange.bind(this);
        this.handleCustomChange = this.handleCustomChange.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleDropDownChange = this.handleDropDownChange.bind(this);
        this.currentStep = this.currentStep.bind(this);
        this.moveNext = this.moveNext.bind(this);
    }
    currentStep(){
        return this.props.formCurrentStep;
    }
    moveNext(step){
        this.setState({
            "currentStep": parseInt(step)
        });
        this.props.moveNext(parseInt(step));
    }
    handleShowMoreOptionChange(){
        this.setState({
            show_more_option: !this.state.show_more_option
        });
    }
    handleCustomChange(name, value){
        this.setState({
            booking_time: value
        });
    }
    handleInputChange(name, value){
        this.setState({
            [name]: value
        });

        //console.log(name, value);
    }
    handleDropDownChange(name, value){
        // console.log(name, value);
    }
    CleaningRequest(){
        var moveTypes = [
            {id: 1, value: "full", label: "Moving my full home"},
            {id: 2, value: "partial", label: "Moving a few items"}
        ];
        var cleaning_frequency = [
            {id: "66", value: "66", label: "Every week"},
            {id: "67", value: "67", label: "Every 2 weeks"},
            {id: "65", value: "65", label: "One time only"},
            {id: "240", value: "240", label: "Several times a week"}
        ];

        var number_of_cleaners = [
            {id: 1, value: "1", label: "1 Cleaner"},
            {id: 2, value: "2", label: "2 Cleaners"},
            {id: 3, value: "3", label: "3 Cleaners"},
            {id: 4, value: "4", label: "4 Cleaners"}
        ];
        var hours_required = [
            {id: 2, value: "2", label: "2 hours"},
            {id: 3, value: "3", label: "3 hours"},
            {id: 4, value: "4", label: "4 hours"},
            {id: 5, value: "5", label: "5 hours"},
            {id: 6, value: "6", label: "6 hours"},
            {id: 7, value: "7", label: "7 hours"}
        ];
        var field_own_equipments = [
            {id: 'yes', value: "Yes", label: "Yes"},
            {id: 'no', value: "No", label: "No"}
        ];

        var other_services = [
            {id: 'ironing', value: "ironing", label: "Ironing"},
            {id: 'yes', value: "inside-window-cleaning", label: "Interior window cleaning "},
        ];

        var show_more_option = [
            {id: 'show-more', value: "no", label: "Show more options"}
        ];

        var isShowMore = this.state.show_more_option;

        var service_needed_value = this.state.field_service_needed;

        var hours_required_value = this.state.hours_required;

        var number_of_cleaners_value = this.state.number_of_cleaners;


        var field_own_equipments_value = this.state.field_own_equipments;

        var other_services_values = this.state.other_services;

        //other_services
        var currentStepClass = this.currentStep() === 1 ? '' : "d-none";

        return (
            <div id="section-request-form" className={currentStepClass}>
                <section>
                    <FormFieldsTitle title="How often do you need cleaning?" />
                    <CheckRadioBoxInput InputType="radio" name="field_service_needed" inputValue={service_needed_value} items={cleaning_frequency} onInputChange={this.handleInputChange}  childClass="col-6 col-sm-3 mb-3 d-flex" />
                </section>
                <section className="netflix-cleaning-message-mobile mb-4">
                    <div className="row">
                        <div className="col">
                            Save money when you sign up for a monthly subscription. <a href="book-online/subscription">Click Here</a>
                        </div>
                    </div>
                </section>
                <section>
                    <FormFieldsTitle title="How many cleaners do you need?" />
                    <CheckRadioBoxInput InputType="radio" name="number_of_cleaners" inputValue={number_of_cleaners_value} items={number_of_cleaners} onInputChange={this.handleInputChange} childClass="col-6 col-sm-3 mb-3 d-flex" />
                </section>
                <section>
                    <FormFieldsTitle title="How long should they stay?" />
                    <CheckRadioBoxInput InputType="radio" name="hours_required" inputValue={hours_required_value} items={hours_required} onInputChange={this.handleInputChange} childClass="col-6 col-sm-2 mb-3 d-flex" />
                </section>
                <section>
                    <FormFieldsTitle title="Do you need cleaning materials? " />
                    <CheckRadioBoxInput InputType="radio" name="field_own_equipments" inputValue={field_own_equipments_value} items={field_own_equipments} onInputChange={this.handleInputChange} childClass="col-6 col-sm-3 mb-3 d-flex" />
                </section>
                <section>
                    <FormFieldsTitle title="Pick your start date and time" />
                    <div className="row mb-4">
                        <DatePick name="move_date"  />
                        <TimePicker name="booking_time" onInputChange={ this.handleCustomChange }  />
                    </div>
                </section>
                <section>
                    <FormFieldsTitle title="Any special requirements or comments? (Optional)" />
                    <div className="row mb-4">
                        <div className="col-6 col-sm-4 mb-3 d-flex">
                            <label className="border rounded btn-block pointer">
                                <input name="show_more_option" className="d-none" value={this.state.show_more_option} onChange={(e) => this.handleShowMoreOptionChange()} type="checkbox"  />
                                <span className="btn-radio">Show more options</span>
                            </label>
                        </div>
                    </div>
                </section>
                <section className={!isShowMore && "d-none"} >
                    <FormFieldsTitle title="Do you need ironing or window cleaning?" />
                    <CheckRadioBoxInput InputType="checkbox" inputValue={other_services_values} name="other_services" items={other_services}  childClass="col-6 col-sm-4 mb-3 d-flex" />
                    <FormFieldsTitle title="Any special instructions" />
                    <div className="row mb-4">
                        <div className="col mb-3">
                            <TextareaInput name="details" placeholder="Example: The keys are under the door mat, and the money is on the kitchen counter."  />
                        </div>
                    </div>
                </section>
                <section>
                    <BookNextStep title="Contact Details" moveNext={this.moveNext} toStep="2" />
                </section>
            </div>
        )
    }
    ContactDetails(){
        var cityOptions = [{id: 1, value: "1", label: "Dubai"}, {id: 2, value: "2", label: "Abu Dhabi"}, {id: 3, value: "3", label: "Sharjah"}];
        var currentStepClass = this.currentStep() === 2 ? '' : "d-none";
        //var currentStepClass =  ''
        var isLocationDetailShow = "no";

        var contact_details = {
            input_email:this.state.input_email,
            input_phone:this.state.input_phone,
            input_name:this.state.input_name,
            input_last_name:this.state.input_last_name
        }

        return (
            <div id="personal-information-form" className={currentStepClass}>
                <ContactDetailsStep cityOptions={cityOptions} isLocationDetailShow={isLocationDetailShow} contact_details={contact_details} handleDropDownChange={this.handleInputChange} handleInputChange={this.handleInputChange}/>
                <section>
                    <BookNextStep title="Select payment method" moveNext={this.moveNext} toStep="3" />
                </section>
            </div>
        )
    }
    PaymentSection(){
        var payment = this.state.payment;
        var currentStepClass = this.currentStep() === 3 ? '' : "d-none";
        //var currentStepClass ='';
        var paymentTypes = [
            {id: 0, value: "credit", label: "Credit card", desc: "Get 10% off on first cleaning when pay by card", image: "visa.png", text: "Verify your card now, pay only when service is delivered. Your card will be verified for 1 AED and then the 1 AED will be reversed"},
            {id: 1, value: "cash", label: "Cash on delivery", desc: "100% Secure, Easy payment, Trusted partners", image: "money.svg", text: "No card? No problem, Just pay our cleaners cash when the job is done."}];
        return (
            <div id="payment-method-form" className={currentStepClass}>
                <section className="payment">
                    <PaymentMethods items={paymentTypes} name="payment" inputValue={payment} onInputChange={this.handleInputChange} />
                </section>
            </div>
        );
    }
    render(){
        return(
            <React.Fragment>
                {this.CleaningRequest()}
                {this.ContactDetails()}
                {this.PaymentSection()}
            </React.Fragment>
        )
    }
}

export default MovingFormPage;
