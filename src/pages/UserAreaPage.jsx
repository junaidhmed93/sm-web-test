import React from "react";
import Loader from "../components/Loader";
import SocialButton from "../components/SocialButton";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { showLoader, hideLoader, setSignIn, scrollToTop, setUserProfile } from "../actions/index";
import CreditCardManagement from '../components/CreditCardManagement';
import MyBookings from '../components/MyBookings';
import MyQuotes from '../components/MyQuotes';
import MyProfile from '../components/MyProfile';
import WalletComponent from '../components/WalletComponent';
import { imgixReactBase, quality } from '../imgix';
import { withCookies } from 'react-cookie';
import qs from 'query-string';
import staticVariables from '../../src/staticVariables';
import stringConstants from '../constants/stringConstants';
import locationHelper from "../helpers/locationHelper";

class UserAreaPage extends React.Component {

    constructor(props) {
        super(props);
        if (this.props.location.hash === '') {
            this.props.location.hash = '#bookings';
        }
        const { cookies } = this.props;
        var register_credit_card = cookies.get('register_credit_card');
        if (register_credit_card === "1") {
            this.props.location.hash = '#creditcard';
            cookies.set('register_credit_card', 0, { path: '/' });
        }
        // console.log(this.props.location.hash);
        this.state = {
            page: this.props.location.hash,
            selectedTab: 0,
        }

        this.hashChangeHandler = this.hashChangeHandler.bind(this);
        this.changeTab = this.changeTab.bind(this);
        this.updateMetaData = this.updateMetaData.bind(this);
        this.locationHelperInit = locationHelper.init.bind(this);
        this.locationHelperInit();
        // console.log(qs.parse(this.props.location.search, { ignoreQueryPrefix: true }));
    }

    updateMetaData() {
        // console.log('updating meta data');
        document.title = 'ServiceMarket';
        var allMetaElements = document.getElementsByTagName('meta');
        for (var i = 0; i < allMetaElements.length; i++) {
            if (allMetaElements[i].getAttribute("name") == "description") {
                allMetaElements[i].setAttribute('content', 'ServiceMarket');
                break;
            }
        }
    }
    componentDidMount() {
        window.addEventListener("hashchange", this.hashChangeHandler, false);
        this.updateMetaData();
    }
    componentDidUpdate(prevProps) {
        let hashVal = window.location.hash;
        if (hashVal != '' && this.state.page != hashVal) {
            this.setState({
                "page": hashVal
            });
            // console.log(this.state.page);
        }
    }

    componentWillUnmount() {
        window.removeEventListener("hashchange", this.hashChangeHandler, false);
    }

    hashChangeHandler() {
        let hashVal = window.location.hash;
        this.setState({
            "page": hashVal
        });
        // console.log(this.state.page);
        scrollToTop();
    }

    logout() {
        this.props.setSignIn({});
        const { cookies } = this.props;
        cookies.remove('_user_details', { path: '/' });
        setUserProfile({});
        this.props.history.push("/" + this.props.match.params.lang + "/" + this.props.currentCity);
        window.location.reload();
    }
    changeTab(tab) {
        this.setState({
            selectedTab: tab
        });
    }
    render() {
        const { page } = this.state;

        let subPage;

        if (page === '#bookings') {
            subPage = (<MyBookings />);
        } else if (page === '#quotes') {
            subPage = (<MyQuotes />);
        } else if (page === '#details') {
            subPage = (<MyProfile />);
        }
        else if (page === '#creditcard') {
            subPage = (<CreditCardManagement />);
        }
        else if (page === '#wallet') {
            subPage = (<WalletComponent />);
        }

        return (
            <main>
                <div className="container-fluid region region-content page-users">
                    <div id="block-system-main" className="block block-system">
                        <div className="content">
                            <div className="static-page-fluid mbl service-container ">
                                <div className="row ">
                                    <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12 user-left-menu hidden-xs">
                                        <div className="card container-fluid mb-3 shadow-sm">
                                            <div className="user-detail-con mth mbh">
                                                <div className="user-image">
                                                    <img width={181} src={"/dist/images/sm-user-icon.png"} />
                                                </div>
                                                <h5 className="user-name">{this.props.signInDetails.customer ? this.props.signInDetails.customer.firstName : ''}</h5>
                                                {
                                                    ((this.props.userProfile && this.props.userProfile.address) && ((this.props.userProfile.address.area && this.props.userProfile.address.area != null) || (this.props.userProfile.address.city && this.props.userProfile.address.city != null))) ? (
                                                        <div className="user-location text-center mtm">{((this.props.userProfile.address.area != '' && this.props.userProfile.address.area != null) ? (this.props.userProfile.address.area + ', ') : '') + ((this.props.userProfile.address.city != '' && this.props.userProfile.address.city != null) ? (this.props.userProfile.address.city) : '')}</div>
                                                    ) : ''
                                                }

                                            </div>
                                            <ul className="nav ">
                                                <li role="presentation" className={"first " + (this.state.selectedTab === 1 ? 'active' : '')}>
                                                    <a href="#bookings" rel="nofollow, noindex" onClick={this.changeTab.bind(this, 1)}>
                                                        <i className="fa fa-calendar mr-2"></i>
                                                        {locationHelper.translate("MY_BOOKINGS")}
                                                        <i className="my-indicator pull-right ms-arrow_forward mts"></i>
                                                    </a>
                                                </li>
                                                <li role="presentation" className={this.state.selectedTab === 2 ? 'active' : ''} >
                                                    <a href="#quotes" rel="nofollow, noindex" onClick={this.changeTab.bind(this, 2)}>
                                                        <i className="fa fa-list mr-2"></i>
                                                        {locationHelper.translate("MY_QUOTES")}
                                                        <i className="my-indicator pull-right ms-arrow_forward mts"></i>
                                                    </a>
                                                </li>
                                                <li role="presentation" className={this.state.selectedTab === 3 ? 'active' : ''} >
                                                    <a href="#details" rel="nofollow, noindex" onClick={this.changeTab.bind(this, 3)}>
                                                        <i className="fa fa-user mr-2"></i>
                                                        {locationHelper.translate("MY_PROFILE")}
                                                        <i className="my-indicator pull-right ms-arrow_forward mts"></i>
                                                    </a>
                                                </li>
                                                <li role="presentation" className={this.state.selectedTab === 4 ? 'active' : ''} >
                                                    <a href="#creditcard" rel="nofollow, noindex" onClick={this.changeTab.bind(this, 4)}>
                                                        <i className="fa fa-credit-card mr-2"></i>
                                                        {locationHelper.translate("CREDIT_CARDS_MANAGEMENT")}
                                                        <i className="my-indicator pull-right ms-arrow_forward mts"></i>
                                                    </a>
                                                </li>
                                                <li role="presentation" className={"first " +this.state.selectedTab === 6 ? 'active' : ''} >
                                                    <a href="#wallet" rel="nofollow, noindex" onClick={this.changeTab.bind(this, 6)}>
                                                        <img src={ "../../../../dist/images/wallet-filled-money-tool-dark.png"} className="mh-100" height="15" width='14' alt="" 
                                                        style={{    verticalAlign: 'baseline', margin: this.props.lang == 'ar'? '0 0 0 10px' : '0 10px 0 0'}} />
                                                        {locationHelper.translate("MY_WALLET")}
                                                        <i className="my-indicator pull-right ms-arrow_forward mts"></i>
                                                    </a>
                                                </li>
                                                <li role="presentation" className={"last " + (this.state.selectedTab === 5 ? 'active' : '')} >
                                                    <a href="javascript:void(0)" rel="nofollow, noindex"
                                                        onClick={() => this.logout()}>
                                                        <i className="fa fa-power-off mr-2"></i>
                                                        {locationHelper.translate("LOGOUT")}
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>

                                    {subPage}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        )

    }
}

function mapStateToProps(state) {
    return {
        loader: state.loader,
        currentCity: state.currentCity,
        lang: state.lang,
        signInDetails: state.signInDetails,
        userProfile: state.userProfile
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ showLoader, hideLoader, setSignIn, setUserProfile }, dispatch);
}

export default withCookies(connect(mapStateToProps, mapDispatchToProps)(UserAreaPage));


//<CreditCardManagement />
//<MyBookings />
//<MyQuotes />