import React from "react";
import DefaultBanner from "../components/DefaultBanner";
import SectionTitle from "../components/SectionTitle";
import TitleTextBlock from "../components/TitleTextBlock";
import Loader from "../components/Loader";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {fetchPrivacyData, showLoader, hideLoader} from "../actions/index";

class PrivacyPage extends React.Component{
    _isMounted = false;
    constructor(props) {
        super(props);
        this.updateMetaData = this.updateMetaData.bind(this);
    }
    createMarkup(htmlString) {
        return {__html: htmlString};
    }
    componentDidMount(){
        this._isMounted = true;
        if ( !this.props.privacyData.acf ) {
            this.props.showLoader();
            this.props.fetchPrivacyData(this.props.lang)
            .then(()=> {
                if (this._isMounted) {
                    this.updateMetaData();
                    this.props.hideLoader();
                }
            });
        } else {
            this.updateMetaData();
        }
    }
    componentWillUnmount(){
        this._isMounted = false;
    }
    updateMetaData() {
        // console.log('updating meta data');
        document.title = this.props.privacyData.acf.seo_meta_title ? this.props.privacyData.acf.seo_meta_title : 'ServiceMarket';
        var allMetaElements = document.getElementsByTagName('meta');
        for (var i=0; i<allMetaElements.length; i++) {
            if (allMetaElements[i].getAttribute("name") == "description") {
                allMetaElements[i].setAttribute('content', this.props.privacyData.acf.seo_meta_description ? this.props.privacyData.acf.seo_meta_description : 'ServiceMarket');
                break;
            }
        }
    }
    render() {
        const {privacyData, loader} = this.props;
        if(privacyData.acf && !loader){
        return (
            <main>
                <DefaultBanner title={privacyData.acf.banner_title} background={privacyData.acf.banner_image}/>
                <section className="py-5 bg-white">
                    <div className="container">
                        {privacyData.acf.banner_description.length ? ( <p dangerouslySetInnerHTML={this.createMarkup(privacyData.acf.banner_description)}></p> ) : ''}
                        <SectionTitle title={privacyData.title.rendered} />
                        <TitleTextBlock text={privacyData.acf.general_content}/>
                    </div>
                </section>
            </main>
        );
        }else{
            return ( <main loader={loader ? "show": "hide"}><Loader/></main>);
        }
    }
}

function mapStateToProps(state){
	return {
        loader: state.loader,
        privacyData: state.privacyData,
        CurrentCity: state.CurrentCity,
        lang : state.lang,
	}
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({fetchPrivacyData, showLoader, hideLoader}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(PrivacyPage);
