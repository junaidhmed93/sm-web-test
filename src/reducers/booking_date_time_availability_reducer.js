import {SET_DATE_TIME_AVAILABILITY} from "../actions/index";

export default function(state = false, action){
	switch(action.type){
		case SET_DATE_TIME_AVAILABILITY:
			return action.payload;
	}
	return state;
}