import {SET_REVIEW_DATA} from "../actions/index";

export default function(state = {}, action){
	switch(action.type){
		case SET_REVIEW_DATA:
			return action.payload;
	}
	return state;
}