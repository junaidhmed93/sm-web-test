import {SET_COMPANIES_DATA} from "../actions/index";

export default function(state = {}, action){
	switch(action.type){
		case SET_COMPANIES_DATA:
			return action.payload;
	}
	return state;
}