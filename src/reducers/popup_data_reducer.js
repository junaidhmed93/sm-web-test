import {SET_POPUP_MENUE_DATA} from "../actions/index";

export default function(state = {}, action){
	switch(action.type){
		case SET_POPUP_MENUE_DATA:
			return action.payload;
	}
	return state;
}