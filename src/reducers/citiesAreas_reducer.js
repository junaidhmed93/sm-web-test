import {SET_CITIS_AREAS} from "../actions/index";

export default function(state = [], action){
	switch(action.type){
		case SET_CITIS_AREAS:
			return action.payload;
	}
	return state;
}