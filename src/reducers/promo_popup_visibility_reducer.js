import {PROMO_MODAL_VISIBILITY} from "../actions/index";

export default function(state = {}, action){
	switch(action.type){
		case PROMO_MODAL_VISIBILITY:
			return action.payload;
	}
	return state;
}