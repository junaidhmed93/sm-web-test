import {SET_FAQS_DATA} from "../actions/index";

export default function(state = {}, action){
	switch(action.type){
		case SET_FAQS_DATA:
			return action.payload;
	}
	return state;
}