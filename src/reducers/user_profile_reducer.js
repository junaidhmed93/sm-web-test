import {SET_USER_PROFILE,SET_WALLET_HISTORY} from "../actions/index";

export default function(state = false, action){
	switch(action.type){
		case SET_USER_PROFILE:
			return action.payload;
		
		case SET_WALLET_HISTORY:
			{
				return Object.assign({}, state, { walletHistory: action.payload });
			}
	}
	
	return state;
}