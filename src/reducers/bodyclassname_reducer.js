import {SET_BODY_CLASS} from "../actions/index";

export default function(state = '', action){
	switch(action.type){
		case SET_BODY_CLASS:
			return action.payload;
	}
	return state;
}