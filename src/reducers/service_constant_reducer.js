import {SET_SERVICE_CONSTANTS, DEFAULT_LANG} from "../actions/index";
export default function(state = [], action){
	switch(action.type){
		case SET_SERVICE_CONSTANTS:
			let lang = action.lang;
			var parentServices = action.payload.parentServices;
			var subServices = action.payload.subServices;
			var service_name = "";
			var service_constants = {};
			var service_id = "";
			var parentServiceConstant = make_service_constant(parentServices,lang);
			var subServicesConstant = make_service_constant(subServices,lang);
			service_constants = Object.assign(parentServiceConstant,subServicesConstant );
			//console.log(service_constants);
			return service_constants;
	}
	return state;
}
function make_service_constant(services = [], lang){
	var service_constants = {};
	var service_id = "";
	var service_name ="";
	var string_replace = ['-', '(', ')', ',', '/', '&'];
	var string_to_replace = ['', '', '', '', '', ''];
	let itemName= "";
	services.map((item, index) => {
		itemName = lang != DEFAULT_LANG ? item.nameEn : item.name;
		if(item.isOtherService == 1 && (item.parentServiceName != null || item.parentServiceNameEn != null)){
			service_name = lang != DEFAULT_LANG ?  itemName+" "+ item.parentServiceNameEn : itemName+" "+ item.parentServiceName;
			service_name = (service_name).replaceArray(string_replace, string_to_replace);
		}else{
			service_name = (itemName).replaceArray(string_replace, string_to_replace);
		}
		service_name = service_name.split(' ').join('_');
		service_name = "SERVICE_"+service_name.toUpperCase();
		service_name = service_name.toUpperCase();
		//service_name = "SERVICE_"+item.serviceCode;
		service_id = item.id;
		service_constants[service_name] = service_id;
	})
	return service_constants;
}
String.prototype.replaceArray = function(find, replace) {
	var replaceString = this;
	for (var i = 0; i < find.length; i++) {
		replaceString = replaceString.replace(find[i], replace[i]);
	}
	return replaceString;
};