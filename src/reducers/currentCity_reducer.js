import {SET_CURRENT_CITY} from "../actions/index";

export default function(state = 'dubai', action){
	switch(action.type){
		case SET_CURRENT_CITY:
			return action.payload;
	}
	return state;
}