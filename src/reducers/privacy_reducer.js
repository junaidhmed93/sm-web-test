import {SET_PRIVACY_DATA} from "../actions/index";

export default function(state = {}, action){
	switch(action.type){
		case SET_PRIVACY_DATA:
			return action.payload;
	}
	return state;
}