import {LOGIN_MODAL_VISIBILITY} from "../actions/index";

export default function(state = false, action){
	switch(action.type){
		case LOGIN_MODAL_VISIBILITY:
			return action.payload;
	}
	return state;
}