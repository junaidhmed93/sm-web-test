import {SET_MY_BOOKINGS, CANCEL_REQUEST, FREEZE_REQUEST, EDIT_REQUEST, CANCEL_EVENT_REQUEST, EDIT_ALL_REQUEST} from "../actions/index";
import {UNSUBSCRIBE_REQUEST} from "../actions";

export default function (state = {newBookings: [], historyBookings: [], upcomingBookings: []}, action) {
    var upcomingBookings = state.upcomingBookings.filter((item) => item.bookingId !== action.id);
    var newBookings = state.newBookings.filter((item) => item.bookingId !== action.id);
    switch (action.type) {
        case SET_MY_BOOKINGS:
            return action.payload;
        case CANCEL_REQUEST:
            return {
                "historyBookings": state.historyBookings,
                "newBookings": newBookings,
                "upcomingBookings": upcomingBookings
            };
        case CANCEL_EVENT_REQUEST:
            upcomingBookings = state.upcomingBookings.filter((item) => item.bookingEventId !== action.id);
            return {
                "historyBookings": state.historyBookings,
                "newBookings": state.newBookings,
                "upcomingBookings": upcomingBookings
            };
        case FREEZE_REQUEST:
            upcomingBookings = state.upcomingBookings.filter((item) => {
                return (item.date < action.startDate || item.date > action.endDate);
            });
            return {
                "historyBookings": state.historyBookings,
                "newBookings": newBookings,
                "upcomingBookings": upcomingBookings
            };
        case EDIT_REQUEST:
            upcomingBookings = state.upcomingBookings.map((item) => {
                if(item.bookingEventId === action.id) {
                    item.status = "Change Requested";
                }
                return item;
            });
            return {
                "historyBookings": state.historyBookings,
                "newBookings": newBookings,
                "upcomingBookings": upcomingBookings
            };
        case EDIT_ALL_REQUEST:
            upcomingBookings = state.upcomingBookings.map((item) => {
                if(item.bookingId === action.id) {
                    item.status = "Change Requested";
                }
                return item;
            });
            return {
                "historyBookings": state.historyBookings,
                "newBookings": newBookings,
                "upcomingBookings": upcomingBookings
            };
        case UNSUBSCRIBE_REQUEST:
            return {
                "historyBookings": state.historyBookings,
                "newBookings": newBookings,
                "upcomingBookings": upcomingBookings
            }
    };
    return state;
}