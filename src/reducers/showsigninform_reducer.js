import {SIGNIN_FORM_VISIBILITY} from "../actions/index";

export default function(state = false, action){
	switch(action.type){
		case SIGNIN_FORM_VISIBILITY:
			return action.payload;
	}
	return state;
}