import {FORGET_PASSWORD_MODAL_VISIBILITY} from "../actions";

export default function(state = false, action){
	switch(action.type){
		case FORGET_PASSWORD_MODAL_VISIBILITY:
			return action.payload;
	}
	return state;
}