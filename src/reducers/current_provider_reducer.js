import {SET_CURRENT_PROVIDER} from "../actions/index";

export default function(state = {}, action){
	switch(action.type){
		case SET_CURRENT_PROVIDER:
			return action.payload;
	}
	return state;
}