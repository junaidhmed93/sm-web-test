import {SET_ZOHO_MIGRATED_SERVICE} from "../actions/index";

export default function(state = [], action){
	switch(action.type){
		case SET_ZOHO_MIGRATED_SERVICE:
			return action.payload;
	}
	return state;
}