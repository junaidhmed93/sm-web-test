import {SET_JOURNEY3_SUBSERVICE_DATA} from "../actions/index";

export default function(state = {}, action){
	switch(action.type){
		case SET_JOURNEY3_SUBSERVICE_DATA:
			return action.payload;
	}
	return state;
}