import {SET_ALL_REVIEWS} from "../actions/index";
export default function(state = [], action){
	switch(action.type){
		case SET_ALL_REVIEWS:
			let reviews = action.payload;
			/*if(reviews.length){
				reviews = reviews.slice(0, 2);
			}*/
			return reviews;
	}
	return state;
}