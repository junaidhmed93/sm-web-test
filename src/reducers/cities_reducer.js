import {SET_CITIES, DEFAULT_LANG} from "../actions/index";
import commonHelper from "../helpers/commonHelper"
export default function(state = [], action){
	switch(action.type){
		case SET_CITIES:
			return add_city_slug(action.payload, action.lang, action.removeArea);
	}
	return state;
}
function add_city_slug(payload, lang , removeArea){
	var cities = [];
	var city_name = "";
	payload.map((item) => {
		if(removeArea){
			delete item.cityDto["areas"]
		}
		city_name = lang != DEFAULT_LANG  ? item.nameEn :item.name;
		item['slug'] = commonHelper.slugify(city_name);
		cities.push(item);
	})
	return cities;
}