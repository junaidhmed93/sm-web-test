import {SET_ABOUT_DATA} from "../actions/index";

export default function(state = {}, action){
	switch(action.type){
		case SET_ABOUT_DATA:
			return action.payload;
	}
	return state;
}