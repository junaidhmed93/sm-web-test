import { combineReducers } from 'redux';
import HomepageReducer from './homepage_reducer';
import LandingReducer from './landing_reducer';
import LandingConfirmationReducer from './landing_confirmation_reducer';
import LoaderReducer from './showloader_reducer';
import URLConstantsReducer from './url_constant_reducer';
import LocationConstantsReducer from './location_constant_reducer';
import serviceConstantsReducer from './service_constant_reducer.js';
import serviceReducer from './service_all_constant_reducer.js';
import dataConstantsReducer from './data_values_constant_reducer.js';
import newDataConstantsReducer from './new_data_values_constant_reducer.js';
import liteWeightPriceReducer from './lite_weight_price_reducer';
import WhySMReducer from './whysm_reducer';
import HowIWReducer from './howitworks_reducer';
import AboutReducer from './about_reducer';
import BotReducer from './bot_reducer';
import BePartnerReducer from './bepartner_reducer';
import PricePlanReducer from './cleaning_price_plan_reducer.js';
import ReviewReducer from './review_reducer';
import ReviewsReducer from './reviews_reducer';
import TeamReducer from './team_reducer';
import FaqsReducer from './faqs_reducer';
import PrivacyReducer from './privacy_reducer';
import DisclaimerReducer from './disclaimer_reducer';
import TermsReducer from './terms_reducer';
import PRPReducer from './prp_reducer';
import CitiesReducer from './cities_reducer';
import CurrentLang from './currentLangReducer';
import CurrentCity from './currentCity_reducer';
import pageParams from './pageParamsReducer';
import CurrentProvider from './current_provider_reducer';
import currentCityData from './currentCityData_reducer';
import currentCityID from './currentCityIDReducer';
import BodyClassName from './bodyclassname_reducer';
import MainServicesReducer from './mainservices_reducer';
import ShowMainMenuRducer from './showmainmenu_reducer';
import ShowLoginMenuReducer from './showloginmenu_reducer';
import ShowForgetPasswordMenuReducer from './show_forget_password_menu_reducer';
import ShowFeedbackMenuReducer from './show_feedback_menu_reducer';
import ShowSignInFormReducer from './showsigninform_reducer';
import FooterServicesReducer from './footerservices_reducer';
import LogosReducer from './logos_reducer';
import FooterItemsReducer from './footer_items_reducer';
import SignInReducer from './signin_reducer.js';
import TaxPlanReducer from './taxplan_reducer.js';
import bookingDateTimeAvailabilityReducer from './booking_date_time_availability_reducer.js';

import MyBookingsReducer from './my_bookings_reducer';
import MyQuotesReducer from './my_quotes_reducer';
import MyCreditCardsReducer from './my_creditcards_reducer';
import SetUserProfileReducer from './user_profile_reducer';
import SetResetUserDetailsReducer from './reset_user_details_reducer';
import CitiesAreasReducer from './citiesAreas_reducer';
import PopupDataReducer from './popup_data_reducer';
import showPromoMenu from './promo_popup_visibility_reducer';
import Journey3SubserviceDataReducer from './journey3_subservice_data_reducer';
import CountriesReducer from './countries_reducer';
import CompaniesData from './companies_reducer';
import dateandTimeReducer from './dateandTimeReducer';
import apiServicesReducer from './api_services_reducer';
import bookingServicesReducer from './bookingServicesReducer';
import zohoMigratedServicesReducer from './zohoMigratedServicesReducer';
import showlangswitchReducer from './showlangswitch_reducer';


import {SET_JOURNEY3_SUBSERVICE_DATA} from "../actions";


const rootReducer = combineReducers({
  homepageData: HomepageReducer,
  landingData: LandingReducer,
  landingConfirmationData: LandingConfirmationReducer,
  loader: LoaderReducer,
  //URLConstants : URLConstantsReducer,
  //locationConstants:LocationConstantsReducer,
  allServices:serviceReducer,
  serviceConstants: serviceConstantsReducer,
  dataConstants: dataConstantsReducer,
  newDataConstants:newDataConstantsReducer,
  liteWeightPrices:liteWeightPriceReducer,
  why_sm: WhySMReducer,
  how_iw: HowIWReducer,
  logos: LogosReducer,
  footerItemsData: FooterItemsReducer,
  loader: LoaderReducer,
  aboutData: AboutReducer,
  isBotData: BotReducer,
  bePartnerData: BePartnerReducer,
  PricePlan: PricePlanReducer,
  reviewData: ReviewReducer,
  reviewsData: ReviewsReducer,
  teamData: TeamReducer,
  faqsData: FaqsReducer,
  privacyData: PrivacyReducer,
  companiesData: CompaniesData,
  disclaimerData: DisclaimerReducer,
  termsData: TermsReducer,
  prpData: PRPReducer,
  cities: CitiesReducer,
  lang: CurrentLang,
  currentCity: CurrentCity,
  pageParams: pageParams,
  currentProvider: CurrentProvider,
  currentCityData: currentCityData,
  currentCityID: currentCityID,
  bodyClass: BodyClassName,
  mainServices: MainServicesReducer,
  footerServices: FooterServicesReducer,
  showMainMenu: ShowMainMenuRducer,
  showLoginMenu: ShowLoginMenuReducer,
  showForgetPasswordMenu: ShowForgetPasswordMenuReducer,
  showFeedbackMenu: ShowFeedbackMenuReducer,
  showSignInForm: ShowSignInFormReducer,
  signInDetails: SignInReducer,
  TaxPlanReducer: TaxPlanReducer,
  bookingDateTimeAvailabilityReducer:bookingDateTimeAvailabilityReducer,
  myBookingsData: MyBookingsReducer,
  myQuotesData: MyQuotesReducer,
  myCreditCardsData: MyCreditCardsReducer,
  userProfile: SetUserProfileReducer,
  resetUserDetails: SetResetUserDetailsReducer,
  citiesAreas: CitiesAreasReducer,
  popupData: PopupDataReducer,
  showPromoModal: showPromoMenu,
  journey3SubserviceDataReducer: Journey3SubserviceDataReducer,
  countries: CountriesReducer,
  dateAndTime: dateandTimeReducer,
  apiServices: apiServicesReducer,
  bookingServices: bookingServicesReducer,
  zohoMigratedServices: zohoMigratedServicesReducer,
  langSwitchVisibility: showlangswitchReducer
});

export default rootReducer;