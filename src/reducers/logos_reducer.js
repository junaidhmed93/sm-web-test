import {SET_PARTNERS_LOGOS, processStaticData} from "../actions/index";

export default function(state = {}, action){
	switch(action.type){
		case SET_PARTNERS_LOGOS:
			return processStaticData(action);
			//return typeof action.payload[0] != "undefined" ? action.payload[0] : action.payload;
	}
	return state;
}