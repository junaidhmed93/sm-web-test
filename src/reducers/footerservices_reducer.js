import {SET_FOOTER_SERVICES} from "../actions/index";

export default function(state = [], action){
	switch(action.type){
		case SET_FOOTER_SERVICES:
			return action.payload;
	}
	return state;
}