import {SET_MY_QUOTES} from "../actions/index";

export default function(state = {data: []}, action){
	switch(action.type){
		case SET_MY_QUOTES:
			return action.payload;
	}
	return state;
}