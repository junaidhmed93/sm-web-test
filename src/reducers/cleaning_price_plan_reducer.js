import {SET_CLEANING_BOOKING_PRICING_PLAN} from "../actions/index";
export default function(state = [], action){
	switch(action.type){
		case SET_CLEANING_BOOKING_PRICING_PLAN:
			return action.payload;
	}
	return state;
}