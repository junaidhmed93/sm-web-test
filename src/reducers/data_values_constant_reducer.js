import {SET_DATA_DICTIONARY_VALUES} from "../actions/index";
export default function(state = [], action){
	switch(action.type){
		case SET_DATA_DICTIONARY_VALUES:
			var dataValues = action.payload;
			var newDataValues = make_data_constant(dataValues) ;
			return newDataValues;
	}
	return state;
}
function make_data_constant(dataValues = []){
	var category_name = "";
	var newDataValues = {};
	dataValues.map((item, index) => {
		category_name = (item.category).toUpperCase();
		//console.log(category_name, item.dataValues);
		newDataValues[category_name] = item.dataValues;
	})
	return newDataValues;
}