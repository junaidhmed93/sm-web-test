import { SET_ALL_COUNTRIES } from "../actions/index";
export default function (state = [], action) {
  switch (action.type) {
    case SET_ALL_COUNTRIES:
      // console.log('countries reducer');
      // console.log(action.payload);
      const countries = action.payload;      
      return countries;
  }
  return state;
}
