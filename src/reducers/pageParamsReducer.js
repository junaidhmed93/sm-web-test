import {SET_PAGE_PARAMS} from "../actions/index";

export default function(state = {}, action){
	switch(action.type){
		case SET_PAGE_PARAMS:
			return action.payload;
	}
	return state;
}