import {SET_TERMS_DATA} from "../actions/index";

export default function(state = {}, action){
	switch(action.type){
		case SET_TERMS_DATA:
			return action.payload;
	}
	return state;
}