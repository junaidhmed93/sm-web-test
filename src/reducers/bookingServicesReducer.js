import {SET_BOOKING_SERVICES} from "../actions/index";

export default function(state = {}, action){
	switch(action.type){
        case SET_BOOKING_SERVICES:
		return {
            bookingServices:setBookingID(action.payload),
            quotesServices:setQuotesID(action.payload)
        };
	}
	return state;
}
function setBookingID(payload){
    var arr = []; 
    var bookingServices = payload;
    bookingServices.map((item) => { 
          if( item.bookingEnabled != 0){
            arr.push(item.id);
          }
          item.subServices.filter(service => service.bookingEnabled != 0 ? arr.push(service.id) : [] ) 
    })
    //console.log("arr",arr);
    return arr;
}
function setQuotesID(payload){
    var arr = []; 
    var bookingServices = payload;
    bookingServices.map((item) => { 
          if( item.isRequestable != 0){
            arr.push(item.id);
          }
          item.subServices.filter(service => service.isRequestable != 0 ? arr.push(service.id) : [] ) 
    })
    //console.log("arr",arr);
    return arr;
}