import {SET_SIGNIN} from "../actions/index";

export default function(state = false, action){
	switch(action.type){
		case SET_SIGNIN:
			return action.payload;
	}
	return state;
}