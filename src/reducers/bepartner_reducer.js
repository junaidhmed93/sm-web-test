import {SET_BECOME_PARTNER_DATA} from "../actions/index";

export default function(state = {}, action){
	switch(action.type){
		case SET_BECOME_PARTNER_DATA:
			return action.payload;
	}
	return state;
}