import {SET_HOMEPAGE_DATA} from "../actions/index";

export default function(state = [], action){
	switch(action.type){
		case SET_HOMEPAGE_DATA:
			return action.payload;
	}
	return state;
}