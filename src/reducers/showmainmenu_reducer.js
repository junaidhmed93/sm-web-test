import {MAIN_MENU_VISIBILITY} from "../actions/index";

export default function(state = false, action){
	switch(action.type){
		case MAIN_MENU_VISIBILITY:
			return action.payload;
	}
	return state;
}