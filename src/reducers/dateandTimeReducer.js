import {SET_DATE_TIME} from "../actions/index";

export default function(state = [], action){
	switch(action.type){
		case SET_DATE_TIME:
			return action.payload;
	}
	return state;
}