import {SET_TEAM_DATA} from "../actions/index";

export default function(state = {}, action){
	switch(action.type){
		case SET_TEAM_DATA:
			return action.payload;
	}
	return state;
}