import {cityCodes} from "./../actions/index"
export default function(state = [], action){
	
	const constant = {
		/* defaults */
		DEFAULT_CITY_ID :  1, // dubai id
		DEFAULT_CITY_NAME : 'dubai', // dubai name
		DEFAULT_COUNTRY_NAME : 'UAE',
		DEFAULT_COUNTRY_ID : 1,
		DEFAULT_CURRENCY : "AED",
		/* location IDs  */
		DUBAI_ID : 1,
		ABU_DHABI_ID : 2,
		SHARJAH_ID : 3,
		SHARJAH_MAPPED_ID :  6,
		DOHA_ID : 5,
		DOHA_CITY_ID : 13,
		JEDDAH_ID : 15,
		RIYADH_ID : 16,
		BAHRAIN_ID : 17,
		KUWAIT_ID : 18,
		SINGAPORE_ID : 19,
		SINGAPORE_SL_ID : 10,
		KUALA_LUMPUR_ID : 20,
		KUALA_LUMPUR_SL_ID : 11,
		HONGKONG_ID : 21,
		HONGKONG_SL_ID : 12,
		/* city IDs */
		DAMMAM_ID : 22,
		MUSCAT_ID : 23,
		UAE_ID : 1,
		QATAR_ID : 178,
		KSA_ID : 192,
		KW_ID : 116, // KUWAIT country ID
		BH_ID : 18, // BAHRAIN country ID
		SP_ID : 197, // SINGAPORE country ID
		MALAYSIA_ID : 131, // KUALA LUMPUR country ID
		CHINA_ID : 97, // HONGKONG country ID
		OMAN_ID : 165, // MUSCAT country ID
		cityCodes: cityCodes
	};
	/*var cityCodes = {};
	constant.cityCodes[constant.DUBAI]= "DXB";
	constant.cityCodes[constant.ABU_DHABI]= "AUH";
	constant.cityCodes[constant.SHARJAH]= "SHJ";
	constant.cityCodes[constant.DOHA]= "DOH";
	constant.cityCodes[constant.JEDDAH]= "JED";
	constant.cityCodes[constant.RIYADH]= "RUH";
	constant.cityCodes[constant.BAHRAIN]= "BAH";
	constant.cityCodes[constant.KUWAIT]= "KWI";
	constant.cityCodes[constant.DAMMAM]= "DMM";
	constant.cityCodes[constant.MUSCAT]= "MCT"*/

	return constant;
}