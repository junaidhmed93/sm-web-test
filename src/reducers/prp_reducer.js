import {SET_PRP_DATA} from "../actions/index";

export default function(state = {}, action){
	switch(action.type){
		case SET_PRP_DATA:
			return action.payload;
	}
	return state;
}