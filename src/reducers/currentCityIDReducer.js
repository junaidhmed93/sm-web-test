import {SET_CURRENT_CITY_ID} from "../actions/index";

export default function(state = [], action){
	switch(action.type){
		case SET_CURRENT_CITY_ID:
			return action.payload;
	}
	return state;
}