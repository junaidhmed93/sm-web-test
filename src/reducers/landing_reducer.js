import {SET_LANDING_DATA} from "../actions/index";

export default function(state = [], action){
	switch(action.type){
		case SET_LANDING_DATA:
			return action.payload;
	}
	return state;
}