import {SET_RESET_USER_DETAILS} from "../actions/index";

export default function(state = false, action){
	switch(action.type){
		case SET_RESET_USER_DETAILS:
			return action.payload;
	}
	return state;
}