import {SET_DISCLAIMER_DATA} from "../actions/index";

export default function(state = {}, action){
	switch(action.type){
		case SET_DISCLAIMER_DATA:
			return action.payload;
	}
	return state;
}