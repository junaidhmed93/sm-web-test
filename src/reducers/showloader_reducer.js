import {SHOW_LOADER} from "../actions/index";
import {HIDE_LOADER} from "../actions/index";

export default function(state = false, action){
	switch(action.type){
		case SHOW_LOADER:
            return true;
		case HIDE_LOADER:
			return false;   
	}
	return state;
}