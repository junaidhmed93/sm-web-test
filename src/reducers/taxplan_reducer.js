import {SET_TAX_PLAN} from "../actions/index";

export default function(state = false, action){
	switch(action.type){
		case SET_TAX_PLAN:
			return action.payload;
	}
	return state;
}