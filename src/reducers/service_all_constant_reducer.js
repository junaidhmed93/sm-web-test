import {SET_ALL_SERVICE_CONSTANT} from "../actions/index";
export default function(state = [], action){
	switch(action.type){
		case SET_ALL_SERVICE_CONSTANT:
			return action.payload;
	}
	return state;
}