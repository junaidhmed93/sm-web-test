import {LANG_SWITCH_VISIBILITY} from "../actions/index";

export default function(state = false, action){
	switch(action.type){
		case LANG_SWITCH_VISIBILITY:
			return action.payload;
	}
	return state;
}