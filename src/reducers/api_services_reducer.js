import {SET_API_SERVICES} from "../actions/index";

export default function(state = {}, action){
	switch(action.type){
        case SET_API_SERVICES:
		return action.payload;
	}
	return state;
}