import {SET_CURRENT_CITY_DATA} from "../actions/index";

export default function(state = [], action){
	switch(action.type){
		case SET_CURRENT_CITY_DATA:
			return action.payload;
	}
	return state;
}