import {SET_CURRENT_LANG} from "../actions/index";

export default function(state = 'en', action){
	switch(action.type){
		case SET_CURRENT_LANG:
			return action.payload;
	}
	return state;
}