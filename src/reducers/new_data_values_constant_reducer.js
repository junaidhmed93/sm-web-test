import {SET_DATA_CONSTANT_VALUES} from "../actions/index";
export default function(state = [], action){
	switch(action.type){
		case SET_DATA_CONSTANT_VALUES:
			var newDataValues = action.payload;
			newDataValues = process_data_constant(newDataValues.constants);
			return newDataValues;
	}
	return state;
}
function process_data_constant(dataValues = []){
	var category_name = "";
	var newDataValues = {};
	var newItem = {};
	//console.log(dataValues);
	Object.keys(dataValues).map((key, index) => {
		newItem = {};
		dataValues[key].map((item) => {
			item["value"] = item.id;

			item["label"] = item.name;

		})
	})

	return dataValues;
}