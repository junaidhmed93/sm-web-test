import {SET_MY_CREDIT_CARDS, REGISTER_CREDIT_CARDS} from "../actions/index";

export default function(state = {}, action){
    //console.log(state);
	switch(action.type){
		case SET_MY_CREDIT_CARDS:
			return action.payload;
		case REGISTER_CREDIT_CARDS:
        	return {
				state,
				'redirect_url': action.payload
			};
	}
	return state;
}