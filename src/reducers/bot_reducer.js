import {SET_BOT} from "../actions/index";

export default function(state = {}, action){
	switch(action.type){
		case SET_BOT:
			return action.payload;
	}
	return state;
}