import {FEEDBACK_MODAL_VISIBILITY} from "../actions";

export default function(state = false, action){
	switch(action.type){
		case FEEDBACK_MODAL_VISIBILITY:
			return action.payload;
	}
	return state;
}