import commonHelper from "./commonHelper"
import { UAE_ID, DUBAI, DEFAULT_LANG, KSA_ID, arabicAllowedPages, ABU_DHABI, LANG_AR, locationConstants } from "../actions";
import en from '../constants/en';
import ar from '../constants/ar';
const languages = {
    en,
    ar
};
const locationHelper = {
    thisVar: this,
    Props : null,
    init(){
        //console.log("locationHelper", this.props)
        locationHelper.Props = this.props;
    },
    allCites: typeof window == "undefined" || typeof window.REDUX_DATA == "undefined" ? [] : window.REDUX_DATA.cities,
    translate: function(key, passeLang = ""){
        let lang = ( locationHelper.Props != null && (typeof locationHelper.Props.lang != "undefined") ) ? locationHelper.Props.lang: DEFAULT_LANG;
        if(passeLang != ""){
            lang = passeLang;
        }
        var strings = typeof languages[lang][key] != "undefined" ? languages[lang][key] : key;
        let substring = "{city}";
        if(strings.includes(substring)){
            let currentCity = locationHelper.getCurrentCity() == ABU_DHABI ? "abu_dhabi" : locationHelper.getCurrentCity();
            let cityKey = currentCity.toUpperCase();
            let cityConstant = typeof languages[lang][cityKey] != "undefined" ? languages[lang][cityKey] : cityKey;
            strings = commonHelper.replaceAll(strings,"{city}", cityConstant);
        }
        return strings;
    },
    getCurrentCity(){
        let currentCity = ( locationHelper.Props != null && typeof locationHelper.Props.currentCity != "undefined" )? locationHelper.Props.currentCity : DUBAI;
        //console.log(currentCity)
        return currentCity;
    },
    showLangNav(slug){
        let currentCity = locationHelper.getCurrentCity();
        return (locationHelper.getCurrentCountryId() == KSA_ID && arabicAllowedPages.includes(slug)) ? true : false;
    },
    fetchCities: function () {
        var cities = [];
        var allCites = this.allCites;
        var currentCountryId = this.getCurrentCountryId();
        //console.log("currentCountryId", currentCountryId);
        var city_value = "";
        allCites.map(function (item, index) {
            if (item.cityDto.countryDto.id == currentCountryId) {
                city_value = commonHelper.slugify(item.nameEn);
                cities.push({ id: item.id, value: city_value, name: item.name, label: item.name, code: item.code, slug:city_value });
            }
        })
        return cities;
    },
    fetchCitiesforUserProfile: function () {
        var cities = [];
        var allCites = this.allCites;
        var currentCountryId = this.getCurrentCountryId();
        var city_value = "";
        allCites.map(function (item, index) {
            city_value = commonHelper.slugify(item.nameEn);
            cities.push({ id: item.id, relativeId:item.cityDto.id, value: city_value, name: item.name, label: item.name, code: item.code });
        })
        return cities;
    },
    getCurrentCityId() {
        var currentCity = typeof window == "undefined" ? "dubai" : window.REDUX_DATA.currentCity;
        var location = this.getLocationByName(currentCity);
        return location.id;
    },
    getCurrentCityDetails() {
        var currentCity = typeof window == "undefined" ? "dubai" : window.REDUX_DATA.currentCity;
        var location = this.getLocationByName(currentCity);
        return location;
    },
    getCurrentCountryId: function () {
        if(typeof window != "undefined"){
            //console.log("calling Here 123");
            var currentCityData = window.REDUX_DATA.currentCityData;
            //console.log("currentCityData[0].cityDto.countryDto.id", currentCityData);
            return (currentCityData.length && typeof currentCityData[0].cityDto.countryDto.id != "undefined") ? currentCityData[0].cityDto.countryDto.id : UAE_ID;
        }
        return UAE_ID;
    },
    getAreasByCity(city_id) {
        var areas = {};
        var allCites = this.allCites;
        //console.log("allCites", allCites)
        allCites.map(function (item, index) {
            if (item.id == city_id) {
                areas = item.cityDto.areas;
            }
        })
        return areas;
    },
    getLocationById(locationId) {
        var city = {}
        var allCites = this.allCites;
        var city_value = "";
        allCites.map(function (item, index) {
            if (locationId == item.id) {
                city_value = commonHelper.slugify(item.nameEn);
                city = { id: item.id, value: item.slug, name: item.name, label: item.name, code:item.code };
            }
        })
        return city;
    },
    getServiceLocationById(locationId, returnId = "id") {
        var city = {}
        var allCites = this.allCites;
        var city_value = "";
        allCites.map(function (item, index) {
            if (locationId == item.id || locationId == item.cityDto.id) {
                city_value = commonHelper.slugify(item.name);
                city = { itemId : item.id, id: item.cityDto.id, value: city_value, name: item.name, label: item.name, code:item.code  };
            }
        })
        return city[returnId];
    },
    getLocationByName(location_name) {
        var city = {}
        var allCites = this.allCites;
        var city_value = "";
        allCites.map(function (item, index) {
            city_value = commonHelper.slugify(item.nameEn);
            if (location_name == city_value || location_name == item.name) {
                city = { id: item.id, cityId: item.cityDto.id, value: city_value, name: item.name, label: item.name, slug: city_value, code: item.code };
            }
        })
        return city;
    },
    getLocationByNameArabic(cityName) {
        var city = {}
        var allCites = this.allCites;
        //console.log('allCites',allCites);
        var cityValue = "";
        allCites.map(function (item, index) {
            cityValue = commonHelper.slugify(item.name);
            //console.log('slugify cityValue',cityValue);
            if (cityName.trim() === item.name.trim()) {
                city = { id: item.id, value: item.name, name: item.name, label: item.name, slug: item.name, code: item.code };
            }
        })
        return city;
    },
    getLocationByNameLocationID(location_name) {
        var city = {}
        var allCites = this.allCites;

        var city_value = "";
        allCites.map(function (item, index) {
            city_value = commonHelper.slugify(item.name);
            if (location_name == city_value) {
                city = { id: item.cityDto.id, value: city_value, name: item.name, label: item.name };
            }
        })
        return city;
    },
    getAreaByName(areas, area_name, lang = DEFAULT_LANG) {
        var area_value = "";
        var area = {};
        area_name =  lang == LANG_AR ? area_name : commonHelper.slugify(area_name);
        if( (typeof areas != "undefined" && areas != null) &&  areas.length ) {
            areas.map(function (item, index) {
                area_value =  lang == LANG_AR ? item.title : commonHelper.slugify(item.title);
                if (area_name == area_value) {
                    area = { id: item.id, value: area_value, title: item.title };
                }
            })
        }else{
            area = "";
        }
        return area;
    },
    getCurrentCityData() {
        var currentCityData = typeof window == "undefined" ? [] : window.REDUX_DATA.currentCityData;
        return currentCityData
    },
    getCurrentCurrency() {
        var currency = typeof window == "undefined" ? "AED" : locationConstants.DEFAULT_CURRENCY;
        var currentCityData = this.getCurrentCityData();
        if (currentCityData.length) {
            currency = window.REDUX_DATA.currentCityData[0].cityDto.countryDto.currencyDto.code
        }
        return currency;
    },
    findObjectByKey(array, key, value) {
        for (var i = 0; i < array.length; i++) {
            if (array[i][key] === value) {
                return array[i];
            }
        }
        return null;
    }
}
export default locationHelper;
