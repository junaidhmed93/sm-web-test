import moment from 'moment';
import {fetchDateTime, fetchAllApiServices,getCityCode, isCoreBooking, DEFAULT_LANG, LANG_EN} from "../actions";
import {withCookies} from "react-cookie";
import {connect} from "react-redux";
//var CryptoJS = require("crypto-js");
import locationHelper from './locationHelper';
import staticVariables from "../staticVariables";
String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}
const commonHelper = {
    replaceString: function(){

    },
    slugify:function(text){
        if(typeof text == "undefined" || text == null) { return text; }
        return text.toString().toLowerCase()
            .replace(/\s+/g, '-')           // Replace spaces with -
            .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
            .replace(/\-\-+/g, '-')         // Replace multiple - with single -
            .replace(/^-+/, '')             // Trim - from start of text
            .replace(/-+$/, '');            // Trim - from end of text
    },
    urlArgs:function(index = "", pathname = ""){
       if(pathname != ""){
            pathname = pathname;
       }else{
        pathname = (typeof window != "undefined" && typeof window.location != "undefined") ? window.location.pathname : ""
       }
       var paths =   pathname != "" ? pathname.split( '/' ) : [];
       //console.log("paths", paths);
        if(paths.length) {
            paths.shift();
            return index != "" ? paths[index] : paths;
        }
        return "";
    },
    processDataValues:function(dataConstants){
        var data_values = {};

        var constant_name = "";
        if( typeof dataConstants != "undefined" && dataConstants.length){
            dataConstants.map((item, index) => {
                constant_name = "DATA_VALUE_"+item.constant;
                data_values[constant_name] = item.id;
            })
        }

        return data_values;
    },
    replaceAll : function(target, search, replacement) {
        return target.replace(new RegExp(search, 'g'), replacement);
    },
    getNewDataValues:function(key){
        var dataConstants = (typeof window != "undefined") ? window.REDUX_DATA.newDataConstants: [];
        var returnDataConstant = [];
        if( typeof dataConstants[key] != "undefined" ){
            returnDataConstant = dataConstants[key];
        }
        return returnDataConstant;
    },
    isInteractiveService:function(serviceID){
        var serviceId = parseInt(serviceID);
        var serviceConstants = (typeof window != "undefined") ? window.REDUX_DATA.serviceConstants: {};
        if(Object.keys(serviceConstants).length) {
            var interactiveServices = [
                serviceConstants.SERVICE_INTERNATIONAL_MOVE,
                serviceConstants.SERVICE_LOCAL_MOVE,
                serviceConstants.SERVICE_STORAGE,
                serviceConstants.SERVICE_PHOTOGRAPHY,
                serviceConstants.SERVICE_PARTTIME_NANNIES__BABYSITTERS,
                serviceConstants.SERVICE_CATERING,
                serviceConstants.SERVICE_CURTAINS__BLINDS
            ];

            if (interactiveServices.includes(serviceId)) {
                return true;
            }
        }
        return false;
    },
    lookupServices:function(keyName, keyValue, asDropdownOptions = true, asCheckBoxes = false, searchParentOnly = false){
        //service_type_id
        var allServices = (typeof window != "undefined") ? window.REDUX_DATA.allServices : {};
        var filteredSubServices = [];
        var output = [];
        var serviceTitle = "";
        if(Object.keys(allServices).length) {
            if (keyName == "parent_service_id") {
                var parentService = allServices["parentServices"].filter((item) => {
                    return item.id == keyValue;
                })
                if (parentService.length) {
                    filteredSubServices = allServices["subServices"].filter((subServiceItem) => parentService[0].subServices.includes(subServiceItem.id));
                }
            }
            if (filteredSubServices.length) {
                filteredSubServices.map((item) => {
                    var serviceTitle = this.jsUcfirst(item.label);
                    if(item.isOtherService == 0) {
                        if (asDropdownOptions || asCheckBoxes) {
                            output.push({id: item.id, label: serviceTitle, value: item.id, disabled: false})
                        } else {
                            output.push(item);
                        }
                    }
                })
            }
        }
        return output;
    },
    getWordAtPosition:function(text, position)
    {
        var start = position;
        var end = position;
        while (text[start] !== ',' && start > 0) start--;
        while (text[end] !== ',' && end < text.length - 1) end++;
        return text.substring((start === 0 ? 0 : start + 1), end);
    },
    divideStringByterm:function (text, term) {
        var start = text.toLowerCase().indexOf(term.toLowerCase());
        var end = start + term.length;

        var subBefore = start !== -1 ? text.substring(0, start) : '';
        var subHighlited = start !== -1 ? text.substring(start, end) : text.substring(0, end);
        var subAfter = text.substring(end);

        return {
            subBefore: subBefore,
            subHighlited: subHighlited,
            subAfter: subAfter,
            exist: (start !== -1)
        }
    }
    ,
    getIndicesOf:function(searchStr, str, caseSensitive) {

        var searchStrLen = searchStr.length;
        if (searchStrLen == 0) {
            return [];
        }
        var startIndex = 0, index, indices = [];
        if (!caseSensitive) {
            str = str.toLowerCase();
            searchStr = searchStr.toLowerCase();
        }
        while ((index = str.indexOf(searchStr, startIndex)) > -1) {
            indices.push(index);
            startIndex = index + searchStrLen;
        }
        return indices;
    },
    convertUnixTimestampToStringDate:function(unix_timestamp) {
        const date = new Date(unix_timestamp);
       
        const day = date.getDate();
        const month = date.getMonth()+1;
        const year = date.getFullYear();

        return day +'-' +month + '-' + year
    },
    categoriesOrder:function() {
        return [1, 3, 6, 4, 5, 7, 2];
    },
    footerCategoriesOrder:function() {
        return [1, 2, 3, 4, 5, 6, 7];
    },
    movingSizeIdConverter:function(movingSize, getOfficeId = false)
    {
        var movingValue = "1";

        switch (movingSize) {
            case 'studio':
                movingValue = "9";

                break;

            case '1 Br':
                movingValue = "1";

                break;

            case '2 Br':
                movingValue = "2";

                break;

            case '3 Br':
                movingValue = "3";

                break;

            case '4 Br':
                movingValue = "4";

                break;

            case '2 Br h/v':
                movingValue = "5";

                break;

            case '3 Br h/v':
                movingValue = "6";

                break;
            case '4 Br h/v':
                movingValue = "7";

                break;
            case '5 Br h/v':
                movingValue = "8";

                break;
            case 'other':
                movingValue = "10";

                break;

            case 'office':
                /*if (getOfficeId) {

                }*/
                movingValue = "11";
                break;


            default:
                movingValue = "1";
                break;
        }

        return movingValue;
    },
    sizeToServiceConverter:function( movingSize, moveType )
    {
        
        var allServiceConstant = (typeof window != "undefined") ? window.REDUX_DATA.serviceConstants: {};
        var serviceID = allServiceConstant.SERVICE_LOCAL_MOVE;
        switch (movingSize) {

            case 'studio':
                serviceID = (moveType == allServiceConstant.SERVICE_LOCAL_MOVE) ? allServiceConstant.SERVICE_LOCAL_SMALL_MOVE : allServiceConstant.SERVICE_INTERNATIONAL_SMALL_MOVE;
                break;

            case '1 Br':
            case '2 Br':
            case '3 Br':
            case '4 Br':
            case 'other':
                serviceID = (moveType == allServiceConstant.SERVICE_LOCAL_MOVE) ? allServiceConstant.SERVICE_LOCAL_APARTMENT_MOVE : allServiceConstant.SERVICE_INTERNATIONAL_APARTMENT_MOVE;
                break;

            case '2 Br h/v':
            case '3 Br h/v':
            case '4 Br h/v':
            case '5 Br h/v':
                serviceID = (moveType == allServiceConstant.SERVICE_LOCAL_MOVE) ? allServiceConstant.SERVICE_LOCAL_VILLA_MOVE : allServiceConstant.SERVICE_INTERNATIONAL_VILLA_MOVE;
                break;

            case 'office':
                serviceID = allServiceConstant.SERVICE_LOCAL_OFFICE_MOVE;
                break;

            default:
                serviceID = (moveType == allServiceConstant.SERVICE_LOCAL_MOVE) ? allServiceConstant.SERVICE_LOCAL_APARTMENT_MOVE : allServiceConstant.SERVICE_INTERNATIONAL_APARTMENT_MOVE;
                break;
        }

        return serviceID;
    },
    objToParams:function(data){
        var out = [];
        for (var key in data) {
            if (data.hasOwnProperty(key)) {
                out.push(key + '=' + encodeURIComponent(data[key]));
            }
        }
        return out.join('&');
    },
    numbersToDays: function (numbers) {
        var res = '';
        var days = [
              {id: "1", value: "7", label: "Sunday" },
              {id: "2", value: "1", label: "Monday"},
              {id: "3", value: "2", label: "Tuesday"},
              {id: "4", value: "3", label: "Wednesday"},
              {id: "5", value: "4", label: "Thursday"},
              {id: "6", value: "6", label: "Saturday"}
          ];
        for(var i=0;i<numbers.length;i++) {
            for(var j=0;j<days.length;j++) {
                if(numbers[i] == days[j].value) {
                    res += days[j].label + ", ";
                }
            }
        }
        return res;
    },
    getPaymentMethod(payment = "" ){
        var payment_method = "BOOKING_PAYMENT_METHOD_EMPTY";
        if(payment === 'credit'){
            payment_method = "CREDIT_CARD"
        }
        else if(payment === 'cash'){
            payment_method = "CASH_ON_DELIVERY"
        }else{
            payment_method = "BOOKING_PAYMENT_METHOD_EMPTY"
        }
        return payment_method;
    },
    getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    },
    getCardTypeImage(platForm){
        var cardTypeImage = '';
        //console.log("platForm", platForm);

        if( platForm == "Visa Credit" ) {
            cardTypeImage = '/dist/images/new_visa_updated.png';
        }else if (platForm == "MasterCard Credit"){
            cardTypeImage = '/dist/images/new_master.png';
        }else{
            cardTypeImage = '/dist/images/new_visa_updated.png';
        }

        return cardTypeImage;
    },
    getDiscountText(discount, discountType, currency = 'AED' ) {

        var discountText = '';

        if(discountType == 'AMOUNT_OFF') {
            discountText = discount + " " +currency;
        }
        else if(discountType == 'PERCENT_OFF') {
            discountText = discount + "%";
        }

        return discountText;
    },
    getLWBDiscountText(discount, discountType, currency = 'AED' ) {

        var discountText = '';

        if(discountType == 'AMOUNT_OFF') {
            discountText = discount + " " +currency;
        }
        else if(discountType == 'PERCENT') {
            discountText = discount + "%";
        }

        return discountText;
    },
    getCookieByName(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    },
    getCookie(name) {
        var match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'));
        if (match) return match[2];
    },
    isUserLoggedIn(){
         var user_details = this.getCookieByName("_user_details") != "" ? JSON.parse(this.getCookieByName("_user_details")) : "";
         return typeof user_details == "object" && Object.keys(user_details).length ? true : false;
    },
    getUserToken(){
        var userToken = "";
        if(this.isUserLoggedIn()){
            var user_details = JSON.parse(this.getCookieByName("_user_details"));
            userToken = user_details.access_token;
        }
        return userToken;
    },
    getGoogleAPIKey(){
        return 'AIzaSyA5BM0Pc2XIVpL8E7LKfteKBgXpq1oOEZk';
    },

    getServiceSlugByParentID(url_constants, service_constants, serviceID) {
        // TODO uodate SERVICE_HOME_INSURANCE and SERVICE_CAR_INSURANCE reslut
        var res = '';
        switch (serviceID) {
            case service_constants.SERVICE_CLEANING_AND_MAID_SERVICES:
                res = url_constants.CLEANING_MAID_PAGE_URI;
                break;
            case service_constants.SERVICE_MAINTENANCE:
                res = url_constants.MAINTENANCE_PAGE_URI;
                break;
            case service_constants.SERVICE_PEST_CONTROL:
                res = url_constants.PEST_CONTROL_PAGE_URI;
                break;
            case service_constants.SERVICE_GARDENING:
                res = url_constants.GARDENING_PAGE_URI;
                break;
            case service_constants.SERVICE_PAINTING:
                res = url_constants.PAINTERS_PAGE_URI;
                break;
            case service_constants.SERVICE_LOCAL_MOVE:
                res = url_constants.LOCAL_MOVER_PAGE_URI;
                break;
            case service_constants.SERVICE_INTERNATIONAL_MOVE:
                res = url_constants.INTERNATIONAL_MOVER_PAGE_URI;
                break;
            case service_constants.SERVICE_STORAGE:
                res = url_constants.STORAGE_COMPANIES_PAGE_URI;
                break;
            case service_constants.SERVICE_CAR_SHIPPING:
                res = url_constants.CAR_SHIPPING_PAGE_URI;
                break;
            case 151:
                res = url_constants.BABYSITTING_PAGE_URI;
                break;
            case service_constants.SERVICE_CATERING:
                res = url_constants.CATERING_PAGE_URI;
                break;
            case service_constants.SERVICE_PHOTOGRAPHY:
                res = url_constants.PHOTOGRAPHY_PAGE_URI;
                break;
            case service_constants.SERVICE_CURTAINS__BLINDS:
                res = url_constants.CURTAINS_PAGE_URI;
                break;
            case service_constants.SERVICE_HOME_INSURANCE:
                res = url_constants.HOME_INSURANCE_PAGE_URI;
                break;
            case service_constants.SERVICE_CAR_INSURANCE:
                res = url_constants.CAR_INSURANCE_PAGE_URI;
                break;
        }
        return res;
    },
    getServiceImageNameByParentID(service_constants, serviceID) {
        var res = '/dist/images/';
        switch (serviceID) {
            case service_constants.SERVICE_CLEANING_AND_MAID_SERVICES:
                res += 'cleaning/';
                break;
            case service_constants.SERVICE_MAINTENANCE:
                res += 'maintenance/';
                break;
            case service_constants.SERVICE_PEST_CONTROL:
                res += 'pest-control/';
                break;
            case service_constants.SERVICE_GARDENING:
                res += 'gardening/';
                break;
            case service_constants.SERVICE_PAINTING:
                res += 'painting/';
                break;
            case service_constants.SERVICE_LOCAL_MOVE:
                res += 'local-moving/';
                break;
            case service_constants.SERVICE_INTERNATIONAL_MOVE:
                res += 'international-moving/';
                break;
            case service_constants.SERVICE_STORAGE:
                res += 'storage/';
                break;
            case service_constants.SERVICE_CAR_SHIPPING:
                res += 'car-shipping/';
                break;
            case service_constants.SERVICE_PARTTIME_NANNIES__BABYSITTERS:
                res += 'babysitting/';
                break;
            case service_constants.SERVICE_CATERING:
                res += 'catering/';
                break;
            case service_constants.SERVICE_PHOTOGRAPHY:
                res += 'photography/';
                break;
            case service_constants.SERVICE_CURTAINS__BLINDS:
                res += 'custom-curtains/';
                break;
            case service_constants.SERVICE_HOME_INSURANCE:
                res += 'home-insurance/';
                break;
            case service_constants.SERVICE_CAR_INSURANCE:
                res += 'car-insurance/';
                break;
        }
        res += 'desktop.jpg';
        return res;
    },
    getServiceIDBySlug(url_constants, service_constants, slug) {
        // TODO add SERVICE_HOME_INSURANCE and SERVICE_CAR_INSURANCE
        var res = 0;
        switch (slug) {
            case url_constants.CLEANING_MAID_PAGE_URI:
                res = service_constants.SERVICE_CLEANING_AND_MAID_SERVICES;
                break;
            case url_constants.MAINTENANCE_PAGE_URI:
                res = service_constants.SERVICE_MAINTENANCE;
                break;
            case url_constants.PEST_CONTROL_PAGE_URI:
                res = service_constants.SERVICE_PEST_CONTROL;
                break;
            case url_constants.GARDENING_PAGE_URI:
                res = service_constants.SERVICE_GARDENING;
                break;
            case url_constants.PAINTERS_PAGE_URI:
                res = service_constants.SERVICE_PAINTING;
                break;
            case url_constants.LOCAL_MOVER_PAGE_URI:
                res = service_constants.SERVICE_LOCAL_MOVE;
                break;
            case url_constants.INTERNATIONAL_MOVER_PAGE_URI:
                res = service_constants.SERVICE_INTERNATIONAL_MOVE;
                break;
            case url_constants.STORAGE_COMPANIES_PAGE_URI:
                res = service_constants.SERVICE_STORAGE;
                break;
            case url_constants.CAR_SHIPPING_PAGE_URI:
                res = service_constants.SERVICE_CAR_SHIPPING;
                break;
            case url_constants.BABYSITTING_PAGE_URI:
                res = 151;
                break;
            case url_constants.CATERING_PAGE_URI:
                res = service_constants.SERVICE_CATERING;
                break;
            case url_constants.PHOTOGRAPHY_PAGE_URI:
                res = service_constants.SERVICE_PHOTOGRAPHY;
                break;
            case url_constants.CURTAINS_PAGE_URI:
                res = service_constants.SERVICE_CURTAINS__BLINDS;
                break;
            case url_constants.CAR_INSURANCE_PAGE_URI:
                res = service_constants.SERVICE_CAR_INSURANCE;
                break;
            case url_constants.HOME_INSURANCE_PAGE_URI:
                res = service_constants.SERVICE_HOME_INSURANCE;
                break;
            case url_constants.LAUNDRY_DRY_CLEANING:
                res = service_constants.SERVICE_LAUNDRY_AND_DRY_CLEANING
                break;
        }
        return res;
    },
    getServiceReviewNameBySlug(url_constants, service_constants, slug) {
        var res = 'Home Services';
        switch (slug) {
            case url_constants.LOCAL_MOVER_PAGE_URI:
                res = "local moving";
                break;
            case url_constants.INTERNATIONAL_MOVER_PAGE_URI:
                res = "international moving";
                break;
            case url_constants.STORAGE_COMPANIES_PAGE_URI:
                res = "storage services";
                break;
            case url_constants.CAR_SHIPPING_PAGE_URI:
                res = "car shipping";
                break;
            case url_constants.CLEANING_MAID_PAGE_URI:
                res = "cleaning";
                break;
            case url_constants.PAINTERS_PAGE_URI:
                res = "painting";
                break;
            case url_constants.CURTAINS_PAGE_URI:
                res = "curtains and blind services";
                break;
            case url_constants.GARDENING_PAGE_URI:
                res = "gardening services";
                break;
            case url_constants.PEST_CONTROL_PAGE_URI:
                res = "pest control";
                break;
            case url_constants.MAINTENANCE_PAGE_URI:
                res = "maintenance and handyman services";
                break;
            case url_constants.HOME_INSURANCE_PAGE_URI:
                res = "home insurance";
                break;
            case url_constants.CAR_INSURANCE_PAGE_URI:
                res = "car insurance";
                break;
            case url_constants.CATERING_PAGE_URI:
                res = "catering";
                break;
            case url_constants.PHOTOGRAPHY_PAGE_URI:
                res = "photography";
                break;
            case 'deep-cleaning':
                res = "deep cleaning";
                break;
            case 'ac-maintenance':
                res = "ac maintenance";
                break;
            case 'electrician-services':
                res = "electrician services";
                break;
            case 'plumbers':
                res = "plumbers";
                break;
            case 'carpenters':
                res = "carpenters";
                break;
            case 'locksmiths':
                res = "locksmiths";
                break;
            case url_constants.SOFA_AND_UPHOLSTERY_CLEANING_PAGE_URI:
                res = "locksmiths";
                break;
            case url_constants.CARPET_CLEANING_PAGE_URI:
                res = "Carpet cleaning";
                break;
            case url_constants.MATTRESS_CLEANING_PAGE_URI:
                res = "Mattress cleaning";
                break;
            case url_constants.WINDOW_CLEANING_PAGE_URI:
                res = "Window cleaning";
                break;
            case 'office-cleaning':
                res = "Office Cleaning";
                break;
            case url_constants.POOL_CLEANING_PAGE_URI:
                res = "Pool  cleaning";
                break;
            case 'water-tank-cleaning':
                res = "Water tank cleaning";
                break;
            case url_constants.LAUNDRY_DRY_CLEANING:
                res = "Laundry services";
            break;
        }
        return res;
    },
    replaceCurrentCityName(text, existingCurrentCityData=[]){
        var currentCityData = (typeof window != "undefined") ? window.REDUX_DATA.currentCityData : existingCurrentCityData;
        var cityName = "";
        var returnText = text;
        if(currentCityData.length){
            cityName = currentCityData[0].name;
            return returnText.replace(/%s/g, cityName)
        }
        return returnText;
    },
    replaceCodedCurrentCityName(text, currentCity){
        var cityName = this.toTitleCase(currentCity.replace('-', ' '));
        return text.replace(/%s/g, cityName)
    },
    getSeoFilterTextArray(serviceId, key = "", existingServiceConstants = {}){
        var serviceConstants = (typeof window != "undefined") ? window.REDUX_DATA.serviceConstants: existingServiceConstants;
        var seoText = {};
        var returndata = {};
        if(Object.keys(serviceConstants).length) {
            seoText[serviceConstants.SERVICE_LOCAL_MOVE] = {
                'header':'Get free quotes from local movers in %s',
                'seoTitle':'Please select up to 5 local moving companies in %s',
                'seoTitlePromo':'Get quotes from local movers in %s that are currently running promotions',
                'seoDescription':'Please select up to 5 local movers in %s that you would like to receive custom quotes from, and our customer care team will contact you shortly.',
                'seoDescriptionPromo':'Please select up to 5 local movers in %s that are currently running promotions and our customer care team will contact you shortly to get you the best moving rates.',
                'maxLimit': 5
            };

            seoText[serviceConstants.SERVICE_INTERNATIONAL_MOVE] = {
                'header':'Get free quotes from international movers in %s',
                'seoTitle':'Please select up to 5 international shipping companies in %s',
                'seoTitlePromo':'Get quotes from international movers in %s that are currently running promotions',
                'seoDescription':'Please select up to 5 international movers in %s that you would like to receive custom quotes from, and our customer care team will contact you shortly.',
                'seoDescriptionPromo':'Please select up to 5 international movers in %s that are currently running promotions and our customer care team will contact you shortly to get you the best moving rates.',
                'maxLimit': 5
            }
            
            seoText[serviceConstants.SERVICE_STORAGE] = {
                'header':'Get free quotes from storage companies in %s',
                'seoTitle':'Please select up to 5 storage companies in %s',
                'seoTitlePromo':'Get quotes from storage companies in %s that are currently running promotions',
                'seoDescription':'Please select up to 5 storage companies in %s that you would like to receive custom quotes from, and our customer care team will contact you shortly.',
                'seoDescriptionPromo':'Please select up to 5 storage companies in %s that are currently running promotions and our customer care team will contact you with your free quotes.',
                'maxLimit': 5
            }
            seoText[serviceConstants.SERVICE_CLEANING_AND_MAID_SERVICES] = {
                'header':'Get free quotes from maid and cleaning companies in %s for cleaning services',
                'seoTitle':'Please select up to 3 cleaning and maid companies in %s',
                'seoTitlePromo':'Get quotes from cleaning companies in %s that are currently running promotions',
                'seoDescription':'Please select up to 3 cleaning and maid companies in %s that you would like to receive custom quotes from, and the companies will contact you shortly.',
                'seoDescriptionPromo':'Please select up to 3 cleaning companies in %s that are currently running promotions, and they will contact you shortly with your free quotes.',
                'maxLimit': 3
            }
            seoText[serviceConstants.SERVICE_MAINTENANCE] = {
                'header':'Get free quotes from maintenance companies in %s',
                'seoTitle':'Please select up to 3 maintenance companies in %s',
                'seoTitlePromo':'Get quotes from maintenance companies in %s that are currently running promotions',
                'seoDescription':'Please select up to 3 maintenance companies in %s that you would like to receive custom quotes from, and the companies will contact you shortly.',
                'seoDescriptionPromo':'Please select up to 3 maintenance companies in %s that are currently running promotions, and they will contact you shortly with your free quotes.',
                'maxLimit': 3
            }
            seoText[serviceConstants.SERVICE_PAINTING] = {
                'header':'Get free quotes from painting companies in %s for wall painting services',
                'seoTitle':'Please select up to 3 painting companies in %s',
                'seoTitlePromo':'Get quotes from painting companies in %s that are currently running promotions',
                'seoDescription':'Please select up to 3 painting companies in %s that you would like to receive custom quotes from, and the companies will contact you shortly.',
                'seoDescriptionPromo':'Please select up to 3 painting companies in %s that are currently running promotions, and they will contact you shortly with your free quotes.',
                'maxLimit': 3
            }
            seoText[serviceConstants.SERVICE_PEST_CONTROL] = {
                'header':'Get free quotes from pest control companies in %s',
                'seoTitle':'Please select up to 3 pest control companies in %s',
                'seoTitlePromo':'Get quotes from pest control companies in %s that are currently running promotions',
                'seoDescription':'Please select up to 3 pest control companies in %s that you would like to receive custom quotes from, and the companies will contact you shortly.',
                'seoDescriptionPromo':'Please select up to 3 pest control companies in %s that are currently running promotions, and they will contact you shortly with your free quotes.',
                'maxLimit': 3
            }
            seoText[serviceConstants.SERVICE_GARDENING] = {
                'header':'Get free quotes from landscaping companies in %s',
                'seoTitle':'Please select up to 3 landscaping companies in %s',
                'seoTitlePromo':'Get quotes from landscaping companies in  %s that are currently running promotions',
                'seoDescription':'Please select up to 3 landscaping companies in %s that you would like to receive custom quotes from, and the companies will contact you shortly.',
                'seoDescriptionPromo':'Please select up to 3 landscaping companies in %s that are currently running promotions, and they will contact you shortly with your free quotes.',
                'maxLimit': 3
            }

            seoText[serviceConstants.SERVICE_HOME_INSURANCE] = {
                'header':'Compare quotes and buy home insurance from insurance providers in %s',
                'seoTitle':' Compare live quotes from the home insurance companies listed below',
                'seoTitlePromo':'Get quotes from home insurance companies in %s that are currently running promotions',
                'seoDescription':'With ServiceMarket, you can view live custom quotes for home insurance from the companies listed below, and buy your policy directly on our site. Just press the "Compare and buy home insurance" button to start the process.',
                'seoDescriptionPromo':'Please select up to 3 home insurance companies in %s that are currently running promotions, and our customer care team will contact you shortly with your free quotes.',
                'maxLimit': 3
            }
            seoText[serviceConstants.SERVICE_CAR_INSURANCE] = {
                'header':'Compare quotes and buy car insurance from insurance providers in %s',
                'seoTitle':'Compare live quotes from the car insurance companies listed below',
                'seoTitlePromo':'Get quotes from car insurance companies in %s that are currently running promotions',
                'seoDescription':'With ServiceMarket, you can view live custom quotes for car insurance from the companies listed below, and buy your policy directly on our site. Just press the "Compare and buy car insurance" button to start the process.',
                'seoDescriptionPromo':'Please select up to 3 car insurance companies in %s that are currently running promotions, and our customer care team will contact you shortly with your free quotes.',
                'maxLimit': 3
            }
            seoText[serviceConstants.SERVICE_CAR_SHIPPING] = {
                'header':'Get free quotes from car shipping companies in %s',
                'seoTitle':'Please select up to 3 car shipping companies in %s',
                'seoTitlePromo':'Get quotes from car shipping companies in %s that are currently running promotions',
                'seoDescription':'Please select up to 3 car shipping companies in %s that you would like to receive custom quotes from and the companies will contact you shortly.',
                'seoDescriptionPromo':'Please select up to 3 car shipping companies in %s that are currently running promotions, and our customer care team will contact you shortly with your free quotes.',
                'maxLimit': 3
            }

            seoText[serviceConstants.SERVICE_CURTAINS__BLINDS] = {
                'header':'Get free quotes from companies for custom blinds and curtains in %s',
                'seoTitle':'Please select up to 5 curtains &amp; blinds companies in %s',
                'seoTitlePromo':'Get quotes from curtains &amp; blinds companies in %s',
                'seoDescription':'Please select up to 5 professional curtains and blinds companies in %s that you would like to receive custom quotes from, and our customer care team will contact you shortly.',
                'seoDescriptionPromo':'Please select up to 5 curtains &amp; blinds companies in %s that are currently running promotions and our customer care team will contact you shortly to get you the best moving rates.',
                'maxLimit': 5
            }
            seoText[serviceConstants.SERVICE_CATERING] = {
                'header':'Get free quotes from catering companies in %s',
                'seoTitle':'Please select up to 5 catering companies in %s',
                'seoTitlePromo':'Get quotes from catering companies in %s',
                'seoDescription':'Please select up to 5 professional catering companies in %s that you would like to receive custom quotes from, and our customer care team will contact you shortly.',
                'seoDescriptionPromo':'Please select up to 5 catering in %s that are currently running promotions and our customer care team will contact you shortly to get you the best moving rates.',
                'maxLimit': 5
            }
            seoText[serviceConstants.SERVICE_PHOTOGRAPHY] = {
                'header':'Get free quotes from professional photographers in %s',
                'seoTitle':'Please select up to 5 photographers and photo studios in %s',
                'seoTitlePromo':'Get quotes from photography companies in %s',
                'seoDescription':'Please select up to 5 professional photographers and photo studios in %s that you would like to receive custom quotes from, and our customer care team will contact you shortly.',
                'seoDescriptionPromo':'Please select up to 5 photography in %s that are currently running promotions and our customer care team will contact you shortly to get you the best moving rates.',
                'maxLimit': 5
            }

            seoText[serviceConstants.SERVICE_PARTTIME_NANNIES__BABYSITTERS] = {
                'header':'Get free quotes from professional babysitters and nannies in %s',
                'seoTitle':'Please select up to 5 babysitting and nanny agencies in %s',
                'seoTitlePromo':'Get quotes from babysitting companies in %s',
                'seoDescription':'Please select up to 5 professional babysitting and nanny agencies in %s that you would like to receive custom quotes from, and our customer care team will contact you shortly.',
                'seoDescriptionPromo':'Please select up to 5 babysitting in %s that are currently running promotions and our customer care team will contact you shortly to get you the best moving rates.',
                'maxLimit': 5
            }
            if(key != "" && ( typeof seoText[serviceId] != "undefined" && typeof seoText[serviceId][key] != "undefined" )){
                returndata = seoText[serviceId][key];
            }
            else if(typeof seoText[serviceId] != "undefined"){
                returndata = seoText[serviceId];
            }else{
                returndata = {};
            }
        }
        return returndata;
    },
    getSeoJourney2TextArray(serviceId, key = "", existingServiceConstants = {}){
        var serviceConstants = (typeof window != "undefined") ? window.REDUX_DATA.serviceConstants: existingServiceConstants;
        var seoText = {};
        var returndata = {};
        if(Object.keys(serviceConstants).length) {
            seoText[serviceConstants.SERVICE_LOCAL_MOVE] = {
                'seoTitle':'Choose up to 3 %s Movers and UAE Moving Companies',
                'seoDescription':'Request quotes from your own pick of %s moving companies based on average price, customer reviews, services offered, promotions availability and more!',
                'maxLimit': 5
            };

            seoText[serviceConstants.SERVICE_INTERNATIONAL_MOVE] = {
                'seoTitle':'Choose up to 5 %s International Movers and Shipping Companies',
                'seoDescription':'Request quotes from your selection of international moving companies in %s based on average price, customer reviews, services offered, promotions and more!',
                'maxLimit': 5
            }

            seoText[serviceConstants.SERVICE_STORAGE] = {
                'seoTitle':'Select up to 3 Storage Companies for Storage in %s, UAE',
                'seoDescription':'Choose %s storage companies based on your specifications like price, customer ratings & services offered such as self-storage & climate controlled storage',
                'maxLimit': 5
            }
            seoText[serviceConstants.SERVICE_CLEANING_AND_MAID_SERVICES] = {
                'seoTitle':'Cleaning & Maid Companies in %s for Cleaning Services',
                'seoDescription':'Need a %s housemaid or cleaning services for your home? Read reviews and either get quotes from %s maid companies or book one right away on ServiceMarket!',
                'maxLimit': 3
            }
            seoText[serviceConstants.SERVICE_MAINTENANCE] = {
                'seoTitle':'Choose up to 3 Handyman & Maintenance Companies in %s',
                'seoDescription':'Need a handyman in %s for maintenance services for your home? Read customer reviews & select from %s maintenance companies and be contacted right away!',
                'maxLimit': 3
            }
            seoText[serviceConstants.SERVICE_PAINTING] = {
                'seoTitle':'Choose up to 3 Painters for Painting Services in %s',
                'seoDescription':'Need a %s painter or expert wall painting services for your home? Read customer reviews & quickly select from painting companies and be contacted right away!',
                'maxLimit': 3
            }
            seoText[serviceConstants.SERVICE_PEST_CONTROL] = {
                'seoTitle':'Choose up to 3 Pest Control Companies in %s | ServiceMarket',
                'seoDescription':'Need %s pest control services for your home? Read customer reviews & quickly select up to 3 pest control companies in %s and be contacted right away!',
                'maxLimit': 3
            }
            seoText[serviceConstants.SERVICE_GARDENING] = {
                'seoTitle':'Choose up to 3 Gardening Companies in %s for Gardening Quotes',
                'seoDescription':'Read reviews of %s gardening and landscaping companies that offer gardening services for your %s garden. Select 3 companies to be contacted right away!',
                'maxLimit': 3
            }

            seoText[serviceConstants.SERVICE_HOME_INSURANCE] = {
                'seoTitle':' Choose up to 3 Home & Contents Insurance Companies in %s',
                'seoDescription':'Read customer reviews and compare quotes for property, home contents, or personal belongings insurance from up to 3 home insurance companies in %s.',
                'maxLimit': 3
            }
            seoText[serviceConstants.SERVICE_CAR_INSURANCE] = {
                'seoTitle':'Choose up to 3 Car Insurance Companies in %s to Compare Quotes',
                'seoDescription':'Read online customer reviews of car insurance companies in %s that offer motor insurance, and compare free personalized quotes from up to 3 companies!',
                'maxLimit': 3
            }
            seoText[serviceConstants.SERVICE_CAR_SHIPPING] = {
                'seoTitle':'Find %s Car Shipping Companies to Transport Your Vehicle',
                'seoDescription':'Read customer reviews and request quotes from a selection of car shipping companies in %s, UAE for expert freight and cargo shipping services',
                'maxLimit': 3
            }

            seoText[serviceConstants.SERVICE_CURTAINS__BLINDS] = {
                'seoTitle':'Choose up to 3 %s curtains and blinds companies to get quotes',
                'seoDescription':'Choose Up To 3 Companies To Send You Quotes',
                'maxLimit': 5
            }
            seoText[serviceConstants.SERVICE_CATERING] = {
                'seoTitle':'Choose up to 5 catering companies in %s',
                'seoDescription':'Request quotes for catering services in %s from professional catering companies based on menus, customer reviews, services offered, promotions and more!',
                'maxLimit': 5
            }
            seoText[serviceConstants.SERVICE_PHOTOGRAPHY] = {
                'seoTitle':'Choose up to 5 photo studios or photographers in %s',
                'seoDescription':'Request quotes for photography services from professional %s photographers and photo studios based on customer reviews, services offered, promotions and more!',
                'maxLimit': 5
            }

            seoText[serviceConstants.SERVICE_PARTTIME_NANNIES__BABYSITTERS] = {
                'seoTitle':'Choose up to 5 babysitting and nanny agencies in %s',
                'seoDescription':'Request quotes for nanny services from professional %s nannies and babysitters based on customer reviews, services offered, qualifications and more!',
                'maxLimit': 5
            }
            if(key != "" && ( typeof seoText[serviceId] != "undefined" && typeof seoText[serviceId][key] != "undefined" )){
                returndata = seoText[serviceId][key];
            }
            else if(typeof seoText[serviceId] != "undefined"){
                returndata = seoText[serviceId];
            }else{
                returndata = {};
            }
        }
        return returndata;
    },
    getSeoHomePageTextArray( lang = DEFAULT_LANG ){
        var seoText = {
            seoTitle: locationHelper.translate('HOME_PAGE_META_TITLE',lang),
            seoDesc: locationHelper.translate('HOME_PAGE_META_DESC',lang)
        }
        //locationHelper.translate('AC_SERVICE_REPAIR_BOOKING',lang)}
        //if(lang ==)
        return seoText;
    },
    getSeoCareersTextArray(){
        var seoText = {
            seoTitle: 'Work at ServiceMarket',
            seoDesc: 'If you’re someone who’s looking for a fast-paced and fun place to work, where you can help solve real world problems, then ServiceMarket is the place for you.'
        }
        return seoText;
    },
    getSeoPartnersTextArray(){
        var seoText = {
            seoTitle: 'Our Partners in %s | ServiceMarket',
            seoDesc: 'Our network of partners in %s is always growing. You can find movers, storage companies, cleaners, handymen and even landscapers for all your household needs!'
        };
        return seoText;
    },
    getSeoContactUsTextArray(){
        var seoText = {
            seoTitle: 'Contact & Find Us | ServiceMarket',
            seoDesc: 'Do you have an inquiry we can help with? Do you have some feedback for us? Just fill out the form and we\'ll do our very best to get back to you right away!'
        };
        return seoText;
    },
    getHomeCarInsuranceLinks(serviceID, service_constants){
        if (serviceID == service_constants.SERVICE_HOME_INSURANCE) {
            return '/en/uae/insurance/quotes/get/home';
        }
        return '/en/uae/insurance/quotes/get/motor';
    },
    getServiceProviderDetails(serviceProvider, nestedServices, parentServices) {
        var reviews = [];
        var OfferCatalog = [];
        parentServices.map(item => {
            var itemListElement = [];
            if (nestedServices[item.id]) {
                (nestedServices[item.id]).map(offer => {
                    itemListElement.push({"@type":"Offer","itemOffered":{"@type":"Service","name":offer.label}});
                });
            }
            OfferCatalog.push({
                "@type":"OfferCatalog",
                "name": item.label,
                "itemListElement": itemListElement
            })
        })
        if (serviceProvider && serviceProvider.customerReviews) {
            serviceProvider.customerReviews.map(item => {
                reviews.push({
                    "@type":"Review",
                    "author":item.reviewerName,
                    "datePublished":moment(item.creationDate).format("DD/MM/YYYY"),
                    "name": item.approvedTitle,
                    "description": item.approvedReview,
                    "reviewRating":{"@type":"Rating","ratingValue":item.rating,"bestRating":5}
                });
            });
        }


        var details = {
            "@context":"http:\/\/schema.org",
            "@type":"LocalBusiness",
            "name": serviceProvider.name,
            "logo": serviceProvider.logo,
            "image": serviceProvider.logo,
            "url": serviceProvider.website,
            "telephone": serviceProvider.headOffice.addressResponse.phoneNumber,
            "aggregateRating": {
                "@type":"AggregateRating",
                "ratingValue": serviceProvider.averageRating,
                "bestRating":5,
                "ratingCount": serviceProvider.rating,
                "reviewCount": serviceProvider.numberOfReviews
            },
            "priceRange": serviceProvider.providerClass,
            "hasOfferCatalog":{
                "@type":"OfferCatalog",
                "itemListElement":OfferCatalog
            },
            "memberOf":[{"@type":"Organization","name":(serviceProvider.accreditation.length?(serviceProvider.accreditation[0]).accreditation:'')}],
            "contactPoint":{
                "@type":"ContactPoint",
                "name": serviceProvider.contactPersonName,
                "contactType":"Sales","telephone": (serviceProvider.contactInformation?serviceProvider.contactInformation.phone:'')
            },
            "address":{
                "@type":"PostalAddress",
                "streetAddress": serviceProvider.headOffice.addressResponse.line1,
                "addressLocality": serviceProvider.headOffice.addressResponse.city,
                "addressCountry":{"@type":"Country","name": serviceProvider.headOffice.addressResponse.country}
                },
            "review": reviews
        }
        return details;
    },
    timeSlotElements($time){

        var $time_label = $time >= 12 ? ($time-12)+" PM" : $time+" AM";

        if($time_label == "0 PM"){
            $time_label = "12 PM";
        }

        var $timeId = $time < 10 ? "0"+$time+"00" : $time+"00" ;

        var $timeValue = $time < 10 ? "0"+$time+":00" : $time+":00" ;

        var slot = {"id": $timeId, "value": $timeValue, "label": $time_label};

        return slot;
    },
    updateTimingSlot(booking_date){

        var time = typeof window != "undefined" && (typeof window.REDUX_DATA.bookingDateTimeAvailabilityReducer != "undefined" && typeof window.REDUX_DATA.bookingDateTimeAvailabilityReducer.bookingTimings!= "undefined") ? window.REDUX_DATA.bookingDateTimeAvailabilityReducer.bookingTimings: [];

        var new_slots = [];

        var i = 0; // Time indexing to Select Time 0 for Default Timing and 1 for Ramdan
        var isDateFallonRamdan = false;

        booking_date = booking_date.split('-').reverse().join('-');

        if(time.length) {

            var selectedDate = moment(booking_date),
                now = moment(window.REDUX_DATA.dateAndTime), // server time should pass
                nowDay = now.day();

            var day = selectedDate.day(),
                diff = selectedDate.add(1, 'day').diff(now, 'days'),
                hour = now.hour(),
                minutes = now.minutes(),
                startHour = parseInt(time[i].startHour),
                workingHours = parseInt(time[i].workingHours),
                closingHour = startHour + workingHours,
                //selectedTime = $('input[name=job_time]:checked').val(),
                selectableHourStart = startHour,
                hourRequired = parseInt(time[i].hoursRequired),
                closedTime = 0;

            if (hour < startHour) {
                hour = startHour;
                closedTime = 1;
            } else if (hour > closingHour) {
                hour = closingHour;
                closedTime = 1;
            }


            if (diff == 0) {
                selectableHourStart = hour + hourRequired;
                if (minutes > 1 && closedTime == 0) {
                    selectableHourStart++; //skip currently running hour
                }

            } else if (diff == 1 || (diff == 2 && day == 6)) {

                if (diff == 1 && day == 6) { // if its saturday and today is friday( 1 day different )the slot should start from 1pm.
                    selectableHourStart = 13;
                } else {
                    // next day should start by compensating the remaining required hours
                    selectableHourStart = startHour - closingHour + (hour + hourRequired);
                }

            }

            for (selectableHourStart; selectableHourStart <= closingHour; selectableHourStart++) {
                if (isDateFallonRamdan == 1 && selectableHourStart == closingHour) { // hiding ramlan times.
                    selectableHourStart = selectableHourStart + 1;
                }
                if (selectableHourStart >= startHour) {
                    var slot = this.timeSlotElements(selectableHourStart);
                    new_slots.push(slot)
                }
            }
        }
        //console.log(new_slots);

        return new_slots;
    },
    isSlotAvailable(bookingDate, timeOfBooking = ""){
        var booking_time = "";
        var returnVal = false;
        var selectedSlotAvailable = [];
        var slots = [];
        if(bookingDate != ""){
            slots =  this.updateTimingSlot(bookingDate);
        }
        if(timeOfBooking != "") {
            selectedSlotAvailable = slots.filter(
                (item) => (item.label == timeOfBooking.label
            ));
            if( selectedSlotAvailable.length ){
                booking_time = timeOfBooking.label
                returnVal = false;//timeOfBooking.label
            }else{
                booking_time = "";
                returnVal = true;
            }
        }
        return returnVal;
    },
    jsUcfirst(string) {
        return string.charAt(0).toUpperCase() + string.slice(1).toLocaleLowerCase();
    },
    toTitleCase(str) {
        return str.replace(
            /\w\S*/g,
            function(txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            }
        );
    },
    getCurrentTime(thisVar){
        var countryId = locationHelper.getCurrentCountryId();
        const currentDateTime = thisVar.props.fetchDateTime(countryId);
        Promise.all([currentDateTime]).then(()=> {
            //window.REDUX_DATA["dateAndTime"] = reduxState.dateAndTime;
            thisVar.setState({
                startDate: startDate
            })
        }).catch(function (error) {
            // handle error
            // console.log("Error Fetch", error);
        });
    },
    /*setCurrentAPISerivces(thisVar, city,cities){
        var cityCode = getCityCode(city, cities);
        const fetchAllApiServicesCall = thisVar.props.fetchAllApiServices(cityCode);
        thisVar.setState({
            loader: true
        })
        Promise.all([fetchAllApiServicesCall]).then(() => {
            thisVar.setState({
                loader: false
            })
        }).catch(function (error) {
            // handle error
            console.log("Error setCurrentAPISerivces", error);
        });
    },*/
    isServiceAvailable(service_id){
        var apiServices = window.REDUX_DATA.apiServices
    },
    getHomePageImage(city){
        return 'header-' + city + '.jpg';
    },
    isBooking(service_id){
        var arr = []; 
        var bookingServices = (typeof window != "undefined" && typeof window.REDUX_DATA.apiServices != "undefined") ? window.REDUX_DATA.apiServices : [];
        bookingServices.map((item) => { 
              if( item.bookingEnabled != 0){
                arr.push(item.id);
              }
              item.subServices.filter(service => service.bookingEnabled != 0 ? arr.push(service.id) : [] ) 
        })
        //console.log("arr",arr);
        return ( arr.length && arr.includes(service_id) ) ? true : false;
    },
    zendeskMapForServices( lang = DEFAULT_LANG ) {
        return [
            {id: 0, value:'ac_service_booking', label: locationHelper.translate('AC_SERVICE_REPAIR_BOOKING',lang)},
            {id: 1, value:'babysitters', label: locationHelper.translate('BABYSITTING',lang)},
            {id: 2, value:'car_shipping', label: locationHelper.translate('CAR_SHIPPING',lang)},
            {id: 3, value:'catering', label: locationHelper.translate('CATERING',lang)},
            {id: 4, value:'booking_cleaning', label: locationHelper.translate('CLEANING_BOOKING',lang)},
            {id: 5, value:'curtains_blinds', label: locationHelper.translate('CURTAINS_BLINDS',lang)},
            {id: 6, value:'deep_cleaning', label: locationHelper.translate('DEEP_CLEANING',lang)},
            {id: 7, value:'water_tank_cleaning_booking', label: locationHelper.translate('WATER_TANK_CLEANING_BOOKING',lang)},
            {id: 8, value:'mattress_cleaning', label: locationHelper.translate('MATTRESS_CLEANING',lang)},
            {id: 9, value:'mattress_cleaning_booking', label: locationHelper.translate('MATTRESS_CLEANING_BOOKING',lang)},
            {id: 10, value:'electrician_booking', label: locationHelper.translate('ELECTRICIAN_BOOKING',lang)},
            {id: 11, value:'carpet_cleaning', label: locationHelper.translate('CARPET_CLEANING',lang)},
            {id: 12, value:'upholstery_cleaning', label: locationHelper.translate('SOFA_CLEANING',lang)},
            {id: 13, value:'gardening', label: locationHelper.translate('HANDYMAN_BOOKING',lang)},
            {id: 14, value:'handyman_booking', label: locationHelper.translate('GARDENING',lang)},
            {id: 15, value:'insurance', label: locationHelper.translate('INSURANCE',lang)},
            {id: 16, value:'maintenance', label: locationHelper.translate('MAINTENANCE',lang)},
            {id: 17, value:'moving', label: locationHelper.translate('MOVING',lang)},
            {id: 18, value:'moving_booking', label: locationHelper.translate('MOVING_BOOKING',lang)},
            {id: 19, value:'painting', label: locationHelper.translate('PAINTING',lang)},
            {id: 20, value:'booking_painting', label: locationHelper.translate('PAINTING_BOOKING',lang)},
            {id: 21, value:'pest_control_booking', label: locationHelper.translate('PEST_CONTROL_BOOKING',lang)},
            {id: 22, value:'photography', label: locationHelper.translate('PHOTOGRAPHY',lang)},
            {id: 23, value:'plumbing_booking', label: locationHelper.translate('PLUMBING_BOOKING',lang)},
            {id: 24, value:'storage', label: locationHelper.translate('STORAGE',lang)},
            {id: 25, value:'', label: locationHelper.translate('OTHER',lang)},
            ]
    },splitString(str, separator, limit) {
        str = str.split(separator);
    
        if(str.length > limit) {
            var ret = str.splice(0, limit);
            ret.push(str.join(separator));
    
            return ret;
        }
    
        return str;
    },
    encryptBody(data) {
        // var ciphertext = CryptoJS.AES.encrypt(JSON.stringify(data), staticVariables.ENCRYPTION_PASS).toString();
        // return ciphertext;
        return data;
    },
    getUserAddress(stateData){
        let city_object = stateData.input_address_city;
        let addressDetails = {
            "apartment": stateData.input_address_area_building_apartment,
            //"area": stateData.input_address_area.id,
            "building": stateData.input_address_area_building_name,
            "cityId": city_object.id,
            "phoneNumber": stateData.input_phone
        }
        let userSelectedArea = this.getUserSelectedArea(stateData);
        if(userSelectedArea == null){
            addressDetails['area'] = null;
            addressDetails['areaTitle'] = this.getUserSelectedArea(stateData, true);
        }else{
            addressDetails['area'] = userSelectedArea;
        }
        return addressDetails;
    },
    getUserSelectedArea(stateData, returnText = false){
        let areaId = (stateData.input_address_area != null && typeof stateData.input_address_area.id != "undefined") ? stateData.input_address_area.id : null;
        areaId = ( typeof stateData.userAddress != "undefined" && (stateData.userAddress.area_id != null && typeof stateData.userAddress.area_id != "undefined")) ? stateData.userAddress.area_id : areaId;
        return returnText ? stateData.input_address_area : areaId;
    },
    isLocalStorageAvailable(){
        let hasLocalStorage = null;
        try {
            localStorage.testBit = 1;
            hasLocalStorage = true;
        } catch (e) {
            hasLocalStorage = false;
        }
        return hasLocalStorage;
    }
}
export default commonHelper;
