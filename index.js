require( "babel-register" )( {
    presets: [ "react", "env" , "stage-0"],
    plugins: [
        "dynamic-import-node",
        "syntax-dynamic-import",
        "react-loadable/babel"
      ],
} );
require.extensions['.css'] = () => {
    return;
};
require( "./server" );
